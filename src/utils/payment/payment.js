import React, { Component } from "react";
// import FormControlLabel from "@material-ui/core/FormControlLabel";
// import Radio from "@material-ui/core/Radio";
// import RadioGroup from "@material-ui/core/RadioGroup";
import "react-datepicker/dist/react-datepicker.css";
import "../../scss/common-payment.scss";
import ZenForm from "../../components/input/form";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
// import ListItemText from "@material-ui/core/ListItemText";
import cashIcon from "../../assets/icons/cash-icon.svg";
import walletIcon from "../../assets/icons/wallet_icon.svg";
import cardIcon from "../../assets/icons/card_icon.svg";
import chequeIcon from "../../assets/icons/cheque_icon.svg";
import netbankingIcon from "../../assets/icons/netbanking_icon.svg";
import zqmoneyIcon from "../../assets/icons/zqmoney_icon.svg";
import loanIcon from "../../assets/icons/loan.svg";
import SchoolIcon from '../../assets/icons/school.svg';
class Payment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paymentType: "",
      receiptFormData: [],
      paymentTypes: [],
      activeTab: 0,
      refund: false,
    };
  }
  componentWillMount = () => {
    console.log("props", this.props);
    let newData = this.props.receiptFormData;
    this.setState({
      receiptFormData: newData,
      paymentTypes: this.props.paymentTypes,
      paymentType: this.props.paymentType,
      refund: this.props.refund,
      activeTab: 0
    });
    this.onGetForm(this.props.paymentType);
    console.log(this.state);
  };

  selectPaymentType = (e, i) => {
    this.setState({ paymentType: e, receiptFormData: [], activeTab: i }, () => {
      this.onGetForm(e);
      if (this.props.onPaymentModeSelection != null) {
        this.props.onPaymentModeSelection(e);
      }
    });
  };

  onGetForm = (type) => {
    let formData = [];
    this.props.receiptFormData.map((item) => {
      if (type == item.paymentType || item.category == "button") {
        formData.push(item);
      }
    });
    this.setState({ receiptFormData: formData });
  };

  onSubmit = (data, item, format) => {
    console.log("***data***", item);
    let paymentType = this.state.paymentType;
    if (this.props.onPaymentSubmit != null) {
      this.props.onPaymentSubmit(data, item, format, paymentType);
    }
  };
  onInputChanges = (value, item, event, dataS) => {
    console.log(value, item, event, dataS);
    // item['defaultValue'] = value
    if (this.props.onInputChanges != undefined) {
      this.props.onInputChanges(value, item, event, dataS);
    }
  };
  formBtnHandle = (item) => {
    this.props.onFormBtnEvent(item);
  };
  cleanDataNew = (value, item, event, dataS) => {
    console.log(value, item, event, dataS);
  }
  render() {
    return (
      <React.Fragment>
        <div className="payment-tab-wrapper">


          <div className="payment-wrap">
            <div className="mode-of-payment">
              <div className="payment-title">
                ADD {this.state.refund ? "REFUND AMOUNT" : "PAYMENT"}
              </div>
              <div className="paymenttype-radioGroup">
                {this.state.paymentTypes.map((item, index) => {
                  return (
                    <List component="nav" aria-label="main mailbox folders" className={this.state.paymentType == item ? "list-items active-payment-tab" : "list-items"}>
                      <ListItem
                        button
                        onClick={this.props.onPaymentPreview == true ? null : () => this.selectPaymentType(item, index)}
                      >
                        <ListItemIcon>
                          {item === "zqMoney" ? (
                            <img alt="icon" src={zqmoneyIcon} />
                          ) : item === "Cash" ? (
                            <img alt="icon" src={cashIcon} />
                          ) : item === "Cheque/DD" ? (
                            <img alt="icon" src={chequeIcon} />
                          ) : item === "Netbanking" ? (
                            <img alt="icon" src={netbankingIcon} />
                          ) : item === "Card" ? (
                            <img alt="icon" src={cardIcon} />
                          ) : item === "Scholarship" ? (
                            <img alt="icon" src={SchoolIcon} />
                          ) : item === "Loan" ? (
                            <img alt="icon" src={loanIcon} />
                          ) : (
                                          <img alt="icon" src={walletIcon} />
                                        )}
                        </ListItemIcon>
                        {item}
                      </ListItem>
                    </List>
                  );
                })}
                {/* <RadioGroup
                  column
                  aria-label="position"
                  name="position"
                  defaultValue="Document"
                >
                  {this.state.paymentTypes.map((item) => {
                    return (
                      <FormControlLabel
                        value={item}
                        control={
                          <Radio
                            color="primary"
                            disabled={this.props.disabled}
                          />
                        }
                        label={item}
                        checked={this.state.paymentType === item}
                        onChange={() => this.selectPaymentType(item)}
                      />
                    );
                  })}
                </RadioGroup> */}
              </div>
            </div>
            <div className="payment">
              {/* <p className="payment-type-title"> {this.state.paymentType} Transaction</p> */}
              {this.state.receiptFormData.length > 0 ? (
                <ZenForm
                  inputData={this.state.receiptFormData}
                  onSubmit={this.onSubmit}
                  onInputChanges={this.onInputChanges}
                  onFormBtnEvent={(item) => this.formBtnHandle(item)}
                  cleanData={(value, item, event, dataS) => { this.cleanDataNew(value, item, event, dataS) }}
                />
              ) : null}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default Payment;
