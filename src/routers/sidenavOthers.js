import DoneAllIcon from '@material-ui/icons/DoneAll';
export default {
    items: [{
        icon: "zq-icon-dashboard",
        name: "Dashboard",
        title: "View Dashboard",
        url: '/:baseURL/main/dashboard',
        children: []
    },
    {
        icon: "zq-icon-transaction",
        name: "Transactions",
        title: "Manage Transactions",
        children: [
            // {
            //     name: "Deposit",
            //     url: "/:baseURL/main/transactions/deposit"
            // },
            {
                name: "Demand Note",
                url: "/:baseURL/main/transactions/demand-note"
            },
            {
                name: "Receive Payment",
                url: "/:baseURL/main/transactions/receive-payment"
            },
            {
                name: "Send Receipt",
                url: "/:baseURL/main/transactions/send-receipts"
            },
            // {
            //     name: "Collect Fees",
            //     url: "/main/txn/collect-fees"
            // },
            {
                name: "Scholarships",
                url: "/:baseURL/main/transactions/scholarships"
            },
            {
                name: "Loans",
                url: "/:baseURL/main/transactions/loans"
            },
            {
                name: "Refund",
                url: "/:baseURL/main/transactions/refund"
            }
        ]
    },
    {
        name: "Tasks",
        icon: "icon-tasks",
        title: "Tasks",
        url: '/:baseURL/main/task',
        children: []
    }
    ]
}


