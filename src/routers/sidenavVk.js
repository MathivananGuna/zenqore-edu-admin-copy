import DoneAllIcon from '@material-ui/icons/DoneAll';
export default {
    items: [{
        icon: "zq-icon-dashboard",
        name: "Dashboard",
        title: "View Dashboard",
        url: '/:baseURL/main/dashboard',
        children: []
    },
    {
        icon: "zq-icon-transaction",
        name: "Transactions",
        title: "Manage Transactions",
        children: [
            {
                name: "Demand Note",
                url: "/:baseURL/main/transactions/demand-note"
            },
            {
                name: "Collect Payment",
                url: "/:baseURL/main/transactions/receive-payment"
            },
            {
                name: "Scholarships",
                url: "/:baseURL/main/transactions/scholarships"
            },
            {
                name: "Loans",
                url: "/:baseURL/main/transactions/loans"
            },
            {
                name: "Refund",
                url: "/:baseURL/main/transactions/refund"
            }
        ]
    },
    {
        name: "Reports",
        icon: "zq-icon-reports",
        title: "View your Reports",
        children: [
            {
                name: "Application",
                url: "/:baseURL/main/reports/application"
            },
            {
                name: "Demand Note",
                url: "/:baseURL/main/reports/demand-note"
            },
            {
                name: "Fees Collection",
                url: "/:baseURL/main/reports/fees-collection"
            },
            {
                name: "Student Statement",
                url: "/:baseURL/main/reports/student-statement"
            },
            {
                name: "Program Plan Statement",
                url: "/:baseURL/main/reports/program-plan-statement"
            },
            {
                name: "Defaulter Report",
                url: "/:baseURL/main/reports/defaulter-report"
            },

            {
                name: "Fee pending",
                url: "/:baseURL/main/reports/fee-pending"
            },
            {
                name: "Refund",
                url: "/:baseURL/main/reports/refund"
            },
            {
                name: "Scholarships",
                url: '/:baseURL/main/reports/scholarships'
            },
            {
                name: "Loans",
                url: '/:baseURL/main/reports/loans'
            }
        ]
    },
    {
        name: 'Fee Configuration',
        icon: "zq-config-configuration",
        title: "Fee Configuration",
        children: [
            {
                name: "Fee Types",
                url: '/:baseURL/main/fee-configuration/fee-types'
            },
            {
                name: "Fee Structure",
                url: '/:baseURL/main/fee-configuration/fee-structure'
            },
            {
                name: "Fee Inventory",
                url: "/:baseURL/main/fee-configuration/fees-manager"
            },
            {
                name: "Student Fee Mapping",
                url: "/:baseURL/main/fee-configuration/student-fees-mapping"
            },
            

            ]
        },
        {
            name: 'Setup',
            icon: "zq-icon-settings",
            title: "Manage your Setup",
            children: [
                {
                    name: "Users",
                    url: '/:baseURL/main/setup/masters/user'
                },
                {
                    name: "Masters",
                    children: [
                        {
                            name: "Students",
                            url: '/:baseURL/main/setup/masters/students'
                        },
                        {
                            name: "Program Plan",
                            url: '/:baseURL/main/setup/masters/program-plan'
                        },

                    // {
                    //     name: "Fee Types",
                    //     url: '/:baseURL/main/setup/masters/fee-types'
                    // },
                    // {
                    //     name: "Fee Structure",
                    //     url: '/:baseURL/main/setup/masters/fee-structure'
                    // },
                    // {
                    //     name: "Fee Structure",
                    //     url: '/:baseURL/main/setup/masters/fee-structure'
                    // },
                    // {
                    //     name: "Routes",
                    //     url: '/main/setup/routes'
                    // },
                    // {
                    //     name: "Stops",
                    //     url: '/main/setup/stops'
                    // },
                    // {
                    //     name: "Uniform",
                    //     url: '/main/setup/uniform'
                    // },
                    // {
                    //     name: "Books",
                    //     url: '/main/setup/books'
                    // },
                    {
                        name: "Payment Schedule",
                        url: '/:baseURL/main/setup/masters/payment-schedule'
                    },
                    {
                        name: "Reminders",
                        url: '/:baseURL/main/setup/masters/reminders'
                    },
                    // {
                    //     name: "Discounts",
                    //     url: '/main/setup/discounts'
                    // },
                    {
                        name: "Installments",
                        url: '/:baseURL/main/setup/masters/installment'
                    },
                    {
                        name: "Late Fees",
                        url: '/:baseURL/main/setup/masters/late-fees'
                    },
                    {
                        name: "Categories",
                        url: '/:baseURL/main/setup/masters/categories'
                    },
                    {
                        name: "Concessions",
                        url: '/:baseURL/main/setup/masters/concession'
                    },
                    {
                        name: "Scholarships",
                        url: '/:baseURL/main/setup/master/scholarship'
                    },
                    {
                        name: "Loans",
                        url: '/:baseURL/main/setup/master/loans'
                    }
                ]
            },
            {
                name: "Settings",
                url: '/:baseURL/main/setup/settings'
                // children: [
                //     {
                //         name: "Logo",
                //         url: '/main/setup/Logo'
                //     },
                //     {
                //         name: "Name and address",
                //         url: '/main/setup/name-address'
                //     },
                //     {
                //         name: "Templates",
                //         url: '/main/setup/templates'
                //     },
                //     {
                //         name: "Email Server",
                //         url: '/main/setup/email-server'
                //     },
                //     {
                //         name: "SMS Gateway",
                //         url: '/main/setup/sms-gateway'
                //     },
                //     {
                //         name: "Payment Gateway",
                //         url: '/main/setup/payment-gateway'
                //     },
                //     {
                //         name: "Academic Year",
                //         url: '/main/setup/academic-year'
                //     }
                // ]
            }
        ]
    }]
}


