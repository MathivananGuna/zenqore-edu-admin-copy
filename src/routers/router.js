import React from 'react';

const dashboard = React.lazy(() => import('../components/dashboard'));
const Campuses = React.lazy(() => import('../components/views/setup/masters/campuses/campuses'))
const Student = React.lazy(() => import('../components/views/setup/masters/student/student'));
const programPlan = React.lazy(() => import('../components/views/setup/masters/programPlan/programPlan'));
const feeStructure = React.lazy(() => import('../components/views/setup/masters/feeStructure/feeStructure'));
const paymentSchedule = React.lazy(() => import('../components/views/setup/masters/paymentSchedule/paymentSchedule'));
const reminderPlan = React.lazy(() => import('../components/views/setup/masters/remainderPlan/remainderPlan'));
const FeeTypes = React.lazy(() => import('../components/views/setup/masters/fee-type/fees-type'));
const Installments = React.lazy(() => import('../components/views/setup/masters/installment/installment'));
const StudentFeeReport = React.lazy(() => import('../components/views/reports/student-fee/student-fee-report'));
const DemandNoteReport = React.lazy(() => import('../components/views/reports/demand-note/demandNote'));
const StudentStmtReport = React.lazy(() => import('../components/views/reports/student-stmt/studentStmtReport'));
const DefaulterReport = React.lazy(() => import('../components/views/reports/defaulter-report/defaulterReport'));
const FeePendingReport = React.lazy(() => import('../components/views/reports/fee-pending/feePendingReport'));
const ProgramPlanStmtReport = React.lazy(() => import('../components/views/reports/programPlan-stmt/programPlanStmt'));
const ApplicationReport = React.lazy(() => import('../components/views/reports/application-report/application-report'));
const Refund = React.lazy(() => import('../components/views/reports/refund/refund'));
const Settings = React.lazy(() => import('../components/views/setup/settings/settings'));
const LateFees = React.lazy(() => import('../components/views/setup/masters/late-fees/late-fees'));
const FeesManager = React.lazy(() => import('../components/views/configuration/fees-manager/fees-manager'));
const StudentFeesMapping = React.lazy(() => import('../components/views/configuration/student-fee-mapping/student-fee-mapping'));
const receiptHtmlEditor = React.lazy(() => import('../components/receipt-editor'))
const CollectDeposit = React.lazy(() => import('../components/views/transactions/collect-deposit/collect-deposit'))
const SendDemandNote = React.lazy(() => import('../components/views/transactions/send-demand-note/send-demand-note'));
const ReceivePayment = React.lazy(() => import('../components/views/transactions/receive-payment/receive-payment'));
const Categories = React.lazy(() => import('../components/views/setup/masters/category/category'));
const Concession = React.lazy(() => import('../components/views/setup/masters/concession/concession'));
const RefundTxn = React.lazy(() => import('../components/views/transactions/refund/refund'));
// const ReconcileDataPortal = React.lazy(()=>import('../components/views/reconcile/reconcile'))
const ReconcileDataPortal = React.lazy(() => import('../components/views/reconcile/reconciliation'))
const Loans = React.lazy(() => import('../components/views/transactions/loans/loans'));
const ScholarshipTransaction = React.lazy(() => import('../components/views/transactions/scholarships/scholarship-transaction'));
const MasterLoans = React.lazy(() => import('../components/views/setup/masters/loans/master-loans'));
const MasterScholarship = React.lazy(() => import('../components/views/setup/masters/schollarship-master/master-scholarship'));
const ReportsLoans = React.lazy(() => import('../components/views/reports/loans-report/loans-report'));
const Scholarshipsreport = React.lazy(() => import('../components/views/reports/scholarship/scholarship'));
const TransactionDeposit = React.lazy(() => import('../components/views/transactions/deposit/deposit'));
const ProfilePicture = React.lazy(() => import('../components/views/profile-picture'));
const User = React.lazy(() => import('../components/views/setup/masters/user/user'));
const SendReceipts = React.lazy(() => import('../components/views/transactions/send-receipt/send-receipt'));
const CancelTransaction = React.lazy(() => import('../components/views/transactions/cancel/cancel'));
const FranchisesComp = React.lazy(() => import('../components/views/setup/masters/franchises/franchises'));
const Task = React.lazy(() => import('../components/task/task'))
const CreateTask = React.lazy(() => import('../components/task/createtask/createtask'))
const Onboard = React.lazy(() => import('../components/onboard'));
const ReceivableReport = React.lazy(() => import('../components/views/reports/receivable/receivable'))
const ChequeReport = React.lazy(() => import('../components/views/reports/cheque/cheque'))
const StudentFeeCollectionReport = React.lazy(() => import('../components/views/reports/student-feecollection-Reports/student-feecollection-report'))
const StudentFeePendingReport = React.lazy(() => import('../components/views/reports/student-fee-pending-reports/feePendingReport'));
const SNMA_ApplicatonReport = React.lazy(() => import('../components/views/snma-reports/application-report/application-report'))
const SNMA_DemandNoteReport = React.lazy(() => import('../components/views/snma-reports/demand-note/demandNote'))
const SNMA_FeeCollectionReport = React.lazy(() => import('../components/views/snma-reports/student-fee/student-fee-report'))
const SNMA_FeePendingReport = React.lazy(() => import('../components/views/snma-reports/fee-pending/feePendingReport'))
const SNMA_ReceivablesReport = React.lazy(() => import('../components/views/snma-reports/receivable/receivable'))
const ReportsRoyalty = React.lazy(() => import('../components/views/reports/royalty/royalty'));

const routes = [
    { path: '/:baseURL/main/dashboard', name: 'Dashboard', component: dashboard },
    { path: '/:baseURL/main/receiptEditor', name: 'ReceiptHtmlEditor', component: receiptHtmlEditor },
    { path: '/:baseURL/main/setup/masters/campuses', name: 'Campuses', component: Campuses },
    { path: '/:baseURL/main/setup/masters/students', name: 'Student', component: Student },
    { path: '/:baseURL/main/setup/masters/program-plan', name: 'programPlan', component: programPlan },
    { path: '/:baseURL/main/fee-configuration/fee-structure', name: 'feeStructure', component: feeStructure },
    { path: '/:baseURL/main/setup/masters/payment-schedule', name: 'paymentSchedule', component: paymentSchedule },
    { path: '/:baseURL/main/setup/masters/reminders', name: 'reminderPlan', component: reminderPlan },
    { path: '/:baseURL/main/fee-configuration/fee-types', name: 'Fees-type', component: FeeTypes },
    { path: '/:baseURL/main/setup/masters/installment', name: 'Installment', component: Installments },
    { path: '/:baseURL/main/reports/fees-collection', name: 'Student-Report', component: StudentFeeReport },
    { path: '/:baseURL/main/reports/demand-note', name: 'Student-Report', component: DemandNoteReport },
    { path: '/:baseURL/main/reports/student-statement', name: 'Student-Statement-Report', component: StudentStmtReport },
    { path: '/:baseURL/main/reports/defaulter-report', name: 'Defaulter-Report', component: DefaulterReport },
    { path: '/:baseURL/main/reports/fee-pending', name: 'Fee Pending-Report', component: FeePendingReport },
    { path: '/:baseURL/main/reports/refund', name: 'Refund', component: Refund },
    { path: '/:baseURL/main/reports/application', name: 'Application-Report', component: ApplicationReport },
    { path: '/:baseURL/main/reports/program-plan-statement', name: 'Program Plan Statement-Report', component: ProgramPlanStmtReport },
    { path: '/:baseURL/main/setup/settings', name: 'Settings', component: Settings },
    { path: '/:baseURL/main/setup/masters/late-fees', name: 'late-fees', component: LateFees },
    { path: '/:baseURL/main/fee-configuration/fees-manager', name: 'Fees-manager', component: FeesManager },
    { path: '/:baseURL/main/fee-configuration/student-fees-mapping', name: 'Student-Fees-Mapping', component: StudentFeesMapping },
    { path: '/:baseURL/main/transactions/collect-deposit', name: 'Collect-Deposit', component: CollectDeposit },
    { path: '/:baseURL/main/transactions/demand-note', name: 'Send-Demand-Note', component: SendDemandNote },
    { path: '/:baseURL/main/transactions/receive-payment', name: 'Receive-Payment', component: ReceivePayment },
    { path: '/:baseURL/main/transactions/refund', name: 'Refund Transaction', component: RefundTxn },
    { path: '/:baseURL/main/setup/masters/categories', name: 'Categories', component: Categories },
    { path: '/:baseURL/main/setup/masters/concession', name: 'Concession', component: Concession },
    { path: '/:baseURL/main/reconcile', name: 'Reconcile', component: ReconcileDataPortal },
    { path: '/:baseURL/main/transactions/scholarships', name: 'Scholarships', component: ScholarshipTransaction },
    { path: '/:baseURL/main/transactions/loans', name: 'Loans', component: Loans },
    { path: '/:baseURL/main/setup/master/loans', name: 'Loans', component: MasterLoans },
    { path: '/:baseURL/main/setup/master/scholarship', name: 'Loans', component: MasterScholarship },
    { path: '/:baseURL/main/reports/loans', name: 'Loans', component: ReportsLoans },
    { path: '/:baseURL/main/reports/scholarships', name: 'Scholarship-reports', component: Scholarshipsreport },
    { path: '/:baseURL/main/transactions/deposit', name: 'Transactions-deposit', component: TransactionDeposit },
    { path: '/:baseURL/main/profile-picture', name: 'Profile Picture', component: ProfilePicture },
    { path: '/:baseURL/main/setup/masters/user', name: 'User', component: User },
    { path: '/:baseURL/main/transactions/send-receipts', name: 'Send-Receipts', component: SendReceipts },
    { path: '/:baseURL/main/task', name: 'Task', component: Task },
    { path: '/:baseURL/main/createtask', name: 'CreateTask', component: CreateTask },
    { path: '/:baseURL/main/transactions/cancel', name: 'cancel', component: CancelTransaction },
    { path: '/:baseURL/main/onboard', name: 'Onboard', component: Onboard },
    { path: '/:baseURL/main/reports/cheque', name: 'Cheque-Report', component: ChequeReport },
    { path: '/:baseURL/main/reports/receivable', name: 'Receivable-Report', component: ReceivableReport },
    { path: '/:baseURL/main/reports/student-feecollection', name: 'Student-Feecollection-Report', component: StudentFeeCollectionReport },
    { path: '/:baseURL/main/reports/student-fee-pending-reports', name: 'Fee Pending-Report', component: StudentFeePendingReport },
    { path: '/:baseURL/main/setup/masters/franchises', name: 'franchises', component: FranchisesComp },
    { path: '/:baseURL/main/reports/royalty', name: 'royalty', component: ReportsRoyalty },
    // { path: '/:baseURL/main/reports/student-feecollection', name: 'Student-Feecollection-Report', component: StudentFeeCollectionReport },
    // ------------------------------------SNMA Reports are as Follows---------------------
    { path: '/:baseURL/main/reports/snma/application', name: 'Application Report', component: SNMA_ApplicatonReport },
    { path: '/:baseURL/main/reports/snma/demand-note', name: 'Demand Note Report', component: SNMA_DemandNoteReport },
    { path: '/:baseURL/main/reports/snma/fees-collection', name: 'Fee Collection Report', component: SNMA_FeeCollectionReport },
    { path: '/:baseURL/main/reports/snma/fee-pending', name: 'Fee Pending Report', component: SNMA_FeePendingReport },
    { path: '/:baseURL/main/reports/snma/receivable', name: 'Receivables Report', component: SNMA_ReceivablesReport },
    // ------------------------------------SNMA Reports are as Follows---------------------
    { path: '/:baseURL/main/reports/student-feecollection', name:'Student-Feecollection-Report', component: StudentFeeCollectionReport }

];
export default routes;