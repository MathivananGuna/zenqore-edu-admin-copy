// NOTE:
// roleView --> add which role should view in UI
// baseViewHide --> add which baseURL not view in UI
export default {
    items: [{
        icon: "zq-icon-dashboard",
        name: "Dashboard",
        title: "View Dashboard",
        url: '/:baseURL/main/dashboard',
        children: [],
        roleView: ['admin', 'staff', 'principal', 'others', 'seller', 'headseller', 'finance'],
        baseViewHide: []
    },
    {
        icon: "zq-icon-transaction",
        name: "Transactions",
        title: "Manage Transactions",
        roleView: ['admin', 'staff', 'seller', 'headseller', 'finance'],
        baseViewHide: [],
        children: [
            // {
            //     name: "Deposit",
            //     url: "/:baseURL/main/transactions/deposit"
            // },
            // {
            //     name: "Collect Deposit",
            //     url: "/:baseURL/main/transactions/collect-deposit",
            //     roleView: ['admin', 'staff'],
            //     baseViewHide: []
            // },
            {
                name: "Demand Note",
                label: "transaction_demandNote",
                url: "/:baseURL/main/transactions/demand-note",
                roleView: ['admin', 'staff', 'seller', 'headseller', 'finance'],
                baseViewHide: []
            },
            {
                name: "Collect Payment",
                label: "transaction_collectPayment",
                url: "/:baseURL/main/transactions/receive-payment",
                roleView: ['admin', 'staff'],
                baseViewHide: []
            },
            {
                name: "Collect Fees",
                url: "/:baseURL/main/transactions/receive-payment",
                roleView: ['seller', 'headseller', 'finance'],
                baseViewHide: []
            },
            {
                name: "Send Receipt",
                label: "transaction_sendReceipt",
                url: "/:baseURL/main/transactions/send-receipts",
                roleView: ['admin', 'staff', 'seller', 'headseller', 'finance'],
                baseViewHide: []
            },
            // {
            //     name: "Collect Fees",
            //     url: "/main/txn/collect-fees"
            // },
            {
                name: "Scholarships",
                label: "transactions_scholarships",
                url: "/:baseURL/main/transactions/scholarships",
                roleView: ['admin', 'staff'],
                baseViewHide: ['bldeaps']
            },
            {
                name: "Loans",
                label: "transactions_loans",
                url: "/:baseURL/main/transactions/loans",
                roleView: ['admin', 'staff'],
                baseViewHide: ['bldeaps']
            },
            {
                name: "Refund",
                label: "transactions_refund",
                url: "/:baseURL/main/transactions/refund",
                roleView: ['admin', 'staff', 'seller', 'headseller', 'finance'],
                baseViewHide: ['vkgi', 'vkgroup']
            },
            // {
            //     name: "Cancel",
            //     url: "/:baseURL/main/transactions/cancel",
            //     roleView: ['admin'],
            //     baseViewHide: []
            // },
            {
                name: "Cancel Transaction",
                label: "transactions_cancelTransaction",
                url: "/:baseURL/main/transactions/cancel",
                roleView: ['admin', 'seller', 'headseller', 'finance'],
                baseViewHide: []
            },
        ]
    },
    {
        name: "Tasks",
        icon: "icon-zen-task",
        title: "Tasks",
        label: "tasks",
        url: '/:baseURL/main/task',
        children: [],
        roleView: ['admin', 'staff'],
        baseViewHide: ['vkgi', 'vkgroup', 'bldeaps']
    },
    {
        name: "Reconciliation",
        icon: "zq-payroll-reconreconciliation",
        title: "Reconciliation",
        label: "reconciliation",
        url: '/:baseURL/main/reconcile',
        children: [],
        roleView: ['admin'],
        baseViewHide: ['vkgi', 'vkgroup',]
    },
    {
        icon: "icon-onboard-user",
        name: "Onboard",
        title: "Onboard User",
        url: '/:baseURL/main/onboard',
        roleView: ['seller', 'headseller', 'finance'],
        baseViewHide: [],
        children: []
    },
    {
        name: "Reports",
        icon: "zq-icon-reports",
        title: "View your Reports",
        roleView: ['admin', 'principal', 'staff', 'seller', 'finance', 'headseller'],
        baseViewHide: [],
        children: [
            {
                name: "Application",
                url: "/:baseURL/main/reports/application",
                roleView: ['admin',],
                baseViewHide: ['snma']
            },
            {
                name: "Demand Note",
                url: "/:baseURL/main/reports/demand-note",
                roleView: ['admin',],
                baseViewHide: ['snma']
            },
            {
                name: "Fees Collection",
                url: "/:baseURL/main/reports/fees-collection",
                roleView: ['admin', 'principal', 'staff'],
                baseViewHide: ['snma']
            },
            {
                name: "Fees Collection",
                url: "/:baseURL/main/reports/student-feecollection",
                roleView: ['seller', 'headseller', 'finance'],
                baseViewHide: ['snma']
            },
            {
                name: "Student Statement",
                url: "/:baseURL/main/reports/student-statement",
                roleView: ['admin',],
                baseViewHide: ['snma']
            },
            {
                name: "Program Plan Statement",
                url: "/:baseURL/main/reports/program-plan-statement",
                roleView: ['admin',],
                baseViewHide: ['snma']
            },
            {
                name: "Defaulter Report",
                url: "/:baseURL/main/reports/defaulter-report",
                roleView: ['admin',],
                baseViewHide: ['snma', 'bldeaps']
            },
            {
                name: "Fee pending",
                url: "/:baseURL/main/reports/fee-pending",
                roleView: ['admin'],
                baseViewHide: ['snma']
            },
            {
                name: "Fees Pending",
                url: "/:baseURL/main/reports/student-fee-pending-reports",
                roleView: ['seller', 'headseller', 'finance'],
                baseViewHide: ['mnrgroup', 'hkbk', 'gmit', 'vkgi', 'cmr', 'srm', 'nmit', 'ksa', 'snma', 'zenqore']
            },
            {
                name: "Refund",
                url: "/:baseURL/main/reports/refund",
                roleView: ['admin', 'staff'],
                baseViewHide: ['snma']
            },
            {
                name: "Scholarships",
                url: '/:baseURL/main/reports/scholarships',
                roleView: ['admin',],
                baseViewHide: ['snma', 'bldeaps']
            },
            {
                name: "Loans",
                url: '/:baseURL/main/reports/loans',
                roleView: ['admin',],
                baseViewHide: ['snma', 'bldeaps']
            },
            {
                name: "Cheque/DD",
                url: '/:baseURL/main/reports/cheque',
                roleView: ['admin',],
                baseViewHide: ['snma', 'bldeaps']
            },
            {
                name: "Receivables",
                url: '/:baseURL/main/reports/receivable',
                roleView: ['admin', 'seller', 'headseller', 'finance'],
                baseViewHide: ['snma']
            },
            {
                name: "Royalty",
                url: '/:baseURL/main/reports/royalty',
                roleView: ['admin', 'seller', 'headseller', 'finance'],
                baseViewHide: ['mnrgroup', 'hkbk', 'gmit', 'vkgi', 'cmr', 'srm', 'nmit', 'ksa', 'bkah', 'zenqore', 'bldeaps']
            },

            // --------------------- URL Routing links are different for SNMA only
            {
                name: "Application",
                url: "/:baseURL/main/reports/snma/application",
                roleView: ['admin', 'seller', 'headseller', 'finance'],
                baseViewHide: ['mnrgroup', 'hkbk', 'gmit', 'vkgi', 'cmr', 'srm', 'nmit', 'ksa', 'bkah', 'zenqore', 'bldeaps', 'euks']
            },
            {
                name: "Demand Note",
                url: "/:baseURL/main/reports/snma/demand-note",
                roleView: ['admin', 'seller', 'headseller', 'finance'],
                baseViewHide: ['mnrgroup', 'hkbk', 'gmit', 'vkgi', 'cmr', 'srm', 'nmit', 'ksa', 'bkah', 'zenqore', 'bldeaps', 'euks']
            },
            {
                name: "Fees Collection",
                url: "/:baseURL/main/reports/snma/fees-collection",
                roleView: ['admin', 'principal', 'seller', 'headseller', 'staff', 'finance'],
                baseViewHide: ['mnrgroup', 'hkbk', 'gmit', 'vkgi', 'cmr', 'srm', 'nmit', 'ksa', 'bkah', 'zenqore', 'bldeaps', 'euks']
            },
            {
                name: "Fee pending",
                url: "/:baseURL/main/reports/snma/fee-pending",
                roleView: ['admin', 'seller', 'headseller', 'finance'],
                baseViewHide: ['mnrgroup', 'hkbk', 'gmit', 'vkgi', 'cmr', 'srm', 'nmit', 'ksa', 'bkah', 'zenqore', 'bldeaps', 'euks']
            },
            {
                name: "Receivables",
                url: '/:baseURL/main/reports/snma/receivable',
                roleView: ['admin', 'seller', 'headseller', 'finance'],
                baseViewHide: ['mnrgroup', 'hkbk', 'gmit', 'vkgi', 'cmr', 'srm', 'nmit', 'ksa', 'bkah', 'zenqore', 'bldeaps', 'euks']
            },
            // --------------------- URL Routing links are different for SNMA only

        ]
    },
    {
        name: 'Fee Configuration',
        icon: "zq-config-configuration",
        title: "Fee Configuration",
        roleView: ['admin'],
        baseViewHide: [],
        children: [
            {
                name: "Fee Types",
                label: "fee_configuration_feeTypes",
                url: '/:baseURL/main/fee-configuration/fee-types',
                roleView: ['admin'],
                baseViewHide: []
            },
            {
                name: "Fee Structure",
                label: "fee_configuration_feeStructure",
                url: '/:baseURL/main/fee-configuration/fee-structure',
                roleView: ['admin'],
                baseViewHide: []
            },
            {
                name: "Fee Inventory",
                label: "fee_configuration_feeInventory",
                url: "/:baseURL/main/fee-configuration/fees-manager",
                roleView: ['admin'],
                baseViewHide: []
            },
            {
                name: "Student Fee Mapping",
                label: "fee_configuration_studentFeeMapping",
                url: "/:baseURL/main/fee-configuration/student-fees-mapping",
                roleView: ['admin'],
                baseViewHide: []
            },
        ]
    },
    {
        name: 'Setup',
        icon: "zq-icon-settings",
        title: "Manage your Setup",
        roleView: ['admin'],
        baseViewHide: [],
        children: [
            {
                name: "Users",
                label: "setup_users",
                url: '/:baseURL/main/setup/masters/user',
                roleView: ['admin'],
                baseViewHide: []
            },
            {
                name: "Masters",
                roleView: ['admin'],
                baseViewHide: [],
                children: [ 
                    {
                        name: "Franchises",
                        url: '/:baseURL/main/setup/masters/franchises',
                        roleView: ['admin'],
                        baseViewHide: ['mnrgroup', 'hkbk', 'gmit', 'vkgi', 'cmr', 'srm', 'nmit', 'ksa', 'bkah', 'zenqore']
                    },
                    {
                        name: "Campuses",
                        url: '/:baseURL/main/setup/masters/campuses',
                        roleView: ['admin'],
                        baseViewHide: []
                    },
                    {
                        name: "Students",
                        label: "setup_students",
                        url: '/:baseURL/main/setup/masters/students',
                        roleView: ['admin'],
                        baseViewHide: []
                    },
                    {
                        name: "Program Plan",
                        label: "setup_programPlan",
                        url: '/:baseURL/main/setup/masters/program-plan',
                        roleView: ['admin'],
                        baseViewHide: []
                    },
                    // {
                    //     name: "Fee Types",
                    //     url: '/:baseURL/main/setup/masters/fee-types'
                    // },
                    // {
                    //     name: "Fee Structure",
                    //     url: '/:baseURL/main/setup/masters/fee-structure'
                    // },
                    // {
                    //     name: "Fee Structure",
                    //     url: '/:baseURL/main/setup/masters/fee-structure'
                    // },
                    // {
                    //     name: "Routes",
                    //     url: '/main/setup/routes'
                    // },
                    // {
                    //     name: "Stops",
                    //     url: '/main/setup/stops'
                    // },
                    // {
                    //     name: "Uniform",
                    //     url: '/main/setup/uniform'
                    // },
                    // {
                    //     name: "Books",
                    //     url: '/main/setup/books'
                    // },
                    {
                        name: "Payment Schedule",
                        label: "setup_paymentSchedule",
                        url: '/:baseURL/main/setup/masters/payment-schedule',
                        roleView: ['admin'],
                        baseViewHide: []
                    },
                    {
                        name: "Reminders",
                        label: "setup_reminders",
                        url: '/:baseURL/main/setup/masters/reminders',
                        roleView: ['admin'],
                        baseViewHide: []
                    },
                    // {
                    //     name: "Discounts",
                    //     url: '/main/setup/discounts'
                    // },
                    {
                        name: "Installments",
                        label: "setup_installments",
                        url: '/:baseURL/main/setup/masters/installment',
                        roleView: ['admin'],
                        baseViewHide: []
                    },
                    {
                        name: "Late Fees",
                        label: "setup_lateFees",
                        url: '/:baseURL/main/setup/masters/late-fees',
                        roleView: ['admin'],
                        baseViewHide: []
                    },
                    {
                        name: "Categories",
                        label: "setup_categories",
                        url: '/:baseURL/main/setup/masters/categories',
                        roleView: ['admin'],
                        baseViewHide: []
                    },
                    {
                        name: "Concessions",
                        label: "setup_concessions",
                        url: '/:baseURL/main/setup/masters/concession',
                        roleView: ['admin'],
                        baseViewHide: []
                    },
                    {
                        name: "Scholarships",
                        label: "setup_scholarships",
                        url: '/:baseURL/main/setup/master/scholarship',
                        roleView: ['admin'],
                        baseViewHide: ['bldeaps']
                    },
                    {
                        name: "Loans",
                        label: "setup_loans",
                        url: '/:baseURL/main/setup/master/loans',
                        roleView: ['admin'],
                        baseViewHide: ['bldeaps']
                    }
                ]
            },
            {
                name: "Settings",
                label: "setup_settings",
                url: '/:baseURL/main/setup/settings',
                roleView: ['admin'],
                baseViewHide: []
                // children: [
                //     {
                //         name: "Logo",
                //         url: '/main/setup/Logo'
                //     },
                //     {
                //         name: "Name and address",
                //         url: '/main/setup/name-address'
                //     },
                //     {
                //         name: "Templates",
                //         url: '/main/setup/templates'
                //     },
                //     {
                //         name: "Email Server",
                //         url: '/main/setup/email-server'
                //     },
                //     {
                //         name: "SMS Gateway",
                //         url: '/main/setup/sms-gateway'
                //     },
                //     {
                //         name: "Payment Gateway",
                //         url: '/main/setup/payment-gateway'
                //     },
                //     {
                //         name: "Academic Year",
                //         url: '/main/setup/academic-year'
                //     }
                // ]
            }
        ]
    }]
}
