import React, { Component } from 'react';
import AddIcon from '@material-ui/icons/Add';
import SortIcon from '@material-ui/icons/Sort';
import SearchIcon from '@material-ui/icons/Search';
import SearchSVG from '../assets/icons/table-search-icon.svg';
import SortSVG from '../assets/icons/table-sort-icon.svg';
import DownloadSVG from '../assets/icons/table-download-icon.svg';
import ShareSVG from '../assets/icons/table-share-icon.svg';

import PrintSVG from '../assets/icons/table-print-icon.svg';
import PrintIcon from '@material-ui/icons/Print';
import ShareIcon from '@material-ui/icons/Share';
import { withRouter } from 'react-router-dom';
import GetAppIcon from '@material-ui/icons/GetApp';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import PublishSharpIcon from '@material-ui/icons/PublishSharp';
import { SelectPicker, Button } from 'rsuite';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import moment from 'moment';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { DatePicker } from 'rsuite';

const TodayStart = moment(); const TodayEnd = moment(); const WeekEnd = moment(); const QuarterEnd = moment(); const MonthEnd = moment(); const YearEnd = moment();
const WeekStart = moment().subtract(1, 'week');
const MonthStart = moment().startOf('month');
const QuarterStart = moment().startOf('quarter');
const YearStart = moment().startOf('year');
class ContainerNavbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            env: JSON.parse(localStorage.getItem('env')),
            email: localStorage.getItem('email'),
            channel: localStorage.getItem('channel'),
            authToken: localStorage.getItem('auth_token'),
            tab: 0,
            containerNav: undefined,
            searchInput: false,
            searchInputValue: "",
            inputShow: false,
            // printTimeStamp:''
            query: "",
            callCount: 0,
            selectDateRange: [
                {
                    "label": "Today",
                    "value": "Today"
                },
                {
                    "label": "This Week",
                    "value": "thisWeek"
                },
                {
                    "label": "This Month",
                    "value": "thisMonth"
                },
                {
                    "label": "This Quarter",
                    "value": "thisQuarter"
                },
                {
                    "label": "This Year",
                    "value": "thisYear"
                },
                {
                    "label": "Custom",
                    "value": "custom"
                }
            ],
            selectCampus: [
                {
                    "label":"All Campus",
                    "value":"All Campus"
                },
                {
                    "label": "EUROKIDS INTERNATIONAL",
                    "value": "EUROKIDS INTERNATIONAL"
                }
            ],
            franchiseData: [
                {
                    "label": "All Franchises",
                    "value": "All Franchises"
                },
                {
                    "label": "Demo Institute",
                    "value": "Demo Institute"
                },
                {
                    "label": "Zenqore Education",
                    "value": "Zenqore Education"
                },
                {
                    "label": "Ken42 Education",
                    "value": "Ken42 Education"
                }
            ],
            TodayFrom: TodayStart.toDate(),
            TodayTo: TodayEnd.toDate(),
            WeekFrom: WeekStart.toDate(),
            WeekTo: WeekEnd.toDate(),
            MonthFrom: MonthStart.toDate(),
            MonthTo: MonthEnd.toDate(),
            QuarterFrom: QuarterStart.toDate(),
            QuarterTo: QuarterEnd.toDate(),
            YearFrom: YearStart.toDate(),
            YearTo: YearEnd.toDate(),
            selectedFromDate: null,
            selectedToDate: null,
            openDatepickerModel: false
        }
        this.searchInputRef = React.createRef(this.searchInputRef)
        this.timeOutID = React.createRef(this.timeOutID)
        // this.searchInputRef = React.createRef();
    }
    printScreen = () => {
        this.props.printScreen()

    }
    onDownload = () => {
        console.log('download event')
        this.props.onDownload()
    }
    componentWillMount = () => {
        console.log(this.props.containerNav)
        if (this.props.containerNav != undefined) {
            this.setState({ containerNav: this.props.containerNav })
        }
        // document.removeEventListener('mousedown', this.handleClickOutside);
    }
    componentDidMount() {
        // document.addEventListener('mousedown', this.handleClickOutside);
    }
    onSearchClick = () => {
        console.log('search')
        if (this.state.searchInputValue.length == 0) {
            this.setState({ searchInput: !this.state.searchInput })
        }
    }
    searchHandle = (e) => {
        console.log(e.target.value)
        this.setState({ searchInputValue: e.target.value })
        // this.props.searchValue(e.target.value)
        console.log(this.searchInputRef.current)
        clearTimeout(this.timeOutID.current)
        if (!e.target.value.trim()) return this.props.searchValue("")
        this.timeOutID.current = setTimeout(() => {
            this.setState({ query: this.searchInputRef.current })
            this.props.searchValue(e.target.value).then(() => this.setState({ callCount: this.state.callCount + 1 })
            )
        }, 1000)
    }
    onSelectChange = (value, item, event) => {
        console.log(value, item, event)
        this.props.onSelectChange(value, item, event)
    }
    // handleClickOutside = (event) => {
    //     console.log(event)
    //     if (this.searchInputRef && !this.searchInputRef.current.contains(event.target) && event.target.className != 'material-searchIcon' && !this.state.searchInputValue.length > 0) {
    //         this.setState({ searchInput: false })
    //     }
    // }  
    onSelectDateRange = (e) => {
        // dateDurationVal --> get date duration 
        // getAllDataFun --> cancelled date format
        var { TodayFrom, TodayTo, WeekFrom, WeekTo, MonthFrom, MonthTo, QuarterFrom, QuarterTo, YearFrom, YearTo } = this.state;
        if (String(e) === "today") {
            this.setState({ selectedFromDate: this.tableDateFormat(TodayFrom), selectedToDate: this.tableDateFormat(TodayTo) }, () => {
                this.props.dateDurationVal(new Date(this.state.selectedFromDate), new Date(this.state.selectedToDate))
            })
        } else if (String(e) === "thisWeek") {
            this.setState({ selectedFromDate: this.tableDateFormat(WeekFrom), selectedToDate: this.tableDateFormat(WeekTo) }, () => {
                this.props.dateDurationVal(new Date(this.state.selectedFromDate), new Date(this.state.selectedToDate))
            })
        } else if (String(e) === "thisMonth") {
            this.setState({ selectedFromDate: this.tableDateFormat(MonthFrom), selectedToDate: this.tableDateFormat(MonthTo) }, () => {
                this.props.dateDurationVal(new Date(this.state.selectedFromDate), new Date(this.state.selectedToDate))
            })
        } else if (String(e) === "thisQuarter") {
            this.setState({ selectedFromDate: this.tableDateFormat(QuarterFrom), selectedToDate: this.tableDateFormat(QuarterTo) }, () => {
                this.props.dateDurationVal(new Date(this.state.selectedFromDate), new Date(this.state.selectedToDate))
            })
        } else if (String(e) === "thisYear") {
            this.setState({ selectedFromDate: this.tableDateFormat(YearFrom), selectedToDate: this.tableDateFormat(YearTo) }, () => {
                this.props.dateDurationVal(new Date(this.state.selectedFromDate), new Date(this.state.selectedToDate))
            })
        } else if (String(e) === "custom") {
            this.setState({ openDatepickerModel: true, selectedFromDate: "", selectedToDate: "" })
        } else {
            console.log("none");
            this.props.getAllDataFun()
        }
        setTimeout(() => { console.log(this.state.selectedFromDate, this.state.selectedToDate) }, 500)
    }
    tableDateFormat = (ev) => {
        if (ev === undefined || ev === '') { }
        else {
            let getDate = `${String(ev.getDate()).length == 1 ? `0${ev.getDate()}` : ev.getDate()}`;
            let getMonth = `${String(ev.getMonth() + 1).length == 1 ? `0${ev.getMonth() + 1}` : ev.getMonth() + 1}`;
            let getYear = `${ev.getFullYear()}`
            let today = `${getYear}-${getMonth}-${getDate}`
            return today
        }
    }
    closeDatepickerModal = () => { this.setState({ openDatepickerModel: false }) }
    confirmDatepickerModal = () => {
        if (this.state.selectedFromDate === "") { alert("Please select From date"); }
        else if (this.state.selectedToDate === "") { alert("Please select To date"); }
        else {
            if (this.state.selectedToDate < this.state.selectedFromDate) {
                alert("From date should not be greater than To date")
            }
            else if (this.state.selectedFromDate > this.state.selectedToDate) {
                alert("To date should not be lesser than From date")
            }
            else {
                this.setState({ openDatepickerModel: false });
                this.props.dateDurationVal(this.state.selectedFromDate, this.state.selectedToDate)
            }
        }
    }
    onSelectFromDate = (e) => {
        this.setState({ selectedFromDate: e })
    }
    onSelectToDate = (e) => {
        this.setState({ selectedToDate: e })
    }
    render() {
        return (<React.Fragment >
            {this.state.openDatepickerModel === true ?
                <div className="nav-bar-date-picker">
                    <Dialog open={this.state.openDatepickerModel} aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description" className='logout-menu-wrapNew' style={{ zIndex: '2' }}>
                        <DialogTitle id="alert-dialog-title" className="logout-header-text">Select Date</DialogTitle>
                        <DialogContent>
                            <div style={{ display: "inline" }}>
                                <DatePicker style={{ width: 200, marginRight: '10px' }} onChange={(value, item, event) => this.onSelectFromDate(value, item, event)} />
                                <DatePicker style={{ width: 200 }} onChange={(value, item, event) => this.onSelectToDate(value, item, event)} />
                            </div>
                        </DialogContent>
                        <DialogActions className="logout-header-btns">
                            <Button onClick={this.closeDatepickerModal} className="btns-submit btns-cancel" color="primary" style={{ marginRight: "15px" }}>Cancel</Button>
                            <Button onClick={this.confirmDatepickerModal} className="btns-submit" color="primary" autoFocus>Confirm</Button>
                        </DialogActions>
                    </Dialog>
                </div>
                : null}
            {this.state.containerNav != undefined ? <div className="invoice-page-header container-nav-wrap">
                <div className="header-title">
                    {this.state.containerNav.isBack ? <div className="icon-back-arrow-box"><KeyboardBackspaceIcon className="icon-back-arrow" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} /></div> : null}
                    <p className="header-txt">{this.state.containerNav.name}
                        <span className="invoices-count-text">{this.state.containerNav.isTotalCount ? (this.state.containerNav.total) : null}</span>
                    </p>
                </div>
                <div className="header-tools" >
                    {this.state.containerNav.selectFranchiseOption ? <div className="pay-receive-header-btns" title="Select duration">
                        <SelectPicker className="SelectPicker-branch" placeholder="Select Franchise" data={this.state.franchiseData} style={{ width: '160px', marginRight: "10px" }} defaultValue="All Franchises" />
                    </div> : null}
                    {this.state.containerNav.selectCampusOption ? <div className="pay-receive-header-btns" title="Select duration">
                    <SelectPicker className="SelectPicker-branch" placeholder={this.props.placeholder || "Select"} data={this.props.SelectCampusdata} defaultValue={this.props.onSelectCampusDefaultValue} onClean={this.props.onSelectClean} onChange={(value, item, event) => this.onSelectChange(value, item, event)} style={{ width: '200px', marginRight: "10px" }} />
                    </div> : null}
                    
                    {this.state.containerNav.isSelectAppReport ? <div className="pay-receive-header-btns" title="Select">
                        <SelectPicker className="SelectPicker-branch" placeholder={this.props.placeholder || "Select"} data={this.props.Selectdata} defaultValue={this.props.onSelectDefaultValue} onClean={this.props.onSelectClean} onChange={(value, item, event) => this.onSelectChange(value, item, event)} style={{ width: '200px', marginRight: "10px" }} />
                    </div> : null}
                    {this.state.containerNav.selectSectionOption ? <div className="pay-receive-header-btns" title="Select duration">
                        <SelectPicker className="SelectPicker-branch" placeholder={this.props.placeholder || "Select"} data={this.props.SelectSectionData} defaultValue={this.props.onSelectDefaultValue} onClean={this.props.onSelectClean} onChange={(value, item, event) => this.onSelectChange(value, item, event)} style={{ width: '200px', marginRight: "10px" }} />
                    </div> : null}
                    {this.state.containerNav.selectPickerOption ? <div className="pay-receive-header-btns" title="Select duration">
                        <SelectPicker className="SelectPicker-branch" placeholder="Select duration" data={this.state.selectDateRange} style={{ width: '160px', marginRight: "10px" }} onChange={(value, item, event) => this.onSelectDateRange(value, item, event)} defaultValue="Today" />
                    </div> : null}
                    {this.state.containerNav.isSelectBatchReport ? <div className="pay-receive-header-btns" title="Select">
                        <SelectPicker className="SelectPicker-branch" placeholder={this.props.placeholder || "Select"} data={this.props.SelectBatchData} defaultValue={this.props.onSelectBatchDefaultValue} onClean={this.props.onSelectClean} onChange={(value, item, event) => this.onSelectChange(value, item, event)} style={{ width: '200px', marginRight: "10px" }} />
                    </div> : null}
                    {/* {this.state.containerNav.isSearch ? <div className={(this.state.searchInput && String(this.state.searchInputValue).length == 0) ? "icon-box search-input-wrap" : "icon-box search-icon-wrap"} tooltip-title="Search"> */}
                    {this.state.containerNav.isSearch ? <div className={(this.state.searchInput) ? "icon-box search-input-wrap" : "icon-box search-icon-wrap"} tooltip-title="Search">

                        {/* <SearchIcon className="material-searchIcon" /> */}
                        {/* <img src={SearchSVG} alt="Search" className="material-searchIcon" ></img> */}
                        {(this.state.searchInput && String(this.state.searchInputValue).length == 0) ?
                            <React.Fragment>
                                <Input
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton
                                                onClick={() => this.onSearchClick()}
                                            >
                                                <img src={SearchSVG} alt="Search" className="material-searchIcon" ></img>
                                            </IconButton>
                                        </InputAdornment>
                                    } ref={this.searchInputRef}
                                    value={this.state.searchInputValue}
                                    onChange={(e) => this.searchHandle(e)}
                                />
                            </React.Fragment> : (this.state.searchInput && String(this.state.searchInputValue).length > 0) ?
                                <React.Fragment>
                                    <Input
                                        endAdornment={
                                            <InputAdornment position="end">
                                                <IconButton
                                                    onClick={() => this.onSearchClick()}
                                                >
                                                    <img src={SearchSVG} alt="Search" className="material-searchIcon" ></img>
                                                </IconButton>
                                            </InputAdornment>
                                        } ref={this.searchInputRef}

                                        value={this.state.searchInputValue}
                                        onChange={(e) => this.searchHandle(e)}
                                    />
                                </React.Fragment> : <img src={SearchSVG} alt="Search" className="material-searchIcon" onClick={() => this.onSearchClick()}></img>}
                    </div> : null}
                    {this.state.containerNav.isSort ? <div className="icon-box" tooltip-title="Filter">
                        {/* <SortIcon className="material-SortIcon" /> */}
                        <img src={SortSVG} alt="Sort" className="material-searchIcon"></img>
                    </div> : null}
                    {this.state.containerNav.isPrint ?
                        <div className="icon-box" tooltip-title="Print" onClick={this.printScreen}>
                            <img src={PrintSVG} alt="Print" className="material-searchIcon" ></img>
                        </div> : null}
                    {this.state.containerNav.isDownload ?
                        <div className="icon-box" tooltip-title={this.state.downloadTitle == undefined ? "Download" : this.state.downloadTitle}>
                            {/* <GetAppIcon className="material-SortIcon" /> */}
                            <img src={DownloadSVG} onClick={() => this.props.onDownload()} alt="Download" className="material-searchIcon"></img>
                        </div> : null}
                    {this.state.containerNav.isShare ? <div className="icon-box" tooltip-title="Share" style={{ marginRight: this.state.containerNav.isNew ? '10px' : '0px' }}>
                        {/* <ShareIcon className="material-SortIcon" /> */}
                        <img src={ShareSVG} alt="Share" className="material-searchIcon"></img>
                    </div> : null}

                    {/* {this.state.containerNav.isNew ? <div>
                        <button className="add-item-btn" onClick={() => this.props.onAddNew()} tooltip-title={this.state.containerNav.name == "Support" ? 'Create New Ticket' : "Add New " + this.state.containerNav.name} >{!this.state.containerNav.isSubmit ? <AddIcon className="material-addIcon" /> : null}{this.state.containerNav.newName}</button>
                    </div> : null} */}

                    {this.state.containerNav.isNew ? <div>
                        <button className="add-item-btn" onClick={() => this.props.onAddNew()} >{!this.state.containerNav.isSubmit ? <AddIcon className="material-addIcon" /> : null}{this.state.containerNav.newName}</button>
                    </div> : null}
                    {this.state.containerNav.isSync ? <div>
                        <button className="add-item-btn sync-now-btn" onClick={() => this.props.onAddNew()} tooltip-title={'Sync Now'} >{this.state.containerNav.newName}</button>
                    </div> : null}
                    {this.state.containerNav.isDownloadTemp ? <div>
                        <button className="add-item-btn ctcDownload-btn" onClick={() => this.onDownload()} tooltip-title={"Download"} style={{ width: "162px", marginLeft: "-1px" }}>{!this.state.containerNav.isSubmit ? <GetAppIcon className="material-addIcon" /> : null}{this.state.containerNav.downloadName}</button>
                    </div> : null}
                    {this.state.containerNav.isDownloadTemp ? <div>
                        <button className="add-item-btn ctcUpload-btn" onClick={() => this.props.onUpload()} tooltip-title={"Upload"} style={{ width: "162px" }}>{!this.state.containerNav.isSubmit ? <PublishSharpIcon className="material-addIcon" /> : null}{this.state.containerNav.uploadName}</button>
                    </div> : null}
                    {this.state.containerNav.isPayslipBtn ? <div>
                        <button className="add-item-btn" onClick={() => this.props.onSendPayslip()} tooltip-title={"Send Payslip"} >{this.state.containerNav.PayslipBtnName}</button>
                    </div> : null}
                    {/* <div className="pay-receive-header-btns" style={{ width: '151px' }} title="Date Duration">
                                    <SelectPicker title="Select Duration" className="select-picker-duration" defaultValue={String('all')} placeholder="Select Duration" data={this.state.DateDuration} onChange={this.DurationSelect} />
                                </div> */}
                    {this.state.containerNav.isSelectPeriod ? <div className="pay-receive-header-btns" style={{ width: '151px' }} title="Date Duration">
                        <SelectPicker className="SelectPicker-branch" placeholder="Select Duration" data={this.state.DateDuration} onChange={this.DurationSelect} />
                    </div> : null}
                    {this.state.containerNav.isMasterPayslipName ? <div>
                        <button className="add-item-btn ctcUpload-btn SubmitPayslip" tooltip-title={"Submit Payslip"} onClick={() => this.props.onSubmitPayslip()} >{this.state.containerNav.masterPayslipName}</button>
                    </div> : null}

                </div>
            </div> : null}
        </React.Fragment >);
    }
}

export default withRouter(ContainerNavbar);