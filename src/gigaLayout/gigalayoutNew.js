import React, { Component, Suspense } from 'react';
import { Redirect, Route, Switch, useHistory } from 'react-router-dom';
// import * as router from 'react-router-dom';

import navigation from '../routers/sidenav';
import otherNavigation from '../routers/sidenavOthers'
import sidenavVk from '../routers/sidenavVk';
import routes from '../routers/router';
// import logo from '../assets/images/logo.png';
// import logoIcon from '../assets/images/zq-logo.jpeg';
import logoIcon from '../assets/icons/sidenav-zen-logo.svg';
// import logoTxt from '../assets/images/ZQ-text.png';
import logoTxt from '../assets/icons/zq_logo-text.svg';
import rightBurgerSVG from '../assets/icons/right-aligned-burger.svg';
import leftBurgerSVG from '../assets/icons/left-aligned-burger.svg';
// import FolderOutlinedIcon from '@material-ui/icons/FolderOutlined';
// import dashboardIcon from '../assets/icons/dashboard-icon.svg';
// import fileSVG from '../assets/icons/files-icon.svg';

import GigaHeader from './gigaHeader';
import ReactGA from 'react-ga';
import Aside from './aside';
import '../scss/gigalayout.scss';
import Loader from '../utils/loader/loaders';
import { withStyles } from "@material-ui/core/styles";
import Tooltip from '@material-ui/core/Tooltip';
import DateFormatContext from './context'
import Axios from 'axios';
import Ken42Logo from '../assets/images/ken42.png'
import ZenLogo from '../assets/images/zq_logo.png'
import CampusLogo from '../assets/images/campus_logo.png'
import HKBK from '../assets/images/hkbk_logo.jpg'
import ProfilePicture from '../assets/images/profile-large.png';

const ZqTooltip = withStyles((theme) => ({
    tooltip: {
        // backgroundColor: 'seagreen',
        // color: 'white',        
        backgroundColor: '#FFC501',
        color: 'black',
        boxShadow: theme.shadows[1],
        fontSize: 14,
        paddingTop: 6,
        paddingBottom: 6,
        paddingRight: 10,
        paddingLeft: 10,
        zIndex: 1111111
    },
}))(Tooltip);
class GigaLayout extends Component {

    constructor(props) {
        super(props);
        this.state = {
            menuToggle: false,
            asideShow: false,
            isLoader: false,
            overSideNav: false,
            leftAligned: true,
            addClass: false,
            navigation: navigation,
            client: localStorage.getItem('clientName'),
            steps: [
                {
                    target: '.my-first-step',
                    content: 'This is my awesome feature!',
                },
                {
                    target: '.my-other-step',
                    content: 'This another awesome feature!',
                },
            ],
            currentRoute: '',
            dateFormat: localStorage.getItem('dateFormat') !== 'undefined' ? localStorage.getItem('dateFormat') : 'DD-MM-YYYY',
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem('auth_token'),
            loginEmail: localStorage.getItem('channel'),
            baseURL: localStorage.getItem('baseURL'),
            orgName: '',
            profilefirstName: '',
            profileLastName: '',
            profilePicture: '',
            reportLabel: localStorage.getItem('reportLabel') ? localStorage.getItem('reportLabel') : 'REG ID',
            classLabel: localStorage.getItem('classLabel') ? localStorage.getItem('classLabel') : 'CLASS/BATCH',
            logoPosition: localStorage.getItem('logoPosition') ? localStorage.getItem('logoPosition') : "4",
            usernameDetails: {}
        }
        this.appRef = React.createRef()
        this.unlisten = null;
    }

    componentWillMount() {
        let getRole = localStorage.getItem('role')
        console.log('componentWillMount', getRole)
        this.setState({ navigation: navigation })
        // if (localStorage.getItem('baseURL').toLowerCase() == "vkgroup" || localStorage.getItem('baseURL').toLowerCase() == "vkgi") {
        //     this.setState({
        //         navigation: sidenavVk
        //     })
        // }
        // else {
        //     if (String(getRole) && String(getRole).toLowerCase() !== "admin") {
        //         this.setState({
        //             navigation: otherNavigation
        //         })
        //     }
        //     else {
        //         this.setState({ navigation: navigation })
        //     }
        // }
    }
    componentDidMount() {
        // ReactGA.initialize('UA-166923864-1', {
        //     debug: false
        // });
        // this.unlisten = this.props.history.listen(path => {
        //     ReactGA.set({ page: path.pathname }); // Update the user's current page
        //     ReactGA.pageview(path.pathname); // Record a pageview for the given page
        // })
        ReactGA.initialize('UA-190128203-1', {
            debug: false
        });
        this.unlisten = this.props.history.listen(path => {
            ReactGA.set({ page: path.pathname }); // Update the user's current page
            ReactGA.pageview(path.pathname); // Record a pageview for the given page
        })
        let path = this.props.location.pathname
        this.setState({ currentRoute: path, leftAligned: false, }, () => {
            this.onMenuToggle(true)
        });
        this.getDateFormat();
        this.getUserProfile();
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.history && this.props.history.action && this.props.history.action == "PUSH" && (prevState.currentRoute != this.props.history.location.pathname)) {
            this.setState({ currentRoute: this.props.history.location.pathname })
        }
    }
    componentWillUnmount() {

        if (this.unlisten) this.unlisten();
    }
    goToRepository = () => {
        this.props.history.push(`/${localStorage.getItem('baseURL')}/main/filerepository`);
    }
    goSupport = () => {
        this.props.history.push(`/${localStorage.getItem('baseURL')}/main/support`)
    }
    goTask = () => {
        this.props.history.push(`/${localStorage.getItem('baseURL')}/main/task`);
    }
    getUserProfile = () => {
        Axios.get(`${this.state.env['userProfile']}/zq/user/profile?userName=${this.state.loginEmail}`).then(res => {
            let usernameDetails = res.data.data
            this.setState({ usernameDetails }, () => {
                // console.log(this.state.usernameDetails, "usernameDetails")
            })
        })
    }
    getDateFormat = () => {
        Axios({
            url: `${this.state.env['zqBaseUri']}/edu/getUserProfile`,
            method: 'GET',
            headers: {
                "Authorization": this.state.authToken
            }
        }).then(res => {
            // console.log("getUserProfile", res)
            let api_data = res.data.data
            if (api_data !== {}) {
                let orgId = api_data.orgId;
                console.log('gigalayoutNew', orgId)
                const headers = {
                    "Authorization": this.state.authToken
                }
                let orgName = api_data.instituteName;
                let profilefirstName = api_data.firstName
                let profileLastName = api_data.lastName
                orgName = orgName === null || orgName === "" ? "Vijaykiran Knowledge Park" : orgName
                Axios.get(`${this.state.env['zqBaseUri']}/setup/settings?instituteid=${orgId}`, { headers: headers })
                    .then(resp => {
                        if (resp.data.length !== 0) {
                            let dateFormat = resp.data[0]['instituteDetails']['dateFormat']
                            let profilePicture = resp.data[0].logo.logo !== null ? resp.data[0].logo.logo + '?' + Math.random() : ProfilePicture;
                            let reportLabel, classLabel, logoPosition;
                            if (resp.data[0]["label"]) {
                                if (resp.data[0]["label"][0]) {
                                    reportLabel = resp.data[0]["label"][0]["regId"] ? resp.data[0]["label"][0]["regId"] : "REG ID"
                                    classLabel = resp.data[0]["label"][0]["classBatch"] ? resp.data[0]["label"][0]["classBatch"] : "CLASS/BATCH"
                                }
                                else {
                                    reportLabel = "REG ID"
                                    classLabel = "CLASS/BATCH"
                                }
                            } else {
                                reportLabel = "REG ID"
                                classLabel = "CLASS/BATCH"
                            }
                            if (resp.data[0]['logoPositions']) {
                                if (resp.data[0]['logoPositions']['position']) {
                                    logoPosition = resp.data[0]['logoPositions']['position']
                                } else {
                                    logoPosition = '4'
                                }
                            } else {
                                logoPosition = '4'
                            }
                            if (dateFormat) {
                                if (dateFormat.includes("YYYY")) {
                                    // dateFormat = dateFormat === 'undefined' || dateFormat === undefined || dateFormat === null || dateFormat === "null" || dateFormat === "" ? 'DD-MM-YYYY' : dateFormat
                                    dateFormat = dateFormat
                                } else {
                                    dateFormat = 'DD-MM-YYYY'
                                }
                            } else {
                                dateFormat = 'DD-MM-YYYY'
                            }
                            if (resp.data[0]['favicon']) {
                                if (resp.data[0]['favicon']['favicon']) this.setFavicon(resp.data[0]['favicon']['favicon'])
                                else this.setFavicon("https://uploads-ssl.webflow.com/5ee891a3891ce1f991bcd2d6/5ef71159da6e9d49c1e2748c_favicon-32x32.png")
                            }
                            localStorage.setItem('dateFormat', dateFormat)
                            localStorage.setItem('reportLabel', reportLabel)
                            localStorage.setItem('classLabel', classLabel)
                            localStorage.setItem('logoPosition', logoPosition)
                            this.setState({ dateFormat, orgName, profilefirstName, profileLastName, profilePicture, reportLabel, classLabel, logoPosition }, () => {
                                console.log("gigalayout API dateFormat", this.state.dateFormat, this.state.logoPosition, this.state.profilePicture)
                            })
                        }
                    })
            }

        })
    }
    updateDateFormat = (format) => {
        this.setState({ dateFormat: format }, () => {
            localStorage.setItem('dateFormat', format)
            console.log('date format updated')
        })
    }
    updateOrgName = (name) => {
        if (name !== "" || name !== null) {
            this.setState({ orgName: name }, () => {
                console.log("orgName updated")
            })
        }
    }
    updateProfilefirstName = (name) => {
        if (name !== "" || name !== null) {
            this.setState({ profilefirstName: name }, () => {
                console.log("profile first updated")
            })
        }
    }
    updateProfileLastName = (name) => {
        if (name !== "" || name !== null) {
            this.setState({ profileLastName: name }, () => {
                console.log("profile last updated")
            })
        }
    }
    updateProfilePicture = (picture) => {
        if (picture !== "" || picture !== null) {
            this.setState({ profilePicture: picture }, () => {
                console.log("profilePicture updated")
            })
        }
    }
    updateReportLabel = (label) => {
        if (label !== "" || label !== null) {
            this.setState({ reportLabel: label }, () => {
                localStorage.setItem('reportLabel', label)
                console.log("label updated", this.state.reportLabel)
            })
        } else {
            this.setState({ reportLabel: 'REG ID' })
        }
    }
    updateClassLabel = (label) => {
        if (label !== "" || label !== null) {
            this.setState({ classLabel: label }, () => {
                localStorage.setItem('classLabel', label)
                console.log("class updated", this.state.classLabel)
            })
        } else {
            this.setState({ classLabel: 'CLASS/BATCH' })
        }
    }
    updateLogoPositions = (position) => {
        if (position) {
            this.setState({ logoPosition: position }, () => {
                localStorage.setItem('logoPosition', position)
                console.log("Logo Position updated", this.state.logoPosition)
            })
        } else {
            this.setState({ logoPosition: 'parentLeftTop,campusRightTop,vendorLeftBottom' })
        }
    }
    setFavicon = (favicon) => {
        var link = document.querySelector("link[rel~='icon']");
        if (favicon) {
            if (link) {
                link.href = favicon
            } else {
                link.href = 'https://uploads-ssl.webflow.com/5ee891a3891ce1f991bcd2d6/5ef71159da6e9d49c1e2748c_favicon-32x32.png'
            }
        } else {
            link.href = 'https://uploads-ssl.webflow.com/5ee891a3891ce1f991bcd2d6/5ef71159da6e9d49c1e2748c_favicon-32x32.png'
        }
    }
    loading = () => {
        return <Loader />
    }
    onSideNav = (item, e, child) => {
        console.log("sidenav data", item, e, child);
        if (item.url !== this.state.currentRoute) {
            this.setState({ overSideNav: true, addClass: true })
            // ------------------------
            let top = e.currentTarget.offsetTop
            let offsetwidth = e.currentTarget.offsetWidth

            let getNavList = document.querySelectorAll(`.${child}`)
            for (var i = 0; i < getNavList.length; i++) {
                getNavList[i]['classList'] = String(getNavList[i]['classList']).replace('open', '')
            }
            // eslint-disable-next-line
            e.currentTarget.classList = e.currentTarget.classList + ' ' + 'open'
            let sidePanel = document.getElementsByClassName('sub-navbar')
            // eslint-disable-next-line
            for (var i = 0; i < sidePanel.length; i++) {
                if (String(sidePanel[i].parentNode.className).includes('open')) {
                    if (String(sidePanel[i].parentNode.className).includes('sec-sidenav-li')) {
                        let setTop = sidePanel[i]['parentNode']['offsetTop']
                        if ((window.innerHeight / 2) < sidePanel[i]['offsetHeight']) {
                            sidePanel[i]['style']["top"] = '0px'
                        } else {
                            sidePanel[i]['style']["top"] = '0px'
                        }
                    } else {
                        if (i === 0) {
                            sidePanel[i]['style']["top"] = '0px'
                        } else {
                            sidePanel[i]['style']["top"] = '0px'
                        }
                    }
                }

            }
            let currentUrl = window.location.href.split('#')[1]
            let convertedCurrentUrl = String(window.location.href.split('#')[1]).replace('/:baseURL', `/${localStorage.getItem('baseURL')}`)
            let convertedUrl = String(item.url).replace('/:baseURL', `/${localStorage.getItem('baseURL')}`)
            // -------------------
            if (item.url !== undefined) {
                if (item.url == `/main/dashboard`) {
                    this.setState({ leftAligned: false }, () => {
                        this.onMenuToggle(true)
                    });
                }
                let currentUrl = window.location.href.split('#')[1]
                let convertedCurrentUrl = String(window.location.href.split('#')[1]).replace('/:baseURL', `/${localStorage.getItem('baseURL')}`)
                let convertedUrl = String(item.url).replace('/:baseURL', `/${localStorage.getItem('baseURL')}`)
                // -------------------
                if (item.url !== undefined) {
                    if (item.url == "/main/dashboard") {
                        this.setState({ leftAligned: false }, () => {
                            this.onMenuToggle(true)
                        });
                    }

                    let path = String(item.url)
                    this.setState({ currentRoute: this.removeBaseURL(path) });
                    let redirect = true
                    setTimeout(() => {
                        this.onResetNavbar('sidenav-li')
                        this.onResetNavbar('sec-sidenav-li')
                        this.onResetNavbar('third-sidenav-li')
                        this.setState({ overSideNav: false, addClass: false })
                    });
                    routes.map(route => {
                        if (item.url === route.path) {
                            redirect = false
                        } else {
                            redirect = redirect
                        }
                    })
                    if (redirect) {
                        this.props.history.push(`/${localStorage.getItem('baseURL')}/main/coming-soon`)
                    }
                    else if (item.url === convertedUrl) {
                        this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`)
                        setTimeout(() => { this.props.history.push(convertedUrl) }, 1)
                    } else {
                        this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`)
                        setTimeout(() => { this.props.history.push(convertedUrl) }, 1)
                    }

                }
            }

        }
    }
    onResetNavbar = (nav) => {
        let getNavList = document.querySelectorAll(`.${nav}`)

        for (var i = 0; i < getNavList.length; i++) {
            getNavList[i]['classList'] = String(getNavList[i]['classList']).replace('open', '')
        }
    }
    appendList = (listItem, headTitle, index, navbarClass, p) => {
        return <ul className="sub-navbar">
            <p className="sub-navbar-title">{headTitle}</p>
            {listItem.map((child, childIndex) => {
                // let activeClass = child.url && child.url === this.state.currentRoute ? 'active' : ''
                let activeClass = ''
                if (child.children) {
                    child.children.forEach(val => {
                        if (this.removeBaseURL(val.url) === this.state.currentRoute) activeClass = 'active'
                    });
                }
                else activeClass = child.url && this.removeBaseURL(child.url) === this.state.currentRoute ? 'active' : ''
                return <li
                    style={{
                        display: child.roleView.includes(localStorage.getItem('role').toLowerCase()) === true && child.baseViewHide.includes(localStorage.getItem('baseURL').toLowerCase()) === false ? "block" : "none"
                    }}
                    id={'child' + index + childIndex} key={index + childIndex} className={child.icon === undefined ? `${activeClass} ${navbarClass}-sidenav-li with-icon` : `${activeClass} ${navbarClass}-sidenav-li without-icon`}
                    onClick={(e) => this.onSideNav(child, e, `${navbarClass}-sidenav-li`)}>
                    {/* <div className="sub-side-nav-icon"><i className={child.icon + ' icon'}></i></div> */}
                    <span className={navbarClass + '_span'}>{child.name}</span>
                    {child.children !== undefined ?
                        <React.Fragment>
                            <i className="fa fa-chevron-right side-ind-icon"></i>
                            {this.appendList(child.children, child.name, childIndex, 'third')}
                        </React.Fragment> : null}
                </li>
            })}
        </ul>
    }
    //SideNav Expand Collapse
    onMenuToggle = (toggle) => {
        // this.setState({ menuToggle: toggle })
        if (toggle) {
            this.appRef.current.className = "app menu-collapse"
        } else {
            this.appRef.current.className = "app"
        }
    }
    onHomeClick = () => {
        // this.props.history.push('/main/dashboard')
    }
    onTest = () => {
        // console.log('test')
        this.setState({ asideShow: true })
    }
    onResetNavbarWrap = () => {
        setTimeout(() => {
            this.onResetNavbar('sidenav-li')
            this.onResetNavbar('sec-sidenav-li')
            this.onResetNavbar('third-sidenav-li')
            this.setState({ overSideNav: false, addClass: false })
        });
    }
    removeBaseURL = (text) => {
        return String(text).replace('/:baseURL', `/${localStorage.getItem('baseURL')}`)

    }
    addActiveClass = (element) => {
        if (this.state.currentRoute !== this.removeBaseURL(element.url)) {
            let checkActive = element.children.find(ele => this.removeBaseURL(ele.url) === this.state.currentRoute)
            const doesChildExist = element.children.find(ele => ele.children)
            if (doesChildExist && !checkActive) {
                let isActive = false
                element.children.forEach(val => {
                    // if (val.url === this.state.currentRoute) isActive = true
                    val.children && val.children.forEach(childVal => {
                        if (this.removeBaseURL(childVal.url) === this.state.currentRoute) isActive = true
                    });
                });
                return isActive
            }
            else {
                return checkActive ? true : false
            }
        } else {
            return true
        }
    }
    onCampusImgError = () => {
        let { usernameDetails } = this.state;
        let text = "FC";
        // if (usernameDetails["firstName"]) {
        //   text = usernameDetails["firstName"].charAt(0) + usernameDetails["firstName"].charAt(usernameDetails["firstName"].length - 1).toUpperCase()
        // }
        if (usernameDetails["firstName"] && usernameDetails["lastName"]) {
            text = usernameDetails["firstName"].charAt(0) + usernameDetails["lastName"].charAt(0)
        } else if (usernameDetails["firstName"]) {
            text = usernameDetails["firstName"].charAt(0)
        } else if (usernameDetails["lastName"]) {
            text = usernameDetails["lastName"].charAt(0)
        }

        var canvas = document.createElement("canvas");
        canvas.width = 40;
        canvas.height = 40;
        var ctx = canvas.getContext('2d');
        ctx.font = "20px Arial";
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.fillText(text, ctx.canvas.width >> 1, ctx.canvas.height >> 1);
        let src = canvas.toDataURL();
        usernameDetails["profilePic"] = src
        console.log("Error", src);
        this.setState({ usernameDetails })
    }
    notificationRouter = (e) => {
        this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`)
        setTimeout(() => { this.props.history.push(e); }, 1)
    }
    render() {
        const { steps } = this.state;
        return (<React.Fragment>
            {this.state.isLoader ? <Loader /> : null}
            <div className={`app ${this.state.addClass ? 'add-index' : ''}`}>

                <div className="app" ref={this.appRef}  >

                    <div className={`app-header option${this.state.logoPosition}`}>
                        <div className="sidenav-wrap" style={{ position: 'relative' }}>
                            {/* <ZqTooltip title='Dashboard' placement='bottom' > */}
                            <div className="parent-logo-wrap">
                                {this.state.client !== "ken42" ?
                                    <div className="logo-wrap">
                                        <img src={this.state.menuToggle ? logoIcon : logoIcon} alt="Vendor Logo" onClick={this.onHomeClick} />
                                        <p>{this.state.menuToggle ? null : <img src={logoTxt} className="zq-txt-wrap" alt="Vendor Logo" />}</p>
                                    </div>
                                    :
                                    <div className="logo-wrap">
                                        <div className="kenLogo parent-logo">
                                            <img src={this.state.profilePicture} alt="Logo" onClick={this.onHomeClick} />
                                            {/* <img src={Ken42Logo} alt="Ken Logo" onClick={this.onHomeClick} /> */}
                                        </div>
                                        {this.state.baseURL === 'vkgi' || this.state.baseURL === 'mnrgroup' ? <div className="kenLogo campas-logo">
                                            <img src={CampusLogo} alt="Logo" />
                                        </div> : null}
                                        <div className="kenLogo vendor-logo">
                                            {/* <img src={ZenLogo} alt="Ken Logo" onClick={this.onHomeClick} /> */}
                                            <img src={Ken42Logo} alt="Logo" onClick={this.onHomeClick} />
                                        </div>
                                        {/* <p>{this.state.menuToggle ? null : <span className="ken42logotext">Ken42</span>}</p> */}
                                    </div>
                                }
                            </div>
                            {/* </ZqTooltip> */}
                            <div className="burger-menu-div">
                                {this.state.leftAligned ?
                                    <img src={leftBurgerSVG} onClick={() => {
                                        this.setState({ leftAligned: false, addClass: false, }, () => this.onMenuToggle(true));
                                    }}></img> :
                                    <img src={rightBurgerSVG} onClick={() => {
                                        this.setState({ leftAligned: true, addClass: false }, () => this.onMenuToggle(false));
                                    }} ></img>
                                }
                            </div>
                            <div className="sidenav-over-wrap" onClick={this.onResetNavbarWrap} style={{ display: this.state.overSideNav ? "block" : "none" }}></div>
                            {/* SIDENAVBAR CODE */}
                            <div className="list-ui-sidenav" >
                                <ul style={{
                                    paddingTop: '0px',
                                }}>
                                    {this.state.navigation.items.map((item, index) => {
                                        return (
                                            <li id={'parent' + index} key={index}
                                                onClick={(e) => this.onSideNav(item, e, `sidenav-li`)}
                                                style={{
                                                    display: item.roleView.includes(localStorage.getItem('role').toLowerCase()) === true && item.baseViewHide.includes(localStorage.getItem('baseURL').toLowerCase()) === false ? "block" : "none"
                                                }}
                                                className={this.addActiveClass(item) ? (item.icon === undefined ? "sidenav-li with-icon active" : "sidenav-li  without-icon active") : item.icon === undefined ? "sidenav-li with-icon" : "sidenav-li  without-icon "}>
                                                <ZqTooltip title={item.title} placement='right' className="zqtooltip-position">
                                                    <div className="side-nav-icon"><i className={item.icon + ' icon'}></i>
                                                        <span className="parent-sidenav-name">{item.name}</span>
                                                    </div>
                                                    {/* <i className="fa fa-chevron-right side-ind-icon"></i> */}
                                                </ZqTooltip>
                                                {item.children !== undefined ?
                                                    <React.Fragment>
                                                        <i className="fa fa-chevron-right side-ind-icon"></i>
                                                        {this.appendList(item.children, item.name, index, "sec", this.addActiveClass(item) ? 'active' : '')}
                                                    </React.Fragment> : null}
                                            </li>
                                        )
                                    })}
                                </ul>
                            </div>
                            <div className={`vendor-logo-wrap leftBottom${this.state.logoPosition}`}>
                                {this.state.client !== "ken42" ?
                                    <div className="logo-wrap">
                                        <img src={this.state.menuToggle ? logoIcon : logoIcon} alt="Zenqore Logo" onClick={this.onHomeClick} />
                                        <p>{this.state.menuToggle ? null : <img src={logoTxt} className="zq-txt-wrap" alt="Zenqore Logo" />}</p>
                                    </div>
                                    :
                                    <div className="logo-wrap">
                                        <div className="kenLogo parent-logo">
                                            {/* <img src={Ken42Logo} alt="Ken Logo" onClick={this.onHomeClick} /> */}
                                            <img src={this.props.profilePicture} alt="zen Logo" onClick={this.onHomeClick} />
                                        </div>
                                        {this.state.baseURL === "vkgi" || this.state.baseURL === "mnrgroup" ? <div className="kenLogo campas-logo">
                                            <img src={CampusLogo} alt="Logo" />
                                        </div> : null}
                                        <div className="kenLogo vendor-logo">
                                            {/* <img src={ZenLogo} alt="zen Logo" onClick={this.onHomeClick} /> */}
                                            <img src={Ken42Logo} alt="Ken Logo" onClick={this.onHomeClick} />
                                        </div>
                                        {/* <p>{this.state.menuToggle ? null : <span className="ken42logotext">Zenqore</span>}</p> */}
                                    </div>
                                }
                            </div>
                            {/* <GigaHeader /> */}
                            {/* <div className="footer-sidenav">
                                    <ZqTooltip title='Files Repository' placement='right' className="zqtooltip-position" >
                                        <div className="files-div" >
                                            <FolderOutlinedIcon onClick={this.goToRepository} style={{ fontSize: '20px' }} />
                                        </div>
                                    </ZqTooltip>
                                    <ZqTooltip title='Tasks' placement='right' className="zqtooltip-position">
                                        <div className="clipboards-div">
                                            <i className="zq-icon-clipboard" onClick={this.goTask} ></i>
                                        </div>
                                    </ZqTooltip>
                                    <ZqTooltip title='Support' placement='right' className="zqtooltip-position">
                                        <div className="supportings-icon-div">
                                            <i className="zq-icon-supports" onClick={this.goSupport} ></i>
                                        </div>
                                    </ZqTooltip>
                                </div> */}
                        </div>


                        {/* <button onClick={this.onTest}>TEST</button> */}
                    </div>
                    {this.state.asideShow ? <Aside asideShow={this.state.asideShow} asideType="history" /> : null}

                    <DateFormatContext.Provider value={{
                        dateFormat: localStorage.getItem("dateFormat"), updateDateFormat: this.updateDateFormat, updateOrgName: this.updateOrgName,
                        updateProfileLastName: this.updateProfileLastName, updateProfilefirstName: this.updateProfilefirstName,
                        updateProfilePicture: this.updateProfilePicture, updateReportLabel: this.updateReportLabel, reportLabel: localStorage.getItem("reportLabel"),
                        classLabel: localStorage.getItem("classLabel"), updateClassLabel: this.updateClassLabel,
                        logoPosition: this.state.logoPosition, updateLogoPositions: this.updateLogoPositions,
                    }}>
                        <div className="app-container" >
                            <GigaHeader
                                orgName={this.state.orgName}
                                profilefirstName={this.state.profilefirstName}
                                profileLastName={this.state.profileLastName}
                                profilePicture={this.state.profilePicture}
                                notificationRouter={(e) => { this.notificationRouter(e) }}
                            />
                            <Suspense fallback={this.loading()}>
                                <Switch>
                                    {routes.map((route, idx) => {
                                        return route.component ? (
                                            <Route
                                                key={idx}
                                                path={route.path}
                                                exact={route.exact}
                                                name={route.name}
                                                render={props => (
                                                    <route.component {...props} />
                                                )} />
                                        ) : (null);
                                    })}
                                    <Redirect from="/" to="/main/coming-soon" />
                                </Switch>
                            </Suspense>
                        </div>
                    </DateFormatContext.Provider>
                </div>
            </div>
        </React.Fragment >);
    }

}

export default GigaLayout;