import React from 'react';

// DateFormatContext context
const DateFormatContext = React.createContext({
    dateFormat: 'DD-MM-YYYY',
    updateDateFormat: ''
});

// export {
//     DateFormatContext
// }

export default DateFormatContext;