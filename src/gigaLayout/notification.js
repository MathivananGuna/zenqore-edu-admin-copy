import React from 'react';
import '../scss/notification.scss';
import Badge from '@material-ui/core/Badge';
import NotificationsIcon from '@material-ui/icons/Notifications';
import axios from 'axios';
import PubNub from 'pubnub';
import { Drawer, Button } from 'rsuite';
import 'rsuite/dist/styles/rsuite-default.css';
// import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import InfoIcon from '@material-ui/icons/Info';
// import ErrorIcon from '@material-ui/icons/Error';
import CancelIcon from '@material-ui/icons/Cancel';
import WarningIcon from '@material-ui/icons/Warning';
import sideNavData from '../routers/sidenav';

class Notification extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            pubnubKeys: {
                subscribeKey: "sub-c-982dbaba-1d98-11ea-8c76-2e065dbe5941",
                publishKey: "pub-c-87ae3cc8-8d0a-40e0-8e0f-dbb286306b21",
            },
            totalNotifyArr: [],
            notificationCount: 0,
            openNotification: false,
            urlIdView: ""
        }
    }
    componentDidMount() {
        setTimeout(() => {
            this.getNotification()
            this.initializePubnub()
        }, 2000)
    }
    getNotification = () => {
        axios.get(`${this.state.env['zqBaseUri']}/edu/notifications?orgId=${this.state.orgId}`, {
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                let sortArray = res.data.data.result.sort(function (a, b) {
                    return new Date(b.createdAt) - new Date(a.createdAt);
                });
                let dateArr = [];
                let totalArr = [];
                sortArray.forEach((data) => {
                    if (dateArr.includes(new Date(data.createdAt).toLocaleDateString()) === true) {
                        let findIndex = dateArr.indexOf(new Date(data.createdAt).toLocaleDateString())
                        totalArr[findIndex].notificationData.push(data)
                    }
                    else {
                        dateArr.push(new Date(data.createdAt).toLocaleDateString())
                        let createObj = {
                            date: data.createdAt,
                            notificationData: []
                        }
                        createObj.notificationData.push(data);
                        totalArr.push(createObj)
                    }
                })
                console.log(totalArr);
                this.setState({ totalNotifyArr: totalArr, notificationCount: res.data.data.viewStatus })
            })
            .catch(err => { console.log(err) })
    }
    initializePubnub = () => {
        const pubnub = new PubNub(this.state.pubnubKeys);
        console.log(pubnub)
        pubnub.subscribe({ channels: [this.state.orgId] });
        pubnub.addListener({
            message: (event) => {
                this.getNotification()
                console.log("eve", event.message.description)
                this.setState({ notificationCount: this.state.notificationCount + 1 })
            },
        });
    }
    clearAllRead = () => {
        let dummyChangeArr = this.state.totalNotifyArr;
        dummyChangeArr.forEach((data) => {
            data.notificationData.forEach((dataOne) => {
                dataOne['readStatus'] = true
            })
        })
        this.setState({ totalNotifyArr: dummyChangeArr })
    }
    clickNotification = (e) => {
        this.setState({ urlIdView: "" })
        if (e.toLowerCase() === 'readall') {
            this.clearAllRead()
        }
        let typeData = e;
        axios({
            method: 'put',
            url: `${this.state.env['zqBaseUri']}/edu/notifications?orgId=${this.state.orgId}&type=${typeData}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => { return e.toLowerCase() === 'readall' ? this.getNotification() : null; })
            .catch(err => { })
        this.setState({
            openNotification: true,
            notificationCount: 0,
            totalNotifyArr: e.toLowerCase() === "clearall" ? [] : this.state.totalNotifyArr
        })
    }
    timeStampAmPm = (date) => {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12;
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }
    findDateDifference = (date) => {
        const date1 = new Date(date);
        const date2 = new Date();
        const diffTime = Math.abs(date2 - date1);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
        console.log(diffTime, diffDays)
        if (diffDays === 1) { return "Today" }
        else if (diffDays === 2) { return "Yesterday" }
        else { return new Date(date).toLocaleString('en-US', { year: 'numeric', month: 'long', day: 'numeric' }) }
    }
    closeNotification = () => { this.setState({ openNotification: false, notificationCount: 0 }) }
    getRoutePage = (e) => {
        let dummyForLoop = this.state.totalNotifyArr
        dummyForLoop.forEach((data) => {
            data.notificationData.forEach((dataOne) => {
                if (dataOne._id === e.currentTarget.id) {
                    dataOne['readStatus'] = true
                }
            })
        })
        if (this.state.urlIdView == `url${e.currentTarget.id}`) {
            this.setState({ urlIdView: "", totalNotifyArr: dummyForLoop })
        }
        else {
            this.setState({ urlIdView: `url${e.currentTarget.id}`, totalNotifyArr: dummyForLoop })
        }
        axios({
            method: 'put',
            url: `${this.state.env['zqBaseUri']}/edu/notifications?orgId=${this.state.orgId}&type=readone&notifyId=${e.currentTarget.id}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => { this.getNotification() })
            .catch(err => { })
    }
    changePathRoute = (e) => {
        let res = [];
        let flatData = flatArray(sideNavData.items)
        function flatArray(array) {
            array.forEach(function (obj) {
                res.push(obj);
                if (obj.children && obj.children.length) {
                    flatArray(obj.children)
                }
            })
            return res;
        }
        console.log(res);
        let finalData = null
        res.forEach((data) => {
            if (data.label !== undefined) {
                if (data.label == e.currentTarget.id) {
                    finalData = data;
                }
            }
            else { }
        })
        let convertedUrl = String(finalData.url).replace('/:baseURL', `/${localStorage.getItem('baseURL')}`);
        console.log(convertedUrl);
        this.props.notificationRouter(convertedUrl)
        this.setState({ openNotification: false })
    }
    convertToCaps = (word) => {
        return word[0].toUpperCase() + word.slice(1).toLowerCase();
    }
    render() {
        return (
            <div className="notification-section">
                <div className="icon-section">
                    <Badge badgeContent={this.state.notificationCount > 100 ? "100+" : this.state.notificationCount} color="primary" className="badge-notification" onClick={() => { this.clickNotification("viewall") }} >
                        <NotificationsIcon className="notification-icon" />
                    </Badge>
                </div>
                <div>
                    <Drawer backdrop={true} show={this.state.openNotification} onHide={this.closeNotification}>
                        <Drawer.Header>
                            <Drawer.Title>Notifications</Drawer.Title>
                        </Drawer.Header>
                        {this.state.totalNotifyArr.length === 0 ? <Drawer.Body> <div className="empty-response-text"><p>No new notifications</p></div> </Drawer.Body>
                            :
                            <Drawer.Body>
                                {this.state.totalNotifyArr.map((dataOne, y) => {
                                    return (
                                        <>
                                            <div className="notification-date" key={`Tkey${y}`}>
                                                {/* <p className="card">{new Date(dataOne.date).toLocaleString('en-US', { year: 'numeric', month: 'long', day: 'numeric' })}</p> */}
                                                <p className="card">{this.findDateDifference(new Date(dataOne.date))}</p>
                                            </div>
                                            {dataOne.notificationData.map((data, i) => {
                                                return (
                                                    <div className="notification-card" id={data['_id']} key={`Mkey${i}`} onClick={this.getRoutePage} >
                                                        <div className="notification-message-section">
                                                            <div className="left-sec" id={`left${i}`}>
                                                                {data.status.toLowerCase() === "success" ?
                                                                    <InfoIcon className="success-notification-icon" /> :
                                                                    data.status.toLowerCase() === "error" ?
                                                                        <CancelIcon className="error-notification-icon" /> :
                                                                        data.status.toLowerCase() === "warning" ?
                                                                            <WarningIcon className="warning-notification-icon" />
                                                                            : null}

                                                            </div>
                                                            <div className="middle-sec" id={`middle${i}`} key={`Lkey${i}`}>
                                                                <p className={data.readStatus === false ? 'title-active' : 'title'}>{this.convertToCaps(data.action.label)}</p>
                                                                <p className={data.readStatus === false ? "message-active" : "message"}>{data.message}</p>
                                                            </div>
                                                            <div className="right-sec" id={`right${i}`} key={`Bkey${i}`}>
                                                                <p className="display-time">{this.timeStampAmPm(new Date(data.createdAt))}</p>
                                                            </div>
                                                        </div>
                                                        <div className={this.state.urlIdView == `url${data['_id']}` ? "url-section-view" : "url-section-block"} id={`url${data['_id']}`}>
                                                            <Button appearance="ghost" title={`go to ${data.action.label}`} id={data.action.title} onClick={this.changePathRoute}>View</Button>
                                                        </div>
                                                    </div>
                                                )
                                            })}
                                        </>
                                    )
                                })}
                            </Drawer.Body>
                        }
                        <Drawer.Footer>
                            {this.state.totalNotifyArr.length === 0 ? null :
                                <>
                                    {/* <Button appearance="ghost">See all activities</Button> */}
                                    <Button appearance="ghost" title="clear all notifications" onClick={() => { this.clickNotification('clearall') }}>Clear all</Button>
                                    <Button appearance="ghost" title="read all notifications" onClick={() => { this.clickNotification('readall') }}>Read all</Button>
                                </>
                            }
                        </Drawer.Footer>
                    </Drawer>
                </div>
            </div>
        )
    }
}
export default Notification;