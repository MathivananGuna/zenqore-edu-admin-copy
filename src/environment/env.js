
let nameSpace = String(window.location.href).split('#')
localStorage.setItem('baseURL', String(nameSpace['1']).split('/')[1])
let BASEURL = localStorage.getItem('baseURL')

const prod = { // for production environment api's
    type: 'prod',
    zqBaseUri: `https://apifeecollection.zenqore.com/${BASEURL}`,
  
    gstinApi: 'https://apiuat.zenqore.com',
    studentUpload: 'https://uat.student.zenqore.com',
    userProfile:'https://apifeecollection.zenqore.com',
    ken42: 'https://apifeecollection.zenqore.com' // Profile pic
}
const uat = { // for user acceptance testing environment api's
    type: 'uat',
    zqBaseUri: `https://uatapifeecollection.zenqore.com/${BASEURL}`,
    headSeller: `https://uatapifeecollection.zenqore.com`,
    automationUri: 'https://apidev.zenqore.com/test',
    gstinApi: 'https://apiuat.zenqore.com',
    studentUpload: 'https://uat.student.zenqore.com',
    userProfile:'https://uatapifeecollection.zenqore.com',
    ken42: 'https://uatapifeecollection.zenqore.com' // Profile pic
}
const dev = { // for developer environment api's
    type: 'dev',
    zqBaseUri: `https://devapifeecollection.zenqore.com/${BASEURL}`,
    gstinApi: 'https://apiuat.zenqore.com',
    studentUpload: 'https://dev.student.zenqore.com',
    clientDetails: 'http://52.172.50.97:3000',
    userProfile:'https://devapifeecollection.zenqore.com',
    ken42: 'https://devapifeecollection.zenqore.com' // Profile pic

}
export default { dev, prod, uat }
