import React, { Component } from 'react';
import { HashRouter, Route, Switch, Redirect, withRouter } from 'react-router-dom';
import './App.scss';
import Axios from 'axios';
import 'rsuite/dist/styles/rsuite-default.css';
import Loader from './utils/loader/loaders';

var env = require('./environment/env').default
const loading = () => { return <Loader /> }
const GigaLayout = React.lazy(() => import('./gigaLayout/gigalayoutNew')); // containers

// Pages
const Login = React.lazy(() => import('./components/login'));
const OTP = React.lazy(() => import('./components/otp'));
const ForgotPassword = React.lazy(() => import('./components/forgotPwd/forgotPassword'));
const Register = React.lazy(() => import('./components/register/register'));
const SignUp = React.lazy(() => import('./components/signup/signup'));
const Signin = React.lazy(() => import('./components/signin/signin'));
const ZenOTP = React.lazy(() => import('./components/zen-otp/zen-otp'));
const Privacy = React.lazy(() => import('./components/legal_documents/privacy_policy/privacy'));
const Terms = React.lazy(() => import('./components/legal_documents/terms_of_service/terms'));
const StudentDataPortal = React.lazy(() => import('./components/studentDataPortal/student-portal-main'));
const FeeCollectionPortal = React.lazy(() => import('./components/feeCollectionPortal/fee-collection-payment'));
const ScholarshipPortal = React.lazy(() => import('./components/scholarshipProcess/scholarship-process'));
const ChallanPayment = React.lazy(() => import('./components/challan-payment/challan-payment'));
// const PageNotFound = React.lazy(() => import('./components/page404/page404'));

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={(props) => localStorage.getItem("auth_token") ? (<Component {...props} />) : (<Redirect to={{ pathname: "/", }} />)}
    />
);
class Main extends Component {
    constructor(props) {
        super(props)
        this.state = {
            params: { r: false },
            env: {}
        }
    }

    componentWillMount() {
        var nameSpace = String(window.location.href).split('#')
        console.log('nameSpace', nameSpace['1'])
        localStorage.setItem("baseURL", String(nameSpace['1']).split('/')[1])
        if (window.location.origin.includes('uat')) { // user acceptance testing environment
            this.setState({ env: env['uat'] });
            localStorage.setItem('env', JSON.stringify(env['uat']));
            localStorage.setItem('automation', "true");
        }
        else if (window.location.origin.includes('dev')) { // developer environment
            this.setState({ env: env['dev'] });
            localStorage.setItem('env', JSON.stringify(env['dev']));
        }
        else if (window.location.origin.includes('https://feecollection.zenqore.com')) { // production environment
            this.setState({ env: env['prod'] });
            localStorage.setItem('env', JSON.stringify(env['prod']));
        }
        else { // if not matched above all the cases switch to developer environment (default)
            this.setState({ env: env['dev'] });
            localStorage.setItem('env', JSON.stringify(env['dev']));
            localStorage.setItem('automation', "false");
        }
        // this.setFavicon();
    }
    setFavicon = () => {
        var link = document.querySelector("link[rel~='icon']");
        let namespace = localStorage.getItem("baseURL") ? localStorage.getItem("baseURL") : "default"
        if (localStorage.getItem(`${namespace}-link`)) {
            if (link) {
                link.href = localStorage.getItem(`${namespace}-link`)
            } else {
                link.href = 'https://uploads-ssl.webflow.com/5ee891a3891ce1f991bcd2d6/5ef71159da6e9d49c1e2748c_favicon-32x32.png'
            }
        } else {
            link.href = 'https://uploads-ssl.webflow.com/5ee891a3891ce1f991bcd2d6/5ef71159da6e9d49c1e2748c_favicon-32x32.png'
        }
    }
    shouldComponentUpdate() {
        return true
    }
    componentDidMount() {
    }
    render() {
        return (
            <HashRouter>
                <React.Suspense fallback={loading()}>
                    <Switch>
                        <Route exact path="/:baseURL" name="Login Page" render={props => <Login {...props} />} />
                        <Route exact path="/:baseURL/OTP" name="OTP Page" render={props => <OTP {...props} />} />
                        <Route exact path="/:baseURL/zqforgotpassword" name="ForgotPassword Page" render={props => <ForgotPassword {...props} />} />
                        <Route exact path="/:baseURL/register" name="Register Page" render={props => <Register {...props} />} />
                        <Route exact path="/:baseURL/zqsignup" name="signup page" render={props => <SignUp {...props} />} />
                        <Route exact path="/:baseURL/zqsignin" name="signin Page" render={props => <Signin {...props} />} />
                        <Route exact path="/:baseURL/zqotp" name="ZenOTP page" render={props => <ZenOTP {...props} />} />
                        <Route exact path="/:baseURL/privacy" name="Privacy" render={props => <Privacy {...props} />} />
                        <Route exact path="/:baseURL/terms" name="Terms" render={props => <Terms {...props} />} />
                        <Route exact path="/:baseURL/Scholarship-process" name="Scholarship Process" render={props => <ScholarshipPortal {...props} />} />
                        <Route exact path="/:baseURL/student-data-portal" name="Student Data Portal" render={props => <StudentDataPortal {...props} />} />
                        <Route exact path="/:baseURL/feeCollection" name="Fee Collection" render={props => <FeeCollectionPortal {...props} />} />
                        <Route exact path="/:baseURL/challan" name="challan-payment" render={props => <ChallanPayment {...props} />} />

                        <PrivateRoute path="/:baseURL/main" name="Home" params={this.state.params} component={GigaLayout} />
                    </Switch>
                </React.Suspense>
            </HashRouter>
        );
    }
}

export default Main;