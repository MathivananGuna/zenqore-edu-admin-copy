import React, { Component } from "react";
import ZqTable from "../../utils/Table/table-component";
import TaskJson from "./task.json";
import "../../scss/task.scss";
import PaginationUI from "../../utils/pagination/pagination";
import "../../scss/common.scss";
import Axios from "axios";
import Loader from "../../utils/loader/loaders";
import { withRouter } from "react-router-dom";
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import ContainerNavbar from "../../gigaLayout/container-navbar";
import PubNub from "pubnub-react";
import CreateTask from  "./createtask/createtask";

import moment from 'moment';
import DateFormatContext from '../../gigaLayout/context';

class Task extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: TaskJson,
      authToken: localStorage.getItem('auth_token'),
      orgId: localStorage.getItem('orgId'),
      env: JSON.parse(localStorage.getItem("env")),
      userID: localStorage.getItem("userID"),
      datas: [],
      noProg: "Fetching Details...",
      totalPages: 1,
      page: 1,
      limit: 10,
      totalRecord: 0,
      containerNav: {
        isBack: false,
        name: "Lists of Tasks",
        isName: true,
        total: 0,
        isTotalCount: false,
        isSearch: false,
        isSort: false,
        isPrint: true,
        isDownload: false,
        isShare: false,
        isNew: false,
        newName: "New",
        isSubmit: false,
      },
      preview:false,
      items:[],
      userData:[]
    };
    this.pubnub = new PubNub({
      subscribeKey: "sub-c-982dbaba-1d98-11ea-8c76-2e065dbe5941",
      publishKey: "pub-c-87ae3cc8-8d0a-40e0-8e0f-dbb286306b21",
    });
    this.pubnub.init(this);
  }

  static contextType = DateFormatContext;
  componentDidMount() {
    // this.getTaskLists();
    this.userData()
  }
  userData =() =>{
    let payloadInsName = localStorage.getItem('baseURL').toUpperCase()
    Axios.get(`${this.state.env["ken42"]}/zq/user/listuser?institute=${payloadInsName}`, {
      headers: {
        'Authorization': this.state.authToken
      }
    }).then(res => {
      let sampleUserData= []
      res.data.data.map((dataOne, i) => {
        if (res.data.data.length == 0) {
          this.setState({ tableResTxt: "No Data" })
        }
        else {
          sampleUserData.push({
            "value": dataOne['_id'],
            "label": `${dataOne.firstName} ${dataOne.lastName}`
          })
         
        }
      })
      this.setState({userData:sampleUserData} , ()=>{
        this.getTaskLists();
      })
    })
    .catch(err => { console.log(err) })
    
  }
  getTaskLists = () => {
    this.setState({datas: [] ,loader: true}, () =>{
    Axios.get(`${this.state.env["zqBaseUri"]}/edu/tasks?orgId=${this.state.orgId}&page=${this.state.page}&limit=${this.state.limit}`,
     { headers: { "Authorization": this.state.authToken } })
      .then((res) => {
        console.log(res, "success getall task");
        if (res.data.data.length === 0) {
          this.setState({ noProg: "No Data",loader: false });
        }
        else {
          let response = res.data.data
          let taskData = this.state.datas
          response.map(item => {
            let assignedTo = ''
           this.state.userData.filter(key =>{
             if( key.value == item.assignedTo ){
               return  assignedTo=key.label
             }
            })
            taskData.push({
              "Task ID": item.taskId,
              "Task Title": item.title,
              "Task Description": item.description ?item.description :'-',
              "Assigned To": assignedTo,
              "Type": item.type == 'refundAdjust' ? "Refund" : item.type == 'adjustRefund' ? "Adjust" : item.type.charAt(0).toUpperCase() + item.type.slice(1),
              "Created On": item.createdAt !== "" ? this.context.dateFormat !== "" ? moment(item.createdAt).format(this.context.dateFormat) : moment(item.createdAt).format('DD/MM/YYYY') : "-",
              "Due Date": item.dueDate !== "" ? this.context.dateFormat !== "" ? moment(item.dueDate).format(this.context.dateFormat) : moment(item.dueDate).format('DD/MM/YYYY') : "-",
              "Status": item.status,
              "Item": JSON.stringify(item)
            })
          })
          this.setState({
            datas: taskData,
            loader: false,
            totalPages: res.data.totalPages,
            page: res.data.page,
            totalRecord: res.data.totalRecord,
            noProg: ''
          })
        }
      }).catch((err) => {
        this.setState({
          datas: [],
          loader: false,
          noProg: "No Data",
        });
        console.log(err, "error getall task");
      });
    })
  }
  onPreviewTasks = (item, index, hd) => {
    this.setState({ loader: true });
    let items = JSON.parse(item.Item)
    this.setState({items : items})
    this.setState({ preview :true })
    // this.props.history.push({
    //   pathname: `/${localStorage.getItem('baseURL')}/main/createtask`,
    //    : { detail: items, taskId: items._id ,taskRefresh: true },
    // });

    this.setState({ loader: false });

  }
  getTasks(l) {
    this.setState({ loader: true, datas: [] });
    Axios.get(
      `${this.state.env["task"]}/tasks/userId?userId=${this.state.userID}&page=${this.state.page}&limit=${this.state.limit}`
    )
      .then((res) => {
        console.log(res, "success getall task");
        if (res.data.length === 0) {
          this.setState({ noProg: "No Data" });
        }
        let getTask = res.data.Data;
        let totalPages = res.data.total;
        console.log(totalPages, "totalPages");
        let totalRecordItem = Number(totalPages) / Number(l);
        console.log(totalRecordItem, "totalRecordItem");
        let totalRecord = 0;
        if (String(totalRecordItem).split(".").length > 1) {
          totalRecord = Number(String(totalRecordItem).split(".")[0]) + 1;
          console.log(totalRecord, "totalrecord if");
        } else {
          totalRecord = totalRecordItem;
          console.log(totalRecord, "totalrecord else");
        }

        this.setState({ totalPages: totalPages, totalRecord: totalRecord });
        let taskList = [];
        getTask.map((item) => {
          var date = new Date(item["createdAt"]);
          item["createdAt"] = `${String(date.getDate()).length == 1
            ? `0${date.getDate()}`
            : date.getDate()
            }/${String(date.getMonth() + 1).length == 1
              ? `0${date.getMonth() + 1}`
              : date.getMonth() + 1
            }/${date.getFullYear()}`;
          let created;
          if (item.status === "Created") {
            let createdObject = [
              {
                name: "Start",
              },
              {
                name: "Close",
              },
              {
                name: "Cancel",
              },
              {
                name: "Remove",
              },
            ];
            created = createdObject;
          } else if (item.status === "Open") {
            let createdObject = [
              {
                name: "Close",
              },
              {
                name: "Cancel",
              },
              {
                name: "Remove",
              },
            ];
            created = createdObject;
          } else if (item.status === "Done") {
            let createdObject = [
              {
                name: "Remove",
              },
            ];
            created = createdObject;
          } else if (item.status === "Cancelled") {
            let createdObject = [
              {
                name: "Start",
              },
              {
                name: "Close",
              },
              {
                name: "Remove",
              },
            ];
            created = createdObject;
          }
          taskList.unshift({
            ID: item.taskId,
            "Created On": item.createdAt,
            Subject: item.subject,
            Description: item.description,
            "Due Date": item.dueDate,
            Status: item.status,
            Item: JSON.stringify(item),
            action: created,
          });
        });
        if (getTask.length === 0) {
          this.setState({ noProg: "No Data" });
        }
        this.setState({
          datas: taskList,
          loader: false,
        });
      })
      .catch((err) => {
        this.setState({
          datas: [],
          loader: false,
          noProg: "No Data",
        });
        console.log(err, "error getall task");
      });
  }
  // onPaginationChange = (page, limit) => {
  //   console.log(page, limit);
  //   this.setState(
  //     { page: Number(page - 1) * Number(limit), limit: limit },
  //     () => {
  //       console.log(this.state.start, "start", this.state.limit, "limit");
  //       this.getTasks(Number(this.state.limit));
  //     }
  //   );
  // };
  onPaginationChange = (page, limit) => {
    this.setState({ page: page, limit: limit }, () => {
      this.getTaskLists()
    })
}
 
  CreateTask = () => {
    this.props.history.push(`/${localStorage.getItem('baseURL')}/main/createtask`);
  };
  Columnclick = (item, e) => {
    console.log("Columnclick", item, e);
  };
  handleActionClick = (item, index, hd, name) => {
    let ID = item.Item;
    let parseData = JSON.parse(ID);
    let _id = parseData._id;
    if (name === "Start") {
      this.setState({ loader: true });
      let status = "";
      status = "start";
      Axios.put(`${this.state.env["task"]}/tasks/${status}/${_id}`)
        .then((res) => {
          this.setState({ loader: true });
          console.log(res, "update task status");
          this.getTasks(10);
        })
        .catch((err) => {
          console.log(err, "error task status");
          this.setState({ loader: false });
        });
    } else if (name === "Close") {
      this.setState({ loader: true });
      let status = "";
      status = "close";
      Axios.put(`${this.state.env["task"]}/tasks/${status}/${_id}`)
        .then((res) => {
          this.setState({ loader: true });
          console.log(res, "update task status");
          this.setState({ loader: false });
          this.getTasks(10);
        })
        .catch((err) => {
          console.log(err, "error task status");
          this.setState({ loader: false });
        });
    } else if (name === "Cancel") {
      this.setState({ loader: true });
      let status = "";
      status = "cancel";
      Axios.put(`${this.state.env["task"]}/tasks/${status}/${_id}`)
        .then((res) => {
          this.setState({ loader: true });
          console.log(res, "update task status");
          this.setState({ loader: false });
          this.getTasks(10);
        })
        .catch((err) => {
          console.log(err, "error task status");
          this.setState({ loader: false });
        });
    } else if (name === "Remove") {
      this.setState({ loader: true });
      let status = "";
      status = "cancel";
      Axios.delete(`${this.state.env["task"]}/tasks/${_id}`)
        .then((res) => {
          this.setState({ loader: true });
          console.log(res, "success delete task status");
          this.setState({ loader: false });
          this.getTasks(10);
        })
        .catch((err) => {
          console.log(err, "error delete task status");
          this.setState({ loader: false });
        });
    }
  };
  handleBack =() =>{
    this.setState({preview :!this.state.preview})
    this.getTaskLists();
  }
  onRowCheckBox = () =>{}
  render() {
    return (
      <React.Fragment>
        {this.state.loader ? <Loader /> : null}
        {!this.state.preview ?
        <div>
        <React.Fragment>
          <div className="trial-balance-header-title">
            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
            <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Tasks</p>
          </div>
        </React.Fragment>

        <div className="invoice-page-wrapper new-table-wrap ">
          {this.state.containerNav == undefined ? null : (
            <ContainerNavbar
              containerNav={this.state.containerNav}
              onAddNew={() => this.CreateTask()}
            />
          )}
          <div className="table-wrapper task-table-wrapper" style={{ marginTop: "10px" }}>
            {this.state.datas.length == 0 ? (
              <p className="noprog-txt">{this.state.noProg}</p>
            ) : (
                <React.Fragment>
                  <div className="remove-last-child-table">
                    <ZqTable
                      data={this.state.datas}
                      rowClick={(item) => {
                        // this.onPreviewIssues(item);
                        this.onPreviewTasks(item);
                      }}
                      onRowCheckBox = {this.onRowCheckBox}
                      handleActionClick={this.handleActionClick}
                    />
                  </div>
                </React.Fragment>
              )}
            {this.state.datas.length !== 0 && (
              <PaginationUI
              total={this.state.totalRecord}
              onPaginationApi={this.onPaginationChange}
              totalPages={this.state.totalPages}
              limit={this.state.limit}
              currentPage={this.state.page}
              />
            )}
          </div>
        </div>
        </div>
        :
        <React.Fragment>
          <CreateTask  details={this.state.items} handleBack={this.handleBack} />
        </React.Fragment>

       }
      </React.Fragment>
    );
  }
}
export default withRouter(Task);
