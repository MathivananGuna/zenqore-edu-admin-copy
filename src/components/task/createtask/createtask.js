import React, { Component, createRef } from "react";
import ZenForm from "../../input/form";
import taskJson from "./createtask.json";
import "../../../scss/task.scss";
import '../../../scss/common.scss';
import '../../../scss/setup.scss';
import "../../../scss/student.scss";
import '../../../scss/receive-payment.scss';
import Axios from "axios";
import Loader from "../../../utils/loader/loaders";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import moment, { defaultFormat } from "moment";
import KeyboardBackspaceSharpIcon from "@material-ui/icons/KeyboardBackspaceSharp";
import { Alert } from "rsuite";
import Snackbar from '@material-ui/core/Snackbar';
class CreateTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      createForm: taskJson.taskJson,
      role: localStorage.getItem('user'),
      authToken: localStorage.getItem('auth_token'),
      orgId: localStorage.getItem('orgId'),
      env: JSON.parse(localStorage.getItem("env")),
      userID: localStorage.getItem("userID"),
      date: new Date(),
      previewDetails: {},
      taskId: "",
      tab: 0,
      noProg: "Fetching Details...",
      formLoadStatus: false,
      tabData: ['TASK INFORMATION', 'TASK DETAILS'],
      snackbar: { openNotification: false, NotificationMessage: "", status: "success" },
      activeTab: 0,
      tableDetails: {
        Action: '',
        year: 'Current Year',
        amount: 0,
        adjustNextAmount: 0,
        adjustCurrentAmount: 0,
        refundAmount: 0,
      },
      paymentTransactions: {
        modeofPayment: '',
        transactionId: '',
        branchType: '',
        bankName: '',
        chequeStatus: '',
        utrNumber: '',
        netBankingType: ''
      },
      studentId: '',
      feeTypeLists: [],
      studentData: [],
      actionType: "Process",
      task_details: {},
      previewStatus:"",
      processId:''
    };
    this.inputFile = createRef(this.inputFile);
  }
  componentDidMount() {
    this.setState({ loader: true });
    let { tableDetails, paymentTransactions, task_details } = this.state;
    if (this.props === undefined) {
      this.props.history.push(`/${localStorage.getItem('baseURL')}/main/task`)
    }
    else {
      let payloadInsName = localStorage.getItem('baseURL').toUpperCase()
      let sampleUserData = [];
      Axios.get(`${this.state.env["ken42"]}/zq/user/listuser?institute=${payloadInsName}`, {
        headers: {
          'Authorization': this.state.authToken
        }
      }).then(res => {
        res.data.data.map((dataOne, i) => {
          if (res.data.data.length == 0) {
            this.setState({ tableResTxt: "No Data" })
          }
          else {
            sampleUserData.push({
              "value": dataOne['_id'],
              "label": `${dataOne.firstName} ${dataOne.lastName}`
            })
          }
        })
      })
      .catch(err => { console.log(err) })
      let details = this.props.details
      this.setState({ previewDetails: details });
      let taskJson = this.state.createForm
      console.log("*****taskJson1**********",taskJson)
      taskJson['Task Information'].map((item) => {
        if (item.name === "taskId") {
          item['defaultValue'] = details['taskId']
          item['required'] = false
        }
        else if (item.name === "title") {
          item['defaultValue'] = details['title']
          item['required'] = false
        }
        else if (item.name === "description") {
          item['defaultValue'] = details['description']
          item['required'] = false
        }
        else if (item.name === "type") {
          item['defaultValue'] = details['type'] == 'refundAdjust' ? "refund" : details['type'] == 'adjustRefund' ? "adjust" : details['type']
          item['required'] = false
          item['readOnly'] = true
          let actionType = details['type'] == 'refundAdjust' ? "refund" : details['type'] == 'adjustRefund' ? "adjust" : details['type']
          tableDetails.Action = actionType.charAt(0).toUpperCase() + actionType.slice(1)
          tableDetails.amount = details.data['amount']
          tableDetails.adjustNextAmount = details.data['adjustNextAmount']
          tableDetails.adjustCurrentAmount = details.data['adjustCurrentAmount']
          tableDetails.refundAmount = details.data['refundAmount']
          this.setState({ tableDetails })
        }    
        else if (item.name === "action") {
          let ActionOpt = []
          // item['defaultValue'] = details['Action']
          item['defaultValue'] = 'Process'
          item['required'] = false
          if (String(this.state.role).toLowerCase() == "admin") {
            ActionOpt = [
              {
                "label": "Assign",
                "value": "Assign"
              }, {
                "label": "Process",
                "value": "Process"
              }
            ]
            item['options'] = ActionOpt
          }
          else {
            ActionOpt = [
              {
                "label": "Process",
                "value": "Process"
              }
            ]
            item['options'] = ActionOpt
          }

        }
        else if (item.name === "assignedTo") {
          item['options'] = sampleUserData
          item['defaultValue'] = details['assignedTo']
          item['required'] = false
          item['readOnly'] = true
        }
        else if (item.name === "dueDate") {
          item['defaultValue'] = details['dueDate'] !== " " ? new Date(details['dueDate']).toString() : new Date()
          item['readOnly'] = true
          item['required'] = false
        }
        else if (item.name === "status") {
          item['defaultValue'] = details['Status']
          item['required'] = false
        }
        
      });
      console.log("*****taskJson2**********",taskJson)
      // if(details['action'].defaultValue == "Process"){
      //   taskJson['Task Information'].map(dataKey =>{
      //     if(dataKey.name == "assignedTo"){
      //       dataKey['readOnly'] = true
      //     }
      //   })
      // }
      // else{
      //   taskJson['Task Information'].map(dataKey =>{
      //     if(dataKey.name == "assignedTo"){
      //       dataKey['readOnly'] = true
      //   }
      // })
      // }
      if (details['type'] === "adjust") {
        let adjustOpt = [
          {
            "label": "Current Year",
            "value": "Current Year"
          },
          {
            "label": "Next Year",
            "value": "Next Year"
          }
        ]
        taskJson['Task Details'] = [{
          "type": "select",
          "name": "yearAdjust",
          "class": "select-input-wrap  branch-input setup-input-wrap ",
          "label": "Year to Adjust",
          "defaultValue": details.data['adjustNextAmount'] > 0 ? 'Next Year' : 'Current Year',
          "options": adjustOpt,
          "readOnly": true,
          "required": false,
          "requiredBoolean": false
        }]
      }
      else {
        taskJson['Task Details'] = [
          {
            "category": "input",
            "type": "text",
            "name": "studentName",
            "label": "Student Name",
            "class": "input-wrap ",
            "readOnly": false,
            "required": true,
            "requiredBoolean": true
          },
          {
            "category": "input",
            "type": "text",
            "name": "studentID",
            "label": "Student ID",
            "class": "input-wrap ",
            "readOnly": false,
            "required": true,
            "requiredBoolean": true
          },
          {
            "category": "input",
            "type": "text",
            "name": "bankName",
            "label": "Bank Name",
            "class": "input-wrap ",
            "readOnly": false,
            "required": true,
            "requiredBoolean": true
          },
          {
            "category": "input",
            "type": "text",
            "name": "bankIfsc",
            "label": "Bank IFSC",
            "class": "input-wrap ",
            "readOnly": false,
            "required": true,
            "requiredBoolean": true
          },
          {
            "category": "input",
            "type": "text",
            "name": "bankAccountName",
            "label": "Bank Account Name",
            "class": "input-wrap ",
            "readOnly": false,
            "required": true,
            "requiredBoolean": true
          },
          {
            "category": "input",
            "type": "text",
            "name": "bankAccountNo",
            "label": "Bank Account Number",
            "class": "input-wrap ",
            "readOnly": false,
            "required": true,
            "requiredBoolean": true
          }]
        taskJson['Task Details'].map((item, i) => {
          if (item.name === "studentID") {
            item['defaultValue'] = details.data['studentId']
            item['required'] = false
          }
          else if (item.name === "studentName") {
            item['defaultValue'] = details.data['studentName']
            item['required'] = false
          }
          else if (item.name === "bankAccountName") {
            item['required'] = false
            item['defaultValue'] = details.data['bankAccountName']
          }
          else if (item.name === "bankAccountNo") {
            item['defaultValue'] = details.data['bankAccountNo']
            item['required'] = false
          }
          else if (item.name === "bankIfsc") {
            item['defaultValue'] = details.data['bankIfsc']
            item['required'] = false
          }
          else if (item.name === "bankName") {
            item['defaultValue'] = details.data['bankName']
            item['required'] = false
          }
        })
      }
      paymentTransactions['modeofPayment'] = details.data.modeofPayment
      paymentTransactions['transactionId'] = details.data.transactionId
      paymentTransactions['branchType'] = details.data.branchType
      paymentTransactions['bankName'] = details.data.bankName
      paymentTransactions['chequeStatus'] = details.data.chequeStatus
      paymentTransactions['utrNumber'] = details.data.utrNumber
      paymentTransactions['netBankingType'] = details.data.netBankingType

      this.setState({
        taskId: details.taskId,
        loader: false,
        createForm: taskJson,
        studentId: details.data['studentId'],
        paymentTransactions,
        task_details: details.data,
        previewStatus : details['status'],
        processId : details['assignedTo']
      });
      this.getStudentsData(details.data['studentId'])
    }
  }


  onInputChanges = (value, item, event, dataS) => {
    console.log(dataS);
    let taskJson = this.state.createForm;
    let adjustOpt = [
      {
        "label": "Current Year",
        "value": "Current Year"
      },
      {
        "label": "Final Year",
        "value": "Final Year"
      }
    ]
   
    if (dataS !== undefined) {
      if (dataS.name == "action") {
       
        if (value == "Process") {
          taskJson['Task Information'].map(dataKey =>{
            if(dataKey.name == "assignedTo"){
              dataKey['defaultValue'] = this.state.processId
              dataKey['readOnly'] = true
            }
          
          })
          this.setState({ actionType: value })
        }
        else{
          taskJson['Task Information'].map(dataKey =>{
            if(dataKey.name == "assignedTo"){
              dataKey['readOnly'] = false
            }
           
          })
          this.setState({ actionType: value })
        }
      }

      if (dataS.name == "type") {
        if (value == "Adjust") {
          taskJson['Task Details'] = [{
            "type": "select",
            "name": "yearAdjust",
            "label": "Year to Adjust",
            "class": "select-input-wrap  branch-input setup-input-wrap ",
            "defaultValue": this.state.previewDetails.data[0]['value'],
            "options": adjustOpt,
            "readOnly": false,
            "required": true,
            "requiredBoolean": true
          }]
        }
        else {
          taskJson['Task Details'] = [
            {
              "category": "input",
              "type": "text",
              "name": "studentName",
              "label": "Student Name",
              "class": "input-wrap ",
              "readOnly": false,
              "required": true,
              "requiredBoolean": true
            },
            {
              "category": "input",
              "type": "text",
              "name": "studentId",
              "label": "Student ID",
              "class": "input-wrap ",
              "readOnly": false,
              "required": true,
              "requiredBoolean": true
            },
            {
              "category": "input",
              "type": "text",
              "name": "bankName",
              "label": "Bank Name",
              "class": "input-wrap ",
              "readOnly": false,
              "required": true,
              "requiredBoolean": true
            },
            {
              "category": "input",
              "type": "text",
              "name": "bankIfsc",
              "label": "Bank IFSC",
              "class": "input-wrap ",
              "readOnly": false,
              "required": true,
              "requiredBoolean": true
            },
            {
              "category": "input",
              "type": "text",
              "name": "bankAccountName",
              "label": "Bank Account Name",
              "class": "input-wrap ",
              "readOnly": false,
              "required": true,
              "requiredBoolean": true
            },
            {
              "category": "input",
              "type": "text",
              "name": "bankAccountNo",
              "label": "Bank Account Number",
              "class": "input-wrap ",
              "readOnly": false,
              "required": true,
              "requiredBoolean": true
            }
          ]
        }
      }
    }
    else {
      if (dataS == undefined && item == undefined) {
        this.setState({ date: new Date(value) }, () => {
          console.log(moment(new Date(this.state.date)).format('DD/MM/YYYY'))
        })
      }
    }
    this.setState({ formLoadStatus: false, createForm: taskJson })
  }

  onSubmit = (e) => {
    e.preventDefault();
    this.setState({ loader: true });
    const { paymentTransactions } = this.state;
    let TaskInformation = this.state.createForm
    let payload = {
      "status":"Assigned",
      "data": this.state.task_details
    }
    TaskInformation['Task Information'].map((task) => {
      if (task['name'] == "taskId") {
        payload['taskId'] = task['defaultValue']
      }
      if (task['name'] == "title") {
        payload['title'] = task['defaultValue']
      }
      if (task['name'] == "description") {
        payload['description'] = task['defaultValue']
      }
      if (task['name'] == 'type') {
        payload['type'] = task['defaultValue']
      }
      if (task['name'] == 'assignedTo') {
        payload['assignedTo'] = task['defaultValue']
      }
      if (task['name'] == "dueDate") {
        payload['dueDate'] = task['defaultValue']
      }
      if (task['name'] == "action") {
        payload['action'] = task['defaultValue']
      }
      
    })
    console.log('payload', JSON.stringify(payload))

    let payloadData = this.state.studentData;
    let relatedStudentFeeId = []
    if (this.state.actionType === 'Process') {
      let otcPayload = {
        "transactionDate": String(this.state.date),
        "relatedTransactions": relatedStudentFeeId,
        "studentId": payloadData.studentDetails['_id'],
        "emailCommunicationRefIds": payloadData.guardianDetails.email == 'NA' || payloadData.guardianDetails.email == '' || payloadData.guardianDetails.email == '-' || payloadData.guardianDetails.email == null ? payloadData.studentDetails.email : payloadData.guardianDetails.email,
        "transactionType": "eduFees",
        "transactionSubType": "feePayment",
        "studentFeeMap": payloadData.studentFeeMapId,
        "amount": Number(this.state.tableDetails.amount),
        "status": "initiated",
        "data": {
          "feesBreakUp": this.state.feeTypeLists,
          "orgId": localStorage.getItem('orgId'),
          "transactionType": "eduFees",
          "transactionSubType": "feePayment",
          "mode": paymentTransactions['modeofPayment'] == 'Bank Transfer' ? 'netbanking': paymentTransactions['modeofPayment'].toLowerCase(),
          "method": "otc",
          "modeDetails": {
            "netBankingType": paymentTransactions['netBankingType'],
            "walletType": null,
            "instrumentNo": paymentTransactions['utrNumber'],
            "cardType": null,
            "nameOnCard": null,
            "cardNumber": null,
            "instrumentDate": String(this.state.date),
            "bankName": paymentTransactions['bankName'],
            "branchName": paymentTransactions['branchType'],
            "transactionId": paymentTransactions['transactionId'],
            "remarks": null
          },
        },
        "paymentTransactionId": paymentTransactions['transactionId'],
        "createdBy": localStorage.getItem('userId'),
        "academicYear": payloadData.programPlanDetails.academicYear,
        "class": payloadData.programPlanDetails.title,
        "studentName": payloadData.studentDetails.firstName + " " + payloadData.studentDetails.lastName,
        "studentRegId": payloadData.studentDetails.regId,
        "programPlanId": payloadData.studentDetails.programPlanId,
        "type": payloadData.receiptStatus == 'immediately' ? 'receipt' : payloadData.receiptStatus
      }
      if (this.state.tableDetails.Action == 'adjust' || this.state.tableDetails.Action == 'Adjust') {

        Axios.post(`${this.state.env['zqBaseUri']}/edu/otcPayments`, otcPayload, {
          headers: {
            "Authorization": localStorage.getItem('auth_token')
          }
        })
          .then(response => {
            console.log('res', response)

          }).catch(err => {
            console.log('eee', err)
            this.setState({ circularLoader: false })
          })
      }
      else if (this.state.tableDetails.Action == 'Refund' || this.state.tableDetails.Action == 'RefundAdjust') {

        Axios.post(`${this.state.env['zqBaseUri']}/edu/otcPayments`, otcPayload, {
          headers: {
            "Authorization": localStorage.getItem('auth_token')
          }
        })
          .then(response => {
            console.log('res', response)
            let otcrespdata = response.data
            let relatedPaymentID = []
            relatedPaymentID.push(otcrespdata['receiptId'])
            let refundPaylaod = {
              "displayName": "",
              "transactionType": "",
              "transactionSubType": "",
              "transactionDate": "",
              "studentId": payloadData.studentDetails['_id'],
              "studentRegId": payloadData.studentDetails.regId,
              "studentName": payloadData.studentDetails.firstName + " " + payloadData.studentDetails.lastName,
              "class": payloadData.programPlanDetails.title,
              "academicYear": payloadData.programPlanDetails.academicYear,
              "programPlan": payloadData.studentDetails.programPlanId,
              "amount": Number(this.state.tableDetails.refundAmount),
              "emailCommunicationRefIds": payloadData.guardianDetails.email == 'NA' || payloadData.guardianDetails.email == '' || payloadData.guardianDetails.email == '-' || payloadData.guardianDetails.email == null ? payloadData.studentDetails.email : payloadData.guardianDetails.email,
              "smsCommunicationRefIds": "",
              "status": "",
              "paymentRefId": otcrespdata.receiptId,
              "paymentTransactionId": null,
              "relatedTransactions": relatedPaymentID,
              "orgId": localStorage.getItem('orgId'),
              "transactionId": null,
              "data": {
                "paid": Number(this.state.tableDetails.refundAmount),
                "orgId": localStorage.getItem('orgId'),
                "displayName": "",
                "studentId": payloadData.studentDetails['_id'],
                "studentRegId": payloadData.studentDetails.regId,
                "class": payloadData.programPlanDetails.title,
                "academicYear": payloadData.programPlanDetails.academicYear,
                "programPlan": payloadData.studentDetails.programPlanId,
                "issueDate": "",
                "dueDate": payloadData.dueDate,
                "feesBreakUp": this.state.feeTypeLists
              },
              "createdBy": localStorage.getItem('userId'),
              "studentFeeMapId": payloadData.studentFeeMapId
            }
            Axios.post(`${this.state.env['zqBaseUri']}/edu/refund`, refundPaylaod, {
              headers: {
                "Authorization": localStorage.getItem('auth_token')
              }
            }).then(response => {
              console.log('res', response)
              this.setState({ loader: false })
            })
              .catch(err => {
                console.log('eee', err)
                this.setState({ loader: false })
              })

          }).catch(err => {
            console.log('eee', err)
            this.setState({ loader: false })
          })

      }

    }
    Axios.put(`${this.state.env["zqBaseUri"]}/edu/tasks/${this.props.details['_id']}?orgId=${this.state.orgId}`, payload, {
      headers: {
        "Authorization": this.state.authToken
      }
    })
      .then((res) => {
        console.log(res, "CreateTask");
        Alert.success("Task has been updated successfully")
        this.setState({ loader: false })
        // let a = this.state.snackbar;
        // a.openNotification = true;
        // a.NotificationMessage = "Task has been updated successfully";
        // a.status = "success";
        // this.setState({ loader: false, snackbar: a })
        // setTimeout(() => {
        //   a.openNotification = false;
        //   this.setState({ snackbar: a })
        // }, 2000)
        this.props.handleBack()
      })
      .catch((err) => {
        Alert.error('api response failed')
        this.setState({ loader: false })
        // let a = this.state.snackbar;
        // a.openNotification = true;
        // a.NotificationMessage = "Failed to update the task item..!";
        // a.status = "error";
        // this.setState({ loader: false, snackbar: a })
        // setTimeout(() => {
        //   a.openNotification = false;
        //   this.setState({ snackbar: a })
        // }, 2000)
      });
  };
  handleBack = () => {
    this.props.handleBack()
  };
  goToHome = () => {
    this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`);
  };
  tabChangeInternal = (e, value) => { console.log(value); this.setState({ activeTab: value }) }
  getStudentsData = (student_id) => {
    this.setState({ loader: true });
    let studentID = student_id
    let headers = {
      'Authorization': this.state.authToken
    };
    Axios.get(`${this.state.env['zqBaseUri']}/edu/feesDetails/${studentID}?orgId=${this.state.orgId}`, { headers })
      .then(res => {
        console.log(res)
        let data = res.data
        let feeTypes = []
        let feesBreakUp = data.feesBreakUp;
        let type = data.receiptStatus
        feesBreakUp.map(feeElt => {
          feeTypes.push({
            'feeTypeId': feeElt['_id'],
            'feeType': feeElt.title,
            'amount': feeElt.title.includes('Tuition') ? Number(this.state.tableDetails.amount).toFixed(2) : Number(feeElt.pending).toFixed(2),
            'feeTypeCode': feeElt.feeTypeCode
          })
        })
        this.setState({ feeTypeLists: feeTypes, studentData: data });
        setTimeout(() => {
          this.setState({ loader: false });
        }, 2000)
      })
      .catch(err => {
        console.log(err)
        this.setState({ loader: false })
      })
  }
  cleanData = () => {
  }
  render() {
    return (
      <React.Fragment>
        {this.state.loader ? <Loader /> : null}
        <div className="reports-student-fees list-of-students-mainDiv table-head-padding">
          <div className="tab-form-wrapper tab-table">
            <div className="preview-btns">
              <div className="preview-header-wrap">
                < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.handleBack() }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                <h6 className="preview-Id-content" onClick={() => { this.handleBack() }}>| Tasks | {this.state.taskId}</h6>
              </div>
            </div>
            <div className="organisation-table student-submit-none">
              <div className="form-new-button-list" style={{ position: "absolute", float: "right", right: '0px', zindex: "1" }}>
             {this.state.previewStatus !== "Assigned" ?  <button className="finish-button-demand" onClick={this.onSubmit} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }}>Submit</button>
               : <button className="finish-button-demand" onClick={() => { this.handleBack() }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }}>Done</button>}
              </div>
              <div className="tab-wrapper">
                <Tabs
                  value={this.state.activeTab}
                  indicatorColor="primary"
                  textColor="primary"
                  onChange={this.tabChangeInternal}
                >
                  {this.state.tabData.map((tab, tabIndex) => {
                    return <Tab label={tab} />
                  })}
                </Tabs>
                <div className="preview-list-tab-data">
                  {this.state.activeTab === 0 ?
                    <React.Fragment>
                      <ZenForm inputData={this.state.createForm['Task Information']} onInputChanges={(e, a, event, dataS) => this.onInputChanges(e, a, event, dataS)} cleanData={this.cleanData} />
                    </React.Fragment>
                    : this.state.activeTab === 1 ?
                      <React.Fragment>
                        <div className='refundWrapper'>
                          <p className="form-hd" style={{ fontSize: "16px", fontWeight: "600" }} >
                            Amount Breakup
                       </p>
                          <div className="zenqore-leftside-wrap scholarship-process">
                            < div style={{ paddingTop: "20px" }}>
                              <table className="receive-pay-tableFormat" style={{ width: "50%", margin: '0px' }}>
                                <thead className="receive-pay-tableFormatHead">
                                  <th className="scholarship-action-type" style={{ width: "30%" }}>Action</th>
                                  <th>Year</th>
                                  <th >Amount (INR)</th>
                                </thead>
                                <tbody >
                                  {this.state.tableDetails.adjustNextAmount <= 0 ?
                                    <tr>
                                      <td className="scholarship-action-type" style={{ textAlign: "center" }}>{this.state.tableDetails.Action} </td>
                                      <td className="scholarship-action-type" style={{ textAlign: "center" }}>{this.state.tableDetails.year}</td>
                                      <td className="scholarship-num-type" style={{ textAlign: "right", paddingRight: "20px" }}>{Number(this.state.tableDetails.amount).toFixed(2)}</td>
                                    </tr>
                                    : <React.Fragment>
                                      <tr>
                                        <td className="scholarship-action-type">Refund</td>
                                        <td className="scholarship-action-type">Current Year</td>
                                        <td className="scholarship-num-type" style={{ textAlign: "right", paddingRight: "20px" }}>{Number(this.state.tableDetails.refundAmount).toFixed(2)}</td>
                                      </tr>
                                      <tr>
                                        <td className="scholarship-action-type">Adjust</td>
                                        <td className="scholarship-action-type">Current Year</td>
                                        <td className="scholarship-num-type" style={{ textAlign: "right", paddingRight: "20px" }}>{Number(this.state.tableDetails.adjustCurrentAmount).toFixed(2)}</td>
                                      </tr>
                                      <tr>
                                        <td className="scholarship-action-type">Adjust</td>
                                        <td className="scholarship-action-type">Next Year</td>
                                        <td className="scholarship-num-type" style={{ textAlign: "right", paddingRight: "20px" }}>{Number(this.state.tableDetails.adjustNextAmount).toFixed(2)}</td>
                                      </tr>
                                    </React.Fragment>}


                                </tbody>
                              </table>
                            </div>
                            <p style={{ fontSize: "16px", fontWeight: "600", paddingTop: '15px', paddingBottom: "15px" }} >Account Details</p>
                          </div>

                          <div >
                            <ZenForm inputData={this.state.createForm['Task Details']} onInputChanges={(e, a, event, dataS) => this.onInputChanges(e, a, event, dataS)} />
                          </div>
                        </div>


                      </React.Fragment>
                      : null
                  }
                </div>
              </div>
            </div>

          </div>
          <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={2000} onClose={this.closeNotification}>
            <Alert onClose={this.closeNotification}
              severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
              {this.state.snackbar.NotificationMessage}
            </Alert>
          </Snackbar>
        </div>

      </React.Fragment>
    );
  }
}
export default CreateTask;
