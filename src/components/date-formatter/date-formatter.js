import React from 'react';
import moment from 'moment';

function formatDate(format, date) {
    if (format !== null && date !== null) {
        let m = moment(date).format(format)
        return m
    }
}

const DateFormatter = (props) => {
    return <span>{formatDate(props.format, props.date)}</span>;
}

export default DateFormatter;