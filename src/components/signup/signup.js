import React, { Component } from 'react';
// import zenqoreHome from '../../assets/images/zenqoreHome.jpeg';
import ZenForm from '../input/form';
import signupForm from './signup.json';
import Axios from 'axios';
import "./../../scss/login.scss";
import Snackbar from '@material-ui/core/Snackbar';
import Button from '@material-ui/core/Button';
// import Loader from '../../utils/loader/loaders';
import zenqoreHome from '../../assets/images/welcomepage.svg';
import ZqsmallLogo from '../../assets/images/small_zqlogo.svg';
import ZqTextLogo from '../../assets/images/zenqore_text.svg';
import OrangeCrcle from '../../assets/images/orange-circle.svg';
import greenCircle from '../../assets/images/greenCircle.svg';
import smallGreenCircle from '../../assets/images/greenCircle2.svg';

import jwt_decode from "jwt-decode";
import Ken42Logo from '../../assets/images/ken42.png';
var CryptoJS = require("crypto-js");

const Cryptr = require('cryptr');
const cryptr = new Cryptr('ZqSecretKey');

var emailFromLink = ""

let configURL = "https://devapifeecollection.zenqore.com/initiate/config"
class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // error: '',
      emailerror: '',
      userInput: '',
      username: '',
      env: JSON.parse(localStorage.getItem('env')),
      signupFormData: [],
      snackBar: false,
      clientDetails: {
      },
    }
  }
  componentWillMount() {
    // this.setState({ env: JSON.parse(localStorage.getItem('env')) })
    let { newpath } = this.state;
    let emailLocation = window.location.href;
    const value = emailLocation.split("?")
    this.setState({ username: value[1], newpath: value[0] })
    this.props.history.push(newpath)
    this.setState({ env: JSON.parse(localStorage.getItem('env')) })
    localStorage.setItem("email", value[1]);
    localStorage.setItem("channel", value[1]);
    emailFromLink = value[1]


    let payload = { "username": emailFromLink }
    Axios.post(`${this.state.env['ken42']}/zqedu/otpregister`, payload)
      .then(response => {
        console.log(response)
      }, (err) => {
        console.log(err.response)
        if (err.response.data.message.includes("already")) {
          this.setState({ hidePasswordInputs: true, emailerror: "You are already registered...Please log in", pageLoader: false, snackBar: true })
          // setTimeout(() => {
          //     // this.props.history.push('/')
          //     this.setState({snackBar:false})
          // }, 1000);
        } else this.setState({ hidePasswordInputs: true, emailerror: "Admin has not approved this email address", disablebtn: true, snackBar: true })
      })
    signupForm.map(item => {
      if (String(item.name) === 'email') {
        item['defaultValue'] = emailFromLink
      }
      // return
    })
    this.setState({ signupFormData: signupForm })

    let instituteName = localStorage.getItem("baseURL")
    Axios.get(`${this.state.env['ken42']}/initiate/config/institute?nameSpace=${instituteName}`)
      .then(response => {
        console.log(response)
        let clientDetails = response.data.data
        console.log(clientDetails)
        localStorage.setItem('clientName', clientDetails.client)
        this.setState({ clientDetails: clientDetails }, () => {

        })
      })
  }

  onSubmit = (data, item) => {
    this.setState({ pageLoader: true });
    var password = CryptoJS.AES.encrypt(String(data.password.value), 'ZqSecretKey').toString()
    console.log('***data***', item)
    let apiDetails = item['apiDetails']['0']
    let regiterDetails = item['apiDetails']['1']
    let loginDetails = item['apiDetails']['2']
    let payload = { "username": data.email.value }
    Axios.post(`${this.state.env[apiDetails['baseUri']]}/${apiDetails['apiUri']}`, payload)
      .then(response => {
        console.log(response)
        this.setState({ hidePasswordInputs: false });
        let payload = { "username": String(data.email.value), "password": password }
        Axios.post(`${this.state.env[regiterDetails['baseUri']]}/${regiterDetails['apiUri']}`, payload)
          .then(response => {
            if (response.data.message.includes("registerd success")) {
              let payloadId = { "username": data.email.value, "password": password }
              // fetch(`${this.state.env.baseUri}/login`, { method: 'POST', body: JSON.stringify(payload), credentials: 'include' }).then(res => {
              Axios.post(`${this.state.env[loginDetails['baseUri']]}/${loginDetails['apiUri']}`, payloadId)
                .then(response => {
                  if (response.data.message.includes("login success")) {
                    this.setState({ pageLoader: false });
                    localStorage.setItem("auth_token", response.data['auth_token']);
                    localStorage.setItem("zen_auth_token", response.data['auth_token'])
                    localStorage.setItem("sessionId", response.data['session_id'])
                    let token = response.data['auth_token']
                    let decodedJson = jwt_decode(token);
                    console.log(decodedJson)
                    console.log("namespace", this.state.clientDetails)
                  }
                  else if (response.data.message.includes("Incorrect otp or Expired")) {
                    this.setState({ pageLoader: false });
                  }
                },
                  (err) => {
                    // console.log('error')
                  }
                )
              document.body.classList.value = 'header-fixed sidebar-lg-show sidebar-fixed signin'

            }

          }, (err) => {
            if (err.response.data.message.includes("Url expired.")) {
              this.setState({ pageLoader: false });
              this.setState({ emailerror: "Verification Link has been expired", enableResendLink: true })
              // this.props.history.push('/zensignup')
            }
            else if (err.response.data.message.includes("already")) {
              this.setState({ hidePasswordInputs: true, emailerror: "You are already registered...Please log in", pageLoader: false })
              setTimeout(() => {
                this.props.history.push(`/${localStorage.getItem('baseURL')}`)
              }, 2000);
            }
          })
        // }

      }, (err) => {
        console.log(err.response)
        if (err.response.data.message.includes("already")) {
          this.setState({ emailerror: "You are already registered...Please log in", pageLoader: false, snackBar: true })
          // setTimeout(() => {
          //   this.setState({snackBar:false})

          // }, 2000);
          // setTimeout(() => {
          //   this.props.history.push('/')
          // }, 2500);
        }
        else
          this.setState({ emailerror: "Admin has not approved this email address", pageLoader: false, snackBar: true })
        setTimeout(() => {
          this.setState({ snackBar: false })
        }, 2000);
      })

  }
  routerpage = (mzg) => {
    if (String(mzg).includes('not approved')) {
      this.props.history.push(`/${localStorage.getItem('baseURL')}`);
    }
    else {
      this.props.history.push(`/${localStorage.getItem('baseURL')}`)
    }
  }
  render() {
    return (<React.Fragment>
      {this.state.clientDetails.client ?
        <React.Fragment>
          <div className="zenqore-header-wrap">
            {this.state.clientDetails.client !== "ken42" ?
              <React.Fragment>
                <img src={ZqsmallLogo} className="zq-logo-image" alt="Zenqore" ></img>
                <img src={ZqTextLogo} className="zq-logo-setup" alt="Zenqore" ></img>
              </React.Fragment>
              :
              <React.Fragment>
                <div className="ken-logo-image">
                  <img src={Ken42Logo} alt="ken42" ></img>
                </div>
                <h4 className="kenLogotext">{this.state.clientDetails.name}</h4>
              </React.Fragment>}
          </div>

          <div className="login-wrap figma">
            <div className="zenqore-image-wrapper">
              <div className="zenqore-image-wrap">
                <React.Fragment>
                  <p className="zq-slogan"> Automated and Integrated Fee Collection <br /> Platform for Schools and Colleges </p>
                  <img src={zenqoreHome} className="zenHome-image" alt="Zenqore-Next Step Accounting" ></img>
                </React.Fragment>
              </div>
            </div>
            <div className="zenqore-leftside-wrap">
              <div className="circles-wrap">
                <img src={OrangeCrcle} className="circle-image" alt="Zenqore" ></img>
                <img src={greenCircle} className="circle-image1" alt="Zenqore" ></img>
                <img src={smallGreenCircle} className="circle-image2" alt="Zenqore" ></img>
              </div>
              <div className={`zenqore-login-container ${this.state.clientDetails.client === "ken42" ? 'ken42login' : ''}`}>
                <div className={`login-content ${this.state.clientDetails.client === "ken42" ? 'ken42loginContainer' : ''}`}>
                  <h3 className="login-form-hd">Welcome to {this.state.clientDetails.welcomeText}</h3>
                  <p className="login-desc-txt">Setup your password and Sign Up to Zenqore</p>
                  <ZenForm inputData={this.state.signupFormData} onSubmit={this.onSubmit} />
                  <div className="signup-req-wrap">
                    {/* <a className="forgot-pwd">Forgot  Password?</a> */}
                    {/* <p className="login-separator-line"></p> */}
                    {/* <p className="login-para">Already have an Account? <span className="login-btn-color" onClick={this.signupProcess} >Login Now</span></p> */}
                    {/* <div className="login-copyright-wrap"><p className="login-copyright-text">© {(new Date().getFullYear())} All rights reserved. Gigaflow Technologies LLP. </p></div> */}
                  </div>
                  <Snackbar
                    anchorOrigin={{
                      vertical: 'top',
                      horizontal: 'right',
                    }}
                    open={this.state.snackBar}
                    autoHideDuration={1000}
                    message={this.state.emailerror}
                    className="info-snackbar"
                    action={
                      <React.Fragment>
                        <Button color="secondary" size="small" onClick={(ev) => { ev.stopPropagation(); this.routerpage(this.state.emailerror) }}>
                          Click Here
                  </Button>
                      </React.Fragment>
                    }
                  />

                </div>
              </div>
            </div>
          </div>
          <div className="login-copyright-wrap figma">
            <div className="login-copyright-text">
              <p>{this.state.clientDetails.copyright}</p>
            </div>
          </div>
        </React.Fragment> : ''}
    </React.Fragment>);
  }
}

export default Signup;