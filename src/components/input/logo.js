import React from 'react';
import { withRouter } from 'react-router-dom';
import ReactCrop from 'react-image-crop';
import Button from '@material-ui/core/Button';
import '../../scss/input.scss';
import 'react-image-crop/dist/ReactCrop.css';
import DemoLogo from '../../assets/images/profile-large.png';
import '../../scss/settings.scss'
import '../../scss/input.scss'
import DragAndDrop from '../../utils/dragDrop';
import CircularProgress from '@material-ui/core/CircularProgress';

class Logo extends React.Component {
    constructor() {
        super();
        this.state = {
            isUpload: false,
            isLoader: false,
            src: null,
            logoPicture: DemoLogo,
            crop: {
                unit: '%',
                width: 30,
                height: 30
            },
        }
        this.logoRef = React.createRef(this.logoRef);
    }
    componentDidMount() {
        console.log("props", this.props)
        if (this.props.logoPicture) {
            this.setState({ logoPicture: this.props.logoPicture })
        } else {
            this.setState({ logoPicture: "" })
        }
        this.props.getLogoData(this.props)
    }
    onSelectFile = (e) => {
        this.setState({ isUpload: true })
        if (e.target.files && e.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () => {
                this.setState({ src: reader.result }) // bae64 format of full image
            });
            reader.readAsDataURL(e.target.files[0]);
        }
    }
    onImageLoaded = image => {
        this.imageRef = image; // the image tag
        let imgRef = this.logoRef.current["imageRef"]
        let height = imgRef.offsetWidth;
        let width = imgRef.offsetHeight;
    };

    onCropComplete = crop => {
        this.makeClientCrop(crop);
    };

    onCropChange = (crop, percentCrop) => {
        // You could also use percentCrop:
        this.setState({ crop }); // cropped part of the whole image
    };

    async makeClientCrop(crop) {
        if (this.imageRef && crop.width && crop.height) {
            const croppedImageUrl = await this.getCroppedImg(
                this.imageRef,
                crop,
                'logo.jpeg'
            );
            this.setState({ croppedImageUrl }, () => {
                // console.log(croppedImageUrl);
                this.props.handleLogo(this.state.logoPicture)
                // this.imageRef.src = ''
            });

        }
    }

    getCroppedImg(image, crop, fileName) {
        var base64data;
        const canvas = document.createElement('canvas');
        const scaleX = image.naturalWidth / image.width;
        const scaleY = image.naturalHeight / image.height;
        canvas.width = crop.width;
        canvas.height = crop.height;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            image,
            crop.x * scaleX,
            crop.y * scaleY,
            crop.width * scaleX,
            crop.height * scaleY,
            0,
            0,
            crop.width,
            crop.height
        );

        return new Promise((resolve, reject) => {
            canvas.toBlob(blob => {
                if (!blob) {
                    console.error('Canvas is empty');
                    return;
                }
                blob.name = fileName;
                window.URL.revokeObjectURL(this.fileUrl);
                this.fileUrl = window.URL.createObjectURL(blob);
                // resolve(this.fileUrl);
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                const scope = this
                reader.onloadend = function () {
                    base64data = reader.result;
                    // console.log(base64data);
                    resolve(base64data)
                    scope.setState({ logoPicture: base64data })
                }
            }, 'image/jpg');

        });
    }
    handleDrop = (files) => {
        this.setState({ isUpload: true })
        if (files && files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener('load', () => {
                this.setState({ src: reader.result }) // base64 format of full image
            });
            reader.readAsDataURL(files[0]);
        }
    }
    onError = () => {
        this.setState({ logoPicture: DemoLogo })
    }
    resetLogo = () => {
        this.setState({ isUpload: false, logoPicture: "" })
    }
    render() {
        const logoContainer = {
            width: "100%",
            height: "69vh",
            backgroundColor: "#f4f5f7",
            borderRadius: "10px",
            border: "1px dashed #505f79",
            display: "flex",
            alignItems: "center",
            justifyContent: "center"
        }
        const displayFlex = {
            width: "100%",
            height: "100%",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center"
        }
        const logoWrap = {
            height: "110px",
            width: "110px",
            borderRadius: "50%",
            transform: "scale(1.2)"
        }
        const logoText = {
            fontSize: "16px",
            fontFamily: "OpenSans-Regular",
            marginTop: "20px"
        }
        const logoText1 = {
            fontSize: "11px",
            fontFamily: "OpenSans-Regular",
            marginTop: "10px"
        }
        const browseButton = {
            textAlign: "center",
            color: "#0052cc",
            border: "1px solid #0052cc",
            padding: "10px 25px",
            borderRadius: "5px",
            fontFamily: "OpenSans-Medium",
            cursor: "pointer",
            marginTop: "10px"
        }
        const styleImageLogo = {
            height: "343px",
            width: "643px",
            marginTop: "6px",
            borderRadius: "10px"
        };
        const clearButton = {
            position: "absolute",
            fontWeight: "600",
            height: "35px",
            right: "55px",
            top: "90px",
            border: "none",
            color: "#0052cc",
            padding: "0",
            fontFamily: "OpenSans-Light",
            textTransform: "capitalize",
            boxShadow: "none",
            background: "none",
            cursor: "pointer",
            zIndex: 1
        }
        return (
            <React.Fragment>
                <div style={logoContainer} className="avatar-wrap-campus-logo" >
                    <Button variant="contained" type="clear" style={clearButton} onClick={() => this.resetLogo()}>Clear</Button>
                    <DragAndDrop handleDrop={this.handleDrop}>
                        {this.state.isUpload === false ?
                            <>
                                {this.state.isLoader ? <>
                                    <CircularProgress style={{ width: '100%', height: '20px', width: '20px', float: 'right', marginRight: '10px' }} />
                                </> : <>
                                    <div style={displayFlex}>
                                        <img src={this.state.logoPicture} onError={this.onError} style={logoWrap} />
                                        <p style={logoText} >Drag and Drop</p>
                                        <p style={logoText1} >or</p>
                                        <label htmlFor="files" style={browseButton} >Browse</label>
                                        <input id="files" className="select-image-input" type="file" accept="image/*" onChange={this.onSelectFile} style={{ display: "none" }} />
                                    </div>
                                </>}
                            </> :
                            <>
                                <ReactCrop
                                    style={styleImageLogo}
                                    src={this.state.src}
                                    crop={this.state.crop}
                                    ruleOfThirds
                                    onImageLoaded={this.onImageLoaded}
                                    onComplete={this.onCropComplete}
                                    onChange={this.onCropChange}
                                    ref={this.logoRef}
                                />
                            </>}
                    </DragAndDrop>
                </div>
            </React.Fragment >
        )
    }
}

export default withRouter(Logo)