import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Loader from "../../utils/loader/loaders";
import Button from '@material-ui/core/Button';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import ZqTable from "../../utils/Table/table-component";
import ZenTabs from '../input/tabs';
import ContainerNavbar from "../../gigaLayout/container-navbar";
import "../../scss/task.scss";
import "../../scss/onboard-user.scss";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import '../../scss/tabs.scss';
import '../../scss/student.scss';
import '../../scss/receive-payment.scss';
import '../../scss/payment-portal.scss';
import '../../scss/portal-stepper.scss';
import '../../scss/student-reports.scss';
import '../../scss/setup.scss';
import moment from 'moment';
import OnboardTableData from './onboardTableData.json';
import OnboardFormData from './onboardFormData.json';
import ZenForm from '../input/form';
import { Alert, Modal, SelectPicker } from "rsuite";
import TextField from '@material-ui/core/TextField';
import '../../scss/input.scss';
import "../../scss/newcss.scss";
import InputDate from '../../components/input/date';
import { Input, InputGroup } from 'rsuite';
import { DatePicker } from 'rsuite';
import Axios from 'axios';
import { event } from 'react-ga';
import Payment from "../payment/payment";
import BrightKidFeesJson from './brightkidFees.json'
import { ListItemAvatar } from '@material-ui/core';
import { el } from 'date-fns/locale';
import PaginationUI from "../../utils/pagination/pagination";
import CloseIcon from '@material-ui/icons/Close';

class Onboard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            noProg: "Fetching...",
            onboardData: [],
            onboardForm: OnboardFormData.ShowOnboardForm,
            onboardStudentForm: OnboardFormData.ShowOnboardForm['Student Details'],
            onboardParentForm: [],
            onboardPaymentForm: [],
            authToken: localStorage.getItem("auth_token"),
            env: JSON.parse(localStorage.getItem('env')),
            orgId: localStorage.getItem("orgId"),
            showOnboardPreview: false,
            noOfInstallmentArray: [],
            response: [],
            selectedNoofInstallment: 4,
            showSellerList: false,
            activeTab: 0,
            tabData: ['Student Details', 'Parent Details', 'Payment Details', 'installment Details'],
            loader: false,
            selectSeller: [{ label: "Prashanth - Seller 1", value: "Prashanth - Seller 1" }, { label: "Mathivanan - Seller 2", value: "Mathivanan - Seller 2" }, { label: "Sankar - Seller 3", value: "Sankar - Seller 3" }, { label: "Jay - Seller 4", value: "Jay - Seller 4" }, { label: "Naveen - Seller 5", value: "Naveen - Seller 5" }],
            selectedSeller: '',
            openModal1: false,
            checkIsPercentage: false,
            discountAmount: undefined,
            offeredDiscount: undefined,
            offeredFees: undefined,
            allotedPercentage: undefined,
            totalPercentage: 0,
            getAmount: undefined,
            totalAmount: undefined,
            previousAmount: undefined,
            totalPercentage: 0,
            applicationId: "",
            selectedProgramType: "",
            headsellerRemarks: "",
            remarks: "",
            financeRemarks: "",
            openModal: false,
            disableButton: false,
            StudentRegId: "demo123",
            StudentName: "Siva A",
            StudentClass: "Play Group",
            paymentType: "Card",
            PaymentFormData: [],
            handleClickData: {},
            getAmountOnchange: false,
            discountAmountChange: false,
            currentUser: "",
            responseFeeDetails: [],
            itemDetails: [{ id: 1 }],
            defaultUnit: null,
            totalProductPrice: 0,
            defaultProgramPlan: "",
            programData: [

            ],
            noOfInstallment: [
                {
                    "label": "1",
                    "value": 1
                },
                {
                    "label": "2",
                    "value": 2
                },
                {
                    "label": "3",
                    "value": 3
                },
                {
                    "label": "4",
                    "value": 4
                },
                {
                    "label": "5",
                    "value": 5
                },
                {
                    "label": "6",
                    "value": 6
                }
            ],
            productList: [
                {
                    "label": "Bookkit",
                    "value": "Bookkit",
                    "price": 2678,
                    "gst": 12
                },
                {
                    "label": "Craft Box",
                    "value": "Craft Box",
                    "price": 700,
                    "gst": 12
                },
                {
                    "label": "Montessory Box 1",
                    "value": "Montessory Box 1",
                    "price": 3500,
                    "gst": 12
                },
                {
                    "label": "Montessory Box 2",
                    "value": "Montessory Box 2",
                    "price": 3500,
                    "gst": 12
                }
            ],
            productPrice: [],
            itemList: [
                {
                    "label": "1",
                    "value": 1
                },
                {
                    "label": "2",
                    "value": 2
                },
                {
                    "label": "3",
                    "value": 3
                },
                {
                    "label": "4",
                    "value": 4
                },
                {
                    "label": "5",
                    "value": 5
                },
                {
                    "label": "6",
                    "value": 6
                },
                {
                    "label": "7",
                    "value": 7
                },
                {
                    "label": "8",
                    "value": 8
                },
                {
                    "label": "9",
                    "value": 9
                },
                {
                    "label": "10",
                    "value": 10
                }
            ],
            discountType: [
                {
                    "label": "Percentage",
                    "value": "Percentage"
                },
                {
                    "label": "Amount",
                    "value": "Amount"
                }
            ],
            selectedDiscountType: "Amount",
            selectedDiscountType2: undefined,
            containerNav: {
                isBack: false,
                name: "Lists of Request",
                isName: true,
                total: 0,
                isTotalCount: false,
                isSearch: false,
                isSort: false,
                isPrint: true,
                isDownload: false,
                isShare: false,
                isNew: false,
                newName: "New",
                isSubmit: false,
            },
            brightKidsData: [],
            installemntAmtStore: {
                "0": undefined
            },
            installemntPerStore: {
                "0": 30
            },
            rowClickData: {},
            previewpage: 1,
            previewlimit: 10,
            previewSampleUserData: [],
            previewtotalRecord: 0,
            previewtotalPages: 1,
            parsedValueData: {},
            totalPreviewRecord: 0,
            page: 1,
            limit: 10,
            totalRecord: 1,
            totalPages: 1,
            searchValue: '',
            paginationCall: "",
        }
    }
    componentDidMount = () => {
        this.onGetDefultData()
        this.leadsAPI();
    }

    leadsAPI =  async () => {
        this.setState({ loader: true, onboardData: [] });
        let currentUser = localStorage.getItem("user")
        let { onboardData } = this.state
        try {
            const apiData = await Axios.get(`${this.state.env['headSeller']}/bkah/edu/leads?orgId=${this.state.orgId}&limit=${this.state.limit}&page=${this.state.totalPages}`,
                {
                    headers: { "Authorization": localStorage.getItem('auth_token') }
                })
            let response = apiData.data.data
            console.log("response", response)

                if (response.length === 0) {
                    this.setState({ noProg: "No Data" });
                }
                onboardData = []
                response.forEach((item, index) => {
                    onboardData.push({
                        "Application ID": item.applicationId,
                        "Date": this.dateFilter(item.createdAt),
                        "Ward Name": item.student.firstName,
                        "Ward Age": this.calculateAge(item.student.dob),
                        "Parent Name": item.parent.name,
                        "Phone No.": item.parent.phoneNumber,
                        "Amount": item.accountStatus === "Finance Approved" ? this.formatCurrency(19845) : "-",
                        "Assigned To": item.assignedTo,
                        "Status": item.accountStatus.charAt(0).toUpperCase() + item.accountStatus.slice(1),
                         Item: JSON.stringify(item)
                    })
                    onboardData.forEach((onboardItem, onboardIndex) => {
                        if (onboardIndex === index) {
                            if (currentUser === "Headseller")
                                onboardItem.action = [
                                    {
                                        "name": "Process Now"
                                    },
                                    {
                                        "name": "Assign to Seller"
                                    }
                                ]
                            if (currentUser === "Seller") {
                                onboardItem.action = [
                                    {
                                        "name": "Process Now"
                                    }
                                ]
                            }
                        }
                    })

                })
            this.setState({ onboardData: [], currentUser }, () => {
                this.setState({ onboardData: onboardData, loader: false, totalPages: apiData.data.totalPages })
            })

        }
        catch (err) {

        }
    }
    onGetDefultData = () => {
        Axios.get(`${this.state.env['headSeller']}/zq/user/programs?type=tution`)
            .then(res => {
                let programData = []
                res.data.forEach(item => {
                    programData.push({
                        label: `Bright Kids ${item.programName}`,
                        value: item.programName.replaceAll("-", " ").replaceAll("Bright Kid ")
                    })
                })
                let brightKidsDatas = res.data
                brightKidsDatas.forEach(item => {
                    item.programName = item.programName.replaceAll("-", " ")
                })
                this.setState({ brightKidsData: brightKidsDatas, programData }, () => {
                })
            }).catch(err => {
            })
    }
    getInstallmentDetailsApi = () => {
        Axios.get(`${this.state.env['zqBaseUri']}/edu/stufeeplan?orgId=${this.state.orgId}&&appId=${this.state.rowClickData.applicationId}`,
            {
                headers: { "Authorization": localStorage.getItem('auth_token') }
            }
        ).then(res => {
            let responseData = res.data;
            if (responseData.length > 1) {
                let selectedProgramType = responseData[0].programPlanHEDAId;
                let getAmount = Number(responseData[0].plannedAmount) + Number(responseData[0].discountAmount);
                let selectedDiscountType = responseData[0].discountType;
                let allotedPercentage = responseData[0].discountPercentage;
                let discountAmount = responseData[0].discountAmount;
                let totalAmount = responseData[0].plannedAmount;
                let selectedNoofInstallment = responseData[1].installments.length;
                let noOfInstallmentArray = []
                let remarks = responseData[0].remarks.seller
                let headsellerRemarks = responseData[0].remarks.headseller
                let financeRemarks = responseData[0].remarks.finance != "" ? responseData[0].remarks.finance : "-"
                responseData[1]['installments'].forEach((item, index) => {
                    noOfInstallmentArray.push({
                        id: index + 1,
                        percentage: item.percentage,
                        description: item.description,
                        amount: item.paidAmount,
                        nextMonth: this.dateFilter(item.dueDate),
                        headsellerRemarks: item.remarks.headseller,
                        remarks: item.remarks.seller,
                        financeRemarks: item.remarks.finance != undefined ? item.remarks.finance : null,
                        status: item.status
                    })
                })
                let lastIndexAmount = noOfInstallmentArray[noOfInstallmentArray.length - 1]['amount']

                this.setState({
                    responseFeeDetails: responseData, selectedProgramType,
                    getAmount, selectedDiscountType, allotedPercentage, discountAmount,
                    totalAmount, selectedNoofInstallment, noOfInstallmentArray, remarks, financeRemarks, headsellerRemarks,
                    lastIndexAmount,
                    previousAmount: totalAmount
                })
            }
            else {
                let { noOfInstallmentArray, selectedProgramType, brightKidsData } = this.state
                noOfInstallmentArray = []
                brightKidsData.forEach(item => {
                    if (item.programName === selectedProgramType) {
                        item.installments.forEach((instalmentItem, index) => {
                            noOfInstallmentArray.push({
                                id: index + 1,
                                percentage: 0,
                                description: "",
                                amount: instalmentItem,
                                nextMonth: new Date(),
                                discount: 0,
                                remarks: "",
                                financeRemarks: "",
                                headsellerRemarks: "",
                                status: "Planned"
                            })
                        })
                        this.setState({
                            offeredDiscount: item.discount,
                            offeredFees: item.offeredPrice,
                            totalAmount: item.offeredPrice,
                            getAmount: item.totalFees,
                            previousAmount: item.offeredPrice,
                        });
                    }
                })
                let lastIndexAmount = noOfInstallmentArray[noOfInstallmentArray.length - 1]['amount']
                this.setState({ noOfInstallmentArray, lastIndexAmount })
            }
        }).catch(err => {

        })
    }
    dateFilter = (ev) => {
        var ts = new Date(ev);
        let getDate = `${String(ts.getDate()).length == 1 ? `0${ts.getDate()}` : ts.getDate()}`;
        let getMonth = `${String(ts.getMonth() + 1).length == 1 ? `0${ts.getMonth() + 1}` : ts.getMonth() + 1}`;
        let getYear = `${ts.getFullYear()}`;
        let today = `${getDate}/${getMonth}/${getYear}`;
        return today;
    };

    calculateAge = (dob) => {
        let getDate = new Date(dob)
        let diff_ms = Date.now() - getDate.getTime();
        let age_dt = new Date(diff_ms);
        return Math.abs(age_dt.getUTCFullYear() - 1970) + ' Years';
    }

    onPreviewOnboard = (item) => {
        this.setState({ activeTab: 0, remarks: "", financeRemarks: "" });
        let parsedItem = JSON.parse(item.Item)
        this.setState({ selectedProgramType: parsedItem['programdetail'].programPlanName }, () => {
            let { brightKidsData, selectedProgramType, getAmount, offeredDiscount, offeredFees, totalAmount } = this.state
            brightKidsData.forEach(item => {
                if (item.programName === selectedProgramType) {
                    getAmount = item.totalFees
                    offeredDiscount = item.discount
                    offeredFees = item.offeredPrice
                    totalAmount = item.offeredPrice
                }
                this.setState({ getAmount, offeredDiscount, offeredFees, totalAmount, previousAmount: item.offeredFees })
            })
        })


        if (parsedItem.accountStatus === "Finance Approved") {
            this.setState({ disableButton: true });
        } else {
            this.setState({ disableButton: false });
        }
        OnboardFormData.ShowOnboardForm['Student Details'].map(item => {
            if (item.name === "regId") {
                item.defaultValue = "-"
            }
            else if (item.name === "firstName") {
                item.defaultValue = parsedItem['student'].firstName || "-"
                this.setState({ StudentName: parsedItem['student'].firstName });
            }
            else if (item.name === "lastName") {
                item.defaultValue = parsedItem['student'].lastName || "-"
            }
            else if (item.name === "addmittedDate") {
                item.defaultValue = parsedItem['student'].admittedOn || "-"
            }
            else if (item.name === "dob") {
                item.defaultValue = moment(parsedItem['student'].dob).format('DD/MM/YYYY') || "-"
            }
            else if (item.name === "gender") {
                item.defaultValue = parsedItem['student'].gender || "-"
            }
            else if (item.name === "CitizenshipData") {
                item.defaultValue = parsedItem['student'].citizenShip.toLowerCase() || "-"
            }
            else if (item.name === "programPlan") {
                item.defaultValue = parsedItem['programdetail'].programPlanName || "-"
            }
            else if (item.name === "email") {
                item.defaultValue = parsedItem['student'].email || "-"
            }
            else if (item.name === "contactStudent") {
                item.defaultValue = parsedItem['student'].phoneNumber || "-"
            }
            else if (item.name === "currenyType") {
                item.defaultValue = parsedItem['currency'].currency || "-"
            }
            else if (item.name === "exchangeRate") {
                item.defaultValue = parsedItem['currency'].exchangeRate || "-"
            }
        })
        OnboardFormData.ShowOnboardForm['Parent Details'].map(item => {
            if (item.name === "fatherName") {
                item.defaultValue = parsedItem['parent'].name || "-"
            }
            else if (item.name === "emailParent") {
                item.defaultValue = parsedItem['parent'].email || "-"
            }
            else if (item.name === "contactParent") {
                item.defaultValue = parsedItem['parent'].phoneNumber || "- "
            }
            else if (item.name === "addressParent-1") {
                item.defaultValue = parsedItem['parent']['address'].address1 || "-"
            }
            else if (item.name === "addressParent-2") {
                item.defaultValue = parsedItem['parent']['address'].address2 || "-"
            }
            else if (item.name === "addressParent-3") {
                item.defaultValue = parsedItem['parent']['address'].address3 || "-"
            }
            else if (item.name === "City") {
                item.defaultValue = parsedItem['parent']['address'].city || "-"
            }
            else if (item.name === "state") {
                item.defaultValue = parsedItem['parent']['address'].state || "-"
            }
            else if (item.name === "country") {
                item.defaultValue = parsedItem['parent']['address'].country || "-"
            }
            else if (item.name === "pincode") {
                item.defaultValue = parsedItem['parent']['address'].pinCode || "-"
            }
        })
        this.setState({ showOnboardPreview: true, rowClickData: parsedItem, applicationId: parsedItem.applicationId }, () => {
            this.getInstallmentDetailsApi()
        });

    }

    formatCurrency = (amount) => {
        return new Intl.NumberFormat("en-IN", {
            style: "currency",
            currency: "INR",
            // minimumFractionDigits: 2,
            // maximumFractionDigits: 2,
        }).format(amount);
    };

    onInputChanges = (value, item, event, datas) => { }
    handleTabChange = (tabName, formDatas) => { }
    cancelViewData = () => { }

    handleActionClick = (item, index, hd, name) => {
        this.setState({ handleClickData: {} }, () => {
            this.setState({ handleClickData: item })
        });
        if (name == "Assign to Seller" && item.Status != "Finance Approved") {
            this.setState({ showSellerList: true, openModal: true });
        } else if (name == "Process Now") {
            this.onPreviewOnboard(item);
            this.setState({ showOnboardPreview: true })
        } else {
            Alert.info("This applicant has already been processed.")
        }
    }

    onAccept = () => {
        this.setState({ loader: true, });
        let { rowClickData, noOfInstallmentArray, handleClickData } = this.state
        let parseHandleClickData = Object.keys(rowClickData).length === 0 ? JSON.parse(handleClickData.Item) : rowClickData
        let getPaidAmount = noOfInstallmentArray.filter(item => item.status !== "Paid")
        let payloadPUT = {
            organizationId: parseHandleClickData.organizationId,
            paymentDetails: {
                applicationId: parseHandleClickData.applicationId,
                studentRegId: "",
                programPlanHEDAId: this.state.selectedProgramType,
                plannedAmount: Number(this.state.totalAmount),
                plannedAmountBreakup: [{
                    feeType: "tuition",
                    amount: Number(this.state.totalAmount)
                }],
                paidAmount: Number(getPaidAmount[0]['amount']),
                paidAmountBreakup: [{
                    feeType: "tuition",
                    amount: Number(getPaidAmount[0]['amount'])
                }],
                pendingAmount: Number(this.state.totalAmount) - Number(getPaidAmount[0]['amount']),
                pendingAmountBreakup: [{
                    feeType: "tuition",
                    amount: Number(this.state.totalAmount) - Number(getPaidAmount[0]['amount'])
                }],
                currency: parseHandleClickData.currency.currency,
                forex: "",
                discountType: this.state.selectedDiscountType,
                discountPercentage: Number(this.state.allotedPercentage),
                discountAmount: Number(this.state.discountAmount),
                discountAmountBreakup: [{
                    feeType: 'tuition',
                    amount: Number(this.state.discountAmount)
                }],
                remarks: {
                    seller: this.state.remarks,
                    finance: this.state.financeRemarks,
                    headseller: this.state.headsellerRemarks
                }
            }
        }
        let installmentPayloadDetails = []
        for (let i = 0; i < noOfInstallmentArray.length; i++) {
            installmentPayloadDetails.push({
                label: `Installment ${i + 1}`,
                description: noOfInstallmentArray[i]["description"],
                dueDate: noOfInstallmentArray[i].nextMonth,
                penaltyStartDate: this.onNext7Days(noOfInstallmentArray[i]['nextMonth']),
                percentage: Number(noOfInstallmentArray[i]['percentage']),
                plannedAmount: Number(noOfInstallmentArray[i]['amount']),
                plalnedAmountBreakup: [{
                    feeType: 'tuition',
                    amount: Number(noOfInstallmentArray[i]['amount'])
                }],
                paidAmount: Number(noOfInstallmentArray[i]['amount']),
                paidAmountBreakup: [{
                    feeType: 'tuition',
                    amount: Number(noOfInstallmentArray[i]['amount'])
                }],
                pendingAmount: 0,
                pendingAmountBreakup: [{
                    feeType: "tuition",
                    amount: 0
                }],
                discountType: this.state.selectedDiscountType,
                discountPercentage: Number(this.state.allotedPercentage),
                discountAmount: Number(this.state.discountAmount),
                discountAmountBreakup: [{
                    feeType: 'tuition',
                    amount: Number(this.state.discountAmount)
                }],
                productsAmount: null,
                productsAmountBreakup: [
                    {
                        productId: null,
                        units: null,
                        perunitPrice: null,
                        totalProductAmount: null,
                        discountAmount: null,
                        GST: null,
                        totalAmount: null
                    }],
                status: "Pending",
                remarks: {
                    seller: noOfInstallmentArray[i]['remarks'],
                    headseller: noOfInstallmentArray[i]['headsellerRemarks'],
                    finance: noOfInstallmentArray[i]['financeRemarks'] || "-"
                },
                transactionId: "",
            })
        }
        payloadPUT['installmentDetails'] = installmentPayloadDetails
        Axios.put(`${this.state.env['headSeller']}/bkah/edu/stufeeplan`, payloadPUT, {
            headers: { "Authorization": localStorage.getItem('auth_token') }
        }).then(res => {
            Alert.success(res.data.message)
            this.mailToParent();
            this.submitFinance(parseHandleClickData);
            this.setState({ loader: false, showOnboardPreview: false, activeTab: 0 }, () => {
                this.leadsAPI();
            });
        }).catch(err => {
            this.setState({ loader: false, showOnboardPreview: false });
        })

    }
    submitFinance = (parseHandleClickData) => {
        let statusPayload = {
            "appId": parseHandleClickData.applicationId,
            "accountStatus": "Finance Approved",
            "assignedTo": parseHandleClickData.assignedTo
        }
        Axios.put(`${this.state.env['zqBaseUri']}/edu/leads/${this.state.orgId}`, statusPayload, {
            headers: { "Authorization": localStorage.getItem('auth_token') }
        }).then(res => {
        })
    }
    mailToParent = () => {
        let { rowClickData, noOfInstallmentArray, handleClickData } = this.state
        let payload = {}
        for (let i = 0; i < noOfInstallmentArray.length; i++) {
            let installment = {}
            installment.percentage = noOfInstallmentArray[i]['percentage']
            installment.amount = this.formatCurrency(noOfInstallmentArray[i]['amount'])
            payload[`installment${i + 1}`] = installment
        }
        payload.info = {
            "studentName": `${this.state.rowClickData.student.firstName} ${this.state.rowClickData.student.lastName}`,
            "parentName": this.state.rowClickData.parent.name,
            "appId": this.state.rowClickData.applicationId,
            "amount": this.state.noOfInstallmentArray[0]['amount'],
            "class": this.state.selectedProgramType,
            // "parentEmail": this.state.rowClickData.parent.email,
            "parentEmail": 'justin.thomas@zenqore.com'
        }
        Axios.post(`${this.state.env['headSeller']}/zq/user/mailtoparent`, payload).then(res => {
            this.setState({ showOnboardPreview: false, loader: false }, () => {
                Alert.info('Email has been sent to parent')
                this.setState({ activeTab: 0 });
            });
        }).catch(err => {
            Alert.error('Something went Wrong. Please try again')
            this.setState({ showOnboardPreview: false, loader: false })
        })
    }

    closeModal = () => {
        this.setState({ openModal: false });
    };
    onSelectSeller = (value, item, event) => {
        let handleClickData = this.state.handleClickData['Application ID'] != undefined ? this.state.handleClickData['Application ID'] : this.state.applicationId;
        this.setState({ loader: true, selectedSeller: undefined });
        let currentUser = localStorage.getItem("user")
        if (currentUser == "Headseller") {
            let payload = {
                sellerName: "Sankar",
                appId: handleClickData
            }
            let data = {
                seller: value,
                appID: handleClickData
            }
            Axios.post(`${this.state.env['headSeller']}/zq/user/sellermail`, payload).then(res => {
                this.setState({ showSellerList: false, showOnboardPreview: false, openModal: false, selectedSeller: value }, () => {
                    Alert.info(`Assigned to ${value}`)
                    this.assignAPI(data);
                    this.setState({ showOnboardPreview: false, onboardData: [], loader: false });
                });
            }).catch("Something went wrong. Please try again.")
        }
    }
    assignAPI = async (data) => {
        let statusPayload = {
            "appId": data.appID,
            "accountStatus": "Assigned",
            "assignedTo": data.seller
        }
        const response = await Axios.put(`${this.state.env['zqBaseUri']}/edu/leads/${this.state.orgId}`, statusPayload, {
            headers: { "Authorization": localStorage.getItem('auth_token') }
        })
        this.leadsAPI();
    }
    onSelectProgramPlan = (value, item, event) => {
        let { getAmount, discountAmount, noOfInstallmentArray, brightKidsData } = this.state

        brightKidsData.map(item => {
            if (item.programName === value) {
                this.setState({ selectedProgramType: value, getAmount: item.totalFees, offeredDiscount: item.discount, offeredFees: item.offeredPrice, previousAmount: item.offeredPrice, totalAmount: item.offeredPrice, selectedNoofInstallment: item.noOfInstallment }, () => {
                    let percentage = (Number(discountAmount) * 100) / Number(getAmount)
                    for (let i = 0; i < item.installments.length; i++) {
                        noOfInstallmentArray[i].amount = item.installments[i]
                    }
                    this.setState({ allotedPercentage: Number(percentage).toFixed(2), noOfInstallmentArray })
                });

            }
        })
    }
    onDiscountChanges = (value, item, event) => {
        let { getAmount, discountAmount } = this.state
        this.setState({ selectedDiscountType: value });
        if (value === "Percentage") {
            let percentage = (Number(discountAmount) * 100) / Number(getAmount)
            this.setState({ percentageField: true, checkIsPercentage: true, allotedPercentage: percentage });
        } else {
            this.setState({ percentageField: false, checkIsPercentage: false });
        }
    }
    onDiscountChanges2 = (value, item, event) => {
        this.setState({ selectedDiscountType2: value, });
        if (value === "Percentage") {
            this.setState({ percentageField2: true, });
        } else {
            this.setState({ percentageField2: false });
        }
    }
    onCleanItem = (value, data) => {
    }
    onCleanProduct = (value, data) => {
    }
    onCleanSeller = (value, data) => {
        this.setState({ selectedProgramType: undefined, getAmount: 0, totalAmount: 0, previousAmount: 0 });
        this.onInstallmentChanges(1)
    }
    onClearDiscount = (value, data) => {
        let { totalAmount, getAmount } = this.state;
        this.setState({ selectedDiscountType: undefined, percentageField: false, checkIsPercentage: false, discountAmount: 0, totalAmount: getAmount, previousAmount: getAmount });
    }
    onClearDiscount2 = (value, data) => {
        this.setState({ selectedDiscountType2: undefined, percentageField2: false });
    }
    tabChangeInternal = (e, value) => {
        if (value == 0) {
            let { onboardStudentForm, onboardForm } = this.state
            let studentForm = []
            studentForm = onboardForm['Student Details']
            this.setState({ onboardStudentForm: [] }, () => {
                onboardStudentForm = studentForm
                this.setState({ onboardStudentForm })
            })
        }
        if (value == 1) {
            let { onboardParentForm, onboardForm } = this.state
            let parentForm = []
            parentForm = onboardForm['Parent Details']
            this.setState({ onboardParentForm: [] }, () => {
                onboardParentForm = parentForm
                this.setState({ onboardParentForm })
            })
        }
        if (value == 2) {
            let { onboardPaymentForm, onboardForm } = this.state
            let paymentDetails = []
            paymentDetails = onboardForm['Payment Details']
            this.setState({ onboardPaymentForm: [] }, () => {
                onboardPaymentForm = paymentDetails
                this.setState({ onboardPaymentForm })
            })
        }
        if (value == 3) {
            let { noOfInstallmentArray, itemDetails, response, totalAmount, getAmount, lastIndexAmount } = this.state
            let getTotalAmounnt = this.state.totalAmount;
            noOfInstallmentArray = []
            let todayDate = new Date()
            let nextMonth
            let count = 0
            // itemDetails.forEach(item => {
            //     if (item.price) {
            //         count = Number(count) + Number(item.price) * Number(item.units)
            //         return count
            //     }
            // })
            let firstIndexAmount
            let installmentArray = []
            installmentArray = this.state.noOfInstallmentArray
            if (this.state.discountAmountChange === true) {
                installmentArray.forEach((item, index) => {
                    if (index === (installmentArray.length - 1)) {
                        item.amount = Number(lastIndexAmount) - (Number(this.state.discountAmount) || 0)
                    }
                })
            }

            this.setState({
                noOfInstallmentArray: installmentArray
            }, () => {

                if (this.state.getAmountOnchange === false) {
                    for (let i = 0; i < this.state.noOfInstallmentArray.length; i++) {
                        nextMonth = i == 0 ? todayDate : nextMonth
                        let percentage = (Number(this.state.noOfInstallmentArray[i].amount) * 100) / Number(totalAmount)
                        percentage = Number(percentage).toFixed(2)
                        if (i > 0) {
                            nextMonth = new Date(new Date().setMonth(nextMonth.getMonth() + 1))
                        }
                        noOfInstallmentArray.push({
                            id: i,
                            percentage: percentage,
                            amount: this.state.noOfInstallmentArray[i].amount,
                            nextMonth: nextMonth,
                            description: this.state.noOfInstallmentArray[i].description,
                            remarks: this.state.noOfInstallmentArray[i].remarks,
                            financeRemarks: this.state.noOfInstallmentArray[i].financeRemarks,
                            headsellerRemarks: this.state.noOfInstallmentArray[i].headsellerRemarks,
                            status: this.state.noOfInstallmentArray[i].status
                        })
                    }

                    this.setState({ noOfInstallmentArray: noOfInstallmentArray })
                }
                else if (this.state.getAmountOnchange === true) {
                    for (let i = 1; i <= this.state.selectedNoofInstallment; i++) {
                        nextMonth = i == 1 ? todayDate : nextMonth
                        let Amount = Number(getAmount) / Number(this.state.selectedNoofInstallment)
                        let percentage = (Number(Amount) * 100) / Number(totalAmount)
                        percentage = Number(percentage).toFixed(2)
                        if (i > 1) {
                            nextMonth = new Date(new Date().setMonth(nextMonth.getMonth() + 1))
                        }

                        noOfInstallmentArray.push({
                            id: i,
                            percentage: percentage,
                            amount: Amount,
                            nextMonth: nextMonth,
                            description: this.state.noOfInstallmentArray[0].description,
                            remarks: this.state.noOfInstallmentArray[0].status,
                            financeRemarks: this.state.noOfInstallmentArray[0].financeRemarks,
                            status: this.state.noOfInstallmentArray[0].status,
                            headsellerRemarks: this.state.noOfInstallmentArray[0].headsellerRemarks

                        })
                    }
                    this.setState({ noOfInstallmentArray: noOfInstallmentArray })
                }
            })

        }
        this.setState({ activeTab: value })
    }
    onItemChanges = (value, index) => {
        let { totalAmount, itemDetails, previousAmount } = this.state
        itemDetails.forEach((item, ind) => {
            if (ind == index) {
                item.units = value
            }
        })
        let count = 0
        itemDetails.map(item => {
            count = Number(count) + Number(item.price) * Number(item.units)
            return count
        })

        totalAmount = Number(previousAmount) + Number(count)
        this.setState({ totalAmount })
    }
    onProductChanges = (value, event, idx) => {
        let { itemDetails, } = this.state
        itemDetails.forEach((item, index) => {
            if (index === idx) {
                item.price = event.price
                item.units = 1
                item.name = value
            }
        })
        this.setState({ itemDetails, }, () => {
            let { itemDetails, previousAmount, totalAmount } = this.state
            let count = 0
            itemDetails.map(item => {
                count = Number(count) + Number(item.price) * Number(item.units)
                return count
            })
            totalAmount = Number(previousAmount) + Number(count)
            this.setState({ totalAmount })
        });
    }
    onInstallmentChanges = (value, event) => {
        let { noOfInstallmentArray, response, lastIndexAmount } = this.state
        if (Number(value) !== 4) {
            let getTotalAmount = this.state.totalAmount;
            noOfInstallmentArray = []
            let listofInstallmentAmount = Number(getTotalAmount) / Number(value)
            let todayDate = new Date()
            let nextMonth
            let percentage = 100 / Number(value)
            percentage = Number(percentage).toFixed(2)
            for (let i = 1; i <= value; i++) {
                nextMonth = i == 1 ? todayDate : nextMonth
                if (i > 1) {
                    nextMonth = new Date(new Date().setMonth(nextMonth.getMonth() + 1))
                }
                noOfInstallmentArray.push({
                    id: i,
                    percentage: percentage,
                    amount: listofInstallmentAmount,
                    nextMonth: nextMonth,
                    description: noOfInstallmentArray.description,
                    remarks: noOfInstallmentArray.remarks,
                    financeRemarks: undefined,
                    status: "Planned"
                })

                lastIndexAmount = noOfInstallmentArray[noOfInstallmentArray.length - 1]['amount']
            }
            this.setState({ noOfInstallmentArray: noOfInstallmentArray, selectedNoofInstallment: value, totalPercentage: 100, lastIndexAmount }, () => {
                console.log("noOfInstallmentArray", this.state.noOfInstallmentArray, noOfInstallmentArray)
                // this.getDiscountAmounnt()


            })
        }
        else {
            let { noOfInstallmentArray, selectedProgramType, brightKidsData } = this.state
            noOfInstallmentArray = []
            brightKidsData.forEach(item => {
                if (item.programName === selectedProgramType) {
                    item.installments.forEach((instalmentItem, index) => {
                        noOfInstallmentArray.push({
                            id: index + 1,
                            percentage: 0,
                            description: "",
                            amount: instalmentItem,
                            nextMonth: new Date(),
                            discount: 0,
                            remarks: "",
                            financeRemarks: "",
                            status: "Planned"
                        })
                    })

                }
            })
            this.setState({ noOfInstallmentArray, selectedNoofInstallment: value })
        }
    }
    onSelectProgram = () => { }
    onCancel = () => {
        this.setState({ showOnboardPreview: false })
    }
    onSubmit = async () => {
        this.setState({ loader: true });
        let { rowClickData, noOfInstallmentArray, handleClickData } = this.state
        let parseHandleClickData = Object.keys(rowClickData).length === 0 ? JSON.parse(handleClickData.Item) : rowClickData
        let getPaidAmount = noOfInstallmentArray.filter(item => item.status !== "Paid")
        console.log('getPaidAmount', getPaidAmount)
        let payload = {
            organizationId: parseHandleClickData.organizationId,
            paymentDetails: {
                applicationId: parseHandleClickData.applicationId,
                studentRegId: "",
                programPlanHEDAId: this.state.selectedProgramType,
                plannedAmount: Number(this.state.totalAmount),
                plannedAmountBreakup: [{
                    feeType: "tuition",
                    amount: Number(this.state.totalAmount)
                }],
                paidAmount: Number(getPaidAmount[0]['amount']),
                paidAmountBreakup: [{
                    feeType: "tuition",
                    amount: Number(getPaidAmount[0]['amount'])
                }],
                pendingAmount: Number(this.state.totalAmount) - Number(getPaidAmount[0]['amount']),
                pendingAmountBreakup: [{
                    feeType: "tuition",
                    amount: Number(this.state.totalAmount) - Number(getPaidAmount[0]['amount'])
                }],
                currency: parseHandleClickData.currency.currency,
                forex: "",
                discountType: this.state.selectedDiscountType,
                discountPercentage: Number(this.state.allotedPercentage),
                discountAmount: Number(this.state.discountAmount),
                discountAmountBreakup: [{
                    feeType: 'tuition',
                    amount: Number(this.state.discountAmount)
                }],
                remarks: {
                    seller: this.state.remarks,
                    finance: this.state.financeRemarks,
                    headseller: this.state.headsellerRemarks
                }
            }
        }
        let installmentPayloadDetails = []
        for (let i = 0; i < noOfInstallmentArray.length; i++) {
            installmentPayloadDetails.push({
                label: `Installment ${i + 1}`,
                description: noOfInstallmentArray[i]["description"],
                dueDate: noOfInstallmentArray[i].nextMonth,
                penaltyStartDate: this.onNext7Days(noOfInstallmentArray[i]['nextMonth']),
                percentage: Number(noOfInstallmentArray[i]['percentage']),
                plannedAmount: Number(noOfInstallmentArray[i]['amount']),
                plalnedAmountBreakup: [{
                    feeType: 'tuition',
                    amount: Number(noOfInstallmentArray[i]['amount'])
                }],
                paidAmount: Number(noOfInstallmentArray[i]['amount']),
                paidAmountBreakup: [{
                    feeType: 'tuition',
                    amount: Number(noOfInstallmentArray[i]['amount'])
                }],
                pendingAmount: 0,
                pendingAmountBreakup: [{
                    feeType: "tuition",
                    amount: 0
                }],
                discountType: this.state.selectedDiscountType,
                discountPercentage: Number(this.state.allotedPercentage),
                discountAmount: Number(this.state.discountAmount),
                discountAmountBreakup: [{
                    feeType: 'tuition',
                    amount: Number(this.state.discountAmount)
                }],
                status: noOfInstallmentArray[i]['status'],
                remarks: {
                    seller: noOfInstallmentArray[i]['remarks'] || "-",
                    headseller: noOfInstallmentArray[i]['headsellerRemarks'] || "-",
                    finance: noOfInstallmentArray[i]['financeRemarks']
                },
                transactionId: "",
            })
        }
        payload['installmentDetails'] = installmentPayloadDetails
        let currentUser = localStorage.getItem("user")
        if (currentUser === "Headseller") {
            console.log("payload", payload)
            Axios.post(`${this.state.env['headSeller']}/bkah/edu/stufeeplan`, payload, {
                headers: { "Authorization": localStorage.getItem('auth_token') }
            }).then(res => {
                Alert.success(res.data.message)
                this.submitHeadseller(parseHandleClickData);
                this.setState({ loader: false, showOnboardPreview: false }, () => {
                    this.leadsAPI();
                });
            }).catch(err => {
                this.setState({ loader: false })
                Alert.error("Something went wrong! Please try again.")
            })
        } else if (currentUser === "Seller") {
            try {
                const response = Axios.put(`${this.state.env['headSeller']}/bkah/edu/stufeeplan`, payload, {
                    headers: { "Authorization": localStorage.getItem('auth_token') }})
                    Alert.success(response.data.message)
                    this.submitSeller(parseHandleClickData);
                    this.setState({ loader: false, showOnboardPreview: false }, () => {
                        this.leadsAPI();
                    });
            }
            catch (err) {
                this.setState({ loader: false })
                Alert.error("Something went wrong! Please try again.")
            }
        }
        // let sellerPayload = {
        //     "financeMember": "Mehul",
        //     "studentName": rowClickData.student.firstName,
        //     "appId": rowClickData.applicationId
        // }
        // Axios.post(`${this.state.env['headSeller']}/zq/user/financemail`, sellerPayload).then(res => Alert.info("Request sent finance for approval."))
        //     .catch(err => console.log(err))

    }
    submitHeadseller = async (parseHandleClickData) => {
        let statusPayload = {
            "appId": parseHandleClickData.applicationId,
            "accountStatus": "Waiting Approval",
            "assignedTo": "Headseller"
        }
        try {
            const response =  Axios.put(`${this.state.env['zqBaseUri']}/edu/leads/${this.state.orgId}`, statusPayload, {
                headers: { "Authorization": localStorage.getItem('auth_token') }
            })
        }
        catch (err) {
            
        }
      
    }
    submitSeller = async (parseHandleClickData) => {
        let statusPayload = {
            "appId": parseHandleClickData.applicationId,
            "accountStatus": "Waiting Approval",
            "assignedTo": "Seller"
        }
        try {
            const response = Axios.put(`${this.state.env['zqBaseUri']}/edu/leads/${this.state.orgId}`, statusPayload, {
                headers: { "Authorization": localStorage.getItem('auth_token') }
            })
        }
        catch (err) {
            
      }
    }
    getAmount = (e) => {
        let getAmount = e
        this.setState({ getAmount: getAmount, discountAmount: 0, totalAmount: getAmount, getAmountOnchange: true })
    }
    getDiscountAmounnt = (e) => {
        let currentUser = localStorage.getItem('user')
        if (this.state.selectedDiscountType != undefined) {
            let { totalAmount, getAmount, offeredFees, noOfInstallmentArray, previousAmount } = this.state
            let discountAmount = e
            totalAmount = Number(previousAmount) - Number(discountAmount)
            this.setState({ discountAmount: discountAmount, discountAmountChange: true, totalAmount, })
        } else {
            Alert.info("Please select a discount type first.")
        }
    }
    getPercentage = (e) => {
        let { totalAmount, getAmount, offeredFees } = this.state
        let allotedPercentage = e
        let percentageAmt = Number(offeredFees) * (Number(allotedPercentage) / 100)
        totalAmount = Number(offeredFees) - Number(percentageAmt)
        this.setState({ discountAmount: percentageAmt, allotedPercentage: allotedPercentage, totalAmount, previousAmount: totalAmount })
    }
    changeAmount = (value, valueIndex) => {
        if (valueIndex !== 0) {
            let { noOfInstallmentArray, totalAmount, selectedNoofInstallment } = this.state
            let installemntAmtStore = this.state.installemntAmtStore
            installemntAmtStore[0] = noOfInstallmentArray[0].amount
            installemntAmtStore[valueIndex] = Number(value)
            if (Number(totalAmount) >= Number(value)) {
                this.setState({ installemntAmtStore }, () => {
                    let calcAmt = 0;
                    Object.keys(this.state.installemntAmtStore).map(ims => {
                        calcAmt += this.state.installemntAmtStore[ims]
                    })
                    let remainingAmount = Number(totalAmount) - Number(calcAmt)
                    let itemInstallmentAmt = (remainingAmount / (Number(selectedNoofInstallment) - Object.keys(this.state.installemntAmtStore).length))
                    noOfInstallmentArray.forEach((item, index) => {
                        if (index === valueIndex) {
                            item.amount = this.state.installemntAmtStore[index] == undefined ? Number(value).toFixed(2) : this.state.installemntAmtStore[index]
                            item.percentage = this.state.installemntAmtStore[index] == undefined ? ((Number(value) / totalAmount) * 100) : ((this.state.installemntAmtStore[index] / totalAmount) * 100)
                        }
                        else {
                            item.amount = this.state.installemntAmtStore[index] == undefined ? itemInstallmentAmt.toFixed(2) : this.state.installemntAmtStore[index]
                            item.percentage = this.state.installemntAmtStore[index] == undefined ? ((itemInstallmentAmt / totalAmount) * 100) : ((this.state.installemntAmtStore[index] / totalAmount) * 100)
                        }
                    })
                    this.setState({ noOfInstallmentArray: noOfInstallmentArray })
                })
            }
            else {
                Alert.info('Invalid Amount')
            }
        }

    }
    changePercentage = (value, valueIndex) => {
        let { noOfInstallmentArray, totalAmount, selectedNoofInstallment, totalPercentage } = this.state
        let installemntPerStore = this.state.installemntPerStore
        installemntPerStore[valueIndex] = value
        // if (totalPercentage >= value) {
        this.setState({ installemntPerStore }, () => {
            let currentPercentageAmount = (Number(value) * Number(totalAmount)) / 100
            let calcAmt = 0;
            Object.keys(this.state.installemntPerStore).map(ims => {
                calcAmt += Number(this.state.installemntPerStore[ims])
            })
            let remainingPercentage = Number(100) - Number(calcAmt)
            let itemInstallmentPercentage = (remainingPercentage / (Number(selectedNoofInstallment) - Object.keys(this.state.installemntPerStore).length))
            noOfInstallmentArray.forEach((item, index) => {
                if (valueIndex !== 0) {
                    if (index === valueIndex) {
                        item.amount = this.state.installemntPerStore[index] != undefined ? Number(currentPercentageAmount).toFixed(2) : this.state.noOfInstallmentArray[index]['amount']
                        item.percentage = this.state.installemntPerStore[index] == undefined ? Number(value) : this.state.installemntPerStore[index]
                    } else {
                        item.amount = this.state.installemntPerStore[index] == undefined ? ((Number(totalAmount) * Number(itemInstallmentPercentage)) / 100) : this.state.noOfInstallmentArray[index]['amount']
                        item.percentage = this.state.installemntPerStore[index] == undefined ? itemInstallmentPercentage : this.state.installemntPerStore[index]
                    }
                }

            })
            this.setState({ noOfInstallmentArray: noOfInstallmentArray })
        });
    }
    changeDescription = (value, index) => {
        let { noOfInstallmentArray } = this.state;
        noOfInstallmentArray.forEach((item, valueIndex) => {
            if (index === valueIndex) {
                item.description = value
            }
        })
        this.setState({ noOfInstallmentArray }, () => {
        })
    }
    // Installment Details Remarks
    changeHeadsellerRemarks = (value, index) => {
        let { noOfInstallmentArray } = this.state;
        noOfInstallmentArray.forEach((item, valueIndex) => {
            if (index === valueIndex) {
                item.headsellerRemarks = value
            }
        })
        this.setState({ noOfInstallmentArray })
    }
    changeRemarks = (value, index) => {
        let { noOfInstallmentArray } = this.state;
        noOfInstallmentArray.forEach((item, valueIndex) => {
            if (index === valueIndex) {
                item.remarks = value
            }
        })
        this.setState({ noOfInstallmentArray })
    }
    changeFinanceRemarks = (value, index) => {
        let { noOfInstallmentArray } = this.state;
        noOfInstallmentArray.forEach((item, valueIndex) => {
            if (index === valueIndex) {
                item.financeRemarks = value
            }
        })
        this.setState({ noOfInstallmentArray })
    }
    // Payment Details Remarks
    changeHeadsellerRemarks1 = (value, index) => {
        this.setState({ headsellerRemarks: value })
    }
    changeRemarks1 = (value, index) => {
        this.setState({ remarks: value })
    }
    changeFinanceRemarks1 = (value, index) => {
        this.setState({ financeRemarks: value })
    }

    onCheckStudent = (item) => {
        return null
    }
    showPreview = () => {
        this.setState({ showPreview: true, openModal1: true });
        let PaymentFormData = [];
        PaymentFormData.push({
            date: new Date(),
            amount: this.state.noOfInstallmentArray[0]['amount'],
            cardNumber: "5123XXXXXXXX2346",
            creditDebit: "Credit",
            cardType: "Visa",
            nameOnCard: "Test",
            transactionNumber: "B40605489E8D93C353F16FC0FEC56165",
            remarks: "-"
        })
        this.setState({ PaymentFormData: PaymentFormData });
    }
    closeModal1 = () => {
        this.setState({ openModal1: false });
    };
    onSelectDate = (value, selectIndex, event) => {
        let { noOfInstallmentArray } = this.state
        noOfInstallmentArray.forEach((item, index) => {
            if (index === selectIndex) {
                item.nextMonth = value
            }
        })
        this.setState({ noOfInstallmentArray: noOfInstallmentArray })
    }
    openAssignModel = () => {
        this.setState({ showSellerList: true, openModal: true });

    }

    onNext7Days = (itemData) => {
        let next7days = new Date(new Date().setDate(itemData.getDate() + 7))
        return next7days
    }

    addItems = (idx, items) => {
        let { itemDetails } = this.state;
        if (itemDetails.length <= 3) {
            let nextID = items.id + 1
            itemDetails.push({ id: nextID })
            this.setState({ itemDetails })
        } else {
            Alert.info("Only 4 Products are available.")
        }
    }
    removeItems = (idx) => {
        let { itemDetails, offeredFees, discountAmount, totalAmount } = this.state;
        if (itemDetails.length >= 2) {
            itemDetails.splice(idx, 1)
            let count = 0
            itemDetails.forEach((item, idx) => {
                console.log(idx)
                if (item.price) {
                    count = Number(totalAmount) - Number(item.price) * Number(item.units)
                    return count
                }
            })
            this.setState({
                itemDetails,
                totalAmount: count
            });
        } else if (itemDetails.length === 1) {
            let discountedAmount = discountAmount != undefined ? discountAmount : 0
            let amountWithChanges = Number(offeredFees) - Number(discountedAmount)
            itemDetails = []
            itemDetails.push({ id: 1 })
            this.setState({ totalAmount: amountWithChanges, itemDetails })
        }
    }
    onPaginationChange2 = (page, limit, datas) => {
        this.setState({ isLoader: true })
        let data = JSON.parse(datas)
        this.setState({ page: page, limit: limit, onboardData: [] }, () => {
            let current_page = Number(page);
            let perPage = Number(limit);
            let total_pages = Math.ceil(data.length / perPage);
            current_page = total_pages < current_page ? total_pages : current_page
            let lastIndex = current_page * perPage;
            let startIndex = lastIndex - perPage;
            let paginatedItems = []
            paginatedItems = data.slice(startIndex, lastIndex)
            this.setState({ previewpage: current_page, previewlimit: perPage, previewtotalPages: total_pages, previewtotalRecord: data.length });
            this.setState({ onboardData: paginatedItems, isLoader: false });
        });
    };

    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.leadsAPI()
        });
    };

    render() {
        return (
            <React.Fragment>
                {this.state.loader ? <Loader /> : null}
                {!this.state.showOnboardPreview ?
                    <React.Fragment>
                        <React.Fragment>
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Onboard Users</p>
                            </div>
                        </React.Fragment>
                        <div className="invoice-page-wrapper new-table-wrap ">
                            {this.state.containerNav == undefined ? null : (
                                <ContainerNavbar
                                    containerNav={this.state.containerNav}
                                    onAddNew={() => this.newOnboard()}
                                />
                            )}
                            <div className="table-wrapper task-table-wrapper" style={{ marginTop: "10px" }}>
                                {this.state.onboardData.length == 0 ? (
                                    <p className="noprog-txt">{this.state.noProg}</p>
                                ) : (<React.Fragment>
                                    <div className="onboard-table-main">
                                        <ZqTable
                                            data={this.state.onboardData}
                                            rowClick={(item) => { this.onPreviewOnboard(item); }}
                                            onRowCheckBox={(item) => { this.onCheckStudent(item) }}
                                            handleActionClick={(item, index, hd, name) => { this.handleActionClick(item, index, hd, name) }}
                                        />
                                    </div>
                                </React.Fragment>)}
                            </div>
                            <div>
                                {this.state.onboardData.length !== 0 ?
                                    <PaginationUI
                                        total={this.state.totalRecord}
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        currentPage={this.state.page}
                                    />
                                    : null}
                            </div>
                        </div>
                    </React.Fragment>
                    : <React.Fragment>
                        <React.Fragment>
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.setState({ showOnboardPreview: false, activeTab: 0 }) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title" onClick={() => { this.setState({ showOnboardPreview: false, activeTab: 0 }) }}> Onboard Listing | {this.state.rowClickData.applicationId}</p>
                            </div>
                        </React.Fragment>
                        <div className="tab-wrapper" style={{ width: "100%" }}>
                            <Tabs
                                value={this.state.activeTab}
                                indicatorColor="primary"
                                textColor="primary"
                                onChange={this.tabChangeInternal}>
                                {this.state.tabData.map((tab, tabIndex) => {
                                    return <Tab label={tab} />
                                })}
                            </Tabs>
                            {localStorage.getItem("user") == "Finance" ? (
                                <div className="onboard-btn-group">
                                    <Button className="confirm-payment-btn cancel-onboard" onClick={this.onReject} disabled={this.state.disableButton === true ? true : false}>Reject</Button>
                                    <Button className="confirm-payment-btn submit-onboard" onClick={this.onAccept} disabled={this.state.disableButton === true ? true : false}>Approve</Button>
                                </div>)
                                :
                                localStorage.getItem("user") == "Headseller" ?
                                    (<div className="onboard-btn-group">
                                        <Button className="confirm-payment-btn cancel-onboard" onClick={this.openAssignModel} disabled={this.state.disableButton === true ? true : false}>Assign</Button>
                                        <Button className="confirm-payment-btn submit-onboard" onClick={this.onSubmit} disabled={this.state.disableButton === true ? true : false}>Submit</Button>
                                    </div>)
                                    :
                                    (<div className="onboard-btn-group">
                                        <Button className="confirm-payment-btn cancel-onboard" onClick={this.onCancel} disabled={this.state.disableButton === true ? true : false}>Cancel</Button>
                                        <Button className="confirm-payment-btn submit-onboard" onClick={this.onSubmit} disabled={this.state.disableButton === true ? true : false}>Submit</Button>
                                    </div>)
                            }
                            <div className="preview-list-tab-data">
                                {this.state.activeTab === 0 ?
                                    <React.Fragment>
                                        <div className="receive-payment-table student-form" style={{ position: "relative", top: "2vh" }}>
                                            {this.state.onboardStudentForm.length > 0 ? <ZenForm inputData={this.state.onboardStudentForm} /> : null}
                                        </div>
                                    </React.Fragment>
                                    : this.state.activeTab === 1 ?
                                        <React.Fragment>
                                            <div className="receive-payment-table student-form" style={{ position: "relative", top: "2vh" }}>
                                                {this.state.onboardParentForm.length > 0 ? <ZenForm inputData={this.state.onboardParentForm} /> : null}
                                            </div>
                                        </React.Fragment>
                                        : this.state.activeTab === 2 ?
                                            <React.Fragment>

                                                <div className="receive-amount-table onBoradTable" style={{ overflowY: "auto", width: "100%", padding: "20px", top: "-36px" }}>
                                                    <p className="payment-details-header">Payment Details</p>
                                                    <table className="receive-pay-tableFormat" style={{ width: "75vh", margin: "20px" }}>
                                                        <tbody>
                                                            <tr>
                                                                <td style={{ padding: "0px 20px", width: "55vh" }}>Program</td>
                                                                <td style={{ padding: "0px", borderRadius: "0px" }}><SelectPicker className="SelectPicker-seller"
                                                                    data={this.state.programData}
                                                                    placeholder="Select Program Plan"
                                                                    onSelect={(value, item, event) => this.onSelectProgramPlan(value, item, event, this.state.inputData)}
                                                                    onClean={(value) => this.onCleanSeller(value, this.state.inputData)}
                                                                    defaultValue={this.state.selectedProgramType}
                                                                    style={{ width: '100%', marginRight: "10px", borderRadius: "0px" }} /></td>
                                                            </tr>
                                                            <tr>
                                                                <td style={{ padding: "0px 20px", width: "55vh" }}> Fees </td>
                                                                <td style={{ padding: "0px" }}><Input placeholder="Enter Amount" value={this.state.getAmount != undefined ? Number(this.state.getAmount).toFixed(2) : "-"} readOnly={true} style={{ borderRadius: "0px" }} /></td>
                                                            </tr>
                                                            <tr>
                                                                <td style={{ padding: "0px 20px", width: "55vh" }}>Offered Discount</td>
                                                                <td style={{ padding: "0px" }}><Input placeholder="Enter Discount" value={this.state.offeredDiscount != undefined ? Number(this.state.offeredDiscount).toFixed(2) : "-"} readOnly={true} style={{ borderRadius: "0px" }} /></td>
                                                            </tr>
                                                            <tr>
                                                                <td style={{ padding: "0px 20px", width: "55vh" }}> Offered Fees </td>
                                                                {console.log(this.state.offeredFees)}
                                                                <td style={{ padding: "0px" }}><Input placeholder="Enter Total" value={this.state.offeredFees != undefined ? Number(this.state.offeredFees).toFixed(2) : "-"} style={{ borderRadius: "0px" }} /></td>
                                                            </tr>
                                                            <tr>
                                                                <td style={{ padding: "0px 20px", width: "55vh" }}> Discount Type</td>
                                                                <td style={{ padding: "0px" }}> <SelectPicker className="SelectPicker-seller"
                                                                    data={this.state.discountType}
                                                                    placeholder="Select Discount Type"
                                                                    onSelect={(value, item, event) => this.onDiscountChanges(value, item, event, this.state.inputData)}
                                                                    defaultValue={this.state.selectedDiscountType}
                                                                    onClean={(value) => this.onClearDiscount(value, this.state.inputData)}
                                                                    style={{ width: '100%', marginRight: "10px", borderRadius: "0px" }} />
                                                                </td>
                                                            </tr>
                                                            {this.state.percentageField ?
                                                                <tr>
                                                                    <td style={{ padding: "0px 20px", width: "55vh" }}> Percentage %</td>
                                                                    <td style={{ padding: "0px" }}><Input placeholder="Enter Percentage" onChange={this.getPercentage} value={this.state.allotedPercentage} type="number" placeholder="Enter Discount Percentage" style={{ borderRadius: "0px" }} /></td>
                                                                </tr>
                                                                : null}
                                                            <tr>
                                                                <td style={{ padding: "0px 20px", width: "55vh" }}> Discount Amount </td>
                                                                <td style={{ padding: "0px" }}><Input placeholder="Enter Discount" onChange={this.getDiscountAmounnt} value={this.state.discountAmount} type="number" placeholder="Enter Discount" readOnly={this.state.checkIsPercentage} style={{ borderRadius: "0px" }} /></td>
                                                            </tr>
                                                            {/* Product List UI Code */}
                                                            {/* {this.state.itemDetails.map((item, idx) => {
                                                                console.log('itemDetails', this.state.itemDetails)
                                                                return (<React.Fragment>
                                                                    <tr className="item-group">
                                                                        <td style={{ padding: "0px", display: "flex", lineHeight: "37px", width: "55vh" }}>
                                                                            <SelectPicker className="SelectPicker-seller"
                                                                                data={this.state.productList}
                                                                                placeholder="Select Product"
                                                                                onSelect={(value, item) => this.onProductChanges(value, item, idx,)}
                                                                                onClean={(value) => this.onCleanProduct(value, this.state.inputData)}
                                                                                value={item.name}
                                                                                style={{ width: '100%', borderRadius: "0px" }} />
`                                                                            <SelectPicker className="SelectPicker-seller"
                                                                                data={this.state.itemList}
                                                                                placeholder="Units"
                                                                                value={item.units}
                                                                                onSelect={(value) => this.onItemChanges(value, idx)}
                                                                                onClean={(value) => this.onCleanItem(value, this.state.inputData)}
                                                                                defaultValue={this.state.defaultUnit}
                                                                                style={{ width: '43%', borderRadius: "0px" }} />
                                                                        </td>
                                                                        <td style={{ padding: "0px", width: 295 }}>
                                                                            <Input placeholder="Product Price" value={item.price} style={{ borderRadius: "0px", width: "100%" }} />
                                                                        </td>
                                                                        <td style={{ border: "none", boxShadow: "none" }}>
                                                                            <CloseIcon className="remove-item" onClick={() => { this.removeItems(idx) }} />
                                                                        </td>
                                                                        {idx == this.state.itemDetails.length - 1 ?
                                                                            <td style={{ border: "none", boxShadow: "none" }} ><button className="submit-onboard" onClick={() => this.addItems(idx, item)}>Add Product</button></td>
                                                                            : null}
                                                                    </tr>
                                                                </React.Fragment>)
                                                            })} */}
                                                            <tr>
                                                                <td style={{ padding: "0px 20px", width: "55vh" }}> Total Fees </td>
                                                                <td style={{ padding: "0px" }}><Input placeholder="Enter Total" value={this.state.totalAmount != undefined ? Number(this.state.totalAmount).toFixed(2) : "-"} style={{ borderRadius: "0px" }} /></td>
                                                            </tr>
                                                            <tr>
                                                                <td style={{ padding: "0px 20px", width: "55vh" }}> No. of Installment </td>
                                                                <td style={{ padding: "0px", width: 295 }}>

                                                                    <SelectPicker className="SelectPicker-seller"
                                                                        data={this.state.noOfInstallment}
                                                                        placeholder="Select No. of Installments"
                                                                        onSelect={(value, item, event) => this.onInstallmentChanges(value, item, event, this.state.inputData)}
                                                                        onClean={(value) => this.onCleanSeller(value, this.state.inputData)}
                                                                        defaultValue={this.state.selectedNoofInstallment}
                                                                        style={{ width: '100%', marginRight: "10px", borderRadius: "0px" }} />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style={{ padding: "0px 20px", width: "55vh" }}>Headseller Remarks </td>
                                                                <td style={{ padding: "0px", width: 295 }}>
                                                                    <Input placeholder="Enter Remarks" value={this.state.headsellerRemarks}
                                                                        onChange={(value) => { this.changeHeadsellerRemarks1(value) }}
                                                                        disabled={localStorage.getItem("user") == "Headseller" ? false : true}
                                                                        style={{ borderRadius: "0px" }} />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style={{ padding: "0px 20px", width: "55vh" }}>Seller Remarks </td>
                                                                <td style={{ padding: "0px", width: 295 }}>
                                                                    <Input placeholder="Enter Remarks" value={this.state.remarks}
                                                                        onChange={(value) => { this.changeRemarks1(value) }}
                                                                        disabled={localStorage.getItem("user") == "Seller" ? false : true}
                                                                        style={{ borderRadius: "0px" }} />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style={{ padding: "0px 20px", width: "55vh" }}>Finance Remarks </td>
                                                                <td style={{ padding: "0px", width: 295 }}>
                                                                    <Input placeholder="Enter Remarks" value={this.state.financeRemarks}
                                                                        onChange={(value) => { this.changeFinanceRemarks1(value) }}
                                                                        disabled={localStorage.getItem("user") == "Finance" ? false : true}
                                                                        style={{ borderRadius: "0px" }} />
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </React.Fragment> : this.state.activeTab === 3 ? <>
                                                <div className="receive-amount-table onBoradTable">
                                                    <React.Fragment>
                                                        <div>
                                                            <p className="payment-details-header">Installment Details</p>
                                                            <table className="receive-pay-tableFormat" style={{ width: "97%", margin: "20px" }}>
                                                                <thead className="receive-pay-tableFormatHead">
                                                                    <th style={{ width: "150px" }}>Installment No.</th>
                                                                    <th style={{ width: "295px" }}>Description</th>
                                                                    <th style={{ width: "175px" }}>Due Date</th>
                                                                    <th style={{ width: "130px" }}>Percentage</th>
                                                                    <th style={{ width: "200px" }}>Amount</th>
                                                                    <th style={{ width: "400px" }}>Headseller Remarks</th>
                                                                    <th style={{ width: "400px" }}>Seller Remarks</th>
                                                                    <th style={{ width: "400px" }}>Finance Remarks</th>
                                                                    <th style={{ width: "95px" }}>Status</th>
                                                                </thead>
                                                                <tbody>
                                                                    {this.state.noOfInstallmentArray.map((item, index) => {
                                                                        return (
                                                                            <tr onClick={item.status === "Paid" ? this.showPreview : null}>
                                                                                <td style={{ padding: "0px 20px" }}>Installment {index + 1}</td>
                                                                                <td style={{ padding: "0px" }}>
                                                                                    <Input style={{ width: "100%", borderRadius: "0px" }} placeholder="Enter Description"
                                                                                        type="text" onChange={(value) => { this.changeDescription(value, index) }}
                                                                                        value={item.description}
                                                                                    />
                                                                                </td>
                                                                                <td style={{ padding: "0px" }}>
                                                                                    <DatePicker oneTap style={{ width: "100%", borderRadius: "0px" }}
                                                                                        onSelect={(value, item, event) => this.onSelectDate(value, index)}
                                                                                        value={item.nextMonth}
                                                                                        readOnly={item.status === "Paid" ? true : false}
                                                                                        format='DD/MM/YYYY' />
                                                                                </td>
                                                                                <td style={{ padding: "0px" }}>
                                                                                    <Input style={{ width: "100%", borderRadius: "0px" }} placeholder="Enter Description"
                                                                                        type="number" onChange={(value) => { this.changePercentage(value, index) }}
                                                                                        readOnly={item.status === "Paid" ? true : false}
                                                                                        value={Number(item.percentage).toFixed(2)} />
                                                                                </td>
                                                                                <td style={{ padding: "0px" }}>
                                                                                    <Input style={{ width: "100%", borderRadius: "0px" }}
                                                                                        placeholder="Enter Amount" type="number"
                                                                                        onChange={(value) => { this.changeAmount(value, index) }}
                                                                                        readOnly={item.status === "Paid" ? true : false}
                                                                                        value={Number(item.amount)} />
                                                                                </td>
                                                                                <td style={{ padding: "0px" }}>
                                                                                    <Input style={{ width: "100%", borderRadius: "0px" }}
                                                                                        placeholder="Enter Headseller Remarks" type="text"
                                                                                        onChange={(value) => { this.changeHeadsellerRemarks(value, index) }}
                                                                                        readOnly={localStorage.getItem("user") == "Headseller" ? false : true}
                                                                                        value={item.headsellerRemarks} />
                                                                                </td>
                                                                                <td style={{ padding: "0px" }}>
                                                                                    <Input style={{ width: "100%", borderRadius: "0px" }}
                                                                                        placeholder="Enter Seller Remarks" type="text"
                                                                                        onChange={(value) => { this.changeRemarks(value, index) }}
                                                                                        readOnly={localStorage.getItem("user") == "Seller" ? false : true}
                                                                                        disabled={item.status === "Paid" ? true : false}
                                                                                        value={item.remarks} />
                                                                                </td>
                                                                                <td style={{ padding: "0px" }}>
                                                                                    <Input style={{ width: "100%", borderRadius: "0px" }}
                                                                                        placeholder="Enter Finance Remarks" type="text"
                                                                                        onChange={(value) => { this.changeFinanceRemarks(value, index) }}
                                                                                        readOnly={localStorage.getItem("user") === "Finance" ? false : true}
                                                                                        disabled={item.status === "Paid" ? true : false}
                                                                                        value={item.financeRemarks} />
                                                                                </td>
                                                                                <td className={`Status-${item.status}`}>
                                                                                    <span>{item.status}</span>
                                                                                </td>
                                                                            </tr>
                                                                        )
                                                                    })}
                                                                    <tr>
                                                                        <td style={{ fontSize: 14 }}>Total</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td style={{ fontSize: 14 }}> 100</td>
                                                                        <td style={{ fontSize: 14 }}>{Number(this.state.totalAmount).toFixed(2)}</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </React.Fragment>
                                                </div>
                                            </> : null}
                            </div>
                        </div>
                    </React.Fragment>}
                {
                    this.showPreview ? <React.Fragment>
                        <Modal className="reconcile-preview-payment-wrap" style={{ top: "5%", borderRadius: "2px" }} onHide={this.closeModal1} size="xs" show={this.state.openModal1}>
                            <Modal.Header>
                                <Modal.Title></Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="bodyContent" style={{ paddingBottom: "10px", marginTop: "10px" }} >
                                <Payment
                                    StudentRegId={this.state.StudentRegId}
                                    appId={this.state.rowClickData.applicationId}
                                    StudentName={this.state.StudentName}
                                    StudentClass={this.state.StudentClass}
                                    paymentType={this.state.paymentType}
                                    PaymentFormData={this.state.PaymentFormData}
                                    onPaymentPreview={true}
                                />
                            </Modal.Body>
                        </Modal>
                    </React.Fragment> : null
                }
                {
                    this.state.showSellerList ? <React.Fragment>
                        <Modal className="show-seller-list-wrap" style={{ top: "5%", borderRadius: "2px" }} onHide={this.closeModal} size="xs" show={this.state.openModal}>
                            <Modal.Header>
                                <Modal.Title></Modal.Title>
                            </Modal.Header>
                            <Modal.Body className="bodyContent" style={{ paddingBottom: "10px", marginTop: "10px" }} >
                                <div className="select-picker-main" style={{ marginTop: "0px" }}>
                                    <p className="payment-details-header" style={{ padding: "0px", paddingBottom: "5px" }}>Select a Seller: </p>
                                    <SelectPicker className="SelectPicker-seller"
                                        data={this.state.selectSeller}
                                        placeholder="Select Seller"
                                        onSelect={(value, item, event, data) => this.onSelectSeller(value, item, event, this.state.inputData)}
                                        onClean={(value) => this.onCleanSeller(value, this.state.inputData)}
                                        style={{ width: '100%', marginRight: "10px", borderRadius: "0px" }} />
                                </div>
                            </Modal.Body>
                        </Modal>
                    </React.Fragment> : null
                }

            </React.Fragment >)
    }
}
export default withRouter(Onboard)