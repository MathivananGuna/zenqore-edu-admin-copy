import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Recaptcha from 'react-recaptcha';
import Axios from 'axios';
import { withRouter } from 'react-router-dom';
import ZenForm from '../input/form';
import loginForm from './login.json'
import "./../../scss/login.scss";
import Snackbar from '@material-ui/core/Snackbar';
import Button from '@material-ui/core/Button';
import Loader from '../../utils/loader/loaders';
import zenqoreHome from '../../assets/images/welcomepage.svg';
import ZqsmallLogo from '../../assets/images/small_zqlogo.svg';
import ZqTextLogo from '../../assets/images/zenqore_text.svg';
import OrangeCrcle from '../../assets/images/orange-circle.svg';
import greenCircle from '../../assets/images/greenCircle.svg';
import smallGreenCircle from '../../assets/images/greenCircle2.svg';
import Ken42Logo from '../../assets/images/ken42.png';
import VkgiLogo from '../../assets/images/vkgi_logo.png';
import { GoogleLogin } from 'react-google-login';
import { refreshTokenSetup } from '../../utils/helpers/refreshtoken';
import History from '../../utils/helpers/browserhistory';
const Cryptr = require('cryptr');
const cryptr = new Cryptr('ZqSecretKey');
let configURL = "https://devapifeecollection.zenqore.com/initiate/config"
class LoginComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: '',
      userInput: '',
      env: JSON.parse(localStorage.getItem('env')),
      loginFormData: [],
      snackBar: false,
      isLoader: false,
      vijayKiranLogo: 'https://supportings.blob.core.windows.net/zenqore-supportings/vkgi.png',
      clientDetails: {

      },
      SRMLogo: 'https://supportings.s3.amazonaws.com/Picture2.png'
    }

  }

  componentWillMount() {
    let instituteName = localStorage.getItem("baseURL")
    Axios.get(`${this.state.env["ken42"]}/initiate/config/institute?nameSpace=${instituteName}`)
      .then(response => {
        console.log(response)
        let clientDetails = response.data.data
        console.log(clientDetails)
        localStorage.setItem('clientName', clientDetails.client)
        this.setState({ clientDetails: clientDetails })
      })
  }
  componentDidMount() {
    //invite accountant
    if (window.location.href.includes('?')) {
      let { newpath } = this.state;
      let emailLocation = window.location.href;
      const value = emailLocation.split("?")
      let inviteId = value[1].split('=')
      this.setState({ inviteId: inviteId, newpath: value[0] })
      this.props.history.push(newpath)
      this.setState({ env: JSON.parse(localStorage.getItem('env')) })
      localStorage.setItem("inviteId", inviteId[1].split('#')[0]);
    }

    this.setState({ env: JSON.parse(localStorage.getItem('env')) })
    let loginFormData = []
    let automation = localStorage.getItem('automation') != undefined ? localStorage.getItem('automation') : "false"

    loginForm.map(item => {
      if (automation == "true" && item.category == 'recaptcha') {
        item['validation'] = true
        item['error'] = false
        loginFormData.push(item)
      } else {
        item['error'] = false
        loginFormData.push(item)
      }
    })
    this.setState({ loginFormData: loginFormData })
  }

  signupProcess = () => {
    this.props.history.push(`/${localStorage.getItem('baseURL')}/register`)
  }

  // onInternalLogin = () => {
  //   let payload = {
  //     authToken : localStorage.getItem('gsuite_authToken'),
  //     loginClient:'google'
  //   }
  //   Axios.post(`http://52.140.12.132:3000/config/institute/internalLogin`,payload)
  //   .then(response => {
  //     console.log(response)

  //   })
  // }

  onSubmit = (data, item, fd, format) => {
    console.log('***data***', data.email.value)
    let userInput = data.email.value;
    localStorage.setItem("tempEmail",userInput)
    // if (data.email.value == "fc.admin.di.all@zenoqre.com" || data.email.value == "fc.staff.f1@zenqore.com") {

    // }

    let payload = { "username": "fc.admin.euks@zenqore.com" }
    let apiDetails = item['apiDetails']['0']
    localStorage.setItem('channel', payload.username)
    let baseURL = localStorage.getItem('baseURL')
    this.setState({ isLoader: true });
    Axios.post(`${this.state.env[apiDetails['baseUri']]}/${apiDetails['apiUri']}`, payload)
      .then(response => {
        this.setState({ isLoader: false });
        if (format == 'ph') {
          this.props.history.push({
            pathname: `${baseURL}/otp`, userInput: userInput
          })
        } else {
          this.props.history.push({
            pathname: `/${baseURL}/zqsignin`, userInput: userInput
          })
        }
      },
        (error) => {
          if (error.response.status === 400 || error.response.status === 409) {
            if (error.response.data.message.includes("Please register")) {
              this.setState({ isLoader: false });
              this.setState({ error: "You are not a registered user. Please contact your administrator.", snackBar: true, userInput: userInput });
            }
            setTimeout(() => {
              this.setState({ snackBar: false })
            }, 2000);
          }
          return Promise.reject(error.response);
        }
      )
  }

  onRegister = () => {
    this.setState({ snackBar: false }, () => {
      this.props.history.push({
        pathname: '/register', userInput: this.state.userInput
      })
    });

  }
  ken42SignIn = () => {
  }
  responseGoogle = (response) => {
    console.log(response);
    let url = window.location.origin
    localStorage.setItem("gsuite_authToken", response.tokenId)
    let baseURL = localStorage.getItem("baseURL")
    if (response.accessToken) {
      let stringifyData = JSON.stringify(response)
      let payload = {
        authToken: localStorage.getItem('gsuite_authToken'),
        loginClient: 'google'
      }
      Axios.post(`${this.state.env.ken42}/initiate/config/institute/internalLogin`, payload)
        .then(response => {
          console.log("response", response)
          if (response.data.status == 'success') {
            localStorage.setItem("auth_token", response.data.data['auth_token'])
            localStorage.setItem("zen_auth_token", response.data.data['auth_token'])
            localStorage.setItem("sessionId", response.data.data['session_id'])
            this.props.history.push(`/${baseURL}/main/dashboard`)
          }
        },
          (err) => {


          })
      localStorage.setItem("Data", stringifyData);
      refreshTokenSetup(response)
    }
  }

  onLoginUser = () => {

  }
  // inputBlur=(value, item, e)=>{
  //   if (this.state.env['type'] === 'qa' || this.state.env['type'] === 'passive') {
  //     Axios.get(`${this.state.env['automationUri']}/test_config?contact=${value}`)
  //         .then(resp => {
  //           console.log(resp)
  //           if(resp.data.automate[0] != undefined){
  //             var automation = resp.data.automate[0]['automate']
  //             let loginFormData = []

  //             console.log(this.state.loginFormData)
  //               loginForm.map(item => {
  //                 if (automation == "true" && item.category == 'recaptcha') {
  //                   item['validation'] = true
  //                   loginFormData.push(item)
  //                 } else {
  //                   loginFormData.push(item)
  //                 }
  //               })
  //               this.setState({ loginFormData: loginFormData })

  //           }
  //         })
  //         .catch(err => {
  //             localStorage.setItem('automation', "false");
  //         })
  // }
  // }
  handleCopyright = () => {
    window.open('https://www.ken42.com/')
  }
  render() {
    return (
      < React.Fragment>
        {this.state.isLoader ? <Loader /> : null}
        {this.state.clientDetails.client ?
          <React.Fragment>
            <div className="zenqore-header-wrap">
              {this.state.clientDetails.client !== "ken42" ?
                <React.Fragment>
                  <img src={ZqsmallLogo} className="zq-logo-image" alt="Zenqore" ></img>
                  <img src={ZqTextLogo} className="zq-logo-setup" alt="Zenqore" ></img>
                </React.Fragment>
                :
                <React.Fragment>
                  {this.state.clientDetails.nameSpace === 'srm' ?
                    //SRM
                    <React.Fragment>
                      <div className="ken-logo-image">
                        <img src={this.state.SRMLogo} alt="SRM" ></img>
                      </div>
                      <h4 className="kenLogotext" style={{ color: "#005fad" }}>SRM University</h4>
                    </React.Fragment> :
                    <>{this.state.clientDetails.nameSpace === 'vkgi' || this.state.clientDetails.nameSpace === 'mnrgroup' ?
                      //vkgi || hkbk
                      <React.Fragment>
                        <div className="ken-logo-image">
                          <img src={this.state.vijayKiranLogo} alt="VKGI" style={{ width: '45px' }} ></img>
                        </div>
                        <h4 className="kenLogotext">Vijaykiran Group of Institutions</h4>
                      </React.Fragment> :
                      //rest
                      <React.Fragment>
                        <div className="ken-logo-image">
                          <img src={Ken42Logo} alt="ken42" ></img>
                        </div>
                        <h4 className="kenLogotext">{this.state.clientDetails.name}</h4>
                      </React.Fragment>}</>
                  }
                </React.Fragment>}
            </div>
            <div className="login-wrap figma">
              <div className="zenqore-image-wrapper">
                <div className="zenqore-image-wrap">
                  <React.Fragment>
                    <p className="zq-slogan"> Automated and Integrated Fee Collection <br /> Platform for Schools and Colleges </p>
                    <img src={zenqoreHome} className="zenHome-image" alt="Zenqore-Next Step Accounting" ></img>
                  </React.Fragment>
                </div>
              </div>
              <div className="zenqore-leftside-wrap">
                <div className="circles-wrap">
                  <img src={OrangeCrcle} className="circle-image" alt="Zenqore" ></img>
                  <img src={greenCircle} className="circle-image1" alt="Zenqore" ></img>
                  <img src={smallGreenCircle} className="circle-image2" alt="Zenqore" ></img>
                </div>
                <div className={`zenqore-login-container ${this.state.clientDetails.client === "ken42" ? 'ken42login' : ''}`}>
                  <div className={`login-content ${this.state.clientDetails.client === "ken42" ? 'ken42loginContainer' : ''}`}>
                    {this.state.clientDetails.nameSpace === "srm" ?
                      //SRM
                      <h3 className="login-form-hd login-form-hd-srm">Welcome to SRM University</h3> :
                      <>{this.state.clientDetails.nameSpace === 'vkgi' || this.state.clientDetails.nameSpace === 'mnrgroup' ?
                        //VKGI
                        <h3 className="login-form-hd login-form-vkgi">Welcome to Vijaykiran Group of Institutions</h3> :
                        //REST
                        <h3 className="login-form-hd">Welcome to {this.state.clientDetails.welcomeText}</h3>
                      }</>
                    }
                    {/* <h3 className="login-form-hd">Welcome Back</h3> */}
                    <p className="login-desc-txt">Please login to your account</p>
                    {/* {this.state.loginFormData.length > 0 ? <ZenForm inputBlur={this.inputBlur} inputData={this.state.loginFormData} onSubmit={this.onSubmit} /> : null} */}
                    {this.state.clientDetails.loginClient !== 'google' ? <ZenForm inputData={this.state.loginFormData} onSubmit={this.onSubmit} /> : <GoogleLogin
                      clientId={this.state.clientDetails.clientId}
                      render={renderProps => (
                        <Button onClick={renderProps.onClick} className="primary-btn login-btn ken42-login">Login</Button>
                      )}
                      buttonText="Login"
                      onSuccess={this.responseGoogle}
                      onFailure={this.responseGoogle}
                      cookiePolicy={'single_host_origin'}
                    />}
                    <div className="signup-req-wrap">
                      {/* <a className="forgot-pwd">Forgot  Password?</a> */}
                      {/* <p className="login-separator-line"></p>
                <p className="login-para">Don't have any account yet? <span className="login-btn-color" onClick={this.signupProcess} >Signup Request</span></p> */}
                      {/* <div className="login-copyright-wrap"><p className="login-copyright-text">© {(new Date().getFullYear())} All rights reserved. Gigaflow Technologies LLP. </p></div> */}
                    </div>
                    <Snackbar
                      anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                      }}
                      open={this.state.snackBar}
                      autoHideDuration={1000}
                      message={this.state.error}
                      className={'info-snackbar alert-red'}
                      action={
                        <React.Fragment>
                          {/* <Button color="secondary" size="small" onClick={this.onRegister}>
                      Click Here
                  </Button> */}
                        </React.Fragment>
                      }
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="login-copyright-wrap figma">
              <div className={this.state.clientDetails.nameSpace === 'srm' ? 'login-copyright-text-srm' : 'login-copyright-text'}>
                {/* <p>Copyright © {(new Date().getFullYear())} Gigaflow Technologies LLP. All rights reserved</p> */}
                {this.state.clientDetails.nameSpace === "srm" ?
                  //SRM
                  <p style={{ cursor: "pointer" }} onClick={this.handleCopyright}>Powered by Ken42</p> :
                  <>{this.state.clientDetails.nameSpace === 'vkgi' || this.state.clientDetails.nameSpace === 'mnrgroup' ?
                    //VKGI
                    <p style={{ cursor: "pointer" }} onClick={this.handleCopyright}>Powered by Ken42</p>
                    //REST
                    : <p>{this.state.clientDetails.copyright}</p>}</>
                }
              </div>
            </div> </React.Fragment> : ''
        }
      </React.Fragment>);
  }
}

export default withRouter(LoginComponent);