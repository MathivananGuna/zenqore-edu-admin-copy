import React, { Component } from 'react';
import profilePicture from '../../../assets/images/profile-large.png';
import axios from 'axios';
import { Alert } from 'rsuite';
import Avatar from 'react-avatar-edit';
import ContainerNavbar from '../../../gigaLayout/container-navbar';
import '../../../scss/profile-header.scss';
import '../../../scss/settings.scss';
import PublishOutlinedIcon from "@material-ui/icons/PublishOutlined";
import { withRouter } from 'react-router-dom';
import Loader from "../../../utils/loader/loaders";
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
class ProfilePicture extends Component {
    constructor(props) {
        super(props)
        this.state = {
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem('auth_token'),
            channel: localStorage.getItem('channel'),
            email: localStorage.getItem('email'),
            zen_auth_token: localStorage.getItem('zen_auth_token'),
            loginEmail: '',
            loginUserEmail: localStorage.getItem('channel'),
            profilePicture: profilePicture,
            firstName: '',
            lastName: '',
            compName: '',
            Role: '',
            organization: [],
            DefaultOrganization: [],
            userProfile: [],
            loader: false,
            containerNav: {
                isBack: true,
                name: 'Profile Picture',
                isName: true,
                isSearch: false,
                isSort: false,
                isPrint: false,
                isDownload: false,
                isShare: false,
                isNew: false,
                newName: "Submit",
                isSubmit: false
            },
            avatarWidth: 0,
            avatarHeight: 0,
            payloadFname:"",
            payloadLname:"",
        }
        this.logoRef = React.createRef(this.logoRef);
    }
    componentWillMount() {
     
    }
    componentDidMount() {
        this.setState({ profilePicture: null })
        setTimeout(() => {
            console.log('this.logoRef', this.logoRef.current.offsetHeight, this.logoRef.current.offsetWidth)
            this.setState({ avatarWidth: this.logoRef.current.offsetWidth, avatarHeight: this.logoRef.current.offsetHeight }, () => {
                console.log(this.state.avatarWidth, this.state.avatarHeight)
            })
        }, 0);

        axios({
            url: `${this.state.env['userProfile']}/zq/user/profile?userName=${this.state.loginUserEmail}`,
            method: 'GET',
            headers: {
              "Authorization": this.state.authToken
            }
          })
          .then(res => {
              console.log(res);
              this.setState({payloadFname: res.data.data.firstName, payloadLname: res.data.data.lastName})
          })
          .catch(err => {console.log(err)})
        // axios({
        //     url: `${this.state.env['newMasterId']}/master/get/getUserProfile`,
        //     method: 'POST',
        //     data: {
        //         "authToken": this.state.zen_auth_token
        //     }
        // }).then(res => {
        //     console.log("getUserProfile", res)
        //     let userProfile = res.data;
        //     let firstName = userProfile.FirstName;
        //     let lastName = userProfile.LastName;
        //     let loginEmail = userProfile.UserEmail;
        //     localStorage.setItem("orgId", userProfile.OrgList[0].orgId)
        //     this.setState({
        //         userProfile: userProfile, firstName: firstName, lastName: lastName, loginEmail: loginEmail
        //     })
        // })
    }
    onClose = () => {
        this.setState({ profilePicture: null })
    }
    onCrop = (profilePicture) => {
        this.setState({ profilePicture })
    }
    closeProfilePicture = () => {
        this.props.history.push('/main/dashboard')
    }
   
    onBeforeFileLoad(elem) {
        //File size is set to 1.5MB
        if (elem.target.files[0].size > 1500000) {
            Alert.error("Image size cannot be greater than 1.5MB !")
            elem.target.value = "";
        };
    }
    //SAVE PROFILE PICTURE API
    savePicture = () => {
        this.setState({ loader: true })
        const { profilePicture } = this.state;
        axios({
            url: `${this.state.env['ken42']}/zq/user/profile?userName=${this.state.channel}`,
            method: 'PUT',
            data: {
                "firstName": this.state.payloadFname,
                "lastName": this.state.payloadLname,
                "profilePic": profilePicture
            },
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => {
            console.log("profile picture", res)
            // Alert.success(res.data.message)
            Alert.success("Profile changed successfully")
            this.setState({ imgUpdate: true })
            setTimeout(() => {
                this.setState({ loader: false });
                this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`)
                window.location.reload();
            }, 1500)
        }).catch(err => {
            this.setState({ loader: false });
            Alert.error("Failed to upload profile picture. Please try again !")
        })
    }
    render() {
        const borderStyle = {
            border: "1px dotted #1359c1",
            borderRadius: "10px"
        };
        const style = {
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            cursor: "pointer",
            backgroundColor: "#1359c11c",
            borderRadius: "10px",
        };
        const label = (
            <div className="setting-logo-card">
                <div>
                    <div className="card-third-round">
                        <PublishOutlinedIcon className="onboard-icons" />
                    </div>
                    <h6 className="onboard-card-header">Upload Image</h6>
                    <p className="onboard-card-title" style={{ color: "#000" }}>
                        Drag and drop Image or
                  <span className="browse-content">browse</span>
                    </p>
                </div>
            </div>
        );
        return (
            <React.Fragment>
                {this.state.loader ? <Loader /> : null}
                <div className='profile-picture-main'>
                    <div className='profile-picture-headers'>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Profile Picture</p>
                        </div>
                        {/* {this.state.containerNav == undefined ? null : <ContainerNavbar containerNav={this.state.containerNav} />} */}
                        <div className="headers-column2">

                        </div>
                    </div>
                    <div className="profile-header-btns" >
                        <button className="btns-submit add-item-btn" onClick={this.savePicture} tooltip-title="Submit" disabled={this.state.profilePicture == null}>Submit</button>
                    </div>
                    <div className='profile-picture-data-wrapper'>
                        <div className='avatar-editor' ref={this.logoRef}>
                            {
                                (this.state.avatarWidth > 0 && this.state.avatarHeight > 0) ?
                                    <Avatar
                                        width={this.state.avatarWidth}
                                        height={this.state.avatarHeight}
                                        onCrop={this.onCrop}
                                        onClose={this.onClose}
                                        onBeforeFileLoad={this.onBeforeFileLoad}
                                        src={this.state.src}
                                        className="avatar-editor-wrapper"
                                        label={label}
                                        id="avatar"
                                        labelStyle={style}
                                        borderStyle={borderStyle}
                                    /> : null
                            }
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default withRouter(ProfilePicture);