import React, { Component } from 'react';
// import { withRouter } from 'react-router-dom';
import axios from 'axios';
import xlsx from 'xlsx';
import Loader from '../../../../utils/loader/loaders';
// import KenTable from '../../../../utils/Table/kenTable';
// import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
// import studentStmtResponse from './student-stmt-Reponse';
import '../../../../scss/student-reports.scss';
import '../../../../scss/student.scss';
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import PaginationUI from "../../../../utils/pagination/pagination";
import '../../../../scss/common-table.scss';
import DateFormatter from '../../../date-formatter/date-formatter'
import DateFormatContext from '../../../../gigaLayout/context'
// import JsonResponse3 from '../../../feeCollectionPortal/response3.json';
import moment from 'moment';
// import { tr } from 'date-fns/locale';
class studentStmtReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            demandNoteTable: [],
            showTable: true,
            env: JSON.parse(localStorage.getItem('env')),
            orgId: localStorage.getItem('orgId'),
            campusId :  localStorage.getItem("campusId"),
            userId : localStorage.getItem('userId'),
            containerNav: {
                isBack: false,
                name: "List of Student Statement",
                isName: false,
                isSearch: true,
                isSort: false,
                isPrint: true,
                isDownload: true,
                isShare: false,
                isNew: false,
                newName: "New",
                isSubmit: false,
                isSelectAppReport: true,
                selectPickerOption: true,
                selectCampusOption: true,
                selectSectionOption:true,
                selectFranchiseOption: false
            },
            tableHeader: ["REG ID", "STUDENT NAME", "CLASS/BATCH", "ADMISSION DATE"],
            printReportArr: [],
            printable: false,
            previewTableData: [],
            filterData: [],
            previewStudentTableData: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 0,
            noData: false,
            isLoader: false,
            previewpage: 1,
            previewlimit: 10,
            previewtotalRecord: 0,
            previewtotalPages: 0,
            totalPreviewRecord: 0,
            particularItem: undefined,
            regIdReport: '',
            appDatas: [
                {
                    label: "All Program Plan",
                    value: "All"
                }
            ],
            campusData : [
                {
                    label: "All Campus",
                    value: "All"
                }
            ],
            sectionData : [
                {
                    label: "All Section",
                    value: "All"
                }
            ],
            filterKey: "All",
            paginationCall: ""
        }
    }
    static contextType = DateFormatContext;
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            if (this.state.paginationCall === "allData") this.onDataCall();
            else if (this.state.paginationCall === "filterData") this.onFilterCall(page, limit);
            else this.onDataCall();
        });
        console.log(page, limit);
    };
    componentDidMount() {
        this.onDataCall()
        this.filterDataNew();
        let tableHeader = [`${this.context.reportLabel ? this.context.reportLabel : 'REG ID'}`, "STUDENT NAME", `${this.context.classLabel ? this.context.classLabel : "CLASS/BATCH"}`, "ADMISSION DATE"];
        this.setState({ tableHeader })
    }
    onDataCall = () => {
        this.setState({ isLoader: true })
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ printReportArr: [], paginationCall: "allData" }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/studentStatement?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&classbatchName=${this.state.filterKey}&page=${this.state.page}&limit=${this.state.limit}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    console.log(resp);
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    this.setState({ printReportArr: resp.data.data, filterData: resp.data.data, totalPages: resp.data.totalPages, totalRecord: resp.data.totalRecord, noData })

                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true })
                })

        })
    }
    filterDataNew = () => {
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        axios.get(`${this.state.env['zqBaseUri']}/edu/reports/studentStatement?orgId=${this.state.orgId}campusId=${this.state.campusId}&userId=${userId}`, {
            headers: {
                'Authorization': localStorage.getItem("auth_token"),
            }
        }).then(resp => {
            let dropDownData = [{
                "value": "All",
                label: "All Program Plan",

            }]
            let allData = resp.data.data
            let programPlanData = Array.from(new Set(allData.map(a => a['CLASS/BATCH']))) //getting all Department names in array
            programPlanData.map((item, index) => {
                dropDownData.push({
                    "value": item == undefined ? "-" : item,
                    "label": item == undefined ? "-" : item
                })
            })
            this.setState({ appDatas: dropDownData })
            this.setState({ isLoader: false })
        })
            .catch(err => {
                console.log(err);
                this.setState({ isLoader: false, appDatas: [], noData: true })
            })

    }
    formatAmount = (amount) => {
        let a = Number(amount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })
        let b = a.replace('₹', "")
        return b
    }
    onDownloadEvent = () => {
        this.setState({ isLoader: true })
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        axios.get(`${this.state.env['zqBaseUri']}/edu/reports/studentStatement?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&classbatchName=${this.state.filterKey}`, {
            headers: {
                'Authorization': localStorage.getItem("auth_token")
            }
        })
            .then(resp => {
                console.log("student fee", resp);
                var createXlxsData = [];
                var createXlxsDataItem = [];
                let regId = this.context.reportLabel || "REG ID"
                let batch = this.context.classLabel || "CLASS/BATCH"
                    `${this.context.classLabel ? this.context.classLabel : "CLASS/BATCH"}`
                if (this.state.particularItem == undefined) {
                    resp.data.data.map(item => {
                        createXlxsData.push({
                            [regId]: item["REGISTRATION ID"],
                            "STUDENT NAME": item["STUDENT NAME"],
                            [batch]: item["CLASS/BATCH"],
                            "ADMISSION DATE": item["ADMISSION DATE"] == "NaN/NaN/NaN" || item["ADMISSION DATE"] == undefined || item["ADMISSION DATE"] == null ? "-" : moment(item["ADMISSION DATE"]).format(this.context.dateFormat),

                        })
                        console.log('**DATA**', item)

                    })
                    var ws = xlsx.utils.json_to_sheet(createXlxsData);
                    var wb = xlsx.utils.book_new();
                    xlsx.utils.book_append_sheet(wb, ws, "Student Statement");
                    xlsx.writeFile(wb, "student_statement.xlsx");
                    //xlsx.writeFile(wb, "fee_pending.csv");
                    this.setState({ isLoader: false })
                }
                else {
                    var particularData =  resp.data.data.filter(task =>{
                       return task["REGISTRATION ID"] == this.state.previewStudentTableData[0][regId]    
                    })
                    // var particularData = resp.data.data[this.state.particularItem]
                    createXlxsData.push({
                        [regId]: particularData[0]["REGISTRATION ID"],
                        "STUDENT NAME": particularData[0]["STUDENT NAME"],
                        "CLASS/BATCH": particularData[0]["CLASS/BATCH"],
                        "ADMISSION DATE": particularData[0]["ADMISSION DATE"] == "NaN/NaN/NaN" || particularData[0]["ADMISSION DATE"] == undefined || particularData[0]["ADMISSION DATE"] == null ? "-" : moment(particularData[0]["ADMISSION DATE"]).format(this.context.dateFormat),
                    })
                    particularData[0].items.map((dataOne, c) => {
                        let transactionDate = '';
                        let paymentDate = '';
                        if (dataOne["DEMAND NOTE DATE"]) {
                            if (dataOne["DEMAND NOTE DATE"].includes("/")) { transactionDate = dataOne["DEMAND NOTE DATE"].split("/") }
                            else if (dataOne["DEMAND NOTE DATE"].includes("-")) { transactionDate = dataOne["DEMAND NOTE DATE"].split("-") }
                        }
                        if (dataOne["PAYMENT DATE"]) {
                            if (dataOne["PAYMENT DATE"].includes("/")) { paymentDate = dataOne["PAYMENT DATE"].split("/") }
                            else if (dataOne["PAYMENT DATE"].includes("-")) { paymentDate = dataOne["PAYMENT DATE"].split("-") }
                        }
                        if (String(dataOne.name).toLowerCase() != "total") {
                            createXlxsDataItem.push({
                                "DEMAND NOTE DATE": moment(new Date(transactionDate[2], transactionDate[1] - 1, transactionDate[0])).format(this.context.dateFormat),
                                "PAYMENT DATE": moment(new Date(paymentDate[2], paymentDate[1] - 1, paymentDate[0])).format(this.context.dateFormat),
                                "PARTICULARS": dataOne["PARTICULARS"],
                                // "DUE AMOUNT": this.formatCurrency(dataOne["DUE AMOUNT"]),
                                // "PAID AMOUNT": this.formatCurrency(dataOne["PAID AMOUNT"]),
                                // "BALANCE": this.formatCurrency(dataOne["BALANCE"]),
                                "DUE AMOUNT (INR)": this.formatAmount(dataOne["DUE AMOUNT"]),
                                "PAID AMOUNT (INR)": this.formatAmount(dataOne["PAID AMOUNT"]),
                                "BALANCE (INR)": this.formatAmount(dataOne["BALANCE"]),
                                "PAYMENT MODE": dataOne["PAYMENT MODE"].charAt(0).toUpperCase() + dataOne["PAYMENT MODE"].slice(1)
                            })
                        }
                    })
                    var ws = xlsx.utils.json_to_sheet(createXlxsData);
                    var wsf = xlsx.utils.json_to_sheet(createXlxsDataItem);
                    var wb = xlsx.utils.book_new();
                    xlsx.utils.book_append_sheet(wb, ws, "Student Statement");
                    xlsx.utils.book_append_sheet(wb, wsf, "Statement of Payment");
                    xlsx.writeFile(wb, "student_statement.xlsx");
                    this.setState({ isLoader: false })
                }
            })
            .catch(err => {
                console.log(err, 'error')
                this.setState({ isLoader: false })
            })
    }
    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    onCheckbox = (selectedItem) => {

        console.log('selectedItem', selectedItem)
        this.setState({ selectedData: selectedItem })
        var amountInit = 0
        if (selectedItem.length > 0) {
            selectedItem.map(item => {
                if (item.Description != 'Total') {
                    amountInit = amountInit + item['Amount']
                }
            })
        }
        this.setState({ amount: amountInit })
        this.setState({ amountFormat: this.formatCurrency(amountInit) })
    }
    onPaginationChange2 = (page, limit, datas) => {
        let data = JSON.parse(datas)
        this.setState({ page: page, limit: limit }, () => {
            // this.onDataCall();
            let current_page = Number(page);
            let perPage = Number(limit);
            let total_pages = Math.ceil(data.length / perPage);
            console.log("total", total_pages)
            current_page = total_pages < current_page ? total_pages : current_page
            let lastIndex = current_page * perPage;
            let startIndex = lastIndex - perPage;
            let paginatedItems = data.slice(startIndex, lastIndex)
            this.setState({ previewpage: current_page, previewlimit: perPage, previewtotalPages: total_pages, previewtotalRecord: data.length });
            this.setState({ previewTableData: paginatedItems });
        });
        console.log(page, limit, this.state.previewtotalPages);
    };
    showStudentStmtPreview = (previewItem, index) => {
        this.setState({
            //preview: true,
            previewpage: 1,
            previewlimit: 10,
            regIdReport: previewItem['REGISTRATION ID']
        });
        console.log("index", index)
        // let preview = previewItem.items;
        let preview = previewItem.items && previewItem.items.length > 0 ? previewItem.items : []
        console.log("preview", preview)
        // let previewItemsArray = []
        this.setState({ particularItem: ((Number(this.state.page) - 1) * 10) + Number(index) });

        let regId = this.context.reportLabel || "REG ID"
        let batch = this.context.classLabel || "CLASS/BATCH"
        let previewArray = [
            {
                [regId]: previewItem['REGISTRATION ID'],
                "STUDENT NAME": previewItem["STUDENT NAME"],
                [batch]: previewItem["CLASS/BATCH"],
                "ADMISSION DATE": previewItem['ADMISSION DATE'] == "NaN/NaN/NaN" || previewItem["ADMISSION DATE"] == undefined || previewItem["ADMISSION DATE"] ? previewItem["ADMISSION DATE"] : moment(previewItem["ADMISSION DATE"]).format(this.context.dateFormat)
            }
        ]
        localStorage.setItem('previewTableData', JSON.stringify(preview))
        let current_page = Number(this.state.previewpage);
        let perPage = Number(this.state.previewlimit);
        let total_pages = Math.ceil(preview.length / perPage);

        current_page = total_pages < current_page ? total_pages : current_page
        let lastIndex = current_page * perPage;
        let startIndex = lastIndex - perPage;
        let paginatedItems = preview.slice(startIndex, lastIndex)
        this.setState({
            previewStudentTableData: previewArray,
            previewTableData: paginatedItems,
            previewtotalPages: total_pages,
            previewtotalRecord: preview.length
        }, () => {
            console.log(this.state.previewStudentTableData)
            this.setState({ showTable: false });
        });
    }
    handleBack = () => {
        this.setState({ particularItem: undefined })
        if (this.state.showTable) {
            this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`);
        }
        else {
            this.setState({ showTable: true, page: 1 });
        }
    }
    searchHandle = (searchValue) => {
        console.log('searchValue', searchValue)
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ isLoader: true })
        if (searchValue.length > 0) {
            return axios.get(`${this.state.env['zqBaseUri']}/edu/reports/studentStatement?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&page=${this.state.page}&limit=${this.state.limit}&classbatchName=${this.state.filterKey}&searchKey=${searchValue}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    this.setState({
                        printReportArr: resp.data.data.reverse(),
                        totalPages: resp.data.totalPages,
                        totalRecord: resp.data.totalRecord,
                        noData
                    })

                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true, dataInfo: "No Data" })
                })

            // this.setState({
            //     printReportArr: [],
            //     dataInfo: "Fetching Data..."
            // })
        }
        else {
            this.setState({ printReportArr: this.state.filterData, isLoader: false })
        }
        // })

    }
    onSelectPrgmPlan = (value, item, label) => {
        let selectedValue = value
        this.setState({ filterKey: selectedValue })
        if (value === "All") {
            this.onDataCall()
        } else {
            this.onFilterCall(1, 10)
        }
    }
    onFilterCall = (page, limit) => {
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ isLoader: true, paginationCall: "filterData", page: page, limit: limit })
        this.setState({ printReportArr: [] }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/studentStatement?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&classbatchName=${this.state.filterKey}&page=${this.state.page}&limit=${this.state.limit}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token"),
                }
            })
                .then(resp => {
                    console.log(resp);
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    this.setState({ printReportArr: resp.data.data, filterData: resp.data.data, totalPages: resp.data.totalPages, totalRecord: resp.data.totalRecord, noData })
                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true })
                })

        })
    }
    onSelectClean = () => {
        this.setState({ filterKey: 'All' })
    }
    printScreen = () => {
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ isLoader: false }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/studentStatement?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&classbatchName=${this.state.filterKey}&pagination=false`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    console.log(resp);
                    this.setState({
                        printable: true,
                        printReportArr: resp.data.data,
                    }, () => {
                        this.setState({ printable: false })
                        window.print();
                        this.onDataCall();
                    })
                })
        });
    }
    render() {
        let regId = this.context.reportLabel || "REG ID"
        let batch = this.context.classLabel || "CLASS/BATCH"
        return (<React.Fragment >
            {this.state.isLoader && <Loader />}
            {/* { this.state.demandNoteTable.length > 0 ? <KenTable tableData={this.state.demandNoteTable} onCheckbox={this.onCheckbox} checkBox={true} /> : null} */}
            <div className="reports-student-fees list-of-students-mainDiv">
                {/* <div className="student-report-header-title">
                    <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.handleBack} style={{ marginRight: 10, cursor: 'pointer' }} />
                    {this.state.showTable != false ?
                        <p className="top-header-title">Report | Student Statement </p> :
                        <p className="top-header-title">Student Statement | {this.state.regIdReport} </p>
                    }
                </div> */}
                <div className="trial-balance-header-title">
                    <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.handleBack} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                    {this.state.showTable ?
                        <p className="top-header-title" onClick={this.handleBack}>| Student Statement </p> :
                        <p className="top-header-title" onClick={this.handleBack}>| Student Statement | {this.state.regIdReport} </p>}
                </div>
                <div className="reports-body-section print-hd">
                    <React.Fragment>
                        <ContainerNavbar
                            containerNav={this.state.containerNav}
                            onDownload={() => this.onDownloadEvent()}
                            searchValue={(searchValue) => this.searchHandle(searchValue)}
                            Selectdata={this.state.appDatas}
                            SelectCampusdata = {this.state.campusData}
                            SelectSectionData={this.state.sectionData}
                            onSelectDefaultValue={this.state.filterKey}
                            onSelectCampusDefaultValue={this.state.filterKey}
                            onSelectClean={this.onSelectClean}
                            onSelectChange={this.onSelectPrgmPlan}
                            printScreen={this.printScreen} />
                        <div className="print-time">{this.state.printTime}</div>
                    </React.Fragment>
                    {/* <p className="statement-date-time">Date: {<DateFormatter date={new Date()} format={this.context.dateFormat} />}</p> */}
                    <div className="reports-data-print-table">
                        {/* <div className=""> */}
                        {this.state.showTable ?
                            <React.Fragment>
                                <table className="transaction-table-review reports-tableRow-header">
                                    <thead>
                                        <tr>
                                            {this.state.tableHeader.map((data, i) => {
                                                return <th className={"demand-note " + String(data).replace(' ', '')} key={i + 1}>{data}</th>
                                            })}
                                        </tr>
                                    </thead>
                                    {this.state.printReportArr.length > 0 ?
                                        <tbody>
                                            {this.state.printReportArr.map((data, i) => {
                                                return (<tr key={i + 1} id={i + 1} className="reports-tableRow-body"
                                                    onClick={(e) => { this.showStudentStmtPreview(data, i) }}
                                                    style={{ cursor: "pointer" }}>
                                                    {/* <td className="transaction-sno">{data.txnId}</td> */}
                                                    <td className="transaction-vch-num" >{data["REGISTRATION ID"]}</td>
                                                    <td className="transaction-vch-num" >{data["STUDENT NAME"]}</td>
                                                    <td className="transaction-vch-num" >{data["CLASS/BATCH"]}</td>
                                                    <td className="transaction-vch-type">{data['ADMISSION DATE'] == "NaN/NaN/NaN" || data["ADMISSION DATE"] == undefined || data["ADMISSION DATE"] == null ? "-" : <DateFormatter date={data['ADMISSION DATE']} format={this.context.dateFormat} />}</td>

                                                </tr>
                                                )
                                            })}
                                        </tbody> : <tbody>
                                            <tr>
                                                {this.state.noData ? <td className="noprog-txt" colSpan={this.state.tableHeader.length}>No data...</td> :
                                                    <td className="noprog-txt" colSpan={this.state.tableHeader.length}>Fetching the data...</td>
                                                }

                                            </tr></tbody>
                                    }

                                </table>
                                <div>
                                    {this.state.printReportArr.length == 0 ? null :
                                        <PaginationUI
                                            total={this.state.totalRecord}
                                            onPaginationApi={this.onPaginationChange}
                                            totalPages={this.state.totalPages}
                                            limit={this.state.limit}
                                            currentPage={this.state.page}
                                        />}
                                </div>
                            </React.Fragment>
                            :
                            <React.Fragment>
                                <table className="transaction-table-review reports-tableRow-header">
                                    <thead>
                                        <tr>
                                            {Object.keys(this.state.previewStudentTableData[0]).map((data, i) => {
                                                return <th className={"demand-note " + String(data).replace(' ', '')} key={i + 1}>{data}</th>
                                            })}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.previewStudentTableData.map((data, i) => {
                                            return (<tr key={i + 1} id={i + 1} className="reports-tableRow-body">
                                                <td className="transaction-vch-num" >{data[regId]}</td>
                                                <td className="transaction-vch-num" >{data["STUDENT NAME"]}</td>
                                                <td className="transaction-vch-num" >{data[batch]}</td>
                                                <td className="transaction-vch-type">{data['ADMISSION DATE'] == "NaN/NaN/NaN" || data["ADMISSION DATE"] == undefined || data["ADMISSION DATE"] == null ? "-" : <DateFormatter date={data['ADMISSION DATE']} format={this.context.dateFormat} />}</td>
                                            </tr>
                                            )
                                        })}
                                    </tbody>
                                </table>
                                <p className="statement-of-payment-text">Statement of Payment</p>
                                {this.state.previewTableData.length > 0 ?
                                    <React.Fragment>
                                        <table className="transaction-table-review reports-tableRow-header">
                                            <thead>
                                                <tr>
                                                    {Object.keys(this.state.previewTableData[0]).map((data, i) => {
                                                        if (String(data) == "DUE AMOUNT") {
                                                            return <th className={"demand-note " + String(data).replace(' ', '')} key={i + 1}>TOTAL FEES</th>
                                                        }
                                                        else return <th className={"demand-note " + String(data).replace(' ', '')} key={i + 1}>{data}</th>
                                                    })}
                                                </tr>
                                            </thead>
                                            {this.state.printReportArr.length > 0 ?
                                                <tbody>
                                                    {this.state.previewTableData.map((data, i) => {
                                                        let DNdate = data["DEMAND NOTE DATE"].split("/"); DNdate = [DNdate[2], DNdate[1], DNdate[0]].join("/");
                                                        let paymentDate = data["PAYMENT DATE"].split("/"); paymentDate = [paymentDate[2], paymentDate[1], paymentDate[0]].join("/");
                                                        return (<tr key={i + 1} id={i + 1} className="reports-tableRow-body">
                                                            {/* style={{ cursor: "pointer" }} */}
                                                            <td className="transaction-vch-num" style={{ width: "180px" }} ><DateFormatter date={new Date(DNdate)} format={this.context.dateFormat} /></td>
                                                            <td className="transaction-vch-num" ><DateFormatter date={new Date(paymentDate)} format={this.context.dateFormat} /></td>
                                                            <td className="transaction-vch-num" >{data["PARTICULARS"]}</td>
                                                            <td className="transaction-vch-num" >{this.formatCurrency(Number(data["DUE AMOUNT"]))}</td>
                                                            <td className="transaction-vch-type">{this.formatCurrency(Number(data["PAID AMOUNT"]))}</td>
                                                            <td className="transaction-vch-type">{this.formatCurrency(Number(data["BALANCE"]))}</td>
                                                            <td className="transaction-vch-type">{data["PAYMENT MODE"].charAt(0).toUpperCase() + data["PAYMENT MODE"].slice(1)}</td>
                                                        </tr>
                                                        )
                                                    })}
                                                </tbody> :
                                                <tbody>
                                                    <tr>
                                                        <td className="noprog-txt" colSpan={this.state.tableHeader.length}>Fetching the data...</td>
                                                    </tr></tbody>
                                            }
                                        </table>
                                        <div>
                                            {this.state.previewTableData.length == 0 ? null :
                                                <PaginationUI
                                                    total={this.state.previewtotalRecord}
                                                    onPaginationApi={this.onPaginationChange2}
                                                    totalPages={this.state.previewtotalPages}
                                                    limit={this.state.previewlimit}
                                                    currentPage={this.state.previewpage}
                                                    allData={localStorage.getItem('previewTableData')}
                                                />}
                                        </div>
                                    </React.Fragment>
                                    :
                                    <p style={{ fontSize: 16, textAlign: 'center', marginTop: 30 }}>No Statement</p>
                                    // : <table>
                                    //     <tbody>
                                    //         <tr>
                                    //             <td colSpan={this.state.tableHeader.length}>No Statement</td>
                                    //         </tr></tbody>
                                    // </table>
                                }
                            </React.Fragment>}
                        {/* </div> */}
                        {/* <div>
                            {this.state.printReportArr.length == 0 ? null :
                                <PaginationUI
                                    total={this.state.totalRecord}
                                    onPaginationApi={this.onPaginationChange}
                                    totalPages={this.state.totalPages}
                                    limit={this.state.limit}
                                    currentPage={this.state.page}
                                />}
                        </div> */}
                    </div>
                </div>
            </div>

        </React.Fragment >);
    }
}

export default studentStmtReport;