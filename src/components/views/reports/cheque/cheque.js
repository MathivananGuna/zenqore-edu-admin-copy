import React, { Component } from 'react';
import '../../../../scss/fees-type.scss';
import './cheque.scss';
import '../../../../scss/setup.scss';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../utils/Table/table-component";
import PaginationUI from "../../../../utils/pagination/pagination";
import Loader from "../../../../utils/loader/loaders";
import ChequeDataJson from './cheque.json';
import ZenForm from "../../../../components/input/zqform";
import axios from 'axios';
import xlsx from 'xlsx';
import moment from 'moment';
class Cheque extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            containerNav: {
                isBack: false,
                name: "List of Cheque/DD",
                isName: true,
                isSearch: true,
                isSort: false,
                isPrint: true,
                isDownload: true,
                isShare: false,
                isNew: false,
                newName: "New",
                isSubmit: false,
                isSelectAppReport: true,
                selectPickerOption: true,
                selectCampusOption: true,
                selectSectionOption: true,
                selectFranchiseOption: false
            },
            tablePrintDetails: [],
            chequeReportData: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 1,
            noProg: "Fetching Data ...",
            PreStudentName: "",
            previewList: false,
            env: JSON.parse(localStorage.getItem('env')),
            orgId: localStorage.getItem('orgId'),
            previewListForm: ChequeDataJson.formJson,
            appDatas: [
                {
                    label: "All",
                    value: "All"
                }
            ],
            filterKey: "All",
        }
    }
    componentDidMount() {
        this.onGetChequeData()
    }

    onGetChequeData = async () => {
        try {
            const apiData = await axios.get(`${this.state.env['zqBaseUri']}/edu/cheque?orgId=${this.state.orgId}&page=${this.state.page}&perPage=${this.state.limit}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
            let response = apiData.data.data.data
            console.log('response', response)
            if (response.length > 0) {
                let chequeData = []
                response.map(item => {
                    chequeData.push({
                        "Cheque/DD No.": item.chequeNo,
                        "Cheque/DD Date": moment(item.chequeDate).format('DD/MM/YYYY'),
                        "Bank": item.bankName,
                        "Amount": Number(item.totalAmount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' }),
                        "Collected By": item.collectedBy || "Zenqore Education",
                        "Collected On": moment(item.collectedOn).format('DD/MM/YYYY'),
                        "Days since Collected": item.daysSinceCollected || "-",
                        "Status": item.status
                    })
                })
                let SamplechequeData = [...chequeData, ...ChequeDataJson.tableData]

                this.setState({ chequeReportData: SamplechequeData, totalRecord: apiData.data.data.totalRecord, totalPages: apiData.data.data.totalPages, limit: apiData.data.data.perPage })
            }
            else {
                let SamplechequeData = [...ChequeDataJson.tableData]
                this.setState({ noProg: "No Data", chequeReportData: SamplechequeData })
            }

        }
        catch (err) {

        }
        // axios.get(`${this.state.env['zqBaseUri']}/edu/cheque?orgId=${this.state.orgId}&page=${this.state.page}&perPage=${this.state.limit}`, {
        //     headers: {
        //         'Authorization': localStorage.getItem("auth_token")
        //     }
        // }).then(res => {
        //     console.log(res)
        //     let response  = res.data.data.data
        //     if (response.length > 0) {
        //         let chequeData = []
        //         response.map(item => {
        //             chequeData.push({
        //                 "Cheque/DD No.": item.chequeNo,
        //                 "Cheque/DD Date": moment(item.chequeDate).format('DD/MM/YYYY'),
        //                 "Bank": item.bankName,
        //                 "Amount": item.totalAmount,
        //                 "Collected By": item.collectedBy,
        //                 "Collected On": moment(item.collectedOn).format('DD/MM/YYYY'),
        //                 "Days since Collected": item.daysSinceCollected || "-",
        //                 "Status": item.status
        //             })
        //         })

        //         this.setState({ chequeReportData: chequeData, totalRecord:res.data.data.totalRecord,totalPages:res.data.data.totalPages,limit:res.data.data.perPage})
        //     }
        //     else {
        //         this.setState({noProg:"No Data"})
        //     }
        // }).catch(err => {
        //     console.log(err)
        // })

    }
    onAddNewLoans = () => { }
    onPreviewLoan = (e) => { }
    onPaginationChange = (page, limit) => {
        console.log(page, limit)
        this.setState({ page: page, limit: limit }, () => {
            this.onGetChequeData()
        })
    }
    onPreviewLoanList = () => { }
    moveBackTable = () => {
        this.setState({ previewList: false })
    }
    actionClickFun = (e) => {
        console.log(e);
    }
    handleTabChange = () => { }
    allSelect = () => { }
    onInputChanges = () => { }
    searchHandle = () => { }
    onSelectPrgmPlan = (value, item, label) => {
        let selectedValue = value
        this.setState({ filterKey: selectedValue })
        //this.onDataCall()
    }
    onSelectClean = () => {
        this.setState({ filterKey: 'All' })
    }
    onDownloadEvent = () => {
        this.setState({ isLoader: true })
        var createXlxsData = []
        var ws = xlsx.utils.json_to_sheet(createXlxsData);
        var wb = xlsx.utils.book_new();
        xlsx.utils.book_append_sheet(wb, ws, "Cheque Reports");
        xlsx.writeFile(wb, "Cheque_Reports.xlsx");
        //xlsx.writeFile(wb, "demand_note_reports.csv");
        this.setState({ isLoader: false })
    }
    render() {
        return (
            <div className="list-of-feeType-mainDiv">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                <React.Fragment>
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Cheque/DD</p>
                        </div>
                    </React.Fragment>
                    <div className="masters-body-div">
                        <React.Fragment>
                            <ContainerNavbar containerNav={this.state.containerNav} onAddNew={() => { this.onAddNewLoans(); }} searchValue={(searchValue) => this.searchHandle(searchValue)} Selectdata={this.state.appDatas} onSelectDefaultValue={this.state.filterKey} onSelectClean={this.onSelectClean} onDownload={() => this.onDownloadEvent()} onSelectChange={this.onSelectPrgmPlan} />
                        </React.Fragment>
                        <div className="table-wrapper" style={{ marginTop: "10px" }}>
                            {this.state.chequeReportData.length === 0 ? <p className="noprog-txt">{this.state.noProg}</p> :
                                <React.Fragment>
                                    <div className="remove-last-child-table" >
                                        <ZqTable
                                            data={this.state.chequeReportData}
                                            allSelect={this.allSelect}
                                            rowClick={(item) => { this.onPreviewLoan(item) }}
                                            onRowCheckBox={(item) => { this.allSelect(item) }}
                                            handleActionClick={(item) => { this.actionClickFun(item) }}
                                        />
                                    </div>
                                </React.Fragment>
                            }
                            {this.state.chequeReportData.length !== 0 ? <PaginationUI onPaginationApi={this.onPaginationChange} totalPages={this.state.totalPages} limit={this.state.limit} total={this.state.totalRecord} /> : null}
                        </div>
                    </div>
                </React.Fragment>
            </div>
        )
    }
}
export default Cheque;