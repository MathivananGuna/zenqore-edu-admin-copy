import React, { Component } from 'react';
// import { withRouter } from 'react-router-dom';
import axios from 'axios';
import xlsx from 'xlsx';
import Loader from '../../../../utils/loader/loaders';
// import KenTable from '../../../../utils/Table/kenTable';
// import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
// import programPlanStmtResponse from './programPlanStmtResponse';
import '../../../../scss/student-reports.scss';
import '../../../../scss/student.scss';
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import moment from 'moment';
import PaginationUI from "../../../../utils/pagination/pagination";
import '../../../../scss/common-table.scss';
import DateFormatter from '../../../date-formatter/date-formatter'
import DateFormatContext from '../../../../gigaLayout/context'
class programPlanStmtReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            demandNoteTable: [],
            env: JSON.parse(localStorage.getItem('env')),
            orgId: localStorage.getItem('orgId'),
            campusId :  localStorage.getItem("campusId"),
            userId : localStorage.getItem('userId'),
            containerNav: {
                isBack: false,
                name: " List of Program Plan Statement",
                isName: true,
                isSearch: true,
                isSort: false,
                isPrint: true,
                isDownload: true,
                isShare: false,
                isNew: false,
                newName: "New",
                isSubmit: false,
                isSelectAppReport: true,
                selectPickerOption: true,
                selectCampusOption: true,
                selectSectionOption:true,
            },
            tableHeader: ["PROGRAM ID", "PROGRAM NAME", "TOTAL STUDENTS", "TOTAL FEES COLLECTED"],
            printReportArr: [],
            filterData: [],
            showTable: true,
            previewStudentTableData: [],
            previewTableData: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPreviewRecord: 0,
            totalPages: 0,
            previewpage: 1,
            previewlimit: 10,
            previewtotalRecord: 0,
            previewtotalPages: 0,
            noData: false,
            isLoader: false,
            particularItem: undefined,
            programIdReport: '',
            appDatas: [
                {
                    label: "All Program Plan",
                    value: "All"
                }
            ],
            campusData : [
                {
                    label: "All Campus",
                    value: "All"
                }
            ],
            sectionData : [
                {
                    label: "All Section",
                    value: "All"
                }
            ],
            filterKey: "All",
            printable: false,
            searchValue: ''
        }
    }
    static contextType = DateFormatContext;
    onPaginationChange = (page, limit, data) => {
        this.setState({ page: page, limit: limit }, () => {
            if (this.state.searchValue.length == 0) {
                if (this.state.paginationCall === "allData") this.onDataCall();
                else if (this.state.paginationCall === "filterData") this.onFilterCall(page, limit);
                else this.onDataCall()
            }
            else {
                this.searchHandle(this.state.searchValue)
            }
        });
        console.log(page, limit);
    };
    onPaginationChange2 = (page, limit, datas) => {
        let data = JSON.parse(datas)
        this.setState({ page: page, limit: limit }, () => {
            // this.onDataCall();
            let current_page = Number(page);
            let perPage = Number(limit);
            let total_pages = Math.ceil(data.length / perPage);
            console.log("total", total_pages)
            current_page = total_pages < current_page ? total_pages : current_page
            // let offset = (current_page - 1) * perPage;
            let lastIndex = current_page * perPage;
            let startIndex = lastIndex - perPage;
            let paginatedItems = data.slice(startIndex, lastIndex)

            this.setState({ previewpage: current_page, previewlimit: perPage, previewtotalPages: total_pages, previewtotalRecord: data.length });
            this.setState({ previewTableData: paginatedItems });
        });
        console.log(page, limit, this.state.previewtotalPages);
    };
    componentDidMount() {
        this.onDataCall();
        this.filterDataNew();
    }
    onDataCall = () => {
        this.setState({ isLoader: true })
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ printReportArr: [], paginationCall: "allData" }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/programPlanStatement?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&classbatchName=${this.state.filterKey}&page=${this.state.page}&limit=${this.state.limit}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token"),
                }
            })
                .then(resp => {
                    console.log(resp);
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    this.setState({ printReportArr: resp.data.data, filterData: resp.data.data, totalPages: resp.data.totalPages, totalRecord: resp.data.totalRecord, noData })

                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true })
                })

        })
    }
    filterDataNew = () => {
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        axios.get(`${this.state.env['zqBaseUri']}/edu/reports/programPlanStatement?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}`, {
            headers: {
                'Authorization': localStorage.getItem("auth_token"),
            }
        }).then(resp => {
            let dropDownData = [{
                "value": "All",
                "label": "All Program Plan"
            }]
            let allData = resp.data.data
            let programPlanData = Array.from(new Set(allData.map(a => a['PROGRAM NAME']))) //getting all Department names in array
            programPlanData.map((item, index) => {
                dropDownData.push({
                    "value": item == undefined ? "-" : item,
                    "label": item == undefined ? "-" : item
                })
            })
            this.setState({ appDatas: dropDownData })
            this.setState({ isLoader: false })
        })
            .catch(err => {
                console.log(err);
                this.setState({ isLoader: false, appDatas: [], noData: true })
            })

    }
    onDownloadEvent = () => {
        this.setState({ isLoader: true })
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        axios.get(`${this.state.env['zqBaseUri']}/edu/reports/programPlanStatement?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&classbatchName=${this.state.filterKey}`, {
            headers: {
                'Authorization': localStorage.getItem("auth_token")
            }
        })
            .then(resp => {
                console.log("student fee", resp);
                var createXlxsData = [];
                var createXlxsDataItem = [];
                if (this.state.particularItem == undefined) {
                    resp.data.data.map(item => {
                        createXlxsData.push({
                            "PROGRAM ID": item['PROGRAM ID'],
                            "PROGRAM NAME": item['PROGRAM NAME'],
                            // "PROGRAM FEE (INR)": this.formatAmount(item["PROGRAM FEE"]),
                            "TOTAL STUDENTS": item["TOTAL STUDENTS"],
                            // "TOTAL FEES (INR)": this.formatAmount(item["TOTAL FEES"]),
                            "TOTAL FEES COLLECTED (INR)": this.formatAmount(item["TOTAL FEES COLLECTED"]),
                            // "BALANCE (INR)": this.formatAmount(item["BALANCE"]),
                        })
                        console.log('**DATA**', item)

                    })
                    var ws = xlsx.utils.json_to_sheet(createXlxsData);
                    var wb = xlsx.utils.book_new();
                    xlsx.utils.book_append_sheet(wb, ws, "Program Plan Statement");
                    xlsx.writeFile(wb, "program_plan_statement.xlsx");
                    //xlsx.writeFile(wb, "fee_pending.csv");
                    this.setState({ isLoader: false })
                }
                else {
                    // var particularData = resp.data.data[this.state.particularItem];
                    var particularData = resp.data.data.filter(task => {
                        return task["PROGRAM ID"] == this.state.previewStudentTableData[0]['PROGRAM ID']
                    })
                    createXlxsData.push({
                        "PROGRAM ID": particularData[0]['PROGRAM ID'],
                        "PROGRAM NAME": particularData[0]['PROGRAM NAME'],
                        // "PROGRAM FEE (INR)": this.formatAmount(particularData[0]["PROGRAM FEE"]),
                        "TOTAL STUDENTS (INR)": particularData[0]["TOTAL STUDENTS"],
                        // "TOTAL FEES (INR)": this.formatAmount(Number(particularData[0]["TOTAL FEES"])),
                        "TOTAL FEES COLLECTED (INR)": this.formatAmount(Number(particularData[0]["TOTAL FEES COLLECTED"])),
                        // "BALANCE (INR)": this.formatAmount(Number(particularData[0]["BALANCE"])),
                    })
                    particularData[0].items.map((dataOne, c) => {
                        if (String(dataOne.name).toLowerCase() != "total") {
                            let transactionDate;
                            if (dataOne["TRANSACTION DATE"]) {
                                if (dataOne["TRANSACTION DATE"].includes("/")) { transactionDate = dataOne["TRANSACTION DATE"].split("/") }
                                else if (dataOne["TRANSACTION DATE"].includes("-")) { transactionDate = dataOne["TRANSACTION DATE"].split("-") }
                            }
                            createXlxsDataItem.push({
                                "TRANSACTION NO": dataOne["TRANSACTION NO"],
                                "DEMAND NOTE NO": dataOne["DEMAND NOTE NO"],
                                "TRANSACTION DATE": moment(new Date(transactionDate[2], transactionDate[1] - 1, transactionDate[0])).format(this.context.dateFormat),
                                "STUDENT NAME": dataOne["STUDENT NAME"],
                                "PARTICULARS": dataOne["PARTICULARS"],
                                // "DUE AMOUNT (INR)": this.formatAmount(Number(dataOne["DUE AMOUNT"])),
                                "PAID AMOUNT (INR)": this.formatAmount(Number(dataOne["PAID AMOUNT"])),
                                "BALANCE (INR)": this.formatAmount(Number(dataOne["BALANCE"]))
                            })
                        }
                    })
                    var ws = xlsx.utils.json_to_sheet(createXlxsData);
                    var wsf = xlsx.utils.json_to_sheet(createXlxsDataItem);
                    var wb = xlsx.utils.book_new();
                    xlsx.utils.book_append_sheet(wb, ws, "Program Plan Statement");
                    xlsx.utils.book_append_sheet(wb, wsf, "Statement of Payment");
                    xlsx.writeFile(wb, "programplan_statement.xlsx");
                    this.setState({ isLoader: false })
                }
            })
            .catch(err => {
                console.log(err, 'err0rrrrr')
                this.setState({ isLoader: false })
            })


    }
    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    formatAmount = (amount) => {
        let a = Number(amount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })
        let b = a.replace('₹', "")
        return b
    }
    onCheckbox = (selectedItem) => {
        console.log('selectedItem', selectedItem)
        this.setState({ selectedData: selectedItem })
        var amountInit = 0
        if (selectedItem.length > 0) {
            selectedItem.map(item => {
                if (item.Description != 'Total') {
                    amountInit = amountInit + item['Amount']
                }
            })
        }
        this.setState({ amount: amountInit })
        this.setState({ amountFormat: this.formatCurrency(amountInit) })
    }
    showProgramPlanStmtPreview = (previewItem, index) => {
        this.setState({
            previewpage: 1,
            previewlimit: 10,
            programIdReport: previewItem['PROGRAM ID']
        });
        let preview = previewItem.items && previewItem.items.length > 0 ? previewItem.items : []
        this.setState({ particularItem: ((Number(this.state.page) - 1) * 10) + Number(index) });
        let previewArray = [
            {
                "PROGRAM ID": previewItem['PROGRAM ID'],
                "PROGRAM NAME": previewItem['PROGRAM NAME'],
                // "PROGRAM CODE": previewItem["PROGRAM CODE"],
                // "PROGRAM FEE": previewItem["PROGRAM FEE"],
                "TOTAL STUDENTS": previewItem["TOTAL STUDENTS"],
                // "TOTAL FEES": previewItem["TOTAL FEES"],
                "TOTAL FEES COLLECTED": previewItem["TOTAL FEES COLLECTED"],
                // "BALANCE": previewItem["BALANCE"]
            }
        ]
        localStorage.setItem('previewTableData', JSON.stringify(preview))
        let current_page = Number(this.state.previewpage);
        let perPage = Number(this.state.previewlimit);
        let total_pages = Math.ceil(preview.length / perPage);

        current_page = total_pages < current_page ? total_pages : current_page
        // let offset = (current_page - 1) * perPage;
        let lastIndex = current_page * perPage;
        let startIndex = lastIndex - perPage;
        let paginatedItems = preview.slice(startIndex, lastIndex)
        this.setState({
            previewStudentTableData: previewArray,
            previewTableData: paginatedItems,
            previewtotalPages: total_pages,
            previewtotalRecord: preview.length
        }, () => {
            console.log(this.state.previewStudentTableData)
            this.setState({ showTable: false });
        });
    }
    handleBack = () => {
        this.setState({ particularItem: undefined })
        if (this.state.showTable) {
            this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`);
        }
        else {
            this.setState({ showTable: true, page: 1 });
        }
    }
    searchHandle = (searchValue) => {
        this.setState({ isLoader: true })
        // console.log('searchValue', searchValue)
        this.setState({ searchValue: searchValue });
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        // this.setState({
        //     printReportArr: [],
        //     dataInfo: "Fetching Data..."
        // }, () => {
        if (String(searchValue).length > 0) {
            return axios.get(`${this.state.env['zqBaseUri']}/edu/reports/programPlanStatement?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&page=${this.state.page}&limit=${this.state.limit}&classbatchName=${this.state.filterKey}&searchKey=${searchValue}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    this.setState({
                        printReportArr: resp.data.data,
                        totalPages: resp.data.totalPages,
                        totalRecord: resp.data.totalRecord,
                        noData
                    })

                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true, dataInfo: "No Data" })
                })
        } else {
            this.onDataCall()
        }
        // })

    }
    onSelectPrgmPlan = (value, item, label) => {
        let selectedValue = value
        this.setState({ filterKey: selectedValue })
        if (value === "All") {
            this.onDataCall()
        } else {
            this.onFilterCall(1, 10)
        }
    }
    onFilterCall = (page, limit) => {
        this.setState({ isLoader: true, paginationCall: "filterData", page: page, limit: limit })
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ printReportArr: [] }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/programPlanStatement?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&classbatchName=${this.state.filterKey}&page=${this.state.page}&limit=${this.state.limit}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token"),
                }
            })
                .then(resp => {
                    console.log(resp);
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    this.setState({ printReportArr: resp.data.data, filterData: resp.data.data, totalPages: resp.data.totalPages, totalRecord: resp.data.totalRecord, noData })
                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true })
                })

        })
    }
    onSelectClean = () => {
        this.setState({ filterKey: 'All' })
    }
    printScreen = () => {
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ isLoader: false }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/programPlanStatement?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&classbatchName=${this.state.filterKey}&pagination=false`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    console.log(resp);
                    this.setState({
                        printable: true,
                        printReportArr: resp.data.data
                    }, () => {
                        this.setState({ printable: false })
                        window.print();
                        this.onDataCall();
                    })

                })
        });
    }
    render() {
        return (<React.Fragment >
            {this.state.isLoader && <Loader />}
            {/* { this.state.demandNoteTable.length > 0 ? <KenTable tableData={this.state.demandNoteTable} onCheckbox={this.onCheckbox} checkBox={true} /> : null} */}
            <div className="reports-student-fees list-of-students-mainDiv">
                {/* <div className="student-report-header-title">
                    <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.handleBack} style={{ marginRight: 10, cursor: 'pointer' }} />
                    {this.state.showTable ?
                        <p className="top-header-title">Report | Program Plan Statement</p> :
                        <p className="top-header-title">Program Plan Statement | {this.state.programIdReport}</p>}
                </div> */}
                <div className="trial-balance-header-title">
                    <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.handleBack} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                    {this.state.showTable ?
                        <p className="top-header-title">| Program Plan Statement</p> :
                        <p className="top-header-title">| Program Plan Statement | {this.state.programIdReport}</p>}
                </div>
                <div className="reports-body-section print-hd">
                    <React.Fragment>
                        <ContainerNavbar 
                        containerNav={this.state.containerNav} 
                        onDownload={() => this.onDownloadEvent()} 
                        searchValue={(searchValue) => this.searchHandle(searchValue)} 
                        Selectdata={this.state.appDatas} 
                        SelectCampusdata = {this.state.campusData}
                        SelectSectionData={this.state.sectionData}
                        onSelectDefaultValue={this.state.filterKey}
                        onSelectCampusDefaultValue={this.state.filterKey}
                        onSelectClean={this.onSelectClean} 
                        onSelectChange={this.onSelectPrgmPlan} 
                        printScreen={this.printScreen} />
                        <div className="print-time">{this.state.printTime}</div>
                    </React.Fragment>
                    {/* <p className="statement-date-time">Date: {<DateFormatter date={new Date()} format={this.context.dateFormat} />}</p> */}
                    <div className="reports-data-print-table">
                        <div className="">
                            {this.state.showTable ?
                                <React.Fragment>
                                    <table className="transaction-table-review reports-tableRow-header">
                                        <thead>
                                            <tr>
                                                {this.state.tableHeader.map((data, i) => {
                                                    return <th className={"first-child-space demand-note " + String(data).replace(' ', '')} key={i + 1}>{data}</th>
                                                })}
                                            </tr>
                                        </thead>
                                        {this.state.printReportArr.length > 0 ?
                                            <tbody>
                                                {this.state.printReportArr.map((data, i) => {
                                                    return (<tr key={i + 1} id={i + 1} className="reports-tableRow-body"
                                                        onClick={(e) => { e.preventDefault(); this.showProgramPlanStmtPreview(data, i) }}
                                                        style={{ cursor: "pointer" }}>
                                                        {/* <td className="transaction-sno">{data.txnId}</td> */}
                                                        <td className="transaction-vch-num" >{data["PROGRAM ID"]}</td>
                                                        <td className="transaction-vch-num" >{data["PROGRAM NAME"]}</td>
                                                        {/* <td className="transaction-vch-num" >{data["PROGRAM CODE"]}</td> */}
                                                        {/* <td className="transaction-vch-type ppstmt-amount-right-align">{this.formatCurrency(Number(data["PROGRAM FEE"]))}</td> */}
                                                        <td className="transaction-vch-num" >{data["TOTAL STUDENTS"]}</td>
                                                        {/* <td className="transaction-vch-type ppstmt-amount-right-align">{this.formatCurrency(Number(data["TOTAL FEES"]))}</td> */}
                                                        <td className="transaction-vch-type ppstmt-amount-right-align">{this.formatCurrency(Number(data["TOTAL FEES COLLECTED"]))}</td>
                                                        {/* <td className="transaction-vch-type ppstmt-amount-right-align">{this.formatCurrency(Number(data["BALANCE"]))}</td> */}
                                                    </tr>
                                                    )
                                                })}
                                            </tbody> :
                                            <tbody>
                                                <tr>
                                                    {this.state.noData ? <td className="noprog-txt" colSpan={this.state.tableHeader.length}>No data...</td> :
                                                        <td className="noprog-txt" colSpan={this.state.tableHeader.length}>Fetching the data...</td>
                                                    }

                                                </tr></tbody>
                                        }
                                    </table>
                                    <div>
                                        {this.state.printReportArr.length == 0 ? null :
                                            <PaginationUI
                                                total={this.state.totalRecord}
                                                onPaginationApi={this.onPaginationChange}
                                                totalPages={this.state.totalPages}
                                                limit={this.state.limit}
                                                currentPage={this.state.page}
                                            />}
                                    </div>
                                </React.Fragment>
                                :
                                <React.Fragment>
                                    <table className="transaction-table-review reports-tableRow-header">
                                        <thead>
                                            <tr>
                                                {Object.keys(this.state.previewStudentTableData[0]).map((data, i) => {
                                                    return <th className={"first-child-space demand-note " + String(data).replace(' ', '')} key={i + 1}>{data}</th>
                                                })}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.previewStudentTableData.map((data, i) => {
                                                return (<tr key={i + 1} id={i + 1} className="reports-tableRow-body">
                                                    <td className="transaction-vch-num" >{data["PROGRAM ID"]}</td>
                                                    <td className="transaction-vch-num" >{data["PROGRAM NAME"]}</td>
                                                    {/* <td className="transaction-vch-num" >{data["PROGRAM CODE"]}</td> */}
                                                    {/* <td className="transaction-vch-type ppstmt-amount-right-align">{this.formatCurrency(Number(data["PROGRAM FEE"]))}</td> */}
                                                    <td className="transaction-vch-num" >{data["TOTAL STUDENTS"]}</td>
                                                    {/* <td className="transaction-vch-type ppstmt-amount-right-align">{this.formatCurrency(Number(data["TOTAL FEES"]))}</td> */}
                                                    <td className="transaction-vch-type ppstmt-amount-right-align">{this.formatCurrency(Number(data["TOTAL FEES COLLECTED"]))}</td>
                                                    {/* <td className="transaction-vch-type ppstmt-amount-right-align">{this.formatCurrency(Number(data["BALANCE"]))}</td> */}
                                                </tr>
                                                )
                                            })}
                                        </tbody>
                                    </table>
                                    <p className="statement-of-payment-text">Statement of Payment</p>
                                    {this.state.previewTableData.length == 0 ?
                                        <p style={{ fontSize: 16, textAlign: 'center', marginTop: 30 }}>No Statement</p>
                                        :
                                        <React.Fragment>
                                            <table className="transaction-table-review reports-tableRow-header">
                                                <thead>
                                                    <tr>
                                                        {Object.keys(this.state.previewTableData[0]).map((data, i) => {
                                                            return <th className={"demand-note " + String(data).replace(' ', '')} key={i + 1}>{data}</th>
                                                        })}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {this.state.previewTableData.map((data, i) => {
                                                        let transactionDate;
                                                        if (data["TRANSACTION DATE"]) {
                                                            if (data["TRANSACTION DATE"].includes("/")) { transactionDate = data["TRANSACTION DATE"].split("/") }
                                                            else if (data["TRANSACTION DATE"].includes("-")) { transactionDate = data["TRANSACTION DATE"].split("-") }
                                                        }
                                                        return (<tr key={i + 1} id={i + 1} className="reports-tableRow-body">
                                                            {/* style={{ cursor: "pointer" }} */}
                                                            <td className="transaction-vch-num" >{data["TRANSACTION NO"]}</td>
                                                            <td className="transaction-vch-num" >{data["RECEIPT NO"]}</td>
                                                            <td className="transaction-vch-num" >{String(data["DEMAND NOTE NO"]).length == 0 ? '-' : data["DEMAND NOTE NO"]}</td>
                                                            {/* <td className="transaction-vch-num" ><DateFormatter date={data["TRANSACTION DATE"]} format={this.context.dateFormat} /></td> */}
                                                            <td className="transaction-vch-num" >{data["TRANSACTION DATE"] && data["TRANSACTION DATE"] !== "NA" && data["TRANSACTION DATE"] !== "-" ? <DateFormatter date={new Date(transactionDate[2], transactionDate[1] - 1, transactionDate[0])} format={this.context.dateFormat} /> : "-"}</td>
                                                            <td className="transaction-vch-num" >{data["STUDENT NAME"]}</td>
                                                            <td className="transaction-vch-num" >{data["PARTICULARS"]}</td>
                                                            {/* <td className="transaction-vch-num ppstmt-amount-right-align" >{this.formatCurrency(Number(data["DUE AMOUNT"]))}</td> */}
                                                            <td className="transaction-vch-type ppstmt-amount-right-align">{this.formatCurrency(Number(data["PAID AMOUNT"]))}</td>
                                                            <td className="transaction-vch-type ppstmt-amount-right-align">{this.formatCurrency(Number(data["BALANCE"]))}</td>
                                                        </tr>
                                                        )
                                                    })}
                                                </tbody>
                                            </table> <div>
                                                {this.state.previewTableData.length == 0 ? null :
                                                    <PaginationUI
                                                        total={this.state.previewtotalRecord}
                                                        onPaginationApi={this.onPaginationChange2}
                                                        totalPages={this.state.previewtotalPages}
                                                        limit={this.state.previewlimit}
                                                        currentPage={this.state.previewpage}
                                                        allData={localStorage.getItem('previewTableData')}
                                                    />}
                                            </div> </React.Fragment>}
                                </React.Fragment>}
                        </div>

                    </div>
                </div>
            </div>
        </React.Fragment >);
    }
}

export default programPlanStmtReport;