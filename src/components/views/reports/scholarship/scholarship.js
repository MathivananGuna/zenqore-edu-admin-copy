import React, { Component } from 'react';
import '../../../../scss/student.scss';
import '../../../../scss/setup.scss';
// import '../../../../scss/fees-type.scss';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../utils/Table/table-component";
import PaginationUI from "../../../../utils/pagination/pagination";
import Loader from "../../../../utils/loader/loaders";
import sholarshipDataJson from './scholarship.json';
import ZenForm from "../../../input/zqform";
import ZenTabs from "../../../input/tabs";
import xlsx from 'xlsx';
import axios from 'axios';
import moment from 'moment';
// import ScholarData from "./scholar-previe.json";

class Scholarships extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orgId: localStorage.getItem('orgId'),
      userId: localStorage.getItem('userId'),
      env: JSON.parse(localStorage.getItem('env')),
      authToken: localStorage.getItem('auth_token'),
      containerNav: {
        isBack: false,
        name: "List of Scholarships",
        isName: true,
        isSearch: true,
        isSort: false,
        isPrint: true,
        isDownload: true,
        isShare: false,
        isNew: false,
        newName: "New",
        isSubmit: false,
        isSelectAppReport: true,
        selectPickerOption: true,
        selectCampusOption: true,
        selectSectionOption:true,
        selectFranchiseOption: false
      },
      tablePrintDetails: [],
      tabView: true,
      scholarFormData: [],
      page: 1,
      limit: 10,
      totalRecord: 0,
      totalPages: 1,
      scholarId: '',
      appDatas: [
        {
          label: "All Progrm Plan",
          value: "All"
        }
      ],
      campusData : [
        {
            label: "All Campus",
            value: "All"
        }
    ],
    sectionData : [
        {
            label: "All Section",
            value: "All"
        }
    ],
      filterKey: "All",
      listOfStudentData: [],
      noProg: "Fetching data..."
    }
  }
  componentDidMount() {
    this.getScholarshipData()
  }
  getScholarshipData = () => {
    this.setState({ LoaderStatus: true }, () => {
      axios({
        method: 'get',
        url: `${this.state.env["zqBaseUri"]}/edu/transactions/scholarships?orgId=${this.state.orgId}&page=${this.state.page}&limit=${this.state.limit}`,
        headers: {
          'Authorization': this.state.authToken
        }
      })
        .then(res => {
          console.log(res.data)
          let regId = this.context.reportLabel || "REG ID"
          let batch = this.context.classLabel || "CLASS/BATCH"
          if (res.data.status) {
            if (res.data.data.length == 0) {
              this.setState({ noProg: "No Data", LoaderStatus: false, datas: [] })
            }
            else {
              let datas = [];
              this.setState({ listOfStudentData: [] }, () => {
                res.data.data.map((item) => {
                  datas.push({
                    "Id": item.displayName,
                    [regId]: item.studentRegId,
                    "Student Name": item.studentName,
                    [batch]: item.class,
                    "Academic Year": item.academicYear,
                    "Scholarship Provider": item.data.scholarshipProvider,
                    "Amount Sanctioned": this.formatCurrency(Number(item.amount)),
                    "Amount Received": this.formatCurrency(Number(item.amount)),
                    "Mode of payment": item.data.modeofPayment,
                    "Created On": item.createdAt !== "" ? this.context.dateFormat !== "" ? moment(item.createdAt).format(this.context.dateFormat) : moment(item.createdAt).format('DD/MM/YYYY') : "-",
                    "Item": JSON.stringify(item),
                    "Status": "Sanctioned",
                  })
                })
                console.log(datas);
                this.setState({
                  listOfStudentData: datas,
                  totalPages: res.data.totalPages,
                  page: res.data.page,
                  totalRecord: res.data.totalRecord,
                  LoaderStatus:false
                })
              })
            }
          }
        })
        .catch(err => {
          this.setState({ noProg: "No Data", tablePrintDetails: [], previewList: false, LoaderStatus: false })
        })
    })
  }
  formatCurrency = (amount) => {
    return (new Intl.NumberFormat('en-IN', {
        style: 'currency',
        currency: 'INR',
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    }).format(amount))
}
  onAddNewLoans = () => { }
  onPreviewScholarship = (item) => {
    this.setState({ tabView: true, scholarId: item.ID })
    let scholarFormData = this.state.scholarFormData;
    let details = item;
    let objKeys = Object.keys(details);
    // let entityInstanceId = details["_id"];
    console.log(details);
    this.setState({
      Name: item['Designation Name'],
      departmentId: item.ID
      //   idosId: details["idos_id"],
      //   entityInstanceId: entityInstanceId,
    });
    this.setState({ tabview: false, preview: true });
    objKeys.map((key) => {
      Object.keys(scholarFormData).forEach(dataKey => {
        scholarFormData[dataKey].map(item => {
          // let keyName = key.toLowerCase().replace(/ /g, "");
          // let name = String(details['Department Head']).split()
          if (item.name == "ID") {
            item["defaultValue"] = details['ID'];
            item["readOnly"] = false;
            item["required"] = false;
          }
          if (item['name'] == 'RegID') {
            item['defaultValue'] = details['REG ID']
            item['readOnly'] = true
            item['required'] = false
          }
          if (item['name'] == 'StudentName') {
            item['defaultValue'] = details['STUDENT NAME']
            item['readOnly'] = true
            item['required'] = false
          }
          if (item['name'] == 'ProgramPlan') {
            item['defaultValue'] = details['Program Plan']
            item['readOnly'] = true
            item['required'] = false
          }
          if (item['name'] == 'YearOfJoin') {
            item['defaultValue'] = details['Year of Joining']
            item['readOnly'] = true
            item['required'] = false
          }
          else if (item.name == "scholarshiptype") {
            item["defaultValue"] = 'Private';
            item["readOnly"] = false;
            item["required"] = false;
          }
          else if (item.name == "scholarshipprovider") {
            item["defaultValue"] = details['Scholarship Provider']
            item["readOnly"] = false;
            item["required"] = false;
          }
          else if (item.name == "modeofPayment") {
            item["defaultValue"] = details['Mode of payment'];
            item["readOnly"] = false;
            item["required"] = false;
          }
          else if (item.name == "scholarshipamountsanctioned") {
            item["defaultValue"] = details['Amount Sanctioned'];
            item["readOnly"] = false;
            item["required"] = false;
          }
          else if (item.name == "scholarshipamountreceived") {
            item["defaultValue"] = details['Amount Received'];
            item["readOnly"] = false;
            item["required"] = false;
          }
          else if (item.name == "status") {
            item["defaultValue"] = details['Status'];
            item["readOnly"] = false;
            item["required"] = false;
          }
        })
      })
    })
    this.setState({ scholarFormData });

  };
  onPaginationChange = (page, limit) => {
    this.setState({ page: page, limit: limit }, () => {
      this.getDesignations();
    });
  };
  onPreviewStudentList1 = () => { }
  formBtnHandle = (item) => {
    return item.type === "clear" ? this.cleartabData() : this.canceltabData();
  };
  cleartabData = () => {
    let issueDetailData = [];
    let formData = this.state.ServiceFormData;
    this.setState({ ServiceFormData: [] }, () => {
      formData.map((item) => {
        item["defaultValue"] = undefined;
        item["readOnly"] = false;
        if (item.required) {
          item["validation"] = false;
        }
      });
      issueDetailData = formData;
      console.log(issueDetailData, "issueDetailData");
      this.setState({
        ServiceFormData: issueDetailData,
      });
    });
  };
  canceltabData = () => {
    this.resetform();
    this.setState({ tabview: true });
  };
  onInputChanges = () => {

  }
  moveBackTable = () => {

    this.setState({ tabView: true });

  }
  handleTabChange = (tabName, formDatas) => { }
  searchHandle = () => { }
  onSelectPrgmPlan = (value, item, label) => {
    let selectedValue = value
    this.setState({ filterKey: selectedValue })
    //this.onDataCall()
  }
  onSelectClean = () => {
    this.setState({ filterKey: 'All' })
  }
  onDownloadEvent = () => {
    this.setState({ isLoader: true })
    var createXlxsData = []
    var ws = xlsx.utils.json_to_sheet(createXlxsData);
    var wb = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(wb, ws, "Scholarship Reports");
    xlsx.writeFile(wb, "scholarship_reports.xlsx");
    //xlsx.writeFile(wb, "demand_note_reports.csv");
    this.setState({ isLoader: false })
    // axios.get(`${this.state.env['zqBaseUri']}/edu/reports/demandNote?orgId=${this.state.orgId}`, {
    //     headers: {
    //         'Authorization': localStorage.getItem("auth_token")
    //     }
    // })
    //     .then(resp => {
    //         console.log(resp);
    //         var createXlxsData = []
    //         resp.data.data.map(data => {
    //             data.data.students[0].feesBreakup.map((dataOne, c) => {
    //                 if (String(dataOne.description).toLowerCase() != "total") {
    //                     createXlxsData.push({
    //                         "DEMAND NOTE ID": data.displayName,
    //                         "REG ID": data.data.students[0].regId,
    //                         "STUDENT NAME": data.data.students[0].studentName,
    //                         "ACADEMIC YEAR": data.data.students[0].academicYear,
    //                         "CLASS/BATCH": data.data.students[0].class,
    //                         // "ISSUED DATE": moment(new Date(data.todayDate)).format(this.context.dateFormat),
    //                         "ISSUED DATE": data.todayDate,
    //                         "DUEDATE": moment(new Date(data.data.students[0].dueDate)).format(this.context.dateFormat),
    //                         "DESCRIPTION": dataOne.description,
    //                         "AMOUNT": this.formatCurrency(dataOne.amount),
    //                         "STATUS": dataOne.status
    //                     })
    //                 }
    //             })
    //             console.log('**DATA**', data)

    //         })
    //         var ws = xlsx.utils.json_to_sheet(createXlxsData);
    //         var wb = xlsx.utils.book_new();
    //         xlsx.utils.book_append_sheet(wb, ws, "Scholarship Reports");
    //         xlsx.writeFile(wb, "scholarship_reports.xlsx");
    //         //xlsx.writeFile(wb, "demand_note_reports.csv");
    //         this.setState({ isLoader: false })
    //     })
    //     .catch(err => {
    //         console.log(err, 'err0rrrrr')
    //         this.setState({ isLoader: false })
    //     })
  }
  render() {
    return (
      <div className="list-of-students-mainDiv">
        {this.state.LoaderStatus == true ? <Loader /> : null}
        {this.state.tabView ?
          <React.Fragment>
            <div className="trial-balance-header-title">
              <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
              <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Scholarships</p>
            </div>

            <div className="masters-body-div">
              <React.Fragment>
                <ContainerNavbar 
                containerNav={this.state.containerNav} 
                onAddNew={() => { this.onAddNewLoans(); }} 
                searchValue={(searchValue) => this.searchHandle(searchValue)} 
                Selectdata={this.state.appDatas} 
                SelectCampusdata = {this.state.campusData}
                SelectSectionData={this.state.sectionData}
                onSelectDefaultValue={this.state.filterKey}
                onSelectCampusDefaultValue={this.state.filterKey}
                onSelectClean={this.onSelectClean} 
                onDownload={() => this.onDownloadEvent()} 
                onSelectChange={this.onSelectPrgmPlan} />
              </React.Fragment>
              <div className="remove-last-child-table">
                {this.state.listOfStudentData.length > 0 ?
                  <ZqTable
                    allSelect={this.allSelect}
                    data={this.state.listOfStudentData}
                    rowClick={(item) => { this.onPreviewScholarship(item) }}
                    onRowCheckBox={(item) => { this.onPreviewStudentList1(item) }}
                  /> :
                  <p className="noprog-txt">{this.state.noProg}</p>}
              </div>
              <div className="setup-pagination">

                {/* <PaginationUI
                      total={this.state.totalRecord}
                      onPaginationApi={this.onPaginationChange}
                      totalPages={this.state.totalPages}
                      limit={this.state.limit}
                    /> */}

              </div>

            </div></React.Fragment>
          :
          <React.Fragment>
            <div className="tab-form-wrapper tab-table">
              <div className="preview-btns">
                <div className="preview-header-wrap">
                  < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                  <h6 className="preview-Id-content" onClick={this.moveBackTable}>| Scholarships | {this.state.scholarId}</h6>
                </div>
                {/* <div className="notes-history-wrapper" style={{ right: "86px" }}>
                                        <div className="history-icon-box" title="View History" onClick={this.openHistory}> <RestoreOutlinedIcon className="material-historyIcon" /> <p>History</p></div>
                                    </div> */}
              </div>
              <div className="organisation-table student-submit-none">
                {/* <ZenTabs tabData={this.state.scholarFormData} className="preview-wrap" tabEdit={this.state.preview} form={this.state.scholarFormData} value={0} onInputChanges={this.onInputChanges} onFormBtnEvent={(item) => { this.formBtnHandle(item); }} onTabFormSubmit={this.onFormSubmit} handleTabChange={this.handleTabChange} key={0} /> */}
              </div>
            </div>
          </React.Fragment>
          //     <React.Fragment>
          //     <div className="tab-form-wrapper tab-table">
          //         <div className="preview-btns">
          //             <div className="preview-header-wrap">
          //                 < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop:1 }}  />
          //                 <h6 className="preview-Id-content" onClick={this.moveBackTable}>| Scholarships | {this.state.scholarId}</h6>
          //             </div>
          //             {/* <div className="notes-history-wrapper">
          //                 <div className="history-icon-box" title="View History" onClick={this.showHistory}> <RestoreOutlinedIcon className="material-historyIcon" /> <p>History</p></div>
          //             </div> */}
          //         </div>
          //         <div className="goods-header">
          //             <p className="info-input-label">SCHOLARSHIP DETAILS</p>
          //         </div>
          //         <div className="goods-wrapper goods-parent-wrap">
          //             {this.state.viewType == "preview" ?
          //                 <ZenForm
          //                     formData={this.state.scholarFormData}
          //                     className="goods-wrap"
          //                     onInputChanges={(e, a) => this.onInputChanges(e, a)}
          //                     onFormBtnEvent={(item) => {
          //                         this.formBtnHandle(item);
          //                     }}
          //                     clear={true}
          //                     onSubmit={(e) => this.onSubmit("preview", e.target.elements)}
          //                 /> : <ZenForm
          //                     formData={this.state.scholarFormData}
          //                     className="goods-wrap"
          //                     onInputChanges={(e, a) => this.onInputChanges(e, a)}
          //                     onFormBtnEvent={(item) => {
          //                         this.formBtnHandle(item);
          //                     }}
          //                     clear={true}
          //                     onSubmit={(e) => this.onSubmit("addNew", e.target.elements)}
          //                 />}
          //         </div>
          //     </div>
          // </React.Fragment>
        }





      </div>




    )
  }
}
export default Scholarships;