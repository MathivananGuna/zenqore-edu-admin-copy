const feePendingResponse = {
    "message": "get fee collections by transactionSubType",
    "type": "success",
    "data": [
        {
            "programPlanId": "BKAH_Playgroup",
            "programPlanName": "Bright Kids @Home - Playgroup",
            "numberOfStudents": 3,
            "pendingStudents": 3,
            "totalFees": 59535,
            "totalFeesCollected": 5954,
            "totalPending": 45644,
            "_id": "5fa245b1b2f383000906afcb",
            "entityId": "5f7d689880505d2574aec1bd",
            "todayDate": "04/11/2020",
            "__v": 0,
            items: [
                {
                    "regId": "BKAH-001/001",
                    "studentName": "Siva",
                    "programPlanName": "Bright Kids @Home - Playgroup",
                    "totalInstallments": 4,
                    "paidInstallments": 1,
                    "pendingInstallments": 3,
                    "totalFees": 19845,
                    "totalPaid": 5954,
                    "totalPending": 13891
                }, {
                    "regId": "BKAH-001/002",
                    "studentName": "XYZ",
                    "programPlanName": "Bright Kids @Home - Playgroup",
                    "totalInstallments": 4,
                    "paidInstallments": 0,
                    "pendingInstallments": 4,
                    "totalFees": 19845,
                    "totalPaid": 0,
                    "totalPending": 19845
                }, {
                    "regId": "BKAH-001/003",
                    "studentName": "Abhi",
                    "programPlanName": "Bright Kids @Home - Playgroup",
                    "totalInstallments": 4,
                    "paidInstallments": 0,
                    "pendingInstallments": 4,
                    "totalFees": 19845,
                    "totalPaid": 0,
                    "totalPending": 19845
                }
            ]
        },
        {
            "programPlanId": "BKAH_Nursery",
            "programPlanName": "Bright Kids @Home - Nursery",
            "numberOfStudents": 3,
            "pendingStudents": 3,
            "totalFees": 66150,
            "totalFeesCollected": 0,
            "totalPending": 66150,
            "_id": "5fa245b1b2f383000906afcb",
            "entityId": "5f7d689880505d2574aec1bd",
            "todayDate": "04/11/2020",
            "__v": 0,
            items: [
                {
                    "regId": "BKAH-002/001",
                    "studentName": "Siva",
                    "programPlanName": "Bright Kids @Home - Nursery",
                    "totalInstallments": 4,
                    "paidInstallments": 0,
                    "pendingInstallments": 4,
                    "totalFees": 22050,
                    "totalPaid": 0,
                    "totalPending": 22050
                }, {
                    "regId": "BKAH-002/002",
                    "studentName": "XYZ",
                    "programPlanName": "Bright Kids @Home - Nursery",
                    "totalInstallments": 4,
                    "paidInstallments": 0,
                    "pendingInstallments": 4,
                    "totalFees": 22050,
                    "totalPaid": 0,
                    "totalPending": 22050
                }, {
                    "regId": "BKAH-002/003",
                    "studentName": "Abhi",
                    "programPlanName": "Bright Kids @Home - Nursery",
                    "totalInstallments": 4,
                    "paidInstallments": 0,
                    "pendingInstallments": 4,
                    "totalFees": 22050,
                    "totalPaid": 0,
                    "totalPending": 22050
                }
            ]
        },
        {
            "programPlanId": "BKAH_LKG",
            "programPlanName": "Bright Kids @Home - LKG",
            "numberOfStudents": 3,
            "pendingStudents": 3,
            "totalFees": 88830,
            "totalFeesCollected": 0,
            "totalPending": 88830,
            "_id": "5fa245b1b2f383000906afcb",
            "entityId": "5f7d689880505d2574aec1bd",
            "todayDate": "04/11/2020",
            "__v": 0,
            items: [
                {
                    "regId": "BKAH-003/001",
                    "studentName": "Siva",
                    "programPlanName": "Bright Kids @Home - LKG",
                    "totalInstallments": 4,
                    "paidInstallments": 0,
                    "pendingInstallments": 4,
                    "totalFees": 29610,
                    "totalPaid": 0,
                    "totalPending": 29610
                }, {
                    "regId": "BKAH-003/002",
                    "studentName": "XYZ",
                    "programPlanName": "Bright Kids @Home - LKG",
                    "totalInstallments": 4,
                    "paidInstallments": 0,
                    "pendingInstallments": 4,
                    "totalFees": 29610,
                    "totalPaid": 0,
                    "totalPending": 29610
                }, {
                    "regId": "BKAH-003/003",
                    "studentName": "Abhi",
                    "programPlanName": "Bright Kids @Home - LKG",
                    "totalInstallments": 4,
                    "paidInstallments": 0,
                    "pendingInstallments": 4,
                    "totalFees": 29610,
                    "totalPaid": 0,
                    "totalPending": 29610
                }
            ]
        },
        {
            "programPlanId": "BKAH_UKG",
            "programPlanName": "Bright Kids @Home - UKG",
            "numberOfStudents": 3,
            "pendingStudents": 3,
            "totalFees": 103950,
            "totalFeesCollected": 0,
            "totalPending": 103950,
            "_id": "5fa245b1b2f383000906afcb",
            "entityId": "5f7d689880505d2574aec1bd",
            "todayDate": "04/11/2020",
            "__v": 0,
            items: [
                {
                    "regId": "BKAH-004/001",
                    "studentName": "Siva",
                    "programPlanName": "Bright Kids @Home - UKG",
                    "totalInstallments": 4,
                    "paidInstallments": 0,
                    "pendingInstallments": 4,
                    "totalFees": 34650,
                    "totalPaid": 0,
                    "totalPending": 34650
                }, {
                    "regId": "BKAH-004/002",
                    "studentName": "XYZ",
                    "programPlanName": "Bright Kids @Home - UKG",
                    "totalInstallments": 4,
                    "paidInstallments": 0,
                    "pendingInstallments": 4,
                    "totalFees": 34650,
                    "totalPaid": 0,
                    "totalPending": 34650
                }, {
                    "regId": "BKAH-004/003",
                    "studentName": "Abhi",
                    "programPlanName": "Bright Kids @Home - UKG",
                    "totalInstallments": 4,
                    "paidInstallments": 0,
                    "pendingInstallments": 4,
                    "totalFees": 34650,
                    "totalPaid": 0,
                    "totalPending": 34650
                }
            ]
        }
    ]
}

export default feePendingResponse