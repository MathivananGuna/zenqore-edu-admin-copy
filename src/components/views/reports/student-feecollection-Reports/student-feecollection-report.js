import React, { Component } from "react";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import '../../../../scss/student-reports.scss';
import '../../../../scss/student.scss';
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import axios from 'axios';
import xlsx from 'xlsx';
import PaginationUI from "../../../../utils/pagination/pagination";
import '../../../../scss/common-table.scss';
import Loader from '../../../../utils/loader/loaders';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import DateFormatter from '../../../date-formatter/date-formatter';
import DateFormatContext from '../../../../gigaLayout/context';
import Button from '@material-ui/core/Button';
import Checkbox from "@material-ui/core/Checkbox";
import ZqTable from "../../../../utils/Table/table-component";

class StudentFeeCollection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            env: JSON.parse(localStorage.getItem('env')),
            orgId: localStorage.getItem('orgId'),
            authToken: localStorage.getItem("auth_token"),
            containerNav: {
                isBack: false,
                name: "List of Fees Collection",
                noProg: "Fetching...",
                isName: true,
                isSearch: true,
                isSort: false,
                isPrint: true,
                isDownload: true,
                isShare: false,
                isNew: false,
                newName: "New",
                isSubmit: false,
                isSelectAppReport: true,
                selectPickerOption: true
            },
            tableHeader: ["Receipt ID", "REG ID", "STUDENT NAME", "ACADEMIC YEAR", "CLASS/BATCH", "DESCRIPTION", "TOTAL FEES", "PAID", "PAID ON", "MODE", "PENDING", "TXN ID", "STATUS"],
            printReportArr: [],
            filterData: [],
            page: 1,
            limit: 5,
            totalRecord: 0,
            totalPages: 0,
            noData: false,
            isLoader: false,
            studenTableData: [],
            appDatas: [
                {
                    label: "All Collected Fees",
                    value: "All"
                }
            ],
            filterKey: "All",
            paginationCall: "",
            totalAmount: 0,
            totalPaidAmount: 0,
            totalPendingAmount: 0,
            typeOfPagination: '',
            fromDateData: '',
            toDateData: '',
            totalCashView: 0,
            totalChequeView: 0,
            totalCardView: 0,
            totalNetbankingView: 0,
            totalWalletView: 0,
            totalUpiView: 0,
        }
    }
    static contextType = DateFormatContext;
    componentDidMount() {

        this.onDataCall();
        this.filterDataNew();
        let tableHeader = ["Receipt ID", `${this.context.reportLabel ? this.context.reportLabel : "REG ID"}`, "STUDENT NAME", "ACADEMIC YEAR", `${this.context.classLabel ? this.context.classLabel : "CLASS/BATCH"}`, "DESCRIPTION", "TOTAL FEES", "PAID", "PAID ON", "MODE", "PENDING", "TXN ID", "STATUS"]
        this.setState({ tableHeader })
        axios.get(`${this.state.env['headSeller']}/bkah/edu/leads?orgId=${this.state.orgId}`,
            {
                headers: {
                    "Authorization": localStorage.getItem('auth_token')
                }
            }

        )
            .then(res => {
                let response = res.data.data
                console.log(response, "response")
                let brightKidTableData = []
                let TodayDate = new Date()
                response.forEach((item, index) => {
                    brightKidTableData.push({
                        "Receipt ID": `RCPT-0${index + 1}`,
                        "Application ID": item.applicationId,
                        "Student Name": item.student.firstName,
                        "Class/Batch": item.programdetail.programPlanName != undefined ? `Bright Kid ${item.programdetail.programPlanName}` : "Bright Kid @Home - Playgroup",
                        "Total fees": "19845.00",
                        "Paid": "5954.00",
                        "Paid on": this.dateFilter(TodayDate),
                        "mode": "Card",
                        " pending Amount": "13891.00",
                        "Transaction ID ": "BD0AC9FB00C62433",
                        "description": "Installment 1",
                        "Status": item.accountStatus.charAt(0).toUpperCase() + item.accountStatus.slice(1),
                        Item: JSON.stringify(item)

                    })
                })
                this.setState({ studenTableData: brightKidTableData, isLoader: false })
            })
            .catch(err => {
                console.log(err)
            })


    }
    filterDataNew = () => {
        axios.get(`${this.state.env['zqBaseUri']}/edu/reports/feePayment?orgId=${this.state.orgId}&page=${this.state.page}&limit=${this.state.limit}`, {
            headers: {
                'Authorization': localStorage.getItem("auth_token"),
            }
        })
            .then(resp => {

                let dropDownData = [{
                    "label": "All Collected Fees",
                    "value": "All"
                }]

                let allData = resp.data.data
                let programPlanData = Array.from(new Set(allData.map(a => a.classBatch))) //getting all Department names in array
                programPlanData.map((item, index) => {
                    dropDownData.push({
                        "value": item == undefined ? "-" : item,
                        "label": item == undefined ? "-" : item
                    })
                })
                this.setState({ appDatas: dropDownData, downloadTotalRecord: resp.data.totalRecord })

                this.setState({ isLoader: false })
            })
            .catch(err => {
                console.log(err);
                this.setState({ isLoader: false, appDatas: [], noData: true })
            })

    }
    onDataCall = () => {
        this.setState({ isLoader: true, paginationCall: "allData", })
        this.setState({ printReportArr: [] }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/feePayment?orgId=${this.state.orgId}&classbatchName=${this.state.filterKey}&page=${this.state.page}&limit=${this.state.limit}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token"),
                }
            })
                .then(resp => {
                    console.log(resp);
                    let noData;
                    if (resp.data.data.length === 0) {
                        this.setState({ noProg: "No Data" });
                    } else {
                        noData = false;
                    }
                    this.setState({
                        printReportArr: resp.data.data,
                        filterData: resp.data.data, noData,
                        totalPages: resp.data.totalPages,
                        page: resp.data.currentPage,
                        downloadTotalRecord: resp.data.totalRecord,
                        totalAmount: resp.data.totalAmount,
                        totalPaidAmount: resp.data.totalPaid,
                        totalPendingAmount: resp.data.totalPending,
                        totalCashView: resp.data.totalCash === undefined ? 0 : resp.data.totalCash,
                        totalChequeView: resp.data.totalCheque === undefined ? 0 : resp.data.totalCheque,
                        totalCardView: resp.data.totalCard === undefined ? 0 : resp.data.totalCard,
                        totalNetbankingView: resp.data.totalNetbanking === undefined ? 0 : resp.data.totalNetbanking,
                        totalWalletView: resp.data.totalWallet === undefined ? 0 : resp.data.totalWallet,
                        totalUpiView: resp.data.totalUpi === undefined ? 0 : resp.data.totalUpi,
                    })
                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true })
                    // var respErr = { "status": "success", "message": "feePayment reports", "data": [{ "studentName": "Adiba Nisar", "regId": "STUD_001", "academicYear": "2020-21", "classBatch": "BE_ENG_CSC_20-21", "DemandId": "DN_2020-21_001", "description": [{ "name": "Tuition Fee", "due": 145000, "paid": 145000, "paidDate": "12/11/2020", "balance": 0, "status": "Paid", "txnId": "5fae2d05daeb301acc833ec6" }, { "name": "Total", "due": 145000, "paid": 145000, "paidDate": "-", "balance": 0, "status": "Paid", "txnId": "-" }] }, { "studentName": "Abdul Nasar", "regId": "STUD_002", "academicYear": "2020-21", "classBatch": "BE_ENG_MEC_20-21", "DemandId": "DN_2020-21_002", "description": [{ "name": "Tuition Fee", "due": 125000, "paid": 125000, "paidDate": "11/11/2020", "balance": 0, "status": "Paid", "txnId": "5fae2d090888b11acc10be14" }, { "name": "Total", "due": 125000, "paid": 125000, "paidDate": "-", "balance": 0, "status": "Paid", "txnId": "-" }] }, { "studentName": "Pradeep Kumar", "regId": "STUD_003", "academicYear": "2020-21", "classBatch": "BE_ENG_CVL_20-21", "DemandId": "DN_2020-21_003", "description": [{ "name": "Uniform Plan", "due": 2000, "paid": 800, "paidDate": "10/11/2020", "balance": 2200, "status": "Partial", "txnId": "5fae2d0d7074061accc1a355" }, { "name": "Tuition Fee", "due": 2000, "paid": 1200, "paidDate": "10/11/2020", "balance": 1800, "status": "Partial", "txnId": "5fae2d0d7074061accc1a355" }, { "name": "Total", "due": 4000, "paid": 2000, "paidDate": "-", "balance": 4000, "status": "Partial", "txnId": "-" }] }, { "studentName": "Mohammed Yaseen R", "regId": "STUD_003", "academicYear": "2020-21", "classBatch": "BE_ENG_CVL_20-21", "DemandId": "DN_2020-21_004", "description": [{ "name": "Transport Fee", "due": 280000, "paid": 11275.17, "paidDate": "25/10/2020", "balance": 128724.83, "status": "Partial", "txnId": "5fae2d1aa3335d1acc4e9a77" }, { "name": "Tuition Fee", "due": 280000, "paid": 131543.62, "paidDate": "25/10/2020", "balance": 8456.38, "status": "Partial", "txnId": "5fae2d1aa3335d1acc4e9a77" }, { "name": "Uniform Plan", "due": 280000, "paid": 5637.58, "paidDate": "25/10/2020", "balance": 134362.42, "status": "Partial", "txnId": "5fae2d1aa3335d1acc4e9a77" }, { "name": "Tuition Fee", "due": 280000, "paid": 131543.62, "paidDate": "25/10/2020", "balance": 8456.38, "status": "Partial", "txnId": "5fae2d1aa3335d1acc4e9a77" }, { "name": "Total", "due": 1120000, "paid": 279999.99, "paidDate": "-", "balance": 280000.01, "status": "Partial", "txnId": "-" }] }], "currentPage": null, "perPage": 10, "nextPage": null, "totalRecord": 4, "totalPages": 1 }

                    // this.setState({ printReportArr: respErr.data, totalPages: respErr.totalPages, totalRecord: respErr.totalRecord })
                })

        })
    }
    dateFilter = (ev) => {

        var ts = new Date(ev);
        let getDate = `${String(ts.getDate()).length == 1 ? `0${ts.getDate()}` : ts.getDate()}`;
        let getMonth = `${String(ts.getMonth() + 1).length == 1 ? `0${ts.getMonth() + 1}` : ts.getMonth() + 1}`;
        let getYear = `${ts.getFullYear()}`;
        let today = `${getDate}/${getMonth}/${getYear}`;
        return today;
    };

    onFilterCall = (page, limit) => {
        this.setState({ isLoader: true, paginationCall: "filterData" })
        this.setState({ printReportArr: [] }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/feePayment?orgId=${this.state.orgId}&classbatchName=${this.state.filterKey}&page=${this.state.page}&limit=${this.state.limit}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token"),
                }
            })
                .then(resp => {
                    console.log(resp);
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    // total number of pages
                    // let totalPages = resp.data.data.length || [];
                    // totalPages = Math.ceil(totalPages / this.state.limit);

                    // total data based on Pagination
                    // let filterData = [];
                    // resp.data.data.map((item, i) => {
                    //     if (i < this.state.limit) filterData.push(item)
                    // })

                    this.setState({
                        printReportArr: resp.data.data,
                        totalPages: resp.data.totalPages,
                        totalRecord: resp.data.totalRecord,
                        noData,
                        totalPages: resp.data.totalPages,
                        page: resp.data.currentPage,
                        totalAmount: resp.data.totalAmount === undefined ? 0 : resp.data.totalAmount,
                        totalPaidAmount: resp.data.totalPaid === undefined ? 0 : resp.data.totalPaid,
                        totalPendingAmount: resp.data.totalPending === undefined ? 0 : resp.data.totalPending,
                        totalCashView: resp.data.totalCash === undefined ? 0 : resp.data.totalCash,
                        totalChequeView: resp.data.totalCheque === undefined ? 0 : resp.data.totalCheque,
                        totalCardView: resp.data.totalCard === undefined ? 0 : resp.data.totalCard,
                        totalNetbankingView: resp.data.totalNetbanking === undefined ? 0 : resp.data.totalNetbanking,
                        totalWalletView: resp.data.totalWallet === undefined ? 0 : resp.data.totalWallet,
                        totalUpiView: resp.data.totalUpi === undefined ? 0 : resp.data.totalUpi,
                    })
                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true })
                })

        })
    }

    handleBackFun = () => {
        this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`);
    }
    formatAmount = (amount) => {
        let a = Number(amount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })
        let b = a.replace('₹', "")
        return b
    }
    onDownloadEvent = () => {
        this.setState({ isLoader: true })
        axios.get(`${this.state.env['zqBaseUri']}/edu/reports/feePayment?orgId=${this.state.orgId}&classbatchName=${this.state.filterKey}`, {
            headers: {
                'Authorization': localStorage.getItem("auth_token")
            }
        })
            .then(resp => {
                console.log("student fee", resp);
                var createXlxsData = [];
                let summaryData = []
                summaryData.push({
                    "TOTAL COLLECTION": resp.data.totalPaid === undefined ? 0.0 : Number(resp.data.totalPaid).toFixed(2),
                    "CASH": resp.data.totalCash === undefined ? 0.0 : Number(resp.data.totalCash).toFixed(2),
                    "CHEQUE": resp.data.totalCheque === undefined ? 0.0 : Number(resp.data.totalCheque).toFixed(2),
                    "CARD": resp.data.totalCard === undefined ? 0.0 : Number(resp.data.totalCard).toFixed(2),
                    "NETBANKING": resp.data.totalNetbanking === undefined ? 0.0 : Number(resp.data.totalNetbanking).toFixed(2),
                    "WALLET": resp.data.totalWallet === undefined ? 0.0 : Number(resp.data.totalWallet).toFixed(2),
                    "UPI": resp.data.totalUpi === undefined ? 0.0 : Number(resp.data.totalUpi).toFixed(2)
                })
                let regId = this.context.reportLabel || "REG ID";
                let batch = this.context.classLabel || "CLASS/BATCH";
                resp.data.data.map(item => {
                    item.description.map((dataOne, i) => {
                        if (String(dataOne.name).toLowerCase() !== "total") {
                            createXlxsData.push({
                                "RECEIPT ID": item.displayName,
                                // "DEMAND NOTE ID": item.DemandId === undefined ? "-" : item.DemandId,
                                [regId]: item.regId,
                                "STUDENT NAME": item.studentName,
                                "ACADEMIC YEAR": item.academicYear,
                                [batch]: item.classBatch,
                                "DESCRIPTION": dataOne.name,
                                "TOTAL FEES (INR)": this.formatAmount(dataOne.due),
                                "PAID (INR)": this.formatAmount(dataOne.paid),
                                "PAID ON": String(dataOne.paidDate),
                                "MODE": item.paymentDetails.data.mode,
                                "PENDING (INR)": this.formatAmount(dataOne.balance),
                                "TXN ID": dataOne.txnId,
                                // "REFUND": this.formatAmount(item.refundAmount),
                                "STATUS": dataOne.status === undefined ? "" : dataOne.status
                            })
                        }
                    })
                    console.log('**DATA**', item)

                })
                var ws = xlsx.utils.json_to_sheet(createXlxsData);
                var wb = xlsx.utils.book_new();
                var ws2 = xlsx.utils.json_to_sheet(summaryData);
                xlsx.utils.book_append_sheet(wb, ws2, "Summary Report");
                xlsx.utils.book_append_sheet(wb, ws, "Student Fee Reports");
                xlsx.writeFile(wb, "student_fee_reports.xlsx");
                // xlsx.writeFile(wb, "demand_note_reports.csv");
                this.setState({ isLoader: false })
            })
            .catch(err => {
                console.log(err, 'err0rrrrr')
                this.setState({ isLoader: false })
            })


    }
    printScreen = () => {
        this.setState({ isLoader: false }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/feePayment?orgId=${this.state.orgId}&classbatchName=${this.state.filterKey}&pagination=false`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    console.log(resp);
                    this.setState({
                        printReportArr: resp.data.data
                    }, () => {
                        window.print();
                        this.onDataCall();
                    })
                })
        });
    }
    searchHandle = (searchValue) => {
        this.setState({ isLoader: true })
        console.log('searchValue', searchValue)

        // this.setState({
        //     printReportArr: [],
        //     dataInfo: "Fetching Data..."
        // }, () => {
        if (String(searchValue).length > 0) {
            return axios.get(`${this.state.env['zqBaseUri']}/edu/reports/feePayment?orgId=${this.state.orgId}&page=${this.state.page}&limit=${this.state.limit}&classbatchName=${this.state.filterKey}&searchKey=${searchValue}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    this.setState({
                        printReportArr: resp.data.data.reverse(),
                        totalPages: resp.data.totalPages,
                        totalRecord: resp.data.totalRecord,
                        noData
                    })

                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true, dataInfo: "No Data" })
                })
        } else {
            this.setState({ printReportArr: this.state.filterData, isLoader: false })
        }
        // })

    }
    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    onSelectPrgmPlan = (value, item, label) => {
        let selectedValue = value
        this.setState({ filterKey: selectedValue, page: 1, limit: 5, totalRecord: 0, totalPages: 0, })
        if (value === "All") {
            this.onDataCall()
        } else {
            this.onFilterCall()
        }
    }
    onSelectClean = () => {
        this.setState({ filterKey: 'All' })
    }
    downloadTallyXML = () => {
        const anchor = document.createElement("a");
        anchor.href = "https://d8wau2wreg.execute-api.us-east-1.amazonaws.com/dev/migrate/receipt?orgid=5fa8daece3eb1f18d4250e55";
        anchor.id = "xml-download"
        document.body.appendChild(anchor);
        anchor.click();
        document.body.removeChild(anchor)
    }
    onChangeDateDuration = (e, f) => {
        this.setState({ isLoader: true, page: 1, limit: 5, totalRecord: 0, totalPages: 0, printReportArr: [], typeOfPagination: 'date', fromDateData: e, toDateData: f }, () => {
            console.log(e, f);
            this.filterWithDate(e, f);
        })
    }
    tableDateFormat = (ev) => {
        if (ev === undefined || ev === '') { }
        else {
            let getDate = `${String(ev.getDate()).length == 1 ? `0${ev.getDate()}` : ev.getDate()}`;
            let getMonth = `${String(ev.getMonth() + 1).length == 1 ? `0${ev.getMonth() + 1}` : ev.getMonth() + 1}`;
            let getYear = `${ev.getFullYear()}`
            let today = `${getYear}-${getMonth}-${getDate}`
            return today
        }
    }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            if (this.state.typeOfPagination === 'date') {
                this.onPaginationDateChange(page, limit)
            }
            else {
                if (this.state.paginationCall === "allData") this.onDataCall();
                else if (this.state.paginationCall === "filterData") this.onFilterCall(page, limit);
                else this.onDataCall();
            }
        });
        console.log(page, limit);
    };
    onPaginationDateChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.filterWithDate(this.state.fromDateData, this.state.toDateData)
        });
    }
    filterWithDate = (f, t) => {
        this.setState({ isLoader: true }, () => {
            axios({
                method: 'get',
                url: `${this.state.env["zqBaseUri"]}/edu/getPeriodicFeeCollection?fromDate=${this.tableDateFormat(f)}&toDate=${this.tableDateFormat(t)}&page=${this.state.page}&perPage=${this.state.limit}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            })
                .then(res => {
                    console.log(res);
                    let noData;
                    if (res.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    this.setState({
                        printReportArr: res.data.data,
                        filterData: res.data.data,
                        noData,
                        totalPages: res.data.totalPages,
                        page: res.data.page,
                        downloadTotalRecord: res.data.totalRecord,
                        totalAmount: res.data.totalAmount,
                        totalPaidAmount: res.data.totalPaid,
                        totalPendingAmount: res.data.totalPending,
                        totalCashView: res.data.totalCash === undefined ? 0 : res.data.totalCash,
                        totalChequeView: res.data.totalCheque === undefined ? 0 : res.data.totalCheque,
                        totalCardView: res.data.totalCard === undefined ? 0 : res.data.totalCard,
                        totalNetbankingView: res.data.totalNetbanking === undefined ? 0 : res.data.totalNetbanking,
                        totalWalletView: res.data.totalWallet === undefined ? 0 : res.data.totalWallet,
                        totalUpiView: res.data.totalUpi === undefined ? 0 : res.data.totalUpi,
                        isLoader: false
                    })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true })
                })
        })
    }
    allSelect = (item) => {
        return null;
    }
    onPreviewLoan = (item) => {
        console.log(item)
        return null;
    }
    refreshTableFun = () => {
        this.setState({ typeOfPagination: '' }, () => {
            this.onDataCall();
        })
    }
    render() {
        return (
            <React.Fragment>
                {this.state.isLoader && <Loader />}
                <div className="reports-student-fees list-of-students-mainDiv" >
                    <div className="trial-balance-header-title">
                        <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                        <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Fees Collection</p>
                    </div>
                    <div className="reports-body-section print-hd">
                        <React.Fragment>
                            <div className="demand-note-container" style={{ display: 'block' }}>
                                <ContainerNavbar
                                    containerNav={this.state.containerNav}
                                    onDownload={() => this.onDownloadEvent()}
                                    searchValue={(searchValue) => this.searchHandle(searchValue)}
                                    Selectdata={this.state.appDatas}
                                    onSelectDefaultValue={this.state.filterKey}
                                    onSelectClean={this.onSelectClean}
                                    onSelectChange={this.onSelectPrgmPlan}
                                    printScreen={this.printScreen}
                                    dateDurationVal={(e, f) => { this.onChangeDateDuration(e, f) }}
                                    getAllDataFun={this.refreshTableFun}
                                />
                                <div className="print-time">{this.state.printTime}</div>
                                {/* <div className="application-main-sec-div">
                                    <div className="left-section-app" style={{ width: '20%' }}>
                                        <p>Total collection</p>
                                    </div>
                                    <div className="left-section-app" style={{ width: '13.3%' }}>
                                        <p>Cash</p>
                                    </div>
                                    <div className="left-section-app" style={{ width: '13.3%' }}>
                                        <p>Cheque/DD</p>
                                    </div>
                                    <div className="left-section-app" style={{ width: '13.3%' }}>
                                        <p>Card</p>
                                    </div>
                                    <div className="left-section-app" style={{ width: '13.3%' }}>
                                        <p>Netbanking</p>
                                    </div>
                                    <div className="left-section-app" style={{ width: '13.3%' }}>
                                        <p>Wallet</p>
                                    </div>
                                    <div className="left-section-app" style={{ width: '13.3%' }}>
                                        <p>UPI</p>
                                    </div>
                                </div> */}
                                <div className="mode-type-amount-view">
                                    <div className="cash-type-payment" style={{ width: '20%' }}>
                                        <p className="mode-title">Total collection</p>
                                        <p className="mode-amount">{Number(5954.00).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                    </div>
                                    <div className="cash-type-payment">
                                        <p className="mode-title">Cash</p>
                                        <p className="mode-amount">{Number(this.state.totalCashView).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                    </div>
                                    <div className="chequeDD-type-payment">
                                        <p className="mode-title">Cheque/DD</p>
                                        <p className="mode-amount">{Number(this.state.totalChequeView).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                    </div>
                                    <div className="card-type-payment">
                                        <p className="mode-title">Card</p>
                                        <p className="mode-amount">{Number(5954.00).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                    </div>
                                    <div className="netBanking-payment">
                                        <p className="mode-title">Netbanking</p>
                                        <p className="mode-amount">{Number(this.state.totalNetbankingView).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                    </div>
                                    <div className="wallet-payment">
                                        <p className="mode-title">Wallet</p>
                                        <p className="mode-amount">{Number(this.state.totalWalletView).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                    </div>
                                    <div className="upi-payment">
                                        <p className="mode-title">UPI</p>
                                        <p className="mode-amount">{Number(this.state.totalUpiView).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                    </div>
                                </div>

                                {/* <div className="application-main-sec-div">
                                    <div className="left-section-app" style={{ width: '33%' }}>
                                        <p>Total Amount: <span>{Number(this.state.totalAmount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</span></p>
                                    </div>
                                    <div className="left-section-app" style={{ width: '33%' }}>
                                        <p>Paid Amount: <span>{Number(this.state.totalPaidAmount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</span></p>
                                    </div>
                                    <div className="right-section-app" style={{ width: '34%' }}>
                                        <p>Pending Amount: <span>{Number(this.state.totalPendingAmount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</span></p>
                                    </div>
                                </div> */}
                                {/* <div className="mode-type-amount-view">
                                    <div className="cash-type-payment">
                                        <p className="mode-title">Cash</p>
                                        <p className="mode-amount">{Number(this.state.totalCashView).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                    </div>
                                    <div className="chequeDD-type-payment">
                                        <p className="mode-title">Cheque/DD</p>
                                        <p className="mode-amount">{Number(this.state.totalChequeView).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                    </div>
                                    <div className="card-type-payment">
                                        <p className="mode-title">Card</p>
                                        <p className="mode-amount">{Number(this.state.totalCardView).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                    </div>
                                    <div className="netBanking-payment">
                                        <p className="mode-title">Netbanking</p>
                                        <p className="mode-amount">{Number(this.state.totalNetbankingView).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                    </div>
                                    <div className="wallet-payment">
                                        <p className="mode-title">Wallet</p>
                                        <p className="mode-amount">{Number(this.state.totalWalletView).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                    </div>
                                    <div className="upi-payment">
                                        <p className="mode-title">UPI</p>
                                        <p className="mode-amount">₹0.00</p>
                                    </div>
                                </div> */}
                                {/* <Button title="click to download Tally XML file" className="send-demand-note-btn" onClick={this.downloadTallyXML} style={{ minWidth: 'max-content', bottom: '5px', position: "relative" }} >Download Tally XML</Button> */}
                            </div>
                        </React.Fragment>
                        <div className="reports-data-print-table studentFee-clct-reports">
                            <div className="table-wrapper" style={{ marginTop: "10px" }}>
                                {this.state.studenTableData.length == 0 ? <p className="noprog-txt">{this.state.noProg}</p> :
                                    <React.Fragment>
                                        <div className="remove-last-child-table" >
                                            <ZqTable
                                                data={this.state.studenTableData}
                                                allSelect={this.allSelect}
                                                rowClick={(item) => { this.onPreviewLoan(item) }}
                                                onRowCheckBox={(item) => { this.allSelect(item) }}
                                                handleActionClick={(item) => { this.actionClickFun(item) }}
                                            />

                                        </div>
                                    </React.Fragment>
                                }
                                {/* {this.state.studenTableData.length !== 0 ? <PaginationUI onPaginationApi={this.onPaginationChange} totalPages={this.state.totalPages} limit={this.state.limit} total={this.state.totalRecord} /> : null} */}
                            </div>
                            {/* <div>
                                {this.state.printReportArr.length == 0 ? null :
                                    <PaginationUI
                                        total={this.state.totalRecord}
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        currentPage={this.state.page}
                                    />}
                            </div> */}
                        </div>
                    </div>
                </div >
            </React.Fragment>
        )
    }
}
export default StudentFeeCollection;