import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Loader from "../../../../utils/loader/loaders";
import Button from '@material-ui/core/Button';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import ZqTable from "../../../../utils/Table/table-component";
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import "../../../../scss/task.scss";
import "../../../../scss/onboard-user.scss";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import '../../../../scss/tabs.scss';
import '../../../../scss/student.scss';
import '../../../../scss/receive-payment.scss';
import '../../../../scss/payment-portal.scss';
import '../../../../scss/portal-stepper.scss';
import '../../../../scss/student-reports.scss';
import '../../../../scss/setup.scss';
import moment from 'moment';
import OnboardTableData from './studentTableData.json';
import OnboardFormData from './studentDetails.json';
import ZenForm from '../../../input/form';
import { Alert, Modal, SelectPicker } from "rsuite";
import TextField from '@material-ui/core/TextField';
import '../../../../scss/input.scss';
import "../../../../scss/newcss.scss";
import { Input, InputGroup } from 'rsuite';
import { DatePicker } from 'rsuite';
import Axios from 'axios';
import { event } from 'react-ga';
import Payment from "../../../payment/payment";
import BrightKidFeesJson from './brightskid.json'
import { ListItemAvatar } from '@material-ui/core';

class Onboard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            noProg: "Fetching...",
            onboardData: [],
            onboardForm: OnboardFormData.ShowOnboardForm,
            onboardParentForm: [],
            authToken: localStorage.getItem("auth_token"),
            env: JSON.parse(localStorage.getItem('env')),
            orgId: localStorage.getItem("orgId"),
            onboardStudentForm: OnboardFormData.ShowOnboardForm['Student Details'],
            showOnboardPreview: false,
            noOfInstallmentArray: [{
                id: 1, percentage: 0, description: "", amount: 5954, nextMonth: new Date(), discount: 0, remarks: "", financeRemarks: "", status: "Planned"
            },
            {
                id: 2, percentage: 0, description: "", amount: 5954, nextMonth: new Date(), discount: 0, remarks: "", financeRemarks: "", status: "Planned"
            },
            {
                id: 3, percentage: 0, description: "", amount: 3969, nextMonth: new Date(), discount: 0, remarks: "", financeRemarks: "", status: "Planned"
            },
            {
                id: 4, percentage: 0, description: "", amount: 3969, nextMonth: new Date(), discount: 0, remarks: "", financeRemarks: "", status: "Planned"
            }
            ],
            response: [],
            selectedNoofInstallment: 4,
            showSellerList: false,
            activeTab: 0,
            tabData: ['Student Details', 'Parent Details', 'Payment Details', 'installment Details'],
            loader: false,
            selectSeller: [{ label: "Prashanth - Seller 1", value: "Prashanth - Seller 1" }, { label: "Mathivanan - Seller 2", value: "Mathivanan - Seller 2" }, { label: "Sankar - Seller 3", value: "Sankar - Seller 3" }, { label: "Jay - Seller 4", value: "Jay - Seller 4" }, { label: "Naveen - Seller 5", value: "Naveen - Seller 5" }],
            selectedSeller: '',
            openModal1: false,
            checkIsPercentage: false,
            discountAmount: 8505,
            allotedPercentage: 0,
            totalPercentage: 0,
            getAmount: 28350,
            totalAmount: 19845,
            totalPercentage: 0,
            selectedProgramType: "Bright Kid @Home - Play Group",
            remarks: localStorage.getItem("user") == "Finance" ? "Test" : "",
            financeRemarks: "",
            openModal: false,
            StudentRegId: "demo123",
            StudentName: "Siva A",
            StudentClass: "Play Group",
            paymentType: "Card",
            PaymentFormData: [],
            handleClickData: {},
            getAmountOnchange: false,
            programData: [
                {
                    label: "Bright Kid @Home - Play Group",
                    value: 'Bright Kid @Home - Play Group'
                },
                {
                    label: "Bright Kid @Home - Nursery",
                    value: 'Bright Kid @Home - Nursery'
                },
                {
                    label: "Bright Kid @Home - LKG",
                    value: 'Bright Kid @Home - LKG'
                },
                {
                    label: "Bright Kid @Home - UKG",
                    value: 'Bright Kid @Home - UKG'
                }
            ],
            noOfInstallment: [
                {
                    "label": "1",
                    "value": 1
                },
                {
                    "label": "2",
                    "value": 2
                },
                {
                    "label": "3",
                    "value": 3
                },
                {
                    "label": "4",
                    "value": 4
                },
                {
                    "label": "5",
                    "value": 5
                },
                {
                    "label": "6",
                    "value": 6
                }
            ],
            discountType: [
                {
                    "label": "Percentage",
                    "value": "Percentage"
                },
                {
                    "label": "Amount",
                    "value": "Amount"
                }
            ],
            selectedDiscountType: "Amount",
            selectedDiscountType2: undefined,
            containerNav: {
                isBack: false,
                name: "Lists of Request",
                isName: true,
                total: 0,
                isTotalCount: false,
                isSearch: false,
                isSort: false,
                isPrint: true,
                isDownload: false,
                isShare: false,
                isNew: false,
                newName: "New",
                isSubmit: false,
            },
            installemntAmtStore: {},
            installemntPerStore: {},
            rowClickData: {}
        }
    }
    componentDidMount = () => {
        let currentUser = localStorage.getItem("user")
        // if (currentUser === "Seller") {
        //     let { onboardData } = this.state
        //     onboardData = OnboardTableData.seller
        //     this.setState({ onboardData: onboardData });
        // }
        // else if (currentUser === "Headseller") {
        //     let { onboardData } = this.state
        //     onboardData = OnboardTableData.headseller
        //     this.setState({ onboardData: onboardData });
        // } else if (currentUser === "Finance") {
        //     let { onboardData } = this.state
        //     onboardData = OnboardTableData.finance
        //     this.setState({ onboardData: onboardData });
        // }
        console.log(this.state.env)
        let { selectedNoofInstallment, onboardData } = this.state

        Axios.get(`${this.state.env['headSeller']}/bkah/edu/leads?orgId=${this.state.orgId}`,
            {
                headers: {
                    "Authorization": localStorage.getItem('auth_token')
                }
            }

        )
            .then(res => {
                let response = res.data
                console.log(response, "response")
                onboardData = []
                response.forEach((item, index) => {
                    if (currentUser === "Headseller") {
                        onboardData.push({
                            "Application ID": item.applicationId,
                            "Date": this.dateFilter(item.createdAt),
                            "Ward Name": item.student.firstName,
                            "Ward Age": this.calculateAge(item.student.dob),
                            "Parent Name": item.parent.name,
                            "Phone No.": item.parent.phoneNumber,
                            "Program": "Play Group",
                            "Amount": "-",
                            "Assigned To": `Siva - Head Seller`,
                            "Status": item.accountStatus.charAt(0).toUpperCase() + item.accountStatus.slice(1),
                            // "action": [
                            //     {
                            //         "name": "Process Now"
                            //     },
                            //     {
                            //         "name": "Assign to Seller"
                            //     }
                            // ],
                            Item: JSON.stringify(item)

                        })
                    }
                    else if (currentUser === "Seller") {
                        onboardData.push({
                            "Application ID": item.applicationId,
                            "Date": this.dateFilter(item.createdAt),
                            "Ward Name": item.student.firstName,
                            "Ward Age": this.calculateAge(item.student.dob),
                            "Parent Name": item.parent.name,
                            "Phone No.": item.parent.phoneNumber,
                            "Program": "Play Group",
                            "Amount": "-",
                            "Assigned To": `Sankar - Seller`,
                            "Status": index == 0 ? "Assigned" : "Requested",
                            // "action": [
                            //     {
                            //         "name": "Process Now"
                            //     }
                            // ],
                            Item: JSON.stringify(item)
                        })
                    }
                    else if (currentUser === "Finance") {
                        onboardData.push({
                            "Application ID": item.applicationId,
                            "Date": this.dateFilter(item.createdAt),
                            "Ward Name": item.student.firstName,
                            "Ward Age": this.calculateAge(item.student.dob),
                            "Parent Name": item.parent.name,
                            "Phone No.": item.parent.phoneNumber,
                            "Program": "Play Group",
                            "Amount": index == 0 ? this.formatCurrency(19845) : "-",
                            "Assigned To": `Sankar - Seller`,
                            "Status": index == 0 ? "Waiting Approval" : "Requested",
                            Item: JSON.stringify(item)
                        })
                    }
                })

                console.log('onboardData', onboardData)
                this.setState({ onboardData: [] }, () => {
                    this.setState({ onboardData: onboardData })
                    console.log(this.state.onboardData)
                })
            })
            .catch(error => {
                console.log(error)
            })

    }

    dateFilter = (ev) => {
        var ts = new Date(ev);
        let getDate = `${String(ts.getDate()).length == 1 ? `0${ts.getDate()}` : ts.getDate()}`;
        let getMonth = `${String(ts.getMonth() + 1).length == 1 ? `0${ts.getMonth() + 1}` : ts.getMonth() + 1}`;
        let getYear = `${ts.getFullYear()}`;
        let today = `${getDate}/${getMonth}/${getYear}`;
        return today;
    };

    calculateAge = (dob) => {
        let getDate = new Date(dob)
        let diff_ms = Date.now() - getDate.getTime();
        let age_dt = new Date(diff_ms);
        return Math.abs(age_dt.getUTCFullYear() - 1970) + ' Years';
    }

    onPreviewOnboard = (item) => {

        console.log("onboard item", item)
        let parsedItem = JSON.parse(item.Item)
        OnboardFormData.ShowOnboardForm['Student Details'].map(item => {
            if (item.name === "regId") {
                item.defaultValue = parsedItem.applicationId
            }
            else if (item.name === "firstName") {
                item.defaultValue = parsedItem['student'].firstName
                this.setState({ StudentName: parsedItem['student'].firstName });
            }
            else if (item.name === "lastName") {
                item.defaultValue = parsedItem['student'].lastName
            }
            else if (item.name === "addmittedDate") {
                item.defaultValue = parsedItem['student'].admittedOn
            }
            else if (item.name === "dob") {
                item.defaultValue = moment(parsedItem['student'].dob).format('DD/MM/YYYY')
            }
            else if (item.name === "gender") {
                item.defaultValue = parsedItem['student'].gender
            }
            else if (item.name === "CitizenshipData") {
                item.defaultValue = parsedItem['student'].citizenShip
            }
            else if (item.name === "programPlan") {
                item.defaultValue = parsedItem['student'].programName
            }
            else if (item.name === "email") {
                item.defaultValue = parsedItem['student'].email
            }
            else if (item.name === "contactStudent") {
                item.defaultValue = parsedItem['student'].phoneNumber
            }
            else if (item.name === "currenyType") {
                item.defaultValue = parsedItem['currency'].currency
            }
            else if (item.name === "exchangeRate") {
                item.defaultValue = parsedItem['currency'].exchangeRate
            }
        })
        OnboardFormData.ShowOnboardForm['Parent Details'].map(item => {
            if (item.name === "fatherName") {
                item.defaultValue = parsedItem['parent'].name
            }
            else if (item.name === "emailParent") {
                item.defaultValue = parsedItem['parent'].email
            }
            else if (item.name === "contactParent") {
                item.defaultValue = parsedItem['parent'].phoneNumber
            }
            else if (item.name === "addressParent-1") {
                item.defaultValue = parsedItem['parent']['address'].address1
            }
            else if (item.name === "addressParent-2") {
                item.defaultValue = parsedItem['parent']['address'].address2
            }
            else if (item.name === "addressParent-3") {
                item.defaultValue = parsedItem['parent']['address'].address3
            }
            else if (item.name === "City") {
                item.defaultValue = parsedItem['parent']['address'].city
            }
            else if (item.name === "state") {
                item.defaultValue = parsedItem['parent']['address'].state
            }
            else if (item.name === "country") {
                item.defaultValue = parsedItem['parent']['address'].country
            }
            else if (item.name === "pincode") {
                item.defaultValue = parsedItem['parent']['address'].pinCode
            }
        })
        this.setState({ showOnboardPreview: true, rowClickData: parsedItem });
    }

    formatCurrency = (amount) => {
        return new Intl.NumberFormat("en-IN", {
            style: "currency",
            currency: "INR",
            // minimumFractionDigits: 2,
            // maximumFractionDigits: 2,
        }).format(amount);
    };
    onInputChanges = (value, item, event, datas) => { }
    handleTabChange = (tabName, formDatas) => { }
    cancelViewData = () => { }
    handleActionClick = (item, index, hd, name) => {
        this.setState({ handleClickData: item });
        console.log(item)
        if (name == "Assign to Seller") {
            this.setState({ showSellerList: true, openModal: true });
        }
        if (name == "Process Now") {
            this.onPreviewOnboard(item);
            this.setState({ showOnboardPreview: true })
        }
        // if (name == "Approve") {
        //     this.setState({ loader: true });
        //     Axios.get(`${this.state.env['headSeller']}/zq/user/mailtoparent`).then(res => {
        //         setTimeout(() => {
        //             this.setState({ showSellerList: false, onboardData: [], openModal: false, loader: false }, () => {
        //                 let onboardData = []
        //                 Alert.info("Email has been sent to parent.")
        //                 onboardData = OnboardTableData.finance
        //                 onboardData.map(item => {
        //                     if (item["Ward Name"] == "Siva A") {
        //                         item['Status'] = "Finance Approved"
        //                         return item
        //                     }
        //                 })
        //                 this.setState({ onboardData: onboardData });
        //             });
        //         }, 2000)
        //     }).catch(err => console.log(err));
        // }
        // Check if this is required or not
        // if (name == "Send Demand Note") {
        //     this.setState({ loader: true });
        //     Axios.get(`${this.state.env['headSeller']}/zq/user/demandnote`).then(res => {
        //         setTimeout(() => {
        //             this.setState({ showSellerList: false, onboardData: [], openModal: false, loader: false }, () => {
        //                 let onboardData = []
        //                 Alert.info("Demand Note sent to parent.")
        //                 onboardData = OnboardTableData.finance
        //                 onboardData.map(item => {
        //                     if (item["Ward Name"] == "Siva A") {
        //                         item['Status'] = "Done"
        //                         return item
        //                     }
        //                 })
        //                 this.setState({ onboardData: onboardData });
        //             });
        //         }, 2000)
        //     }).catch(err => console.log(err));
        // }

    }
    onAccept = () => {
        this.setState({ loader: true, });
        console.log("************Final Data************", this.state.noOfInstallmentArray, this.state.rowClickData)
        let onboardData = []
        onboardData = this.state.onboardData;
        let { noOfInstallmentArray } = this.state
        let payload = {}
        console.log("noOfInstallmentArray", noOfInstallmentArray)
        for (let i = 0; i < noOfInstallmentArray.length; i++) {
            let installment = {}
            installment.percentage = noOfInstallmentArray[i]['percentage']
            installment.amount = this.formatCurrency(noOfInstallmentArray[i]['amount'])
            payload[`installment${i + 1}`] = installment

        }
        payload.info = {
            "studentName": `${this.state.rowClickData.student.firstName} ${this.state.rowClickData.student.lastName}`,
            "parentName": this.state.rowClickData.parent.name,
            "appId": this.state.rowClickData.applicationId,
            "amount": this.state.noOfInstallmentArray[0]['amount'],
            "class": this.state.selectedProgramType,
            "parentEmail": 'justin.thomas@zenqore.com'
        }
        console.log(payload)
        Axios.post(`${this.state.env['headSeller']}/zq/user/mailtoparent`, payload).then(res => {
            setTimeout(() => {
                this.setState({ showOnboardPreview: false, onboardData: [], loader: false }, () => {
                    Alert.info("Email has been sent to parent.")
                    onboardData.forEach(item => {
                        if (item["Ward Name"] == this.state.StudentName) {
                            item['Status'] = "Finance Approved"
                            return item
                        }
                    })
                    let { noOfInstallmentArray } = this.state;
                    noOfInstallmentArray.forEach(item => item.status = "Pending")

                    console.log(this.state.noOfInstallmentArray)
                    this.setState({ onboardData: onboardData, noOfInstallmentArray: noOfInstallmentArray, activeTab: 0 });
                });
            }, 2000)
            setTimeout(() => {
                let onboardData = []
                onboardData = this.state.onboardData
                this.setState({ onboardData: [] }, () => {
                    console.log('onboardData', onboardData)
                    onboardData[0].Status = "Done"
                    console.log(onboardData)
                    let { noOfInstallmentArray } = this.state;
                    noOfInstallmentArray.forEach((item, index) => {
                        if (index == 0) {
                            item.status = "Paid"
                        }
                    })
                    this.setState({ noOfInstallmentArray, onboardData })
                })
            }, 20000)
        }).catch(err => {
            Alert.error("Something went wrong. Please try again.")
            this.setState({ showOnboardPreview: false, loader: false })
        })
    }
    closeModal = () => {
        this.setState({ openModal: false });
    };
    onSelectSeller = (value, item, event) => {
        let handleClickData = this.state.handleClickData['Application ID'];
        this.setState({ loader: true });
        let currentUser = localStorage.getItem("user")
        if (currentUser == "Headseller") {
            let payload = {
                sellerName: "Sankar",
                appId: handleClickData
            }
            Axios.post(`${this.state.env['headSeller']}/zq/user/sellermail`, payload).then(res => {
                setTimeout(() => {
                    let onboardData = []
                    onboardData = this.state.onboardData
                    this.setState({ showSellerList: false, onboardData: [], openModal: false, selectedSeller: value, loader: false }, () => {
                        Alert.info(`Assigned to ${value}`)
                        onboardData.map(item => {
                            if (item["Ward Name"] == "siva") {
                                item['Assigned To'] = `${value}`
                                item['Status'] = "Assigned"
                                return item
                            }
                        })
                        this.setState({ onboardData: onboardData });
                    });
                }, 2000)
            }).catch("Something went wrong. Please try again.")
        }
        console.log(value, item, event)
    }
    onSelectProgramPlan = (value, item, event) => {
        // let { getAmount, discountAmount, noOfInstallmentArray } = this.state

        // BrightKidFeesJson.map(item => {
        //     if (item.title === value) {
        //         this.setState({ selectedProgramType: value, getAmount: item.total, discountAmount: item.discount, totalAmount: item.total, selectedNoofInstallment: item.noOfInstallment }, () => {
        //             let percentage = (Number(discountAmount) * 100) / Number(getAmount)
        //             console.log("percentageChange", getAmount, discountAmount, percentage, noOfInstallmentArray)
        //             for (let i = 0; i < item.instl.length; i++) {
        //                 noOfInstallmentArray[i].amount = item.instl[i]
        //             }
        //             // item.instl.forEach(instalAmount => {
        //             //     noOfInstallmentArray.forEach(installmentItem=> installmentItem.amount = instalAmount)
        //             // })
        //             console.log("noOfInstallmentArray", noOfInstallmentArray)
        //             this.setState({ allotedPercentage: Number(percentage).toFixed(2), noOfInstallmentArray })
        //         });

        //     }
        // })
    }
    onDiscountChanges = (value, item, event) => {
        // let { getAmount, discountAmount } = this.state
        // this.setState({ selectedDiscountType: value });
        // if (value === "Percentage") {
        //     let percentage = (Number(discountAmount) * 100) / Number(getAmount)
        //     console.log('percentage', percentage)
        //     this.setState({ percentageField: true, checkIsPercentage: true, allotedPercentage: percentage });
        // } else {
        //     this.setState({ percentageField: false, checkIsPercentage: false });
        // }
    }
    onDiscountChanges2 = (value, item, event) => {
        this.setState({ selectedDiscountType2: value, });
        if (value === "Percentage") {
            this.setState({ percentageField2: true, });
        } else {
            this.setState({ percentageField2: false });
        }
    }
    onCleanSeller = (value, data) => {
        this.setState({ selectedProgramType: undefined, getAmount: 0, totalAmount: 0 });
        this.onInstallmentChanges(1)
        console.log(value, data)
    }
    onClearDiscount = (value, data) => {
        let { totalAmount, getAmount } = this.state;
        this.setState({ selectedDiscountType: undefined, percentageField: false, checkIsPercentage: false, discountAmount: 0, totalAmount: getAmount });
        console.log(value, data)
    }
    onClearDiscount2 = (value, data) => {
        this.setState({ selectedDiscountType2: undefined, percentageField2: false });
        console.log(value, data)
    }
    tabChangeInternal = (e, value) => {
        if (value == 1) {
            let { onboardParentForm, onboardForm } = this.state
            let parentForm = []
            parentForm = onboardForm['Parent Details']
            this.setState({
                onboardParentForm: []
            }, () => {
                onboardParentForm = parentForm
                this.setState({ onboardParentForm, onboardStudentForm: [] }, () => {
                    console.log(this.state.onboardParentForm)
                })
            })
        }
        if (value == 0) {
            let { onboardStudentForm, onboardForm } = this.state
            let studentForm = []
            studentForm = onboardForm['Student Details']
            this.setState({
                onboardStudentForm: []
            }, () => {
                onboardStudentForm = studentForm
                this.setState({ onboardStudentForm, onboardParentForm: [] })
            })
        }
        if (value == 2) {
            let { onboardStudentForm, onboardForm } = this.state
            let paymentDetails = []
            paymentDetails = onboardForm['Payment Details']
            this.setState({
                onboardStudentForm: []
            }, () => {
                onboardStudentForm = paymentDetails
                this.setState({ onboardStudentForm, onboardParentForm: [] })
            })
        }
        if (value == 3) {
            let { noOfInstallmentArray, response, totalAmount, getAmount } = this.state
            let getTotalAmounnt = this.state.totalAmount;
            noOfInstallmentArray = []
            console.log(this.state.noOfInstallmentArray)
            let todayDate = new Date()
            let nextMonth
            if (this.state.getAmountOnchange === false) {
                for (let i = 0; i < this.state.noOfInstallmentArray.length; i++) {
                    nextMonth = i == 0 ? todayDate : nextMonth
                    let percentage = (Number(this.state.noOfInstallmentArray[i].amount) * 100) / Number(totalAmount)
                    console.log(this.state.noOfInstallmentArray[i].amount, totalAmount)
                    percentage = Number(percentage).toFixed(2)
                    if (i > 0) {
                        nextMonth = new Date(new Date().setMonth(nextMonth.getMonth() + 1))
                    }
                    noOfInstallmentArray.push({
                        id: i,
                        percentage: percentage,
                        amount: this.state.noOfInstallmentArray[i].amount,
                        nextMonth: nextMonth,
                        description: this.state.noOfInstallment[i].description,
                        remarks: this.state.noOfInstallmentArray[i].remarks,
                        financeRemarks: undefined,
                        status: this.state.noOfInstallmentArray[i].status
                    })
                }
                this.setState({ noOfInstallmentArray: noOfInstallmentArray })
            }
            else if (this.state.getAmountOnchange === true) {
                for (let i = 1; i <= this.state.selectedNoofInstallment; i++) {
                    nextMonth = i == 1 ? todayDate : nextMonth
                    let Amount = Number(getAmount) / Number(this.state.selectedNoofInstallment)
                    console.log(Amount, getAmount, this.state.selectedNoofInstallment)
                    let percentage = (Number(Amount) * 100) / Number(totalAmount)
                    percentage = Number(percentage).toFixed(2)
                    if (i > 1) {
                        nextMonth = new Date(new Date().setMonth(nextMonth.getMonth() + 1))
                    }

                    noOfInstallmentArray.push({
                        id: i,
                        percentage: percentage,
                        amount: Amount,
                        nextMonth: nextMonth,
                        description: this.state.noOfInstallmentArray[0].description,
                        remarks: this.state.noOfInstallmentArray[0].status,
                        financeRemarks: undefined,
                        status: this.state.noOfInstallmentArray[0].status

                    })
                }
                this.setState({ noOfInstallmentArray: noOfInstallmentArray })
            }
        }
        this.setState({ activeTab: value })
    }

    onInstallmentChanges = (value, event) => {
        // let { noOfInstallmentArray, response } = this.state
        // let getTotalAmounnt = this.state.totalAmount;
        // noOfInstallmentArray = []
        // let listofInstallmentAmount = Number(getTotalAmounnt) / Number(value)
        // let todayDate = new Date()
        // let nextMonth
        // let percentage = 100 / Number(value)
        // percentage = Number(percentage).toFixed(2)
        // for (let i = 1; i <= value; i++) {
        //     nextMonth = i == 1 ? todayDate : nextMonth
        //     console.log('nextMonth', nextMonth)
        //     if (i > 1) {
        //         nextMonth = new Date(new Date().setMonth(nextMonth.getMonth() + 1))
        //     }
        //     noOfInstallmentArray.push({
        //         id: i,
        //         percentage: percentage,
        //         amount: listofInstallmentAmount,
        //         nextMonth: nextMonth,
        //         description: noOfInstallmentArray.description,
        //         remarks: noOfInstallmentArray.remarks,
        //         financeRemarks: undefined,
        //         status: "Planned"
        //     })
        // }
        // console.log('nextYear', noOfInstallmentArray)
        // this.setState({ noOfInstallmentArray: noOfInstallmentArray, selectedNoofInstallment: value, totalPercentage: 100 })
    }
    onSelectProgram = () => { }
    onCancel = () => {
        this.setState({ showOnboardPreview: false })
    }
    onSubmit = () => {
        let currentUser = localStorage.getItem("user")
        let { onboardData, rowClickData, noOfInstallmentArray } = this.state
        if (currentUser == "Seller") {
            console.log(rowClickData)
            onboardData = this.state.onboardData
            console.log("onboardData", onboardData)

            onboardData.forEach(item => {
                if (item["Ward Name"] == rowClickData.student.firstName) {
                    item['Status'] = "Waiting Approval"
                    item['Amount'] = this.formatCurrency(this.state.totalAmount)
                    // Check if this is required or not
                    // item.action = [{
                    //     name: "Send Demand Note"
                    // }]
                }
                this.setState({ onboardData: onboardData, showOnboardPreview: false });
            })
            setTimeout(() => {
                let onboardData = []
                onboardData = this.state.onboardData
                this.setState({
                    onboardData: []
                }, () => {
                    onboardData.forEach(item => {
                        if (item["Ward Name"] == rowClickData.student.firstName) {
                            item['Status'] = "Done"
                            // Check if this is required or not
                            // item.action = [{
                            //     name: "Send Demand Note"
                            // }]
                        }
                        this.setState({ onboardData: onboardData, showOnboardPreview: false }, () => {
                            console.log(this.state.onboardData)
                        });
                    })
                })

            }, 10000)
        } else if (currentUser == "Headseller") {
            let { onboardData } = this.state
            onboardData = this.state.onboardData
            onboardData.map(item => {
                if (item["Ward Name"] == rowClickData.student.firstName) {
                    item['Status'] = "Waiting Approval"
                }
                this.setState({ onboardData: onboardData, showOnboardPreview: false });
            })
        }
        let sellerPayload = {
            "financeMember": "Mehul",
            "studentName": rowClickData.student.firstName,
            "appId": rowClickData.applicationId
        }
        console.log('sellerPayload', sellerPayload)
        Axios.post(`${this.state.env['headSeller']}/zq/user/financemail`, sellerPayload).then(res => Alert.info("Request sent finance for approval."))

            .catch(err => console.log(err))
        setTimeout(() => {
            let onboardData = []
            onboardData = this.state.onboardData
            this.setState({ onboardData: [] }, () => {
                onboardData[0].Status = "Done"
                console.log(onboardData)
                noOfInstallmentArray.forEach((item, index) => {
                    if (index == 0) {
                        item.status = "Paid"

                    }
                })

                this.setState({ noOfInstallmentArray, onboardData }, () => {
                    console.log(this.state.noOfInstallmentArray, this.state.onboardData)
                })
            })
        }, 20000)


    }
    getAmount = (e) => {
        let getAmount = e
        this.setState({ getAmount: getAmount, discountAmount: 0, totalAmount: getAmount, getAmountOnchange: true })
    }
    getDiscountAmounnt = (e) => {
        if (this.state.selectedDiscountType != undefined) {
            let { totalAmount, getAmount } = this.state
            let discountAmount = e
            totalAmount = Number(getAmount) - Number(discountAmount)
            this.setState({ discountAmount: discountAmount, totalAmount })
        } else {
            Alert.info("Please select a discount type first.")
        }
    }
    getPercentage = (e) => {
        let { totalAmount, getAmount } = this.state
        let allotedPercentage = e
        let percentageAmt = Number(getAmount) * (Number(allotedPercentage) / 100)
        totalAmount = Number(getAmount) - Number(percentageAmt)
        this.setState({ discountAmount: percentageAmt, allotedPercentage: allotedPercentage, totalAmount })
    }
    changeAmount = (value, valueIndex) => {
        console.log(value, valueIndex)
        let { noOfInstallmentArray, totalAmount, selectedNoofInstallment } = this.state
        let installemntAmtStore = this.state.installemntAmtStore
        installemntAmtStore[valueIndex] = Number(value)
        if (Number(totalAmount) >= Number(value)) {
            this.setState({ installemntAmtStore }, () => {
                let calcAmt = 0;
                Object.keys(this.state.installemntAmtStore).map(ims => {
                    calcAmt += this.state.installemntAmtStore[ims]
                })
                let remainingAmount = Number(totalAmount) - Number(calcAmt)
                let itemInstallmentAmt = (remainingAmount / (Number(selectedNoofInstallment) - Object.keys(this.state.installemntAmtStore).length))
                noOfInstallmentArray.forEach((item, index) => {
                    if (index === valueIndex) {
                        item.amount = this.state.installemntAmtStore[index] == undefined ? Number(value).toFixed(2) : this.state.installemntAmtStore[index]
                        item.percentage = this.state.installemntAmtStore[index] == undefined ? ((Number(value) / totalAmount) * 100) : ((this.state.installemntAmtStore[index] / totalAmount) * 100)
                    } else {
                        item.amount = this.state.installemntAmtStore[index] == undefined ? itemInstallmentAmt.toFixed(2) : this.state.installemntAmtStore[index]
                        item.percentage = this.state.installemntAmtStore[index] == undefined ? ((itemInstallmentAmt / totalAmount) * 100) : ((this.state.installemntAmtStore[index] / totalAmount) * 100)
                    }
                })
                console.log(noOfInstallmentArray)
                this.setState({ noOfInstallmentArray: noOfInstallmentArray })
            })
        } else {
            Alert.info('Invalid Amount')
        }
    }
    changePercentage = (value, valueIndex) => {
        console.log(value, valueIndex)
        let { noOfInstallmentArray, totalAmount, selectedNoofInstallment, totalPercentage } = this.state
        let installemntPerStore = this.state.installemntPerStore
        installemntPerStore[valueIndex] = value
        // if (totalPercentage >= value) {
        this.setState({ installemntPerStore }, () => {
            let currentPercentageAmount = (Number(value) * Number(totalAmount)) / 100
            let calcAmt = 0;
            Object.keys(this.state.installemntPerStore).map(ims => {
                calcAmt += Number(this.state.installemntPerStore[ims])
            })
            let remainingPercentage = Number(100) - Number(calcAmt)
            let itemInstallmentPercentage = (remainingPercentage / (Number(selectedNoofInstallment) - Object.keys(this.state.installemntPerStore).length))
            noOfInstallmentArray.forEach((item, index) => {
                if (index === valueIndex) {
                    item.amount = this.state.installemntPerStore[index] != undefined ? Number(currentPercentageAmount).toFixed(2) : this.state.noOfInstallmentArray[index]['amount']
                    item.percentage = this.state.installemntPerStore[index] == undefined ? Number(value) : this.state.installemntPerStore[index]
                } else {
                    item.amount = this.state.installemntPerStore[index] == undefined ? ((Number(totalAmount) * Number(itemInstallmentPercentage)) / 100) : this.state.noOfInstallmentArray[index]['amount']
                    item.percentage = this.state.installemntPerStore[index] == undefined ? itemInstallmentPercentage : this.state.installemntPerStore[index]
                }
            })
            console.log(noOfInstallmentArray)
            this.setState({ noOfInstallmentArray: noOfInstallmentArray })
        });
        // } else {
        //     Alert.info("Invalid Percentage")
        // }


        // this.setState({ noOfInstallmentArray })

    }
    changeDescription = (value, index) => {
        console.log(value, index)
        let { noOfInstallmentArray } = this.state;
        noOfInstallmentArray.forEach((item, valueIndex) => {
            if (index === valueIndex) {
                item.description = value
            }
        })
        this.setState({ noOfInstallmentArray }, () => {
            console.log('description', this.state.noOfInstallmentArray)
        })
    }
    changeFinanceRemarks = (value, index) => {
        console.log(value, index)
        let { noOfInstallmentArray } = this.state;
        noOfInstallmentArray.forEach((item, valueIndex) => {
            if (index === valueIndex) {
                item.financeRemarks = value
            }
        })
        this.setState({ noOfInstallmentArray })
    }
    changeRemarks = (value, index) => {
        console.log(value, index)
        let { noOfInstallmentArray } = this.state;
        noOfInstallmentArray.forEach((item, valueIndex) => {
            if (index === valueIndex) {
                item.remarks = value
            }
        })
        this.setState({ noOfInstallmentArray, remarks: value })
    }
    changeRemarks1 = (value, index) => {
        // if same remarks have to be added to all uncomment it
        // let { noOfInstallmentArray } = this.state;
        // noOfInstallmentArray.forEach((item) => {
        //     item.remarks = value
        // })
        this.setState({ remarks: value })
    }
    onCheckStudent = (item) => {
        return null
    }
    showPreview = () => {
        this.setState({ showPreview: true, openModal1: true });
        let PaymentFormData = [];
        PaymentFormData.push({
            date: new Date(),
            amount: this.state.noOfInstallmentArray[0]['amount'],
            cardNumber: "5123XXXXXXXX2346",
            creditDebit: "Credit",
            cardType: "Visa",
            nameOnCard: "Test",
            transactionNumber: "B40605489E8D93C353F16FC0FEC56165",
            remarks: "-"
        })
        this.setState({ PaymentFormData: PaymentFormData });
        console.log("****Show Preview****")
    }
    closeModal1 = () => {
        this.setState({ openModal1: false });
    };
    onSelectDate = (value, selectIndex, event) => {
        // console.log(value, selectIndex, event)
        // let { noOfInstallmentArray } = this.state
        // noOfInstallmentArray.forEach((item, index) => {
        //     if (index === selectIndex) {
        //         item.nextMonth = value
        //     }
        // })
        // console.log(noOfInstallmentArray)
        // this.setState({ noOfInstallmentArray: noOfInstallmentArray })
    }
    openAssignModel = () => {
        this.setState({ showSellerList: true, openModal: true });

    }
    gobackToPreview = () => {
    
    }
    render() {
        return (
            <React.Fragment>
                {this.state.loader ? <Loader /> : null}
                {!this.state.showOnboardPreview ?
                    <React.Fragment>
                        <React.Fragment>
                            <div className="trial-balance-header-title" onClick = {this.gobackToPreview}>
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Onboard Users</p>
                            </div>
                        </React.Fragment>
                        <div className="invoice-page-wrapper new-table-wrap ">
                            {this.state.containerNav == undefined ? null : (
                                <ContainerNavbar
                                    containerNav={this.state.containerNav}
                                    onAddNew={() => this.newOnboard()}
                                />
                            )}
                            <div className="table-wrapper task-table-wrapper" style={{ marginTop: "10px" }}>
                                {this.state.onboardData.length == 0 ? (
                                    <p className="noprog-txt">{this.state.noProg}</p>
                                ) : (<React.Fragment>
                                    <div className="onboard-table-main">
                                        {console.log("data", this.state.onboardData)}
                                        <ZqTable
                                            data={this.state.onboardData}
                                            rowClick={(item) => { this.onPreviewOnboard(item); }}
                                            onRowCheckBox={(item) => { this.onCheckStudent(item) }}
                                            handleActionClick={(item, index, hd, name) => { this.handleActionClick(item, index, hd, name) }}
                                        />
                                    </div>
                                </React.Fragment>)}
                            </div>
                        </div>
                    </React.Fragment>
                    : <React.Fragment>
                        <React.Fragment>
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.setState({ showOnboardPreview: false, activeTab: 0 }) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title" onClick={() => { this.setState({ showOnboardPreview: false, activeTab: 0 }) }}> Onboard Listing | {this.state.rowClickData.applicationId}</p>
                            </div>
                        </React.Fragment>
                        <div className="tab-wrapper" style={{ width: "100%" }}>
                            <Tabs
                                value={this.state.activeTab}
                                indicatorColor="primary"
                                textColor="primary"
                                onChange={this.tabChangeInternal}>
                                {this.state.tabData.map((tab, tabIndex) => {
                                    return <Tab label={tab} />
                                })}
                            </Tabs>
                            {/* {localStorage.getItem("user") == "Finance" ? (
                                <div className="onboard-btn-group">
                                    <Button className="confirm-payment-btn cancel-onboard" onClick={this.onReject}>Reject</Button>
                                    <Button className="confirm-payment-btn submit-onboard" onClick={this.onAccept}>Approve</Button>
                                </div>)
                                :
                                localStorage.getItem("user") == "Headseller" ?
                                (<div className="onboard-btn-group">
                                    <Button className="confirm-payment-btn cancel-onboard" onClick={this.openAssignModel}>Assign</Button>
                                    <Button className="confirm-payment-btn submit-onboard" onClick={this.onSubmit}>Submit</Button>
                                    </div>)
                                    :
                                    (<div className="onboard-btn-group">
                                    <Button className="confirm-payment-btn cancel-onboard" onClick={this.onCancel}>Cancel</Button>
                                    <Button className="confirm-payment-btn submit-onboard" onClick={this.onSubmit}>Submit</Button>
                                    </div>)
                            } */}
                            <div className="preview-list-tab-data">
                                {this.state.activeTab === 0 ?
                                    <React.Fragment>
                                        {console.log(this.state.onboardStudentForm)}
                                        <div className="receive-payment-table student-form" style={{ position: "relative", top: "8vh" }}>
                                            {this.state.onboardStudentForm.length > 0 ? <ZenForm inputData={this.state.onboardStudentForm} /> : null}
                                        </div>
                                    </React.Fragment>
                                    : this.state.activeTab === 1 ?
                                        <React.Fragment>
                                            {console.log(" this.state.onboardParentForm", this.state.onboardParentForm)
                                            }
                                            <div className="receive-payment-table student-form" style={{ position: "relative", top: "8vh" }}>
                                                {this.state.onboardParentForm.length > 0 ? <ZenForm inputData={this.state.onboardParentForm} /> : null}
                                            </div>
                                        </React.Fragment>
                                        : this.state.activeTab === 2 ?
                                            <React.Fragment>
                                                <div className="receive-amount-table onBoradTable">
                                                    <p className="payment-details-header" style={{padding:"25px 0px 25px 20px"}}>Payment Details</p>
                                                    <table className="receive-pay-tableFormat" style={{ width: "40%", margin: "20px" }}>
                                                        <tbody>
                                                            <tr>
                                                                <td style={{ padding: "0px 20px" }}>Program</td>
                                                                <td style={{ padding: "0px", borderRadius: "0px" }}><SelectPicker className="SelectPicker-seller"
                                                                    data={this.state.programData}
                                                                    placeholder="Select Program Plan"
                                                                    onSelect={(value, item, event) => this.onSelectProgramPlan(value, item, event, this.state.inputData)}
                                                                    onClean={(value) => this.onCleanSeller(value, this.state.inputData)}
                                                                    defaultValue={this.state.selectedProgramType}
                                                                    style={{ width: '100%', marginRight: "10px", borderRadius: "0px" }} /></td>
                                                            </tr>
                                                            <tr>
                                                                <td style={{ padding: "0px 20px" }}> Amount </td>
                                                                <td style={{ padding: "0px" }}><Input placeholder="Enter Amount"  value={Number(this.state.getAmount)} style={{ borderRadius: "0px" }} /></td>
                                                            </tr>
                                                            <tr>
                                                                <td style={{ padding: "0px 20px" }}> Discount Type</td>
                                                                <td style={{ padding: "0px" }}> <SelectPicker className="SelectPicker-seller"
                                                                    data={this.state.discountType}
                                                                    placeholder="Select Discount Type"
                                                                    onSelect={(value, item, event) => this.onDiscountChanges(value, item, event, this.state.inputData)}
                                                                    defaultValue={this.state.selectedDiscountType}
                                                                    onClean={(value) => this.onClearDiscount(value, this.state.inputData)}
                                                                    style={{ width: '100%', marginRight: "10px", borderRadius: "0px" }} />
                                                                </td>
                                                            </tr>
                                                            {this.state.percentageField ?
                                                                <tr>
                                                                    <td style={{ padding: "0px 20px" }}> Percentage %</td>
                                                                    <td style={{ padding: "0px" }}><Input placeholder="Enter Percentage"  value={Number(this.state.allotedPercentage)} style={{ borderRadius: "0px" }} /></td>
                                                                </tr>
                                                                : null}
                                                            <tr>
                                                                <td style={{ padding: "0px 20px" }}> Discount Amount</td>
                                                                <td style={{ padding: "0px" }}><Input placeholder="Enter Discount" value={Number(this.state.discountAmount)} readOnly={this.state.checkIsPercentage} style={{ borderRadius: "0px" }} /></td>
                                                            </tr>
                                                            <tr>
                                                                <td style={{ padding: "0px 20px" }}> Total </td>
                                                                <td style={{ padding: "0px" }}><Input placeholder="Enter Total" value={Number(this.state.totalAmount).toFixed(2)} style={{ borderRadius: "0px" }} /></td>
                                                            </tr>
                                                            <tr>
                                                                <td style={{ padding: "0px 20px" }}> No. of Installment </td>
                                                                <td style={{ padding: "0px", width: 295 }}>

                                                                    <SelectPicker className="SelectPicker-seller"
                                                                        data={this.state.noOfInstallment}
                                                                        placeholder="Select No. of Installments"
                                                                        onSelect={(value, item, event) => this.onInstallmentChanges(value, item, event, this.state.inputData)}
                                                                        onClean={(value) => this.onCleanSeller(value, this.state.inputData)}
                                                                        defaultValue={this.state.selectedNoofInstallment}
                                                                        style={{ width: '100%', marginRight: "10px", borderRadius: "0px" }} />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style={{ padding: "0px 20px" }}> Remarks </td>
                                                                <td style={{ padding: "0px", width: 295 }}>
                                                                    <Input placeholder="Enter Remarks" value={localStorage.getItem("user") == "Finance" ? "Agreed for 4 Installments" : this.state.remarks}
                                                                        style={{ borderRadius: "0px" }} />
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </React.Fragment> : this.state.activeTab === 3 ? <>
                                                <div className="receive-amount-table onBoradTable">
                                                    <React.Fragment>
                                                        <div>
                                                            <p className="payment-details-header" style={{padding:"25px 0px 25px 20px"}}>Installment Details</p>
                                                            <table className="receive-pay-tableFormat" style={{ width: "97%", margin: "20px" }}>
                                                                <thead className="receive-pay-tableFormatHead">
                                                                    <th style={{ width: "150px" }}>Installment No.</th>
                                                                    <th style={{ width: "295px" }}>Description</th>
                                                                    <th style={{ width: "295px" }}>Due Date</th>
                                                                    <th style={{ width: "295px" }}>Percentage</th>
                                                                    <th style={{ width: "295px" }}>Amount</th>
                                                                    <th style={{ width: "295px" }}>Seller Remarks</th>
                                                                    <th style={{ width: "295px" }}>Finance Remarks</th>
                                                                    <th style={{ width: "95px" }}>Status</th>
                                                                </thead>
                                                                <tbody>
                                                                    {this.state.noOfInstallmentArray.map((item, index) => {
                                                                        console.log(this.state.noOfInstallmentArray)
                                                                        return (
                                                                            <tr onClick={item.status === "Paid" ? this.showPreview : null}>
                                                                                <td style={{ padding: "0px 20px" }}>Installment {index + 1}</td>
                                                                                <td style={{ padding: "0px" }}>
                                                                                    <Input style={{ width: "100%", borderRadius: "0px" }} placeholder="Enter Description"
                                                                                        type="text" 
                                                                                        value={item.description}
                                                                                    />
                                                                                </td>
                                                                                <td style={{ padding: "0px" }}>
                                                                                    <DatePicker oneTap style={{ width: "100%", borderRadius: "0px" }}
                                                                                        onSelect={(value, item, event) => this.onSelectDate(value, index)}
                                                                                        value={item.nextMonth}
                                                                                        readOnly={item.status === "Paid" ? true : false}
                                                                                        format='DD/MM/YYYY' />
                                                                                </td>
                                                                                <td style={{ padding: "0px" }}>
                                                                                    <Input style={{ width: "100%", borderRadius: "0px" }} placeholder="Enter Percentage"
                                                                                        type="number" 
                                                                                        readOnly={item.status === "Paid" ? true : false}
                                                                                        value={Number(item.percentage)} />
                                                                                </td>
                                                                                <td style={{ padding: "0px" }}>
                                                                                    <Input style={{ width: "100%", borderRadius: "0px" }}
                                                                                        placeholder="Enter Amount" type="number"
                                                                                        readOnly={item.status === "Paid" ? true : false}
                                                                                        value={Number(item.amount)} />
                                                                                </td>
                                                                                <td style={{ padding: "0px" }}>
                                                                                    <Input style={{ width: "100%", borderRadius: "0px" }}
                                                                                        placeholder="Enter Seller Remarks" type="text"
                                                                                        readOnly={localStorage.getItem("user") == "Finance" ? true : false}
                                                                                        value={localStorage.getItem("user") == "Finance" ? "-" : item.remarks} />
                                                                                </td>
                                                                                <td style={{ padding: "0px" }}>
                                                                                    <Input style={{ width: "100%", borderRadius: "0px" }}
                                                                                        placeholder="Enter Finance Remarks" type="text"
                                                                                        readOnly={item.status === "Paid" || localStorage.getItem("user") == "Seller" || localStorage.getItem("user") == "Headseller" ? true : false}
                                                                                        value={item.financeRemarks} />
                                                                                </td>
                                                                                <td className={`Status-${item.status}`}>
                                                                                    <span>{item.status}</span>
                                                                                </td>
                                                                            </tr>
                                                                        )
                                                                    })}
                                                                    <tr>
                                                                        <td style={{ fontSize: 14 }}>Total</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td style={{ fontSize: 14 }}> 100</td>
                                                                        <td style={{ fontSize: 14 }}>{Number(this.state.totalAmount).toFixed(2)}</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </React.Fragment>
                                                </div>
                                            </> : null}
                            </div>
                        </div>
                    </React.Fragment>}
                {this.showPreview ? <React.Fragment>
                    <Modal className="reconcile-preview-payment-wrap" style={{ top: "5%", borderRadius: "2px" }} onHide={this.closeModal1} size="xs" show={this.state.openModal1}>
                        <Modal.Header>
                            <Modal.Title></Modal.Title>
                        </Modal.Header>
                        <Modal.Body className="bodyContent" style={{ paddingBottom: "10px", marginTop: "10px" }} >
                            <Payment
                                StudentRegId={this.state.StudentRegId}
                                appId={this.state.rowClickData.applicationId}
                                StudentName={this.state.StudentName}
                                StudentClass={this.state.StudentClass}
                                paymentType={this.state.paymentType}
                                PaymentFormData={this.state.PaymentFormData}
                                onPaymentPreview={true}
                            />
                        </Modal.Body>
                    </Modal>
                </React.Fragment> : null}
                {this.state.showSellerList ? <React.Fragment>
                    <Modal className="show-seller-list-wrap" style={{ top: "5%", borderRadius: "2px" }} onHide={this.closeModal} size="xs" show={this.state.openModal}>
                        <Modal.Header>
                            <Modal.Title></Modal.Title>
                        </Modal.Header>
                        <Modal.Body className="bodyContent" style={{ paddingBottom: "10px", marginTop: "10px" }} >
                            <div className="select-picker-main" style={{ marginTop: "0px" }}>
                                <p className="payment-details-header" style={{ padding: "0px", paddingBottom: "5px" }}>Select a Seller: </p>
                                <SelectPicker className="SelectPicker-seller"
                                    data={this.state.selectSeller}
                                    placeholder="Select Seller"
                                    onSelect={(value, item, event, data) => this.onSelectSeller(value, item, event, this.state.inputData)}
                                    onClean={(value) => this.onCleanSeller(value, this.state.inputData)}
                                    style={{ width: '100%', marginRight: "10px", borderRadius: "0px" }} />
                            </div>
                        </Modal.Body>
                    </Modal>
                </React.Fragment> : null}

            </React.Fragment>)
    }
}
export default withRouter(Onboard)