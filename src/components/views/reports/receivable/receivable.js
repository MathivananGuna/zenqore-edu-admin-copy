import React, { Component } from 'react';
import '../../../../scss/fees-type.scss';
import '../../../../scss/setup.scss';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../utils/Table/table-component";
import PaginationUI from "../../../../utils/pagination/pagination";
import Loader from "../../../../utils/loader/loaders";
import LoanDataJson from './receivable.json';
import ZenForm from "../../../../components/input/form";
import xlsx from 'xlsx';
import StudentDetailsForm from './studentDetails.json';
import { Input, InputGroup } from 'rsuite';
import { DatePicker } from 'rsuite';
import moment from 'moment';
import { Alert, Modal, SelectPicker } from "rsuite";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import '../../../../scss/input.scss';
import "../../../../scss/newcss.scss";
import '../../../../scss/tabs.scss';
import '../../../../scss/student.scss';
import '../../../../scss/receive-payment.scss';
import '../../../../scss/payment-portal.scss';
import '../../../../scss/portal-stepper.scss';
import '../../../../scss/student-reports.scss';
import '../../../../scss/setup.scss';
import "../../../../scss/task.scss";
import "../../../../scss/onboard-user.scss";
import OnboardComponent from '../../../onboard/onboard.js'
import Preview from './preview';
import PaidPreview from './paidPreview'
class Receivable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            containerNav: {
                isBack: false,
                name: "List of Receivables",
                isName: true,
                isSearch: true,
                isSort: false,
                isPrint: true,
                isDownload: true,
                isShare: false,
                isNew: false,
                newName: "New",
                isSubmit: false,
                isSelectAppReport: true
            },
            tablePrintDetails: [],
            page: 1,
            limit: 10,
            selectMonth: "",
            totalRecord: 0,
            activeTab: 0,
            totalPages: 1,
            noProg: "Fetching Data ...",
            PreStudentName: "",
            tabData: ['Student Details', 'Parent Details', 'Payment Details', 'installment Details'],
            previewList: false,
            monthData: ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"],
            showStudentPreview: false,
            totalReceivableData: [
                {
                    "Reg ID": "-",
                    "Student Name": "Suraj Reddy",
                    "Class/Batch": "Bright Kids at Home",
                    "Total": "19845.00",
                },
            ],
            receivableData: [],
            previewListForm: LoanDataJson.formJson,
            appDatas: [
                {
                    label: "6 Months",
                    value: 6
                },
                {
                    label: "12 Months",
                    value: 12
                }
            ],
            filterKey: 6,
            onboardStudentForm: StudentDetailsForm.ShowOnboardForm['Student Details'],
            onboardParentForm: StudentDetailsForm.ShowOnboardForm['Parent Details'],
            rowClickItems: {},

            noOfInstallmentArray: [{
                id: 1, percentage: 0, description: "", amount: 5954, nextMonth: new Date(), discount: 0, remarks: "", financeRemarks: "", status: "Planned"
            },
            {
                id: 2, percentage: 0, description: "", amount: 5954, nextMonth: new Date(), discount: 0, remarks: "", financeRemarks: "", status: "Planned"
            },
            {
                id: 3, percentage: 0, description: "", amount: 3969, nextMonth: new Date(), discount: 0, remarks: "", financeRemarks: "", status: "Planned"
            },
            {
                id: 4, percentage: 0, description: "", amount: 3969, nextMonth: new Date(), discount: 0, remarks: "", financeRemarks: "", status: "Planned"
            }
            ],

        }

    }
    componentDidMount() {
        let monthData = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"]
        let currentMonth = new Date().getMonth()
        let getListOfMonth = []

        let getJsonData = [{ "title": "Receivable" }, { "title": "Received" }]

        let forLength = Number(6) + Number(currentMonth)
        for (let i = currentMonth; i < forLength; i++) {
            getListOfMonth.push(monthData[i])
        }
        getJsonData.forEach(element1 => {
            getListOfMonth.forEach(element2 => {
                Object.assign(element1, { [element2]: "0.00" })
            })
        })
        getJsonData.forEach(item => {
            if (item.title === "Received") {
                item['mar'] = this.formatCurrency(5954)
            }
            else if (item.title === "Receivable") {
                item['mar'] = this.formatCurrency(5954)
                item['apr'] = this.formatCurrency(5954)
                item['may'] = this.formatCurrency(3969)
                item['jun'] = this.formatCurrency(3969)
            }



        })
        // let receivableArray = []
        // receivableArray.push(getJsonData)
        this.setState({ receivableData: getJsonData })
    }
    onAddNewLoans = () => { }
    onPreviewLoan = (e) => {
        console.log(e)
        let obj = {
            "Reg ID": e["Reg ID"],
            "Class/Batch": e["Class/Batch"],
            "Student Name": e["Student Name"],
        }

        this.setState({ previewList: true, rowClickItems: e })

    }
    onPaginationChange = () => {

     }
    onPreviewLoanList = () => {

    }
    moveBackTable = () => {
        this.setState({ previewList: false })
    }
    actionClickFun = (e) => {

        console.log(e);
    }
    formatCurrency = (amount) => {
        return new Intl.NumberFormat("en-IN", {
            style: "currency",
            currency: "INR",
            // minimumFractionDigits: 2,
            // maximumFractionDigits: 2,
        }).format(amount);
    };
    handleTabChange = () => { }
    allSelect = () => { }
    onInputChanges = () => { }
    searchHandle = () => { }
    //   process = (a, b) => {
    //     let printVal = []
    //       monthData.forEach((data, i) => {
    //           if (i >= a && i <= b + 1)
    //           {
    //               printVal.push(data)
    //           }
    //       });
    //     if(printVal.length === b){
    //     console.log(printVal)
    //     }
    //     else{
    //     let findDiff = Math.abs(b - printVal.length);
    //         monthData.forEach((data, i) => {
    //             if (i < findDiff) {
    //                 printVal.push(data)
    //             }
    //         }); console.log(printVal)
    //     }
    // }

    onSelectPrgmPlan = (value, item, label) => {
        let monthData = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"]
        let currentMonth = new Date().getMonth()
        let getListOfMonth = []
        let getJsonData = [{ "title": "Receivable" }, { "title": "Received" }]
        let ReceivableArray = []
        let forLength = Number(value) + Number(currentMonth)
    

        this.setState({ selectMonth: value, filterKey: value, receivableData: [] }, () => {
            if (value === 6) {
                for (let i = currentMonth; i < forLength; i++) {
                    getListOfMonth.push(monthData[i])
                }
                getJsonData.forEach(element => {
                    getListOfMonth.forEach(element2 => {
                        Object.assign(element, { [element2]: "0.00" })
                    })
                })
                getJsonData.forEach(item => {
                    if (item.title === "Received") {
                        item['mar'] = this.formatCurrency(5954)
                    }
                    else if (item.title === "Receivable") {
                        item['mar'] = this.formatCurrency(5954)
                        item['apr'] = this.formatCurrency(5954)
                        item['may'] = this.formatCurrency(3969)
                        item['jun'] = this.formatCurrency(3969)
                    }
                })
                console.log('getJsonData',getJsonData)
                this.setState({ receivableData: getJsonData }, () => {
                    
                })
            }
            else if (value === 12) {

                for (let i = currentMonth; i <= 11; i++) {
                    getListOfMonth.push(monthData[i])
                }
            
                getListOfMonth.push(monthData[0])
                getListOfMonth.push(monthData[1])
                
                getJsonData.forEach(element => {
                    getListOfMonth.forEach(element2 => {
                        Object.assign(element, { [element2]: "0.00" })
                    })
                })
                getJsonData.forEach(item => {
                    if (item.title === "Received") {
                        item['mar'] = this.formatCurrency(5954)
                    }
                    else if (item.title === "Receivable") {
                        item['mar'] = this.formatCurrency(5954)
                        item['apr'] = this.formatCurrency(5954)
                        item['may'] = this.formatCurrency(3969)
                        item['jun'] = this.formatCurrency(3969)
                    }



                })
                this.setState({ receivableData: getJsonData }, () => {
                })
            }


        })

        //this.onDataCall()
    }

    onSelectClean = () => {
        this.setState({ filterKey: 'All' })
    }
    onDownloadEvent = () => {
        this.setState({ isLoader: true })
        var createXlxsData = []
        var ws = xlsx.utils.json_to_sheet(createXlxsData);
        var wb = xlsx.utils.book_new();
        xlsx.utils.book_append_sheet(wb, ws, "Receivable Reports");
        xlsx.writeFile(wb, "Receivable_Reports.xlsx");
        //xlsx.writeFile(wb, "demand_note_reports.csv");
        this.setState({ isLoader: false })
    }

    goBack = () => {
        this.setState({ previewList: false })
    }
    previewDetails = () => {

        this.setState({ showStudentPreview: true })
    }
    tabChangeInternal = (e, value) => {
        // if (value == 1) {
        //     let { onboardParentForm, onboardForm } = this.state
        //     let parentForm = []
        //     parentForm = onboardForm['Parent Details']
        //     this.setState({
        //         onboardParentForm: []
        //     }, () => {
        //         onboardParentForm = parentForm
        //         this.setState({ onboardParentForm, onboardStudentForm: [] }, () => {
        //             console.log(this.state.onboardParentForm)
        //         })
        //     })
        // }
        // if (value == 0) {
        //     let { onboardStudentForm, onboardForm } = this.state
        //     let studentForm = []
        //     studentForm = onboardForm['Student Details']
        //     this.setState({
        //         onboardStudentForm: []
        //     }, () => {
        //         onboardStudentForm = studentForm
        //         this.setState({ onboardStudentForm, onboardParentForm: [] })
        //     })
        // }
        // if (value == 2) {
        //     let { onboardStudentForm, onboardForm } = this.state
        //     let paymentDetails = []
        //     paymentDetails = onboardForm['Payment Details']
        //     this.setState({
        //         onboardStudentForm: []
        //     }, () => {
        //         onboardStudentForm = paymentDetails
        //         this.setState({ onboardStudentForm, onboardParentForm: [] })
        //     })
        // }
        // if (value == 3) {
        //     let { noOfInstallmentArray, response, totalAmount, getAmount } = this.state
        //     let getTotalAmounnt = this.state.totalAmount;
        //     noOfInstallmentArray = []
        //     console.log(this.state.noOfInstallmentArray)
        //     let todayDate = new Date()
        //     let nextMonth
        //     if (this.state.getAmountOnchange === false) {
        //         for (let i = 0; i < this.state.noOfInstallmentArray.length; i++) {
        //             nextMonth = i == 0 ? todayDate : nextMonth
        //             let percentage = (Number(this.state.noOfInstallmentArray[i].amount) * 100) / Number(totalAmount)
        //             console.log(this.state.noOfInstallmentArray[i].amount, totalAmount)
        //             percentage = Number(percentage).toFixed(2)
        //             if (i > 0) {
        //                 nextMonth = new Date(new Date().setMonth(nextMonth.getMonth() + 1))
        //             }
        //             noOfInstallmentArray.push({
        //                 id: i,
        //                 percentage: percentage,
        //                 amount: this.state.noOfInstallmentArray[i].amount,
        //                 nextMonth: nextMonth,
        //                 description: this.state.noOfInstallment[i].description,
        //                 remarks: this.state.noOfInstallmentArray[i].remarks,
        //                 financeRemarks: undefined,
        //                 status: this.state.noOfInstallmentArray[i].status
        //             })
        //         }
        //         this.setState({ noOfInstallmentArray: noOfInstallmentArray })
        //     }
        //     else if (this.state.getAmountOnchange === true) {
        //         for (let i = 1; i <= this.state.selectedNoofInstallment; i++) {
        //             nextMonth = i == 1 ? todayDate : nextMonth
        //             let Amount = Number(getAmount) / Number(this.state.selectedNoofInstallment)
        //             console.log(Amount, getAmount, this.state.selectedNoofInstallment)
        //             let percentage = (Number(Amount) * 100) / Number(totalAmount)
        //             percentage = Number(percentage).toFixed(2)
        //             if (i > 1) {
        //                 nextMonth = new Date(new Date().setMonth(nextMonth.getMonth() + 1))
        //             }

        //             noOfInstallmentArray.push({
        //                 id: i,
        //                 percentage: percentage,
        //                 amount: Amount,
        //                 nextMonth: nextMonth,
        //                 description: this.state.noOfInstallmentArray[0].description,
        //                 remarks: this.state.noOfInstallmentArray[0].status,
        //                 financeRemarks: undefined,
        //                 status: this.state.noOfInstallmentArray[0].status

        //             })
        //         }
        //         this.setState({ noOfInstallmentArray: noOfInstallmentArray })
        //     }
        // }
        this.setState({ activeTab: value })
    }
    comeback = () => {
        this.setState({ previewList: false });
    }
    render() {
        return (
            <>
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.previewList == false ?
                <div className="list-of-feeType-mainDiv">
                    <React.Fragment>
                        <React.Fragment>
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Receivable</p>
                            </div>
                        </React.Fragment>
                        <div className="masters-body-div">
                            <React.Fragment>
                                <ContainerNavbar containerNav={this.state.containerNav} onAddNew={() => { this.onAddNewLoans(); }} searchValue={(searchValue) => this.searchHandle(searchValue)} Selectdata={this.state.appDatas} placeholder={"Duration"} onSelectDefaultValue={this.state.filterKey} onSelectClean={this.onSelectClean} onDownload={() => this.onDownloadEvent()} onSelectChange={this.onSelectPrgmPlan} />
                            </React.Fragment>
                            <div className="table-wrapper" style={{ marginTop: "10px" }}>
                                {this.state.receivableData.length == 0 ? <p className="noprog-txt">{this.state.noProg}</p> :
                                    <React.Fragment>
                                        <div className="remove-last-child-table" >
                                            <ZqTable
                                                data={this.state.receivableData}
                                                allSelect={this.allSelect}
                                                rowClick={(item) => { this.onPreviewLoan(item) }}
                                                onRowCheckBox={(item) => { this.allSelect(item) }}
                                                handleActionClick={(item) => { this.actionClickFun(item) }}
                                            />

                                        </div>
                                    </React.Fragment>
                                }
                                {this.state.receivableData.length !== 0 ? <PaginationUI onPaginationApi={this.onPaginationChange} totalPages={this.state.totalPages} limit={this.state.limit} total={this.state.totalRecord} /> : null}
                            </div>
                        </div>
                    </React.Fragment>
                </div>
                    
                    :
                    <React.Fragment>
                        <PaidPreview comeback={this.comeback} />
                    </React.Fragment>
                }
          </>
        )
    }
}
export default Receivable;