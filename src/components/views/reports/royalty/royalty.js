import React, { Component } from "react";
import '../../../../scss/student.scss';
import '../../../../scss/setup.scss';
import StudentFormJson from './student.json';
import axios from "axios";
import moment from 'moment';
import { Timeline, Drawer } from 'rsuite';
import Loader from "../../../../utils/loader/loaders";
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../utils/Table/table-component";
import PaginationUI from "../../../../utils/pagination/pagination";
import ZenTabs from '../../../../components/input/tabs';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import ZqHistory from "../../../../gigaLayout/change-history";
import DateFormatContext from '../../../../gigaLayout/context';
import xlsx from 'xlsx';
class ListofStudents extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            campusId: localStorage.getItem('campusId') === "null" || localStorage.getItem('campusId') === null ? undefined : localStorage.getItem('campusId'),
            containerNav: StudentFormJson.containerNav,
            containerNavRoyalty: StudentFormJson.containerNavRoyality,
            sampleStudentsData: [],
            tableResTxt: "Fetching Data...",
            studentsFormjson: StudentFormJson.studentJson,
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 1,
            tabViewForm: false,
            studentName: "",
            history: false,
            showHistory: false,
            isPreviewStudentData: true,
            historyData: StudentFormJson.HistoryData,
            formLoadStatus: false,
            downloadTotalRecord: 1,
            filterData: [],
            searchValue: "",
            previewRoyalityData: false,
            previewDataS:[]
        }
    }
    static contextType = DateFormatContext;
    componentDidMount() {
        console.log("contextLabel", this.context.reportLabel)
        this.getDetailsData();
    }
    getDetailsData = () => {
        this.setState({ sampleStudentsData: [] })
        let data = [
            {
                "Franchise name": "Zenqore Education",
                "Total Fee Collected": "₹25,00,000",
                "Percentage royalty": "20",
                "Royalty amount": "₹5,00,000.00",
                "Royalty Paid": "₹2,00,000.00",
                "Balance": "₹3,00,000.00"
            },
            {
                "Franchise name": "Ken42 Education",
                "Total Fee Collected": "₹35,00,000",
                "Percentage royalty": "25",
                "Royalty amount": "₹10,00,000.00",
                "Royalty Paid": "₹1,00,000.00",
                "Balance": "₹9,00,000.00"
            }
        ];
        let dataOne = [
            {
                "Franchise name": "Zenqore Education",
                "Total Fee Collected": "₹25,00,000",
                "Percentage royalty": "20",
                "Royalty amount": "₹5,00,000.00",
                "Royalty received": "₹2,00,000.00",
                "Balance": "₹3,00,000.00"
            },
            {
                "Franchise name": "Ken42 Education",
                "Total Fee Collected": "₹35,00,000",
                "Percentage royalty": "25",
                "Royalty amount": "₹10,00,000.00",
                "Royalty received": "₹1,00,000.00",
                "Balance": "₹9,00,000.00"
            }
        ]
        this.setState({ sampleStudentsData: localStorage.getItem("tempEmail") === "fc.staff.f1@zenqore.com" ? dataOne : data })
    }
    handleBackFun = () => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`); }
    onPreviewStudentList = (e) => {
        console.log(e);
        var previewData = [
            {
                "receipt Id": "RCPT_2020-21_001",
                "reg Id": "EUROJBN57720",
                "Student name": "Aadhyanth Arun",
                "Academic year": "2021-22",
                "class/batch": "2021-Playshool-18m",
                "Description": "New Admission Fee",
                "Total Fees": "₹60,000.00",
                "Paid": "₹60,000.00",
                "Paid on": "07-03-2021",
                "Mode": "CHEQUE",
                "Pending": "₹0.00",
                "Txn Id": "182022",
                "Status": "Partial"
            },
            {
                "receipt Id": "RCPT_2020-21_002",
                "reg Id": "EUROJBN17618",
                "Student name": "Natarajan Senthil Nathan",
                "Academic year": "2021-22",
                "class/batch": "2021-Playshool-18m",
                "Description": "New Admission Fee",
                "Total Fees": "₹40000.00",
                "Paid": "₹8,555.00",
                "Paid on": "31-03-2021",
                "Mode": "CHEQUE",
                "Pending": "₹0.00",
                "Txn Id": "632892",
                "Status": "Partial"
            },
            {
                "receipt Id": "RCPT_2020-21_003",
                "reg Id": "EUROJBN56920",
                "Student name": "Ganishika Suresh",
                "Academic year": "2021-22",
                "class/batch": "2021-Playshool-18m",
                "Description": "New Admission Fee",
                "Total Fees": "₹30,000.00",
                "Paid": "5,000.00",
                "Paid on": "31-03-2021",
                "Mode": "CHEQUE",
                "Pending": "₹25000.00",
                "Txn Id": "501235",
                "Status": "Partial"
            },
            {
                "receipt Id": "RCPT_2020-21_004",
                "reg Id": "EUROJBN57020",
                "Student name": "Ishani SN",
                "Academic year": "2021-22",
                "class/batch": "2021-Playshool-18m",
                "Description": "New Admission Fee",
                "Total Fees": "₹30,000.00",
                "Paid": "1,507.00",
                "Paid on": "31-03-2021",
                "Mode": "CHEQUE",
                "Pending": "₹28493.00",
                "Txn Id": "452100",
                "Status": "Partial"
            },
        ]
        this.setState({ previewRoyalityData: true, previewDataS: previewData, tabViewForm: true })
    }
    onPreviewStudentList1 = (e) => {
        console.log(e);
        var previewData = [
            {
                "receipt Id": "RCPT_2020-21_001",
                "reg Id": "EUROJBN57720",
                "Student name": "Aadhyanth Arun",
                "Academic year": "2021-22",
                "class/batch": "2021-Playshool-18m",
                "Description": "New Admission Fee",
                "Total Fees": "₹60,000.00",
                "Paid": "₹60,000.00",
                "Paid on": "07-03-2021",
                "Mode": "CHEQUE",
                "Pending": "₹0.00",
                "Txn Id": "182022",
                "Status": "Partial"
            },
            {
                "receipt Id": "RCPT_2020-21_002",
                "reg Id": "EUROJBN17618",
                "Student name": "Natarajan Senthil Nathan",
                "Academic year": "2021-22",
                "class/batch": "2021-Playshool-18m",
                "Description": "New Admission Fee",
                "Total Fees": "₹40000.00",
                "Paid": "₹8,555.00",
                "Paid on": "31-03-2021",
                "Mode": "CHEQUE",
                "Pending": "₹0.00",
                "Txn Id": "632892",
                "Status": "Partial"
            },
            {
                "receipt Id": "RCPT_2020-21_003",
                "reg Id": "EUROJBN56920",
                "Student name": "Ganishika Suresh",
                "Academic year": "2021-22",
                "class/batch": "2021-Playshool-18m",
                "Description": "New Admission Fee",
                "Total Fees": "₹30,000.00",
                "Paid": "5,000.00",
                "Paid on": "31-03-2021",
                "Mode": "CHEQUE",
                "Pending": "₹25000.00",
                "Txn Id": "501235",
                "Status": "Partial"
            },
            {
                "receipt Id": "RCPT_2020-21_004",
                "reg Id": "EUROJBN57020",
                "Student name": "Ishani SN",
                "Academic year": "2021-22",
                "class/batch": "2021-Playshool-18m",
                "Description": "New Admission Fee",
                "Total Fees": "₹30,000.00",
                "Paid": "1,507.00",
                "Paid on": "31-03-2021",
                "Mode": "CHEQUE",
                "Pending": "₹28493.00",
                "Txn Id": "452100",
                "Status": "Partial"
            },
        ]
        this.setState({ previewRoyalityData: true, previewDataS: previewData, tabViewForm: true })
    }
    cancelPreview = () => {
        this.setState({ previewRoyalityData: false,  tabViewForm: false })
    }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            if (this.state.searchValue.length == 0) {
                this.getDetailsData()
            }
            else {
                this.searchHandle(this.state.searchValue)
            }
        })
    }
    moveBackTable = () => { this.setState({ tabViewForm: false }) }
    handleTabChange = (tabName, formDatas) => { }
    onFormSubmit = (data, item) => { }
    openHistory = () => { this.setState({ history: true, showHistory: true }); };
    onCloseForm = () => { this.setState({ showHistory: false }); };
    allSelectData = () => { }
    onInputChanges = (value, item, event, dataS) => {
        console.log(dataS);
        let studentFormJsonData = StudentFormJson.studentJson;
        if (dataS.name == "currenyType") {
            if (dataS.defaultValue == "INR") {
                Object.keys(studentFormJsonData).forEach(tab => {
                    studentFormJsonData[tab].forEach(task => {
                        if (task.name == "exchangeRate") {
                            task['defaultValue'] = "74.00";
                        }
                    })
                })
            }
            if (dataS.defaultValue == "USD") {
                Object.keys(studentFormJsonData).forEach(tab => {
                    studentFormJsonData[tab].forEach(task => {
                        if (task.name == "exchangeRate") {
                            task['defaultValue'] = "1.00";
                        }
                    })
                })
            }
        }
        this.setState({ formLoadStatus: false })
    }
    searchHandle = () => {}
    // searchHandle = (searchValue) => {
    //     console.log('searchValue', searchValue)
    //     this.setState({ LoaderStatus: true })
    //     this.setState({ searchValue: searchValue });
    //     if (searchValue.length > 0) {
    //         return axios.get(`${this.state.env['zqBaseUri']}/edu/master/students?orgId=${this.state.orgId}&page=${this.state.page}&limit=${this.state.limit}&searchKey=${searchValue}`, {
    //             headers: {
    //                 'Authorization': localStorage.getItem("auth_token")
    //             }
    //         }).then(res => {
    //             let printArr = res.data.data;
    //             let studentsDataTable = [];
    //             let RegID = this.context.reportLabel || "Reg ID"
    //             let batch = this.context.classLabel || "CLASS/BATCH";
    //             this.setState({ sampleStudentsData: [] }, () => {
    //                 if (res.data.status == "success") {
    //                     if (res.data.data.length == 0) {
    //                         this.setState({ tableResTxt: "No Data", sampleStudentsData: [], LoaderStatus: false })
    //                     }
    //                     else {
    //                         printArr.map((data) => {
    //                             studentsDataTable.push({
    //                                 [RegID]: data.regId,
    //                                 // "HEDA Id": "-",
    //                                 "First Name": data.firstName,
    //                                 "Last Name": data.lastName,
    //                                 [batch]: data.programPlan[0].title,
    //                                 "parent name": data.guardianDetails[0].firstName,
    //                                 "Phone no": data.phoneNo,
    //                                 "Email": data.email,
    //                                 "Admitted on": data.admittedOn ? data.admittedOn : "-",
    //                                 "Campus Name": data.campuseDetails ? data.campuseDetails.length !== 0 ? data.campuseDetails.map(item => item['legalName']) : "" : "",
    //                                 "is Final Year": data.isFinalYear === true ? "Yes" : data.isFinalYear === false ? "No" : "-",
    //                                 // "is Final Year": "-",
    //                                 "Status": data.status == 1 ? "Active" : "Inactive",
    //                                 "Item": JSON.stringify(data)
    //                             })
    //                         })
    //                         this.setState({
    //                             sampleStudentsData: studentsDataTable,
    //                             totalApiRes: res.data,
    //                             tabViewForm: false,
    //                             totalRecord: res.data.total,
    //                             totalPages: res.data.totalPages,
    //                             page: res.data.currentPage,
    //                             downloadTotalRecord: res.data.totalRecord,
    //                             LoaderStatus: false
    //                         })
    //                     }
    //                 }

    //                 else {
    //                     this.setState({ tableResTxt: "Loading Error", })
    //                 }
    //             })
    //         })
    //             .catch(err => {
    //                 this.setState({ tableResTxt: "Loading Error", sampleStudentsData: this.state.filterData, LoaderStatus: false })
    //             })
    //     }
    //     else {
    //         this.getDetailsData()
    //     }
    // }
    printScreen = () => {
        let page = 1
        let limit = this.state.downloadTotalRecord || 1
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/students?orgId=${this.state.orgId}&page=${page}&limit=${limit}`,
            headers: {
                'Authorization': this.state.authToken
            }
        }).then(res => {
            console.log("res", res)
            this.setState({ sampleStudentsData: [] }, () => {
                let printArr = res.data.data;
                let studentsDataTable = [];
                let RegID = this.context.reportLabel || "Reg ID"
                let batch = this.context.classLabel || "CLASS/BATCH";
                if (res.data.status == "success") {
                    if (res.data.data.length == 0) {
                        this.setState({ tableResTxt: "No Data" })
                    }
                    else {
                        printArr.map((data) => {
                            studentsDataTable.push({
                                [RegID]: data.regId,
                                // "HEDA Id": "-",
                                "First Name": data.firstName,
                                "Last Name": data.lastName,
                                [batch]: data.programPlan[0].title,
                                "parent name": data.guardianDetails[0].firstName,
                                "Phone no": data.phoneNo,
                                "Email": data.email,
                                "Admitted on": data.admittedOn ? data.admittedOn : "-",
                                "Campus Name": data.campuseDetails ? data.campuseDetails.length !== 0 ? data.campuseDetails.map(item => item['legalName']) : "" : "",
                                "is Final Year": data.isFinalYear === true ? "Yes" : data.isFinalYear === false ? "No" : "-",
                                // "is Final Year": "-",
                                "Status": data.status == 1 ? "Active" : "Inactive",
                            })
                        })
                        this.setState({
                            sampleStudentsData: studentsDataTable,
                        }, () => {
                            setTimeout(() => {
                                window.print()
                                this.getDetailsData()
                            }, 1);
                        })
                    }
                }
            })
        }
        ).catch(err => {
            console.log(err, 'errorrrrr')
        })
    }
    onDownload = () => {
        let page = 1
        let limit = this.state.downloadTotalRecord || 1
        this.setState({ LoaderStatus: true }, () => {
            try {
                axios({
                    method: 'get',
                    url: `${this.state.env["zqBaseUri"]}/edu/master/students?orgId=${this.state.orgId}&page=${page}&limit=${limit}`,
                    headers: {
                        'Authorization': this.state.authToken
                    }
                }).then(res => {
                    var createXlxsData = [];
                    let RegID = this.context.reportLabel || "Reg ID"
                    let batch = this.context.classLabel || "CLASS/BATCH";
                    if (res.status === 200) {
                        res.data.data.map((data) => {
                            createXlxsData.push({
                                [RegID]: data.regId,
                                // "HEDA Id": "-",
                                "First Name": data.firstName,
                                "Last Name": data.lastName,
                                [batch]: data.programPlan[0].title,
                                "parent name": data.guardianDetails[0].firstName,
                                "Phone no": data.phoneNo,
                                "Email": data.email,
                                "Admitted on": data.admittedOn ? data.admittedOn : "-",
                                "Campus Name": data.campuseDetails ? data.campuseDetails.length !== 0 ? data.campuseDetails.map(item => item['legalName']) : "" : "",
                                "is Final Year": data.isFinalYear === true ? "Yes" : data.isFinalYear === false ? "No" : "-",
                                // "is Final Year": "-",
                                "Status": data.status == 1 ? "Active" : "Inactive",
                            })
                        })
                        var ws = xlsx.utils.json_to_sheet(createXlxsData);
                        var wb = xlsx.utils.book_new();
                        xlsx.utils.book_append_sheet(wb, ws, "Students");
                        xlsx.writeFile(wb, "student.xlsx");
                        this.setState({ LoaderStatus: false })
                    }
                }).catch(err => {
                    console.log(err, 'errorrrrr')
                    this.setState({ LoaderStatus: false })
                })
            } catch (err) {
                console.log("try err", err)
            }
        })
    }
    render() {
        return (
            <div className="list-of-students-mainDiv table-head-padding">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.tabViewForm == false ?
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title">| Royalty</p>
                        </div>
                        <div className="masters-body-div">
                            {this.state.containerNav == undefined ? null :
                                <React.Fragment>
                                    <ContainerNavbar containerNav={this.state.containerNav} searchValue={(searchValue) => this.searchHandle(searchValue)} onDownload={this.onDownload} printScreen={this.printScreen} />
                                    <div className="print-time">{this.state.printTime}</div>
                                </React.Fragment>
                            }
                            <div className="remove-last-child-table remove-first-child-table">
                                {this.state.sampleStudentsData.length !== 0 ?
                                    < ZqTable
                                        allSelect={this.allSelectData}
                                        data={this.state.sampleStudentsData}
                                        rowClick={(item) => { this.onPreviewStudentList(item) }}
                                        onRowCheckBox={(item) => { this.onPreviewStudentList1(item) }} />
                                    :
                                    <p className="noprog-txt">{this.state.tableResTxt}</p>
                                }
                            </div>
                            <div>
                                {this.state.sampleStudentsData.length !== 0 ?
                                    <PaginationUI
                                        total={this.state.totalRecord}
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        currentPage={this.state.page} />
                                    :
                                    null
                                }
                            </div>
                        </div>
                    </React.Fragment> : null}
                {this.state.previewRoyalityData === true ?
                    <>
                        <React.Fragment>
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.cancelPreview} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title">| Royalty | Fee Collection</p>
                            </div>
                            <div className="masters-body-div">
                                {this.state.containerNav == undefined ? null :
                                    <React.Fragment>
                                        <ContainerNavbar containerNav={this.state.containerNavRoyalty} searchValue={(searchValue) => this.searchHandle(searchValue)} onDownload={this.onDownload} printScreen={this.printScreen} />
                                        <div className="print-time">{this.state.printTime}</div>
                                    </React.Fragment>
                                }
                                <div className="remove-last-child-table remove-first-child-table">
                                    {this.state.previewDataS.length !== 0 ?
                                        < ZqTable
                                            allSelect={this.allSelectData}
                                            data={this.state.previewDataS}
                                            rowClick={(item) => { this.onPreviewStudentList(item) }}
                                            onRowCheckBox={(item) => { this.onPreviewStudentList1(item) }} />
                                        :
                                        <p className="noprog-txt">{this.state.tableResTxt}</p>
                                    }
                                </div>
                                <div>
                                    {this.state.previewDataS.length !== 0 ?
                                        <PaginationUI
                                            total={this.state.totalRecord}
                                            onPaginationApi={this.onPaginationChange}
                                            totalPages={this.state.totalPages}
                                            limit={this.state.limit}
                                            currentPage={this.state.page} />
                                        :
                                        null
                                    }
                                </div>
                            </div>
                        </React.Fragment>
                    </> : null
                }
                {this.state.history ?
                    <Drawer size='xs' placement='right' show={this.state.showHistory}>
                        <Drawer.Header style={{ display: 'flex', paddingRight: '0px' }}>
                            <Drawer.Title className="change-history-title">CHANGE HISTORY</Drawer.Title>
                            <HighlightOffIcon title="Close" onClick={this.onCloseForm} className="change-history-close-btn" />
                        </Drawer.Header>
                        <Drawer.Body>
                            <Timeline align='left'>
                                <ZqHistory historyData={this.state.historyData} onCloseForm={this.onCloseForm} />
                            </Timeline>
                        </Drawer.Body>
                    </Drawer>
                    : null
                }
            </div>
        )
    }
}
export default ListofStudents;