import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import '../../../../scss/student-reports.scss';
import '../../../../scss/student.scss';
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import PaginationUI from "../../../../utils/pagination/pagination";
import '../../../../scss/common-table.scss';
import axios from 'axios';
import moment from 'moment';
import xlsx from 'xlsx';
import Loader from '../../../../utils/loader/loaders';
import DateFormatter from '../../../date-formatter/date-formatter'
import DateFormatContext from '../../../../gigaLayout/context'
class Refund extends Component {
    constructor(props) {
        super(props);
        this.state = {
            authToken: localStorage.getItem('auth_token'),
            env: JSON.parse(localStorage.getItem('env')),
            campusId :  localStorage.getItem("campusId"),
            userId : localStorage.getItem('userId'),
            orgId: localStorage.getItem('orgId'),
            containerNav: {
                isBack: false,
                name: "Refund",
                isName: true,
                isSearch: true,
                isSort: false,
                isPrint: true,
                isDownload: true,
                isShare: false,
                isNew: false,
                newName: "New",
                isSubmit: false,
                isSelectAppReport: true,
                selectPickerOption: true,
                selectCampusOption: true,
                selectSectionOption:true,
                selectFranchiseOption: false
            },
            tableHeader: ["REFUND ID", "REG ID", "STUDENT NAME", "ACADEMIC YEAR", "CLASS/BATCH", "REFUND AMOUNT", "MODE", "REFUNDED ON", "TXN ID", "STATUS"],
            printReportArr: [

            ],
            previewStudentTableData: [],
            previewTableData: [],
            showTable: true,
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 0,
            noData: false,
            isLoader: false,
            previewpage: 1,
            previewlimit: 10,
            previewtotalRecord: 0,
            previewtotalPages: 0,
            totalPreviewRecord: 0,
            particularItem: undefined,
            appDatas: [
                {
                    label: "All Program Plan",
                    value: "All"
                }
            ],
            campusData : [
                {
                    label: "All Campus",
                    value: "All"
                }
            ],
            sectionData : [
                {
                    label: "All Section",
                    value: "All"
                }
            ],
            filterKey: "All",
            typeOfPagination: '',
            fromDateData: '',
            toDateData: '',
            paginationCall: ""
        }
    }
    static contextType = DateFormatContext;
    componentDidMount = () => {
        this.onDataCall();
        this.filterDataNew();
        let tableHeader = ["REFUND ID", `${this.context.reportLabel ? this.context.reportLabel : 'REG ID'}`, "STUDENT NAME", "ACADEMIC YEAR", `${this.context.classLabel ? this.context.classLabel : "CLASS/BATCH"}`, "REFUND AMOUNT", "MODE", "REFUNDED ON", "TXN ID", "STATUS"]
        this.setState({ tableHeader })
    }
    // onPaginationChange = (page, limit) => {
    //     this.setState({ page: page, limit: limit }, () => {
    //         if (this.state.paginationCall === "allData") this.onDataCall();
    //         else if (this.state.paginationCall === "filterData") this.onFilterCall(page, limit);
    //         else this.onDataCall();
    //     });
    //     console.log(page, limit);
    // };
    onDataCall = () => {
        this.setState({ isLoader: true })
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ printReportArr: [], paginationCall: "allData" }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/refund?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&classbatchName=${this.state.filterKey}&page=${this.state.page}&limit=${this.state.limit}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token"),
                }
            })
                .then(resp => {
                    console.log(resp);
                    let regId = this.context.reportLabel || "Reg ID";
                    let batch = this.context.classLabel || "CLASS/BATCH";
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    var refundData = []
                    resp.data.data.map(item => {
                        var description = ""
                        item.description.map(desc => {
                            description = description + ' ' + desc.feeType
                        })
                        var demandNoteId = item.demandNoteId['0']['primaryTransaction']
                        // item.demandNoteId.map(dnId => {
                        //     demandNoteId = demandNoteId + ' ' + dnId.primaryTransaction
                        // })
                        refundData.push({
                            "refundId": item.refundId,
                            // "demandNoteId": demandNoteId,
                            [regId]: item.regId,
                            "studentName": item.studentName,
                            "academicYear": item.academicYear,
                            [batch]: item["class/Batch"],
                            "refund amount": item.refunded,
                            "mode": item.mode === undefined || item.mode === null ? "-" : item.mode,
                            "refundedOn": new Date(item.refundedOn).toLocaleDateString(),
                            "txnId": item.txnId === undefined || item.txnId === null ? "-" : item.txnId,
                            "status": item.status
                        })
                    })
                    this.setState({ printReportArr: refundData.reverse(), filterData: refundData.reverse(), totalPages: resp.data.totalPages, totalRecord: resp.data.totalRecord, noData })
                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ printReportArr: [], isLoader: false, noData: true })
                })
        })
    }

    //check the dropdown batch name
    filterDataNew = () => {
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        axios.get(`${this.state.env['zqBaseUri']}/edu/reports/refund?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}`, {
            headers: {
                'Authorization': localStorage.getItem("auth_token"),
            }
        }).then(resp => {
            let dropDownData = [{
                "value": "All",
                "label": "All Refunds"
            }]
            let allData = resp.data.data
            let programPlanData = Array.from(new Set(allData.map(a => a['class/Batch']))) //getting all Department names in array
            programPlanData.map((item, index) => {
                dropDownData.push({
                    "value": item == undefined ? "-" : item,
                    "label": item == undefined ? "-" : item
                })
            })
            this.setState({ appDatas: dropDownData })
            this.setState({ isLoader: false })
        })
            .catch(err => {
                console.log(err);
                this.setState({ isLoader: false, appDatas: [], noData: true })
            })

    }
    onDateFormat = (d) => {
        let dateField = new Date(String(d));
        let month = dateField.getMonth() + 1;
        month = String(month).length == 1 ? `0${String(month)}` : String(month);
        let date = dateField.getDate();
        date = String(date).length == 1 ? `0${String(date)}` : String(date);
        let year = dateField.getFullYear();
        return `${date}/${month}/${year}`;
    }
    handleBack = () => {
        this.setState({ particularItem: undefined })
        if (this.state.showTable) {
            this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`);
        }
        else {
            this.setState({ showTable: true });
        }
    }
    searchHandle = (searchValue) => {
        console.log('searchValue', searchValue)
        this.setState({ isLoader: true })
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        if (searchValue.length > 0) {
            return axios.get(`${this.state.env['zqBaseUri']}/edu/reports/refund?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&page=${this.state.page}&limit=${this.state.limit}&classbatchName=${this.state.filterKey}&searchKey=${searchValue}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    this.setState({
                        printReportArr: resp.data.data.reverse(),
                        totalPages: resp.data.totalPages,
                        totalRecord: resp.data.totalRecord,
                        noData
                    })

                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true, dataInfo: "No Data" })
                })
        }
        else {
            this.setState({ printReportArr: this.state.filterData, isLoader: false })
        }

    }
    onSelectPrgmPlan = (value, item, label) => {
        let selectedValue = value
        this.setState({ filterKey: selectedValue })
        if (value === "All") {
            this.onDataCall()
        } else {
            this.onFilterCall(1, 10)
        }
    }
    onFilterCall = (page, limit) => {
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ isLoader: true, paginationCall: "filterData", page: page, limit: limit })
        this.setState({ printReportArr: [] }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/refund?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&classbatchName=${this.state.filterKey}&page=${this.state.page}&limit=${this.state.limit}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token"),
                }
            })
                // .then(resp => {
                //     console.log(resp);
                //     let noData;
                //     if (resp.data.data.length === 0) {
                //         noData = true;
                //     } else {
                //         noData = false;
                //     }
                //     this.setState({ printReportArr: resp.data.data, filterData: resp.data.data, totalPages: resp.data.totalPages, totalRecord: resp.data.totalRecord, noData })
                //     this.setState({ isLoader: false })
                // })

                .then(resp => {
                    console.log(resp);
                    let batch = this.context.classLabel || "CLASS/BATCH";
                    let regId = this.context.reportLabel || "Reg ID";
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    var refundData = []
                    resp.data.data.map(item => {
                        var description = ""
                        item.description.map(desc => {
                            description = description + ' ' + desc.feeType
                        })
                        refundData.push({
                            "refundId": item.refundId,
                            // "demandNoteId": demandNoteId,
                            [regId]: item.regId,
                            "studentName": item.studentName,
                            "academicYear": item.academicYear,
                            [batch]: item["class/Batch"],
                            "refund amount": item.refunded,
                            "mode": item.mode === undefined || item.mode === null ? "-" : item.mode,
                            "refundedOn": new Date(item.refundedOn).toLocaleDateString(),
                            "txnId": item.txnId === undefined || item.txnId === null ? "-" : item.txnId,
                            "status": item.status
                        })
                    })
                    this.setState({ printReportArr: refundData.reverse(), filterData: refundData.reverse(), totalPages: resp.data.totalPages, totalRecord: resp.data.totalRecord, noData })
                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true })
                })

        })
    }
    printScreen = () => {
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ isLoader: false }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/refund?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&classbatchName=${this.state.filterKey}&pagination=false`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    console.log(resp);
                    this.setState({
                        printable: true,
                        printReportArr: resp.data.data
                    }, () => {
                        this.setState({ printable: false })
                        window.print();
                        this.onDataCall();
                    })
                })
        });
    }
    onSelectClean = () => {
        this.setState({ filterKey: 'All' })
    }
    onDownloadEvent = () => {
        this.setState({ isLoader: true })
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        axios.get(`${this.state.env['zqBaseUri']}/edu/reports/refund?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&classbatchName=${this.state.filterKey}`, {
            headers: {
                'Authorization': localStorage.getItem("auth_token")
            }
        })
            .then(resp => {
                console.log(resp);
                var createXlxsData = []
                let regId = this.context.reportLabel || "REG ID"
                let batch = this.context.classLabel || "CLASS/BATCH"
                resp.data.data.map(item => {
                    var dateReg = new RegExp(/^\d{2}[./-]\d{2}[./-]\d{4}$/)
                    let refundedOn = item['refundedOn']
                    if (item['refundedOn']) {
                        if (dateReg.test(item['refundedOn'])) {
                            if (item['refundedOn'].includes("/")) { refundedOn = String(item['refundedOn']).split('/')['1'] + '/' + String(item['refundedOn']).split('/')['0'] + '/' + String(item['refundedOn']).split('/')['2'] }
                            else if (item['refundedOn'].includes("-")) { refundedOn = String(item['refundedOn']).split('-')['1'] + '/' + String(item['refundedOn']).split('-')['0'] + '/' + String(item['refundedOn']).split('-')['2'] }
                        } else {
                            refundedOn = item['refundedOn']
                        }
                    }
                    var description = ""
                    item.description.map(desc => {
                        description = description + ' ' + desc.feeType
                    })
                    createXlxsData.push({
                        "REFUND ID": item.refundId,
                        // "DEMAND NOTE ID": demandNoteId,
                        [regId]: item.regId,
                        "STUDENT NAME": item.studentName,
                        "ACADEMIC YEAR": item.academicYear,
                        [batch]: item["class/Batch"],
                        "REFUNDED": this.formatCurrency(item.refunded),
                        // "txnId": item.txnId,
                        "MODE": item.mode === undefined || item.mode === null ? "-" : item.mode,
                        "REFUNDED ON": moment(refundedOn).format(this.context.dateFormat),
                        "txnId": item.txnId === undefined || item.txnId === null ? "-" : item.txnId,
                        "STATUS": item.status
                    })

                })
                var ws = xlsx.utils.json_to_sheet(createXlxsData);
                var wb = xlsx.utils.book_new();
                xlsx.utils.book_append_sheet(wb, ws, "Refund Reports");
                xlsx.writeFile(wb, "refund_reports.xlsx");
                this.setState({ isLoader: false })
            })
            .catch(err => {
                console.log(err, 'err0rrrrr')
                this.setState({ isLoader: false })
            })


    }

    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    onChangeDateDuration = (e, f) => {
        this.setState({
            isLoader: true,
            page: 1,
            limit: 5,
            totalRecord: 0,
            totalPages: 0,
            printReportArr: [],
            typeOfPagination: 'date',
            fromDateData: e,
            toDateData: f
        }, () => {
            console.log(e, f);
            this.filterWithDate(e, f);
        })

    }
    tableDateFormat = (ev) => {
        if (ev === undefined || ev === '') { }
        else {
            let getDate = `${String(ev.getDate()).length == 1 ? `0${ev.getDate()}` : ev.getDate()}`;
            let getMonth = `${String(ev.getMonth() + 1).length == 1 ? `0${ev.getMonth() + 1}` : ev.getMonth() + 1}`;
            let getYear = `${ev.getFullYear()}`
            let today = `${getYear}-${getMonth}-${getDate}`
            return today
        }
    }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            if (this.state.typeOfPagination === 'date') {
                this.onPaginationDateChange(page, limit)
            }
            else {
                if (this.state.paginationCall === "allData") this.onDataCall();
                else if (this.state.paginationCall === "filterData") this.onFilterCall(page, limit);
                else this.onDataCall();
            }
        });
        console.log(page, limit);
    };
    onPaginationDateChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.filterWithDate(this.state.fromDateData, this.state.toDateData)
        });
    }
    filterWithDate = (f, t) => {
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ isLoader: true }, () => {
            axios({
                method: 'get',
                url: `${this.state.env['zqBaseUri']}/edu/getPeriodicRefunds?campusId=${this.state.campusId}&userId=${userId}&fromDate=${this.tableDateFormat(f)}&toDate=${this.tableDateFormat(t)}&page=${this.state.page}&perPage=${this.state.limit}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            })
                .then(res => {
                    console.log(res);
                    let batch = this.context.classLabel || "CLASS/BATCH";
                    let regId = this.context.reportLabel || "Reg ID";
                    let noData;
                    if (res.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    var refundData = []
                    res.data.data.map(item => {
                        var description = ""
                        item.description.map(desc => {
                            description = description + ' ' + desc.feeType
                        })
                        refundData.push({
                            "refundId": item.refundId,
                            // "demandNoteId": demandNoteId,
                            [regId]: item.regId,
                            "studentName": item.studentName,
                            "academicYear": item.academicYear,
                            [batch]: item["class/Batch"],
                            "refund amount": item.refunded,
                            "mode": item.mode === undefined || item.mode === null ? "-" : item.mode,
                            "refundedOn": new Date(item.refundedOn).toLocaleDateString(),
                            "txnId": item.txnId === undefined || item.txnId === null ? "-" : item.txnId,
                            "status": item.status
                        })
                    })
                    this.setState({
                        printReportArr: refundData,
                        noData,
                        totalPages: res.data.totalPages,
                        page: res.data.page,
                        downloadTotalRecord: res.data.totalRecord,
                        isLoader: false
                    })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true })
                })
        })
    }
    render() {
        console.log("printReportArr", this.state.printReportArr)
        return (
            <React.Fragment >
                {this.state.isLoader &&
                    <Loader />
                }
                <div className="reports-student-fees list-of-students-mainDiv">
                    {/* <div className="student-report-header-title">
                        <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.handleBack} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                        <p className="top-header-title">| Refund</p>
                    </div> */}

                    <div className="trial-balance-header-title">
                        <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                        <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Refund</p>
                    </div>

                    <div className="reports-body-section print-hd">
                        <React.Fragment>
                            <ContainerNavbar
                                containerNav={this.state.containerNav}
                                onDownload={() => this.onDownloadEvent()}
                                searchValue={(searchValue) => this.searchHandle(searchValue)}
                                Selectdata={this.state.appDatas}
                                SelectCampusdata = {this.state.campusData}
                                SelectSectionData={this.state.sectionData}
                                onSelectDefaultValue={this.state.filterKey}
                                onSelectCampusDefaultValue={this.state.filterKey}
                                onSelectClean={this.onSelectClean}
                                onSelectChange={this.onSelectPrgmPlan}
                                printScreen={this.printScreen}
                                dateDurationVal={(e, f) => { this.onChangeDateDuration(e, f) }}
                                getAllDataFun={this.onDataCall}
                                
                            />
                            <div className="print-time">{this.state.printTime}</div>
                        </React.Fragment>
                        <div className="reports-data-print-table">
                            <div className="">
                                <React.Fragment>
                                    <table className="transaction-table-review reports-tableRow-header">
                                        <thead>
                                            <tr>
                                                {this.state.tableHeader.map((data, i) => {
                                                    return <th className={"demand-note-refund " + String(data).replace(' ', '')} key={i + 1}>{data}</th>
                                                })}
                                            </tr>
                                        </thead>
                                        {this.state.printReportArr.length > 0 ?
                                            <tbody>
                                                {this.state.printReportArr.map((item, i) => {
                                                    var dateReg = new RegExp(/^\d{2}[./-]\d{2}[./-]\d{4}$/)
                                                    let refundedOn = item['refundedOn']
                                                    if (item['refundedOn']) {
                                                        if (dateReg.test(item['refundedOn'])) {
                                                            if (item['refundedOn'].includes("/")) { refundedOn = String(item['refundedOn']).split('/')['1'] + '/' + String(item['refundedOn']).split('/')['0'] + '/' + String(item['refundedOn']).split('/')['2'] }
                                                            else if (item['refundedOn'].includes("-")) { refundedOn = String(item['refundedOn']).split('-')['1'] + '/' + String(item['refundedOn']).split('-')['0'] + '/' + String(item['refundedOn']).split('-')['2'] }
                                                        } else {
                                                            refundedOn = item['refundedOn']
                                                        }
                                                    }
                                                    return (<tr key={i + 1} id={i + 1} className="reports-tableRow-body" style={{ cursor: "pointer", padding: '0px' }}>
                                                        <td className="transaction-vch-num" >{item['refundId']}</td>
                                                        {/* <td className="transaction-vch-num" >{item['demandNoteId']}</td> */}
                                                        <td className="transaction-vch-num" >{item[`${this.context.reportLabel ? this.context.reportLabel : 'REG ID'}`]}</td>
                                                        <td className="transaction-vch-num" >{item['studentName']}</td>
                                                        <td className="transaction-vch-type">{item['academicYear']}</td>
                                                        <td className="transaction-vch-type">{item[`${this.context.classLabel ? this.context.classLabel : 'CLASS/BATCH'}`]}</td>
                                                        {/* <td className="transaction-vch-type">{item['description']}</td> */}
                                                        <td className="transaction-vch-type">{this.formatCurrency(item['refund amount'])}</td>
                                                        <td className="transaction-vch-type">{item['mode']}</td>
                                                        <td className="transaction-vch-type">{item['refundedOn'] && item['refundedOn'] !== "NA" && item['refundedOn'] !== "-" ? <DateFormatter date={refundedOn} format={this.context.dateFormat} /> : "-"}</td>
                                                        <td className="transaction-vch-type">{item['txnId']}</td>
                                                        {/* <td className="transaction-vch-type">{item['status']}</td> */}
                                                        <td className="transaction-debit">
                                                            <p
                                                                className={String(item['status']).toLowerCase().includes("pending") ? "Status-Pending" : String(item['status']).toLowerCase().includes("paid") ? "Status-Active" : String(item['status']).toLowerCase().includes("partial") ? "Status-Partial" : "Status-Partial"}
                                                                style={{
                                                                    textAlign: "center",
                                                                    color: item['status'] == "Pending" ? "#FF5630" : item['status'] == "Paid" ? "#00875A" : "#000000",
                                                                    fontWeight: String(item['status']).includes("Total") ? "" : '', borderBottom: "none", padding: '0px'
                                                                }}>
                                                                <span>{item['status']}</span>
                                                            </p>
                                                        </td>
                                                    </tr>)
                                                })}

                                            </tbody>
                                            : <tbody>
                                                <tr>
                                                    {this.state.noData ? <td className="noprog-txt" colSpan={this.state.tableHeader.length}>No data...</td> :
                                                        <td className="noprog-txt" colSpan={this.state.tableHeader.length}>Fetching the data...</td>
                                                    }

                                                </tr>
                                            </tbody>
                                        }
                                    </table>
                                    <div>
                                        {this.state.printReportArr.length == 0 ? null :
                                            <PaginationUI
                                                total={this.state.totalRecord}
                                                onPaginationApi={this.onPaginationChange}
                                                totalPages={this.state.totalPages}
                                                limit={this.state.limit}
                                                currentPage={this.state.page}

                                            />
                                        }
                                    </div>
                                </React.Fragment>

                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment >
        )
    }

}
export default withRouter(Refund);