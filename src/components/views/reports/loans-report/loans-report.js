import React, { Component } from 'react';
import '../../../../scss/fees-type.scss';
import '../../../../scss/setup.scss';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../utils/Table/table-component";
import PaginationUI from "../../../../utils/pagination/pagination";
import Loader from "../../../../utils/loader/loaders";
import LoanDataJson from './loans-report.json';
import ZenForm from "../../../../components/input/zqform";
import xlsx from 'xlsx';
class Loans extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            containerNav: {
                isBack: false,
                name: "List of Loans",
                isName: true,
                isSearch: true,
                isSort: false,
                isPrint: true,
                isDownload: true,
                isShare: false,
                isNew: false,
                newName: "New",
                isSubmit: false,
                isSelectAppReport: true,
                selectPickerOption: true,
                selectCampusOption: true,
                selectSectionOption:true,
                selectFranchiseOption: false
            },
            tablePrintDetails: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 1,
            noProg: "Fetching Data ...",
            PreStudentName: "",
            previewList: false,
            previewListForm: LoanDataJson.formJson,
            appDatas: [
                {
                    label: "All Program Plan",
                    value: "All"
                }
            ],
            campusData: [
                {
                    label: "All Campus",
                    value: "All"
                }
            ],
            sectionData: [
                {
                    label: "All Section",
                    value: "All"
                }
            ],
            filterKey: "All",
        }
    }
    componentDidMount() { }
    onAddNewLoans = () => { }
    onPreviewLoan = (e) => { }
    onPaginationChange = () => { }
    onPreviewLoanList = () => { }
    moveBackTable = () => {
        this.setState({ previewList: false })
    }
    actionClickFun = (e) => {
        console.log(e);
    }
    handleTabChange = () => { }
    allSelect = () => { }
    onInputChanges = () => { }
    searchHandle = () => { }
    onSelectPrgmPlan = (value, item, label) => {
        let selectedValue = value
        this.setState({ filterKey: selectedValue })
        //this.onDataCall()
    }
    onSelectClean = () => {
        this.setState({ filterKey: 'All' })
    }
    onDownloadEvent = () => {
        this.setState({ isLoader: true })
        var createXlxsData = []
        var ws = xlsx.utils.json_to_sheet(createXlxsData);
        var wb = xlsx.utils.book_new();
        xlsx.utils.book_append_sheet(wb, ws, "Loan Reports");
        xlsx.writeFile(wb, "loan_reports.xlsx");
        //xlsx.writeFile(wb, "demand_note_reports.csv");
        this.setState({ isLoader: false })
    }
    render() {
        return (
            <div className="list-of-feeType-mainDiv">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.previewList == false ?
                    <React.Fragment>
                        <React.Fragment>
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Loans</p>
                            </div>
                        </React.Fragment>
                        <div className="masters-body-div">
                            <React.Fragment>
                                <ContainerNavbar
                                    containerNav={this.state.containerNav}
                                    onDownload={() => this.onDownloadEvent()}
                                    searchValue={(searchValue) => this.searchHandle(searchValue)}
                                    Selectdata={this.state.appDatas}
                                    SelectCampusdata = {this.state.campusData}
                                    SelectSectionData={this.state.sectionData}
                                    onSelectDefaultValue={this.state.filterKey}
                                    onSelectCampusDefaultValue={this.state.filterKey}
                                    onSelectClean={this.onSelectClean}
                                    onSelectChange={this.onSelectPrgmPlan}
                                    dateDurationVal={(e, f) => { this.onChangeDateDuration(e, f) }}
                                    printScreen={this.printScreen} />
                            </React.Fragment>
                            <div className="table-wrapper" style={{ marginTop: "10px" }}>
                                {LoanDataJson.tableData.length == 0 ? <p className="noprog-txt">{this.state.noProg}</p> :
                                    <React.Fragment>
                                        <div className="remove-last-child-table" >
                                            {/* <ZqTable
                                                data={LoanDataJson.tableData}
                                                allSelect={this.allSelect}
                                                rowClick={(item) => { this.onPreviewLoan(item) }}
                                                onRowCheckBox={(item) => { this.allSelect(item) }}
                                                handleActionClick={(item) => { this.actionClickFun(item) }}
                                            /> */}
                                            <p className="noprog-txt">No Data</p>
                                        </div>
                                    </React.Fragment>
                                }
                                {/* {LoanDataJson.tableData.length !== 0 ? <PaginationUI onPaginationApi={this.onPaginationChange} totalPages={this.state.totalPages} limit={this.state.limit} total={this.state.totalRecord} /> : null} */}
                            </div>
                        </div>
                    </React.Fragment> :
                    <React.Fragment>
                        <div className="tab-form-wrapper tab-table">
                            <div className="preview-btns">
                                <div className="preview-header-wrap">
                                    < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                    <h6 className="preview-Id-content" onClick={this.moveBackTable}>| Loans | {this.state.PreStudentName}</h6>
                                </div>
                            </div>
                            <div className="goods-header">
                                <p className="info-input-label">BASIC DETAILS</p>
                            </div>
                            <div className="goods-wrapper goods-parent-wrap">
                                <ZenForm
                                    formData={LoanDataJson.formJson}
                                    className="goods-wrap"
                                    onInputChanges={(e, a) => this.onInputChanges(e, a)}
                                    onFormBtnEvent={(item) => {
                                        this.formBtnHandle(item);
                                    }}
                                    clear={true}
                                    onSubmit={(e) => this.onSubmit("preview", e.target.elements)}
                                />
                            </div>
                        </div>
                    </React.Fragment>
                }
            </div>
        )
    }
}
export default Loans;