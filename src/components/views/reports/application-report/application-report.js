import React, { Component } from "react";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import '../../../../scss/student-reports.scss';
import '../../../../scss/student.scss';
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import axios from 'axios';
import xlsx from 'xlsx';
import PaginationUI from "../../../../utils/pagination/pagination";
import '../../../../scss/common-table.scss';
import Loader from '../../../../utils/loader/loaders';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import DateFormatter from '../../../date-formatter/date-formatter';
import DateFormatContext from '../../../../gigaLayout/context';
import Button from '@material-ui/core/Button';
import Checkbox from "@material-ui/core/Checkbox";
import TextField from '@material-ui/core/TextField';
import moment from 'moment';
import { SelectPicker } from "rsuite";

class ApplicationReport extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            env: JSON.parse(localStorage.getItem('env')),
            orgId: localStorage.getItem('orgId'),
            authToken: localStorage.getItem("auth_token"),
            campusId :  localStorage.getItem("campusId"),
            userId : localStorage.getItem("userId"),
            containerNav: {
                isBack: false,
                name: "Application",
                isName: true,
                isSearch: true,
                isSort: false,
                isPrint: true,
                isDownload: true,
                isShare: false,
                isNew: false,
                newName: "New",
                isSubmit: false,
                isSelectAppReport: true,
                selectPickerOption: true,
                selectCampusOption: true,
                selectSectionOption:true,
                selectFranchiseOption: false
            },
            tableHeader: ["SelectIcon", "APPLICATION ID", "NAME", "EMAIL", "MOBILE NO.", "ACADEMIC PROGRAM", "SOURCE", "CURRENCY", "AMOUNT", "TRANSACTION ID", "TRANSACTION DATE", "STATUS"],
            printReportArr: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 0,
            noData: false,
            isLoader: false,
            filterData: [],
            dataInfo: "Fetching Data...",
            finalINRdata: "",
            finalUSDdata: "",
            appDatas: [
                {
                    label: "Mechanical",
                    value: "Mechanical"
                },
                {
                    label: "Computer Science",
                    value: "Computer Scinece"
                },
                {
                    label: "All Program Plan",
                    value: "All"
                }
            ],
            campusData : [
                {
                    label: "All Campus",
                    value: "All"
                }
            ],
            sectionData : [
                {
                    label: "All Section",
                    value: "All"
                }
            ],
            filterKey: "All",
            finalDomesticData: '',
            finalintApp: '',
            allData: [],
            totalAedData: 0
        }
        this.TableRef = React.createRef(this.TableRef);
    }
    static contextType = DateFormatContext;
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.onDataCall();
        });
        console.log(page, limit);
    };
    componentDidMount() {
        this.onDataCall();
        this.filterDataNew()
    }
    filterDataNew = () => {
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        axios.get(`${this.state.env['zqBaseUri']}/edu/reports/application?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&filterKey=${this.state.filterKey}`, {
            headers: {
                'Authorization': localStorage.getItem("auth_token")
            }
        })
            .then(resp => {

                let dropDownData = [{
                    "value": "All",
                    "label": "All Program Plan"
                }]

                let allData = resp.data.data
                let programPlanData = Array.from(new Set(allData.map(a => a.programPlan))) //getting all Department names in array
                programPlanData.map((item, index) => {
                    dropDownData.push({
                        "value": item == undefined ? "-" : item,
                        "label": item == undefined ? "-" : item
                    })
                })
                this.setState({ appDatas: dropDownData })

                this.setState({ isLoader: false })
            })
            .catch(err => {
                console.log(err);
                this.setState({ isLoader: false, appDatas: [], noData: true })
            })

    }
    onDataCall = () => {
        this.setState({ isLoader: true })
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ printReportArr: [] }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/application?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&filterKey=${this.state.filterKey}&page=${this.state.page}&limit=${this.state.limit}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    console.log(resp);
                    let respData = resp.data
                    // let INRamt = []; let USDamt = [];
                    // let INRApp = 0; let USDApp = 0;
                    // resp.data.data.map((dataTwo, idx) => {
                    //     if (dataTwo.currencyCode == "USD") {
                    //        if(dataTwo.status == 'paid'){
                    //         USDamt.push(dataTwo.amount)
                    //         USDApp = USDApp + 1
                    //        }else{
                    //         USDApp = USDApp + 1 
                    //        }

                    //     }
                    //     if (dataTwo.currencyCode == "INR") {
                    //         if(dataTwo.status == 'paid'){
                    //         INRamt.push(dataTwo.amount)
                    //         INRApp = INRApp + 1
                    //         }else{
                    //             INRApp = INRApp + 1   
                    //         }

                    //     }
                    // })
                    // let finalINR = INRamt.reduce((a, b) => a + b, 0);
                    // let finalUSD = USDamt.reduce((a, b) => a + b, 0);
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    let dataApp = []
                    resp.data.data.map(item => {
                        dataApp.push({
                            ...item,
                            "issueAt": item.createdAt !== "" ? this.context.dateFormat !== "" ? moment(item.createdAt).format(this.context.dateFormat) : moment(item.createdAt).format('DD/MM/YYYY') : "-"
                        })
                    })



                    this.setState({
                        printReportArr: dataApp, filterData: dataApp, totalPages: resp.data.totalPages, totalRecord: resp.data.totalRecord, noData, dataInfo: noData ? 'No Data' : '',
                        finalINRdata: Number(respData.totalINR).toFixed(2), finalUSDdata: Number(respData.totalUSD).toFixed(2)
                        , finalDomesticData: respData.domesticApp, finalintApp: respData.internationalApp, totalAedData: respData.totalAED === undefined ? 0 : respData.totalAED
                    })

                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true })
                    // var respErr = { "status": "success", "message": "feePayment reports", "data": [{ "studentName": "Adiba Nisar", "regId": "STUD_001", "academicYear": "2020-21", "classBatch": "BE_ENG_CSC_20-21", "DemandId": "DN_2020-21_001", "description": [{ "name": "Tuition Fee", "due": 145000, "paid": 145000, "paidDate": "12/11/2020", "balance": 0, "status": "Paid", "txnId": "5fae2d05daeb301acc833ec6" }, { "name": "Total", "due": 145000, "paid": 145000, "paidDate": "-", "balance": 0, "status": "Paid", "txnId": "-" }] }, { "studentName": "Abdul Nasar", "regId": "STUD_002", "academicYear": "2020-21", "classBatch": "BE_ENG_MEC_20-21", "DemandId": "DN_2020-21_002", "description": [{ "name": "Tuition Fee", "due": 125000, "paid": 125000, "paidDate": "11/11/2020", "balance": 0, "status": "Paid", "txnId": "5fae2d090888b11acc10be14" }, { "name": "Total", "due": 125000, "paid": 125000, "paidDate": "-", "balance": 0, "status": "Paid", "txnId": "-" }] }, { "studentName": "Pradeep Kumar", "regId": "STUD_003", "academicYear": "2020-21", "classBatch": "BE_ENG_CVL_20-21", "DemandId": "DN_2020-21_003", "description": [{ "name": "Uniform Plan", "due": 2000, "paid": 800, "paidDate": "10/11/2020", "balance": 2200, "status": "Partial", "txnId": "5fae2d0d7074061accc1a355" }, { "name": "Tuition Fee", "due": 2000, "paid": 1200, "paidDate": "10/11/2020", "balance": 1800, "status": "Partial", "txnId": "5fae2d0d7074061accc1a355" }, { "name": "Total", "due": 4000, "paid": 2000, "paidDate": "-", "balance": 4000, "status": "Partial", "txnId": "-" }] }, { "studentName": "Mohammed Yaseen R", "regId": "STUD_003", "academicYear": "2020-21", "classBatch": "BE_ENG_CVL_20-21", "DemandId": "DN_2020-21_004", "description": [{ "name": "Transport Fee", "due": 280000, "paid": 11275.17, "paidDate": "25/10/2020", "balance": 128724.83, "status": "Partial", "txnId": "5fae2d1aa3335d1acc4e9a77" }, { "name": "Tuition Fee", "due": 280000, "paid": 131543.62, "paidDate": "25/10/2020", "balance": 8456.38, "status": "Partial", "txnId": "5fae2d1aa3335d1acc4e9a77" }, { "name": "Uniform Plan", "due": 280000, "paid": 5637.58, "paidDate": "25/10/2020", "balance": 134362.42, "status": "Partial", "txnId": "5fae2d1aa3335d1acc4e9a77" }, { "name": "Tuition Fee", "due": 280000, "paid": 131543.62, "paidDate": "25/10/2020", "balance": 8456.38, "status": "Partial", "txnId": "5fae2d1aa3335d1acc4e9a77" }, { "name": "Total", "due": 1120000, "paid": 279999.99, "paidDate": "-", "balance": 280000.01, "status": "Partial", "txnId": "-" }] }], "currentPage": null, "perPage": 10, "nextPage": null, "totalRecord": 4, "totalPages": 1 }

                    // this.setState({ printReportArr: respErr.data, totalPages: respErr.totalPages, totalRecord: respErr.totalRecord })
                })

        })
    }

    handleBackFun = () => {
        this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`);
    }
    onDownloadEvent = () => {
        this.setState({ isLoader: true })
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        axios.get(`${this.state.env['zqBaseUri']}/edu/reports/application?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&filterKey=${this.state.filterKey}`, {
            headers: {
                'Authorization': localStorage.getItem("auth_token")
            }
        })
            .then(resp => {
                console.log("student fee", resp);
                var createXlxsData = []
                resp.data.data.map(item => {
                    createXlxsData.push({
                        "APPLICATION ID": item.applicationId,
                        "NAME": item.name,
                        "EMAIL": item.email,
                        "MOBILE NO.": item.mobile,
                        "ACADEMIC PROGRAM": item.programPlan,
                        "SOURCE": item.gatewayType,
                        "CURRENCY": item.currencyCode,
                        "AMOUNT(INR)": Number(item.amount).toFixed(2),
                        // "SUBMITTED ON": item.createdAt !== undefined ? moment(item.createdAt).format('DD/MM/YYYY HH:mm:ss') : "-",
                        "TRANSACTION ID": item.transactionId == undefined ? '-' : item['transactionId'],
                        "TRANSACTION DATE": item.createdAt !== undefined ? moment(item.createdAt).format(this.context.dateFormat) : "-",
                        "STATUS": item.status
                    })
                    // console.log('**DATA**', item)

                })
                var ws = xlsx.utils.json_to_sheet(createXlxsData);
                var wb = xlsx.utils.book_new();
                xlsx.utils.book_append_sheet(wb, ws, "Application Report");
                xlsx.writeFile(wb, "application_report.xlsx");
                // xlsx.writeFile(wb, "demand_note_reports.csv");
                this.setState({ isLoader: false })
            })
            .catch(err => {
                console.log(err, 'errorrrrr')
                this.setState({ isLoader: false })
            })


    }
    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    downloadTallyXML = () => {
        const anchor = document.createElement("a");
        anchor.href = "https://d8wau2wreg.execute-api.us-east-1.amazonaws.com/dev/migrate/receipt?orgid=5fa8daece3eb1f18d4250e55";
        anchor.id = "xml-download"
        document.body.appendChild(anchor);
        anchor.click();
        document.body.removeChild(anchor)
    }
    searchTable = () => {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("outlined-disabled");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    searchHandle = (searchValue) => {
        console.log('searchValue', searchValue)
        // let takeFilterIndex = {}
        // this.setState({ printReportArr: [], dataInfo: "Fetching Data..." },
        //     () => {
        if (searchValue.length > 0) {
            // this.state.filterData.map((item, index) => {
            //     Object.keys(item).map(key => {
            //         if (String(item[key]).toLowerCase().includes(String(searchValue).toLowerCase())) {
            //             takeFilterIndex[index] = index
            //         }
            //     })
            // })
            // let filterItems = []
            // Object.keys(takeFilterIndex).map(itemIndex => {
            //     filterItems.push(this.state.filterData[itemIndex])
            // })
            // this.setState({ printReportArr: filterItems, dataInfo: filterItems.length == 0 ? 'No Data' : 'Fetching Data...' })
            let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
            return axios.get(`${this.state.env['zqBaseUri']}/edu/reports/searchapplication?campusId=${this.state.campusId}&userId=${userId}&page=${this.state.page}&limit=${this.state.limit}&orgId=${this.state.orgId}&searchKey=${searchValue}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    console.log(resp);
                    let INRamt = []; let USDamt = [];
                    let INRApp = 0; let USDApp = 0;
                    resp.data.data.map((dataTwo, idx) => {
                        if (dataTwo.currencyCode == "USD") {
                            if (dataTwo.status == 'paid') {
                                USDamt.push(dataTwo.amount)
                                USDApp = USDApp + 1
                            } else {
                                USDApp = USDApp + 1
                            }
                        }
                        if (dataTwo.currencyCode == "INR") {
                            if (dataTwo.status == 'paid') {
                                INRamt.push(dataTwo.amount)
                                INRApp = INRApp + 1
                            } else {
                                INRApp = INRApp + 1
                            }
                        }
                    })
                    let finalINR = INRamt.reduce((a, b) => a + b, 0);
                    let finalUSD = USDamt.reduce((a, b) => a + b, 0);
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    let dataApp = []
                    resp.data.data.map(item => {
                        dataApp.push({
                            ...item,
                            "issueAt": item.createdAt !== "" ? this.context.dateFormat !== "" ? moment(item.createdAt).format(this.context.dateFormat) : moment(item.createdAt).format('DD/MM/YYYY') : "-"
                        })
                    })
                    this.setState({
                        printReportArr: dataApp, filterData: dataApp, totalPages: resp.data.totalPages, totalRecord: resp.data.totalRecord, noData, dataInfo: noData ? 'No Data' : '', finalINRdata: Number(finalINR).toFixed(2), finalUSDdata: Number(finalUSD).toFixed(2)
                        , finalDomesticData: INRApp, finalintApp: USDApp
                    })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true, dataInfo: "No Data" })
                    // var respErr = { "status": "success", "message": "feePayment reports", "data": [{ "studentName": "Adiba Nisar", "regId": "STUD_001", "academicYear": "2020-21", "classBatch": "BE_ENG_CSC_20-21", "DemandId": "DN_2020-21_001", "description": [{ "name": "Tuition Fee", "due": 145000, "paid": 145000, "paidDate": "12/11/2020", "balance": 0, "status": "Paid", "txnId": "5fae2d05daeb301acc833ec6" }, { "name": "Total", "due": 145000, "paid": 145000, "paidDate": "-", "balance": 0, "status": "Paid", "txnId": "-" }] }, { "studentName": "Abdul Nasar", "regId": "STUD_002", "academicYear": "2020-21", "classBatch": "BE_ENG_MEC_20-21", "DemandId": "DN_2020-21_002", "description": [{ "name": "Tuition Fee", "due": 125000, "paid": 125000, "paidDate": "11/11/2020", "balance": 0, "status": "Paid", "txnId": "5fae2d090888b11acc10be14" }, { "name": "Total", "due": 125000, "paid": 125000, "paidDate": "-", "balance": 0, "status": "Paid", "txnId": "-" }] }, { "studentName": "Pradeep Kumar", "regId": "STUD_003", "academicYear": "2020-21", "classBatch": "BE_ENG_CVL_20-21", "DemandId": "DN_2020-21_003", "description": [{ "name": "Uniform Plan", "due": 2000, "paid": 800, "paidDate": "10/11/2020", "balance": 2200, "status": "Partial", "txnId": "5fae2d0d7074061accc1a355" }, { "name": "Tuition Fee", "due": 2000, "paid": 1200, "paidDate": "10/11/2020", "balance": 1800, "status": "Partial", "txnId": "5fae2d0d7074061accc1a355" }, { "name": "Total", "due": 4000, "paid": 2000, "paidDate": "-", "balance": 4000, "status": "Partial", "txnId": "-" }] }, { "studentName": "Mohammed Yaseen R", "regId": "STUD_003", "academicYear": "2020-21", "classBatch": "BE_ENG_CVL_20-21", "DemandId": "DN_2020-21_004", "description": [{ "name": "Transport Fee", "due": 280000, "paid": 11275.17, "paidDate": "25/10/2020", "balance": 128724.83, "status": "Partial", "txnId": "5fae2d1aa3335d1acc4e9a77" }, { "name": "Tuition Fee", "due": 280000, "paid": 131543.62, "paidDate": "25/10/2020", "balance": 8456.38, "status": "Partial", "txnId": "5fae2d1aa3335d1acc4e9a77" }, { "name": "Uniform Plan", "due": 280000, "paid": 5637.58, "paidDate": "25/10/2020", "balance": 134362.42, "status": "Partial", "txnId": "5fae2d1aa3335d1acc4e9a77" }, { "name": "Tuition Fee", "due": 280000, "paid": 131543.62, "paidDate": "25/10/2020", "balance": 8456.38, "status": "Partial", "txnId": "5fae2d1aa3335d1acc4e9a77" }, { "name": "Total", "due": 1120000, "paid": 279999.99, "paidDate": "-", "balance": 280000.01, "status": "Partial", "txnId": "-" }] }], "currentPage": null, "perPage": 10, "nextPage": null, "totalRecord": 4, "totalPages": 1 }

                    // this.setState({ printReportArr: respErr.data, totalPages: respErr.totalPages, totalRecord: respErr.totalRecord })
                })
        } else {
            this.setState({ printReportArr: this.state.filterData })
        }
        // })

    }
    onSelectPrgmPlan = (value, item, label) => {
        let selectedValue = value
        this.setState({ filterKey: selectedValue })
        this.setState({
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 0,
        }, () => {
            this.onDataCall()
        })

    }
    onSelectClean = () => {
        this.setState({ filterKey: 'All' })
    }
    onChangeDateDuration = (e, f) => {
        console.log(e, f)
    }
    render() {
        return (
            <React.Fragment>
                {this.state.isLoader && <Loader />}
                < div className="reports-student-fees list-of-students-mainDiv" >
                    {/* <div className="student-report-header-title">
                        <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push('/main/dashboard') }} style={{ marginRight: 10, cursor: 'pointer' }} />
                        <p className="top-header-title">Report | Fees Collection</p>
                    </div> */}
                    <div className="trial-balance-header-title">
                        <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                        <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Application</p>
                    </div>
                    <div className="reports-body-section print-hd">
                        <React.Fragment>
                            {/* <ContainerNavbar containerNav={this.state.containerNav} onDownload={() => this.onDownloadEvent()} />
                            <div className="print-time">{this.state.printTime}</div> */}

                            <div className="demand-note-container">
                                <ContainerNavbar
                                    containerNav={this.state.containerNav}
                                    onDownload={() => this.onDownloadEvent()}
                                    searchValue={(searchValue) => this.searchHandle(searchValue)}
                                    Selectdata={this.state.appDatas}
                                    SelectCampusdata = {this.state.campusData}
                                    SelectSectionData={this.state.sectionData}
                                    onSelectDefaultValue={this.state.filterKey}
                                    onSelectCampusDefaultValue={this.state.filterKey}
                                    onSelectClean={this.onSelectClean}
                                    onSelectChange={this.onSelectPrgmPlan}
                                    dateDurationVal={(e, f) => { this.onChangeDateDuration(e, f) }}
                                />
                                <div className="print-time">{this.state.printTime}</div>
                                {/* <Button className="send-demand-note-btn" onClick={this.downloadTallyXML} style={{ minWidth: 'max-content', bottom: '5px', position: "relative" }} >Download Tally XML</Button> */}
                            </div>

                            <div className="application-main-sec-div">
                                <div className="left-section-app">
                                    <p>Total Domestic Application : <span>{this.state.finalDomesticData == "" ? "0" : this.state.finalDomesticData}</span></p>
                                </div>
                                <div className="left-section-app">
                                    <p>Total fees INR: <span>{this.state.finalINRdata == "" ? "0.00" : this.state.finalINRdata}</span></p>
                                </div>
                                <div className="right-section-app" style={{ width: "30%" }}>
                                    <p>Total International Application : <span>{this.state.finalintApp == "" ? "0" : this.state.finalintApp}</span></p>
                                </div>
                                <div className="right-section-app">
                                    <p>Total fees USD: <span>{this.state.finalUSDdata == "" ? "0.00" : this.state.finalUSDdata}</span></p>
                                </div>
                                <div className="right-section-app">
                                    <p>Total AED: <span>{this.state.totalAedData}</span></p>
                                </div>
                            </div>
                        </React.Fragment>
                        <div className="reports-data-print-table aplicationTable">
                            <div className="transaction-review-mainDiv">
                                <table className="transaction-table-review reports-tableRow-header col-split-td" id='myTable' ref={this.TableRef}>
                                    <thead>
                                        <tr>
                                            {this.state.tableHeader.map((data, i) => {
                                                if (data == "SelectIcon") {
                                                    return (
                                                        <th key={i} style={{ width: "15px" }}><Checkbox checked={false} name="checkedF" indeterminate={!this.state.checked} color="primary" /></th>
                                                    )
                                                }
                                                else {
                                                    return <th className={'student-fee ' + String(data).replace(' ', '')} key={i + 1}>{data}</th>
                                                }
                                            })}
                                        </tr>
                                    </thead>
                                    {this.state.printReportArr.length > 0 ?
                                        <tbody className="studentfee-reports-table">
                                            {this.state.printReportArr.map((data, i) => {
                                                return (
                                                    <tr key={i + 1} id={i + 1}>
                                                        {/* <td className="transaction-sno">{data.txnId}</td> */}
                                                        <td style={{ borderBottom: "1px solid #dfe1e6" }}><Checkbox value="checkedA" name={i} inputProps={{ 'aria-label': 'Checkbox A' }} color="primary" /></td>
                                                        <td className="transaction-vch-type appReport-id" >{data.applicationId}</td>
                                                        <td className="transaction-vch-num" style={{ width: "200px", textAlign: "left", paddingLeft: '10px' }}>{data.name}</td>
                                                        <td className="transaction-vch-num" style={{ textAlign: "left", paddingLeft: '10px' }}>{data.email}</td>
                                                        <td className="transaction-vch-type" style={{ width: "100px" }}>{data.mobile}</td>
                                                        <td className="transaction-vch-type" style={{ width: "230px", textAlign: "left", paddingLeft: '10px' }}>{data.programPlan}</td>
                                                        <td className="transaction-vch-type" style={{ width: "100px", textTransform: "uppercase" }}>{data.gatewayType}</td>
                                                        <td className="transaction-vch-type">{data.currencyCode}</td>
                                                        <td className="transaction-vch-type application-amount-right-align" style={{ width: "100px" }}>{Number(data.amount).toFixed(2)}</td>
                                                        <td className="transaction-vch-type">{data.transactionId == undefined ? '-' : data['transactionId']}</td>
                                                        <td className="transaction-vch-type" style={{ width: "100px" }}>{data.createdAt !== undefined ? moment(data.createdAt).format(this.context.dateFormat) : "-"}</td>
                                                        <td className={data.status == 'failed' ? "transaction-vch-type Status-Inactive" : data.status == 'submitted' ? "transaction-vch-type Status-Submitted" : "transaction-vch-type Status-Active"}> <span>{data.status == 1 ? '-' : data.status.charAt(0).toUpperCase() + data.status.slice(1)}</span></td>
                                                    </tr>
                                                )
                                            })}
                                        </tbody> :
                                        <tbody>
                                            <tr>
                                                <td className="noprog-txt" colSpan={this.state.tableHeader.length}>{this.state.dataInfo}</td>
                                            </tr></tbody>
                                    }
                                </table>
                            </div>
                            <div>
                                {this.state.printReportArr.length == 0 ? null :
                                    <PaginationUI
                                        total={this.state.totalRecord}
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        currentPage={this.state.page}
                                    />}
                            </div>
                        </div>
                    </div>
                </div >
            </React.Fragment>
        )
    }
}
export default ApplicationReport;