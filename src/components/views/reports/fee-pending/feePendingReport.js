import React, { Component } from 'react';
// import { withRouter } from 'react-router-dom';
import axios from 'axios';
import xlsx from 'xlsx';
import Loader from '../../../../utils/loader/loaders';
import '../../../../scss/student.scss';
// import KenTable from '../../../../utils/Table/kenTable';
// import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import feePendingResponse from './feePendingResponse';
import '../../../../scss/student-reports.scss';
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import PaginationUI from "../../../../utils/pagination/pagination";
import '../../../../scss/common-table.scss';
// import moment from 'moment';
import DateFormatContext from '../../../../gigaLayout/context'
// import JsonResponse3 from '../../../feeCollectionPortal/response3.json';
class feePendingReport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            demandNoteTable: [],
            env: JSON.parse(localStorage.getItem('env')),
            orgId: localStorage.getItem('orgId'),
            campusId :  localStorage.getItem("campusId"),
            userId : localStorage.getItem('userId'),
            containerNav: {
                isBack: false,
                name: "List of Fees Pending",
                isName: true,
                isSearch: true,
                isSort: false,
                isPrint: true,
                isDownload: true,
                isShare: false,
                isNew: false,
                newName: "New",
                isSubmit: false,
                isSelectAppReport: true,
                selectPickerOption: true,
                selectCampusOption: true,
                selectSectionOption:true
            },
            tableHeader: ["PROGRAM ID", "PROGRAM PLAN NAME", "NO. OF STUDENTS", "PENDING STUDENTS", "TOTAL FEES", "TOTAL FEES COLLECTED", "TOTAL PENDING"],

            printReportArr: feePendingResponse.data,
            previewStudentTableData: [],
            previewTableData: [],
            showTable: true,
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 0,
            noData: false,
            previewpage: 1,
            previewlimit: 10,
            previewtotalRecord: 0,
            previewtotalPages: 0,
            totalPreviewRecord: 0,
            particularItem: undefined,
            feependingIdReport: '',
            appDatas: [
                {
                    label: "All Program Plan",
                    value: "All"
                }
            ],
            campusData : [
                {
                    label: "All Campus",
                    value: "All"
                }
            ],
            sectionData : [
                {
                    label: "All Section",
                    value: "All"
                }
            ], 
            filterKey: "All",
            searchValue: '',
            paginationCall: "",
            brightKidTableData: [],
        }
    }
    static contextType = DateFormatContext;
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            if (this.state.searchValue.length == 0) {
                if (this.state.paginationCall === "allData") this.onDataCall();
                else if (this.state.paginationCall === "filterData") this.onFilterCall(page, limit);
                else this.onDataCall();
            }
            else {
                this.searchHandle(this.state.searchValue)
            }
        });
        console.log(page, limit);
    };
    componentDidMount() {
        console.log(feePendingResponse.data)
        let tableHeader = ["PROGRAM ID", `${this.context.classLabel ? this.context.classLabel : 'CLASS/BATCH'}`, "NO. OF STUDENTS", "PENDING STUDENTS", "TOTAL FEES", "TOTAL FEES COLLECTED", "TOTAL PENDING"]
        this.setState({ tableHeader })
        this.onDataCall()
        this.filterDataNew();




        // feePendingResponse.data.map((item, itemIndex) => {
        //     demandNoteTable.push({
        //         "Program Code": item["programPlanId"],
        //         "Program Plan Name": item["programPlanName"],
        //         "No. of Students": item["numberOfStudents"],
        //         "Pending Students": item["pendingStudents"],
        //         "Total Fees": item["totalFees"],
        //         "Total Fees Collected": item["totalFeesCollected"],
        //         "Total Pending": item['totalPending'],
        //     })
        // })

        // // -----------------------
        // console.log(demandNoteTable)
        // this.setState({ demandNoteTable: demandNoteTable })

    }

    getFeeCollectionData = () => {

        axios.get(`${this.state.env['headSeller']}/bkah/edu/leads?orgId=${this.state.orgId}`,
            {
                headers: {
                    "Authorization": localStorage.getItem('auth_token')
                }
            }
        )
            .then(res => {
                let response = res.data
                console.log(response, "response")
            }).catch(err => console.log(err))

    }
    filterDataNew = () => {
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        axios.get(`${this.state.env['zqBaseUri']}/edu/reports/feePending?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}`, {
            headers: {
                'Authorization': localStorage.getItem("auth_token"),
            }
        })
            .then(resp => {

                let dropDownData = [{
                    "value": "All",
                    "label": "All Pending Fees"
                }]

                let allData = resp.data.data
                let reportData = Array.from(new Set(allData.map(a => a.programPlanName))) //getting all Department names in array
                reportData.map((item, index) => {
                    dropDownData.push({
                        "value": item == undefined ? "-" : item,
                        "label": item == undefined ? "-" : item
                    })
                })
                this.setState({ appDatas: dropDownData })

                this.setState({ isLoader: false })
            })
            .catch(err => {
                console.log(err);
                this.setState({ isLoader: false, appDatas: [], noData: true })
            })

    }
    onDataCall = () => {
        this.setState({ isLoader: false })
        // let demandNoteTable = []
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ printReportArr: [], paginationCall: "allData" }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/feePending?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&classbatchName=${this.state.filterKey}&page=${this.state.page}&limit=${this.state.limit}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    console.log(resp);
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    this.setState({ printReportArr: resp.data.data, filterData: resp.data.data, totalPages: resp.data.totalPages, totalRecord: resp.data.totalRecord, noData })

                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true })
                    // var respErr = { "status": "success", "message": "feePending reports", "data": [{ "programPlanId": "PRGPLN_003", "programPlanName": null, "numberOfStudents": 5, "pendingStudents": 2, "items": [{ "regId": "STUD_003", "studentName": "Pradeep Kumar", "programPlanName": null, "totalFees": 5000, "totalPaid": 2000, "totalPending": 3000 }], "totalFees": 5000, "totalFeesCollected": 2000, "totalPending": 3000 }, { "programPlanId": "PRGPLN_001", "programPlanName": "BE Computer Science  ", "numberOfStudents": 5, "pendingStudents": 2, "items": [{ "regId": "STUD_001", "studentName": "Adiba Nisar", "programPlanName": "BE Computer Science  ", "totalFees": 145000, "totalPaid": 145000, "totalPending": 0 }], "totalFees": 145000, "totalFeesCollected": 145000, "totalPending": 0 }, { "programPlanId": "5fab8b7f2ae048251083235e", "programPlanName": null, "numberOfStudents": 5, "pendingStudents": 2, "items": [{ "regId": "STUD_003", "studentName": "Mohammed Yaseen R", "programPlanName": null, "totalFees": 152000, "totalPaid": 142818.79, "totalPending": 9181.209999999992 }, { "regId": "STUD_001", "studentName": "Adiba Nisar", "programPlanName": null, "totalFees": 146000, "totalPaid": 137181.2, "totalPending": 8818.800000000017 }], "totalFees": 298000, "totalFeesCollected": 279999.99, "totalPending": 18000.01000000001 }, { "programPlanId": "PRGPLN_002", "programPlanName": "BE Mechanical Engg 2020-21", "numberOfStudents": 5, "pendingStudents": 2, "items": [{ "regId": "STUD_002", "studentName": "Abdul Nasar", "programPlanName": "BE Mechanical Engg 2020-21", "totalFees": 125000, "totalPaid": 125000, "totalPending": 0 }], "totalFees": 125000, "totalFeesCollected": 125000, "totalPending": 0 }], "currentPage": null, "perPage": 10, "nextPage": null, "totalRecord": 5, "totalPages": 1 }
                    // this.setState({ printReportArr: respErr.data, totalPages: respErr.totalPages, totalRecord: respErr.totalRecord })
                })

        })
    }
    onFilterCall = (page, limit) => {
        this.setState({ isLoader: true, paginationCall: "filterData", page: page, limit: limit })
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ printReportArr: [] }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/feePending?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&classbatchName=${this.state.filterKey}&page=${this.state.page}&limit=${this.state.limit}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token"),
                }
            })
                .then(resp => {
                    console.log(resp);
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    this.setState({ printReportArr: resp.data.data, totalPages: resp.data.totalPages, totalRecord: resp.data.totalRecord, noData })
                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true })
                })

        })
    }
    printScreen = () => {
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ isLoader: false }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/feePending?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&classbatchName=${this.state.filterKey}&pagination=false`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    console.log(resp);
                    this.setState({
                        printable: true,
                        printReportArr: resp.data.data
                    }, () => {
                        this.setState({ printable: false })
                        window.print();
                        this.onDataCall();
                    })
                })
        });
    }
    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    onCheckbox = (selectedItem) => {
        console.log('selectedItem', selectedItem)
        this.setState({ selectedData: selectedItem })
        var amountInit = 0
        if (selectedItem.length > 0) {
            selectedItem.map(item => {
                if (item.Description != 'Total') {
                    amountInit = amountInit + item['Amount']
                }
            })
        }
        this.setState({ amount: amountInit })
        this.setState({ amountFormat: this.formatCurrency(amountInit) })
    }

    removeNull = (a) => {
        return a.split(",").filter(val => val != "" && val != "null" && val != "undefined").join(",")
    }

    onPaginationChange2 = (page, limit, datas) => {
        let data = JSON.parse(datas)
        this.setState({ page: page, limit: limit }, () => {
            // this.onDataCall();
            let current_page = Number(page);
            let perPage = Number(limit);
            let total_pages = Math.ceil(data.length / perPage);
            console.log("total", total_pages)
            current_page = total_pages < current_page ? total_pages : current_page
            // let offset = (current_page - 1) * perPage;
            let lastIndex = current_page * perPage;
            let startIndex = lastIndex - perPage;
            let paginatedItems = data.slice(startIndex, lastIndex)


            this.setState({ previewpage: current_page, previewlimit: perPage, previewtotalPages: total_pages, previewtotalRecord: data.length });
            this.setState({ previewTableData: paginatedItems });
        });
        console.log("yyyy", page, limit, this.state.previewtotalPages);
    };

    showFeePendingPreview = (previewItem, index) => {
        this.setState({
            previewpage: 1,
            previewlimit: 10,
            feependingIdReport: previewItem['programPlanDisplayName']
        });
        let preview = previewItem.items && previewItem.items.length > 0 ? previewItem.items : []
        let previewItemsArray = []
        this.setState({ particularItem: ((Number(this.state.page) - 1) * 10) + Number(index) })
        let regId = this.context.reportLabel || "Reg ID";
        let batch = this.context.classLabel || "CLASS/BATCH";
        preview.map(item => {
            previewItemsArray.push({
                [regId]: item.regId,
                "Student Name": item.studentName,
                [batch]: item.programPlanName,
                "Total Fees": item.totalFees,
                "Total Paid": item.totalPaid,
                "Total Pending": item.totalPending
            })
        })
        let previewArray = [
            {
                "PROGRAM ID": previewItem['programPlanDisplayName'],
                // "PROGRAM CODE": previewItem['programPlanId'],
                [batch]: previewItem["programPlanName"],
                "NO. OF STUDENTS": previewItem["numberOfStudents"],
                "PENDING STUDENTS": previewItem["pendingStudents"],
                "TOTAL FEES": previewItem["totalFees"],
                "TOTAL FEES COLLECTED": previewItem["totalFeesCollected"],
                "TOTAL PENDING": previewItem["totalPending"]
            }
        ]
        localStorage.setItem('previewTableData', JSON.stringify(previewItemsArray))
        let current_page = Number(this.state.previewpage);
        let perPage = Number(this.state.previewlimit);
        let total_pages = Math.ceil(previewItemsArray.length / perPage);

        current_page = total_pages < current_page ? total_pages : current_page
        // let offset = (current_page - 1) * perPage;
        let lastIndex = current_page * perPage;
        let startIndex = lastIndex - perPage;
        let paginatedItems = previewItemsArray.slice(startIndex, lastIndex)
        this.setState({
            previewStudentTableData: previewArray,
            previewTableData: paginatedItems,
            previewtotalPages: total_pages,
            previewtotalRecord: previewItemsArray.length
        }, () => {
            this.setState({ showTable: false });
        });
    }
    handleBack = () => {
        this.setState({ particularItem: undefined })
        if (this.state.showTable) {
            this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`);
        }
        else {
            this.setState({ showTable: true, page: 1 });
        }
    }
    searchHandle = (searchValue) => {
        console.log('searchValue', searchValue)
        this.setState({ isLoader: true, searchValue: searchValue })
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        if (searchValue.length > 0) {
            return axios.get(`${this.state.env['zqBaseUri']}/edu/reports/feePending?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&page=${this.state.page}&limit=${this.state.limit}&classbatchName=${this.state.filterKey}&searchKey=${searchValue}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    this.setState({
                        printReportArr: resp.data.data.reverse(),
                        totalPages: resp.data.totalPages,
                        totalRecord: resp.data.totalRecord,
                        noData
                    })

                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true, dataInfo: "No Data" })
                })
        }
        else {
            // this.setState({ printReportArr: this.state.filterData, isLoader: false })

            this.onDataCall()
        }

    }
    onSelectPrgmPlan = (value, item, label) => {
        let selectedValue = value
        this.setState({ filterKey: selectedValue })
        if (value === "All") {
            this.onDataCall()
        } else {
            this.onFilterCall(1, 10)
        }
    }
    onSelectClean = () => {
        this.setState({ filterKey: 'All' })
    }
    formatAmount = (amount) => {
        let a = Number(amount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })
        let b = a.replace('₹', "")
        return b
    }
    onDownloadEvent = () => {
        this.setState({ isLoader: true })
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        axios.get(`${this.state.env['zqBaseUri']}/edu/reports/feePending?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&classbatchName=${this.state.filterKey}`, {
            headers: {
                'Authorization': localStorage.getItem("auth_token")
            }
        })
            .then(resp => {
                console.log("fee pending", resp);
                var createXlxsData = []
                var createXlxsDataItem = []
                let batch = this.context.classLabel || "CLASS/BATCH";
                if (this.state.particularItem == undefined) {

                    resp.data.data.map(item => {
                        createXlxsData.push({
                            "PROGRAM ID": item.programPlanDisplayName,
                            // "PROGRAM CODE": item.programPlanId,
                            [batch]: item.programPlanName,
                            "NO. OF STUDENTS": item.numberOfStudents,
                            "PENDING STUDENTS": item.pendingStudents,
                            // "TOTAL FEES": this.formatCurrency(item.totalFees),
                            // "TOTAL FEES COLLECTED": this.formatCurrency(item.totalFeesCollected),
                            // "TOTAL PENDING": this.formatCurrency(item.totalPending)
                            "TOTAL FEES (INR)": this.formatAmount(item.totalFees),
                            "TOTAL FEES COLLECTED (INR)": this.formatAmount(item.totalFeesCollected),
                            "TOTAL PENDING (INR)": this.formatAmount(item.totalPending)
                        })
                        console.log('**DATA**', item)

                    })
                    var ws = xlsx.utils.json_to_sheet(createXlxsData);
                    var wb = xlsx.utils.book_new();
                    xlsx.utils.book_append_sheet(wb, ws, "Fee Pending");
                    xlsx.writeFile(wb, "fee pending.xlsx");
                    //xlsx.writeFile(wb, "fee_pending.csv");
                    this.setState({ isLoader: false })
                } else {
                    // var particularData = resp.data.data[this.state.particularItem]
                    var particularData = resp.data.data.filter(task => {
                        return task["PROGRAM ID"] == this.state.previewStudentTableData[0]['PROGRAM ID']
                    })
                    createXlxsData.push({
                        "PROGRAM ID": particularData[0].programPlanDisplayName,
                        // "PROGRAM CODE": particularData.programPlanId,
                        [batch]: particularData[0].programPlanName,
                        "NO. OF STUDENTS": particularData[0].numberOfStudents,
                        "PENDING STUDENTS": particularData[0].pendingStudents,
                        // "TOTAL FEES": this.formatCurrency(particularData.totalFees),
                        // "TOTAL FEES COLLECTED": this.formatCurrency(particularData.totalFeesCollected),
                        // "TOTAL PENDING": this.formatCurrency(particularData.totalPending)
                        "TOTAL FEES (INR)": this.formatAmount(particularData[0].totalFees),
                        "TOTAL FEES COLLECTED (INR)": this.formatAmount(particularData[0].totalFeesCollected),
                        "TOTAL PENDING (INR)": this.formatAmount(particularData[0].totalPending)
                    })
                    particularData[0].items.map((dataOne, c) => {
                        if (String(dataOne.name).toLowerCase() != "total") {
                            createXlxsDataItem.push({
                                "REG ID": dataOne["regId"],
                                "STUDENT NAME": dataOne["studentName"],
                                [batch]: dataOne["programPlanName"],
                                // "TOTAL FEES": this.formatCurrency(dataOne["totalPaid"]),
                                // "TOTAL PAID": this.formatCurrency(dataOne["totalFees"]),
                                // "TOTAL PENDING": this.formatCurrency(dataOne["totalPending"])
                                "TOTAL FEES (INR)": this.formatAmount(dataOne["totalPaid"]),
                                "TOTAL PAID (INR)": this.formatAmount(dataOne["totalFees"]),
                                "TOTAL PENDING (INR)": this.formatAmount(dataOne["totalPending"])
                            })
                        }
                    })
                    var ws = xlsx.utils.json_to_sheet(createXlxsData);
                    var wsf = xlsx.utils.json_to_sheet(createXlxsDataItem);
                    var wb = xlsx.utils.book_new();
                    xlsx.utils.book_append_sheet(wb, ws, "Fee Pending");
                    xlsx.utils.book_append_sheet(wb, wsf, "Studentwise Fee Pending");
                    xlsx.writeFile(wb, "fee pending.xlsx");
                    //xlsx.writeFile(wb, "fee_pending.csv");
                    this.setState({ isLoader: false })
                }
            })
            .catch(err => {
                console.log(err, 'errorrrrr')
                this.setState({ isLoader: false })
            })


    }
    render() {
        let regId = this.context.reportLabel || "Reg ID";
        return (<React.Fragment >
            {this.state.isLoader &&
                <Loader />
            }
            {/* { this.state.demandNoteTable.length > 0 ? <KenTable tableData={this.state.demandNoteTable} onCheckbox={this.onCheckbox} checkBox={true} /> : null} */}
            <div className="reports-student-fees list-of-students-mainDiv">
                {/* <div className="student-report-header-title">
                    <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.handleBack} style={{ marginRight: 10, cursor: 'pointer' }} />
                    {this.state.showTable ?
                        <p className="top-header-title">Report | Fee Pending</p> :
                        <p className="top-header-title">Fee Pending | {this.state.feependingIdReport}</p>
                    }
                </div> */}

                <div className="trial-balance-header-title">
                    <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.handleBack} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                    {this.state.showTable ?
                        <p className="top-header-title">| Fees Pending</p> :
                        <p className="top-header-title">| Fees Pending | {this.state.feependingIdReport}</p>
                    }
                </div>

                <div className="reports-body-section print-hd">
                    <React.Fragment>
                        <ContainerNavbar
                            containerNav={this.state.containerNav}
                            onDownload={() => this.onDownloadEvent()}
                            searchValue={(searchValue) => this.searchHandle(searchValue)}
                            Selectdata={this.state.appDatas}
                            SelectCampusdata = {this.state.campusData}
                            SelectSectionData={this.state.sectionData}
                            onSelectDefaultValue={this.state.filterKey}
                            onSelectCampusDefaultValue={this.state.filterKey}
                            onSelectClean={this.onSelectClean}
                            onSelectChange={this.onSelectPrgmPlan}
                            printScreen={this.printScreen}
                            dateDurationVal={(e, f) => { this.onChangeDateDuration(e, f) }}
                        />
                        <div className="print-time">{this.state.printTime}</div>
                    </React.Fragment>
                    <div className="reports-data-print-table">
                        <div className="">
                            {this.state.showTable ?
                                <React.Fragment>
                                    <table className="transaction-table-review reports-tableRow-header">
                                        <thead>
                                            <tr>
                                                {this.state.tableHeader.map((data, i) => {
                                                    return <th className={"first-child-space demand-note " + String(data).replace(' ', '')} key={i + 1}>{data}</th>
                                                })}
                                            </tr>
                                        </thead>
                                        {this.state.printReportArr.length > 0 ?
                                            <tbody>
                                                {this.state.printReportArr.map((data, i) => {
                                                    return (<tr key={i + 1} id={i + 1} className="reports-tableRow-body"
                                                        onClick={(e) => { e.preventDefault(); this.showFeePendingPreview(data, i) }}
                                                        style={{ cursor: "pointer" }}>
                                                        <td className="transaction-vch-num">{data.programPlanDisplayName}</td>
                                                        {/* <td className="transaction-vch-num" >{data.programPlanId}</td> */}
                                                        <td className="transaction-vch-num" >{data.programPlanName}</td>
                                                        <td className="transaction-vch-num" >{data.numberOfStudents}</td>
                                                        <td className="transaction-vch-num" >{data.pendingStudents}</td>
                                                        <td className="transaction-vch-type">{this.formatCurrency(Number(data.totalFees))}</td>
                                                        <td className="transaction-vch-type">{this.formatCurrency(Number(data.totalFeesCollected))}</td>
                                                        <td className="transaction-vch-type">{this.formatCurrency(Number(data.totalPending))}</td>
                                                    </tr>
                                                    )
                                                })}
                                            </tbody> :
                                            <tbody>
                                                <tr>
                                                    {this.state.noData ? <td className="noprog-txt" colSpan={this.state.tableHeader.length}>No data...</td> :
                                                        <td className="noprog-txt" colSpan={this.state.tableHeader.length}>Fetching the data...</td>
                                                    }

                                                </tr>
                                            </tbody>
                                        }
                                    </table>
                                    <div>
                                        {this.state.printReportArr.length == 0 ? null :
                                            <PaginationUI
                                                total={this.state.totalRecord}
                                                onPaginationApi={this.onPaginationChange}
                                                totalPages={this.state.totalPages}
                                                limit={this.state.limit}
                                                currentPage={this.state.page}
                                            />}
                                    </div>
                                </React.Fragment>
                                :
                                <React.Fragment>
                                    <table className="transaction-table-review reports-tableRow-header">
                                        <thead>
                                            <tr>
                                                {Object.keys(this.state.previewStudentTableData[0]).map((data, i) => {
                                                    return <th className={"first-child-space demand-note " + String(data).replace(' ', '')} key={i + 1}>{data}</th>
                                                })}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.previewStudentTableData.map((data, i) => {
                                                return (<tr key={i + 1} id={i + 1} className="reports-tableRow-body">
                                                    <td className="transaction-vch-num" >{data["PROGRAM ID"]}</td>
                                                    {/* <td className="transaction-vch-num" >{data["PROGRAM CODE"]}</td> */}
                                                    <td className="transaction-vch-num" >{data[`${this.context.classLabel ? this.context.classLabel : 'CLASS/BATCH'}`]}</td>
                                                    <td className="transaction-vch-num" >{data['NO. OF STUDENTS']}</td>
                                                    <td className="transaction-vch-num" >{data['PENDING STUDENTS']}</td>
                                                    <td className="transaction-vch-type">{this.formatCurrency(Number(data['TOTAL FEES']))}</td>
                                                    <td className="transaction-vch-type">{this.formatCurrency(Number(data['TOTAL FEES COLLECTED']))}</td>
                                                    <td className="transaction-vch-type">{this.formatCurrency(Number(data['TOTAL PENDING']))}</td>
                                                </tr>
                                                )
                                            })}
                                        </tbody>
                                    </table>
                                    <p className="statement-of-payment-text">Studentwise Fee Pending</p>
                                    {this.state.previewTableData.length == 0 ?
                                        <p style={{ fontSize: 16, textAlign: 'center', marginTop: 30 }}>No Data</p>
                                        :
                                        <React.Fragment>
                                            <table className="transaction-table-review reports-tableRow-header">
                                                <thead>
                                                    <tr>
                                                        {Object.keys(this.state.previewTableData[0]).map((data, i) => {
                                                            return <th className={"demand-note " + String(data).replace(' ', '')} key={i + 1}>{data}</th>
                                                        })}
                                                    </tr>
                                                </thead>
                                                {this.state.printReportArr.length > 0 ?
                                                    <tbody>
                                                        {this.state.previewTableData.map((data, i) => {
                                                            return (<tr key={i + 1} id={i + 1} className="reports-tableRow-body">
                                                                {/* style={{ cursor: "pointer" }} */}
                                                                <td className="transaction-vch-num" >{data[regId]}</td>
                                                                <td className="transaction-vch-num" >{data["Student Name"]}</td>
                                                                <td className="transaction-vch-num" >{data[`${this.context.classLabel ? this.context.classLabel : 'CLASS/BATCH'}`]}</td>
                                                                <td className="transaction-vch-num" >{this.formatCurrency(Number(data["Total Fees"]))}</td>
                                                                <td className="transaction-vch-type">{this.formatCurrency(Number(data["Total Paid"]))}</td>
                                                                <td className="transaction-vch-type">{this.formatCurrency(Number(data["Total Pending"]))}</td>
                                                            </tr>
                                                            )
                                                        })}
                                                    </tbody> :
                                                    <tbody>
                                                        <tr>
                                                            <td className="noprog-txt" colSpan={this.state.tableHeader.length}>Fetching the data...</td>
                                                        </tr></tbody>}
                                            </table>
                                            <div>
                                                {this.state.previewTableData.length == 0 ? null :
                                                    <PaginationUI
                                                        total={this.state.previewtotalRecord}
                                                        onPaginationApi={this.onPaginationChange2}
                                                        totalPages={this.state.previewtotalPages}
                                                        limit={this.state.previewlimit}
                                                        currentPage={this.state.previewpage}
                                                        allData={localStorage.getItem('previewTableData')}
                                                    />}
                                            </div>
                                        </React.Fragment>
                                    }

                                </React.Fragment>}
                        </div>

                    </div>
                </div>
            </div>

        </React.Fragment >);
    }
}

export default feePendingReport;