import React from 'react';
import '../../../../scss/transaction-scholarship.scss';
import '../../../../scss/payment-portal.scss';
import '../../../../scss/portal-stepper.scss';
import '../../../../scss/student-reports.scss';
import '../../../../scss/receive-payment.scss';
import '../../../../scss/student.scss';
import '../../../../scss/setup.scss';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import success from '../../../../assets/icons/zq-success.svg';
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import componentData from './scholarship-transaction-data.json';
// import scholarshipFormData from './scholarship-payment.json';
import ZqTable from "../../../../utils/Table/table-component";
import PaginationUI from "../../../../utils/pagination/pagination";
import Loader from "../../../../utils/loader/loaders";
import ZenTabs from '../../../../components/input/tabs';
import ZenForm from '../../../input/form';
import { Steps, Alert } from 'rsuite';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import PaymentJS from './payment-scholarship';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import PublishOutlinedIcon from '@material-ui/icons/PublishOutlined';
import LinearProgress from '@material-ui/core/LinearProgress';
import * as xlsx from "xlsx";
import axios from 'axios';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import ScholarshipFile from './Scholarship.xlsx';
import CancelIcon from '@material-ui/icons/Cancel';
import Axios from 'axios';


import moment from 'moment';
import DateFormatContext from '../../../../gigaLayout/context';

function SnackAlert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class ScholarshipTransaction extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orgId: localStorage.getItem('orgId'),
            userId: localStorage.getItem('userId'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem('auth_token'),
            tableListView: "tableList",
            listOfStudentData: [],
            pendingAmountData: null,
            userForm: componentData.formDataListPreview["ScholarshipDetails"],
            demandNoteDetails: [],
            selectedInfo: [],
            studentFeeID: [],
            paymentAmount: '',
            paymentCategory: ["Scholarship"],
            closePreview: true,
            disablePayment: true,
            noProg: "Fetching Data ...",
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 1,
            totalInvoices: [],
            isLoader: false,
            totalInvoices1: [],
            previewId: "",
            activeStep: 2,
            activeStepFile: 0,
            upload: false,
            filename: '',
            attachmentFileData: '',
            pdfLoaded: 0,
            processLoaded: 0,
            scholarshipStatus: true,
            reviewScholarshipData: [],
            sheetDataItemNew: [],
            snackbar: { openNotification: false, NotificationMessage: "", status: "success" }
        }
        this.inputFile = React.createRef(this.inputFile)
    }

    static contextType = DateFormatContext;
    componentDidMount() {
        this.getTableData()
    }
    getTableData = () => {
        this.setState({ reviewScholarshipData: [] })
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/transactions/scholarships?orgId=${this.state.orgId}&page=${this.state.page}&limit=${this.state.limit}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                console.log(res.data)
                let regId = this.context.reportLabel || "REG ID"
                let batch = this.context.classLabel || "CLASS/BATCH"
                if (res.data.status) {
                    if (res.data.data.length == 0) {
                        this.setState({ noProg: "No Data", LoaderStatus: false, datas: [] })
                    }
                    else {
                        let datas = [];
                        this.setState({ listOfStudentData: [] }, () => {
                            res.data.data.map((item) => {
                                datas.push({
                                    "Id": item.displayName,
                                    [regId]: item.studentRegId,
                                    "Student Name": item.studentName,
                                    [batch]: item.class,
                                    "Academic Year": item.academicYear,
                                    "Scholarship Provider": item.data.scholarshipProvider,
                                    "Amount Sanctioned": this.formatCurrency(Number(item.amount)),
                                    "Amount Received": this.formatCurrency(Number(item.amount)),
                                    "Mode of payment": item.data.modeofPayment,
                                    "Created On": item.createdAt !== "" ? this.context.dateFormat !== "" ? moment(item.createdAt).format(this.context.dateFormat) : moment(item.createdAt).format('DD/MM/YYYY') : "-",
                                    "Item": JSON.stringify(item),
                                    "Status": "Sanctioned",
                                })
                            })
                            this.setState({
                                listOfStudentData: datas,
                                activeStep: 0,
                                tableListView: "tableList",
                                totalPages: res.data.totalPages,
                                page: res.data.page,
                                totalRecord: res.data.totalRecord,
                            })
                        })
                    }
                }
            })
            .catch(err => {
                this.setState({ noProg: "No Data", tablePrintDetails: [], previewList: false, LoaderStatus: false })
            })
    }

    resetForm = () => {
        let newData = componentData.formDataListPreview;
        Object.keys(newData).forEach(tab => {
            newData[tab].forEach(task => {
                task["error"] = false;
                task['validation'] = false;
                task['errorMsg'] = "";
                task['defaultValue'] = "";
            })
        })
        this.setState({ userForm: newData })
    }
    onAddNewScholarship = () => {
        console.log("Add new");
        this.resetForm();
        this.setState({ tableListView: "addNewList" })
    }
    onSubmit = (data) => {
        console.log('***STUDENT ID***', data.studentID.value)
        let studentId = data.studentID.value
        axios({
            method: 'get',
            url: `${this.state.env['zqBaseUri']}/edu/transactions/scholarships/${studentId}?orgId=${this.state.orgId}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                this.state.demandNoteDetails = []
                let scholarshipData = res.data
                console.log('scholarshipData', scholarshipData)
                let studentName = scholarshipData[0].studentDetails['firstName'] + ' ' + scholarshipData[0].studentDetails['lastName']
                let demandNoteDetails = []
                let feeTypesArray = []
                scholarshipData[0]['feeDetails'].map(item => {
                    feeTypesArray.push(item.feeType)
                })
                // Array(scholarshipData[0]['demandNote']).map(item => {
                demandNoteDetails.push({
                    "Demand Note ID": scholarshipData[0]['demandNote'].displayName,
                    "Payment term": feeTypesArray.toString(),
                    "Total Fee": this.formatCurrency(Number(scholarshipData[0]['demandNote'].amount)),
                    "Paid": this.formatCurrency(Number(scholarshipData[0].paid)),
                    "Pending": this.formatCurrency(Number(scholarshipData[0].pending)),
                    "Status": scholarshipData[0]['demandNote'].status,
                    'Item': JSON.stringify(scholarshipData[0])
                })
                // })
                this.setState({
                    StudentName: studentName,
                    StudentRegId: scholarshipData[0].studentDetails['regId'],
                    StudentClass: scholarshipData[0]['demandNote'].class,
                    demandNoteDetails: demandNoteDetails,
                    // closePreview: false,
                    activeStep: this.state.activeStep + 1,
                    disablePayment: true
                })
            })
            .catch(err => {
                console.log(err)
                Alert.error(err.message)
            })
    }
    onPreviewStudent = (e) => {
        console.log("Preview", e);
    }
    onPreviewStudentList = (e) => {
        // console.log("Preview", e);
        // let previewListData = componentData.formDataListPreview;
        // Object.keys(previewListData).forEach(tab => {
        //     previewListData[tab].forEach(item => {
        //         if (item.name == "ID") {
        //             item["defaultValue"] = e['ID'];
        //             item["readOnly"] = false;
        //             item["required"] = false;
        //         }
        //         if (item['name'] == 'RegID') {
        //             item['defaultValue'] = e['REG ID']
        //             item['readOnly'] = true
        //             item['required'] = false
        //         }
        //         if (item['name'] == 'StudentName') {
        //             item['defaultValue'] = e['STUDENT NAME']
        //             item['readOnly'] = true
        //             item['required'] = false
        //         }
        //         if (item['name'] == 'ProgramPlan') {
        //             item['defaultValue'] = e['Program Plan']
        //             item['readOnly'] = true
        //             item['required'] = false
        //         }
        //         if (item['name'] == 'YearOfJoin') {
        //             item['defaultValue'] = e['Year of Joining']
        //             item['readOnly'] = true
        //             item['required'] = false
        //         }
        //         if (item.name == "scholarshiptype") {
        //             item["defaultValue"] = 'Private';
        //             item["readOnly"] = false;
        //             item["required"] = false;
        //         }
        //         if (item.name == "scholarshipprovider") {
        //             item["defaultValue"] = e['Scholarship Provider']
        //             item["readOnly"] = false;
        //             item["required"] = false;
        //         }
        //         if (item.name == "modeofPayment") {
        //             item["defaultValue"] = e['Mode of payment'];
        //             item["readOnly"] = false;
        //             item["required"] = false;
        //         }
        //         if (item.name == "scholarshipamountsanctioned") {
        //             item["defaultValue"] = e['Amount Sanctioned'];
        //             item["readOnly"] = false;
        //             item["required"] = false;
        //         }
        //         if (item.name == "scholarshipamountreceived") {
        //             item["defaultValue"] = e['Amount Received'];
        //             item["readOnly"] = false;
        //             item["required"] = false;
        //         }
        //         if (item.name == "status") {
        //             item["defaultValue"] = e['Status'];
        //             item["readOnly"] = false;
        //             item["required"] = false;
        //         }
        //     })
        // })
        // this.setState({ tableListView: "previewForm", previewId: e.ID })
    }
    getRefundValue = (data, type) => {

        let obj = {}
        if (type == 'refund') {
            if (data.status[0].refund.refund) {
                obj.adjust = data.status[0].refund.adjust
                obj.refund = data.status[0].refund.refund
                console.log("refund status", JSON.stringify(obj))
                return obj
            }
            else { return data.status[0].refund }
        }
        else if (type == 'adjust') {
            if (data.status[0].adjust.adjust) {
                obj.adjust = data.status[0].adjust.adjust
                obj.refund = data.status[0].adjust.refund
                console.log("refund status", JSON.stringify(obj))
                return obj
            } else { return data.status[0].adjust }
        }
    }
    onRowClickBoxFun = () => {
        console.log("Row select");
    }
    // resetForm = () => {
    //     let newData = this.state.componentData.;
    //     newData.map((task) => {
    //         task["error"] = false;
    //         task['validation'] = false;
    //         task['errorMsg'] = "";
    //         task['defaultValue'] = "";
    //     })
    //     this.setState({ componentData: newData })

    // }
    actionClickFun = () => {
        console.log("Action click");
    }
    goBackToList = () => {
        this.setState({ tableListView: "tableList", activeStep: 0 })
        this.resetForm()
    }
    onFormSubmitData = () => { }
    handleTabChange = () => { }
    // cancelStudentFun = () => { this.setState({ activeStep: 0 }) }
    bytesToSize(bytes) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Byte';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        this.setState({ fileSize: Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i] })
    }
    fileSelected = (ev, i) => {
        let file = ev.target.files
        if (String(file[0].name).includes('.xlsx') && file.length == 1) {
            this.setState({ pdfLoaded: 10 })
            this.setState({ filename: file[0].name, upload: true, attachmentFileData: ev.target.files[0] }, () => {
                this.setState({ pdfLoaded: 25 })
                this.bytesToSize(file[0]['size'])
                this.setState({ pdfName: file[0].name, pdfLoaded: 50 }, () => {
                    setTimeout(() => {
                        this.excelUpload(file[0])
                    }, 100);
                })
            })
        }
        else {
            alert('Please Upload file in xlsx format !..')
        }
    }
    excelUpload = (file) => {
        this.setState({ showViewer: true }, () => {
            this.setState({ pdfExtract: true, pdfLoaded: 100, processLoaded: 0 }, () => {
                this.onExtract(file)
            })
        });
    }
    onExtract = (file) => {
        if (this.state.processLoaded < 75) {
            this.setState({ processLoaded: Number(this.state.processLoaded) + 10 }, () => {
                setTimeout(() => {
                    this.onExtract(file)
                }, 100);

            })
        } else {
            this.setState({ processLoaded: 80 }, () => {
                this.readFile(file);
            })
        }
    }
    readFile = (file) => {
        let f = file;
        let name = f.name;
        this.setState({ fileName: name });
        const reader = new FileReader();
        reader.onload = (evt) => {
            const source = evt.target.result;
            const wb = xlsx.read(source, { type: "binary" });
            /* Get all Worksheet */
            let sheetData = [];
            let sheetDataItem = {};
            console.log('sheets', wb.Sheets)
            let sheetKeys = Object.keys(wb.Sheets)
            sheetKeys.map(key => {
                let particularSheetKey = Object.keys(wb.Sheets[key])
                if (String(key).toLowerCase() == "scholarship") {
                    particularSheetKey.map(itemKey => {
                        if (String(itemKey).includes('1')) {
                            sheetData.push(wb.Sheets[key][itemKey]['v'])

                        }
                    })

                }
            })
            console.log('sheetData', sheetData)
            this.setState({ tableHeader: sheetData })
            wb.SheetNames.forEach(item => {
                const ws = wb.Sheets[item];
                const data = xlsx.utils.sheet_to_json(ws, { defval: "-", raw: false });
                sheetDataItem[item] = data;
            })
            this.setState({ sheetDataItemNew: sheetDataItem['Scholarship'] })

            let scholarDetails = []
            let studentID = []
            //trimobj
            let DummayArray = this.trimObj(sheetDataItem['Scholarship'])
            console.log("****Dummary", DummayArray)

            DummayArray.forEach((item, i) => {
                studentID.push({
                    'regId': item['student registration id'],
                    'sanctionedAmount': item['scholarship amount received']
                })
                console.log('studentID******', studentID)
            })

            //api call
            Axios.post(`${this.state.env['zqBaseUri']}/edu/transactions/getStudentStatus?orgId=${this.state.orgId}`, studentID, {
                headers: {
                    // "Authorization": localStorage.getItem('auth_token')
                }
            }).then(response => {
                let mappingScholarshipData = response.data
                console.log("post response", response.data)

                DummayArray.forEach((dataOne, i) => {
                    // dataOne['Scholarship Amount Received'] = this.formatCurrency(Number(dataOne['Scholarship Amount Received']))
                    let refundArray = []
                    mappingScholarshipData.forEach((data, io) => {
                        if (i === io) {
                            scholarDetails.push(
                                {
                                    ...dataOne,
                                    "Fees": data.totalFees ? Number(data.totalFees) : '-',
                                    "Paid": Number(data.paid),
                                    "Sanctioned": dataOne['scholarship amount received'],
                                    'Is Final Year': data['isFinalYear'] == true ? 'Yes' : 'No',
                                    'Action': data.status.length !== 0 ? data.status[0].status : '-',
                                    'Refund': this.getRefundValue(data, 'refund'),
                                    'Adjust': this.getRefundValue(data, 'adjust'),
                                    'Adjust with': data.status.length !== 0 ? data.status[0]['adjustWith'] : '-'
                                }
                            )
                        }
                    })
                })
                let mergeData = [...scholarDetails, ...DummayArray];
                let result = mergeData.filter(function (a) {
                    return !this[a['student registration id']] && (this[a['student registration id']] = true);
                }, Object.create(null));
                // var result = scholarDetails.reduce((unique, o) => {
                //     if (!unique.some(obj => obj['Student Registration Id'] === o['Student Registration Id'])) {
                //         unique.push(o);
                //     }
                //     return unique;
                // }, [])
                console.log('*******************', result)
                this.setState({ activeStepFile: 1, reviewScholarshipData: result })


            }).catch(err => {
                console.log("post response", err)
                // Alert.error("api response Failed !")
                Alert.error("Invalid Student Details!")
                this.setState({ activeStepFile: 0, upload: false, processLoaded: 0, pdfLoaded: 0 })
            })


        };
        reader.readAsBinaryString(f);
        // setTimeout(() => {
        // Alert.info('Please review and upload')
        // }, 1000)
    }
    trimObj = (obj) => {
        if (!Array.isArray(obj) && typeof obj != 'object') return obj;
        return Object.keys(obj).reduce((acc, key) => {
            acc[key.trim().toLowerCase()] = typeof obj[key] == 'string' ? obj[key].trim() : this.trimObj(obj[key]);
            return acc;
        }, Array.isArray(obj) ? [] : {});
    }
    cancelStudentFun = () => {
        this.setState({ activeStep: this.state.activeStep - 1 })
    }
    confirmStudentFun = () => {
        let scholarshipTypes = []
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/scholarships?orgId=${this.state.orgId}&pagination=false`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                console.log(res.data)
                if (res.data.status == "success") {
                    if (res.data.data.length == 0) {
                        this.state.pendingAmountData.map((data) => {
                            if (data.name == "Scholarshiptype") {
                                data['options'] = []
                                data['readOnly'] = false
                                data['validation'] = true
                            }
                        })
                        this.setState({ viewFormData: true })
                        this.setState({ activeStep: this.state.activeStep + 1 })
                    }
                    else {
                        let datas = [];
                        res.data.data.sort((a, b) => Number(new Date(b.createdAt)) - Number(new Date(a.createdAt))).map((item) => {
                            scholarshipTypes.push({
                                "label": item.type,
                                'value': item._id,
                                'itemDetails': item
                            })
                        })
                        this.state.pendingAmountData.map((data) => {
                            if (data.name == "Scholarshiptype") {
                                data['options'] = scholarshipTypes
                                data['readOnly'] = false
                                data['validation'] = true
                            }
                        })
                        this.setState({ viewFormData: true })
                        this.setState({ activeStep: this.state.activeStep + 1 })
                    }
                }
            })
            .catch((err) => {
                this.state.pendingAmountData.map((data) => {
                    if (data.name == "Scholarshiptype") {
                        data['options'] = []
                        data['readOnly'] = false
                        data['validation'] = true
                    }
                })
                this.setState({ viewFormData: true })
                this.setState({ activeStep: this.state.activeStep + 1 })
            })
    }
    selectedItem = (e, value, index) => {

        console.log("e,", e)
        console.log("value,", value)
        console.log("index", index)
        // this.setState({ disablePayment: false });
        let value1 = JSON.parse(value.Item);
        let paymentAmount = this.state.paymentAmount;
        let selectedAmt = value1.pending
        // if (selectedAmt === 0) {
        //     this.setState({ disablePayment: true });
        // } else {
        //     this.setState({ disablePayment: false });
        // }

        let items = {}
        items = selectedAmt
        if (value.checked == true) {
            let studentFeeID = [];
            studentFeeID = this.state.studentFeeID;
            studentFeeID.push(value1.demandNote.displayName)
            this.setState({ studentFeeID: studentFeeID });
            localStorage.setItem("studentFeeID", this.state.studentFeeID)
            let payableAmt = paymentAmount + Number(items)
            this.setState({ paymentAmount: payableAmt, viewFormData: false });
            console.log("payableAmt", payableAmt)
            this.state.pendingAmountData.map((data) => {
                if (data.name == "Scholarshipamountreceived") {
                    let a = value['Pending'].replace('₹', '');
                    let b = a.replace(/,/g, '');
                    data['defaultValue'] = Number(b).toFixed(2)
                    data['readOnly'] = false
                    data['validation'] = true
                }
            })
            this.setState({ viewFormData: true })
        } else {
            this.setState({ disablePayment: true });
            let studentFeeID = [];
            studentFeeID = this.state.studentFeeID;
            const indexVal = studentFeeID.indexOf(value1.demandNote.displayName)
            if (indexVal > -1) {
                studentFeeID.splice(indexVal, 1)
            }
            this.setState({ studentFeeID: studentFeeID });
            localStorage.setItem("studentFeeID", this.state.studentFeeID)
            let payableAmt = value.rowId == 0 ? 0 : paymentAmount - Number(items)
            this.setState({
                paymentAmount: payableAmt,
                viewFormData: false
            });

            console.log("payableAmt", payableAmt)
            this.state.pendingAmountData.map((data) => {
                if (data.name == "Scholarshipamountreceived") {
                    data['defaultValue'] = Number(payableAmt).toFixed(2)
                    data['readOnly'] = false
                    data['validation'] = true
                }

            })
            this.setState({ viewFormData: true })
        }
        if (this.state.studentFeeID.length == 0) {
            this.setState({ disablePayment: true });
        } else {
            this.setState({ disablePayment: false });
        }
    }
    selectAll = (allItems, a) => {
        this.setState({ disablePayment: false });
        let AmountValues = []
        allItems.map(item => {
            AmountValues.push((JSON.parse(item.Item)).pending)
        })
        console.log(AmountValues)
        let total = AmountValues.reduce((a, b) => a + b, 0)
        console.log(total)
        let value1 = JSON.parse(allItems[0].Item);
        let selectedAmt = value1.pending
        // if (selectedAmt === 0) {
        //     this.setState({ disablePayment: true });
        // } else {
        //     this.setState({ disablePayment: false });
        // }
        if (a == true) {
            let studentFeeID = []
            allItems.map(item => {
                studentFeeID.push(item['Demand Note ID'])
            })
            this.setState({ studentFeeID: studentFeeID });
            localStorage.setItem("studentFeeID", this.state.studentFeeID)
            this.setState({ selectedInfo: allItems, enableBtn: true })
            this.state.pendingAmountData.map((data) => {
                if (data.name == "Scholarshipamountreceived") {
                    data['defaultValue'] = Number(total).toFixed(2)
                    data['readOnly'] = false
                    data['validation'] = true
                }
            })
            this.setState({ paymentAmount: Number(total) })
        }
        else {
            this.setState({ disablePayment: true });
            let studentFeeID = []
            this.setState({ studentFeeID: [] });
            localStorage.setItem("studentFeeID", this.state.studentFeeID)
            this.setState({ selectedInfo: [], enableBtn: false })
            this.state.pendingAmountData.map((data) => {
                if (data.name == "Scholarshipamountreceived") {
                    data['readOnly'] = false
                    data['validation'] = true
                    data['defaultValue'] = ''
                }
            })
            this.setState({ paymentAmount: '' })
        }
    }
    onScholFormchanges = (value, item, e, dataS) => {
    }

    getStepContent = (stepIndex) => {
        switch (stepIndex) {
            case 0:
                return <React.Fragment>
                    <div className="receive-payment-main">
                        {this.state.circularLoader ?
                            <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                                <CircularProgress size={24} />
                            </div> : null}
                        <div className="receive-payment-table">
                            <React.Fragment >
                                <div className="payment-form form-width-add">
                                    <ZenForm inputData={componentData.FetchStudentDetails} onSubmit={this.onSubmit} />
                                </div>
                            </React.Fragment>
                        </div>
                    </div>
                </React.Fragment>
            case 1:
                return <React.Fragment>
                    <div className="receive-payment-main student-list-demand-note">
                        <React.Fragment>
                            <div className="receive-amount-table">
                                <div className="student-txt">
                                    <h3 className="student-details-header">Student Details</h3>
                                    {/* {this.state.closePreview ? */}
                                    <React.Fragment>
                                        <Button className="payment-method-btn" style={{ right: '90px' }} onClick={this.cancelStudentFun}>Cancel</Button>
                                        <Button className={this.state.disablePayment == true ? "payment-method-btn colordisablebtn" : "payment-method-btn"} onClick={this.confirmStudentFun} disabled={this.state.disablePayment == true} style={{ right: '190px' }}>Add</Button>
                                    </React.Fragment>
                                    {/* : null} */}
                                    <div className='student-details'>
                                        <p style={{ marginLeft: "20px" }}><b className="bold-text">Student Reg. ID:</b> {this.state.StudentRegId}</p>
                                        <p style={{ marginLeft: "20px" }}><b className="bold-text">Student Name:</b> {this.state.StudentName}</p>
                                        <p style={{ marginLeft: "20px" }}><b className="bold-text">Student Class:</b> {this.state.StudentClass}</p>
                                    </div>
                                </div>

                                <div className="table-section-student-list">
                                    <ZqTable
                                        data={this.state.demandNoteDetails}
                                        rowClick={(item) => { this.onPreviewStudent(item) }}
                                        onRowCheckBox={(e, value, index) => { this.selectedItem(e, value, index) }}
                                        allSelect={(allItems, a) => { this.selectAll(allItems, a) }}

                                    />
                                </div>
                            </div>
                        </React.Fragment>
                    </div>
                </React.Fragment >
            case 2:
                return <React.Fragment>

                    <div className="receivePaymentPreviewPaymentTab">
                        <div className="collectPaymentPreviewPaymentTab">
                            <PaymentJS
                                paymentAmount={this.state.paymentAmount}
                                studentFeeID={this.state.studentFeeID}
                                studentData={this.state.studentData}
                                guardianDetails={this.state.guardianDetails}
                                paymentType={this.state.paymentType}
                                studentID={this.state.studentID}
                                StudentRegId={this.state.StudentRegId}
                                StudentName={this.state.StudentName}
                                StudentClass={this.state.StudentClass}
                                feeTypeLists={this.state.feeTypeLists}
                                goNextFun={this.goNextFun1}
                                goNextErrFun={this.goNextErrFun1}
                                onPaymentPreview={false}
                            />
                        </div>
                    </div>

                    {/* <div className="receivePaymentPreviewPaymentTab">
                        <div className="collectPaymentPreviewPaymentTab">
                        <PaymentJS
                            paymentFormData={this.state.pendingAmountData}
                            paymentCategory={this.state.paymentCategory}
                            paymentType={'Scholarship'}

                            onInputChanges={this.onScholFormchanges}
                            goNextFun={this.goNextFun1} />
                    </div>
                    </div> */}
                </React.Fragment>
            case 3:
                return <React.Fragment>
                    {
                        this.state.scholarshipStatus ?

                            <React.Fragment>
                                <div className="receive-payment-main student-list-demand-note" style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                                    <div className="success-div">
                                        <CheckCircleIcon className="check-circle" style={{ fontSize: "40px" }} />
                                        <p style={{ marginLeft: "-80px" }}>Scholarship Added Successfully</p>
                                        <div className="scholarship-btn-finish">
                                            <Button variant="contained" type="submit" class="primary-btn form-submit-btn" onClick={this.uploadFinish2}
                                                style={{
                                                    height: "40px",
                                                    position: 'unset',
                                                    marginTop: '20px'
                                                }}
                                            >Finish</Button>
                                        </div>
                                    </div>
                                </div>
                            </React.Fragment>
                            :
                            <React.Fragment>


                                <div className="receive-payment-main student-list-demand-note" style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                                    <div className="success-div">
                                        <CancelIcon className="check-circle2" style={{ fontSize: "40px" }} />
                                        <p style={{ marginLeft: "-80px" }}>Failed to Add Scholarship</p>
                                        <div className="scholarship-btn-finish">
                                            <Button variant="contained" type="submit" class="primary-btn form-submit-btn" onClick={this.uploadFinish2}
                                                style={{
                                                    height: "40px",
                                                    position: 'unset',
                                                    marginTop: '20px'
                                                }}
                                            >Finish</Button>
                                        </div>
                                    </div>
                                </div>
                            </React.Fragment>
                    }
                </React.Fragment >
            default:
                return null;
        }
    }
    goNextFun1 = (data, formItem, values) => {
        console.log("values", values)
        let formdata = values
        let formPayload = {
            "txnDate": "",
            "Scholarshiptype": "",
            "Scholarshipprovider": "",
            "Scholarshipamountreceived": "",
            "Scholarshipmode": "",
            "Account Number": "",
            "Scholarshipbank": "",
            "UTR Number": "",
        }
        formdata.map(item => {
            if (item.name == "date") {
                formPayload.txnDate = item.defaultValue
            }
            else if (item.name == "Scholarshiptype") {
                formPayload.Scholarshiptype = item.defaultValue
            }
            else if (item.name == "Scholarshipprovider") {
                formPayload.Scholarshipprovider = item.defaultValue
            }
            else if (item.name == "Scholarshipamountreceived") {
                formPayload.Scholarshipamountreceived = Number(item.defaultValue).toFixed(2)
            }
            else if (item.name == "Scholarshipmode") {
                formPayload.Scholarshipmode = item.defaultValue
            }
            else if (item.name == "Account Number") {
                formPayload['Account Number'] = item.defaultValue
            }
            else if (item.name == "Scholarshipbank") {
                formPayload.Scholarshipbank = item.defaultValue
            }
            else if (item.name == "UTR Number") {
                formPayload['UTR Number'] = item.defaultValue
            }
        })
        let demandNoteItem = {}
        this.state.demandNoteDetails.map(deItem => {
            if (deItem['Demand Note ID'] == String(this.state.studentFeeID)) {
                demandNoteItem = JSON.parse(deItem.Item)
            }
        })
        let payload = {
            "displayName": "",
            "transactionType": "",
            "transactionSubType": "",
            "transactionDate": formPayload.txnDate,
            "studentId": demandNoteItem.demandNote.studentId,
            "studentRegId": String(this.state.StudentRegId),
            "studentName": String(this.state.StudentName),
            "pendingAmount": Number(demandNoteItem.pending),
            "studentFeeMap": demandNoteItem.studentFeeMapDetails.displayName,
            "class": demandNoteItem.demandNote.class,
            "academicYear": demandNoteItem.demandNote.academicYear,
            "programPlan": demandNoteItem.demandNote.programPlan,
            "emailCommunicationRefIds": demandNoteItem.demandNote.emailCommunicationRefIds,
            "smsCommunicationRefIds": demandNoteItem.demandNote.smsCommunicationRefIds,
            "status": "",
            "relatedTransactions": this.state.studentFeeID,
            "orgId": this.state.orgId,
            "amount": Number(formPayload.Scholarshipamountreceived),
            "feeTypeCode": demandNoteItem.feeDetails[0].feeTypeCode,
            "scholarshipType": formPayload.Scholarshiptype,
            "dateOfReceipt": formPayload.txnDate,
            "scholarshipProvider": formPayload.Scholarshipprovider,
            "amountReceived": Number(formPayload.Scholarshipamountreceived),
            "modeofPayment": formPayload.Scholarshipmode,
            "receivedBank": formPayload.Scholarshipbank,
            "accNumber": formPayload['Account Number'],
            "utrNumber": formPayload['UTR Number'],
            "amountSanctioned": Number(formPayload.Scholarshipamountreceived),
            "chequeNumber": '',
            "issuedBank": '',
            "branch": '',
            "createdBy": demandNoteItem.demandNote.createdBy
        }
        let headers = {
            'Authorization': this.state.authToken
        };
        console.log(JSON.stringify(payload))
        axios.post(`${this.state.env["zqBaseUri"]}/edu/transactions/scholarships`, payload,
            {
                headers
            })
            .then(res => {
                // let a = this.state.snackbar;
                // a.openNotification = true;
                // a.NotificationMessage = "Scholarship added successfully";
                // a.status = "success";
                this.setState({ activeStep: this.state.activeStep + 1, scholarshipStatus: true }, () => {
                    // setTimeout(() => {
                    //     this.setState({ tableListView: "tableList" })
                    // }, 1000);
                })
                // setTimeout(() => {
                //     a.openNotification = false;
                //     this.setState({ snackbar: a })
                // }, 2000)
                this.resetForm('addNew');
            })
            .catch(err => {
                let a = this.state.snackbar;
                a.openNotification = true;
                a.NotificationMessage = "Failed to add the scholarship..!";
                a.status = "error";

                this.setState({ snackbar: a, activeStep: this.state.activeStep + 1, scholarshipStatus: false }, () => {
                    // setTimeout(() => {
                    //     this.setState({ tableListView: "tableList" })
                    // }, 1000);
                })
                setTimeout(() => {
                    a.openNotification = false;
                    this.setState({ snackbar: a })
                }, 2000)
            })
    }

    uploadFileSection = () => {
        console.log("Upload section");
        this.setState({
            tableListView: "uploadSec",
            activeStepFile: 0,
            upload: false,
            pdfLoaded: 0,
            processLoaded: 0
        });
    }
    onButtonClick = () => {
        if (this.state.pdfLoaded == 0) {
            this.inputFile.current.click();
        }
    }
    onReviewSubmit = async () => {
        //upload file api
        let headers = {
            'Authorization': this.state.authToken
        }
        let formdata = new FormData();
        formdata.append("file", this.state.attachmentFileData);
        let response1 = await axios.post(`${this.state.env["zqBaseUri"]}/edu/uploadScholarships`,
            formdata,
            {
                headers
            })
            .then(res => {
                return true
            })
            .catch(err => {
                return false
            })

        let payload = this.state.reviewScholarshipData
        let response2 = await axios.post(`${this.state.env["zqBaseUri"]}/edu/transactions/bulkScholarships?orgId=${this.state.orgId}&userId=${this.state.userId}`,
            payload,
            {
                headers
            })
            .then(res => {
                return true
            })
            .catch(err => {
                return false
            })

        if (response1 && response2) {
            this.setState({ activeStepFile: 2 })
        }
        else {
            this.setState({ activeStepFile: 1 })
        }
    }

    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    uploadFinish2 = () => {

        this.setState({ tableListView: "tableList" }, () => {
            this.getTableData()
            this.resetForm()
        })
    }

    onDownloadEvent = () => {
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/transactions/scholarships?orgId=${this.state.orgId}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(resp => {
                console.log(resp);
                var createXlxsData = []
                resp.data.data.map(item => {
                    createXlxsData.push({
                        "Id": item.displayName,
                        "Reg Id": item.studentRegId,
                        "Student Name": item.studentName,
                        "Class": item.class,
                        "Academic Year": item.academicYear,
                        "Scholarship Provider": item.data.scholarshipProvider,
                        "Amount Sanctioned": this.formatCurrency(Number(item.amount)),
                        "Amount Received": this.formatCurrency(Number(item.amount)),
                        "Mode of payment": item.data.modeofPayment,
                        "Created On": item.createdAt !== "" ? this.context.dateFormat !== "" ? moment(item.createdAt).format(this.context.dateFormat) : moment(item.createdAt).format('DD/MM/YYYY') : "-",
                        "Status": "Sanctioned",
                    })
                })
                var ws = xlsx.utils.json_to_sheet(createXlxsData);
                var wb = xlsx.utils.book_new();
                xlsx.utils.book_append_sheet(wb, ws, "Scholarship");
                xlsx.writeFile(wb, "Schloarship.xlsx");
                this.setState({ isLoader: false })
            })
            .catch(err => {
                console.log(err, 'err0rrrrr')
                this.setState({ isLoader: false })
            })
    }


    getUploadStep = (stepIndex) => {
        switch (stepIndex) {
            case 0:
                return <React.Fragment>
                    <div className="invoice-custom-table-wrapper invoice-extract-wrap scholarship-wrap">
                        <div className="pdf-extract-wrap">
                            <div className="onboard-card dot-card" onClick={() => this.onButtonClick()}>
                                {this.state.upload ?
                                    <React.Fragment>
                                        {/* {this.state.isPortal ? */}
                                        {/* <div className="file-upload-wrap">
                                                    <p className="file-process-hd" style={{ textAlign: 'center' }}>Fetching Data...</p>
                                                </div> : */}
                                        <div className="file-upload-wrap" onClick={(event) => { event.stopPropagation() }}>
                                            {this.state.pdfLoaded !== 100 ?
                                                <span className="file-loaded-percentage">{this.state.pdfLoaded}%</span>
                                                :
                                                <span className="file-loaded-percentage" style={{
                                                    fontSize: '14px',
                                                    color: "#00b8d9"
                                                }}><i class="fa fa-check" aria-hidden="true"></i></span>}
                                            <p className="file-process-hd">Uploading File({this.state.filename})</p>
                                            <LinearProgress color="primary" variant={"determinate"} value={this.state.pdfLoaded} />
                                            {this.state.processLoaded !== 100 ?
                                                <span className="file-loaded-percentage process-percentage">{this.state.processLoaded}%</span>
                                                :
                                                <span className="file-loaded-percentage process-percentage" style={{
                                                    fontSize: '14px',
                                                    color: "#00b8d9"
                                                }}><i class="fa fa-check" aria-hidden="true"></i></span>}
                                            <p className="file-process-hd">Processing File</p>
                                            <LinearProgress color="primary" variant={"determinate"} value={this.state.processLoaded < 75 ? this.state.processLoaded : 75} />
                                        </div>
                                        {/* } */}
                                    </React.Fragment>
                                    : <div>
                                        <input type='file' id='file' disabled={this.state.pdfLoaded == 0 ? false : true} ref={this.inputFile} style={{ display: 'none' }} onChange={(e) => this.fileSelected(e)} />
                                        <div className="card-third-round  " ><PublishOutlinedIcon className="onboard-icons" /> </div>
                                        <h6 className="onboard-card-header">Upload Excel File</h6>
                                        <p className="onboard-card-title" style={{ color: "#000" }} >Drag and drop file or <span className="browse-content" >browse</span> </p>

                                    </div>}

                            </div>
                        </div>
                    </div>
                </React.Fragment>
            case 1:
                return <React.Fragment>
                    <Button className="send-demand-note-btn" style={{
                        minWidth: 'max-content', position: 'absolute',
                        // right: '6px',
                        // bottom: '6px'
                        right: '50px', top: '112px'
                    }} onClick={() => this.onReviewSubmit()}>Submit</Button>
                    {/* <div className="scholarship-upload">
                        <div className="receive-payment-main student-list-demand-note ">
                            <div className="remove-last-child-table remove-first-child-table scholarship-view-table scholarPreviewtableData">
                                {this.state.reviewScholarshipData.length !== 0 ?
                                    <ZqTable
                                        allSelect={this.allSelect}
                                        data={this.state.reviewScholarshipData}
                                        checked={false}
                                        rowClick={() => { }}
                                    /> : <p className="noprog-txt">{this.state.noProg}</p>}
                        
                                </div>
                            </div>

                        </div> */}
                    < div className="reports-student-fees list-of-students-mainDiv" >
                        <div className="reports-body-section print-hd">
                            <div className="reports-data-print-table">
                                <div className="transaction-review-mainDiv">
                                    <table className="transaction-table-review reports-tableRow-header col-split-td scholarPreviewtableData" >
                                        <thead>
                                            <tr>
                                                {Object.keys(this.state.reviewScholarshipData[0]).map((data, i) => {
                                                    return <th className={'student-fee ' + String(data).replace(' ', '')} key={i + 1}>{data}</th>
                                                })}
                                            </tr>
                                        </thead>
                                        {this.state.reviewScholarshipData.length > 0 ?
                                            <tbody className="studentfee-reports-table">
                                                {this.state.reviewScholarshipData.map((data, i) => {
                                                    return (
                                                        <tr key={i + 1} id={i + 1} className="table-preview" >
                                                            <td className="transaction-vch-type">{data['student registration id']}</td>
                                                            <td className="transaction-vch-num" >{data['student name']}</td>
                                                            <td className="transaction-vch-num" >{data['is consent required?']}</td>
                                                            <td className="transaction-vch-num" >{moment(data['date of receipt']).format('DD/MM/YYYY')}</td>
                                                            <td className="transaction-vch-type">{data['scholarship type']}</td>
                                                            <td className="transaction-vch-type">{data['scholarship provider']}</td>
                                                            <td className="transaction-vch-type">{this.formatCurrency(Number(data['scholarship amount received']))}</td>
                                                            <td className="transaction-vch-type">{data['mode of payment']}</td>
                                                            <td className="transaction-vch-type">{data['dd/cheque number']}</td>
                                                            <td className="transaction-vch-type">{data['cheque/dd issued bank']}</td>
                                                            <td className="transaction-vch-type">{data['branch']}</td>
                                                            <td className="transaction-vch-type">{data['utr number']}</td>
                                                            <td className="transaction-vch-type">{data['institute bank name']}</td>
                                                            <td className="transaction-vch-type">{data['institute bank account number']}</td>
                                                            <td className="transaction-vch-type">{data['transaction id']}</td>
                                                            <td className="transaction-vch-type">{data['status']}</td>
                                                            <td className="transaction-vch-type">{this.formatCurrency(Number(data['Fees']))}</td>
                                                            <td className="transaction-vch-type">{this.formatCurrency(Number(data['Paid']))}</td>
                                                            <td className="transaction-vch-type">{this.formatCurrency(Number(data['Sanctioned']))}</td>
                                                            <td className="transaction-vch-type">{data['Is Final Year']}</td>
                                                            {data['Action'] !== 'adjustRefund' ?
                                                                <td className="transaction-vch-type">{data['Action']}</td>
                                                                : <td className="transaction-particulars">
                                                                    <p>Refund</p>
                                                                    <p >Adjust</p>
                                                                </td>}
                                                            {data['Action'] !== 'adjustRefund' ?
                                                                <td className="transaction-vch-type">{this.formatCurrency(Number(data['Refund']))}</td>
                                                                : <td className="transaction-particulars">
                                                                    <p > {this.formatCurrency(Number(data['Refund'].refund))}</p>
                                                                    <p> {this.formatCurrency(Number(data['Adjust'].refund))}</p>
                                                                </td>}
                                                            {data['Action'] !== 'adjustRefund' ?
                                                                <td className="transaction-vch-type">{this.formatCurrency(Number(data['Adjust']))}</td>
                                                                : <td className="transaction-particulars">
                                                                    <p>{this.formatCurrency(Number(data['Refund'].adjust))}</p>
                                                                    <p >{this.formatCurrency(Number(data['Adjust'].adjust))}</p>
                                                                </td>}
                                                            <td className="transaction-vch-type">{data['Adjust with']}</td>

                                                        </tr>
                                                    )
                                                })}
                                            </tbody> :
                                            <tbody>
                                                <tr>
                                                    {this.state.noData ? <td className="noprog-txt" colSpan={this.state.tableHeader.length}>No data...</td> :
                                                        <td className="noprog-txt" colSpan={this.state.tableHeader.length}>Fetching the data...</td>
                                                    }

                                                </tr></tbody>
                                        }
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                </React.Fragment >
            case 2:
                return <React.Fragment>
                    <div className="receive-payment-main student-list-demand-note" style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                        <div style={{ display: "flex", justifyContent: 'center', alignItems: 'center', textAlign: "center", height: "100%" }}>
                            <div>
                                <img src={success} style={{ width: "25%" }} className="register-success-icon" alt="Add Item" title="Add New item" />
                                <p>Scholarship file has been uploaded successfully</p>

                                <Button variant="contained" type="submit" class="primary-btn form-submit-btn" onClick={this.uploadFinish2}
                                    style={{
                                        height: "40px",
                                        position: 'unset',
                                        marginTop: '20px'
                                    }}
                                >Done</Button>

                            </div>
                        </div>
                    </div>
                </React.Fragment >
            default:
                return null;
        }
    }
    // downloadScholarship = () => {
    //     var link = document.createElement("a");
    //     link.download = "Scholarship Sample.xlsx";
    //     link.href = ScholarshipFile;
    //     link.className = "scholarship-file-anchor"
    //     link.click();
    // }
    onPaginationChange = (page, limit) => {
        // this.setState({ page: page, limit: this.state.limit, listOfStudentData: [] }, () => {
        //     let start = (Number(this.state.page) - 1) * Number(this.state.limit)
        //     let end = Number(start) + Number(this.state.limit)
        //     let invoiceList = this.state.totalInvoices
        //     let invoiceLists = invoiceList.slice(start, end)
        //     this.setState({ listOfStudentData: invoiceLists }, () => {
        //         console.log(this.state.listOfStudentData)
        //     })
        // })
        this.setState({ page: page, limit: limit }, () => {
            this.getTableData()
        })
    }
    render() {
        return (<div className="list-of-students-mainDiv transaction-scholarship-main-div table-head-padding" >
            {this.state.isLoader ? <Loader /> : ""}
            {
                this.state.tableListView == "tableList" ?
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Scholarships</p>
                        </div>
                        <div className="main-navbar-head">
                            <ContainerNavbar containerNav={componentData.containerNav} onAddNew={() => { this.onAddNewScholarship(); }} />
                            <Button className="send-demand-note-btn" style={{ minWidth: 'max-content', position: "relative" }} onClick={this.uploadFileSection}>Upload</Button>
                            {/* <Button className="send-demand-note-btn" style={{ minWidth: 'max-content', position: "relative" }} onClick={this.onDownloadEvent}>Download</Button> */}
                            <Button className="send-demand-note-btn" style={{ minWidth: 'max-content', position: "relative" }}>
                                <a href={ScholarshipFile} className={"scholarship-file-anchor"} download="Scholarship Sample.xlsx">
                                    Download
                                </a></Button>
                            {/* <Button className="send-demand-note-btn" style={{ minWidth: 'max-content', position: "relative" }} onClick={this.downloadScholarship} >Download</Button> */}
                        </div>
                        <div className="remove-last-child-table">
                            {this.state.listOfStudentData.length !== 0 ?
                                <React.Fragment>
                                    <ZqTable
                                        allSelect={this.allSelect}
                                        data={this.state.listOfStudentData}
                                        rowClick={(item) => { this.onPreviewStudentList(item) }}
                                        onRowCheckBox={(item) => { this.onRowClickBoxFun(item) }}
                                        handleActionClick={(item) => { this.actionClickFun(item) }}
                                    />
                                    <PaginationUI
                                        total={this.state.totalRecord}
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        currentPage={this.state.page}
                                    />
                                </React.Fragment> : <p className="noprog-txt">{this.state.noProg}</p>}
                            {/* <p className="noprog-txt">No data..</p>/ */}

                        </div>
                        <div className="pagination-footer-transaction">
                            {/* {this.state.listOfStudentData.length !== 0 ? <PaginationUI onPaginationApi={this.onPaginationChange} totalPages={this.state.totalPages} limit={this.state.limit} total={this.state.totalRecord} /> : null} */}
                        </div>
                    </React.Fragment> :
                    this.state.tableListView == "previewForm" ?
                        <React.Fragment>
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.goBackToList} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title" onClick={this.goBackToList}>| Scholarships | {this.state.previewId}</p>
                            </div>
                            <div className="organisation-table">
                                {componentData.formDataListPreview !== undefined ?
                                    <ZenTabs tabData={componentData.formDataListPreview} className="preview-wrap" cleanData={this.cleanDatas} tabEdit={this.state.preview} cancelViewData={this.cancelViewData} form={componentData.formDataListPreview} value={0} onInputChanges={this.onInputChanges} onFormBtnEvent={(item) => { this.formBtnHandle(item); }} onTabFormSubmit={this.onFormSubmit} handleTabChange={this.handleTabChange} key={0} />
                                    : <p className="noprog-txt">{this.state.noProg}</p>}
                            </div>
                        </React.Fragment> :
                        this.state.tableListView == "addNewList" ?
                            <React.Fragment>
                                <div className="trial-balance-header-title">
                                    <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.goBackToList} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                    <p className="top-header-title" onClick={this.goBackToList}>| New Scholarship</p>
                                </div>
                                <div className="migration-main-div portal-login-div vendor-invoice-upload-wrap pdf-extraction-upload-wrap" style={{ height: "calc(100vh - 100px)" }}>
                                    <div className="migration-header-stepper-section">
                                        <div className="zenqore-stepper-section">
                                            <Steps current={this.state.activeStep} currentStatus="process" vertical={false} >
                                                <Steps.Item title="Fetch Details" />
                                                <Steps.Item title="Student Information" />
                                                <Steps.Item title="Transaction details" />
                                                <Steps.Item title="Confirmation" />
                                            </Steps>
                                        </div>
                                    </div>
                                    <div className="migration-body-content-section" style={{ position: 'relative', backgroundColor: "none !important", height: "calc(100vh - 216px)", overflowY: "auto", paddingBottom: "30px", border: "1px solid #dfe1e6" }}>
                                        {this.getStepContent(this.state.activeStep)}
                                    </div>
                                </div>
                            </React.Fragment> :
                            this.state.tableListView == "uploadSec" ?
                                <React.Fragment>
                                    <div className="trial-balance-header-title">
                                        <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.goBackToList} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                        <p className="top-header-title" onClick={this.goBackToList}>| Upload Scholarship</p>
                                    </div>
                                    <div className="migration-main-div portal-login-div vendor-invoice-upload-wrap pdf-extraction-upload-wrap" style={{ height: "calc(100vh - 100px)" }}>
                                        <div className="migration-header-stepper-section">
                                            <div className="zenqore-stepper-section">
                                                <Steps current={this.state.activeStepFile} currentStatus="process" vertical={false} >
                                                    <Steps.Item title="Upload" />
                                                    <Steps.Item title="Preview" />
                                                    <Steps.Item title="Confirmation" />
                                                </Steps>
                                            </div>
                                        </div>
                                        <div className="migration-body-content-section scholarship-section" style={{ backgroundColor: "none !important", height: "calc(100vh - 216px)", overflowY: "auto", overflowX: 'auto', border: "1px solid #dfe1e6", }}>
                                            {this.getUploadStep(this.state.activeStepFile)}
                                        </div>
                                    </div>
                                </React.Fragment> :
                                null
            }

            <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={2000} onClose={this.closeNotification}>
                <SnackAlert onClose={this.closeNotification}
                    severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
                    {this.state.snackbar.NotificationMessage}
                </SnackAlert>
            </Snackbar>
        </div>
        )
    }
}

export default ScholarshipTransaction;
