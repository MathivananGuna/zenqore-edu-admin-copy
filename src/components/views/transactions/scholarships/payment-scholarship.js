import React, { Component } from 'react';
import PaymentForm from '../../../../utils/payment/payment';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import redCross from '../../../../assets/images/red-cross.png';
import Button from '@material-ui/core/Button';
import PaymentData from './payment-form.json';
import axios from 'axios';
import moment from 'moment';
import { Alert } from 'rsuite';
import '../../../../scss/common-payment.scss';
class Payment extends Component {

    constructor(props) {
        super(props);
        this.state = {
            paymentType: "Scholarship",
            env: JSON.parse(localStorage.getItem('env')),
            email: localStorage.getItem('email'),
            channel: localStorage.getItem('channel'),
            authToken: localStorage.getItem('auth_token'),
            orgId: localStorage.getItem('orgId'),
            orgName: localStorage.getItem('orgName'),
            paymentFormData: PaymentData,
            paymentTypes: ['Scholarship'],
            txnReceiptPaymentBank: '',
            paymentItem: {},
            date: new Date(),
            amount: '0',
            dueAmount: '0',
            paid: '0',
            paidPer: '1%',
            duePer: '99%',
            progressStatus: false,
            cardType: '',
            transactionType: '',
            walletType: '',
            payload: undefined,
            modal: false,
            paymentResponse: false,
            errorOccured: false,
            withoutAttachment: true,
            addAttachment: undefined,
            ViewpaymentDetails: [],
            formChanges: false,
            BankName: '',
            gstin: '',
            zqAccountNo: '',
            txnforBranchID: '',
            studentData: this.props.studentData,
            paymentAmount: 100,
            studentID: this.props.studentID,
            studentFeeID: this.props.studentFeeID,
        }
    }
    componentWillMount() {
        let studentData = this.props.studentData
        console.log(studentData)
    }
    componentDidMount = () => {
        let today = new Date().toDateString()
        let paymentType = this.props.paymentType != "" ? this.props.paymentType : "Cash";
        var PaymentFormData = this.props.PaymentFormData != undefined ? this.props.PaymentFormData[0] : null;
        this.setState({ paymentType: paymentType })
        PaymentData.forEach(task => {
            delete task['defaultValue']
            task['readOnly'] = false

        })
        PaymentData.map((item) => {
            if (item.name === "date") {
                item["defaultValue"] = today;
            }
            if (item.name === "amount") {
                item["defaultValue"] = this.props.paymentAmount + '.00';
                item['required'] = false
            }
        });
        if (PaymentFormData != undefined) {
            let objKeys = Object.keys(PaymentFormData);
            objKeys.map(key => {
                PaymentData.map((item) => {
                    let itemName = item.name != undefined ? item.name.toLowerCase().replace(/ /g, "") : "";
                    let keyName = key.toLowerCase().replace(/ /g, "");
                    if (keyName == itemName) {
                        item["defaultValue"] = PaymentFormData[key];
                        item["readOnly"] = true;
                        item["required"] = false;
                    }
                    if (item.name === "amount") {
                        item["defaultValue"] = PaymentFormData.amount + '.00';
                        item['readOnly'] = true
                        item['type'] = "text"
                    }
                    if (item.name === "date") {
                        item["defaultValue"] = today;
                        item['readOnly'] = true
                    }

                });
            })
        }
        this.setState({ PaymentFormData: PaymentData, paidAmount: this.props.paymentAmount + '.00' })
    }
    // componentDidMount = () => {
    //     this.setState({ paymentFormData: this.props.paymentFormData })
    // }

    onPaymentModeSelection = (type) => {
        console.log(type)
        this.setState({ paymentType: type })
    }

    onPaymentSubmit = (data, formItem, format) => {
        console.log(data)
        this.props.goNextFun(data, formItem, format)
    }
    onPayment = () => {
        this.setState({
            modal: true,
            paymentResponse: false,
            errorOccured: false,
        })
    }
    onInputChanges = (value, item, e, dataS) => {
        this.setState({ formChanges: true })
        let selectedValue = dataS != undefined ? dataS.name == 'date' ? value : item.value : e.target.value;
        let type = dataS != undefined ? dataS.name : item.name
        let bankOptions = []

        if (dataS != undefined) {
            if (dataS.name == 'date') {
                this.setState({ date: new Date(selectedValue) })
            }
            if (dataS.name == 'cardtype') {
                this.setState({ cardType: selectedValue })
            }
            if (dataS.name == 'bankname') {
                this.setState({ txnReceiptPaymentBank: selectedValue, BankName: item.label })
            }
            if (dataS.name == 'type') {
                this.setState({ transactionType: selectedValue })
            }
            if (dataS.name == 'walletType') {
                this.setState({ walletType: selectedValue })
            }
            if (dataS.name == "Scholarshiptype") {
                if (item.itemDetails && item.itemDetails.bankDetails !== {}) {
                    Array(item.itemDetails.bankDetails).map(bankItem => {
                        bankOptions.push({
                            'label': bankItem.bankName,
                            'value': bankItem._id
                        })
                    })
                }
                else bankOptions = []
                this.state.paymentFormData.map((data) => {
                    if (data.name == "Scholarshipprovider") {
                        data['defaultValue'] = item.itemDetails.provider
                        data['readOnly'] = data['readOnly']
                        data['requiredBoolean'] = data['requiredBoolean']
                        data['required'] = data['required']
                        data['validation'] = data['validation']
                    }
                    else if (data.name == "Scholarshipbank") {
                        data['options'] = bankOptions
                        data['readOnly'] = data['readOnly']
                        data['requiredBoolean'] = data['requiredBoolean']
                        data['required'] = data['required']
                        data['validation'] = data['validation']
                        data['defaultValue'] = bankOptions[0].value
                    }
                })
            }
            if (dataS.name == 'Scholarshipmode') {
                let addDeljson = [
                    {
                        "category": "input",
                        "type": "text",
                        "name": "UTR Number",
                        "label": "UTR Number",
                        "class": "input-wrap formalign",
                        "readOnly": false,
                        "required": false,
                        "paymentType": "Scholarship"

                    }
                ]
                if (value == "Bank Transfer") {
                    PaymentData.map(task => {
                        if (task.name == "Account Number") {
                            task['label'] = "Account Number";
                        }
                        if (task.name == "UTR Number") {
                            task['label'] = "UTR Number/Remitter Name";
                            task['required'] = true;
                            task['requiredBoolean'] = true;
                        }
                    })
                }
                else if (value == "cheque") {
                    PaymentData.map((task, i) => {
                        if (task.name == "Account Number") {
                            task['label'] = "Cheque Number";
                        }
                        if (task.name == "UTR Number") {
                            task['label'] = "Remarks";
                            task['required'] = false;
                            task['requiredBoolean'] = false;
                        }
                    })
                
            }
            this.setState({ PaymentFormData: PaymentData }) 
        }
        }
        this.onSetValue(type, selectedValue, true)
        // this.props.onInputChanges(value, item, e, dataS)
    }

    onSetValue = (type, value, focus) => {
        let paymentFormData = this.state.paymentFormData
        this.setState({ paymentFormData: [], paymentType: this.state.paymentType }, () => {
            paymentFormData.map(item => {
                if (this.state.paymentType == item.paymentType) {
                    if (item.name == type) {
                        item['defaultValue'] = value
                        item['readOnly'] = item['readOnly']
                        item['requiredBoolean'] = item['requiredBoolean']
                        item['required'] = item['required']
                        item['validation'] = item['validation']
                        item['focus'] = focus != undefined ? focus : false
                        this.setState({ [item.name]: value })
                    } else if (item['defaultValue'] != undefined) {
                        item['defaultValue'] = item['defaultValue']
                        item['focus'] = false
                        this.setState({ [item.name]: item['defaultValue'] })
                    }
                    else {
                        item['focus'] = false
                    }
                }
            })
            this.setState({ paymentFormData: paymentFormData, paymentType: this.state.paymentType })
        })
    }
    onGoToExpenseList = () => {
        if (this.state.errorOccured) {
            this.setState({
                modal: false,
                paymentResponse: false,
                errorOccured: false
            })
        } else {
            this.setState({ paymentFormData: [] }, () => {
                PaymentData.map(pd => {
                    pd['defaultValue'] = undefined
                    pd['readOnly'] = false
                    pd['validation'] = false
                })
                this.setState({ paymentFormData: PaymentData }, () => {
                    // this.props.onGoToExpenseList()
                    this.props.push('transactions/receive-payment')
                })

            })

        }
    }

    onPaymentRowClick = (item) => {
        console.log('selected item', item)
        let paymentType = ''
        this.state.paymentTypes.map(type => {
            type = type == 'Netbanking' ? 'Bank' : type
            if (String(item.source).includes(type)) {
                paymentType = type
            }
        })
        this.setState({ addAttachment: item, paymentType: paymentType, paymentFormData: [] }, () => {
            PaymentData.map(pd => {
                if (pd.name == 'date') {
                    pd['defaultValue'] = item.createdOn
                }
                if (pd.name == 'amount') {
                    pd['defaultValue'] = item.amount
                }
                if (pd.name == 'transactionNumber') {
                    pd['defaultValue'] = item.txnNo != undefined ? item.txnNo : undefined
                }
                if (pd.name == 'branchname') {
                    pd['defaultValue'] = item.branchName != undefined ? item.branchName : undefined
                }
                if (pd.name == 'bankname') {
                    pd.options.map(bank => {
                        if (bank.label == item.BankName) {
                            pd['defaultValue'] = bank.value != undefined ? bank.value : undefined
                        }
                    })

                }
                if (pd.name == 'remarks') {
                    pd['defaultValue'] = item.remarks != undefined ? item.remarks : undefined
                }
                pd['readOnly'] = true
                pd['validation'] = true
            })
            this.setState({ paymentFormData: PaymentData })
        })
    }
    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    render() {

        return (<React.Fragment>
            <div className="receive-amount-table" style={{ width: "70%" }} >
                <div className="student-txt">
                    <div className='student-details' >
                        <p className="student-paraText"><b className="bold-text">Student Name:</b> {this.props.StudentName}</p>
                        <p className="student-paraText"><b className="bold-text">{`${this.context.reportLabel ? this.context.reportLabel : "Reg. ID"}`}:</b> {this.props.StudentRegId} </p>
                        <p className="student-paraText"><b className="bold-text">Program Plan:</b> {this.props.StudentClass}</p>
                    </div>
                </div>
            </div>
            <div className="payment-tab receipt-payment" style={{ borderTop:"1px solid #dfe1e6"}}>
                <div className="payment-current">
                    <PaymentForm
                        receiptFormData={this.state.paymentFormData}
                        paymentType={this.state.paymentType}
                        paymentTypes={this.state.paymentTypes}
                        onPaymentModeSelection={this.onPaymentModeSelection}
                        onPaymentSubmit={this.onPaymentSubmit}
                        onInputChanges={this.onInputChanges}
                    />
                </div>
            </div>

            <Dialog open={this.state.modal} aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description" className='logout-menu-wrap' >
                <DialogContent>
                    <DialogContentText id="alert-dialog-description" className="logout-text">
                        <div style={{ "background": "transparent", "display": "block" }}>
                            <p className="verfication-txt">Transaction {!this.state.paymentResponse ? 'is updating' : (!this.state.errorOccured ? "updated successfully" : 'updated failed')}  &nbsp; <React.Fragment>{!this.state.paymentResponse ? <span className="spinner"></span> : (this.state.errorOccured ? <img src={redCross} alt="Failed" className="wrong-tick-img"></img> : <i className="fa fa-check fa-lg" style={{ padding: "3px" }}></i>)}</React.Fragment></p>
                        </div>
                    </DialogContentText>
                </DialogContent>
                {this.state.extractData ? null : <DialogActions className="logout-header-btns">

                    {this.state.errorOccured ? <Button className="btns-submit" color="primary" autoFocus onClick={this.onGoToExpenseList}>Cancel</Button> : <Button disabled={!this.state.paymentResponse} className="btns-submit" color="primary" autoFocus onClick={this.onGoToExpenseList}>Ok</Button>}
                </DialogActions>}
            </Dialog>
        </React.Fragment>
        )
    }
}
export default Payment;