import React from 'react';
import '../../../../scss/payment-portal.scss';
import '../../../../scss/portal-stepper.scss';
import '../../../../scss/student-reports.scss';
import '../../../../scss/receive-payment.scss';
import '../../../../scss/student.scss';
import '../../../../scss/setup.scss';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../utils/Table/table-component";
import PaginationUI from "../../../../utils/pagination/pagination";
import Loader from "../../../../utils/loader/loaders";
import LoanDataJson from './scholarship.json';
import ZenTabs from '../../../../components/input/tabs';
import { Steps } from 'rsuite';
import ZenForm from '../../../input/form';
import CircularProgress from '@material-ui/core/CircularProgress';
import PaymentJS from './payment-loans';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

class Loans extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            containerNav: {
                isBack: false,
                name: "List of Scholarships",
                isName: true,
                isSearch: false,
                isSort: false,
                isPrint: true,
                isDownload: true,
                isShare: false,
                isNew: true,
                newName: "New",
                isSubmit: false,
            },
            tablePrintDetails: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 1,
            noProg: "Fetching Data ...",
            PreStudentName: "",
            previewList: false,
            previewListForm: LoanDataJson.formJson,
            searchStudent: false,
            activeStep: 0,
        }
    }
    componentDidMount() {
        let a = [];
        LoanDataJson.tableData.map((data) => {
            a.push({
                "ID": String(data.ID),
                "Reg ID": data['Reg ID'],
                "Student Name": data['Student Name'],
                "Program Plan": data['Program Plan'],
                "Year of Joining": data['Year of Joining'],
                "Loan Provider": data['Loan Provider'],
                "Amount Sanctioned": data['Amount Sanctioned'],
                "Amount Received": data['Amount Received'],
                "Mode of Payment": data['Mode of Payment'],
                "Status": data.Status,
                "Item": JSON.stringify(data),
                "action": [{ "name": "Adjust" }, { "name": "Refund" }]
            })
        })
        console.log(a);
        this.setState({ tablePrintDetails: a })
    }
    onAddNewLoans = (e) => {
        console.log(e)
        this.setState({ searchStudent: true })
    }
    onPreviewLoan = (e) => {
        console.log(e);
        let details = e;
        let previewListData = LoanDataJson.formJson
        Object.keys(previewListData).forEach(tab => {
            previewListData[tab].forEach(item => {
                if (item.name == "ID") {
                    item["defaultValue"] = details['ID'];
                    item["readOnly"] = false;
                    item["required"] = false;
                }
                if (item['name'] == 'RegID') {
                    item['defaultValue'] = details['REG ID']
                    item['readOnly'] = true
                    item['required'] = false
                }
                if (item['name'] == 'StudentName') {
                    item['defaultValue'] = details['STUDENT NAME']
                    item['readOnly'] = true
                    item['required'] = false
                }
                if (item['name'] == 'ProgramPlan') {
                    item['defaultValue'] = details['Program Plan']
                    item['readOnly'] = true
                    item['required'] = false
                }
                if (item['name'] == 'YearOfJoin') {
                    item['defaultValue'] = details['Year of Joining']
                    item['readOnly'] = true
                    item['required'] = false
                }
                else if (item.name == "scholarshiptype") {
                    item["defaultValue"] = 'Private';
                    item["readOnly"] = false;
                    item["required"] = false;
                }
                else if (item.name == "scholarshipprovider") {
                    item["defaultValue"] = details['Scholarship Provider']
                    item["readOnly"] = false;
                    item["required"] = false;
                }
                else if (item.name == "modeofPayment") {
                    item["defaultValue"] = details['Mode of payment'];
                    item["readOnly"] = false;
                    item["required"] = false;
                }
                else if (item.name == "scholarshipamountsanctioned") {
                    item["defaultValue"] = details['Amount Sanctioned'];
                    item["readOnly"] = false;
                    item["required"] = false;
                }
                else if (item.name == "scholarshipamountreceived") {
                    item["defaultValue"] = details['Amount Received'];
                    item["readOnly"] = false;
                    item["required"] = false;
                }
                else if (item.name == "status") {
                    item["defaultValue"] = details['Status'];
                    item["readOnly"] = false;
                    item["required"] = false;
                }
            })
        })
        console.log(previewListData);
        this.setState({ previewList: true, PreStudentName: e.ID, previewListForm: previewListData })
    }
    onPaginationChange = () => { }
    onPreviewLoanList = () => { }
    moveBackTable = () => {
        this.setState({ previewList: false })
    }
    actionClickFun = (e) => {
        console.log(e);
    }
    handleTabChange = () => { }
    allSelect = () => { }
    onSubmit = (e) => {
        this.setState({ activeStep: this.state.activeStep + 1 })
    }
    cancelStudentFun = () => {
        this.setState({ activeStep: this.state.activeStep - 1 })
    }
    confirmStudentFun = () => {
        this.setState({ activeStep: this.state.activeStep + 1 })
    }
    goNextFun1 = () => {
        console.log("trigerred");
        this.setState({ activeStep: this.state.activeStep + 1 })
    }
    onPreviewStudentList1 =() =>{}
    onPreviewStudentList =() =>{}
    getStepContent = (stepIndex) => {
        switch (stepIndex) {
            case 0:
                return <React.Fragment>
                    <div className="receive-payment-main">
                        {this.state.circularLoader ?
                            <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                                <CircularProgress size={24} />
                            </div> : null}
                        <div className="receive-payment-table">
                            <React.Fragment >
                                <div className="payment-form form-width-add">
                                    <ZenForm inputData={LoanDataJson.FetchStudentDetails} onSubmit={this.onSubmit} />
                                </div>
                            </React.Fragment>
                        </div>
                    </div>
                </React.Fragment>
            case 1:
                return (
                    <div className="receive-payment-main student-list-demand-note">
                        <React.Fragment>
                            <div className="student-detail-nxt-btn-div">
                                <button className="send-demand-note-btn-new" onClick={this.confirmStudentFun}>Add</button>
                                <button className="send-demand-note-btn-new" onClick={this.cancelStudentFun}>Cancel</button>
                            </div>
                            <div className="table-section-student-list">
                                {this.state.getParticularStuData !== null ?
                                    < ZqTable
                                        allSelect={this.allSelect}
                                        data={LoanDataJson.StudentDetail}
                                        rowClick={(e, item, index) => { this.onPreviewStudentList(e, item, index) }}
                                        onRowCheckBox={(item, a, b, c) => { this.onPreviewStudentList1(item, a, b, c) }} /> :
                                    null}
                            </div>
                        </React.Fragment>
                    </div>
                )
            case 2:
                return <React.Fragment>
                    <div className="receive-payment-main student-list-demand-note">
                        <PaymentJS goNextFun={this.goNextFun1} />
                    </div>
                </React.Fragment >
            case 3:
                return <React.Fragment>
                    <div className="receive-payment-main student-list-demand-note" style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                        <div className="success-div">
                            <CheckCircleIcon className="check-circle" style={{ fontSize: "40px" }} />
                            <p style={{ marginLeft: "-80px" }}>Scholarship Added Successfully</p>
                        </div>
                    </div>
                </React.Fragment >
            default:
                return null;
        }
    }
    render() {
        return (
            <div className="list-of-students-mainDiv table-head-padding">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.previewList == false && this.state.searchStudent !== true ?
                    <React.Fragment>
                        <React.Fragment>
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Scholarships</p>
                            </div>
                        </React.Fragment>
                        <div className="masters-body-div">
                            <React.Fragment>
                                <ContainerNavbar containerNav={this.state.containerNav} onAddNew={() => { this.onAddNewLoans(); }} />
                            </React.Fragment>
                            <div className="table-wrapper" style={{ marginTop: "10px" }}>
                                {this.state.tablePrintDetails.length == 0 ? <p className="noprog-txt">{this.state.noProg}</p> :
                                    <React.Fragment>
                                        <div className="remove-last-child-table" >
                                            <ZqTable
                                                data={LoanDataJson.tableData}
                                                allSelect={this.allSelect}
                                                rowClick={(item) => { this.onPreviewLoan(item) }}
                                                onRowCheckBox={(item) => { this.allSelect(item) }}
                                                handleActionClick={(item) => { this.actionClickFun(item) }}
                                            />
                                        </div>
                                    </React.Fragment>
                                }
                                {this.state.tablePrintDetails.length !== 0 ? <PaginationUI onPaginationApi={this.onPaginationChange} totalPages={this.state.totalPages} limit={this.state.limit} total={this.state.totalRecord} /> : null}
                            </div>
                        </div>
                    </React.Fragment> :
                    <React.Fragment>
                        {this.state.searchStudent !== true ?
                            <div className="tab-form-wrapper tab-table">
                                <div className="preview-btns">
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| Scholarships | {this.state.PreStudentName}</h6>
                                    </div>
                                </div>
                                <div className="organisation-table">
                                    {this.state.previewList == true ?
                                        <ZenTabs
                                            tabData={this.state.previewListForm}
                                            className="preview-wrap"
                                            cleanData={this.cleanDatas}
                                            tabEdit={this.state.preview}
                                            cancelViewData={this.cancelViewData}
                                            form={this.state.previewListForm}
                                            value={0}
                                            onInputChanges={this.onInputChanges}
                                            onFormBtnEvent={(item) => { this.formBtnHandle(item); }}
                                            onTabFormSubmit={this.onFormSubmit}
                                            handleTabChange={this.handleTabChange}
                                            key={0}
                                        /> : null}
                                </div>
                            </div> : null}
                    </React.Fragment>
                }
                {this.state.searchStudent == true ?
                    <React.Fragment>
                        {this.state.LoaderStatus == true ? <Loader /> : null}
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon title="go back" className="keyboard-back-icon" onClick={this.BackToDemandNoteList} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title" title="go back" onClick={this.BackToDemandNoteList}>| Scholarships</p>
                        </div>
                        <div className="migration-main-div portal-login-div vendor-invoice-upload-wrap pdf-extraction-upload-wrap" style={{ height: "calc(100vh - 100px)" }}>
                            <div className="migration-header-stepper-section">
                                <div className="zenqore-stepper-section">
                                    <Steps current={this.state.activeStep} currentStatus="process" vertical={false} >
                                        <Steps.Item title="Fetch Details" />
                                        <Steps.Item title="Student Information" />
                                        <Steps.Item title="Transaction details" />
                                        <Steps.Item title="Confirmation" />
                                    </Steps>
                                </div>
                            </div>
                            <div className="migration-body-content-section" style={{ backgroundColor: "none !important", height: "calc(100vh - 215px)", overflowY: "auto", paddingBottom: "30px", border: "1px solid #dfe1e6" }}>
                                {this.getStepContent(this.state.activeStep)}
                            </div>
                        </div>
                    </React.Fragment> : null}
            </div>
        )
    }
}
export default Loans;