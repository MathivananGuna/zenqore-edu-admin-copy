import React, { Component } from "react";
import '../../../../scss/student.scss';
import '../../../../scss/setup.scss';
import sendReceiptData from './send-receipt-data.json';
import axios from "axios";
import Loader from "../../../../utils/loader/loaders";
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../utils/Table/table-component";
import PaginationUI from "../../../../utils/pagination/pagination";
import ZenTabs from '../../../../components/input/tabs';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Checkbox from "@material-ui/core/Checkbox";
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import IconButton from "@material-ui/core/IconButton";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import EditIcon from '@material-ui/icons/Edit';
import DoneIcon from '@material-ui/icons/Done';
import CancelSVG from '../../../../assets/icons/action-cancel-icon.svg';
import PrintSVG from '../../../../assets/icons/table-print-icon.svg';
import DownloadSVG from '../../../../assets/icons/table-download-icon.svg';
import ShareSVG from '../../../../assets/icons/table-share-icon.svg';
import CircularProgress from '@material-ui/core/CircularProgress';
import DateFormatContext from '../../../../gigaLayout/context';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
class SendReceipt extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            email: localStorage.getItem('email'),
            LoaderStatus: false,
            containerNav: sendReceiptData.containerNav, 
            containerNavList: sendReceiptData.containerNavList,
            tableResTxt: "Fetching Data...",
            receiptTableData: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 1,
            pageOne: 1,
            limitOne: 10,
            totalRecordOne: 0,
            totalPagesOne: 1,
            allReceiptsListData: [],
            viewType: "sendreceipt",
            disableBtn: true,
            selectedListOfReceipt: [],
            snackbar: { openNotification: false, NotificationMessage: "", status: "success" },
            checkFromParent: false,
            dateOfTableData: {},
            selectedItem: [],
            localCheck: false,
            checked: false,
            sendedList: []
        }
    }
    static contextType = DateFormatContext;
    componentDidMount() {
        this.getAllReceiptSent()
    }
    getAllReceiptSent = () => {
        this.setState({ tableResTxt: "Fetching Data...", LoaderStatus: true }, () => {
            // let getReceiptTableData = [];
            // sendReceiptData.sampleSendReceiptData.map((data) => {
            //     getReceiptTableData.push({
            //         "receipt id": data.ReceiptID,
            //         "reg id": data.RegisterID,
            //         "student name": data.StudentName,
            //         "academic year": data.AcademicYear,
            //         "class/batch": data.batch,
            //         "demand note id": data.DemandNoteId,
            //         "amount": data.Amount,
            //         "date": data.Date,
            //         "transaction id": data.TransactionID,
            //         "Status": data.Status != "pending" ? "Done" : "Pending",
            //         "Item": JSON.stringify(data)
            //     })
            // })
            // this.setState({ receiptTableData: getReceiptTableData, LoaderStatus: false })

            let getReceiptTableData = [];
            let headers = {
                'Authorization': this.state.authToken
            };
            axios.get(`${this.state.env["zqBaseUri"]}/edu/getRecieptList?status=sent&page=${this.state.page}&perPage=${this.state.limit}`, { headers })
                .then(res => {
                    let regId = this.context.reportLabel || "REG ID"
                    let batch = this.context.classLabel || "CLASS/BATCH"
                    console.log(res);
                    if (res.data.data.length == 0) {
                        this.setState({ receiptTableData: [], tableResTxt: 'No Data (send-receipts)', LoaderStatus: false })
                    }
                    else {
                        res.data.data.map((data) => {
                            getReceiptTableData.push({
                                "receipt id": data.displayName,
                                [regId]: data.studentRegId,
                                "student name": data.studentName,
                                "academic year": data.academicYear,
                                [batch]: data.class,
                                "demand note id": data.relatedTransactions[0],
                                "amount": Number(data.amount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' }),
                                "date": new Date(data.updatedAt).toLocaleDateString(),
                                "transaction id": data.paymentTransactionId,
                                "Status": data.recieptstatus != "pending" ? "Done" : "Pending",
                                "Item": JSON.stringify(data)
                            })
                        })
                        this.setState({ receiptTableData: getReceiptTableData, totalRecord: res.data.totalRecord, totalPages: res.data.totalPages, page: res.data.page, LoaderStatus: false })
                    }
                })
                .catch(err => { console.log(err); this.setState({ tableResTxt: 'Error loading data', LoaderStatus: false }) })
        })
    }
    allSelectData = (e, r, f) => {
        this.setState({ sendedList: f })
    }
    RowCheckReceipt = (e, a, b, c) => {
        this.setState({ sendedList: c })
    }
    allSelectDataNew = (e, r, f) => {
        console.log(e, r, f);
        if (r === true) {
            this.setState({ disableBtn: false, selectedListOfReceipt: f })
        }
        else if (r === false) {
            this.setState({ disableBtn: true, selectedListOfReceipt: f })
        }
    }
    onPaginationChange = (page, limit) => { this.setState({ page: page, limit: limit }); this.getAllReceiptSent() };
    onPaginationChangeOne = (page, limit) => { this.setState({ pageOne: page, limitOne: limit, checked: false }); this.onAddNewReceipts() };
    onRowClickReceipt = () => { }
    onAddNewReceipts = () => {
        this.setState({ tableResTxt: "Fetching Data...", getAllReceiptsList: [], LoaderStatus: true, sendedList: [] }, () => {
            // let getAllReceiptsList = [];
            // sendReceiptData.sampleListOfReceipt.map((data) => {
            //     getAllReceiptsList.push({
            //         "reg id": data.RegId,
            //         "student name": data.StudentName,
            //         "academic year": data.AcademicYear,
            //         "class/batch": data.batch,
            //         "demand note id": data.DemandNoteId,
            //         "amount": data.Amount,
            //         "date": data.Date,
            //         "transaction id": data.TransactionID,
            //         "bank statement id": data.BankStatementId,
            //         "Status": "Reconciled",
            //         "Item": JSON.stringify(data)
            //     })
            // })
            // this.tableComponentPrepare(getAllReceiptsList)
            // this.setState({ containerNav: sendReceiptData.containerNavList, viewType: "receiptlist", LoaderStatus: false })


            let headers = {
                'Authorization': this.state.authToken
            };
            axios.get(`${this.state.env["zqBaseUri"]}/edu/getRecieptList?status=pending&page=${this.state.pageOne}&perPage=${this.state.limitOne}`, { headers })
                .then(res => {
                    let regId = this.context.reportLabel || "REG ID"
                    let batch = this.context.classLabel || "CLASS/BATCH"
                    if (res.data.data.length === 0) {
                        this.setState({ tableResTxt: 'No data (Pending-receipts)', LoaderStatus: false })
                    }
                    else {
                        console.log(res);
                        let getAllReceiptsList = [];
                        res.data.data.map((data, i) => {
                            getAllReceiptsList.push({
                                [regId]: data.studentRegId,
                                "student name": data.studentName,
                                "academic year": data.academicYear,
                                [batch]: data.class,
                                "demand note id": data.relatedTransactions[0],
                                "amount": Number(data.amount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' }),
                                "date": new Date(data.updatedAt).toLocaleDateString(),
                                "transaction id": data.paymentTransactionId,
                                "bank statement id": data.paymentTransactionId,
                                "Status": data.reconciliationStatus.charAt(0).toUpperCase() + data.reconciliationStatus.slice(1),
                                "Item": JSON.stringify(data)
                            })
                        })
                        this.tableComponentPrepare(getAllReceiptsList)
                        this.setState({ viewType: "receiptlist", containerNav: sendReceiptData.containerNavList, totalRecordOne: res.data.totalRecord, totalPagesOne: res.data.totalPages, pageOne: res.data.page, LoaderStatus: false })
                    }
                })
                .catch(err => { this.setState({ tableResTxt: 'Error loading data', LoaderStatus: false }) })
        })
    }
    tableComponentPrepare = (tabData) => {
        let data = tabData;
        let tableData = [];
        let tableHd = Object.keys(data[0])
        let showDate = false
        let dateOfTableData = {}
        data.map((item, index) => {
            let trItem = {};
            trItem["checked"] = false;
            trItem["rowId"] = index;

            tableHd.map((hd) => {
                if (hd == "action") {
                    trItem["action"] = {
                        action: false,
                        options: item[hd],
                    };
                } else {
                    trItem[hd] = item[hd];
                }
            });
            tableData.push(trItem);
            if (showDate != undefined) {
                dateOfTableData[item.Date] = dateOfTableData[item.Date] == undefined ? [] : dateOfTableData[item.Date]
                dateOfTableData[item.Date].push(trItem)
            }
        })
        console.log(tableData);
        this.setState({ allReceiptsListData: tableData, dateOfTableData: dateOfTableData });
    }
    handleActionOpenClick = (item, index, hd) => {
        console.log("test");
        let tableData = this.state.allReceiptsListData;
        tableData[index].action.action = !item.action.action;
        this.setState({ tableData: tableData }, () => {
            console.log(this.state.tableData);
        });
    };
    static getDerivedStateFromProps(value, item) {
        if (value.checkFromParent == 'uncheck') {
            item.checked = false
            let tableData = item.tableData;
            tableData.map((task) => {
                task.checked = false;
            });
            return { tableData: tableData, checkFromParent: false }
        }
    }
    onRowCheckBox = (e, row, index) => {
        console.log(e);
        let tableData = this.state.allReceiptsListData;
        tableData[index].checked = !row.checked;
        let selectedItem = [];
        this.setState({ tableData: tableData, selectedItem: selectedItem }, () => {
            tableData.map((item) => {
                if (item.checked) {
                    selectedItem.push(item);
                }
            });
            this.setState({ selectedItem: selectedItem }, () => {
                if (this.state.allReceiptsListData.length == this.state.selectedItem.length) {
                    this.setState({ checked: true });
                } else {
                    this.setState({ checked: false });
                }
            });
            console.log('table selectedItem', this.state.selectedItem)
            console.log(e, row, index, this.state.selectedItem);
        });
    }
    onCheckbox = (e, item, index) => {
        console.log(e, item, index);
        this.setState({ checked: !this.state.checked }, () => {
            let tableData = this.state.allReceiptsListData;
            tableData.map((item) => {
                item.checked = this.state.checked;
            });
            this.setState({ tableData: tableData }, () => {
                let selectedItem = []
                if (this.state.checked) {
                    tableData.map((item) => {
                        if (item.checked) {
                            selectedItem.push(item);
                        }
                    });
                }
                this.setState({ selectedItem: selectedItem }, () => {
                    console.log(this.state.checked, this.state.selectedItem);
                });
            })
        });
    }
    BackToTotalReceipt = () => {
        this.setState({ containerNav: sendReceiptData.containerNav })
        this.getAllReceiptSent()
    }
    changeViewFun = () => {
        this.setState({ viewType: "sendreceipt", containerNav: sendReceiptData.containerNav, disableBtn: true, selectedListOfReceipt: [], checked: false, selectedItem: [], pageOne: 1, limitOne: 10, totalRecordOne: 0, totalPagesOne: 1, })
        this.getAllReceiptSent()
    }
    RowCheckReceiptList = (e, row, index, c) => {
        console.log(c);
        if (c.length > 0) { this.setState({ disableBtn: false, selectedListOfReceipt: c }) }
        else { this.setState({ disableBtn: true, selectedListOfReceipt: c }) }
    }
    finishFunc = () => {
        let snackbarUpdate = { openNotification: true, NotificationMessage: "Send successfully", status: "success", viewType: "" }
        this.setState({ snackbar: snackbarUpdate, LoaderStatus: false, checked: false }, () => {
            let snackbarUpdateNew = { openNotification: false, NotificationMessage: "", status: "" }
            this.getAllReceiptSent()
            setTimeout(() => { this.setState({ snackbar: snackbarUpdateNew, viewType: "sendreceipt", containerNav: sendReceiptData.containerNav, selectedItem: [] }) }, 2500)
        });
    }
    sendReceiptList = () => {
        if (this.state.selectedItem.length !== 0) {
            var a = this.state.selectedItem; var b = []; var countArr = 0;
            this.setState({ LoaderStatus: true })
            let self = this
            let initialInterval = setInterval(() => { myfun(a); this.setState({ selectedItem: b }) }, 2000)
            function myfun(e) {
                if (e === undefined) { }
                else {
                    let newArr = e;
                    if (countArr < newArr.length) {
                        delete newArr[countArr].Status;
                        newArr[countArr]['loader'] = "";
                        b.push(newArr);
                        if (countArr === 0) { }
                        else {
                            delete newArr[countArr - 1].loader;
                            newArr[countArr - 1].Status = "Done";
                            b.push(newArr);
                        }
                        countArr = countArr + 1;
                    }
                    else {
                        self.finishFunc()
                        clearInterval(initialInterval);
                        delete newArr[countArr - 1].loader;
                        newArr[countArr - 1].Status = "Done";
                        b.push(newArr);
                    }
                }
            }
        }
        else {
            alert("Please select some items and click send receipt")
        }
    }



    // let a = [];
    // this.state.selectedItem.map((dataOne) => {
    //     this.state.allReceiptsListData.map((data) => {

    //         if (dataOne['reg id'] == data['reg id']) {
    //             delete data['Status']
    //             data['loader'] = ""
    //             a.push(data)
    //         }
    //         else {
    //             a.push(data)
    //         }
    //     })
    // })
    // let removeDuplicate = [...new Set(a)]
    // this.setState({ allReceiptsListData: removeDuplicate })


    // let snackbarUpdate = { openNotification: true, NotificationMessage: "Send successfully", status: "success", viewType: "" }
    // this.setState({ snackbar: snackbarUpdate }, () => {
    //     let snackbarUpdateNew = { openNotification: false, NotificationMessage: "", status: "" }
    //     setTimeout(() => { this.setState({ snackbar: snackbarUpdateNew, viewType: "sendreceipt", containerNav: sendReceiptData.containerNav }) }, 2500)
    // });
    // }


    // sendReceiptList = () => {
    //     let a = [];
    //     let b = this.state.allReceiptsListData;
    //     b.map((data) => {
    //         this.state.selectedItem.map((dataOne) => {
    //             if (data['reg id'] == dataOne['reg id']) {
    //                 a.push({
    //                     "reg id": data['reg id'],
    //                     "student name": data['student name'],
    //                     "academic year": data['academic year'],
    //                     "class/batch": data['class/batch'],
    //                     "demand note id": data['demand note id'],
    //                     "amount": data['amount'],
    //                     "date": data['date'],
    //                     "transaction id": data['transaction id'],
    //                     "bank statement id": data['bank statement id'],
    //                     "loader": ""
    //                 })
    //             }
    //         })
    //     })
    //     this.setState({ allReceiptsListData: a }, () => {
    //         setTimeout(() => {
    //             let c = [];
    //             this.state.allReceiptsListData.map((data) => {
    //                 if (data.loader !== undefined) {
    //                     c.push({
    //                         "reg id": data['reg id'],
    //                         "student name": data['student name'],
    //                         "academic year": data['academic year'],
    //                         "class/batch": data['class/batch'],
    //                         "demand note id": data['demand note id'],
    //                         "amount": data['amount'],
    //                         "date": data['date'],
    //                         "transaction id": data['transaction id'],
    //                         "bank statement id": data['bank statement id'],
    //                         "Status": "Done"
    //                     })
    //                     this.setState({ allReceiptsListData: c })
    //                 }
    //             })
    //             let snackbarUpdate = { openNotification: true, NotificationMessage: "Send successfully", status: "success", viewType: "" }
    //             this.setState({ snackbar: snackbarUpdate }, () => {
    //                 let snackbarUpdateNew = { openNotification: false, NotificationMessage: "", status: "" }
    //                 setTimeout(() => { this.setState({ snackbar: snackbarUpdateNew, viewType: "sendreceipt", containerNav: sendReceiptData.containerNav }) }, 2500)
    //             });
    //         }, 2000)
    //     })
    // }
    unSelectData = () => { }
    resendItem = () => { }
    render() {
        return (
            <div className="list-of-students-mainDiv">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                <React.Fragment>
                    {this.state.viewType == "sendreceipt" ?
                        <React.Fragment>
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Receipts Manager</p>
                            </div>
                            <div className="masters-body-div">
                                {this.state.containerNav == undefined ? null :
                                    <div className="send-receipt-navbar">
                                        <div className="left-section-div">
                                            <React.Fragment>
                                                <ContainerNavbar containerNav={this.state.containerNav} onAddNew={() => { this.onAddNewReceipts(); }} />
                                            </React.Fragment>
                                        </div>
                                        <div className="right-section-div">
                                            <button onClick={this.state.sendedList.length > 0 ? this.resendItem() : this.unSelectData()}
                                                title={this.state.sendedList.length === 0 ? "Please select list of receipts to resend" : "Click to resend"}
                                                style={{ opacity: this.state.sendedList.length === 0 ? 0.6 : 1, cursor: this.state.sendedList.length === 0 ? "no-drop" : "pointer" }}
                                            > Resend </button>
                                        </div>
                                    </div>
                                }
                            </div>
                            <div className="remove-last-child-table remove-first-child-table">
                                {this.state.receiptTableData.length !== 0 ?
                                    < ZqTable
                                        allSelect={(e, r, f) => { this.allSelectData(e, r, f) }}
                                        data={this.state.receiptTableData}
                                        rowClick={(item) => { this.onRowClickReceipt(item) }}
                                        onRowCheckBox={(e, row, index, a) => { this.RowCheckReceipt(e, row, index, a) }} />
                                    :
                                    <p className="noprog-txt">{this.state.tableResTxt}</p>
                                }
                            </div>
                            <div>
                                {this.state.receiptTableData.length !== 0 ?
                                    <PaginationUI
                                        total={this.state.totalRecord}
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        currentPage={this.state.page} />
                                    :
                                    null
                                }
                            </div>
                        </React.Fragment> :
                        this.state.viewType == "receiptlist" ?
                            <React.Fragment>
                                <div className="tab-form-wrapper tab-table send-receipt">
                                    {/* <div className="trial-balance-header-title">
                                        <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.changeViewFun} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                        <p className="top-header-title" onClick={this.changeViewFun}>| Receipt</p>
                                    </div> */}
                                    <div className="masters-body-div">
                                        {this.state.containerNav == undefined ? null :
                                            <React.Fragment>
                                                <div className="send-receipr-nav">
                                                    <ContainerNavbar containerNav={this.state.containerNavList} />
                                                </div>
                                                <div className="cancel-receipt-btn">
                                                    <button onClick={this.changeViewFun}> Cancel</button>
                                                </div>
                                                <div className="send-receipt-btn">
                                                    <button onClick={this.state.selectedItem.length > 0 ? this.sendReceiptList : this.unSelectData}
                                                        title={this.state.selectedItem.length === 0 ? "Please select reconciled transactions to send receipt" : "Click to send receipt"}
                                                        style={{ opacity: this.state.selectedItem.length === 0 ? 0.6 : 1, cursor: this.state.selectedItem.length === 0 ? "no-drop" : "pointer" }}
                                                    > Send Receipt</button>
                                                </div>
                                            </React.Fragment>
                                        }
                                    </div>
                                    <div className="remove-last-child-table remove-first-child-table">
                                        {/* {this.state.allReceiptsListData.length !== 0 ?
                                            < ZqTable
                                                allSelect={(e, r, f) => { this.allSelectDataNew(e, r, f) }}
                                                data={this.state.allReceiptsListData}
                                                rowClick={(item) => { this.onRowClickReceipt(item) }}
                                                onRowCheckBox={(e, row, index, c) => { this.RowCheckReceiptList(e, row, index, c) }} />
                                            :
                                            <p className="noprog-txt">{this.state.tableResTxt}</p>
                                        } */}
                                        {this.state.allReceiptsListData.length !== 0 ?
                                            <table className="zq-table-wrap" id="table-to-xls">
                                                <thead className="zq-table-heading">
                                                    <tr>
                                                        {Object.keys(this.state.allReceiptsListData[0]).map((hd) => {
                                                            return hd == "checked" ? (
                                                                <th key={hd}>
                                                                    <Checkbox
                                                                        checked={this.state.checkFromParent ? this.state.checkFromParent == "checked" ? true : false : this.state.checked}
                                                                        name="checkedF"
                                                                        onChange={(e) => this.onCheckbox(e, hd, this.state.allReceiptsListData)}
                                                                        indeterminate={!this.state.checked}
                                                                        color="primary"
                                                                    />
                                                                </th>
                                                            ) :
                                                                hd == "loader" ? (
                                                                    <th
                                                                        key={hd}
                                                                        style={{
                                                                            display:
                                                                                hd == "rowId" || hd == "Item" ? "none" : "table-cell",
                                                                        }}
                                                                        className={hd == "action" ? "action-cell" : ""}
                                                                        id={'cell' + hd}
                                                                        onClick={() => this.onSortTable(hd)}
                                                                    >
                                                                        <React.Fragment>
                                                                            <span>Status</span>
                                                                        </React.Fragment>
                                                                    </th>
                                                                ) :
                                                                    (
                                                                        <th
                                                                            key={hd}
                                                                            style={{
                                                                                display:
                                                                                    hd == "rowId" || hd == "Item" ? "none" : "table-cell",
                                                                            }}
                                                                            className={hd == "action" ? "action-cell" : ""}
                                                                            id={'cell' + hd}
                                                                            onClick={() => this.onSortTable(hd)}
                                                                        >
                                                                            {hd == "action" ? "" : <React.Fragment>
                                                                                <span>{hd}</span><span className="sort-icon"><ArrowDropUpIcon className="a-up" /><ArrowDropDownIcon className="a-down" /></span>
                                                                            </React.Fragment>}
                                                                        </th>
                                                                    );
                                                        })}
                                                    </tr>
                                                </thead>
                                                <tbody className="zq-table-body">
                                                    {this.state.allReceiptsListData.map((item, index) => {
                                                        return (
                                                            <tr key={index}>
                                                                {Object.keys(item).map((hd) => {
                                                                    return hd == "checked" ? (
                                                                        <td key={index + hd}>
                                                                            <Checkbox
                                                                                name={index}
                                                                                checked={
                                                                                    item.checked != undefined ? item.checked : false
                                                                                }
                                                                                onChange={(e) => this.onRowCheckBox(e, item, index)}
                                                                                id={index}
                                                                                color="primary"
                                                                            />
                                                                        </td>
                                                                    ) :
                                                                        hd == "loader" ? (
                                                                            <td
                                                                                key={index + hd}
                                                                            >
                                                                                <CircularProgress style={{ height: "20px", width: "20px", marginLeft: "35px", marginTop: "5px" }} />
                                                                            </td>
                                                                        ) :
                                                                            (
                                                                                <td
                                                                                    key={index + hd}
                                                                                    style={{
                                                                                        display:
                                                                                            hd == "rowId" || hd == "Item" || hd == "action"
                                                                                                ? "none"
                                                                                                : "table-cell",
                                                                                        // textAlign: hd.toLowerCase().includes('amount')
                                                                                        //   ? 'right' : 'left'
                                                                                    }}
                                                                                    onClick={(e) => {
                                                                                        e.stopPropagation();
                                                                                        // this.onRowClick(item, index, hd)
                                                                                    }}
                                                                                    className={hd == "action" ? "action-cell" : `${hd}-${item[hd]}`}
                                                                                >
                                                                                    {hd != "action" ? (
                                                                                        <span className={hd == "ID" ? "tableID" : hd == "CTC" ? "tableCTC" : hd == "Total Earnings" ? "tableCTC" : hd == "Total Deductions" ? "tableCTC" : (hd.toLowerCase().includes('amount') ? "tableAmount" : "")}>{item[hd]}</span>
                                                                                    ) :
                                                                                        <div style={{ position: "relative" }}>
                                                                                            <IconButton
                                                                                                aria-label="more"
                                                                                                aria-controls="long-menu"
                                                                                                aria-haspopup="true"
                                                                                                onClick={(e) => {
                                                                                                    e.stopPropagation();
                                                                                                    this.handleActionOpenClick(item, index, hd);
                                                                                                }}
                                                                                            >
                                                                                                <MoreVertIcon />
                                                                                            </IconButton>
                                                                                            {item.action != undefined ? (
                                                                                                <React.Fragment>
                                                                                                    {item.action.action ? (
                                                                                                        <ul className="action-item-wrap">
                                                                                                            {item.action.options.map((option) => (
                                                                                                                <li
                                                                                                                    key={option.name}
                                                                                                                    onClick={(e) => {
                                                                                                                        e.stopPropagation();
                                                                                                                        this.handleActionClick(
                                                                                                                            item,
                                                                                                                            index,
                                                                                                                            hd,
                                                                                                                            option.name
                                                                                                                        );
                                                                                                                    }}
                                                                                                                >
                                                                                                                    {option.icon != undefined ? option.icon == 'edit' ? <span className="action-icon-wrap">
                                                                                                                        <EditIcon />
                                                                                                                    </span> : null : null}
                                                                                                                    {option.icon != undefined ? option.icon == 'accept' ? <span className="action-icon-wrap">
                                                                                                                        <DoneIcon />
                                                                                                                    </span> : null : null}
                                                                                                                    {option.icon != undefined ? option.icon == 'reject' ? <span className="action-icon-wrap">
                                                                                                                        {/* <CloseIcon /> */}
                                                                                                                        <img src={CancelSVG} alt={"Cancel"} className="action-icon-image"></img>
                                                                                                                    </span> : null : null}
                                                                                                                    {option.icon != undefined ? option.icon == 'Print' ? <span className="action-icon-wrap">
                                                                                                                        {/* <PrintIcon /> */}
                                                                                                                        <img src={PrintSVG} alt={"Print"} className="action-icon-image"></img>
                                                                                                                    </span> : null : null}
                                                                                                                    {option.icon != undefined ? option.icon == 'Download' ? <span className="action-icon-wrap">
                                                                                                                        {/* <GetAppIcon /> */}
                                                                                                                        <img src={DownloadSVG} alt={"Download"} className="action-icon-image"></img>
                                                                                                                    </span> : null : null}
                                                                                                                    {option.icon != undefined ? option.icon == 'Share' ? <span className="action-icon-wrap">
                                                                                                                        {/* <ShareIcon /> */}
                                                                                                                        <img src={ShareSVG} alt={"Download"} className="action-icon-image"></img>
                                                                                                                    </span> : null : null}
                                                                                                                    {option.name}
                                                                                                                </li>
                                                                                                            ))}
                                                                                                        </ul>
                                                                                                    ) : null}
                                                                                                </React.Fragment>
                                                                                            ) : null}
                                                                                        </div>
                                                                                    }
                                                                                </td>
                                                                            );
                                                                })}
                                                            </tr>
                                                        );
                                                    })}
                                                </tbody>
                                            </table>
                                            :
                                            <p className="noprog-txt">{this.state.tableResTxt}</p>
                                        }
                                    </div>
                                    <div>
                                        {this.state.allReceiptsListData.length !== 0 ?
                                            <PaginationUI
                                                total={this.state.totalRecord}
                                                onPaginationApi={this.onPaginationChangeOne}
                                                totalPages={this.state.totalPagesOne}
                                                limit={this.state.limitOne}
                                                currentPage={this.state.pageOne} />
                                            :
                                            null
                                        }
                                    </div>
                                </div>
                            </React.Fragment> : null
                    }
                </React.Fragment>
                <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={2500} onClose={this.closeNotification}>
                    <Alert onClose={this.closeNotification}
                        severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
                        {this.state.snackbar.NotificationMessage}
                    </Alert>
                </Snackbar>
            </div>
        )
    }
}
export default SendReceipt;