import React, { Component } from 'react';
import PaymentForm from '../../../../utils/payment/payment';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import redCross from '../../../../assets/images/red-cross.png';
import Button from '@material-ui/core/Button';
import PaymentData from './payment-form.json';
import axios from 'axios';
import moment from 'moment';
import { Alert } from 'rsuite';
import '../../../../scss/common-payment.scss';
import { CircularProgress } from "@material-ui/core";
class Payment extends Component {

    constructor(props) {
        super(props);
        this.state = {
            paymentType: 'Loan',
            env: JSON.parse(localStorage.getItem('env')),
            email: localStorage.getItem('email'),
            channel: localStorage.getItem('channel'),
            authToken: localStorage.getItem('auth_token'),
            orgId: localStorage.getItem('orgId'),
            orgName: localStorage.getItem('orgName'),
            PaymentFormData: PaymentData,
            paymentTypes: ['Loan'],
            txnReceiptPaymentBank: '',
            paymentItem: {},
            date: new Date(),
            amount: '0',
            dueAmount: '0',
            paid: '0',
            paidPer: '1%',
            duePer: '99%',
            progressStatus: false,
            cardType: '',
            transactionType: '',
            walletType: '',
            payload: undefined,
            modal: false,
            paymentResponse: false,
            errorOccured: false,
            withoutAttachment: true,
            addAttachment: undefined,
            ViewpaymentDetails: [],
            formChanges: false,
            BankName: '',
            gstin: '',
            zqAccountNo: '',
            txnforBranchID: '',
            studentData: this.props.studentData,
            paymentAmount: 100,
            studentID: this.props.studentID,
            studentFeeID: this.props.studentFeeID,
            circularLoader: false,
        }
    }
    componentWillMount() {
        let studentData = this.props.studentData
        console.log(studentData)
    }

    componentDidMount = () => {
        this.setState({ PaymentFormData: PaymentData })
    }

    onPaymentModeSelection = (type) => {
        console.log(type)
        this.setState({ paymentType: type })
    }

    onPaymentSubmit = (data, formItem, format) => {
        console.log(data);
        this.props.goNextFun()
    }
    onPayment = () => {
        this.setState({
            modal: true,
            paymentResponse: false,
            errorOccured: false,
        })
        let config = {
            headers: {
                Authorization: this.state.authToken,
                'Content-Type': 'application/json'
            }
        }
        var formdata = new FormData()
        // if (this.state.paymentType !== 'zqMoney') {
        if (this.state.paymentType !== 'ZQ_Money') {
            formdata.append('json', JSON.stringify(this.state.payload));
            formdata.append("file", this.state.file);
            formdata.append("orgId", this.state.orgId);
            
        }
        else {
            let headers = {
                'Authorization': this.state.authToken,
            }
            let MoneyPayload = {
                "payeeName": this.state.email,
                "accNo": this.state.zqAccountNo,
                "ifsc": "YESB0CMSNOC",
                "amount": this.state.amount,
                "type": 'purchaseInvoice',
                "transactionId": "invoiceid",
                "gstId": this.state.gstin,
                "orgId": this.state.orgId,
                "payload": this.state.payload

            }

        }
    }
    onInputChanges = (value, item, e, dataS) => {
        this.setState({ formChanges: true })
        let selectedValue =  dataS != undefined ?  dataS.name == 'date' ? value : item.value : e.target.value;
        let type = dataS != undefined ? dataS.name : item.name
        if (dataS != undefined) {
            if (dataS.name == 'date') {
                this.setState({ date: new Date(selectedValue) })
            }
            if (dataS.name == 'cardtype') {
                this.setState({ cardType: selectedValue })
            }
            if (dataS.name == 'bankname') {
                this.setState({ txnReceiptPaymentBank: selectedValue, BankName: item.label })
            }
            if (dataS.name == 'type') {
                this.setState({ transactionType: selectedValue })
            }

            if (dataS.name == 'walletType') {
                this.setState({ walletType: selectedValue })
            }
        }
        this.onSetValue(type, selectedValue, true)
    }

    onSetValue = (type, value, focus) => {
        let PaymentFormData = this.state.PaymentFormData
        this.setState({ PaymentFormData: [], paymentType: this.state.paymentType }, () => {
            PaymentFormData.map(item => {
                if (this.state.paymentType == item.paymentType) {
                    if (item.name == type) {
                        item['defaultValue'] = value
                        item['validation'] = true
                        item['focus'] = focus != undefined ? focus : false
                        this.setState({ [item.name]: value })
                    } else if (item['defaultValue'] != undefined) {
                        item['defaultValue'] = item['defaultValue']
                        item['focus'] = false
                        this.setState({ [item.name]: item['defaultValue'] })
                    }
                    else {
                        item['focus'] = false
                    }
                }
            })
            this.setState({ PaymentFormData: PaymentFormData, paymentType: this.state.paymentType })
        })
    }
    onGoToExpenseList = () => {
        if (this.state.errorOccured) {
            this.setState({
                modal: false,
                paymentResponse: false,
                errorOccured: false
            })
        } else {
            this.setState({ PaymentFormData: [] }, () => {
                PaymentData.map(pd => {
                    pd['defaultValue'] = undefined
                    pd['readOnly'] = false
                    pd['validation'] = false
                })
                this.setState({ PaymentFormData: PaymentData }, () => {
                    // this.props.onGoToExpenseList()
                    this.props.push('transactions/receive-payment')
                })

            })

        }
    }

    onPaymentRowClick = (item) => {
        console.log('selected item', item)
        let paymentType = ''
        this.state.paymentTypes.map(type => {
            type = type == 'Netbanking' ? 'Bank' : type
            if (String(item.source).includes(type)) {
                paymentType = type
            }
        })
        this.setState({ addAttachment: item, paymentType: paymentType, PaymentFormData: [] }, () => {
            PaymentData.map(pd => {
                if (pd.name == 'date') {
                    pd['defaultValue'] = item.createdOn
                }
                if (pd.name == 'amount') {
                    pd['defaultValue'] = item.amount
                }
                if (pd.name == 'transactionNumber') {
                    pd['defaultValue'] = item.txnNo != undefined ? item.txnNo : undefined
                }
                if (pd.name == 'branchname') {
                    pd['defaultValue'] = item.branchName != undefined ? item.branchName : undefined
                }
                if (pd.name == 'bankname') {
                    pd.options.map(bank => {
                        if (bank.label == item.BankName) {
                            pd['defaultValue'] = bank.value != undefined ? bank.value : undefined
                        }
                    })

                }
                if (pd.name == 'remarks') {
                    pd['defaultValue'] = item.remarks != undefined ? item.remarks : undefined
                }
                pd['readOnly'] = true
                pd['validation'] = true
            })
            this.setState({ PaymentFormData: PaymentData })
        })
    }
    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }

    render() {

        return (<React.Fragment>
             {this.state.circularLoader ?
                <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                    <CircularProgress size={24} />
                </div> : null}
            
            <div className="payment-tab receipt-payment">
                <div className="payment-current">
                  <PaymentForm receiptFormData={this.state.PaymentFormData} paymentType={this.state.paymentType} paymentTypes={this.state.paymentTypes} onPaymentModeSelection={this.onPaymentModeSelection} onPaymentSubmit={this.onPaymentSubmit} onInputChanges={this.onInputChanges} /> 
                </div>

                <Dialog open={this.state.modal} aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description" className='logout-menu-wrap' >
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description" className="logout-text">
                            <div style={{ "background": "transparent", "display": "block" }}>
                                <p className="verfication-txt">Transaction {!this.state.paymentResponse ? 'is updating' : (!this.state.errorOccured ? "updated successfully" : 'updated failed')}  &nbsp; <React.Fragment>{!this.state.paymentResponse ? <span className="spinner"></span> : (this.state.errorOccured ? <img src={redCross} alt="Failed" className="wrong-tick-img"></img> : <i className="fa fa-check fa-lg" style={{ padding: "3px" }}></i>)}</React.Fragment></p>
                            </div>
                        </DialogContentText>
                    </DialogContent>
                    {this.state.extractData ? null : <DialogActions className="logout-header-btns">

                        {this.state.errorOccured ? <Button className="btns-submit" color="primary" autoFocus onClick={this.onGoToExpenseList}>Cancel</Button> : <Button disabled={!this.state.paymentResponse} className="btns-submit" color="primary" autoFocus onClick={this.onGoToExpenseList}>Ok</Button>}
                    </DialogActions>}
                </Dialog>
            </div>
        </React.Fragment>
        )
    }
}
export default Payment;