import React, { Component } from 'react';
import '../../../../scss/payment-portal.scss';
import '../../../../scss/portal-stepper.scss';
import '../../../../scss/student-reports.scss';
import '../../../../scss/receive-payment.scss';
import '../../../../scss/student.scss';
import '../../../../scss/setup.scss';
import '../../../../scss/transaction-scholarship.scss';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../utils/Table/table-component";
import PaginationUI from "../../../../utils/pagination/pagination";
import Loader from "../../../../utils/loader/loaders";
import LoanDataJson from './loan-data.json';
import ZenTabs from '../../../../components/input/tabs';
import { Modal, Icon, Steps } from 'rsuite';
import success from '../../../../assets/icons/zq-success.svg';
import ZenForm from '../../../input/form';
import CircularProgress from '@material-ui/core/CircularProgress';
import PaymentJS from './payment-loans';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
// import LoanTemplate from './LoanTemplate.xlsx';
import LoanTemplate from './LoanNew.xlsx';
import Button from '@material-ui/core/Button';
import PublishOutlinedIcon from '@material-ui/icons/PublishOutlined';
import LinearProgress from '@material-ui/core/LinearProgress';
import * as xlsx from "xlsx";
import DateFormatContext from '../../../../gigaLayout/context';

class Loans extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            containerNav: {
                isBack: false,
                name: "List of Loans",
                isName: true,
                isSearch: false,
                isSort: false,
                isPrint: true,
                isDownload: true,
                isShare: false,
                isNew: true,
                newName: "New",
                isSubmit: false,
                isSelectAppReport: true,
                selectPickerOption: true,
                selectCampusOption: true,
                selectSectionOption:true,
                selectFranchiseOption: false
            },
            tablePrintDetails: [],
            totalPages: 1,
            totalInvoices: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalInvoices1: [],
            noProg: "Fetching Data ...",
            PreStudentName: "",
            previewList: false,
            previewListForm: LoanDataJson.formJson,
            searchStudent: false,
            activeStep: 0,
            tableListView: "tableList",
            previewId: "",
            activeStep: 0,
            activeStepFile: 0,
            upload: false,
            pdfLoaded: 0,
            processLoaded: 0,
            reviewScholarshipData: []
        }

        this.inputFile = React.createRef(this.inputFile)
    }
    static contextType = DateFormatContext;
    // componentDidMount() {
    //     let a = [];
    //     LoanDataJson.tableData.map((data) => {
    //         a.push({
    //             "ID": String(data.ID),
    //             "Reg ID": data['Reg ID'],
    //             "Student Name": data['Student Name'],
    //             "Program Plan": data['Program Plan'],
    //             "Year of Joining": data['Year of Joining'],
    //             "Loan Provider": data['Loan Provider'],
    //             "Amount Sanctioned": data['Amount Sanctioned'],
    //             "Amount Received": data['Amount Received'],
    //             "Mode of Payment": data['Mode of Payment'],
    //             "Status": data.Status,
    //             "Item": JSON.stringify(data),
    //             "action": [{ "name": "Adjust" }, { "name": "Refund" }]
    //         })
    //     })
    //     console.log(a);
    //     this.setState({ tablePrintDetails: a })
    // }
    componentDidMount() {
        let responseArray = LoanDataJson.tableData;
        let totalPages = responseArray.length / this.state.limit
        let start = (Number(this.state.page) - 1) * Number(this.state.limit)
        let end = Number(start) + Number(this.state.limit)
        let totalInvoices = responseArray
        this.setState({ totalInvoices }, () => {
            console.log(start, end)
            let invoiceLists = responseArray.slice(start, end)
            let totalPageSplit = String(totalPages).split('.')
            this.setState({
                tablePrintDetails: invoiceLists,
                totalRecords: totalInvoices.length,
                totalPages: totalPageSplit.length == 2 ? (Number(totalPageSplit['0']) + 1) : totalPageSplit['0']
            })
        })
    }
    onAddNewLoans = (e) => {
        console.log(e)
        this.setState({ searchStudent: true })
    }
    onPreviewLoan = (e) => {
        console.log(e);
        let details = JSON.parse(e.Item);
        let previewListData = LoanDataJson.formJson
        Object.keys(previewListData).forEach(tab => {
            previewListData[tab].forEach(task => {
                if (task['name'] == 'loanID') {
                    task['defaultValue'] = e.ID
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'RegID') {
                    task['label'] = this.context.reportLabel || "Reg ID"
                    task['defaultValue'] = e['Reg ID']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'StudentName') {
                    task['defaultValue'] = e['Student Name']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'ProgramPlan') {
                    task['defaultValue'] = e['Program Plan']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'YearOfJoin') {
                    task['defaultValue'] = e['Year of Joining']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'LoanProvider') {
                    task['defaultValue'] = e['Loan Provider']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'amntSanctioned') {
                    task['defaultValue'] = e['Amount Sanctioned']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'amntReceived') {
                    task['defaultValue'] = e['Amount Received']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'dateOfReceipt') {
                    task['defaultValue'] = details['Date of Receipt']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'modeOfPay') {
                    task['defaultValue'] = e['Mode of Payment']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'payStatus') {
                    task['defaultValue'] = e['Status']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'receivedBank') {
                    task['defaultValue'] = "ICICI"
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'accNumber') {
                    task['defaultValue'] = "1007543245678"
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'utrNum') {
                    task['defaultValue'] = "ICICI832403909"
                    task['readOnly'] = true
                    task['required'] = false
                }
            })
        })
        console.log(previewListData);
        this.setState({ previewList: true, PreStudentName: e.ID, previewListForm: previewListData })
    }
    onPaginationChange = () => { }
    onPreviewLoanList = () => { }
    moveBackTable = () => {
        this.setState({ searchStudent: false })
    }
    goBackFun = () => {
        this.setState({ tableListView: "tableList" })
    }
    actionClickFun = (e) => {
        console.log(e);
    }
    handleTabChange = () => { }
    allSelect = () => { }
    onSubmit = (e) => {
        this.setState({ activeStep: this.state.activeStep + 1 })
    }
    cancelStudentFun = () => {
        this.setState({ activeStep: this.state.activeStep - 1 })
    }
    confirmStudentFun = () => {
        this.setState({ activeStep: this.state.activeStep + 1 })
    }
    goNextFun1 = () => {
        console.log("trigerred");
        this.setState({ activeStep: this.state.activeStep + 1 }, () => {
            setTimeout(() => {
                this.setState({ previewList: false, searchStudent: false })
            }, 1500);
        })
    }
    onPreviewStudentList1 = () => { }
    onPreviewStudentList = () => { }

    excelUpload = (file) => {
        this.setState({ showViewer: true }, () => {
            this.setState({ pdfExtract: true, pdfLoaded: 100, processLoaded: 0 }, () => {
                this.onExtract(file)
            })
        });
    }
    onExtract = (file) => {
        if (this.state.processLoaded < 75) {
            this.setState({ processLoaded: Number(this.state.processLoaded) + 10 }, () => {
                setTimeout(() => {
                    this.onExtract(file)
                }, 100);

            })
        } else {
            this.setState({ processLoaded: 80 }, () => {
                this.readFile(file);
            })
        }
    }
    bytesToSize(bytes) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Byte';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        this.setState({ fileSize: Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i] })
    }
    fileSelected = (ev, i) => {
        let file = ev.target.files
        if (String(file[0].name).includes('.xlsx') && file.length == 1) {
            this.setState({ pdfLoaded: 10 })
            this.setState({ filename: file[0].name, upload: true, attachmentFileData: ev.target.files[0] }, () => {
                this.setState({ pdfLoaded: 25 })
                this.bytesToSize(file[0]['size'])
                this.setState({ pdfName: file[0].name, pdfLoaded: 50 }, () => {
                    setTimeout(() => {
                        this.excelUpload(file[0])
                    }, 100);
                })
            })
        }
        else {
            alert('Please Upload file in xlsx format !..')
        }
    }
    readFile = (file) => {
        let f = file;
        let name = f.name;
        this.setState({ fileName: name });
        const reader = new FileReader();
        reader.onload = (evt) => {
            const source = evt.target.result;
            const wb = xlsx.read(source, { type: "binary" });
            /* Get all Worksheet */
            let sheetData = [];
            let sheetDataItem = {};
            console.log('sheets', wb.Sheets)
            let sheetKeys = Object.keys(wb.Sheets)
            sheetKeys.map(key => {
                let particularSheetKey = Object.keys(wb.Sheets[key])
                if (String(key).toLowerCase() == "loans") {
                    particularSheetKey.map(itemKey => {
                        if (String(itemKey).includes('1')) {
                            sheetData.push(wb.Sheets[key][itemKey]['v'])
                        }
                    })
                }
            })
            console.log('sheetData', sheetData)
            wb.SheetNames.forEach(item => {
                const ws = wb.Sheets[item];
                const data = xlsx.utils.sheet_to_json(ws, { defval: "-", raw: false });
                sheetDataItem[item] = data;
            })
            console.log('sheetDataItem', sheetDataItem)
            this.setState({ activeStepFile: 1, reviewScholarshipData: sheetDataItem['Loans'] })
        };
        reader.readAsBinaryString(f);
        // setTimeout(() => {
        // Alert.info('Please review and upload')
        // }, 1000)
    }
    onButtonClick = () => {
        if (this.state.pdfLoaded == 0) {
            this.inputFile.current.click();
        }
    }
    onReviewSubmit = () => {
        this.setState({ activeStepFile: 2 })
    }
    getStepContent = (stepIndex) => {
        switch (stepIndex) {
            case 0:
                return <React.Fragment>
                    <div className="receive-payment-main">
                        {this.state.circularLoader ?
                            <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                                <CircularProgress size={24} />
                            </div> : null}
                        <div className="receive-payment-table">
                            <React.Fragment >
                                <div className="payment-form form-width-add">
                                    <ZenForm inputData={LoanDataJson.FetchStudentDetails} onSubmit={this.onSubmit} />
                                </div>
                            </React.Fragment>
                        </div>
                    </div>
                </React.Fragment>
            case 1:
                return (
                    <div className="receive-payment-main student-list-demand-note">
                        <React.Fragment>
                            <div className="student-detail-nxt-btn-div">
                                <button className="send-demand-note-btn-new" onClick={this.confirmStudentFun}>Add</button>
                                <button className="send-demand-note-btn-new" onClick={this.cancelStudentFun}>Cancel</button>
                            </div>
                            <div className="table-section-student-list">
                                {this.state.getParticularStuData !== null ?
                                    < ZqTable
                                        allSelect={this.allSelect}
                                        data={LoanDataJson.StudentDetail}
                                        rowClick={(e, item, index) => { this.onPreviewStudentList(e, item, index) }}
                                        onRowCheckBox={(item, a, b, c) => { this.onPreviewStudentList1(item, a, b, c) }} /> :
                                    null}
                            </div>
                        </React.Fragment>
                    </div>
                )
            case 2:
                return <React.Fragment>
                    <div className="receive-payment-main student-list-demand-note">
                        <PaymentJS goNextTable={this.goNextTable1} goNextFun={this.goNextFun1} />
                    </div>
                </React.Fragment >
            case 3:
                return <React.Fragment>
                    <div className="receive-payment-main student-list-demand-note" style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                        <div className="success-div">
                            <CheckCircleIcon className="check-circle" style={{ fontSize: "40px" }} />
                            <p style={{ marginLeft: "-63px" }}>Loan Added Successfully</p>
                        </div>
                    </div>
                </React.Fragment >
            default:
                return null;
        }
    }
    uploadFileSection = () => {
        console.log("Upload section");
        this.setState({
            tableListView: "uploadSec",
            activeStepFile: 0,
            upload: false,
            pdfLoaded: 0,
            processLoaded: 0
        });
    }

    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }

    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: this.state.limit, tablePrintDetails: [] }, () => {
            let start = (Number(this.state.page) - 1) * Number(this.state.limit)
            let end = Number(start) + Number(this.state.limit)
            let invoiceList = this.state.totalInvoices
            let invoiceLists = invoiceList.slice(start, end)
            this.setState({ tablePrintDetails: invoiceLists }, () => {
                console.log(this.state.tablePrintDetails)
            })
        })
    }

    goToloanTable = () => {
        let responseArray = this.state.tablePrintDetails
        let regId = this.context.reportLabel || "Reg ID"
        this.state.reviewScholarshipData.map((item, i) => {
            responseArray.push({

                "ID": `LOAN00${responseArray.length + 1}`,
                [regId]: item['Student Registration Id'],
                "Student Name": item['Student Name'],
                "Program Plan": "2020-Class 1",
                "Year of Joining": "2020",
                "Loan Provider": item['Loan Provider'],
                "Date of Receipt": item['Date Of Receipt'],
                "Amount Sanctioned": this.formatCurrency(Number(item['Loan Amount'])),
                "Amount Received": this.formatCurrency(Number(item['Loan Amount'])),
                "Mode of Payment": item['Mode Of Payment'],
                "Status": item.Status == "-" ? 'Adjusted' : item.Status,
                "action": [
                    {
                        "name": "Refund"
                    },
                    {
                        "name": "Adjust"
                    }
                ],
                'Item': JSON.stringify(item)
            })

        })

        let totalPages = responseArray.length / this.state.limit
        let start = (Number(this.state.page) - 1) * Number(this.state.limit)
        let end = Number(start) + Number(this.state.limit)
        let totalInvoices = responseArray
        this.setState({ totalInvoices }, () => {
            console.log(start, end)
            let invoiceLists = responseArray.slice(start, end)
            let totalPageSplit = String(totalPages).split('.')
            this.setState({
                tablePrintDetails: invoiceLists,
                totalRecords: totalInvoices.length,
                totalPages: totalPageSplit.length == 2 ? (Number(totalPageSplit['0']) + 1) : totalPageSplit['0'],
                tableListView: "tableList",
                activeStep: 0
            })
        })

    }

    goBackToList = () => {
        console.log("clicked")
        this.props.history.goBack();
    }

    getUploadStep = (stepIndex) => {
        switch (stepIndex) {
            case 0:
                return <React.Fragment>
                    <div className="invoice-custom-table-wrapper invoice-extract-wrap scholarship-wrap">
                        <div className="pdf-extract-wrap">
                            <div className="onboard-card dot-card" onClick={() => this.onButtonClick()}>
                                {this.state.upload ?
                                    <React.Fragment>
                                        {/* {this.state.isPortal ? */}
                                        {/* <div className="file-upload-wrap">
                                                    <p className="file-process-hd" style={{ textAlign: 'center' }}>Fetching Data...</p>
                                                </div> : */}
                                        <div className="file-upload-wrap" onClick={(event) => { event.stopPropagation() }}>
                                            {this.state.pdfLoaded !== 100 ?
                                                <span className="file-loaded-percentage">{this.state.pdfLoaded}%</span>
                                                :
                                                <span className="file-loaded-percentage" style={{
                                                    fontSize: '14px',
                                                    color: "#00b8d9"
                                                }}><i class="fa fa-check" aria-hidden="true"></i></span>}
                                            <p className="file-process-hd">Uploading File({this.state.filename})</p>
                                            <LinearProgress color="primary" variant={"determinate"} value={this.state.pdfLoaded} />
                                            {this.state.processLoaded !== 100 ?
                                                <span className="file-loaded-percentage process-percentage">{this.state.processLoaded}%</span>
                                                :
                                                <span className="file-loaded-percentage process-percentage" style={{
                                                    fontSize: '14px',
                                                    color: "#00b8d9"
                                                }}><i class="fa fa-check" aria-hidden="true"></i></span>}
                                            <p className="file-process-hd">Processing File</p>
                                            <LinearProgress color="primary" variant={"determinate"} value={this.state.processLoaded < 75 ? this.state.processLoaded : 75} />
                                        </div>
                                        {/* } */}
                                    </React.Fragment>
                                    : <div>
                                        <input type='file' id='file' disabled={this.state.pdfLoaded == 0 ? false : true} ref={this.inputFile} style={{ display: 'none' }} onChange={(e) => this.fileSelected(e)} />
                                        <div className="card-third-round  " ><PublishOutlinedIcon className="onboard-icons" /> </div>
                                        <h6 className="onboard-card-header">Upload Excel File</h6>
                                        <p className="onboard-card-title" style={{ color: "#000" }} >Drag and drop file or <span className="browse-content" >browse</span> </p>

                                    </div>}

                            </div>
                        </div>
                    </div>
                </React.Fragment>
            case 1:
                return <React.Fragment>
                    <div className="receive-payment-main student-list-demand-note">
                        <div className="remove-last-child-table remove-first-child-table scholarship-view-table">
                            {this.state.reviewScholarshipData.length !== 0 ?
                                <ZqTable
                                    allSelect={this.allSelect}
                                    data={this.state.reviewScholarshipData}
                                    checked={false}
                                    rowClick={() => { }}
                                /> : <p className="noprog-txt">{this.state.noProg}</p>}
                        </div>
                        <Button className="send-demand-note-btn" style={{
                            minWidth: 'max-content', position: 'absolute',
                            right: '6px',
                            bottom: '6px'
                        }} onClick={() => this.onReviewSubmit()}>Submit</Button>
                    </div>
                </React.Fragment >
            case 2:
                return <React.Fragment>
                    <div className="receive-payment-main student-list-demand-note" style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                        <div style={{ display: "flex", justifyContent: 'center', alignItems: 'center', textAlign: "center", height: "100%" }}>
                            <div>
                                <img src={success} style={{ width: "25%" }} className="register-success-icon" alt="Add Item" title="Add New item" />
                                <p>Loan file has been uploaded successfully</p>

                                <Button variant="contained" type="submit" class="primary-btn form-submit-btn" onClick={this.goToloanTable}
                                    style={{
                                        height: "40px",
                                        position: 'unset',
                                        marginTop: '20px'
                                    }}
                                >Done</Button>

                            </div>
                        </div>
                    </div>
                </React.Fragment >
            default:
                return null;
        }
    }
    render() {
        return (
            <div className="list-of-students-mainDiv transaction-scholarship-main-div table-head-padding">
                {this.state.tableListView == "tableList" ?
                    <React.Fragment>
                        {this.state.LoaderStatus == true ? <Loader /> : null}
                        {this.state.previewList == false && this.state.searchStudent !== true ?
                            <React.Fragment>
                                <React.Fragment>
                                    <div className="trial-balance-header-title">
                                        <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                        <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Loans</p>
                                    </div>
                                </React.Fragment>
                                <div className="main-navbar-head">
                                    <ContainerNavbar containerNav={this.state.containerNav} onAddNew={() => { this.onAddNewLoans(); }} />
                                    <Button className="send-demand-note-btn" style={{ minWidth: 'max-content', position: "relative" }} onClick={this.uploadFileSection}>Upload</Button>
                                    <Button className="send-demand-note-btn" style={{ minWidth: 'max-content', position: "relative" }}>
                                        <a href={LoanTemplate} className={"scholarship-file-anchor"} download="Loan Sample.xlsx">
                                            Download
                                 </a>
                                    </Button>
                                </div>

                                <div className="table-wrapper" style={{ marginTop: "10px" }}>
                                    {this.state.tablePrintDetails.length == 0 ? <p className="noprog-txt">{this.state.noProg}</p> :
                                        <React.Fragment>
                                            <div className="remove-last-child-table" >
                                                {/* <ZqTable
                                                    data={this.state.tablePrintDetails}
                                                    allSelect={this.allSelect}
                                                    rowClick={(item) => { this.onPreviewLoan(item) }}
                                                    onRowCheckBox={(item) => { this.allSelect(item) }}
                                                    handleActionClick={(item) => { this.actionClickFun(item) }}
                                                />
                                                <PaginationUI
                                                    total={this.state.totalRecord}
                                                    onPaginationApi={this.onPaginationChange}
                                                    totalPages={this.state.totalPages}
                                                    limit={this.state.limit}
                                                    currentPage={this.state.page}
                                                /> */}
                                                <p className="noprog-txt">No data..</p>


                                            </div>
                                        </React.Fragment>
                                    }
                                    {/* {this.state.tablePrintDetails.length !== 0 ? <PaginationUI onPaginationApi={this.onPaginationChange} totalPages={this.state.totalPages} limit={this.state.limit} total={this.state.totalRecord} /> : null} */}
                                </div>
                                {/* </div> */}
                            </React.Fragment> :
                            <React.Fragment>
                                {this.state.searchStudent !== true ?
                                    <div className="tab-form-wrapper tab-table">
                                        <div className="preview-btns">
                                            <div className="preview-header-wrap">
                                                < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.goBackToList} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                                <h6 className="preview-Id-content" onClick={this.goBackToList}>| Loans| {this.state.PreStudentName}</h6>
                                            </div>
                                        </div>
                                        <div className="organisation-table">
                                            {this.state.previewList == true ?
                                                <ZenTabs
                                                    tabData={this.state.previewListForm}
                                                    className="preview-wrap"
                                                    cleanData={this.cleanDatas}
                                                    tabEdit={this.state.preview}
                                                    cancelViewData={this.cancelViewData}
                                                    form={this.state.previewListForm}
                                                    value={0}
                                                    onInputChanges={this.onInputChanges}
                                                    onFormBtnEvent={(item) => { this.formBtnHandle(item); }}
                                                    onTabFormSubmit={this.onFormSubmit}
                                                    handleTabChange={this.handleTabChange}
                                                    key={0}
                                                /> : null}
                                        </div>
                                    </div> : null}
                            </React.Fragment>
                        } </React.Fragment>
                    : null}
                {this.state.searchStudent == true ?
                    <React.Fragment>
                        {this.state.LoaderStatus == true ? <Loader /> : null}
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon title="go back" className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title" title="go back" onClick={this.moveBackTable}>| Loans</p>
                        </div>
                        <div className="migration-main-div portal-login-div vendor-invoice-upload-wrap pdf-extraction-upload-wrap" style={{ height: "calc(100vh - 100px)" }}>
                            <div className="migration-header-stepper-section">
                                <div className="zenqore-stepper-section">
                                    <Steps current={this.state.activeStep} currentStatus="process" vertical={false} >
                                        <Steps.Item title="Fetch Details" />
                                        <Steps.Item title="Student Information" />
                                        <Steps.Item title="Transaction details" />
                                        <Steps.Item title="Confirmation" />
                                    </Steps>
                                </div>
                            </div>
                            <div className="migration-body-content-section" style={{ backgroundColor: "none !important", height: "calc(100vh - 215px)", overflowY: "auto", paddingBottom: "30px", border: "1px solid #dfe1e6" }}>
                                {this.getStepContent(this.state.activeStep)}
                            </div>
                        </div>
                    </React.Fragment> : null}
                {this.state.tableListView == "previewForm" ?
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.goBackToList} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title" onClick={this.goBackToList}>| Loans | {this.state.previewId}</p>
                        </div>
                        <div className="organisation-table">
                            {/* {componentData.formDataListPreview !== undefined ?
                                        <ZenTabs tabData={componentData.formDataListPreview} className="preview-wrap" cleanData={this.cleanDatas} tabEdit={this.state.preview} cancelViewData={this.cancelViewData} form={componentData.formDataListPreview} value={0} onInputChanges={this.onInputChanges} onFormBtnEvent={(item) => { this.formBtnHandle(item); }} onTabFormSubmit={this.onFormSubmit} handleTabChange={this.handleTabChange} key={0} />
                                        : <p className="noprog-txt">{this.state.noProg}</p>} */}
                        </div>
                    </React.Fragment> : null}
                { this.state.tableListView == "addNewList" ?
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.goBackToList} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title" onClick={this.goBackToList}>| New Loan</p>
                        </div>
                        <div className="migration-main-div portal-login-div vendor-invoice-upload-wrap pdf-extraction-upload-wrap" style={{ height: "calc(100vh - 100px)" }}>
                            <div className="migration-header-stepper-section">
                                <div className="zenqore-stepper-section">
                                    <Steps current={this.state.activeStep} currentStatus="process" vertical={false} >
                                        <Steps.Item title="Fetch Details" />
                                        <Steps.Item title="Transaction details" />
                                        <Steps.Item title="Confirmation" />
                                    </Steps>
                                </div>
                            </div>
                            <div className="migration-body-content-section" style={{ position: 'relative', backgroundColor: "none !important", height: "calc(100vh - 216px)", overflowY: "auto", paddingBottom: "30px", border: "1px solid #dfe1e6" }}>
                                {this.getStepContent(this.state.activeStep)}
                            </div>
                        </div>
                    </React.Fragment> : null}
                {this.state.tableListView == "uploadSec" ?
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.goBackFun} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title" onClick={this.goBackFun}>| Upload Loan</p>
                        </div>
                        <div className="migration-main-div portal-login-div vendor-invoice-upload-wrap pdf-extraction-upload-wrap" style={{ height: "calc(100vh - 100px)" }}>
                            <div className="migration-header-stepper-section">
                                <div className="zenqore-stepper-section">
                                    <Steps current={this.state.activeStepFile} currentStatus="process" vertical={false} >
                                        <Steps.Item title="Upload" />
                                        <Steps.Item title="Preview" />
                                        <Steps.Item title="Confirmation" />
                                    </Steps>
                                </div>
                            </div>
                            <div className="migration-body-content-section" style={{ backgroundColor: "none !important", height: "calc(100vh - 216px)", overflowY: "auto", overflowX: 'hidden', paddingBottom: "30px", border: "1px solid #dfe1e6", position: 'relative' }}>
                                {this.getUploadStep(this.state.activeStepFile)}
                            </div>
                        </div>
                    </React.Fragment> : null}


            </div>

        )
    }
}
export default Loans;