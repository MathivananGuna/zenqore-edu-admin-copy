//importing React
import React, { Component } from 'react'
import { withRouter } from 'react-router-dom';
//importing icons
import { Modal, Icon, Steps } from 'rsuite';
import Button from '@material-ui/core/Button';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import StepConnector from '@material-ui/core/StepConnector';
import { Theme, createStyles } from '@material-ui/core/styles';
import Switch, { SwitchClassKey, SwitchProps } from '@material-ui/core/Switch';
import DownloadSVG from '../../../../assets/icons/table-download-icon.svg';
import { Alert } from 'rsuite';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
//importing loaders
import { CircularProgress } from "@material-ui/core";
import Loader from '../../../../utils/loader/loaders';
//importing styles
import { makeStyles, withStyles } from '@material-ui/core/styles';
import '../../../../scss/receive-payment.scss';
import '../../../../scss/payment-portal.scss';
import '../../../../scss/portal-stepper.scss';
import '../../../../scss/setup.scss';
import '../../../../scss/tabs.scss';
import '../../../../scss/student-reports.scss';
import '../../../../scss/student.scss';
import '../../../../scss/common-table.scss';
//importing json
import DepositForm from './collect-deposit.json';
import previewCollectDeposit from './preview-collect-deposit.json';
import ParentInfo from './parent-info.json';
//importing common components
import ZenForm from '../../../input/form';
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import Payment from '../../../payment/payment';
import PaymentConfirmation from './payment-confirmation';
import PaginationUI from "../../../../utils/pagination/pagination";
import DateFormatter from '../../../date-formatter/date-formatter'
import DateFormatContext from '../../../../gigaLayout/context';

import axios from 'axios';
import moment from 'moment'
import xlsx from 'xlsx';

const AntSwitch = withStyles((theme) =>
    createStyles({
        root: {
            width: 28,
            height: 16,
            padding: 0,
            display: 'flex',
        },
        switchBase: {
            padding: 2,
            color: '#0052CC',
            '&$checked': {
                transform: 'translateX(12px)',
                color: '0052CC',
                '& + $track': {
                    opacity: 1,
                    backgroundColor: theme.palette.common.white,
                    borderColor: `1px solid ${theme.palette.grey[500]}`,
                },
            },
        },
        thumb: {
            width: 12,
            height: 12,
            boxShadow: 'none',
        },
        track: {
            border: `1px solid ${theme.palette.grey[500]}`,
            borderRadius: 16 / 2,
            opacity: 1,
            backgroundColor: theme.palette.common.white,
        },
        checked: {},
    }),
)(Switch);

class CollectDeposit extends Component {
    constructor(props) {
        super(props)
        this.state = {
            activeStep: 0,
            steps: ['Enter Details', 'Payment Method', 'Confirmation'],
            nextLoader: false,
            circularLoader: false,
            depositForm: false,
            depositFormData: DepositForm.FetchStudentDetails,
            cashFormData: ParentInfo.CashPayment,
            cardFormData: ParentInfo.CardPayment,
            isNewDeposit: false,
            containerNav: {
                isBack: false,
                name: "List of Deposit",
                isName: true,
                isSearch: false,
                isSort: false,
                isPrint: false,
                isDownload: false,
                isShare: false,
                isNew: true,
                newName: "New",
                isSubmit: false,
            },
            paymentAmount: "0.00",
            studentFeeID: [],
            studentData: [],
            guardianDetails: [],
            feeTypeLists: [],
            paymentType: '',
            studentID: '',
            StudentName: '',
            StudentClass: '',
            StudentRegId: '',
            currencyData: '',
            forex: '',
            paymentAmountForex: '',
            closePreview: true,
            disablePayment: true,
            totalFee: 0,
            paidFee: 0,
            pendingFee: 0,
            paymentID: '',
            receiptID: '',
            checkPaymentStatus: false,
            paidAmount: '',
            razorPayRes: false,
            tableHeader: ["ID", "REG ID", "STUDENT NAME", "ACADEMIC YEAR", "CLASS/BATCH", "DESCRIPTION", "TOTAL FEES", "PAID", "BALANCE", "PAID ON", "TXN ID", "REFUND", "STATUS"],
            printReportArr: [],
            totalPreviewList: [],
            previewListForm: previewCollectDeposit.formJson,
            previewName: "",
            isPreview: false,
            activeTab: 0,
            tabData: ['General', 'Fee Details', 'Transaction Details'],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 1,
            iseditable: true,
            env: JSON.parse(localStorage.getItem('env')),
            orgId: localStorage.getItem("orgId"),
            authToken: localStorage.getItem("auth_token"),
        }
    }
    static contextType = DateFormatContext;
    componentDidMount() {
        let data = [
            // {
            //     "displayName": "DP_2020-21_003",
            //     "studentName": "Abdullah Shareef",
            //     "regId": "1HK17IS001",
            //     "academicYear": "2017-18",
            //     "classBatch": "2017-Information Science",
            //     "DemandId": "",
            //     "refundAmount": 0,
            //     "description": [
            //         {
            //             "name": "Tuition Fee ",
            //             "due": 130000,
            //             "paid": 5000,
            //             "paidDate": "16/02/2021",
            //             "balance": 115000,
            //             "status": "Pending",
            //             "txnId": "TXN/2021/052961"
            //         }
            //     ],
            //     "paymentDetails": {
            //         "_id": "602b6a41c421c669d3b79c25",
            //         "feesLedgerIds": [
            //             "602b6a41c421c64007b79c26"
            //         ],
            //         "emailCommunicationRefIds": [],
            //         "smsCommunicationRefIds": [],
            //         "relatedTransactions": [],
            //         "softwareReconciled": false,
            //         "currency": "INR",
            //         "exchangeRate": 1,
            //         "displayName": "RCPT_2020-21_002",
            //         "transactionDate": "2021-02-16T06:46:19.000Z",
            //         "transactionType": "eduFees",
            //         "transactionSubType": "feePayment",
            //         "studentId": "6006c7ec9568651a2866eb8a",
            //         "studentName": "Abdullah Shareef",
            //         "class": "2017-Information Science",
            //         "academicYear": "2017-18",
            //         "amount": 5000,
            //         "studentRegId": "1HK17IS001",
            //         "receiptNo": "RCPT_2020-21_002",
            //         "programPlan": "6006c7d59568651a2866ea01",
            //         "data": {
            //             "orgId": "5fe0667d8c36be3698b32ca4",
            //             "displayName": "RCPT_2020-21_002",
            //             "transactionType": "eduFees",
            //             "transactionSubType": "feePayment",
            //             "mode": "cash",
            //             "method": "otc",
            //             "modeDetails": {
            //                 "netBankingType": null,
            //                 "walletType": null,
            //                 "instrumentNo": null,
            //                 "instrumentDate": "Tue Feb 16 2021 12:16:19 GMT+0530 (India Standard Time)",
            //                 "bankName": null,
            //                 "cardDetails": {
            //                     "cardType": null,
            //                     "nameOnCard": null,
            //                     "cardNumber": null
            //                 },
            //                 "transactionId": "TXN/2021/052961",
            //                 "remarks": null
            //             },
            //             "feesBreakUp": [
            //                 {
            //                     "feeTypeId": "6006c7d59568651a2866e9fd",
            //                     "feeType": "Tuition Fee ",
            //                     "amount": "5000.00",
            //                     "feeTypeCode": "FT001"
            //                 }
            //             ]
            //         },
            //         "paymentTransactionId": "TXN/2021/052961",
            //         "createdAt": "2021-02-16T06:46:25.181Z",
            //         "updatedAt": "2021-02-16T06:46:25.190Z",
            //         "__v": 0,
            //         "status": "Partial"
            //     }
            // }
        ]
        this.setState({ printReportArr: data })
    }
    changetoStep = (stepperNo, active) => {
        let step = active == 3 ? 0 : stepperNo
        switch (step) {
            case 0:
                return (<React.Fragment>
                    {this.setState({ activeStep: 0, depositForm: false, paymentAmount: "0.00", totalAmount: 0, disablePayment: true })}
                    {
                        this.state.cardFormData.map((data) => {
                            if (data.name == "cardBtn") {
                                data['label'] = 'Confirm Payment'
                            }
                        })
                    }
                    {
                        this.state.cashFormData.map((data) => {
                            if (data.name == "cashBtn") {
                                data['label'] = 'Confirm Payment'
                            }
                        })
                    }
                </React.Fragment>)
            case 1:
                if (stepperNo < active) return (<React.Fragment>
                    {this.setState({ activeStep: 0, paymentAmount: '0.00', totalAmount: 0, disablePayment: true })}
                    {
                        this.state.cardFormData.map((data) => {
                            if (data.name == "cardBtn") {
                                data['label'] = 'Confirm Payment'
                            }
                        })
                    }
                    {
                        this.state.cashFormData.map((data) => {
                            if (data.name == "cashBtn") {
                                data['label'] = 'Confirm Payment'
                            }
                        })
                    }
                </React.Fragment>)
                else if (stepperNo + 1 == active) return (<React.Fragment>
                    {this.setState({ activeStep: 1, paymentAmount: '0.00', disablePayment: true })}
                    {
                        this.state.cardFormData.map((data) => {
                            if (data.name == "cardBtn") {
                                data['label'] = 'Confirm Payment'
                            }
                        })
                    }
                    {
                        this.state.cashFormData.map((data) => {
                            if (data.name == "cashBtn") {
                                data['label'] = 'Confirm Payment'
                            }
                        })
                    }
                </React.Fragment>)
                else { }
        }
    }
    goNextFun1 = (nextProps) => {
        let txnNumber = nextProps.txnNumber;
        let receiptID = nextProps.receiptID;
        let checkPaymentStatus = nextProps.checkPaymentStatus
        let paidAmount = nextProps.paidAmount
        this.setState({ paymentID: txnNumber, receiptID: receiptID, checkPaymentStatus: checkPaymentStatus, paidAmount: paidAmount, razorPayRes: true });
        this.setState({ activeStep: this.state.activeStep + 1 })
    }
    goNextErrFun1 = () => { }
    paymentTypePage = () => {
        let { activeStep } = this.state
        this.setState({ activeStep: activeStep + 1 })
    }
    handleAmtChange = (e, text) => {
        this.setState({ paymentAmount: e.target.value }, () => {
            if (Number(this.state.paymentAmount) > 0) {
                this.setState({ disablePayment: false })
            } else this.setState({ disablePayment: true })
        })
    }
    getStepContent = (stepIndex) => {
        switch (stepIndex) {
            case 0: return (
                <React.Fragment>
                    <div className="receive-payment-main">
                        {this.state.circularLoader ?
                            <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                                <CircularProgress size={24} />
                            </div> : null}
                        {this.state.depositForm == false ?
                            <div className="receive-payment-table" style={{ position: "relative", top: "25vh" }}>
                                {this.state.circularLoader == false ?
                                    <React.Fragment >
                                        <div className="payment-form" style={{ width: "24%", position: "relative", margin: "0 auto" }}>
                                            <ZenForm inputData={this.state.depositFormData} onSubmit={this.onSubmit} />
                                        </div>
                                    </React.Fragment>
                                    : null}
                            </div> :
                            <div className="receive-amount-table">
                                {this.state.circularLoader == false ?
                                    <React.Fragment>
                                        <div className="student-txt student-details-wrapper">
                                            <p style={{ position: 'absolute', left: '10%', top: '10px' }}>Date : {moment().format('DD/MM/YYYY')}</p>
                                            <h3 className="student-details-header" style={{ paddingBottom: "20px" }}>Deposit Details</h3>
                                            {this.state.closePreview ? <Button className={this.state.disablePayment == true ? "payment-method-btn colordisablebtn" : "payment-method-btn"} onClick={this.paymentTypePage} disabled={this.state.disablePayment == true}>Next</Button> : null}

                                            <div className='student-details-recieveTableText'>
                                                <div className='student-details-recieveTableTextHeader'>
                                                    <p className=" recieveTableTextlast"><b className="bold-text">Student Reg. ID : </b> {this.state.StudentRegId}</p>
                                                </div>
                                                <div className='student-details-recieveTableTextHeader' >
                                                    <p className=" recieveTableTextlast" ><b className="bold-text">Student Name : </b> {this.state.StudentName}</p>
                                                </div>
                                                <div className='student-details-recieveTableTextHeader'>
                                                    <p className=" recieveTableTextlast"><b className="bold-text">Student Class : </b> {this.state.StudentClass}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div style={{ paddingTop: '10px', paddingBottom: "20px" }}>
                                            <table className="receive-pay-tableFormat">
                                                <thead className="receive-pay-tableFormatHead">
                                                    <th style={{ width: '7%' }}>No.</th>
                                                    <th>Particulars</th>
                                                    <th style={{ width: '30%' }}>Amount (₹)</th>
                                                </thead>
                                                <tbody>
                                                    {/* {this.state.feeTypeLists.map((item, idx) => {
                                                        return <tr>
                                                            <td className="receive-pay-tableFormatItem" style={{ textAlign: 'center' }}>{idx + 1}</td>
                                                            <td className="receive-pay-tableFormatItem" >{item.feeType}</td>
                                                            <td className="receive-pay-tableFormatAmountInput">
                                                                {this.state.iseditable ?
                                                                    <input type="text" className="feeAmount-input"
                                                                        onChange={(e) => { this.handleAmtChange(e, item, 'amount') }}
                                                                        value={item.amount}>
                                                                    </input> : <span>{item.amount}</span>}
                                                            </td>
                                                        </tr>

                                                    })} */}
                                                    <tr>
                                                        <td className="receive-pay-tableFormatItem" style={{ textAlign: 'center' }}>1</td>
                                                        <td className="receive-pay-tableFormatItem" >Deposit Amount</td>
                                                        <td className="receive-pay-tableFormatAmountInput">
                                                            <input type="text" className="feeAmount-input"
                                                                onChange={(e) => { this.handleAmtChange(e, 'amount') }}
                                                                value={this.state.paymentAmount}>
                                                            </input>
                                                            {/* {this.state.iseditable ?
                                                                <input type="text" className="feeAmount-input"
                                                                    onChange={(e) => { this.handleAmtChange(e, item, 'amount') }}
                                                                    value={item.amount}>
                                                                </input> : <span>{item.amount}</span>} */}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td style={{ textAlign: 'right', paddingRight: 20, fontSize: 16 }} >
                                                            <b>Total (INR)</b>
                                                        </td>
                                                        <td className="totalAmount-payment">{Number(this.state.paymentAmount).toFixed(2)}</td>

                                                    </tr>
                                                    {/* {this.state.currencyData == 'USD' ?
                                                        <tr>
                                                            <td></td>
                                                            <td style={{ textAlign: 'right', paddingRight: 20, fontSize: 16 }} >
                                                                <b>Total (USD)</b>
                                                                <p style={{ fontSize: "12px" }}> @Exchange Rate : {this.state.forex}</p>
                                                            </td>
                                                            <td className="totalAmount-payment">{Number(this.state.paymentAmountForex).toFixed(2)}</td>

                                                        </tr>
                                                        : null} */}
                                                </tbody>
                                            </table>
                                        </div>

                                        {this.state.nextLoader ?
                                            <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                                                <CircularProgress size={24} />
                                            </div> : null}
                                    </React.Fragment>
                                    : null}
                            </div>
                        }
                    </div>
                </React.Fragment>
            )
            case 1: return (
                <React.Fragment>
                    <div className="receive-payment-main">
                        <div className="payment-method payment-method-wrap">
                            <Payment paymentAmount={Number(this.state.paymentAmount)} studentFeeID={this.state.studentFeeID} studentData={this.state.studentData} guardianDetails={this.state.guardianDetails} paymentType={this.state.paymentType} studentID={this.state.studentID} StudentRegId={this.state.StudentRegId} StudentName={this.state.StudentName} StudentClass={this.state.StudentClass} feeTypeLists={this.state.feeTypeLists} goNextFun={this.goNextFun1} onCardSubmit={this.onCardSubmit} onCashSubmit={this.onCashSubmit} goNextErrFun={this.goNextErrFun1} endPoint={"deposits"} />
                        </div>
                    </div>
                </React.Fragment>
            )
            case 2: return (
                <PaymentConfirmation isRazorPay={this.state.isRazorPay} checkPaymentStatus={this.state.checkPaymentStatus} paymentAmount={this.state.paymentAmount} paidAmount={this.state.paidAmount} receiptID={this.state.receiptID} finishProcess={this.finishProcess} paymentID={this.state.paymentID} studentName={this.state.StudentName} />
            )
        }
    }
    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    onSubmit = (data, item, format) => {
        this.setState({ circularLoader: true });
        let studentID = data[0].value;
        let headers = {
            'Authorization': this.state.authToken
        };
        axios.get(`${this.state.env['zqBaseUri']}/edu/feesDetails/${studentID}?orgId=${this.state.orgId}`, { headers })
            .then(res => {
                console.log(res)
                let data = res.data
                let studentDetails = data.studentDetails
                // let demandNoteDetails = data.demandNoteDetails[0]
                let partialAmount = data.partial == null ? true : data.partial
                let feeTypes = []
                let feesBreakUp = data.feesBreakUp;
                feesBreakUp.map(feeElt => {
                    feeTypes.push({
                        'feeTypeId': feeElt['_id'],
                        'feeType': feeElt.title,
                        'amount': Number(feeElt.pending).toFixed(2),
                        'feeTypeCode': feeElt.feeTypeCode
                    })
                })

                this.setState({
                    feeTypeLists: feeTypes,
                    studentData: data,
                    // academicYear: data.programPlanDetails.academicYear,
                    // studentID: demandNoteDetails.studentId,
                    StudentName: studentDetails.firstName + " " + studentDetails.lastName,
                    StudentClass: data.programPlanDetails.title,
                    StudentRegId: studentDetails.regId,
                    guardianDetails: data.guardianDetails,
                    forex: studentDetails.FOREX,
                    currencyData: studentDetails.currency,
                    totalFee: data.totalAmount,
                    paidFee: data.paid,
                    pendingFee: data.pending,
                    // typeReceipt: data.receiptStatus == 'immediately' ? 'receipt' : 'afterReconcilation',
                    // iseditable: partialAmount
                });
                setTimeout(() => {
                    this.setState({ circularLoader: false, depositForm: true });
                    // this.amountNetCal();
                    // this.showStudentInformation(data);
                }, 2000)
            }).catch(err => {
                this.setState({ circularLoader: false, depositForm: false })
            })
    }
    handleNewDeposit = () => {
        this.setState({ isNewDeposit: true, activeStep: 0, depositForm: false })
    }
    onDownloadEvent = () => {
    }
    finishProcess = () => {
        this.setState({ nextLoader: true });
        setTimeout(() => {
            this.setState({
                activeStep: 0,
                paymentAmount: '0.00',
                depositForm: false,
                isNewDeposit: false,
                disablePayment: true,
                StudentName: '',
                StudentClass: '',
                StudentRegId: '',
                parentName: '',
                parentMobile: '',
                parentEmail: '',
                demandNoteID: '',
                paymentID: '',
                paymentType: '',
                nextLoader: false,
                selectedMode: 'Pay with Cash',
                //feecollection report
                isReceivePayment: false
            }, () => {
                // this.onDataCall()
            })
        }, 1000)
    }
    previewRow = (data) => {
        console.log("row clicked", data);
        this.setState({ totalPreviewList: data })
        let details = data
        let feesBreakup = details['description'][0]
        console.log(feesBreakup)
        let previewListData = previewCollectDeposit.formJson
        Object.keys(previewListData).forEach(tab => {
            previewListData[tab].forEach(task => {
                if (task['name'] == 'demandNoteID') {
                    task['defaultValue'] = data["displayName"]
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'RegID') {
                    task['label'] = this.context.reportLabel || "Reg ID"
                    task['defaultValue'] = details["regId"]
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'StudentName') {
                    task['defaultValue'] = details['studentName']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'ProgramPlan') {
                    task['label'] = this.context.classLabel || "Class/Batch"
                    task['defaultValue'] = details['classBatch']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'academicYear') {
                    task['defaultValue'] = details['academicYear']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'feeTypeCode') {
                    task['defaultValue'] = feesBreakup['name']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'totalAmount') {
                    task['defaultValue'] = feesBreakup['due']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'paidAmount') {
                    task['defaultValue'] = feesBreakup['paid']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'pendingAmount') {
                    task['defaultValue'] = feesBreakup['balance']
                    task['readOnly'] = true
                    task['required'] = false
                }
            })
        })
        this.setState({ isPreview: true, previewName: data['displayName'], previewListForm: previewListData })
    }
    tabChangeInternal = (e, value) => { console.log(value); this.setState({ activeTab: value }) }
    onPaginationChange = () => { }
    handleBack = () => {
        this.setState({ isNewDeposit: false, activeStep: 0, depositForm: false, paymentAmount: "0.00", totalAmount: 0, disablePayment: true })
    }

    render() {
        const ColorlibConnector = withStyles({
            alternativeLabel: { top: 22 },
            active: { '& $line': { backgroundColor: "#1359C1", marginTop: '-10px', transitionDelay: '2s' }, },
            completed: { '& $line': { backgroundColor: '#36B37E', marginTop: '-10px' }, },
            line: { height: 2, border: 0, backgroundColor: '#ccc', borderRadius: 1, marginTop: '-10px' },
        })(StepConnector);
        return (
            <React.Fragment>
                {this.state.isNewDeposit ?
                    <React.Fragment>
                        <div className="refund-mainDiv">
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.handleBack} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                <p className="top-header-title" onClick={this.handleBack} >| Collect Deposit</p>
                            </div>
                            <div className="migration-main-div portal-login-div vendor-invoice-upload-wrap pdf-extraction-upload-wrap" style={{ height: "calc(100vh - 100px)" }}>
                                <div className="migration-header-stepper-section">
                                    <div className="zenqore-stepper-section">
                                        <Steps current={this.state.activeStep} currentStatus="process" vertical={false} >
                                            <Steps.Item title="Enter Details" onClick={() => { this.changetoStep(0, this.state.activeStep) }} style={{ cursor: "pointer" }} />
                                            <Steps.Item title="Payment Method" onClick={() => { this.changetoStep(1, this.state.activeStep) }} style={{ cursor: "pointer" }} />
                                            <Steps.Item title="Confirmation" style={{ cursor: "initial" }} />
                                        </Steps>
                                    </div>
                                </div>
                                <div className="migration-body-content-section" style={{ backgroundColor: "#ffffff !important", height: "calc(100vh - 215px)", overflowY: "auto", border: "1px solid #dfe1e6" }}>
                                    {this.getStepContent(this.state.activeStep)}
                                </div>
                            </div>
                        </div>
                    </React.Fragment> :
                    //list, preview
                    <React.Fragment>
                        {this.state.isPreview ?
                            <React.Fragment>
                                <div className="reports-student-fees list-of-students-mainDiv table-head-padding">
                                    <div className="tab-form-wrapper tab-table">
                                        <div className="preview-btns">
                                            <div className="preview-header-wrap">
                                                < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.setState({ isNewDeposit: false, isPreview: false, activeTab: 0 }) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                                <h6 className="preview-Id-content" onClick={() => { this.setState({ isNewDeposit: false, isPreview: false, activeStep: 0, activeTab: 0 }) }}>| Collect Deposit | {this.state.previewName}</h6>
                                            </div>
                                        </div>
                                        <div className="organisation-table student-submit-none">
                                            {/* <div className="form-new-button-list">
                                                <button className="finish-button-demand" onClick={() => { this.setState({ isReceivePayment: false, isPreview: false, activeTab: 0 }) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }}>Done</button>
                                                <img src={DownloadSVG} alt="Download" className="material-searchIcon"></img>
                                            </div> */}
                                            <div className="tab-wrapper">
                                                <Tabs
                                                    value={this.state.activeTab}
                                                    indicatorColor="primary"
                                                    textColor="primary"
                                                    onChange={this.tabChangeInternal}
                                                >
                                                    {this.state.tabData.map((tab, tabIndex) => {
                                                        return <Tab label={tab} />
                                                    })}
                                                </Tabs>
                                                <div className="preview-list-tab-data">
                                                    {this.state.activeTab === 0 ?
                                                        <React.Fragment>
                                                            <ZenForm inputData={this.state.previewListForm.General} />
                                                        </React.Fragment>
                                                        : this.state.activeTab === 1 ?
                                                            <React.Fragment>
                                                                <div className="receive-amount-table">
                                                                    {this.state.circularLoader == false ?
                                                                        <React.Fragment>
                                                                            <div className="student-txt student-details-wrapper">
                                                                                <p className="datedisplayTxt">Date : {moment().format('DD/MM/YYYY')}</p>
                                                                                <h3 className="student-details-header" style={{ paddingBottom: "20px" }}>Fee Collection Details</h3>
                                                                                {/* {this.state.closePreview ? <Button className={this.state.disablePayment == true ? "payment-method-btn colordisablebtn" : "payment-method-btn"} onClick={this.paymentTypePage} disabled={this.state.disablePayment == true}>Next</Button> : null} */}

                                                                                <div className='student-details-recieveTableText'>
                                                                                    <div className='student-details-recieveTableTextHeader'>
                                                                                        {/* <p className="recieveTableText" ><b className="bold-text">Total Fee :  </b>{this.formatCurrency(Number(this.state.totalFee))} </p> */}
                                                                                        <p className=" recieveTableTextlast"><b className="bold-text">Student Reg. ID : </b> {this.state.totalPreviewList.regId}</p>
                                                                                    </div>
                                                                                    <div className='student-details-recieveTableTextHeader' >
                                                                                        {/* <p className="recieveTableText"><b className="bold-text">Paid Fee :  </b>{this.formatCurrency(Number(this.state.paidFee))}</p> */}
                                                                                        <p className="recieveTableTextlast" ><b className="bold-text">Student Name : </b> {this.state.totalPreviewList.studentName}</p>
                                                                                    </div>
                                                                                    <div className='student-details-recieveTableTextHeader'>
                                                                                        {/* <p className="recieveTableText" ><b className="bold-text">Pending Fee :  </b>{this.formatCurrency(Number(this.state.pendingFee))}</p> */}
                                                                                        <p className="recieveTableTextlast"><b className="bold-text">Student Class : </b> {this.state.totalPreviewList.classBatch}</p>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <table className="receive-pay-tableFormat">
                                                                                    <thead className="receive-pay-tableFormatHead">
                                                                                        <th style={{ width: '7%' }}>No.</th>
                                                                                        <th>Particulars</th>
                                                                                        <th style={{ width: '25%' }}>Amount (₹)</th>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        {this.state.totalPreviewList.description.map((item, idx) => {
                                                                                            return <tr>
                                                                                                {item.name != "Total" ?
                                                                                                    <React.Fragment>
                                                                                                        <td className="receive-pay-tableFormatItem">{idx + 1}</td>
                                                                                                        <td>{item.name}</td>
                                                                                                        <td style={{ textAlign: 'right', paddingRight: 20 }}>{item.paid.toFixed(2)}</td>
                                                                                                    </React.Fragment> :
                                                                                                    <React.Fragment>
                                                                                                        <td></td>
                                                                                                        <td style={{ textAlign: 'right', paddingRight: 20, fontSize: 16 }}><b>Total</b></td>
                                                                                                        <td title="Total paid amount" className="totalAmount-payment">{this.formatCurrency(Number(item.paid))}</td>
                                                                                                    </React.Fragment>
                                                                                                }
                                                                                            </tr>
                                                                                        })}

                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </React.Fragment>
                                                                        : null}
                                                                </div>
                                                            </React.Fragment>
                                                            : this.state.activeTab === 2 ?
                                                                <React.Fragment>
                                                                    <div className="receivePaymentPreviewPaymentTab">
                                                                        <div className="collectPaymentPreviewPaymentTab">
                                                                            <Payment paymentType={this.state.paymentType} StudentRegId={this.state.totalPreviewList.regId} StudentName={this.state.totalPreviewList.studentName} StudentClass={this.state.totalPreviewList.classBatch} />
                                                                        </div>
                                                                    </div>
                                                                </React.Fragment> : null
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </React.Fragment> : <React.Fragment>
                                {this.state.isLoader && <Loader />}
                                <div className="reports-student-fees list-of-students-mainDiv">
                                    <div className="trial-balance-header-title">
                                        <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                        <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Collect Deposit</p>
                                    </div>
                                    <div className="reports-body-section print-hd">
                                        <React.Fragment>
                                            <ContainerNavbar containerNav={this.state.containerNav} onAddNew={this.handleNewDeposit} onDownload={() => this.onDownloadEvent()} />
                                            <div className="print-time"></div>
                                        </React.Fragment>
                                        <div className="reports-data-print-table">
                                            <div className="transaction-review-mainDiv">
                                                <table className="transaction-table-review reports-tableRow-header col-split-td" >
                                                    <thead>
                                                        <tr>
                                                            {this.state.tableHeader.map((data, i) => {
                                                                return <th className={'student-fee ' + String(data).replace(' ', '')} key={i + 1}>{data}</th>
                                                            })}
                                                        </tr>
                                                    </thead>
                                                    {this.state.printReportArr.length > 0 ?
                                                        <tbody className="studentfee-reports-table">
                                                            {this.state.printReportArr.map((data, i) => {
                                                                return (
                                                                    <tr key={i + 1} id={i + 1} onClick={() => this.previewRow(data)} className="table-preview" >
                                                                        <td className="transaction-vch-type">{data.displayName}</td>
                                                                        <td className="transaction-vch-num" >{data.regId}</td>
                                                                        <td className="transaction-vch-num" >{data.studentName}</td>
                                                                        <td className="transaction-vch-type">{data.academicYear}</td>
                                                                        <td className="transaction-vch-type">{data.classBatch}</td>
                                                                        <td className="transaction-particulars">
                                                                            {data.description.map((dataOne, c) => {
                                                                                return (
                                                                                    <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}>{dataOne.name}</p>
                                                                                )
                                                                            })}
                                                                        </td>
                                                                        <td className="transaction-debit">
                                                                            {data.description.map((dataOne, c) => {
                                                                                return (
                                                                                    <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}>{Number(Math.abs(dataOne.due)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                                                                )
                                                                            })}
                                                                        </td>
                                                                        <td className="transaction-debit" >
                                                                            {data.description.map((dataOne, c) => {
                                                                                return (
                                                                                    <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}>{Number(Math.abs(dataOne.paid)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                                                                )
                                                                            })}
                                                                        </td>
                                                                        <td className="transaction-debit">
                                                                            {data.description.map((dataOne, c) => {
                                                                                return (
                                                                                    <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}>{Number(Math.abs(dataOne.balance)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                                                                )
                                                                            })}
                                                                        </td>
                                                                        <td className="transaction-debit" style={{ width: "100px" }}>
                                                                            {data.description.map((dataOne, c) => {
                                                                                var paidDateStr = String(dataOne.paidDate).split('/')['1'] + '/' + String(dataOne.paidDate).split('/')['0'] + '/' + String(dataOne.paidDate).split('/')['2']
                                                                                return (
                                                                                    <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}> {dataOne.paidDate !== '-' ? <DateFormatter date={paidDateStr} format={this.context.dateFormat} /> : '-'} </p>
                                                                                )
                                                                            })}
                                                                        </td>
                                                                        <td className="transaction-debit">
                                                                            {data.description.map((dataOne, c) => {
                                                                                return (
                                                                                    <p>{dataOne.txnId == null ? "-" : dataOne.txnId}</p>
                                                                                )
                                                                            })}
                                                                        </td>
                                                                        <td className="transaction-vch-type">{Number(Math.abs(data.refundAmount)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</td>
                                                                        <td className="transaction-debit">
                                                                            {data.description.map((dataOne, c) => {
                                                                                return (
                                                                                    <p className={String(dataOne.status).toLowerCase().includes("pending") ? "Status-Pending" : String(dataOne.status).toLowerCase().includes("paid") ? "Status-Active" : String(dataOne.status).toLowerCase().includes("partial") ? "Status-Partial" : null} style={{ color: dataOne.status == "Pending" ? "#FF5630" : dataOne.status == "Paid" ? "#00875A" : "#000000", fontWeight: String(dataOne.name).includes("Total") ? "" : '' }}><span>{dataOne.status}</span></p>
                                                                                )
                                                                            })}
                                                                        </td>
                                                                    </tr>
                                                                )
                                                            })}
                                                        </tbody> :
                                                        <tbody>
                                                            <tr>
                                                                {this.state.noData ? <td className="noprog-txt" colSpan={this.state.tableHeader.length}>No data...</td> :
                                                                    <td className="noprog-txt" colSpan={this.state.tableHeader.length}>No data...</td>
                                                                }

                                                            </tr></tbody>
                                                    }
                                                </table>
                                            </div>
                                        </div>
                                        <div>
                                            {this.state.printReportArr.length == 0 ? null :
                                                <PaginationUI
                                                    total={this.state.totalRecord}
                                                    onPaginationApi={this.onPaginationChange}
                                                    totalPages={this.state.totalPages}
                                                    limit={this.state.limit}
                                                    currentPage={this.state.page}
                                                />}
                                        </div>
                                    </div>
                                </div>
                            </React.Fragment>}
                    </React.Fragment>}
            </React.Fragment>
        )
    }
}

export default withRouter(CollectDeposit);