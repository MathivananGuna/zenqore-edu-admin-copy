import React, { Component } from 'react';
import axios from 'axios';
import moment from 'moment';
import { Alert } from 'rsuite';
import '../../../../scss/common-payment.scss';
import '../../../../scss/receive-payment.scss';
import { CircularProgress } from "@material-ui/core";
import { getWeekYearWithOptions } from 'date-fns/fp';
import PaymentForm from '../../../../utils/payment/payment';
// import DateFormatContext from '../../gigaLayout/context';

class Deposit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            circularLoader: false,
            DepositFormData: [],
        }
    }
    render() {
        return (
            <React.Fragment>
                {this.state.circularLoader ?
                    <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                        <CircularProgress size={24} />
                    </div> : null}
                <div className="receive-amount-table" style={{ width: "70%" }} >
                    <div className="student-txt">
                        <div className='student-details' >
                            <p className="student-paraText"><b className="bold-text">Student Name:</b> </p>
                            <p className="student-paraText"><b className="bold-text">{`${this.context.reportLabel ? this.context.reportLabel : "Reg. ID"}`}:</b></p>
                            <p className="student-paraText"><b className="bold-text">Program Plan:</b></p>
                        </div>
                    </div>
                </div>
                <div className="payment-tab receipt-payment">
                    <div className="payment-current">
                        {this.state.DepositFormData.length ? <></> : null}
                    </div>

                </div>
            </React.Fragment>
        )
    }
}

export default Deposit