import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import '../../../../scss/receive-payment.scss';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import { CircularProgress } from "@material-ui/core";
import CancelIcon from '@material-ui/icons/Cancel';
import { Alert } from 'rsuite';
import ParentInfo from './parent-info.json';
class PaymentConfirmation extends Component {
    constructor(props) {
        super(props)
        this.state = {
            amountPaid: '',
            studentName: this.props.studentName,
            receiptID: this.props.receiptID,
            authToken: localStorage.getItem("auth_token"),
            env: JSON.parse(localStorage.getItem('env')),
            orgId: localStorage.getItem("orgId"),
            showdownload: false,
            downloadKey: '',
            checkPaymentStatus: this.props.checkPaymentStatus,
            cashFormData: ParentInfo.CashPayment,
            cardFormData: ParentInfo.CardPayment,
            paymentID: this.props.paymentID
        }
        this.downloadRef = React.createRef();
    }
    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    getInvoice = () => {
        let receiptID = this.state.receiptID;
        let headers = {
            'Authorization': this.state.authToken
        };
        let payload = { "key": receiptID }
        axios.post(`${this.state.env['zqBaseUri']}/edu/getReceipt`, payload, { headers })
            .then(res => {
                console.log(res)
                let url = res.data.url;
                //window.open(url, '_blank');
                let downloadAnchor = document.createElement("a")
                downloadAnchor.href = url;
                downloadAnchor.setAttribute("download", receiptID)
                downloadAnchor.target = "_blank"
                document.body.appendChild(downloadAnchor);
                downloadAnchor.click();
                // document.body.removeChild(downloadAnchor)
            })
            .catch(error => {
                Alert.error("Something went wrong...Please try again")
            })
    }
    // getInvoice = () => {
    //     let receiptID = this.state.receiptID;
    //     let payload = { "key": receiptID }
    //     let headers = {
    //         'Authorization': this.state.authToken
    //     };
    //     axios.post(`${this.state.env['zqBaseUri']}/edu/getReceiptBlob`, payload, { headers })
    //         .then(blob => {
    //             const href = window.URL.createObjectURL(blob.data);
    //             let downloadAnchor = document.createElement("a")
    //             downloadAnchor.href = href;
    //             downloadAnchor.setAttribute("download", receiptID)
    //             downloadAnchor.target = "_blank"
    //             document.body.appendChild(downloadAnchor);
    //             downloadAnchor.click();
    //         }).catch(error => {
    //             console.log("show error", error)
    //             // Alert.error("Login Failed !")
    //         })
    // }
    // getInvoice = () => {
    //     let receiptID = this.state.receiptID;
    //     var data = { "key": receiptID };
    //     var config = {
    //         method: 'post',
    //         url: `${this.state.env['zqBaseUri']}/edu/getReceiptBlob`,
    //         headers: {
    //             'Authorization': this.state.authToken
    //         },
    //         data: data
    //     };
    //     axios(config)
    //         .then(function (response) {
    //             console.log(response)
    //             const href = response.data;
    //             let downloadAnchor = document.createElement("a")
    //             downloadAnchor.href = href;
    //             downloadAnchor.setAttribute("download", receiptID)
    //             downloadAnchor.target = "_blank"
    //             document.body.appendChild(downloadAnchor);
    //             downloadAnchor.click();
    //         })
    //         .catch(function (error) {
    //             console.log("Show error", error);
    //         });
    // }
    componentDidMount() {
        // console.log(this.props);
        let checkPaymentStatus = this.props.checkPaymentStatus != null ? this.props.checkPaymentStatus : false
        this.setState({ checkPaymentStatus: checkPaymentStatus });
        if (checkPaymentStatus != false) {
            let receiptID = this.props.receiptID
            this.setState({ receiptID: receiptID });
            let amount = this.formatCurrency(this.props.paidAmount)
            this.setState({ amountPaid: amount, paymentID: this.props.paymentID, nextLoader: false });
        } else {
            this.setState({ nextLoader: false });
        }
    }

    finishProcess = () => {
        this.state.cardFormData.map((data) => {
            if (data.name == "cardBtn") {
                data['label'] = 'Confirm Payment'
            }
        })
        this.state.cashFormData.map((data) => {
            if (data.name == "cashBtn") {
                data['label'] = 'Confirm Payment'
            }
        })
        this.setState({ nextLoader: true });
        let url = new URL(window.location.href);
        let newURL = `${url.origin}/${url.hash}`;
        window.location.href = newURL
        localStorage.removeItem('checkPaymentStatus');
        localStorage.removeItem('paymentAmount');
        localStorage.removeItem('receiptID');
        localStorage.removeItem('studentName');
        localStorage.removeItem('studentFeeMapId');
        localStorage.removeItem('paymentId');
        localStorage.removeItem('studentFeeID');
        this.setState({ nextLoader: false });
        this.props.finishProcess()
    }
    render() {
        return (
            <React.Fragment>
                {this.state.showdownload && <a href={this.state.downloadKey} ref={this.downloadRef}></a>}
                {this.state.nextLoader ?
                    <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                        <CircularProgress size={24} />
                    </div> : null}
                <div className="receive-payment-main">
                    {this.state.checkPaymentStatus == true ?
                        <div className="payment-confirm-page">
                            <CheckCircleIcon className="confirm-icon" />
                            <p className="payment-confirm-text">An amount of <b>{this.state.amountPaid}</b> for the Student <b>{this.state.studentName}</b>&nbsp;
                                        Successfully processed. <br />The transaction ID is <b>{this.state.paymentID}</b> </p>
                            <div className="confirm-btns">
                                <Button className="confirm-payment-btn" onClick={this.getInvoice} >Download</Button>
                                <Button className="confirm-payment-btn" onClick={this.finishProcess}>Finish</Button>
                            </div>
                        </div>
                        :
                        <div className="payment-confirm-page">
                            <CancelIcon className="cancel-icon" />
                            <p className="payment-confirm-text">Transaction for the Student <b>{this.state.studentName}</b> Failed.
                            <br />Please try again. </p>
                            <div className="confirm-btns">
                                <Button className="confirm-payment-btn" onClick={this.finishProcess}>Finish</Button>
                            </div>
                        </div>
                    }
                </div>
            </React.Fragment>
        )
    }
}
export default withRouter(PaymentConfirmation)