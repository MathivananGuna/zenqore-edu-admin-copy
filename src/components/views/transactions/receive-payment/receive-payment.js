import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Steps } from 'rsuite';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import StepConnector from '@material-ui/core/StepConnector';
import { CircularProgress } from "@material-ui/core";
import Loader from '../../../../utils/loader/loaders';
import '../../../../scss/receive-payment.scss';
import '../../../../scss/payment-portal.scss';
import '../../../../scss/portal-stepper.scss';
import '../../../../scss/setup.scss';
import ParentInfo from './parent-info.json';
import ZenForm from '../../../input/form';
// import ZqTable from '../../../../utils/Table/table-component';
import PaymentForm from './receive-payment.json';
// import PaymentPage from './payment-page';
import PaymentConfirmation from './payment-confirmation';
import axios from 'axios';
import { Alert } from 'rsuite';
//feecollection report
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import DateFormatter from '../../../date-formatter/date-formatter'
import DateFormatContext from '../../../../gigaLayout/context';
import PaginationUI from "../../../../utils/pagination/pagination";
import xlsx from 'xlsx';
import '../../../../scss/student-reports.scss';
import '../../../../scss/student.scss';
import '../../../../scss/common-table.scss';
import Payment from '../../../payment/payment';
import moment from 'moment';
import previewReceivePayment from './preview-receive-payment.json';
import { createStyles } from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';
// import DownloadSVG from '../../../../assets/icons/table-download-icon.svg';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import '../../../../scss/tabs.scss';

const AntSwitch = withStyles((theme) =>
    createStyles({
        root: {
            width: 28,
            height: 16,
            padding: 0,
            display: 'flex',
        },
        switchBase: {
            padding: 2,
            color: '#0052CC',
            '&$checked': {
                transform: 'translateX(12px)',
                color: '0052CC',
                '& + $track': {
                    opacity: 1,
                    backgroundColor: theme.palette.common.white,
                    borderColor: `1px solid ${theme.palette.grey[500]}`,
                },
            },
        },
        thumb: {
            width: 12,
            height: 12,
            boxShadow: 'none',
        },
        track: {
            border: `1px solid ${theme.palette.grey[500]}`,
            borderRadius: 16 / 2,
            opacity: 1,
            backgroundColor: theme.palette.common.white,
        },
        checked: {},
    }),
)(Switch);

class ReceivePayment extends Component {
    constructor(props) {
        super(props)
        this.state = {
            nextLoader: false,
            circularLoader: false,
            payementForm: false,
            activeStep: 0,
            steps: ['Enter Details', 'Payment Method', 'Confirmation'],
            paymentFormData: PaymentForm.FetchStudentDetails,
            pendingAmountData: PaymentForm.AmountDetails,
            cashFormData: ParentInfo.CashPayment,
            cardFormData: ParentInfo.CardPayment,
            tablePreviewData: [],
            paymentDetails: [],
            feeTypeLists: [],
            discper_Checked: true,
            env: JSON.parse(localStorage.getItem('env')),
            orgId: localStorage.getItem("orgId"),
            campusId: localStorage.getItem('campusId'),
            userId: localStorage.getItem('userId'),
            authToken: localStorage.getItem("auth_token"),
            studentID: '',
            studentData: [],
            selectedInfo: [],
            studentFeeID: [],
            guardianDetails: [],
            paymentAmount: '',
            viewFormData: true,
            disableSubmit: false,
            StudentName: '',
            StudentClass: '',
            StudentRegId: '',
            parentName: '',
            parentMobile: '',
            parentEmail: '',
            demandNoteID: '',
            paymentID: '',
            paymentType: '',
            receiptID: '',
            itemSelected: false,
            disablePayment: true,
            showTablePreview: false,
            selectedMode: "Pay with Cash",
            closePreview: true,
            checkPaymentStatus: false,
            isRazorPay: false,
            razorPayRes: false,
            //feecollection report
            isReceivePayment: false,
            containerNav: {
                isBack: false,
                name: "Fees Collection",
                isName: true,
                isSearch: true,
                isSort: false,
                isPrint: true,
                isDownload: true,
                isShare: false,
                isNew: true,
                newName: "New",
                isSubmit: false,
            },
            tableHeader: ["ID", "REG ID", "STUDENT NAME", "ACADEMIC YEAR", "CLASS/BATCH", "TOTAL FEES", "PAID", "PAID ON", "MODE", "PENDING", "TXN ID", "STATUS"],
            printReportArr: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 0,
            noData: false,
            isLoader: false,
            //preview
            isPreview: false,
            previewListForm: previewReceivePayment.formJson,
            previewName: '',
            paidAmount: '',
            totalAmount: 0,
            activeTab: 0,
            tabData: ['General', 'Fee Details', 'Transaction Details'],
            totalPreviewList: [],
            forex: '',
            paymentAmountForex: "",
            totalFee: '',
            paidFee: '',
            pendingFee: '',
            typeReceipt: '',
            iseditable: false,
            PaymentFormData: null,
            paymentPropsData: {
                StudentRegId: '',
                StudentName: '',
                StudentClass: '',
                paymentType: ''
            },
            pendingStatusData: [],
            searchValue: '',
            filterKey: 'All'
        }
        this.paymentRef = React.createRef()
    }
    static contextType = DateFormatContext;
    componentDidMount() {
        let currentURL = window.location.href;
        console.log('currentURL', currentURL)
        console.log('window', window.location)
        if (currentURL.includes("razorpay_payment_id")) {
            this.setState({ nextLoader: true })
        }
        this.state.paymentFormData.map((data) => {
            if (data.name == "studentID") {
                data['error'] = false
                data['required'] = true
                data['readOnly'] = false
                data['errorMsg'] = ''
            }
        })
        let tableHeader = ["ID", `${this.context.reportLabel ? this.context.reportLabel : "REG ID"}`, "STUDENT NAME", "ACADEMIC YEAR", `${this.context.classLabel ? this.context.classLabel : "CLASS/BATCH"}`, "TOTAL FEES", "PAID", "PAID ON", "MODE", "PENDING", "TXN ID", "STATUS"]
        this.setState({ tableHeader })
        this.onDataCall();
    }
    componentWillMount() {
        let { newpath } = this.state;
        let currentURL = window.location.href;
        if (currentURL.includes("razorpay_payment_id")) {
            this.setState({ nextLoader: true, isRazorPay: true });
            const payvalue = currentURL.split("?")
            if (payvalue[1] != undefined) {
                var paymentquery = String(payvalue[1]).split(/[=,&]/)
                this.setState({
                    nextLoader: true,
                    paymentID: paymentquery[1],
                    StudentName: localStorage.getItem('studentName'),
                    paymentAmount: localStorage.getItem('paymentAmount')
                })
                this.ledgerEntry(paymentquery[1]);
                let newPath2 = `${payvalue}/#/main/transactions/receive-payment`;
                this.props.history.push({ newPath2 })
                localStorage.setItem('paymentId', paymentquery[1])
                localStorage.setItem('checkPaymentStatus', true)
            } else {
                this.props.history.push({ newpath })
            }
        }
    }
    ledgerEntry = (payID) => {
        let paymentID = payID;
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        axios.get(`${this.state.env['zqBaseUri']}/edu/getPaymentStatus/${paymentID}?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId} `, {
            headers: {
                "Authorization": localStorage.getItem('auth_token')
            }
        }).then(response => {
            let paymentResponse = JSON.parse(response.data.Data)
            let amount = paymentResponse.amount !== undefined ? paymentResponse.amount : ''
            amount = amount.toString()
            amount = amount.slice(0, -2);
            let payload = {
                "transactionDate": new Date(),
                "relatedTransactions": [localStorage.getItem('studentFeeID')],
                "emailCommunicationRefIds": paymentResponse.email,
                "transactionType": "eduFees",
                "transactionSubType": "feePayment",
                "studentFeeMap": localStorage.getItem('studentFeeMapId'),
                "amount": Number(amount),
                "status": "initiated",
                "data": {
                    "orgId": localStorage.getItem('orgId'),
                    "transactionType": "eduFees",
                    "transactionSubType": "feePayment",
                    "mode": "netbanking",
                    "method": "razorpay",
                    "modeDetails": {
                        "netBankingType": null,
                        "walletType": null,
                        "instrumentNo": paymentResponse.acquirer_data.bank_transaction_id,
                        "cardType": null,
                        "nameOnCard": null,
                        "cardNumber": null,
                        "instrumentDate": new Date(),
                        "bankName": paymentResponse.bank,
                        "branchName": null,
                        "transactionId": paymentResponse.acquirer_data.bank_transaction_id,
                        "remarks": null
                    },
                    "amount": Number(amount)
                },
                "paymentTransactionId": paymentResponse.acquirer_data.bank_transaction_id,
                "createdBy": localStorage.getItem('orgId')
            }
            console.log("payload", payload)
            axios.post(`${this.state.env['zqBaseUri']}/edu/feePaymentWithReceipt`, payload, {
                headers: {
                    "Authorization": localStorage.getItem('auth_token')
                }
            }).then(response => {
                let receiptID = response.data.receiptKey;
                this.setState({ activeStep: 3, receiptID: receiptID });
                localStorage.setItem("receiptID", receiptID)
                localStorage.setItem("checkPaymentStatus", true)
                this.setState({ nextLoader: false, receiptID: receiptID, checkPaymentStatus: true, paymentID: localStorage.getItem('paymentId'), paymentAmount: Number(amount) }, () => {
                    this.setState({ razorPayRes: true })
                });
                console.log("post responsew", response)

            }).catch(err => {
                console.log("post responsew", err)
                this.setState({ activeStep: 0, nextLoader: false });
                let url = new URL(window.location.href);
                let newURL = `${url.origin}/${url.hash}`;
                window.location.href = newURL
                Alert.error("Failed to Generate Fee Recipt")
                console.log(err.response.data.message)
                if (err.response.status === 400) {
                    console.log(err.response.data.message)
                    this.setState({ isLoader: false, nextLoader: false });
                    this.setState({ error: err.response.data.message });
                }
                else {
                    this.setState({ activeStep: 0 });
                    this.setState({ error: 'Payment Failed' })
                    this.setState({ nextLoader: false, isLoader: false })
                }
            })
        })
    }
    onSubmit = (data, item, format) => {
        this.setState({ circularLoader: true });
        let studentID = data[0].value;
        let headers = {
            'Authorization': this.state.authToken
        };
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        axios.get(`${this.state.env['zqBaseUri']}/edu/feesDetails/${studentID}?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}`, { headers })
            .then(res => {
                console.log(res)
                let data = res.data
                let studentDetails = data.studentDetails
                // let demandNoteDetails = data.demandNoteDetails[0]
                let partialAmount = data.partial == null ? true : data.partial
                let feeTypes = []
                let feesBreakUp = data.feesBreakUp;
                feesBreakUp.map(feeElt => {
                    feeTypes.push({
                        'feeTypeId': feeElt['_id'],
                        'feeType': feeElt.title,
                        'amount': Number(feeElt.pending).toFixed(2),
                        'feeTypeCode': feeElt.feeTypeCode

                    })
                })

                this.setState({
                    feeTypeLists: feeTypes,
                    studentData: data,
                    academicYear: data.programPlanDetails.academicYear,
                    // studentID: demandNoteDetails.studentId,
                    StudentName: studentDetails.firstName + "" + studentDetails.lastName,
                    StudentClass: data.programPlanDetails.title,
                    StudentRegId: studentDetails.regId,
                    guardianDetails: data.guardianDetails,
                    forex: studentDetails.FOREX,
                    currencyData: studentDetails.currency,
                    totalFee: data.totalAmount,
                    paidFee: data.paid,
                    pendingFee: data.pending,
                    typeReceipt: data.receiptStatus == 'immediately' ? 'receipt' : 'afterReconcilation',
                    iseditable: partialAmount
                });
                setTimeout(() => {
                    this.setState({ circularLoader: false, payementForm: true });
                    this.amountNetCal();
                    // this.showStudentInformation(data);
                }, 2000)
            })
            .catch(err => {
                // Alert.error(err !== undefined ? err.response.data.message : 'error')
                this.setState({ circularLoader: false, payementForm: false })
            })
        // axios.get(`${this.state.env['zqBaseUri']}/edu/studentFeeDetails/${studentID}?orgId=${this.state.orgId}`, { headers })
        //     .then(res => {
        //         console.log(res)
        //         let data = res.data;
        //         let StudentName = data[0].demandNote.studentName;
        //         let StudentClass = data[0].demandNote.class;
        //         let StudentRegId = data[0].demandNote.studentRegId;
        //         let guardianDetails = data[0].guardianDetails;
        //         let studentFeeMapID = data[0].studentFeeMapDetails.displayName;
        //         localStorage.setItem('studentFeeMapId', studentFeeMapID)
        //         let feeTypes = [];
        //         let forex = data[0].studentDetails;
        //         let feesBreakUp = data[0].feesBreakUp;
        //         let totalFee = data[0].studentFeeMapDetails.amount;
        //         let paidFee = data[0].paid;
        //         let pendingFee = data[0].pending;
        //         let typeReceipt = data[0].receiptStatus
        //         feesBreakUp.map(feeElt => {
        //             feeTypes.push({
        //                 'feeTypeId': feeElt.feeTypeId,
        //                 'feeType': feeElt.feeType,
        //                 'amount': Number(feeElt.amount).toFixed(2),
        //                 'feeTypeCode': feeElt.feeTypeCode

        //             })
        //         })
        //         this.setState({
        //             feeTypeLists: feeTypes,
        //             studentData: data,
        //             studentID: studentID,
        //             StudentName: StudentName,
        //             StudentClass: StudentClass,
        //             StudentRegId: StudentRegId,
        //             guardianDetails: guardianDetails,
        //             forex: forex.FOREX,
        //             currencyData: forex.currency,
        //             totalFee: totalFee,
        //             paidFee: paidFee,
        //             pendingFee: pendingFee,
        //             typeReceipt: typeReceipt
        //         });
        //         setTimeout(() => {
        //             this.setState({ circularLoader: false });
        //             this.amountNetCal();
        //             this.showStudentInformation(data);
        //         }, 2000)
        //     }).catch(err => {
        //         Alert.error(err.reponse.data ? err.response.data.message : 'error')
        //         this.setState({ circularLoader: false, payementForm: false })
        //     })
    }
    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    showStudentInformation = (data) => {
        let paymentDetails = [];
        let guardianDetails = data.guardianDetails;
        let parentName = guardianDetails.firstName;
        let parentMobile = guardianDetails.mobile;
        let parentEmail = guardianDetails.email;
        let feeTerm = []
        data.feeDetails.map((newItem) => {
            feeTerm.push(newItem.feeTypesDetails.description)
        })
        let feeTermString = feeTerm.toString().replace(',', ' , ')
        data.forEach(item => {
            paymentDetails.push({
                "Demand Note ID": item.demandNote.displayName,
                "Payment Term": feeTermString,
                "Total Fees": this.formatCurrency(item.demandNote.data.feesBreakUp[0].amount),
                "Paid": this.formatCurrency(item.paid),
                "Pending": this.formatCurrency(item.pending),
                "Item": JSON.stringify(item)
            })
            let studentFeeID = [];
            studentFeeID = this.state.studentFeeID;
            studentFeeID.push(item.demandNote.displayName)
            this.setState({ demandNoteID: item.demandNote.displayName, studentFeeID: studentFeeID });
        })
        this.setState({ paymentDetails: paymentDetails, payementForm: true, parentName: parentName, parentMobile: parentMobile, parentEmail: parentEmail });
        if (Number(this.state.paymentAmount) > 0) {
            this.setState({ disablePayment: false });
        }
        else {
            this.setState({ disablePayment: true });
        }
    }
    goNextFun1 = (nextProps) => {
        console.log("trigerred", nextProps);
        let txnNumber = nextProps.txnNumber;
        let receiptID = nextProps.receiptID;
        let checkPaymentStatus = nextProps.checkPaymentStatus
        let paidAmount = nextProps.paidAmount
        this.setState({ paymentID: txnNumber, receiptID: receiptID, checkPaymentStatus: checkPaymentStatus, paidAmount: paidAmount, razorPayRes: true });
        this.setState({ activeStep: this.state.activeStep + 1 })

    }
    changePaymentMode = (e, value) => {
        this.setState({ selectedMode: value });
    }
    onInputChanges = (value, item, e, dataS) => {
        this.state.pendingAmountData.map(item => {
            if (item.name == "amountPending") {
                item['readOnly'] = false
                item['defaultValue'] = Number(value) === 0 ? '' : Number(value)
                item['required'] = true
                this.setState({ paymentAmount: value });
            }
        })
        if (Number(value) === 0) {
            this.setState({ disableSubmit: true });
        } else {
            this.setState({ disableSubmit: false });
        }
    }
    changetoStep = (stepperNo, active) => {
        let step = active == 3 ? 0 : stepperNo
        switch (step) {
            case 0:
                return (<React.Fragment>
                    {this.setState({ activeStep: 0, payementForm: false, paymentAmount: 0, totalAmount: 0, disablePayment: true })}
                    {
                        this.state.cardFormData.map((data) => {
                            if (data.name == "cardBtn") {
                                data['label'] = 'Confirm Payment'
                            }
                        })
                    }
                    {
                        this.state.cashFormData.map((data) => {
                            if (data.name == "cashBtn") {
                                data['label'] = 'Confirm Payment'
                            }
                        })
                    }
                </React.Fragment>)
            case 1:
                if (stepperNo < active) return (<React.Fragment>
                    {this.setState({ activeStep: 0, paymentAmount: 0, totalAmount: 0, disablePayment: true })}
                    {
                        this.state.cardFormData.map((data) => {
                            if (data.name == "cardBtn") {
                                data['label'] = 'Confirm Payment'
                            }
                        })
                    }
                    {
                        this.state.cashFormData.map((data) => {
                            if (data.name == "cashBtn") {
                                data['label'] = 'Confirm Payment'
                            }
                        })
                    }
                </React.Fragment>)
                else if (stepperNo + 1 == active) return (<React.Fragment>
                    {this.setState({ activeStep: 1, paymentAmount: 0, disablePayment: true })}
                    {
                        this.state.cardFormData.map((data) => {
                            if (data.name == "cardBtn") {
                                data['label'] = 'Confirm Payment'
                            }
                        })
                    }
                    {
                        this.state.cashFormData.map((data) => {
                            if (data.name == "cashBtn") {
                                data['label'] = 'Confirm Payment'
                            }
                        })
                    }
                </React.Fragment>)
                else { }
        }
    }
    goNextErrFun1 = () => {
        this.setState({ checkPaymentStatus: false, razorPayRes: false })
        this.setState({ activeStep: this.state.activeStep + 1 })
    }
    handleAmtChange = (event, selItem, type) => {
        if (!isNaN(event.target.value)) {
            console.log(event, selItem, type);
            let feeType = this.state.feeTypeLists
            let value = event.target.value
            feeType.map(item => {
                if (item.feeTypeId == selItem.feeTypeId) {
                    if (type == "amount") {
                        item[type] = value
                    }
                }
            })
            this.setState({ feeTypeLists: feeType }, () => {
                this.amountNetCal();
            });
        }
    }
    amountNetCal = () => {
        let total = this.state.feeTypeLists.reduce((a, b) => Number(a) + Number(b.amount), 0)
        let totalAfterForex = Number(total) / Number(this.state.forex)
        this.setState({ paymentAmount: total, paymentAmountForex: totalAfterForex })
        if (total > 0) {
            this.setState({ disablePayment: false });
        }
        else this.setState({ disablePayment: true });
    }

    handleDiscPercentage = (e, item) => {
        console.log('discount event', e)
        console.log('discount event', item)
        this.setState({ discper_Checked: !this.state.discper_Checked })

    }
    getStepContent = (stepIndex) => {
        switch (stepIndex) {
            case 0:
                return <React.Fragment>
                    <div className="receive-payment-main">
                        {this.state.circularLoader ?
                            <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                                <CircularProgress size={24} />
                            </div> : null}
                        {this.state.payementForm == false ?
                            <div className="receive-payment-table" style={{ position: "relative", top: "25vh" }}>
                                {this.state.circularLoader == false ?
                                    <React.Fragment >
                                        <div className="payment-form" style={{ width: "24%", position: "relative", margin: "0 auto" }}>
                                            <ZenForm inputData={this.state.paymentFormData} onSubmit={this.onSubmit} />
                                        </div>
                                    </React.Fragment>
                                    : null}
                            </div> :
                            <div className="receive-amount-table">
                                {this.state.circularLoader == false ?
                                    <React.Fragment>
                                        <div className="student-txt student-details-wrapper">
                                            <p style={{ position: 'absolute', left: '10%', top: '10px' }}>Date : {moment().format('DD/MM/YYYY')}</p>
                                            <h3 className="student-details-header" style={{ paddingBottom: "20px" }}>Fee Collection Details</h3>
                                            {this.state.closePreview ? <Button className={this.state.disablePayment == true ? "payment-method-btn colordisablebtn" : "payment-method-btn"} onClick={this.paymentTypePage} disabled={this.state.disablePayment == true}>Next</Button> : null}

                                            {/* <div className='student-details-recieveTableText'>
                                                <div className='student-details-recieveTableTextHeader'>
                                                    <p className="recieveTableText"><b className="bold-text">Student Reg. ID : </b> {this.state.StudentRegId}</p>
                                                    <p className="recieveTableTextlast" ><b className="bold-text">Total Fee :  </b>{this.formatCurrency(Number(this.state.totalFee))} </p>

                                                </div>
                                                <div className='student-details-recieveTableTextHeader' >
                                                    <p className=" recieveTableText" ><b className="bold-text">Student Name : </b> {this.state.StudentName}</p>
                                                    <p className=" recieveTableTextlast"><b className="bold-text">Paid Fee :  </b>{this.formatCurrency(Number(this.state.paidFee))}</p>
                                                </div>
                                                <div className='student-details-recieveTableTextHeader'>
                                                    <p className=" recieveTableText"><b className="bold-text">Student Class : </b> {this.state.StudentClass}</p>
                
                                                </div>
                                            </div> */}
                                            <table className="receive-pay-tableFormat studentDetails-viewTable">
                                                <tbody>
                                                    <tr className="studentDetails-viewTableFirstRow">
                                                        <td className="studentDetails-viewTableRowData"><p>Student Reg. ID: <b>{this.state.StudentRegId}</b></p></td>
                                                        <td className="studentDetails-viewTableRowData"><p>Student Name : <b>{this.state.StudentName}</b></p></td>
                                                        <td className="studentDetails-viewTableRowData"><p>Student Class : <b>{this.state.StudentClass}</b></p></td>
                                                    </tr>
                                                    <tr >
                                                        <td colSpan={3}>
                                                            <div className="studentDetails-viewTableFirstDivWrap">
                                                                <div className="studentDetails-viewTableFirstDiv" ><p>Total Fee : <b>{this.formatCurrency(Number(this.state.totalFee))}</b></p></div>
                                                                <div style={{ paddingLeft: '20px' }}><p>Paid Fee : <b>{this.formatCurrency(Number(this.state.paidFee))}</b></p> </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div style={{ paddingTop: '10px', paddingBottom: "20px" }}>
                                            <table className="receive-pay-tableFormat">
                                                <thead className="receive-pay-tableFormatHead">
                                                    <th style={{ width: '7%' }}>No.</th>
                                                    <th>Particulars</th>
                                                    <th style={{ width: '30%' }}>Amount (₹)</th>
                                                </thead>
                                                <tbody>
                                                    {this.state.feeTypeLists.map((item, idx) => {
                                                        return <tr>
                                                            <td className="receive-pay-tableFormatItem" style={{ textAlign: 'center' }}>{idx + 1}</td>
                                                            <td className="receive-pay-tableFormatItem" >{item.feeType}</td>
                                                            <td className="receive-pay-tableFormatAmountInput">
                                                                {this.state.iseditable ?
                                                                    <input type="text" id={`${idx}`} className="feeAmount-input"
                                                                        onChange={(e) => { this.handleAmtChange(e, item, 'amount') }} value={item.amount}
                                                                        onClick={(e) => e.currentTarget.setSelectionRange(e.currentTarget.value.length, e.currentTarget.value.length, 'none')}
                                                                    // onClick={(e)=> {e.currentTarget.selectionEnd = e.currentTarget.selectionEnd;}}
                                                                    >
                                                                    </input> : <span>{item.amount}</span>}
                                                            </td>
                                                        </tr>

                                                    })}
                                                    <tr>
                                                        <td></td>
                                                        <td style={{ textAlign: 'right', paddingRight: 20, fontSize: 16 }} >
                                                            <b>Total (INR)</b>
                                                        </td>
                                                        <td className="totalAmount-payment">{Number(this.state.paymentAmount).toFixed(2)}</td>

                                                    </tr>
                                                    {this.state.currencyData == 'USD' ?
                                                        <tr>
                                                            <td></td>
                                                            <td style={{ textAlign: 'right', paddingRight: 20, fontSize: 16 }} >
                                                                <b>Total (USD)</b>
                                                                <p style={{ fontSize: "12px" }}> @Exchange Rate : {this.state.forex}</p>
                                                            </td>
                                                            <td className="totalAmount-payment">{Number(this.state.paymentAmountForex).toFixed(2)}</td>

                                                        </tr>
                                                        : null}
                                                </tbody>
                                            </table>
                                        </div>

                                        {this.state.nextLoader ?
                                            <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                                                <CircularProgress size={24} />
                                            </div> : null}
                                    </React.Fragment>
                                    : null}
                            </div>
                        }
                    </div>
                </React.Fragment >
            case 1:
                return <React.Fragment>
                    <div className="receive-payment-main">
                        <div className="payment-method payment-method-wrap">
                            <Payment
                                paymentAmount={this.state.paymentAmount}
                                studentFeeID={this.state.studentFeeID}
                                studentData={this.state.studentData}
                                guardianDetails={this.state.guardianDetails}
                                paymentType={this.state.paymentType}
                                studentID={this.state.studentID}
                                StudentRegId={this.state.StudentRegId}
                                StudentName={this.state.StudentName}
                                StudentClass={this.state.StudentClass}
                                feeTypeLists={this.state.feeTypeLists}
                                goNextFun={this.goNextFun1}
                                onCardSubmit={this.onCardSubmit}
                                onCashSubmit={this.onCashSubmit}
                                goNextErrFun={this.goNextErrFun1}
                                onPaymentPreview={false}
                            />
                        </div>
                    </div>
                </React.Fragment>
            case 2:
                return <PaymentConfirmation isRazorPay={this.state.isRazorPay} checkPaymentStatus={this.state.checkPaymentStatus} paymentAmount={this.state.paymentAmount} paidAmount={this.state.paidAmount} receiptID={this.state.receiptID} finishProcess={this.finishProcess} paymentID={this.state.paymentID} studentName={this.state.StudentName} onPaymentPreview={false} />

            default:
                return '***';
        }
    }
    paymentTypePage = () => {
        this.handleNext();
    }
    handleNext = () => {
        let a = this.state.activeStep;
        this.setState({ activeStep: a + 1 })
    }
    handleBack = () => {
        let a = this.state.activeStep;
        this.setState({ activeStep: a - 1 })
    }
    finishProcess = () => {
        this.setState({ nextLoader: true });
        setTimeout(() => {
            this.setState({
                activeStep: 0,
                payementForm: false,
                paymentAmount: 0,
                StudentName: '',
                StudentClass: '',
                StudentRegId: '',
                parentName: '',
                parentMobile: '',
                parentEmail: '',
                demandNoteID: '',
                paymentID: '',
                paymentType: '',
                nextLoader: false,
                selectedMode: 'Pay with Cash',
                //feecollection report
                isReceivePayment: false
            }, () => {
                this.onDataCall()
            })
        }, 1000)
    }

    //feecollection report
    receivePayment = () => {
        this.setState({ isReceivePayment: true, activeStep: 0, payementForm: false, isPreview: false })
    }

    onDataCall = () => {
        this.setState({ isLoader: true })
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ printReportArr: [] }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/feePayment?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&page=${this.state.page}&limit=${this.state.limit}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    console.log(resp);
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    this.setState({ printReportArr: resp.data.data, totalPages: resp.data.totalPages, totalRecord: resp.data.totalRecord, noData })
                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true })
                })

        })
    }

    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            if (this.state.searchValue.length == 0) {
                this.onDataCall();
            }
            else {
                this.searchHandle(this.state.searchValue)
            }
        });
        console.log(page, limit);
    };
    formatAmount = (amount) => {
        let a = Number(amount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })
        let b = a.replace('₹', "")
        return b
    }
    onDownloadEvent = () => {
        this.setState({ isLoader: true })
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        axios.get(`${this.state.env['zqBaseUri']}/edu/reports/feePayment?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}`, {
            headers: {
                'Authorization': localStorage.getItem("auth_token")
            }
        })
            .then(resp => {
                console.log("student fee", resp);
                var createXlxsData = []
                let regId = this.context.reportLabel || "REG ID";
                let batch = this.context.classLabel || "CLASS/BATCH"
                resp.data.data.map(item => {
                    item.description.map((dataOne, c) => {
                        if (String(dataOne.name).toLowerCase() === "total") {
                            createXlxsData.push({
                                "ID": item.displayName,
                                [regId]: item.regId,
                                "STUDENT NAME": item.studentName,
                                "ACADEMIC YEAR": item.academicYear,
                                [batch]: item.classBatch,
                                // "DESCRIPTION": dataOne.name,
                                // "TOTAL FEES": this.formatCurrency(dataOne.due),
                                // "PAID": this.formatCurrency(dataOne.paid),
                                // "PAID ON": String(dataOne.paidDate),
                                // "PENDING": this.formatCurrency(dataOne.balance),
                                "TOTAL FEES (INR)": this.formatAmount(dataOne.due),
                                "PAID (INR)": this.formatAmount(dataOne.paid),
                                "PAID ON": String(dataOne.paidDate),
                                "MODE": item.paymentDetails.data.mode,
                                "PENDING (INR)": this.formatAmount(dataOne.balance),
                                "TXN ID": dataOne.txnId,
                                "STATUS": dataOne.status
                            })
                        }
                    })
                    console.log('**DATA**', item)

                })
                var ws = xlsx.utils.json_to_sheet(createXlxsData);
                var wb = xlsx.utils.book_new();
                xlsx.utils.book_append_sheet(wb, ws, "Student Fee Reports");
                xlsx.writeFile(wb, "student_fee_reports.xlsx");
                // xlsx.writeFile(wb, "demand_note_reports.csv");
                this.setState({ isLoader: false })
            })
            .catch(err => {
                console.log(err, 'error')
                this.setState({ isLoader: false })
            })


    }

    previewRow = (data) => {
        console.log("row clicked", data);

        this.setState({ totalPreviewList: data })
        let details = data; let pendingStatusData = [];
        details['description'].map((data) => {
            if (data.name == "Total") {
                pendingStatusData.push(data)
            }
        })
        let feesBreakup = details['description'][0]
        console.log(feesBreakup, pendingStatusData)
        let previewListData = previewReceivePayment.formJson
        this.setState({ paymentAmount: data.paymentDetails.amount })
        Object.keys(previewListData).forEach(tab => {
            previewListData[tab].forEach(task => {
                if (task['name'] == 'demandNoteID') {
                    task['defaultValue'] = data["displayName"]
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'RegID') {
                    task['label'] = this.context.reportLabel || "Reg ID"
                    task['defaultValue'] = details["regId"]
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'StudentName') {
                    task['defaultValue'] = details['studentName']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'ProgramPlan') {
                    task['label'] = this.context.classLabel || "Class/Batch"
                    task['defaultValue'] = details['classBatch']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'academicYear') {
                    task['defaultValue'] = details['academicYear']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'feeTypeCode') {
                    task['defaultValue'] = feesBreakup['name']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'totalAmount') {
                    task['defaultValue'] = feesBreakup['due']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'paidAmount') {
                    task['defaultValue'] = feesBreakup['paid']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'pendingAmount') {
                    task['defaultValue'] = feesBreakup['balance']
                    task['readOnly'] = true
                    task['required'] = false
                }
            })
        })
        if (data.paymentDetails !== undefined) {
            let paymentPropsData = {}; let PaymentFormData = [];
            paymentPropsData['StudentRegId'] = data.paymentDetails.studentRegId;
            paymentPropsData['StudentClass'] = data.paymentDetails.class;
            paymentPropsData['StudentName'] = data.paymentDetails.studentName;
            paymentPropsData['date'] = data.paymentDetails.createdAt
            if (data.paymentDetails.data.mode.toLowerCase() === "cash") {
                paymentPropsData['paymentType'] = "Cash";
                let PaymentFormData = [];
                PaymentFormData.push({
                    date: data.paymentDetails.createdAt !== undefined ? this.dateFilter(data.paymentDetails.createdAt) : "-",
                    amount: data.paymentDetails.amount !== undefined ? data.paymentDetails.amount : "-",
                    remarks: data.paymentDetails.data.modeDetails.remarks !== undefined ? data.paymentDetails.data.modeDetails.remarks : "-"
                })
                this.setState({ PaymentFormData: PaymentFormData, paymentPropsData: paymentPropsData })
            }
            else if (data.paymentDetails.data.mode.toLowerCase() === "cheque") {
                let PaymentFormData = [];
                paymentPropsData['paymentType'] = "Cheque/DD";
                PaymentFormData.push({
                    date: data.paymentDetails.createdAt !== undefined ? this.dateFilter(data.paymentDetails.createdAt) : "-",
                    amount: data.paymentDetails.amount !== undefined ? data.paymentDetails.amount : "-",
                    ChequeNumber: data.paymentDetails.data.modeDetails.transactionId,
                    bankname: data.paymentDetails.data.modeDetails.bankName,
                    ['Branch Name']: data.paymentDetails.data.modeDetails.branchName,
                    remarks: data.paymentDetails.data.modeDetails.remarks
                })
                this.setState({ PaymentFormData: PaymentFormData, paymentPropsData: paymentPropsData })
            }
            else if (data.paymentDetails.data.mode.toLowerCase() === 'card') {
                paymentPropsData['paymentType'] = "Card";
                PaymentFormData.push({
                    date: data.paymentDetails.createdAt !== undefined ? this.dateFilter(data.paymentDetails.createdAt) : "-",
                    amount: data.paymentDetails.amount !== undefined ? data.paymentDetails.amount : "-",
                    cardNumber: data.paymentDetails.data.modeDetails.cardNumber,
                    creditDebit: data.paymentDetails.data.modeDetails.creditDebit,
                    cardType: data.paymentDetails.data.modeDetails.cardType,
                    nameOnCard: data.paymentDetails.data.modeDetails.nameOnCard,
                    transactionNumber: data.paymentDetails.data.modeDetails.transactionId,
                    remarks: data.paymentDetails.data.modeDetails.remarks
                })
                this.setState({ PaymentFormData: PaymentFormData, paymentPropsData: paymentPropsData })
            }
            else if (data.paymentDetails.data.mode.toLowerCase() === 'netbanking') {
                paymentPropsData['paymentType'] = "Netbanking";
                PaymentFormData.push({
                    date: data.paymentDetails.createdAt !== undefined ? this.dateFilter(data.paymentDetails.createdAt) : "-",
                    amount: data.paymentDetails.amount !== undefined ? data.paymentDetails.amount : "-",
                    bankname: data.paymentDetails.data.modeDetails.bankName,
                    netBankingType: data.paymentDetails.data.modeDetails.netBankingType,
                    UTRNumber: data.paymentDetails.data.modeDetails.transactionId,
                    remarks: data.paymentDetails.data.modeDetails.remarks
                })
                this.setState({ PaymentFormData: PaymentFormData, paymentPropsData: paymentPropsData })
            }
            else if (data.paymentDetails.data.mode.toLowerCase() === 'wallet') {
                paymentPropsData['paymentType'] = "Wallet";
                PaymentFormData.push({
                    date: data.paymentDetails.createdAt !== undefined ? this.dateFilter(data.paymentDetails.createdAt) : "-",
                    amount: data.paymentDetails.amount !== undefined ? data.paymentDetails.amount : "-",
                    walletType: data.paymentDetails.data.modeDetails.walletType,
                    transactionNumber: data.paymentDetails.data.modeDetails.transactionId,
                    remarks: data.paymentDetails.data.modeDetails.remarks
                })
                this.setState({ PaymentFormData: PaymentFormData, paymentPropsData: paymentPropsData })
            }
            else if (data.paymentDetails.data.mode.toLowerCase().trim() === 'upi') {
                paymentPropsData['paymentType'] = "Wallet";
                PaymentFormData.push({
                    date: data.paymentDetails.createdAt !== undefined ? this.dateFilter(data.paymentDetails.createdAt) : "-",
                    amount: data.paymentDetails.amount !== undefined ? data.paymentDetails.amount : "-",
                    walletType: data.paymentDetails.data.mode.toUpperCase(),
                    transactionNumber: data.paymentDetails.data.modeDetails.transactionId,
                    remarks: data.paymentDetails.data.modeDetails.remarks
                })
                this.setState({ PaymentFormData: PaymentFormData, paymentPropsData: paymentPropsData })
            }
        }
        this.setState({ isPreview: true, previewName: data['displayName'], previewListForm: previewListData, pendingStatusData: pendingStatusData })
    }
    formatCurrency = (amount) => {
        return new Intl.NumberFormat("en-IN", {
            style: "currency",
            currency: "INR",
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
        }).format(amount);
    };
    dateFilter = (ev) => {
        var ts = new Date(ev);
        let getDate = `${String(ts.getDate()).length == 1 ? `0${ts.getDate()}` : ts.getDate()}`;
        let getMonth = `${String(ts.getMonth() + 1).length == 1 ? `0${ts.getMonth() + 1}` : ts.getMonth() + 1}`;
        let getYear = `${ts.getFullYear()}`;
        let today = `${getDate}/${getMonth}/${getYear}`;
        return today;
    };

    handleTabChange = () => { }
    cancelViewData = () => {
        this.setState({ isReceivePayment: false, isPreview: false })
    }
    tabChangeInternal = (e, value) => { console.log(value); this.setState({ activeTab: value }) }
    printScreen = () => {
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ isLoader: false }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/feePayment?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&pagination=false`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    console.log(resp);
                    this.setState({
                        printable: true,
                        printReportArr: resp.data.data
                    }, () => {
                        this.setState({ printable: false })
                        window.print();
                        this.onDataCall();
                    })
                })
        });
    }
    searchHandle = (searchValue) => {
        this.setState({ isLoader: true })
        this.setState({ searchValue: searchValue });
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        if (String(searchValue).length > 0) {
            return axios.get(`${this.state.env['zqBaseUri']}/edu/reports/feePayment?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&classbatchName=${this.state.filterKey}&page=${this.state.page}&limit=${this.state.limit}&searchKey=${searchValue}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    this.setState({
                        printReportArr: resp.data.data.reverse(),
                        totalPages: resp.data.totalPages,
                        totalRecord: resp.data.totalRecord,
                        noData,
                    })

                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true, dataInfo: "No Data" })
                })
        }
        else {
            this.onDataCall()
        }
    }
    render() {
        const ColorlibConnector = withStyles({
            alternativeLabel: { top: 22 },
            active: { '& $line': { backgroundColor: "#1359C1", marginTop: '-10px', transitionDelay: '2s' }, },
            completed: { '& $line': { backgroundColor: '#36B37E', marginTop: '-10px' }, },
            line: { height: 2, border: 0, backgroundColor: '#ccc', borderRadius: 1, marginTop: '-10px' },
        })(StepConnector);
        return (
            this.state.isReceivePayment ?
                <React.Fragment>
                    <div className="refund-mainDiv">
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.setState({ isReceivePayment: false, isPreview: false }) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title" onClick={() => { this.setState({ isReceivePayment: false, isPreview: false }) }}>| Receive Payment</p>
                        </div>
                        <div className="migration-main-div portal-login-div vendor-invoice-upload-wrap pdf-extraction-upload-wrap" style={{ height: "calc(100vh - 100px)" }}>
                            <div className="migration-header-stepper-section">
                                <div className="zenqore-stepper-section">
                                    <Steps current={this.state.activeStep} currentStatus="process" vertical={false} >
                                        <Steps.Item title="Enter Details" onClick={() => { this.changetoStep(0, this.state.activeStep) }} style={{ cursor: "pointer" }} />
                                        <Steps.Item title="Payment Method" onClick={() => { this.changetoStep(1, this.state.activeStep) }} style={{ cursor: "pointer" }} />
                                        {/* <Steps.Item title="Process Fees" style={{ cursor: "initial" }} /> */}
                                        <Steps.Item title="Confirmation" style={{ cursor: "initial" }} />
                                    </Steps>
                                </div>
                            </div>
                            <div className="migration-body-content-section" style={{ backgroundColor: "#ffffff !important", height: "calc(100vh - 215px)", overflowY: "auto", border: "1px solid #dfe1e6" }}>
                                {this.getStepContent(this.state.activeStep)}
                            </div>
                        </div>
                    </div>

                </React.Fragment>
                :
                // feecollection report
                <>{this.state.isPreview ?
                    <React.Fragment>
                        <div className="reports-student-fees list-of-students-mainDiv table-head-padding">
                            <div className="tab-form-wrapper tab-table">
                                <div className="preview-btns">
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.setState({ isReceivePayment: false, isPreview: false, activeTab: 0 }) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={() => { this.setState({ isReceivePayment: false, isPreview: false, activeStep: 0, paymentForm: false, activeTab: 0 }) }}>| Fee Collection | {this.state.previewName}</h6>
                                    </div>
                                </div>
                                <div className="organisation-table student-submit-none">
                                    <div className="form-new-button-list-new">
                                        <button className="finish-button-demand" onClick={() => { this.setState({ isReceivePayment: false, isPreview: false, activeTab: 0 }) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }}>Done</button>
                                        {/* <img src={DownloadSVG} alt="Download" className="material-searchIcon"></img> */}
                                    </div>
                                    <div className="tab-wrapper">
                                        <Tabs
                                            value={this.state.activeTab}
                                            indicatorColor="primary"
                                            textColor="primary"
                                            onChange={this.tabChangeInternal}
                                        >
                                            {this.state.tabData.map((tab, tabIndex) => {
                                                return <Tab label={tab} />
                                            })}
                                        </Tabs>
                                        <div className="preview-list-tab-data">
                                            {this.state.activeTab === 0 ?
                                                <React.Fragment>
                                                    <ZenForm inputData={this.state.previewListForm.General} />
                                                </React.Fragment>
                                                : this.state.activeTab === 1 ?
                                                    <React.Fragment>
                                                        <div className="receive-amount-table">
                                                            {this.state.circularLoader == false ?
                                                                <React.Fragment>
                                                                    <div className="student-txt student-details-wrapper">
                                                                        <p className="datedisplayTxt">Date : {moment().format('DD/MM/YYYY')}</p>
                                                                        <h3 className="student-details-header" style={{ paddingBottom: "20px" }}>Fee Collection Details</h3>
                                                                        {/* {this.state.closePreview ? <Button className={this.state.disablePayment == true ? "payment-method-btn colordisablebtn" : "payment-method-btn"} onClick={this.paymentTypePage} disabled={this.state.disablePayment == true}>Next</Button> : null} */}

                                                                        <table className="receive-pay-tableFormat studentDetails-viewTable">
                                                                            <tbody>
                                                                                <tr className="studentDetails-viewTableFirstRow">
                                                                                    <td className="studentDetails-viewTableRowData"><p>Student Reg. ID: <b>{this.state.totalPreviewList.regId}</b></p></td>
                                                                                    <td className="studentDetails-viewTableRowData"><p>Student Name : <b>{this.state.totalPreviewList.studentName}</b></p></td>
                                                                                    <td className="studentDetails-viewTableRowData"><p>Student Class : <b>{this.state.totalPreviewList.classBatch}</b></p></td>
                                                                                </tr>
                                                                                <tr >
                                                                                    <td colSpan={3}>
                                                                                        <div className="studentDetails-viewTableFirstDivWrap">
                                                                                            <div className="studentDetails-viewTableFirstDiv" ><p>Total Fee : <b>{this.formatCurrency(Number(this.state.pendingStatusData[0].due))}</b></p></div>
                                                                                            <div style={{ paddingLeft: '20px' }}><p>Paid Fee : <b>{this.formatCurrency(Number(this.state.pendingStatusData[0].paid))}</b></p> </div>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <div>
                                                                        <table className="receive-pay-tableFormat">
                                                                            <thead className="receive-pay-tableFormatHead">
                                                                                <th style={{ width: '7%' }}>No.</th>
                                                                                <th>Particulars</th>
                                                                                <th style={{ width: '25%' }}>Amount (₹)</th>
                                                                            </thead>
                                                                            <tbody>
                                                                                {this.state.totalPreviewList.description.map((item, idx) => {
                                                                                    return <tr>
                                                                                        {item.name != "Total" ?
                                                                                            <React.Fragment>
                                                                                                <td className="receive-pay-tableFormatItem">{idx + 1}</td>
                                                                                                <td>{item.name}</td>
                                                                                                <td style={{ textAlign: 'right', paddingRight: 20 }}>{item.paid === undefined ? "-" : item.paid.toFixed(2)}</td>
                                                                                            </React.Fragment> :
                                                                                            <React.Fragment>
                                                                                                <td></td>
                                                                                                <td style={{ textAlign: 'right', paddingRight: 20, fontSize: 16 }}><b>Total</b></td>
                                                                                                <td title="Total paid amount" className="totalAmount-payment">{this.formatCurrency(Number(item.paid))}</td>
                                                                                            </React.Fragment>
                                                                                        }
                                                                                    </tr>
                                                                                })}

                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </React.Fragment>
                                                                : null}
                                                        </div>
                                                    </React.Fragment>
                                                    : this.state.activeTab === 2 ?
                                                        <React.Fragment>
                                                            <div className="receivePaymentPreviewPaymentTab">
                                                                <div className="collectPaymentPreviewPaymentTab">
                                                                    <Payment
                                                                        paymentAmount={this.state.paymentAmount}
                                                                        StudentRegId={this.state.paymentPropsData.StudentRegId}
                                                                        StudentName={this.state.paymentPropsData.StudentName}
                                                                        StudentClass={this.state.paymentPropsData.StudentClass}
                                                                        paymentType={this.state.paymentPropsData.paymentType}
                                                                        PaymentFormData={this.state.PaymentFormData}
                                                                        onPaymentPreview={true}
                                                                        paymentDate={this.state.paymentPropsData.date}
                                                                    />
                                                                </div>
                                                            </div>
                                                        </React.Fragment> : null
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                    :
                    <React.Fragment>
                        {this.state.isLoader && <Loader />}
                        < div className="reports-student-fees list-of-students-mainDiv" >
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Fees Collection</p>
                            </div>
                            <div className="reports-body-section print-hd">
                                <React.Fragment>
                                    <ContainerNavbar containerNav={this.state.containerNav} onAddNew={this.receivePayment} onDownload={() => this.onDownloadEvent()} searchValue={(searchValue) => this.searchHandle(searchValue)} printScreen={this.printScreen} />
                                    <div className="print-time">{this.state.printTime}</div>
                                </React.Fragment>
                                <div className="reports-data-print-table">
                                    <div className="transaction-review-mainDiv">
                                        <table className="transaction-table-review reports-tableRow-header col-split-td" >
                                            <thead>
                                                <tr>
                                                    {this.state.tableHeader.map((data, i) => {
                                                        return <th className={'student-fee ' + String(data).replace(' ', '')} key={i + 1}>{data}</th>
                                                    })}
                                                </tr>
                                            </thead>
                                            {this.state.printReportArr.length > 0 ?
                                                <tbody className="studentfee-reports-table">
                                                    {this.state.printReportArr.map((data, i) => {
                                                        return (
                                                            <tr key={i + 1} id={i + 1} onClick={() => this.previewRow(data)} className="table-preview" style={{ height: "47px" }} >
                                                                {/* <td className="transaction-sno">{data.txnId}</td> */}
                                                                <td className="transaction-vch-type">{data.displayName}</td>
                                                                <td className="transaction-vch-num" >{data.regId}</td>
                                                                <td className="transaction-vch-num" >{data.studentName}</td>
                                                                <td className="transaction-vch-type">{data.academicYear}</td>
                                                                <td className="transaction-vch-type">{data.classBatch}</td>
                                                                {/* <td className="transaction-particulars">
                                                                    {data.description.map((dataOne, c) => {
                                                                        return (
                                                                            <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}>{dataOne.name}</p>
                                                                        )
                                                                    })}
                                                                </td> */}
                                                                <td className="transaction-debit">
                                                                    {data.description.map((dataOne, c) => {
                                                                        if (dataOne.name == "Total") { }
                                                                        else {
                                                                            return (
                                                                                <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '', justifyContent: "flex-end" }}>{Number(Math.abs(dataOne.due)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                                                            )
                                                                        }
                                                                    })}
                                                                </td>
                                                                <td className="transaction-debit" >
                                                                    {data.description.map((dataOne, c) => {
                                                                        if (dataOne.name == "Total") { }
                                                                        else {
                                                                            return (
                                                                                <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '', justifyContent: "flex-end" }}>{Number(Math.abs(dataOne.paid)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                                                            )
                                                                        }
                                                                    })}
                                                                </td>

                                                                <td className="transaction-debit" style={{ width: "100px" }}>
                                                                    {data.description.map((dataOne, c) => {
                                                                        var dateReg = new RegExp(/^\d{2}[./-]\d{2}[./-]\d{4}$/)
                                                                        let paidDateStr = dataOne.paidDate
                                                                        if (dataOne.paidDate) {
                                                                            if (dateReg.test(dataOne.paidDate)) {
                                                                                if (dataOne.paidDate.includes("/")) { paidDateStr = String(dataOne.paidDate).split('/')['1'] + '/' + String(dataOne.paidDate).split('/')['0'] + '/' + String(dataOne.paidDate).split('/')['2'] }
                                                                                else if (dataOne.paidDate.includes("-")) { paidDateStr = String(dataOne.paidDate).split('-')['1'] + '/' + String(dataOne.paidDate).split('-')['0'] + '/' + String(dataOne.paidDate).split('-')['2'] }
                                                                            } else {
                                                                                paidDateStr = dataOne.paidDate
                                                                            }
                                                                        }
                                                                        // var paidDateStr = String(dataOne.paidDate).split('/')['1'] + '/' + String(dataOne.paidDate).split('/')['0'] + '/' + String(dataOne.paidDate).split('/')['2']
                                                                        if (dataOne.name == "Total") { }
                                                                        else {
                                                                            if (dataOne.name.toLowerCase().trim() == "tuition fee" || dataOne.name.toLowerCase().trim() == "total fee") {
                                                                                return (
                                                                                    <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '', justifyContent: "flex-end" }}>{dataOne.paidDate && dataOne.paidDate !== "NA" && dataOne.paidDate !== "-" ? <DateFormatter date={paidDateStr} format={this.context.dateFormat} /> : "-"}</p>
                                                                                )
                                                                            }
                                                                        }
                                                                    })}
                                                                </td>
                                                                <td className="transaction-debit">
                                                                    {data.description.map((dataOne, c) => {
                                                                        if (dataOne.name === "Total") {
                                                                            return (
                                                                                <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '', textTransform: "uppercase" }}>{dataOne.mode}</p>
                                                                            )
                                                                        }
                                                                    })}
                                                                </td>
                                                                <td className="transaction-debit">
                                                                    {data.description.map((dataOne, c) => {
                                                                        if (dataOne.name === "Total") {
                                                                            return (
                                                                                <p style={{ fontWeight: dataOne.name == "Total" ? "" : '', justifyContent: "flex-end" }}>{dataOne.balance === undefined ? "₹0.00" : Number(Math.abs(dataOne.balance)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                                                            )
                                                                        }
                                                                    })}
                                                                </td>
                                                                {/* <td className="transaction-debit">
                                                                    {data.description.map((dataOne, c) => {
                                                                        var paidDateStr;
                                                                        if (dataOne.paidDate) {
                                                                            if (dataOne.paidDate.includes('/')) { paidDateStr = String(dataOne.paidDate).split('/')['1'] + '/' + String(dataOne.paidDate).split('/')['0'] + '/' + String(dataOne.paidDate).split('/')['2'] }
                                                                            else if (dataOne.paidDate.includes('-')) { paidDateStr = String(dataOne.paidDate).split('-')['1'] + '-' + String(dataOne.paidDate).split('-')['0'] + '-' + String(dataOne.paidDate).split('-')['2'] }
                                                                        }

                                                                        if (dataOne.name == "Total") { }
                                                                        else {
                                                                            if (dataOne.name.toLowerCase().trim() == "tuition fee") {
                                                                                return (
                                                                                    <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '', justifyContent: "flex-end" }}> {dataOne && dataOne.paidDate !== "NA" && dataOne.paidDate !== '-' ? <DateFormatter date={paidDateStr} format={this.context.dateFormat} /> : '-'} </p>
                                                                                )
                                                                            }
                                                                        }
                                                                    })}
                                                                </td> */}
                                                                <td className="transaction-debit">
                                                                    {data.description.map((dataOne, c) => {
                                                                        if (dataOne.name == "Total") { }
                                                                        else {
                                                                            if (dataOne.name.toLowerCase().trim() == "tuition fee" || dataOne.name.toLowerCase().trim() == "total fee") {
                                                                                return (
                                                                                    <p>{dataOne.txnId == null ? "-" : dataOne.txnId}</p>
                                                                                )
                                                                            }
                                                                        }
                                                                    })}
                                                                </td>
                                                                {/* <td className="transaction-vch-type">{Number(Math.abs(data.refundAmount)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</td> */}
                                                                <td className="transaction-debit">
                                                                    <p className={String(data.description[0].status).toLowerCase().includes("pending") ? "Status-Pending" : String(data.description[0].status).toLowerCase().includes("paid") ? "Status-Active" : String(data.description[0].status).toLowerCase().includes("partial") ? "Status-Partial" : null} style={{ color: data.description[0].status == "Pending" ? "#FF5630" : data.description[0].status == "Paid" ? "#00875A" : "#000000", fontWeight: String(data.description[0].status).includes("Total") ? "" : '' }}><span style={{ fontSize: "12px" }}>{data.description[0].status}</span></p>

                                                                    {/* {data.description.map((dataOne, c) => {
                                                                        if (dataOne.name !== "Total") { }
                                                                        else {
                                                                            return (
                                                                                <p className={String(dataOne.status).toLowerCase().includes("pending") ? "Status-Pending" : String(dataOne.status).toLowerCase().includes("paid") ? "Status-Active" : String(dataOne.status).toLowerCase().includes("partial") ? "Status-Partial" : null} style={{ color: dataOne.status == "Pending" ? "#FF5630" : dataOne.status == "Paid" ? "#00875A" : "#000000", fontWeight: String(dataOne.name).includes("Total") ? "" : '' }}><span style={{ fontSize: "12px" }}>{dataOne.status}</span></p>
                                                                            )
                                                                        }
                                                                    })} */}
                                                                </td>
                                                            </tr>
                                                        )
                                                    })}
                                                </tbody> :
                                                <tbody>
                                                    <tr>
                                                        {this.state.noData ? <td className="noprog-txt" colSpan={this.state.tableHeader.length}>No data...</td> :
                                                            <td className="noprog-txt" colSpan={this.state.tableHeader.length}>Fetching the data...</td>
                                                        }

                                                    </tr></tbody>
                                            }
                                        </table>
                                    </div>
                                    <div>
                                        {this.state.printReportArr.length == 0 ? null :
                                            <PaginationUI
                                                total={this.state.totalRecord}
                                                onPaginationApi={this.onPaginationChange}
                                                totalPages={this.state.totalPages}
                                                limit={this.state.limit}
                                                currentPage={this.state.page}
                                            />}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </React.Fragment>}</>
        )
    }
}
export default withRouter(ReceivePayment)