import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Modal, Icon, Steps } from 'rsuite';
import Button from '@material-ui/core/Button';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import StepConnector from '@material-ui/core/StepConnector';
import { CircularProgress } from "@material-ui/core";
import Loader from '../../../../utils/loader/loaders';
import '../../../../scss/receive-payment.scss';
import '../../../../scss/payment-portal.scss';
import '../../../../scss/portal-stepper.scss';
import ParentInfo from './parent-info.json';
import ZenForm from '../../../input/form';
import ZqTable from '../../../../utils/Table/table-component';
import PaymentForm from './receive-payment.json';
import PaymentPage from './payment-page';
import PaymentConfirmation from './payment-confirmation';
import axios from 'axios';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import { Alert } from 'rsuite';
import CloseIcon from '@material-ui/icons/Close';
//feecollection report
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import DateFormatter from '../../../date-formatter/date-formatter'
import DateFormatContext from '../../../../gigaLayout/context';
import PaginationUI from "../../../../utils/pagination/pagination";
import xlsx from 'xlsx';
import '../../../../scss/student-reports.scss';
import '../../../../scss/student.scss';
import '../../../../scss/common-table.scss';
class ReceivePayment extends Component {
    constructor(props) {
        super(props)
        this.state = {
            nextLoader: false,
            circularLoader: false,
            payementForm: false,
            activeStep: 1,
            steps: ['Enter Details', 'Payment Method', 'Process Fees', 'Confirmation'],
            paymentFormData: PaymentForm.FetchStudentDetails,
            pendingAmountData: PaymentForm.AmountDetails,
            cashFormData: ParentInfo.CashPayment,
            cardFormData: ParentInfo.CardPayment,
            tablePreviewData: [],
            paymentDetails: [],
            env: JSON.parse(localStorage.getItem('env')),
            orgId: localStorage.getItem("orgId"),
            authToken: localStorage.getItem("auth_token"),
            studentID: '',
            studentData: [],
            selectedInfo: [],
            studentFeeID: [],
            guardianDetails: [],
            paymentAmount: '',
            viewFormData: true,
            disableSubmit: false,
            StudentName: '',
            StudentClass: '',
            StudentRegId: '',
            parentName: '',
            parentMobile: '',
            parentEmail: '',
            demandNoteID: '',
            paymentID: '',
            paymentType: '',
            receiptID: '',
            itemSelected: false,
            disablePayment: true,
            showTablePreview: false,
            selectedMode: "Pay with Cash",
            closePreview: true,
            checkPaymentStatus: false,
            isRazorPay: false,
            razorPayRes: false,

            //feecollection report
            isReceivePayment: false,
            containerNav: {
                isBack: false,
                name: "Fees Collection",
                isName: true,
                isSearch: false,
                isSort: false,
                isPrint: true,
                isDownload: true,
                isShare: false,
                isNew: true,
                newName: "New",
                isSubmit: false,
            },
            tableHeader: ["DEMAND NOTE ID", "REG ID", "STUDENT NAME", "ACADEMIC YEAR", "CLASS/BATCH", "DESCRIPTION", "TOTAL FEES", "PAID", "BALANCE", "PAID ON", "TXN ID", "REFUND", "STATUS"],
            printReportArr: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 0,
            noData: false,
            isLoader: false
        }
    }
    static contextType = DateFormatContext;
    componentDidMount() {
        let currentURL = window.location.href;
        console.log('currentURL', currentURL)
        console.log('window', window.location)
        if (currentURL.includes("razorpay_payment_id")) {
            this.setState({ nextLoader: true })
        }
        this.state.paymentFormData.map((data) => {
            if (data.name == "studentID") {
                data['error'] = false
                data['required'] = true
                data['readOnly'] = false
                data['errorMsg'] = ''
            }
        })
        this.onDataCall();
    }
    componentWillMount() {
        let { newpath } = this.state;
        let currentURL = window.location.href;
        if (currentURL.includes("razorpay_payment_id")) {
            this.setState({ nextLoader: true, isRazorPay: true });
            const payvalue = currentURL.split("?")
            if (payvalue[1] != undefined) {
                var paymentquery = String(payvalue[1]).split(/[=,&]/)
                this.setState({
                    nextLoader: true,
                    paymentID: paymentquery[1],
                    StudentName: localStorage.getItem('studentName'),
                    paymentAmount: localStorage.getItem('paymentAmount')
                })
                this.ledgerEntry(paymentquery[1]);
                let newPath2 = `${payvalue}/#/main/transactions/receive-payment`;
                this.props.history.push({ newPath2 })
                localStorage.setItem('paymentId', paymentquery[1])
                localStorage.setItem('checkPaymentStatus', true)
            } else {
                this.props.history.push({ newpath })
            }
        }
    }
    ledgerEntry = (payID) => {
        let paymentID = payID;
        axios.get(`${this.state.env['zqBaseUri']}/edu/getPaymentStatus/${paymentID}?orgId=${this.state.orgId} `, {
            headers: {
                "Authorization": localStorage.getItem('auth_token')
            }
        }).then(response => {
            let paymentResponse = JSON.parse(response.data.Data)
            let amount = paymentResponse.amount !== undefined ? paymentResponse.amount : ''
            amount = amount.toString()
            amount = amount.slice(0, -2);
            let payload = {
                "transactionDate": new Date(),
                "relatedTransactions": [localStorage.getItem('studentFeeID')],
                "emailCommunicationRefIds": paymentResponse.email,
                "transactionType": "eduFees",
                "transactionSubType": "feePayment",
                "studentFeeMap": localStorage.getItem('studentFeeMapId'),
                "amount": Number(amount),
                "status": "initiated",
                "data": {
                    "orgId": localStorage.getItem('orgId'),
                    "transactionType": "eduFees",
                    "transactionSubType": "feePayment",
                    "mode": "netbanking",
                    "method": "razorpay",
                    "modeDetails": {
                        "netBankingType": null,
                        "walletType": null,
                        "instrumentNo": paymentResponse.acquirer_data.bank_transaction_id,
                        "cardType": null,
                        "nameOnCard": null,
                        "cardNumber": null,
                        "instrumentDate": new Date(),
                        "bankName": paymentResponse.bank,
                        "branchName": null,
                        "transactionId": paymentResponse.acquirer_data.bank_transaction_id,
                        "remarks": null
                    },
                    "amount": Number(amount)
                },
                "paymentTransactionId": paymentResponse.acquirer_data.bank_transaction_id,
                "createdBy": localStorage.getItem('orgId')
            }
            console.log("payload", payload)
            axios.post(`${this.state.env['zqBaseUri']}/edu/feePaymentWithReceipt`, payload, {
                headers: {
                    "Authorization": localStorage.getItem('auth_token')
                }
            }).then(response => {
                // Alert.success("Successful Fee Payment")
                let receiptID = response.data.receiptKey;
                this.setState({ activeStep: 3, receiptID: receiptID });
                localStorage.setItem("receiptID", receiptID)
                localStorage.setItem("checkPaymentStatus", true)
                this.setState({ nextLoader: false, receiptID: receiptID, checkPaymentStatus: true, paymentID: localStorage.getItem('paymentId'), paymentAmount: Number(amount) }, () => {
                    this.setState({ razorPayRes: true })
                });
                console.log("post responsew", response)

            }).catch(err => {
                console.log("post responsew", err)
                this.setState({ activeStep: 0, nextLoader: false });
                let url = new URL(window.location.href);
                let newURL = `${url.origin}/${url.hash}`;
                window.location.href = newURL
                Alert.error("Failed to Generate Fee Recipt")
                console.log(err.response.data.message)
                if (err.response.status === 400) {
                    console.log(err.response.data.message)
                    this.setState({ isLoader: false, nextLoader: false });
                    this.setState({ error: err.response.data.message });
                }
                else {
                    this.setState({ activeStep: 0 });
                    this.setState({ error: 'Payment Failed' })
                    this.setState({ nextLoader: false, isLoader: false })
                }
            })
        })
        // this.setState({ nextLoader: false });
        // this.setState({ activeStep: 2 })
    }
    onSubmit = (data, item, format) => {
        this.setState({ circularLoader: true });
        let studentID = data[0].value;
        let headers = {
            'Authorization': this.state.authToken
        };
        axios.get(`${this.state.env['zqBaseUri']}/edu/studentFeeDetails/${studentID}?orgId=${this.state.orgId}`, { headers })
            .then(res => {
                console.log(res)
                let data = res.data;
                let StudentName = data[0].demandNote.studentName;
                let StudentClass = data[0].demandNote.class;
                let StudentRegId = data[0].demandNote.studentRegId;
                let guardianDetails = data[0].guardianDetails;
                let studentFeeMapID = data[0].studentFeeMapDetails.displayName
                localStorage.setItem('studentFeeMapId', studentFeeMapID)
                this.setState({
                    studentData: data,
                    studentID: studentID,
                    StudentName: StudentName,
                    StudentClass: StudentClass,
                    StudentRegId: StudentRegId,
                    guardianDetails: guardianDetails
                });
                setTimeout(() => {
                    this.setState({ circularLoader: false });
                    this.showStudentInformation(data);
                }, 2000)
            }).catch(err => {
                console.log(err.response.data.message)
                Alert.error(err.response.data.message)
                this.setState({ circularLoader: false, payementForm: false })
            })
    }
    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    showStudentInformation = (data) => {
        let paymentDetails = [];
        let guardianDetails = data[0].guardianDetails;
        let parentName = guardianDetails.firstName;
        let parentMobile = guardianDetails.mobile;
        let parentEmail = guardianDetails.email;
        let feeTerm = []
        data[0].feeDetails.map((newItem) => {
            feeTerm.push(newItem.feeTypesDetails.description)
        })
        let feeTermString = feeTerm.toString().replace(',', ' , ')
        data.forEach(item => {
            paymentDetails.push({
                "Demand Note ID": item.demandNote.displayName,
                "Payment Term": feeTermString,
                "Total Fees": this.formatCurrency(item.demandNote.data.feesBreakUp[0].amount),
                "Paid": this.formatCurrency(item.paid),
                "Pending": this.formatCurrency(item.pending),
                "Item": JSON.stringify(item)
            })
            this.setState({ demandNoteID: item.demandNote.displayName });
        })
        this.setState({ paymentDetails: paymentDetails, payementForm: true, parentName: parentName, parentMobile: parentMobile, parentEmail: parentEmail });
    }
    onCardSubmit = (nextProps) => {
        let txnNumber = nextProps.txnNumber;
        let receiptID = nextProps.receiptID;
        let checkPaymentStatus = nextProps.checkPaymentStatus
        this.setState({ paymentID: txnNumber, receiptID: receiptID, checkPaymentStatus: checkPaymentStatus, razorPayRes: true });
        this.handleNext();
    }
    onCashSubmit = (nextProps) => {
        let txnNumber = nextProps.txnNumber;
        let receiptID = nextProps.receiptID;
        let checkPaymentStatus = nextProps.checkPaymentStatus
        this.setState({ paymentID: txnNumber, receiptID: receiptID, checkPaymentStatus: checkPaymentStatus, razorPayRes: true });
        this.handleNext();
    }
    confirmCash = () => {
        this.setState({ nextLoader: true });
        if (this.state.paymentAmount != 0 || this.state.paymentAmount != undefined || this.state.paymentAmount != null) {
            setTimeout(() => {
                let a = this.state.activeStep;
                this.setState({ activeStep: a + 1, nextLoader: false })
            }, 1000)
        } else {
            this.state.pendingAmountData.map((data) => {
                if (data.name == "amountPending") {
                    data['error'] = true
                    data['required'] = true
                    data['readOnly'] = false
                    data['errorMsg'] = `Invalid Pending Amount`
                }
            })
            this.setState({ nextLoader: false });
        }
        this.setState({ paymentType: 'Cash Payment' });
    }
    confirmCard = () => {
        this.handleNext();
        this.setState({ paymentType: 'Card Payment' });
    }
    confirmCheque = () => {
        this.handleNext();
        this.setState({ paymentType: 'Cheque Payment' });
    }
    confirmRazorpay = () => {
        let payload = {
            "amount": Number(this.state.paymentAmount),
            "paisa": String(this.state.paymentAmount).includes('.') ? String(this.state.paymentAmount).split('.')[1] : "00",
            "paymentReferenceId": Date.now() + `#` + this.state.demandNoteID,
            "acceptPartial": false,
            "name": this.state.studentData[0].demandNote.studentName,
            "mobile": this.state.parentMobile,
            "email": this.state.parentEmail,
            "callBackUrl": `${window.location.href}/?demandId=${localStorage.getItem('studentFeeID')}&orgId=${localStorage.getItem('orgId')}&type=demandNote`,
            "currencyCode": "INR",
            "description": "",
            "paidFor": "demandNote",
            "orgId": localStorage.getItem('orgId')
        }
        console.log("payload", payload)
        axios.post(`${this.state.env["zqBaseUri"]}/edu/payment`, payload, {
            headers: {
                "Authorization": localStorage.getItem('auth_token')
            }
        })
            .then(res => {
                localStorage.setItem('paymentAmount', Number(this.state.paymentAmount))
                localStorage.setItem('studentName', this.state.studentData[0].demandNote.studentName)
                window.open(res.data.data.short_url, '_self');

            }).catch(err => {
                Alert.error("Unable to Load RazorPay")
            })
    }
    onPreviewStudent = (item) => {
        console.log(item)
        this.setState({ showTablePreview: true, nextLoader: true, closePreview: false, tablePreviewData: [] }, () => {
            let parsedData = JSON.parse(item.Item);
            let studentJson = [{
                "type": "heading",
                "label": "Fees Breakup",
                "class": "form-hd"
            }];
            parsedData.feeDetails.map(feeitem => {
                studentJson.push({
                    "category": "input",
                    "type": "text",
                    "name": feeitem.feeTypesDetails.displayName,
                    "label": feeitem.feeTypesDetails.title,
                    "class": "input-wrap tuition-fee",
                    "readOnly": true,
                    "required": false,
                    defaultValue: feeitem.feeManagerDetails.feeDetails.totalAmount != undefined ? this.formatCurrency(feeitem.feeManagerDetails.feeDetails.totalAmount) : 'N/A'
                })
            })
            studentJson.push({
                "type": "heading",
                "label": "Total Amount",
                "class": "form-hd"
            },
                {
                    "category": "input",
                    "type": "text",
                    "name": "totalAmount",
                    "label": "Total Amount",
                    "class": "input-wrap total-amount",
                    "readOnly": false,
                    "required": false,
                    defaultValue: this.formatCurrency(parsedData.demandNote.amount)
                })
            console.log('studentJson', studentJson)
            this.setState({ tablePreviewData: studentJson, nextLoader: false }, () => {
                console.log('tablePreviewData', this.state.tablePreviewData)
            });
        });
    }
    selectedItem = (e, value, index) => {
        this.setState({ disablePayment: false });
        var selectedInfo = this.state.selectedInfo;
        let value1 = JSON.parse(value.Item);
        let paymentAmount = this.state.paymentAmount;
        let selectedAmt = value1.paid === 0 ? value1.demandNote.amount : value1.pending
        if (selectedAmt === 0) {
            this.setState({ disablePayment: true });
        } else {
            this.setState({ disablePayment: false });
        }
        let items = {}
        items = selectedAmt
        if (value.checked == true) {
            let studentFeeID = [];
            studentFeeID = this.state.studentFeeID;
            studentFeeID.push(value1.demandNote.displayName)
            this.setState({ studentFeeID: studentFeeID });
            localStorage.setItem("studentFeeID", this.state.studentFeeID)
            let payableAmt = paymentAmount + Number(items)
            this.setState({ paymentAmount: payableAmt, viewFormData: false });
            console.log("payableAmt", payableAmt)
            this.state.pendingAmountData.map((data) => {
                if (data.name == "amountPending") {
                    data['defaultValue'] = Number(payableAmt).toFixed(2)
                    data['readOnly'] = false
                    data['validation'] = true
                }
            })
            this.setState({ viewFormData: true })
        } else {
            this.setState({ disablePayment: true });
            let studentFeeID = [];
            studentFeeID = this.state.studentFeeID;
            const indexVal = studentFeeID.indexOf(value1.demandNote.displayName)
            if (indexVal > -1) {
                studentFeeID.splice(indexVal, 1)
            }
            this.setState({ studentFeeID: studentFeeID });
            localStorage.setItem("studentFeeID", this.state.studentFeeID)
            let payableAmt = value.rowId == 0 ? 0 : paymentAmount - Number(items)
            this.setState({ paymentAmount: payableAmt, viewFormData: false });
            console.log("payableAmt", payableAmt)
            this.state.pendingAmountData.map((data) => {
                if (data.name == "amountPending") {
                    data['defaultValue'] = Number(payableAmt).toFixed(2)
                    data['readOnly'] = false
                    data['validation'] = true
                }

            })
            this.setState({ viewFormData: true })
        }
        this.setState({ selectedInfo: selectedInfo });
    }
    selectAll = (allItems, a) => {
        this.setState({ disablePayment: false });
        let AmountValues = []
        allItems.map(item => {
            AmountValues.push((JSON.parse(item.Item)).demandNote.amount)
        })
        console.log(AmountValues)
        let total = AmountValues.reduce((a, b) => a + b, 0)
        console.log(total)
        let value1 = JSON.parse(allItems[0].Item);
        let selectedAmt = value1.paid === 0 ? value1.demandNote.amount : value1.pending
        if (selectedAmt === 0) {
            this.setState({ disablePayment: true });
        } else {
            this.setState({ disablePayment: false });
        }
        if (a == true) {
            let studentFeeID = []
            allItems.map(item => {
                studentFeeID.push(item['Demand Note ID'])
            })
            this.setState({ studentFeeID: studentFeeID });
            localStorage.setItem("studentFeeID", this.state.studentFeeID)
            this.setState({ selectedInfo: allItems, enableBtn: true })
            this.state.pendingAmountData.map((data) => {
                if (data.name == "amountPending") {
                    data['defaultValue'] = Number(total).toFixed(2)
                    data['readOnly'] = false
                    data['validation'] = true
                }
            })
            this.setState({ paymentAmount: Number(total) })
        }
        else {
            this.setState({ disablePayment: true });
            let studentFeeID = []
            this.setState({ studentFeeID: [] });
            localStorage.setItem("studentFeeID", this.state.studentFeeID)
            this.setState({ selectedInfo: [], enableBtn: false })
            this.state.pendingAmountData.map((data) => {
                if (data.name == "amountPending") {
                    data['readOnly'] = false
                    data['validation'] = true
                    data['defaultValue'] = ''
                }
            })
            this.setState({ paymentAmount: '' })
        }
    }
    changePaymentMode = (e, value) => {
        this.setState({ selectedMode: value });
    }
    paymentMethod = () => {
        console.log(this.state.selectedMode)
        if (this.state.selectedMode === 'Pay with Cash') {
            this.confirmCash();
        } else if (this.state.selectedMode === 'Pay with Card') {
            this.confirmCard();
        } else if (this.state.selectedMode === 'Pay with Cheque') {
            this.confirmCheque();
        } else if (this.state.selectedMode === 'Pay with RazorPay') {
            this.confirmRazorpay();
        }
    }
    onInputChanges = (value, item, e, dataS) => {
        this.state.pendingAmountData.map(item => {
            if (item.name == "amountPending") {
                item['readOnly'] = false
                item['defaultValue'] = Number(value) === 0 ? '' : Number(value)
                item['required'] = true
                this.setState({ paymentAmount: value });
            }
        })
        if (Number(value) === 0) {
            this.setState({ disableSubmit: true });
        } else {
            this.setState({ disableSubmit: false });
        }
        // if (this.state.disablePayment == true && value.length == 0) {
        //     if (item.name == "amountPending") {
        //         item['readOnly'] = false
        //     }
        // }
        // if (this.state.disablePayment == false && value.length == 0) {
        //     if (item.name == "amountPending") {
        //         item['readOnly'] = false
        //         item['defaultValue'] = ''
        //     }
        // }
        // if (this.state.disablePayment == false && value.length !== 0) {
        //     if (item.name == "amountPending") {
        //         item['readOnly'] = false
        //         item['defaultValue'] = value
        //         this.setState({ paymentAmount: value });
        //     }
        // }
    }
    changetoStep = (stepperNo, active) => {
        let step = active == 3 ? 0 : stepperNo
        switch (step) {
            case 0:
                return (<React.Fragment>
                    {this.setState({ activeStep: 0, payementForm: false, paymentAmount: 0, disablePayment: true })}
                    {
                        this.state.cardFormData.map((data) => {
                            if (data.name == "cardBtn") {
                                data['label'] = 'Confirm Payment'
                            }
                        })
                    }
                    {
                        this.state.cashFormData.map((data) => {
                            if (data.name == "cashBtn") {
                                data['label'] = 'Confirm Payment'
                            }
                        })
                    }
                </React.Fragment>)
            case 1:
                if (stepperNo < active) return (<React.Fragment>
                    {this.setState({ activeStep: 0, paymentAmount: 0, disablePayment: true })}
                    {
                        this.state.cardFormData.map((data) => {
                            if (data.name == "cardBtn") {
                                data['label'] = 'Confirm Payment'
                            }
                        })
                    }
                    {
                        this.state.cashFormData.map((data) => {
                            if (data.name == "cashBtn") {
                                data['label'] = 'Confirm Payment'
                            }
                        })
                    }
                </React.Fragment>)
                else if (stepperNo + 1 == active) return (<React.Fragment>
                    {this.setState({ activeStep: 1, paymentAmount: 0, disablePayment: true })}
                    {
                        this.state.cardFormData.map((data) => {
                            if (data.name == "cardBtn") {
                                data['label'] = 'Confirm Payment'
                            }
                        })
                    }
                    {
                        this.state.cashFormData.map((data) => {
                            if (data.name == "cashBtn") {
                                data['label'] = 'Confirm Payment'
                            }
                        })
                    }
                </React.Fragment>)
                // else return Alert.info('Please complete the 1st Step')
                else { }
        }
    }

    getStepContent = (stepIndex) => {
        switch (stepIndex) {
            case 0:
                return <React.Fragment>
                    <div className="receive-payment-main">
                        {this.state.circularLoader ?
                            <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                                <CircularProgress size={24} />
                            </div> : null}
                        {this.state.payementForm == false ?
                            <div className="receive-payment-table" style={{ position: "relative", top: "25vh" }}>
                                {this.state.circularLoader == false ?
                                    <React.Fragment >
                                        <div className="payment-form" style={{ width: "24%", position: "relative", margin: "0 auto" }}>
                                            <ZenForm inputData={this.state.paymentFormData} onSubmit={this.onSubmit} />
                                        </div>
                                    </React.Fragment>
                                    : null}
                            </div> :
                            <div className="receive-amount-table">
                                {this.state.circularLoader == false ?
                                    <React.Fragment >
                                        <div className="student-txt">
                                            <h3 className="student-details-header">Student Details</h3>
                                            {this.state.closePreview ? <Button className="payment-method-btn" onClick={this.paymentTypePage} disabled={this.state.disablePayment == true}>Next</Button> : null}
                                            <div className='student-details'>
                                                <p style={{ marginLeft: "20px" }}><b className="bold-text">Student Reg. ID:</b> {this.state.StudentRegId}</p>
                                                <p style={{ marginLeft: "20px" }}><b className="bold-text">Student Name:</b> {this.state.StudentName}</p>
                                                <p style={{ marginLeft: "20px" }}><b className="bold-text">Student Class:</b> {this.state.StudentClass}</p>
                                            </div>
                                        </div>
                                        <div className="student-details">
                                            <br />
                                            {!this.state.showTablePreview ? <ZqTable variant='secondary' className="pending-amt-table"
                                                data={this.state.paymentDetails}
                                                rowClick={(item) => { this.onPreviewStudent(item) }}
                                                onRowCheckBox={(e, value, index) => { this.selectedItem(e, value, index) }}
                                                allSelect={(allItems, a) => { this.selectAll(allItems, a) }}
                                            /> :
                                                <div className="preview-form">
                                                    <CloseIcon className="close-preview" onClick={() => this.setState({ showTablePreview: false, closePreview: true, tablePreviewData: [] })} />
                                                    {this.state.tablePreviewData.length > 0 ? <ZenForm inputData={this.state.tablePreviewData} /> : null}
                                                </div>}
                                            <br />
                                        </div>
                                        {this.state.nextLoader ?
                                            <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                                                <CircularProgress size={24} />
                                            </div> : null}
                                    </React.Fragment>
                                    : null}
                            </div>
                        }
                    </div>
                </React.Fragment>
            case 1:
                return <React.Fragment>
                    <div className="receive-payment-main">
                        <div className="payment-method">
                            <ZenForm inputData={this.state.pendingAmountData} onInputChanges={this.onInputChanges} />
                            <div className="payment-radio-btns">
                                <RadioGroup row aria-label="position"
                                    name="position"
                                    defaultValue="Return Item"
                                    className='payment-group'
                                    onChange={(e, value) => this.changePaymentMode(e, value)}
                                    value={this.state.selectedMode}
                                >
                                    <FormControlLabel value="Pay with Cash" control={<Radio color="primary" />} label="Pay with Cash" />
                                    <FormControlLabel value="Pay with Card" control={<Radio color="primary" />} label="Pay with Card" />
                                    <FormControlLabel value="Pay with Cheque" control={<Radio color="primary" />} label="Pay with Cheque" />
                                    <FormControlLabel value="Pay with RazorPay" control={<Radio color="primary" />} label="Pay with RazorPay" />
                                </RadioGroup>
                            </div>
                            <div className="payment-btns">
                                <Button className="razorpay-btn" onClick={this.paymentMethod} disabled={this.state.disableSubmit == true}>Submit</Button>
                                {/* <Button className="pay-cash-btn" onClick={this.confirmCash} disabled={this.state.disablePayment == true}>Pay with Cash</Button>
                                <Button className="pay-credit-btn" onClick={this.confirmCard} disabled={this.state.disablePayment == true}>Pay with Credit/Debit Card</Button>
                                <Button className="razorpay-btn" onClick={this.confirmRazorpay}>Pay with RazorPay</Button> */}
                            </div>
                        </div>
                    </div>
                </React.Fragment>
            case 2:
                return <PaymentPage paymentAmount={this.state.paymentAmount} studentFeeID={this.state.studentFeeID} studentData={this.state.studentData} guardianDetails={this.state.guardianDetails} paymentType={this.state.paymentType} studentID={this.state.studentID} onCardSubmit={this.onCardSubmit} onCashSubmit={this.onCashSubmit} />
            case 3:
                return <React.Fragment>
                    {this.state.razorPayRes ? <PaymentConfirmation isRazorPay={this.state.isRazorPay} checkPaymentStatus={this.state.checkPaymentStatus} paymentAmount={this.state.paymentAmount} receiptID={this.state.receiptID} finishProcess={this.finishProcess} paymentID={this.state.paymentID} studentName={this.state.StudentName} /> : <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                        <CircularProgress size={24} />
                    </div>
                    }
                </React.Fragment>
            default:
                return '***';
        }
    }
    paymentTypePage = () => {
        this.handleNext();
    }
    handleNext = () => {
        let a = this.state.activeStep;
        this.setState({ activeStep: a + 1 })
    }
    handleBack = () => {
        let a = this.state.activeStep;
        this.setState({ activeStep: a - 1 })
    }
    finishProcess = () => {
        this.setState({ nextLoader: true });
        setTimeout(() => {
            this.setState({
                activeStep: 0,
                payementForm: false,
                paymentAmount: 0,
                StudentName: '',
                StudentClass: '',
                StudentRegId: '',
                parentName: '',
                parentMobile: '',
                parentEmail: '',
                demandNoteID: '',
                paymentID: '',
                paymentType: '',
                nextLoader: false,
                selectedMode: 'Pay with Cash',
                //feecollection report
                isReceivePayment: false
            },()=>{
                this.onDataCall()
            })
        }, 1000)
    }

    //feecollection report
    receivePayment = () => {
        this.setState({ isReceivePayment: true })
    }

    onDataCall = () => {
        this.setState({ isLoader: true })
        this.setState({ printReportArr: [] }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/feePayment?orgId=${this.state.orgId}&page=${this.state.page}&limit=${this.state.limit}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token"),
                    // 'Authorization': "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im11bml5YXJhai5uZWVsYW1lZ2FtQGdtYWlsLmNvbSIsImlkIjoiNWY4NTgyZTA2OGEwMDIwMDA4NmM2Y2YxIiwiaWF0IjoxNjA1MjQ3NjQ0LCJleHAiOjE2MDUzMzQwNDR9.m0EMpiZVjkbEWTUjC10WjiRPu0WGP9DBxeV5jBJnyQU",
                    //'client': 'ken42'
                }
            })
                .then(resp => {
                    console.log(resp);
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    this.setState({ printReportArr: resp.data.data, totalPages: resp.data.totalPages, totalRecord: resp.data.totalRecord, noData })
                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true })
                    // var respErr = { "status": "success", "message": "feePayment reports", "data": [{ "studentName": "Adiba Nisar", "regId": "STUD_001", "academicYear": "2020-21", "classBatch": "BE_ENG_CSC_20-21", "DemandId": "DN_2020-21_001", "description": [{ "name": "Tuition Fee", "due": 145000, "paid": 145000, "paidDate": "12/11/2020", "balance": 0, "status": "Paid", "txnId": "5fae2d05daeb301acc833ec6" }, { "name": "Total", "due": 145000, "paid": 145000, "paidDate": "-", "balance": 0, "status": "Paid", "txnId": "-" }] }, { "studentName": "Abdul Nasar", "regId": "STUD_002", "academicYear": "2020-21", "classBatch": "BE_ENG_MEC_20-21", "DemandId": "DN_2020-21_002", "description": [{ "name": "Tuition Fee", "due": 125000, "paid": 125000, "paidDate": "11/11/2020", "balance": 0, "status": "Paid", "txnId": "5fae2d090888b11acc10be14" }, { "name": "Total", "due": 125000, "paid": 125000, "paidDate": "-", "balance": 0, "status": "Paid", "txnId": "-" }] }, { "studentName": "Pradeep Kumar", "regId": "STUD_003", "academicYear": "2020-21", "classBatch": "BE_ENG_CVL_20-21", "DemandId": "DN_2020-21_003", "description": [{ "name": "Uniform Plan", "due": 2000, "paid": 800, "paidDate": "10/11/2020", "balance": 2200, "status": "Partial", "txnId": "5fae2d0d7074061accc1a355" }, { "name": "Tuition Fee", "due": 2000, "paid": 1200, "paidDate": "10/11/2020", "balance": 1800, "status": "Partial", "txnId": "5fae2d0d7074061accc1a355" }, { "name": "Total", "due": 4000, "paid": 2000, "paidDate": "-", "balance": 4000, "status": "Partial", "txnId": "-" }] }, { "studentName": "Mohammed Yaseen R", "regId": "STUD_003", "academicYear": "2020-21", "classBatch": "BE_ENG_CVL_20-21", "DemandId": "DN_2020-21_004", "description": [{ "name": "Transport Fee", "due": 280000, "paid": 11275.17, "paidDate": "25/10/2020", "balance": 128724.83, "status": "Partial", "txnId": "5fae2d1aa3335d1acc4e9a77" }, { "name": "Tuition Fee", "due": 280000, "paid": 131543.62, "paidDate": "25/10/2020", "balance": 8456.38, "status": "Partial", "txnId": "5fae2d1aa3335d1acc4e9a77" }, { "name": "Uniform Plan", "due": 280000, "paid": 5637.58, "paidDate": "25/10/2020", "balance": 134362.42, "status": "Partial", "txnId": "5fae2d1aa3335d1acc4e9a77" }, { "name": "Tuition Fee", "due": 280000, "paid": 131543.62, "paidDate": "25/10/2020", "balance": 8456.38, "status": "Partial", "txnId": "5fae2d1aa3335d1acc4e9a77" }, { "name": "Total", "due": 1120000, "paid": 279999.99, "paidDate": "-", "balance": 280000.01, "status": "Partial", "txnId": "-" }] }], "currentPage": null, "perPage": 10, "nextPage": null, "totalRecord": 4, "totalPages": 1 }

                    // this.setState({ printReportArr: respErr.data, totalPages: respErr.totalPages, totalRecord: respErr.totalRecord })
                })

        })
    }

    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.onDataCall();
        });
        console.log(page, limit);
    };

    // formatCurrency = (amount) => {
    //     return (new Intl.NumberFormat('en-IN', {
    //         style: 'currency',
    //         currency: 'INR',
    //         minimumFractionDigits: 2,
    //         maximumFractionDigits: 2
    //     }).format(amount))
    // }

    onDownloadEvent = () => {
        this.setState({ isLoader: true })
        axios.get(`${this.state.env['zqBaseUri']}/edu/reports/feePayment?orgId=${this.state.orgId}`, {
            headers: {
                'Authorization': localStorage.getItem("auth_token")
            }
        })
            .then(resp => {
                console.log("student fee", resp);
                var createXlxsData = []
                resp.data.data.map(item => {
                    item.description.map((dataOne, c) => {
                        if (String(dataOne.name).toLowerCase() != "total") {
                            createXlxsData.push({
                                "DEMAND NOTE ID": item.DemandId,
                                "REG ID": item.regId,
                                "STUDENT NAME": item.studentName,
                                "ACADEMIC YEAR": item.academicYear,
                                "CLASS/BATCH": item.classBatch,
                                "DESCRIPTION": dataOne.name,
                                "TOTAL FEES": this.formatCurrency(dataOne.due),
                                "PAID": this.formatCurrency(dataOne.paid),
                                "BALANCE": this.formatCurrency(dataOne.balance),
                                "PAID ON": String(dataOne.paidDate),
                                "TXN ID": dataOne.txnId,
                                "STATUS": dataOne.status
                            })
                        }
                    })
                    console.log('**DATA**', item)

                })
                var ws = xlsx.utils.json_to_sheet(createXlxsData);
                var wb = xlsx.utils.book_new();
                xlsx.utils.book_append_sheet(wb, ws, "Student Fee Reports");
                xlsx.writeFile(wb, "student_fee_reports.xlsx");
                // xlsx.writeFile(wb, "demand_note_reports.csv");
                this.setState({ isLoader: false })
            })
            .catch(err => {
                console.log(err, 'err0rrrrr')
                this.setState({ isLoader: false })
            })


    }

    render() {
        const ColorlibConnector = withStyles({
            alternativeLabel: { top: 22 },
            active: { '& $line': { backgroundColor: "#1359C1", marginTop: '-10px', transitionDelay: '2s' }, },
            completed: { '& $line': { backgroundColor: '#36B37E', marginTop: '-10px' }, },
            line: { height: 2, border: 0, backgroundColor: '#ccc', borderRadius: 1, marginTop: '-10px' },
        })(StepConnector);
        return (
            this.state.isReceivePayment ?
                <React.Fragment>
                    <div className="refund-mainDiv">
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.setState({ isReceivePayment: false }) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title">| Receive Payment</p>
                        </div>
                        <div className="migration-main-div portal-login-div vendor-invoice-upload-wrap pdf-extraction-upload-wrap" style={{ height: "calc(100vh - 100px)" }}>
                            <div className="migration-header-stepper-section">
                                <div className="zenqore-stepper-section">
                                    <Steps current={this.state.activeStep} currentStatus="process" vertical={false} >
                                        <Steps.Item title="Enter Details" onClick={() => { this.changetoStep(0, this.state.activeStep) }} style={{ cursor: "pointer" }} />
                                        <Steps.Item title="Payment Method" onClick={() => { this.changetoStep(1, this.state.activeStep) }} style={{ cursor: "pointer" }} />
                                        <Steps.Item title="Process Fees" style={{ cursor: "initial" }} />
                                        <Steps.Item title="Confirmation" style={{ cursor: "initial" }} />
                                    </Steps>
                                </div>
                            </div>
                            <div className="migration-body-content-section" style={{ backgroundColor: "#ffffff !important", height: "calc(100vh - 215px)", overflowY: "auto", paddingBottom: "30px", border: "1px solid #dfe1e6" }}>
                                {this.getStepContent(this.state.activeStep)}
                            </div>
                        </div>
                    </div>

                </React.Fragment>
                :
                // feecollection report
                <React.Fragment>
                    {this.state.isLoader && <Loader />}
                    < div className="reports-student-fees list-of-students-mainDiv" >
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Fees Collection</p>
                        </div>
                        <div className="reports-body-section print-hd">
                            <React.Fragment>
                                <ContainerNavbar containerNav={this.state.containerNav} onAddNew={this.receivePayment} onDownload={() => this.onDownloadEvent()} />
                                <div className="print-time">{this.state.printTime}</div>
                            </React.Fragment>
                            <div className="reports-data-print-table">
                                <div className="transaction-review-mainDiv">
                                    <table className="transaction-table-review reports-tableRow-header col-split-td" >
                                        <thead>
                                            <tr>
                                                {this.state.tableHeader.map((data, i) => {
                                                    return <th className={'student-fee ' + String(data).replace(' ', '')} key={i + 1}>{data}</th>
                                                })}
                                            </tr>
                                        </thead>
                                        {this.state.printReportArr.length > 0 ?
                                            <tbody className="studentfee-reports-table">
                                                {this.state.printReportArr.map((data, i) => {
                                                    return (
                                                        <tr key={i + 1} id={i + 1}>
                                                            {/* <td className="transaction-sno">{data.txnId}</td> */}
                                                            <td className="transaction-vch-type">{data.DemandId}</td>
                                                            <td className="transaction-vch-num" >{data.regId}</td>
                                                            <td className="transaction-vch-num" >{data.studentName}</td>
                                                            <td className="transaction-vch-type">{data.academicYear}</td>
                                                            <td className="transaction-vch-type">{data.classBatch}</td>
                                                            <td className="transaction-particulars">
                                                                {data.description.map((dataOne, c) => {
                                                                    return (
                                                                        <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}>{dataOne.name}</p>
                                                                    )
                                                                })}
                                                            </td>
                                                            <td className="transaction-debit">
                                                                {data.description.map((dataOne, c) => {
                                                                    return (
                                                                        <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}>{Number(Math.abs(dataOne.due)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                                                    )
                                                                })}
                                                            </td>
                                                            <td className="transaction-debit" >
                                                                {data.description.map((dataOne, c) => {
                                                                    return (
                                                                        <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}>{Number(Math.abs(dataOne.paid)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                                                    )
                                                                })}
                                                            </td>
                                                            <td className="transaction-debit">
                                                                {data.description.map((dataOne, c) => {
                                                                    return (
                                                                        <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}>{Number(Math.abs(dataOne.balance)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                                                    )
                                                                })}
                                                            </td>
                                                            <td className="transaction-debit">
                                                                {data.description.map((dataOne, c) => {
                                                                    var paidDateStr = String(dataOne.paidDate).split('/')['1'] + '/' + String(dataOne.paidDate).split('/')['0'] + '/' + String(dataOne.paidDate).split('/')['2']
                                                                    return (
                                                                        <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}> {dataOne.paidDate !== '-' ? <DateFormatter date={paidDateStr} format={this.context.dateFormat} /> : '-'} </p>
                                                                    )
                                                                })}
                                                            </td>
                                                            <td className="transaction-debit">
                                                                {data.description.map((dataOne, c) => {
                                                                    return (
                                                                        <p>{dataOne.txnId == null ? "-" : dataOne.txnId}</p>
                                                                    )
                                                                })}
                                                            </td>
                                                            <td className="transaction-vch-type">{Number(Math.abs(data.refundAmount)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</td>
                                                            <td className="transaction-debit">
                                                                {data.description.map((dataOne, c) => {
                                                                    return (
                                                                        <p style={{ color: dataOne.status == "Pending" ? "#FF5630" : dataOne.status == "Paid" ? "#00875A" : "#000000", fontWeight: String(dataOne.name).includes("Total") ? "" : '' }}>{dataOne.status}</p>
                                                                    )
                                                                })}
                                                            </td>
                                                        </tr>
                                                    )
                                                })}
                                            </tbody> :
                                            <tbody>
                                                <tr>
                                                    {this.state.noData ? <td className="noprog-txt" colSpan={this.state.tableHeader.length}>No data...</td> :
                                                        <td className="noprog-txt" colSpan={this.state.tableHeader.length}>Fetching the data...</td>
                                                    }

                                                </tr></tbody>
                                        }
                                    </table>
                                </div>
                                <div>
                                    {this.state.printReportArr.length == 0 ? null :
                                        <PaginationUI
                                            total={this.state.totalRecord}
                                            onPaginationApi={this.onPaginationChange}
                                            totalPages={this.state.totalPages}
                                            limit={this.state.limit}
                                            currentPage={this.state.page}
                                        />}
                                </div>
                            </div>
                        </div>
                    </div>
                </React.Fragment>
        )
    }
}
export default withRouter(ReceivePayment) 