import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import '../../../../scss/receive-payment.scss';
import Button from '@material-ui/core/Button';
import ParentInfo from './parent-info.json';
import ZenForm from '../../../input/form';
import axios from 'axios';
import { Alert } from 'rsuite';
import { CircularProgress } from "@material-ui/core";
class PaymentPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            cashFormData: ParentInfo.CashPayment,
            cardFormData: ParentInfo.CardPayment,
            ChequePayment: ParentInfo.ChequePayment,
            paymentType: this.props.paymentType,
            paymentAmount: this.props.paymentAmount,
            studentID: this.props.studentID,
            guardianDetails: this.props.guardianDetails,
            txnNumber: '',
            cardType: '',
            nameOnCard: '',
            cardNumber: '',
            studentData: this.props.studentData,
            env: JSON.parse(localStorage.getItem('env')),
            orgId: localStorage.getItem("orgId"),
            authToken: localStorage.getItem("auth_token"),
            circularLoader: false,
            studentFeeID: this.props.studentFeeID,
            bankName:""
        }
    }
    componentDidMount() {
        let studentData = this.props.studentData
        console.log(studentData)
        this.state.cashFormData.map((data) => {
            if (data.name == "cashBtn") {
                data['label'] = 'Confirm payment' + ' ' + this.formatCurrency(this.state.paymentAmount)
            }
            if (data.name == "studentID") {
                data['defaultValue'] = this.state.studentID
            }
            if (data.name == "parentName") {
                data['defaultValue'] = this.state.guardianDetails.firstName
            }
        })
        this.state.cardFormData.map((data) => {
            if (data.name == "cardBtn") {
                data['label'] = 'Confirm payment' + ' ' + this.formatCurrency(this.state.paymentAmount)
            }
            if (data.name == "studentID") {
                data['defaultValue'] = this.state.studentID
            }
            if (data.name == "parentName") {
                data['defaultValue'] = this.state.guardianDetails.firstName
            }
        })
        this.state.ChequePayment.map((data) => {
            if (data.name == "studentID") {
                data['defaultValue'] = this.state.studentID
            }
            if (data.name == "parentName") {
                data['defaultValue'] = this.state.guardianDetails.firstName
            }
            if (data.name == "chequeBtn") {
                data['label'] = 'Confirm payment' + ' ' + this.formatCurrency(this.state.paymentAmount)
            }
        })
    }
    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    onCardSubmit = () => {
        this.state.cardFormData.map((data) => {
            if (data.name == "cardBtn") {
                data['label'] = 'Confirm Payment'
            }
        })
        let payloadData = this.state.studentData;
        let payload = {
            "transactionDate": new Date(),
            "relatedTransactions": this.state.studentFeeID,
            "emailCommunicationRefIds": payloadData[0].demandNote.emailCommunicationRefIds[0],
            "transactionType": "eduFees",
            "transactionSubType": "feePayment",
            "studentFeeMap": payloadData[0].studentFeeMapDetails.displayName,
            "amount": Number(this.state.paymentAmount),
            "status": "initiated",
            "data": {
                "orgId": localStorage.getItem('orgId'),
                "transactionType": "eduFees",
                "transactionSubType": "feePayment",
                "mode": "card",
                "method": "otc",
                "modeDetails": {
                    "netBankingType": null,
                    "walletType": null,
                    "instrumentNo": null,
                    "cardType": this.state.cardType,
                    "nameOnCard": this.state.nameOnCard,
                    "cardNumber": this.state.cardNumber,
                    "instrumentDate": new Date(),
                    "bankName": null,
                    "branchName": null,
                    "transactionId": this.state.txnNumber,
                    "remarks": null
                },
                "amount": Number(this.state.paymentAmount)
            },
            "paymentTransactionId": this.state.txnNumber,
            "createdBy": localStorage.getItem('orgId')
        }
        console.log("payload", payload)
        axios.post(`${this.state.env['zqBaseUri']}/edu/feePaymentWithReceipt`, payload, {
            headers: {
                "Authorization": localStorage.getItem('auth_token')
            }
        }).then(response => {
            console.log("post responsew", response)
            let txnNumber = this.state.txnNumber;
            let receiptID = response.data.receiptKey;
            let nextProps = {
                receiptID: receiptID,
                txnNumber: txnNumber,
                checkPaymentStatus: true
            }
            // Alert.success("Successful Fee Payment")
            this.setState({ circularLoader: false });
            this.state.cardFormData.map((data) => {
                if (data.name == "cardBtn") {
                    data['label'] = 'Confirm Payment'
                }
            })
            this.props.onCardSubmit(nextProps);
        }).catch(err => {
            this.state.cardFormData.map((data) => {
                if (data.name == "cardBtn") {
                    data['label'] = 'Confirm Payment'
                }
            })
            console.log("post responsew", err)
            if (err.response.status === 400) {
                this.setState({ circularLoader: false });
                this.setState({ isLoader: false });
                Alert.error(err.response.data.message)
            }
            else {
                Alert.error('Payment Failed')
                this.setState({ circularLoader: false });
                this.setState({ isLoader: false })
            }
        })
        this.state.cardFormData.map((data) => {
            if (data.name == "cardBtn") {
                data['label'] = 'Confirm Payment'
            }
        })
    }
    onCashSubmit = () => {
        this.setState({ circularLoader: true });
        this.state.cashFormData.map((data) => {
            if (data.name == "cashBtn") {
                data['label'] = 'Confirm Payment'
            }
        })
        let payloadData = this.state.studentData;
        let payload = {
            "transactionDate": new Date(),
            "relatedTransactions": this.state.studentFeeID,
            "emailCommunicationRefIds": payloadData[0].demandNote.emailCommunicationRefIds[0],
            "transactionType": "eduFees",
            "transactionSubType": "feePayment",
            "studentFeeMap": payloadData[0].studentFeeMapDetails.displayName,
            "amount": Number(this.state.paymentAmount),
            "status": "initiated",
            "data": {
                "orgId": localStorage.getItem('orgId'),
                "transactionType": "eduFees",
                "transactionSubType": "feePayment",
                "mode": "cash",
                "method": "otc",
                "modeDetails": {
                    "netBankingType": null,
                    "walletType": null,
                    "instrumentNo": null,
                    "cardType": null,
                    "nameOnCard": null,
                    "cardNumber": null,
                    "instrumentDate": new Date(),
                    "bankName": null,
                    "branchName": null,
                    "transactionId": null,
                    "remarks": null
                },
                "amount": Number(this.state.paymentAmount)
            },
            "paymentTransactionId": null,
            "createdBy": localStorage.getItem('orgId')
        }
        console.log("payload", payload)
        axios.post(`${this.state.env['zqBaseUri']}/edu/feePaymentWithReceipt`, payload, {
            headers: {
                "Authorization": localStorage.getItem('auth_token')
            }
        }).then(response => {
            console.log("post response", response)
            let txnNumber = response.data.data.data.displayName;
            let receiptID = response.data.receiptKey;
            let nextProps = {
                receiptID: receiptID,
                txnNumber: txnNumber,
                checkPaymentStatus: true
            }
            console.log(nextProps)
            // Alert.success("Successful Fee Payment")
            this.setState({ circularLoader: false });
            this.state.cashFormData.map((data) => {
                if (data.name == "cashBtn") {
                    data['label'] = 'Confirm Payment'
                }
            })
            this.props.onCashSubmit(nextProps);
        }).catch(err => {
            this.state.cashFormData.map((data) => {
                if (data.name == "cashBtn") {
                    data['label'] = 'Confirm Payment'
                }
            })
            console.log("post responsew", err)
            if (err.response.status === 400) {
                this.setState({ circularLoader: false });
                this.setState({ isLoader: false });
                Alert.error(err.response.data.message)
            }
            else {
                Alert.error('Payment Failed')
                this.setState({ circularLoader: false });
                this.setState({ isLoader: false })
            }
        })
        this.state.cashFormData.map((data) => {
            if (data.name == "cashBtn") {
                data['label'] = 'Confirm Payment'
            }
        })
    }
    onChequeSubmit = () => {
        console.log("Form submitted", this.state);
        this.setState({ circularLoader: true });
        this.state.cashFormData.map((data) => {
            if (data.name == "chequeBtn") {
                data['label'] = 'Confirm Payment'
            }
        })
        let payloadData = this.state.studentData;
        let payload = {
            "transactionDate": new Date(),
            "relatedTransactions": this.state.studentFeeID,
            "emailCommunicationRefIds": payloadData[0].demandNote.emailCommunicationRefIds[0],
            "transactionType": "eduFees",
            "transactionSubType": "feePayment",
            "studentFeeMap": payloadData[0].studentFeeMapDetails.displayName,
            "amount": Number(this.state.paymentAmount),
            "status": "initiated",
            "data": {
                "orgId": localStorage.getItem('orgId'),
                "transactionType": "eduFees",
                "transactionSubType": "feePayment",
                "mode": "cheque",
                "method": "otc",
                "modeDetails": {
                    "netBankingType": null,
                    "walletType": null,
                    "instrumentNo": null,
                    "cardType": null,
                    "nameOnCard": null,
                    "cardNumber": null,
                    "instrumentDate": new Date(),
                    "bankName": null,
                    "branchName": null,
                    "transactionId": this.state.txnNumber, // cheque number
                    "remarks": null,
                    "bankName": this.state.bankName,
                    "branchType": this.state.BranchType,
                    "chequeStatus": this.state.ChequeStatus
                },
                "amount": Number(this.state.paymentAmount)
            },
            "paymentTransactionId": this.state.txnNumber,
            "createdBy": localStorage.getItem('orgId')
        }
        console.log("payload", payload)
        axios.post(`${this.state.env['zqBaseUri']}/edu/feePaymentWithReceipt`, payload, {
            headers: {
                "Authorization": localStorage.getItem('auth_token')
            }
        }).then(response => {
            console.log("post response", response)
            let txnNumber = response.data.data.data.displayName;
            let receiptID = response.data.receiptKey;
            let nextProps = {
                receiptID: receiptID,
                txnNumber: txnNumber,
                checkPaymentStatus: true
            }
            console.log(nextProps)
            // Alert.success("Successful Fee Payment")
            this.setState({ circularLoader: false });
            this.state.cashFormData.map((data) => {
                if (data.name == "cashBtn") {
                    data['label'] = 'Confirm Payment'
                }
            })
            this.props.onCashSubmit(nextProps);
        }).catch(err => {
            this.state.cashFormData.map((data) => {
                if (data.name == "cashBtn") {
                    data['label'] = 'Confirm Payment'
                }
            })
            console.log("post responsew", err)
            if (err.response.status === 400) {
                this.setState({ circularLoader: false });
                this.setState({ isLoader: false });
                Alert.error(err.response.data.message)
            }
            else {
                Alert.error('Payment Failed')
                this.setState({ circularLoader: false });
                this.setState({ isLoader: false })
            }
        })
    }
    onChequeChanges = (value, item, e, dataS) => {
        console.log(value, item, e, dataS);
        if (item.name == "ChequeNumber") {
            this.setState({ txnNumber: value });
        }
        if (item.name == "BankName") {
            this.setState({ bankName: value });
        }
        if (item.name == "BranchType") {
            this.setState({ BranchType: value });
        }
        if (item.name == "ChequeStatus") {
            this.setState({ ChequeStatus: value });
        }
    }
    onCardChanges = (value, item, e, dataS) => {
        console.log(value, item, e, dataS)``
        if (item.name == "transactionNumber") {
            this.setState({ txnNumber: value });
        }
        if (item.label == "Visa") {
            this.setState({ cardType: value });
        }
        if (item.name == "cardName") {
            this.setState({ nameOnCard: value });
        }
        if (item.name == "Card Number") {
            this.setState({ cardNumber: value });
        }
    }
    cleanDatas = () => {}
    render() {
        return (
            <React.Fragment>
                {this.state.circularLoader ?
                    <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                        <CircularProgress size={24} />
                    </div> : null}
                <div className="receive-payment-main">
                    <div className="razor-pay">
                        <div className="parent-info">
                            {this.state.paymentType == 'Card Payment' ?
                                <div className="card-form"><ZenForm inputData={this.state.cardFormData} onSubmit={this.onCardSubmit} onInputChanges={this.onCardChanges} /></div> :
                                this.state.paymentType == 'Cash Payment' ?
                                    <div className="cash-form"><ZenForm inputData={this.state.cashFormData} onSubmit={this.onCashSubmit} /></div> :
                                    this.state.paymentType == 'Cheque Payment' ?
                                        <div className="cheque-form"><ZenForm inputData={this.state.ChequePayment} onSubmit={this.onChequeSubmit} onInputChanges={this.onChequeChanges} cleanData={this.cleanDatas} /></div> :
                                        null
                            }
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default withRouter(PaymentPage)