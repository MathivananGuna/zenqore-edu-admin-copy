import React from "react";
import '../../../../scss/payment-portal.scss';
import '../../../../scss/portal-stepper.scss';
import '../../../../scss/student-reports.scss';
import '../../../../scss/receive-payment.scss';
import '../../../../scss/vkgi-template.scss';
import '../../../../scss/student.scss';
import '../../../../scss/setup.scss';
import Loader from "../../../../utils/loader/loaders";
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../utils/Table/table-component";
import PaginationUI from "../../../../utils/pagination/pagination";
// import ZenTabs from '../../../input/tabs';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import DemandNoteAllJson from './send-demand-note.json';
import axios from "axios";
import moment from 'moment';
// import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CircularProgress from '@material-ui/core/CircularProgress';
import DateFormatContext from '../../../../gigaLayout/context';
import Checkbox from "@material-ui/core/Checkbox";
import DateFormatter from '../../../date-formatter/date-formatter';
import { Steps } from 'rsuite';
import ZenForm from '../../../input/form';
// import RefundConfirmation from '../refund/refund-confirmation';
import WarningIcon from '@material-ui/icons/Warning';
import xlsx from 'xlsx';
import PreviewDemandNote from './preview-demand-note.json';
import vkgiLogo from '../../../../assets/images/vkgi-logo.png';
// import PrintSVG from '../../../../assets/icons/table-print-icon.svg';
// import DownloadSVG from '../../../../assets/icons/table-download-icon.svg';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
class SendDemandNote extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            campusId: localStorage.getItem('campusId'),
            userId: localStorage.getItem('userId'),
            orgId: localStorage.getItem('orgId'),
            namespace: localStorage.getItem('baseURL'),
            printReportArr: [],
            containerNav: {
                isBack: false,
                name: "List of Demand Notes",
                isName: true,
                isSearch: true,
                isSort: false,
                isPrint: true,
                isDownload: true,
                isShare: false,
                isNew: true,
                newName: "New",
                isSubmit: false,
            },
            tableHeader: ["SelectIcon", "ID", "REG ID", "STUDENT NAME", "ACADEMIC YEAR", "CLASS/BATCH", "ISSUED DATE", "DUE DATE", "AMOUNT", "STATUS"],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 0,
            demandNoteTable: [],
            isLoader: false,
            viewAllDemandNoteList: true,
            activeStep: 0,
            getStudentDetailsForm: DemandNoteAllJson.FetchStudentDetails,
            getParticularStuData: null,
            viewStudentData: false,
            sendingStatus: false,
            totalResData: [],
            snackbar: { openNotification: false, NotificationMessage: "", status: "success" },
            errorSendDemand: "",
            studentNameDemand: "",
            studentIdDemand: "",
            demandNotedate: '',
            apiResMsg: "",
            LoaderStatus: false,
            isNew: false,
            isPreview: false,
            previewListForm: PreviewDemandNote.formJson,
            previewName: '',
            instituteId: localStorage.getItem("instituteId"),
            settingDetails: null,
            previewTableHead: ['Student Reg. ID', 'Student Name', 'Class/Batch', 'Demand Note ID', 'Tuition Fee', 'Paid Amount', 'TOTAL DUE'],
            totalDemandList: null,
            searchValue: '',
            filterKey: 'All'
        }
    }
    static contextType = DateFormatContext;
    componentDidMount() {
        let tableHeader = ["SelectIcon", "ID", `${this.context.reportLabel ? this.context.reportLabel : "REG ID"}`, "STUDENT NAME", "ACADEMIC YEAR", `${this.context.classLabel ? this.context.classLabel : "CLASS/BATCH"}`, "ISSUED DATE", "DUE DATE", "AMOUNT", "STATUS"];
        let previewTableHead = [`${this.context.reportLabel ? this.context.reportLabel : "Student Reg. ID"}`, 'Student Name', `${this.context.classLabel ? this.context.classLabel : "CLASS/BATCH"}`, 'Demand Note ID', 'Tuition Fee', 'Paid Amount', 'TOTAL DUE'];
        this.setState({ tableHeader, previewTableHead })
        this.getAllDemandList();
        this.getSettingList()
        this.resetForm();
    }
    getSettingList = () => {
        axios.get(`${this.state.env['zqBaseUri']}/setup/settings?instituteid=${this.state.instituteId}`, {
            headers: {
                'Authorization': localStorage.getItem("auth_token")
            }
        })
            .then(res => {
                console.log(res);
                this.setState({ settingDetails: res.data[0] })
            })
            .catch(err => { console.log(err) })
    }
    getAllDemandList = () => {
        this.setState({ isLoader: true })
        // let demandNoteTable = []
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ printReportArr: [] }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/demandNote?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&classbatchName=All&page=${this.state.page}&limit=${this.state.limit}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token"),
                }
            })
                .then(resp => {
                    console.log("resp", resp);
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    this.setState({ printReportArr: resp.data.data.reverse(), totalPages: resp.data.totalPages, totalRecord: resp.data.totalRecord, noData })
                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log("error", err)
                    this.setState({ printReportArr: [], isLoader: false, noData: true })
                })

        })
    }
    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    printScreen = () => {
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        this.setState({ isLoader: false }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/reports/demandNote?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&pagination=false`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    console.log(resp);
                    this.setState({
                        printable: true,
                        printReportArr: resp.data.data.reverse()
                    }, () => {
                        this.setState({ printable: false })
                        window.print();
                        this.getAllDemandList();
                    })
                })
        });
    }

    SendNewDemandNote = () => {
        this.setState({ viewAllDemandNoteList: false, isNew: true })
    }
    onPreviewStudentList = (e, item, index) => {
        console.log(e, item, index);
    }
    onPreviewStudentList1 = (e, a, b, c) => {
        console.log(e, a, b, c);
    }
    BackToDemandNoteList = () => {
        this.setState({
            viewAllDemandNoteList: true, isNew: false, isPreview: false, activeStep: 0, errorSendDemand: "",
            studentNameDemand: "",
            studentIdDemand: "",
            apiResMsg: "",
            getParticularStuData: null
        })
        this.resetForm();
    }
    resetForm = () => {
        let setbacktoDefaultForm = DemandNoteAllJson.FetchStudentDetails
        setbacktoDefaultForm.map((task) => {
            if (task.type == "text") {
                delete task['defaultValue']
                task['readOnly'] = false
                task['error'] = false
                task['required'] = task['requiredBoolean']
                task['validation'] = false
                delete task['clear']
            }
            else { }
        })
    }
    searchHandle = (searchValue) => {
        console.log('searchValue', searchValue)
        this.setState({ isLoader: true })
        this.setState({ searchValue: searchValue });
        let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
        if (String(searchValue).length > 0) {
            return axios.get(`${this.state.env['zqBaseUri']}/edu/reports/demandNote?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}&page=${this.state.page}&limit=${this.state.limit}&searchKey=${searchValue}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(resp => {
                    let noData;
                    if (resp.data.data.length === 0) {
                        noData = true;
                    } else {
                        noData = false;
                    }
                    this.setState({
                        printReportArr: resp.data.data.reverse(),
                        totalPages: resp.data.totalPages,
                        totalRecord: resp.data.totalRecord,
                        noData,
                    })

                    this.setState({ isLoader: false })
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ isLoader: false, printReportArr: [], noData: true, dataInfo: "No Data" })
                })
        }
        else {
            this.getAllDemandList()
        }
    }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            if (this.state.searchValue.length == 0) {
                this.getAllDemandList()
            }
            else {
                this.searchHandle(this.state.searchValue)
            }
        });
        console.log(page, limit);
    };
    onSubmit = (formdata) => {
        if (String(formdata[0].value).length > 0) {
            console.log(formdata[0].value);
            let headers = {
                'Authorization': this.state.authToken
            };
            this.setState({ LoaderStatus: true })
            let userId = localStorage.getItem('role').toLowerCase() == 'admin' ? "all" : this.state.userId
            axios.get(`${this.state.env['zqBaseUri']}/edu/getStudentFeesById/${formdata[0].value}?orgId=${this.state.orgId}&campusId=${this.state.campusId}&userId=${userId}`, { headers })
                .then(res => {
                    console.log(res);
                    this.setState({ activeStep: this.state.activeStep + 1, LoaderStatus: false })
                    let a = {};
                    a.ID = res.data.data.studentDetails.regId;
                    a.Name = res.data.data.studentDetails.firstName + " " + res.data.data.studentDetails.lastName;
                    a['BATCH/CLASS'] = res.data.data.programPlanDetails.description;
                    a['Fees Structure'] = res.data.data.feeStructureId;
                    a['Annual Amount'] = Number(res.data.data.totalAmount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' });
                    a['Paid Amount'] = Number(res.data.data.paidAmount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' });
                    a['Pending Amount'] = res.data.data.pendingAmount == null ? "₹0.00" : Number(res.data.data.pendingAmount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' });
                    a['Created On'] = moment(new Date(res.data.data.createdAt)).format(this.context.dateFormat);
                    a['Created By'] = res.data.data.createdBy;
                    a['Item'] = JSON.stringify(res.data.data);
                    this.setState({ getParticularStuData: [a], viewStudentData: true, totalResData: res.data.data })
                    console.log(this.state.getParticularStuData);
                })
                .catch(err => {
                    console.log(err);
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Enter valid student data", status: "error", }
                    this.setState({ snackbar: snackbarUpdate, LoaderStatus: false }, () => {
                        let snackbarUpdate = { openNotification: false, NotificationMessage: "", status: "error", }
                        setTimeout(() => { this.setState({ snackbar: snackbarUpdate }) }, 2000)
                    })
                })
        }
        else { }
    }
    confirmStudentFun = () => {
        let payloadDetails = this.state.totalResData;
        console.log(payloadDetails);
        let feeBreakUpData = [];
        payloadDetails.feeDetails.map((data, i) => {
            if (i === 0) {
                feeBreakUpData.push({
                    "feeTypeId": data._id,
                    "feeTypeCode": data.displayName,
                    "amount": i === 0 ? payloadDetails.totalAmount : 0,
                    "feeType": data.title
                })
            }
            else { }
            // feeBreakUpData.push({
            //     "feeTypeId": data._id,
            //     "feeTypeCode": data.displayName,
            //     "amount": i === 0 ? payloadDetails.totalAmount : 0,
            //     "feeType": data.title
            // })
        })
        this.setState({ activeStep: this.state.activeStep + 1, sendingStatus: true, studentNameDemand: String(payloadDetails.studentName), studentIdDemand: payloadDetails.studentDetails.regId })
        console.log(this.state.totalResData);
        let payloadData = {
            "displayName": "",
            "transactionType": "",
            "transactionSubType": "",
            "transactionDate": "",
            "studentId": payloadDetails.studentId,
            "studentRegId": payloadDetails.studentDetails.regId,
            "studentName": String(payloadDetails.studentName),
            "parentName": payloadDetails.guardianDetails[0].firstName + " " + payloadDetails.guardianDetails[0].lastName,
            "class": payloadDetails.programPlanDetails.title,
            "academicYear": payloadDetails.programPlanDetails.academicYear,
            "orgId": this.state.orgId,
            "programPlan": payloadDetails.programPlanDetails._id,
            "amount": payloadDetails.pendingAmount,
            "dueDate": payloadDetails.dueDate,
            // "emailCommunicationRefIds":"mathivanan.g@zenqore.com",
            "emailCommunicationRefIds": payloadDetails.guardianDetails[0].email == 'NA' || payloadDetails.guardianDetails[0].email == '' || payloadDetails.guardianDetails[0].email == null || payloadDetails.guardianDetails[0].email == '-' ? payloadDetails.studentDetails.email : payloadDetails.guardianDetails[0].email,
            "smsCommunicationRefIds": payloadDetails.guardianDetails[0].mobile == 'NA' || payloadDetails.guardianDetails[0].mobile == '' || payloadDetails.guardianDetails[0].mobile == null || payloadDetails.guardianDetails[0].mobile == '-' ? payloadDetails.studentDetails.phoneNo : payloadDetails.guardianDetails[0].mobile,
            "status": "",
            "relatedTransactions": [],
            "data": {
                "orgId": this.state.orgId,
                "displayName": "",
                "studentId": payloadDetails.studentId,
                "studentRegId": payloadDetails.studentDetails.regId,
                "class": payloadDetails.programPlanDetails.title,
                "academicYear": payloadDetails.programPlanDetails.academicYear,
                "issueDate": "",
                "dueDate": "",
                "feesBreakUp": feeBreakUpData
            },
            "createdBy": localStorage.getItem('userId'),
            "campusId": localStorage.getItem('campusId') ? localStorage.getItem('campusId') : 'all',
            "studentFeeMapId": payloadDetails.displayName,
            "programPlan": payloadDetails.programPlanDetails._id
        }
        console.log(payloadData);
        let finalPayloadNew = [payloadData];
        console.log(finalPayloadNew);
        let headers = {
            'Authorization': this.state.authToken
        };
        axios.post(`${this.state.env["zqBaseUri"]}/edu/multipleDemand`, finalPayloadNew, { headers })
            .then(res => {
                console.log(res);
                this.getAllDemandList();
                this.resetForm();
                this.setState({ errorSendDemand: false, sendingStatus: false, apiResMsg: res.data.message, LoaderStatus: false })
                // setTimeout(() => {
                //     this.BackToDemandNoteList()
                // }, 1500);
            })
            .catch(err => {
                console.log('error resp', err.response);
                if (err.response === undefined) { this.setState({ sendingStatus: false, errorSendDemand: true, apiResMsg: "Failed to send demand note. Network error", LoaderStatus: false }) }
                else {
                    this.setState({ sendingStatus: false, errorSendDemand: true, apiResMsg: err.response.data.Error[0].message, LoaderStatus: false })
                }
            })
    }
    cancelStudentFun = () => {
        this.setState({ activeStep: this.state.activeStep - 1, getParticularStuData: null })
    }
    finishProcess = () => {
        this.setState({ activeStep: 0, getParticularStuData: null })
    }
    getStepContent = (stepIndex) => {
        switch (stepIndex) {
            case 0:
                return <React.Fragment>
                    <div className="receive-payment-main">
                        {this.state.circularLoader ?
                            <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                                <CircularProgress size={24} />
                            </div> : null}
                        <div className="receive-payment-table">
                            <React.Fragment >
                                <div className="payment-form">
                                    <ZenForm inputData={DemandNoteAllJson.FetchStudentDetails} onSubmit={this.onSubmit} />
                                </div>
                            </React.Fragment>
                        </div>
                    </div>
                </React.Fragment>
            case 1:
                return (
                    <div className="receive-payment-main student-list-demand-note">
                        {this.state.viewStudentData == true ?
                            <React.Fragment>
                                <div className="student-detail-nxt-btn-div">
                                    <button title="click to send demand note" className="send-demand-note-btn-new-data" onClick={this.confirmStudentFun}>Send</button>
                                    <button title="click to go back" className="send-demand-note-btn-new" onClick={this.cancelStudentFun}>Cancel</button>
                                </div>
                                <div className="table-section-student-list">
                                    {this.state.getParticularStuData !== null ?
                                        < ZqTable
                                            allSelect={this.allSelect}
                                            data={this.state.getParticularStuData}
                                            rowClick={(e, item, index) => { this.onPreviewStudentList(e, item, index) }}
                                            onRowCheckBox={(item, a, b, c) => { this.onPreviewStudentList1(item, a, b, c) }} /> :
                                        null}
                                </div>
                            </React.Fragment>
                            : <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                                <CircularProgress size={24} />
                            </div>
                        }
                    </div>
                )
            case 2:
                return <React.Fragment>
                    <div className="receive-payment-main student-list-demand-note">
                        {this.state.sendingStatus == true ?
                            <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                                <CircularProgress size={24} />
                            </div> :
                            <div className="send-demand-note-status-div">
                                {this.state.errorSendDemand == false ?
                                    <div className="success-div">
                                        <CheckCircleIcon className="check-circle" />
                                        <p>Demand Note Sent Successfully </p>
                                        <p> Student Name: <span> {this.state.studentNameDemand} </span> </p>
                                        <p> Student ID: <span> {this.state.studentIdDemand} </span> </p>
                                        {/* for the student <span></span><br /></span><br /> */}
                                        {/* <button className="finish-button-demand" onClick={this.finishProcess}>FINISH</button> */}
                                        <button className="finish-button-demand" onClick={this.BackToDemandNoteList}>FINISH</button>
                                    </div> :
                                    this.state.errorSendDemand == true ?
                                        <div className="error-div">
                                            <WarningIcon className="check-circle-warning" />
                                            <p>Failed to send Demand Note for the student <span>{this.state.studentNameDemand}</span><br />Student ID: <span>{this.state.studentIdDemand}</span><br /> Message: <span>{this.state.apiResMsg}</span></p>
                                            <button className="finish-button-demand" onClick={this.finishProcess}>Go Back</button>
                                        </div> : null
                                }
                            </div>
                        }
                    </div>
                </React.Fragment >
            default:
                return null;
        }
    }
    onDownloadEvent = () => {
        this.setState({ isLoader: true });
        var createXlxsData = [];
        let regId = this.context.reportLabel;
        let batch = this.context.classLabel || "CLASS/BATCH";
        this.state.printReportArr.map((data) => {
            data.data.students[0].feesBreakup.map((dataOne, c) => {
                let issuedDate;
                if (data.todayDate) {
                    if (data.todayDate.includes('-')) { issuedDate = data.todayDate.split("-") }
                    else if (data.todayDate.includes('/')) { issuedDate = data.todayDate.split("/") }
                }
                let dueDate;
                if (data.data.students[0].dueDate) {
                    if (data.data.students[0].dueDate.includes('-')) { dueDate = data.data.students[0].dueDate.split("-") }
                    else if (data.data.students[0].dueDate.includes('/')) { dueDate = data.data.students[0].dueDate.split("/") }
                }
                if (String(dataOne.description).toLowerCase() != "total") {
                    createXlxsData.push({
                        "DEMAND NOTE ID": data.displayName,
                        [regId]: data.data.students[0].regId,
                        "STUDENT NAME": data.data.students[0].studentName,
                        "ACADEMIC YEAR": data.data.students[0].academicYear,
                        [batch]: data.data.students[0].class,
                        // "ISSUED DATE": data.todayDate,
                        // "DUEDATE": moment(new Date(data.data.students[0].dueDate)).format(this.context.dateFormat),
                        "ISSUED DATE": data.todayDate && data.todayDate !== "NA" && data.todayDate !== "-" ? moment(new Date(issuedDate[2], issuedDate[1] - 1, issuedDate[0])).format(this.context.dateFormat) : '-',
                        "DUEDATE": data.data.students[0].dueDate && data.data.students[0].dueDate !== "NA" && data.data.students[0].dueDate !== "-" ? moment(new Date(dueDate[2], dueDate[1] - 1, dueDate[0])).format(this.context.dateFormat) : '-',
                        "DESCRIPTION": dataOne.description,
                        "AMOUNT": this.formatCurrency(dataOne.amount),
                        "STATUS": data.status
                    })
                }
            })
        })
        let filterDuplicate = this.uniqueArr(createXlxsData, ['DEMAND NOTE ID', 'STUDENT NAME'])
        var ws = xlsx.utils.json_to_sheet(filterDuplicate);
        var wb = xlsx.utils.book_new();
        xlsx.utils.book_append_sheet(wb, ws, "Demand Note Reports");
        xlsx.writeFile(wb, "demand_note_reports.xlsx");
        //xlsx.writeFile(wb, "demand_note_reports.csv");
        this.setState({ isLoader: false })

        // axios.get(`${this.state.env['zqBaseUri']}/edu/reports/demandNote?orgId=${this.state.orgId}`, {
        //     headers: {
        //         'Authorization': localStorage.getItem("auth_token")
        //     }
        // })
        //     .then(resp => {
        //         console.log(resp);
        //         var createXlxsData = []
        //         let regId = this.context.reportLabel;
        //         let batch = this.context.classLabel || "CLASS/BATCH"
        //         resp.data.data.map(data => {
        //             data.data.students[0].feesBreakup.map((dataOne, c) => {
        //                 let issuedDate;
        //                 if (data.todayDate) {
        //                     if (data.todayDate.includes('-')) { issuedDate = data.todayDate.split("-") }
        //                     else if (data.todayDate.includes('/')) { issuedDate = data.todayDate.split("/") }
        //                 }
        //                 let dueDate;
        //                 if (data.data.students[0].dueDate) {
        //                     if (data.data.students[0].dueDate.includes('-')) { dueDate = data.data.students[0].dueDate.split("-") }
        //                     else if (data.data.students[0].dueDate.includes('/')) { dueDate = data.data.students[0].dueDate.split("/") }
        //                 }
        //                 if (String(dataOne.description).toLowerCase() != "total") {
        //                     createXlxsData.push({
        //                         "DEMAND NOTE ID": data.displayName,
        //                         [regId]: data.data.students[0].regId,
        //                         "STUDENT NAME": data.data.students[0].studentName,
        //                         "ACADEMIC YEAR": data.data.students[0].academicYear,
        //                         [batch]: data.data.students[0].class,
        //                         // "ISSUED DATE": data.todayDate,
        //                         // "DUEDATE": moment(new Date(data.data.students[0].dueDate)).format(this.context.dateFormat),
        //                         "ISSUED DATE": data.todayDate && data.todayDate !== "NA" && data.todayDate !== "-" ? moment(new Date(issuedDate[2], issuedDate[1] - 1, issuedDate[0])).format(this.context.dateFormat) : '-',
        //                         "DUEDATE": data.data.students[0].dueDate && data.data.students[0].dueDate !== "NA" && data.data.students[0].dueDate !== "-" ? moment(new Date(dueDate[2], dueDate[1] - 1, dueDate[0])).format(this.context.dateFormat) : '-',
        //                         "DESCRIPTION": dataOne.description,
        //                         "AMOUNT": this.formatCurrency(dataOne.amount),
        //                         "STATUS": data.status
        //                     })
        //                 }
        //             })
        //             console.log('**DATA**', data, createXlxsData)

        //         })
        //         var ws = xlsx.utils.json_to_sheet(createXlxsData);
        //         var wb = xlsx.utils.book_new();
        //         xlsx.utils.book_append_sheet(wb, ws, "Demand Note Reports");
        //         xlsx.writeFile(wb, "demand_note_reports.xlsx");
        //         //xlsx.writeFile(wb, "demand_note_reports.csv");
        //         this.setState({ isLoader: false })
        //     })
        //     .catch(err => {
        //         console.log(err, 'err0rrrrr')
        //         this.setState({ isLoader: false })
        //     })
    }

    uniqueArr = (arr, keyProps) => {
        const kvArray = arr.map(entry => {
            const key = keyProps.map(k => entry[k]).join('|');
            return [key, entry];
        });
        const map = new Map(kvArray);
        return Array.from(map.values());
    }

    //preview row
    previewRow = (data, name) => {
        console.log("row clicked", data, name)
        let details = data.data.students["0"]
        let feesBreakup = details['feesBreakup'][0]
        let previewListData = PreviewDemandNote.formJson
        Object.keys(previewListData).forEach(tab => {
            previewListData[tab].forEach(task => {
                if (task['name'] == 'demandNoteID') {
                    task['defaultValue'] = data["displayName"]
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'RegID') {
                    task['defaultValue'] = details["regId"]
                    task['label'] = this.context.reportLabel || "Reg ID"
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'StudentName') {
                    task['defaultValue'] = details['studentName']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'ProgramPlan') {
                    task['defaultValue'] = details['class']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'academicYear') {
                    task['defaultValue'] = details['academicYear']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'feeTypeCode') {
                    task['defaultValue'] = feesBreakup['description']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'totalAmount') {
                    task['defaultValue'] = feesBreakup['amount']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'paidAmount') {
                    task['defaultValue'] = feesBreakup['paidAmount']
                    task['readOnly'] = true
                    task['required'] = false
                }
                if (task['name'] == 'pendingAmount') {
                    task['defaultValue'] = feesBreakup['pendingAmount']
                    task['readOnly'] = true
                    task['required'] = false
                }
            })
        })
        this.setState({ viewAllDemandNoteList: false, isNew: false, isPreview: true, previewName: name, previewListForm: previewListData, totalDemandList: data }, () => {

        })
    }
    handleTabChange = () => { }
    cancelViewData = () => {
        this.setState({
            viewAllDemandNoteList: true, isNew: false, isPreview: false, activeStep: 0, errorSendDemand: "",
            studentNameDemand: "",
            studentIdDemand: "",
            apiResMsg: "",
            getParticularStuData: null
        })
        this.resetForm();
    }
    tableDateFormat = (ev) => {
        if (ev === undefined || ev === '') { }
        else {
            let getDate = `${String(ev.getDate()).length == 1 ? `0${ev.getDate()}` : ev.getDate()}`;
            let getMonth = `${String(ev.getMonth() + 1).length == 1 ? `0${ev.getMonth() + 1}` : ev.getMonth() + 1}`;
            let getYear = `${ev.getFullYear()}`
            let today = `${getDate}/${getMonth}/${getYear}`
            return today + ' ' + new Date(ev).toLocaleTimeString()
        }
    }
    reduceAmount = (e) => {
        let storeArr = [];
        let arrReduce = (a, b) => a + b;
        e.forEach((data) => {
            if (data.description == "Total") { }
            else { storeArr.push(data.amount) }
        })
        let changeFormat = this.formatCurrency(storeArr.reduce(arrReduce))
        return changeFormat
    }
    render() {
        const { settingDetails } = this.state
        return (
            <React.Fragment >
                { this.state.LoaderStatus == true ? <Loader /> : null}
                {
                    this.state.viewAllDemandNoteList == true ?
                        <React.Fragment >
                            {this.state.isLoader && <Loader />}
                            <div className="reports-student-fees list-of-students-mainDiv table-head-padding">
                                <div className="trial-balance-header-title">
                                    <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                    <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Demand Note</p>
                                </div>
                                <div className="reports-body-section print-hd">
                                    <React.Fragment>
                                        <ContainerNavbar containerNav={this.state.containerNav} searchValue={(searchValue) => this.searchHandle(searchValue)} onDownload={() => this.onDownloadEvent()} printScreen={this.printScreen} onAddNew={() => { this.SendNewDemandNote(); }} />
                                        <div className="print-time">{this.state.printTime}</div>
                                    </React.Fragment>
                                    <div className="reports-data-print-table">
                                        <div className="transaction-review-mainDiv">
                                            <table className="transaction-table-review reports-tableRow-header col-split-td">
                                                <thead>
                                                    <tr>
                                                        {this.state.tableHeader.map((data, i) => {
                                                            if (data == "SelectIcon") {
                                                                return (
                                                                    <th key={i} style={{ width: "15px" }}><Checkbox checked={false} name="checkedF" indeterminate={!this.state.checked} color="primary" /></th>
                                                                )
                                                            }
                                                            else { return <th className={"demand-note " + String(data).replace(' ', '')} key={i + 1}>{data}</th> }
                                                        })}
                                                    </tr>
                                                </thead>

                                                {this.state.printReportArr.length > 0 ?
                                                    <tbody>
                                                        {this.state.printReportArr.map((data, i) => {
                                                            let issuedDate;
                                                            if (data.todayDate) {
                                                                if (data.todayDate.includes('-')) { issuedDate = data.todayDate.split("-") }
                                                                else if (data.todayDate.includes('/')) { issuedDate = data.todayDate.split("/") }
                                                            }
                                                            let dueDate;
                                                            if (data.data.students[0].dueDate) {
                                                                if (data.data.students[0].dueDate.includes('-')) { dueDate = data.data.students[0].dueDate.split("-") }
                                                                else if (data.data.students[0].dueDate.includes('/')) { dueDate = data.data.students[0].dueDate.split("/") }
                                                            }
                                                            return (<tr key={i + 1} id={i + 1} onClick={() => this.previewRow(data, data.displayName)} className="table-preview" >
                                                                <td style={{ borderBottom: "1px solid #dfe1e6" }}><Checkbox value="checkedA" name={i} inputProps={{ 'aria-label': 'Checkbox A' }} color="primary" /></td>
                                                                <td className="transaction-vch-num" >{data.displayName}</td>
                                                                <td className="transaction-vch-num" >{data.data.students[0].regId}</td>
                                                                <td className="transaction-vch-num" >{data.data.students[0].studentName}</td>
                                                                <td className="transaction-vch-type">{data.data.students[0].academicYear}</td>
                                                                <td className="transaction-vch-type">{data.data.students[0].class}</td>
                                                                {/* <td className="transaction-vch-type">{data.todayDate}</td>
                                                            <td className="transaction-vch-type"><DateFormatter date={data.data.students[0].dueDate} format={this.context.dateFormat} /></td> */}
                                                                <td className="transaction-vch-type">{data.todayDate && data.todayDate !== "NA" && data.todayDate !== "-" ? <DateFormatter date={new Date(issuedDate[2], issuedDate[1] - 1, issuedDate[0])} format={this.context.dateFormat} /> : "-"}</td>
                                                                <td className="transaction-vch-type">{data.data.students[0].dueDate && data.data.students[0].dueDate !== "NA" && data.data.students[0].dueDate !== "-" ? <DateFormatter date={new Date(dueDate[2], dueDate[1] - 1, dueDate[0])} format={this.context.dateFormat} /> : "-"}</td>
                                                                {/* <td className="transaction-particulars">
                                                                {data.data.students[0].feesBreakup.map((dataOne, c) => {
                                                                    return (
                                                                        <p style={{ fontWeight: String(dataOne.description).includes("Total") ? "bold" : '' }}>{dataOne.description}</p>
                                                                    )
                                                                })}
                                                            </td> */}
                                                                <td className="transaction-debit">
                                                                    <p style={{ fontWeight: "bold", }}>{this.reduceAmount(data.data.students[0].feesBreakup)}</p>
                                                                    {/* {data.data.students[0].feesBreakup.map((dataOne, c) => {
                                                                    return (
                                                                        <p style={{ fontWeight: String(dataOne.description).includes("Total") ? "bold" : '' }}>{this.formatCurrency(dataOne.amount)}</p>
                                                                    ) 
                                                                })} */}
                                                                </td>
                                                                <td className="transaction-debit">
                                                                    {data.data.students[0].feesBreakup.map((dataOne, c) => {
                                                                        let status = data.status;
                                                                        if (c === 0) {
                                                                            return (
                                                                                <p className={String(status).toLowerCase().includes("pending") ? "Status-Inactive" : String(status).toLowerCase().includes("paid") ? "Status-Active" : String(status).toLowerCase().includes("partial") ? "Status-Partial" : "Status-Partial"} style={{ color: String(status).toLowerCase().includes("pending") ? "#FF5630" : String(status).toLowerCase().includes("paid") ? "#00875A" : "#000000", fontWeight: String(dataOne.description).includes("Total") ? "" : '' }}><span>{String(status).charAt(0).toUpperCase() + String(status).slice(1)}</span></p>
                                                                            )
                                                                        }
                                                                        else { }
                                                                    })}
                                                                </td>
                                                            </tr>
                                                            )
                                                        })}
                                                    </tbody> :
                                                    <tbody>
                                                        <tr>
                                                            {this.state.noData ? <td className="noprog-txt" colSpan={this.state.tableHeader.length}>No data...</td> :
                                                                <td className="noprog-txt" colSpan={this.state.tableHeader.length}>Fetching the data...</td>
                                                            }

                                                        </tr></tbody>
                                                }
                                            </table>
                                        </div>
                                        <div>
                                            {this.state.printReportArr.length == 0 ? null :
                                                <PaginationUI
                                                    total={this.state.totalRecord}
                                                    onPaginationApi={this.onPaginationChange}
                                                    totalPages={this.state.totalPages}
                                                    limit={this.state.limit}
                                                    currentPage={this.state.page}
                                                />}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </React.Fragment > : null
                }

                <> {
                    this.state.isPreview ?
                        <React.Fragment>
                            <React.Fragment>
                            </React.Fragment>
                            <div className="preview-send-demand-note">
                                <div className="preview-header-wrap">
                                    < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.BackToDemandNoteList} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                    <h6 className="preview-Id-content" onClick={this.BackToDemandNoteList}>| Demand Note | {this.state.previewName}</h6>
                                </div>
                                <div className="form-new-button-list">
                                    <button className="finish-button-demand" onClick={this.BackToDemandNoteList}>Done</button>
                                    {/* <img src={DownloadSVG} alt="Download" className="material-searchIcon"></img> */}
                                </div>
                                <div className="preview-body-content">
                                    {this.state.namespace === 'vkgi' ?
                                        <React.Fragment>
                                            <div className="demandNoteTemplate">
                                                <div className="mainList-template">
                                                    <img title="logo" id="img" alt="college logo" style={{ width: "148px", height: "148px" }} src={vkgiLogo} />
                                                    <div className="mainList-text" style={{ marginLeft: "20px" }}>
                                                        <p className="mainList-para" style={{ margin: "0px" }}>
                                                            <h3 id="insname" style={{ margin: 0, marginBottom: 5, fontFamily: "OpenSans-Medium", fontSize: 15 }}>Jeevanbhima Nagar Branch</h3>
                                                            <span id="city" style={{ fontFamily: "OpenSans-Regular" }} >1054/2, Puttappa Layout, New Thippasandra,</span>
                                                            <br />
                                                            <span id="District" style={{ fontFamily: "OpenSans-Regular" }} > Bengaluru 560075,| Karnataka, Contact: +91 8047185857</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <hr />
                                                <p><strong>Dear Parent,</strong></p>
                                                <br />
                                                <p> Greetings from NCFE! We are happy to announce the commencement of the academic year 2021- 22.</p>
                                                <p>Your ward Aarav Mahesh is in Grade-1 and the annual fee for the academic year  2021- 22 is <b>₹1,00,000.00 </b><br />These fees have to be paid in 2 installments as follows:</p>
                                                <br />
                                                <table style={{ borderCollapse: "collapse", width: "40%", height: "84px", border: "1px solid #000" }}>
                                                    <thead>
                                                        <th className="tableRowData" style={{ width: "20%" }}>TERM</th>
                                                        <th className="tableRowData">DUE DATE</th>
                                                        <th className="tableRowData">AMOUNT</th>
                                                        <th className="tableRowData">STATUS</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td className="tableBodyData">Term 1</td>
                                                            <td className="tableBodyData">15/APR/2021</td>
                                                            <td className="tableBodyData">₹60,000.00</td>
                                                            <td className="tableBodyData">Planned</td>
                                                        </tr>
                                                        <tr>
                                                            <td className="tableBodyData">Term 2</td>
                                                            <td className="tableBodyData">10/OCT/2021</td>
                                                            <td className="tableBodyData">₹40,000.00</td>
                                                            <td className="tableBodyData">Planned</td>
                                                        </tr>
                                                        <tr>
                                                            <td className="tableBodyData"><b>TOTAL</b></td>
                                                            <td className="tableBodyData">-</td>
                                                            <td className="tableBodyData"><b>₹1,00,000.00</b></td>
                                                            <td className="tableBodyData"><b>Planned</b></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <br />
                                                <p>The details for the Term1 fees are as follows:</p><br />
                                                <table style={{ borderCollapse: "collapse", width: "100%", height: "84px", border: "1px solid #000" }}>
                                                    <tbody>
                                                        <tr>
                                                            <td className="tableRowData">Student Admission Number</td>
                                                            <td className="tableRowData">Student Name</td>
                                                            <td className="tableRowData">Grade</td>
                                                            <td className="tableRowData">Demand Note ID</td>
                                                            <td className="tableRowData">Annual Fees</td>
                                                            <td className="tableRowData">Term1 Fees</td>
                                                            <td className="tableRowData">Term2 Fees</td>
                                                        </tr>
                                                        <tr>
                                                            <td className="tableBodyData">5334</td>
                                                            <td className="tableBodyData">Aarav Mahesh</td>
                                                            <td className="tableBodyData">Grade-1</td>
                                                            <td className="tableBodyData">DN/2020-21/001</td>
                                                            <td className="tableBodyData1">₹1,00,000.00</td>
                                                            <td className="tableBodyData1">₹60,000.00</td>
                                                            <td className="tableBodyData1">₹40,000.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td style={{ padding: "0px", paddingRight: "5px" }} className="tableFooterData">PAY NOW</td>
                                                            <td className="tableFooterData">₹60,000.00</td>
                                                        </tr>
                                                        <tr style={{ border: "1px solid #000" }}>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td style={{ padding: "0px", paddingRight: "5px" }} className="tableFooterData">BALANCE</td>
                                                            <td className="tableFooterData">₹40,000.00</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <br />
                                                <p style={{ fontWeight: "bold" }}>Payment due date for this Demand note is <span style={{ textDecoration: "underline" }}>15/APR/2021</span>.</p>
                                                <p>Please click on the following button to initiate the payment before the due date.</p>
                                                <br />
                                                <p> <button className="button button1 payNowButton">Pay Now</button></p>
                                                <p> Please note that a penalty of ₹25 per day will be charged if the fees is not paid by <u> 20/APR/2021</u>.</p>
                                                <br /><p>Regards,</p>
                                                <p><b><span id="inssname"></span>Finance Team</b></p>
                                                <p>NATIONAL CENTRE FOR EXCELLENCE </p>
                                            </div>
                                        </React.Fragment> :
                                        <React.Fragment>
                                            <div className="header-section">
                                                <p>From: <b>noreply@zenqore.com</b></p>
                                                <p>Date: <b>{this.tableDateFormat(new Date(this.state.totalDemandList.fullDate))}</b></p>
                                                <p>Subject: <b>ZQ EDU-Demand Note</b></p>
                                                <p>To: <b>{this.state.totalDemandList.emailCommunicationRefIds}</b></p>
                                            </div>
                                            <div className="logo-address-list">
                                                <div className="institute-logo-display">
                                                    <img src={this.state.settingDetails.logo.logo} alt="institute-logo" />
                                                </div>
                                                <div className="institute-name-display">
                                                    <p className="bold-text">{settingDetails.instituteDetails.instituteName}</p>
                                                    <p>{`${settingDetails.instituteDetails.address1 == "undefined" || settingDetails.instituteDetails.address1.length == 0 ? '' : settingDetails.instituteDetails.address1 + ','} ${settingDetails.instituteDetails.address2} , ${settingDetails.instituteDetails.address3}`}</p>
                                                    <p>{`${settingDetails.instituteDetails.cityTown} , ${settingDetails.instituteDetails.pinCode == "null" || settingDetails.instituteDetails.pinCode.length == 0 ? '' : 'PIN -' + settingDetails.instituteDetails.pinCode}`}</p>
                                                    <p>{`${settingDetails.instituteDetails.stateName} , India`}</p>
                                                    <p>Contact: {settingDetails.instituteDetails.email} </p>
                                                    <p>Ph: {settingDetails.instituteDetails.phoneNumber1} </p>
                                                </div>
                                            </div>
                                            <hr />
                                            <div className="mail-body-section">
                                                <p style={{ fontWeight: "bold" }}>Dear parent,</p>
                                                <p>Please find the fee details of your ward(s) as follows:</p>
                                                <div className="preview-table">
                                                    <table className="table-view">
                                                        <thead>
                                                            <tr>
                                                                {this.state.previewTableHead.map((data, i) => {
                                                                    return (<th id={i}>{data}</th>)
                                                                })}
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>{this.state.totalDemandList.data.students[0].regId}</td>
                                                                <td>{this.state.totalDemandList.data.students[0].studentName}</td>
                                                                <td>{this.state.totalDemandList.data.students[0].class}</td>
                                                                <td>{this.state.totalDemandList.displayName}</td>
                                                                <td>{Number(this.state.totalDemandList.data.students[0].feesBreakup[0].amount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</td>
                                                                <td>{Number(this.state.totalDemandList.data.students[0].feesBreakup[0].paidAmount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</td>
                                                                <td style={{ fontWeight: "bold" }}>{Number(this.state.totalDemandList.data.students[0].feesBreakup[0].pendingAmount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan={6} style={{ textAlign: "right", fontWeight: "bold" }}> GRAND TOTAL </td>
                                                                <td style={{ fontWeight: "bold" }}>{Number(this.state.totalDemandList.data.students[0].feesBreakup[1].amount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <p className="text-bold">{`The Payment due date for this Demand note is ${this.state.totalDemandList.data.students[0].dueDate}.`}</p>
                                                    <p className="text-normal">Please click the button to initiate the payment:</p>
                                                    <button className="pay-now-btn">Pay Now</button>
                                                    <p className="text-normal" style={{}}>Regards,</p>
                                                    <p className="text-bold-final">{settingDetails.instituteDetails.instituteName}</p>
                                                </div>
                                            </div>
                                        </React.Fragment>}
                                </div>
                            </div>
                            {/* <div className="reports-student-fees list-of-students-mainDiv table-head-padding">
                        <div className="tab-form-wrapper tab-table">
                            <div className="preview-btns">
                                <div className="preview-header-wrap">
                                    < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.BackToDemandNoteList} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                    <h6 className="preview-Id-content" onClick={this.BackToDemandNoteList}>| Demand Note | {this.state.previewName}</h6>
                                </div>
                            </div>
                            <div className="organisation-table student-submit-none">
                                <div className="form-new-button-list">
                                    <button className="finish-button-demand" onClick={this.BackToDemandNoteList}>Done</button>
                                    <img src={DownloadSVG} alt="Download" className="material-searchIcon"></img>
                                </div>
                                <ZenTabs
                                    tabData={this.state.previewListForm}
                                    className="preview-wrap"
                                    cleanData={this.cleanDatas}
                                    tabEdit={this.state.preview}
                                    cancelViewData={this.cancelViewData}
                                    form={this.state.previewListForm}
                                    value={0}
                                    onInputChanges={this.onInputChanges}
                                    onFormBtnEvent={(item) => { this.formBtnHandle(item); }}
                                    onTabFormSubmit={this.onFormSubmit}
                                    handleTabChange={this.handleTabChange}
                                    key={0}
                                />
                            </div>
                        </div>
                    </div> */}
                        </React.Fragment> : null
                }</>

                <>{this.state.isNew ? <React.Fragment>

                    {this.state.LoaderStatus == true ? <Loader /> : null}
                    <div className="trial-balance-header-title">
                        <KeyboardBackspaceSharpIcon title="go back" className="keyboard-back-icon" onClick={this.BackToDemandNoteList} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                        <p className="top-header-title" title="go back" onClick={this.BackToDemandNoteList}>| Send Demand Note</p>
                    </div>
                    <div className="migration-main-div portal-login-div vendor-invoice-upload-wrap pdf-extraction-upload-wrap" style={{ height: "calc(100vh - 100px)" }}>
                        <div className="migration-header-stepper-section">
                            <div className="zenqore-stepper-section">
                                <Steps current={this.state.activeStep} currentStatus="process" vertical={false} >
                                    <Steps.Item title="Fetch Details" />
                                    <Steps.Item title="Student Information" />
                                    <Steps.Item title="Confirmation" />
                                </Steps>
                            </div>
                        </div>
                        <div className="migration-body-content-section" style={{ backgroundColor: "none !important", height: "calc(100vh - 215px)", overflowY: "auto", border: "1px solid #dfe1e6" }}>
                            {this.getStepContent(this.state.activeStep)}
                        </div>
                    </div>
                    <div className="list-of-students-mainDiv">
                        <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={2500} onClose={this.closeNotification}>
                            <Alert onClose={this.closeNotification}
                                severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
                                {this.state.snackbar.NotificationMessage}
                            </Alert>
                        </Snackbar>
                    </div>
                </React.Fragment> : null}</>
            </React.Fragment >
        )
    }
}
export default SendDemandNote;