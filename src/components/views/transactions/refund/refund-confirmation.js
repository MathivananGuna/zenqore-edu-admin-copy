import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import '../../../../scss/receive-payment.scss';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import Button from '@material-ui/core/Button';
class RefundConfirmation extends Component {
    constructor(props) {
        super(props)
        this.state = {
            amountPaid: '',
            studentName: this.props.studentName,
        }
    }
    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    componentDidMount() {
        let amount = this.formatCurrency(100000)
        this.setState({ amountPaid: amount, paymentID: this.props.paymentID });
    }
    finishProcess = () => {
        this.props.finishProcess()
    }
    render() {
        return (
            <React.Fragment>
                <div className="receive-payment-main">
                    <div className="payment-confirm-page">
                        <CheckCircleIcon className="confirm-icon" />
                        <p className="payment-confirm-text">A Refund Amount of <b>{this.formatCurrency(this.props.refundAmount)}</b> for the Student <b>{this.state.studentName}</b><br />
                                        Successfully processed.
                                         {/* The transaction ID is <b>{this.props.transactionId}</b> */}
                        </p>
                        <div className="confirm-btns">
                            {/* <Button className="confirm-payment-btn" onClick={this.getInvoice}>Download Receipt</Button> */}
                            <Button className="confirm-payment-btn" onClick={this.finishProcess}>Finish</Button>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default withRouter(RefundConfirmation)