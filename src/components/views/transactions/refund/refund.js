import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Modal, Icon, Steps } from 'rsuite';
import Button from '@material-ui/core/Button';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import StepConnector from '@material-ui/core/StepConnector';
import { CircularProgress } from "@material-ui/core";
import Loader from '../../../../utils/loader/loaders';
import '../../../../scss/payment-portal.scss';
import '../../../../scss/portal-stepper.scss';
import '../../../../scss/student-reports.scss';
import '../../../../scss/receive-payment.scss';
import '../../../../scss/student.scss';
import ZenForm from '../../../input/form';
import ZqTable from '../../../../utils/Table/table-component';
import RefundInputJson from './refund-transaction.json';
// import PaymentPage from './payment-page';
import RefundConfirmation from './refund-confirmation';
import axios from 'axios';
import { Alert } from 'rsuite';
import moment from 'moment';
import { set } from 'date-fns';
import DateFormatter from '../../../date-formatter/date-formatter'
import DateFormatContext from '../../../../gigaLayout/context'
import xlsx from 'xlsx';
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import PaginationUI from "../../../../utils/pagination/pagination";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PreviewForm from './preview-form.json';
import Payment from "../../../payment/payment";
class RefundTxn extends Component {
    constructor(props) {
        super(props)
        this.state = {
            nextLoader: false,
            circularLoader: false,
            newCircular: false,
            payementForm: false,
            activeStep: window.location.href.includes("razorpay_payment_id") ? 2 : 0,
            steps: ['Fetch Details', 'Refund Information', 'Refund', 'Confirmation'],
            RefundInputJsonData: RefundInputJson.FetchStudentDetails,
            pendingAmountData: RefundInputJson.AmountDetails,
            RefundDetails: [],
            env: JSON.parse(localStorage.getItem('env')),
            orgId: localStorage.getItem("orgId"),
            authToken: localStorage.getItem("auth_token"),
            studentID: '',
            RefundStatus: "Fetching...",
            studentData: [],
            selectedInfo: [],
            studentFeeID: [],
            guardianDetails: {},
            StudentName: '',
            paymentAmount: 0,
            viewFormData: true,
            StudentName: '',
            StudentClass: '',
            StudentRegId: '',
            parentName: '',
            parentMobile: '',
            parentEmail: '',
            demandNoteID: '',
            paymentID: '',
            paymentType: 'Cash',
            selectedStudentItem: [],
            studentFeeMapDetails: [],
            refundHeader: ["REGISTRATION ID", "STUDENT NAME", "PARENT NAME", "CLASS/BATCH", "ADMITTED ON"],
            refundBody: [],
            selectedItemCount: 0,
            containerNav: {
                isBack: false,
                name: "Refund",
                isName: true,
                isSearch: false,
                isSort: false,
                isPrint: true,
                isDownload: true,
                isShare: false,
                isNew: true,
                newName: "New",
                isSubmit: false,
            },
            printReportArr: [],
            previewStudentTableData: [],
            previewTableData: [],
            showTable: true,
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 0,
            viewRefundNew: false,
            showPreview: false,
            activeTab: 0,
            tabData: ['Basic Information', 'Transaction Details'],
            previewData: PreviewForm['Basic Information'],
            PaymentFormData: [],
            previewRefundData: [],
            StudentRegId: "",
            StudentName: "",
            StudentClass: "",
            cardType: "", creditDebit: "", netBankingType: "", walletType: "",
            feeTypeLists:[]
        }
    }

    static contextType = DateFormatContext;
    componentDidMount() {
        let tableHeader = ["REFUND ID", "DEMAND NOTE ID", `${this.context.reportLabel ? this.context.reportLabel : "REG ID"}`, "STUDENT NAME", "ACADEMIC YEAR", `${this.context.classLabel ? this.context.classLabel : "CLASS/BATCH"}`, "DESCRIPTION", "REFUNDED", "REFUNDED ON", "STATUS"]
        this.setState({ tableHeader }, () => {
            this.onDataCall()
        })
    }
    componentWillMount() {
        let currentURL = window.location.href;
        if (currentURL.includes("razorpay_payment_id")) {
            this.setState({ nextLoader: true });
            const payvalue = currentURL.split("?")
            if (payvalue[1] != undefined) {
                var paymentquery = String(payvalue[1]).split(/[=,&]/)
                this.props.history.push("/receive-payment")
                this.setState({ paymentID: paymentquery[1] })
                // this.ledgerEntry(paymentquery[1]);
                localStorage.setItem('paymentId', paymentquery[1])
                localStorage.setItem('studentFeeMapId', paymentquery[15])
            }
        }
    }
    // ledgerEntry = (payID) => {
    //     let paymentID = payID;
    //     axios.get(`${this.state.env['zqBaseUri']}/edu/getPaymentStatus/${paymentID} `, {
    //         headers: {
    //             "Authorization": localStorage.getItem('auth_token')
    //         }
    //     }).then(response => {
    //         let paymentResponse = JSON.parse(response.data.Data)
    //         let amount = paymentResponse.amount !== undefined ? paymentResponse.amount : ''
    //         amount = amount.toString()
    //         amount = amount.slice(0, -2);
    //         let payload = {
    //             "transactionDate": new Date(),
    //             "relatedTransactions": localStorage.getItem('studentFeeID'),
    //             "transactionType": "eduFees",
    //             "transactionSubType": "feePayment",
    //             "studentFeeMap": localStorage.getItem('studentFeeMapId'),
    //             "amount": Number(amount),
    //             "status": 'initiated',
    //             "data": {
    //                 "orgId": localStorage.getItem('orgId'),
    //                 "transactionType": "eduFees",
    //                 "transactionSubType": "feePayment",
    //                 "mode": paymentResponse.method,
    //                 "modeDetails": {
    //                     "netBankingType": null,
    //                     "walletType": null,
    //                     "instrumentNo": paymentResponse.acquirer_data.bank_transaction_id,
    //                     "instrumentDate": new Date(),
    //                     "bankName": paymentResponse.bank,
    //                     "branchName": "",
    //                     "transactionId": paymentResponse.acquirer_data.bank_transaction_id,
    //                     "remarks": ""
    //                 },
    //                 "amount": Number(amount),
    //             },
    //             "paymentTransactionId": this.state.paymentId,
    //             "createdBy": localStorage.getItem('orgId'),
    //             "emailCommunicationRefIds": paymentResponse.email
    //         }
    //         console.log("payload", payload)
    //         axios.post(`${this.state.env['zqBaseUri']}/edu/feePayment`, payload, {
    //             headers: {
    //                 "Authorization": localStorage.getItem('auth_token')
    //             }
    //         }).then(response => {
    //             console.log("post responsew", response)
    //             this.setState({ feepayment: true })

    //         }).catch(err => {
    //             console.log("post responsew", err)
    //             if (err.response.status === 400) {

    //                 this.setState({ isLoader: false });
    //                 this.setState({ error: err.response.data.message });
    //             }
    //             else {
    //                 this.setState({ error: 'Payment Failed' })
    //                 this.setState({ isLoader: false })
    //             }
    //         })
    //     })
    //     // this.setState({ nextLoader: false });
    //     // this.setState({ activeStep: 2 })
    // }
    onSubmit = (formdata, item, format) => {
        if (String(formdata[0].value).length > 0) {
            this.setState({ circularLoader: true, newCircular: true });
            let studentID = formdata[0].value;
            let headers = {
                'Authorization': this.state.authToken
            };
            axios.get(`${this.state.env['zqBaseUri']}/edu/getPaidDetails/${studentID}?orgId=${this.state.orgId}`, { headers })
                .then(res => {
                    console.log(res.data.data);
                    let data = res.data.data;
                    let StudentName = data.payment[0].paymentDetails.studentName;
                    let StudentClass = data.payment[0].paymentDetails.class;
                    let StudentRegId = data.payment[0].paymentDetails.studentRegId;
                    let parentName = data.studentDetails.parentName;
                    let admittedOn = data.studentDetails.admittedOn != undefined ? moment(data.studentDetails.admittedOn).format('DD/MM/YYYY') : "-"
                    let refundData = []
                    refundData.push({
                        "StudentRegId": StudentRegId,
                        "parentName": parentName,
                        "StudentName": StudentName,
                        "StudentClass": StudentClass,
                        "admittedOn": admittedOn
                    })
                    let feeTypes = []
                    let feesBreakUp = data.studentFeeMapDetails[0].transactionPlan.feesBreakUp;
                    feesBreakUp.map(feeElt => {
                        feeTypes.push({
                          'feeTypeId': feeElt['_id'],
                          'feeType': feeElt.title,
                          'amount':  Number(feeElt.pending).toFixed(2),
                          'feeTypeCode': feeElt.feeTypeCode
                        })
                      })
                    this.setState({
                        feeTypeLists: feeTypes,
                        studentData: data,
                        refundBody: refundData,
                        studentFeeMapDetails: data.studentFeeMapDetails,
                        guardianDetails: data.guardianDetails,
                        StudentName: StudentName,
                        circularLoader: false,
                    })
                    this.showStudentInformation(data);

                    setTimeout(() => {
                        this.setState({ activeStep: 1, newCircular: false });
                    }, 2000)
                }).catch(err => {
                    if (err.response) Alert.error(err.response.data.message)
                    console.log('error', err)
                    this.setState({ circularLoader: false, newCircular: false, payementForm: false })
                })
        }
        else {
            // this.setState({ viewFormData: true })
            Alert.error("Please Enter the Student Registeration ID..!")
        }
    }
    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.onDataCall()
        });
        console.log(page, limit);
    };
    showStudentInformation = (data) => {
        console.log(data, "****")
        let RefundDetails = [];

        let parentName = data['studentDetails']['parentName']
        let parentMobile = data['studentDetails']['parentPhone']
        let parentEmail = data['studentDetails']['parentEmail']

        let paymentFiltered = []
        let demandnoteFiltered = []
        let paymentAllData = data['payment']
        let regId = this.context.reportLabel || "Reg ID"
        let batch = this.context.classLabel || "CLASS/BATCH"
        // data.map((item, i) => {
        if (data.payment.length > 0) {
            // data['payment']['paymentDetails'].map(paymentItem => {
            //     paymentFiltered.push(paymentItem)
            // })
            // data['payment']['demandNote'].map(paymentItem => {
            //     demandnoteFiltered.push(paymentItem)
            // })


            paymentAllData.map(dataset => {
                RefundDetails.push({
                    "Receipt Id": dataset['paymentDetails']["transactionDisplayName"],
                    "Demand Note Id": dataset['paymentDetails']['primaryTransaction'],
                    [regId]: dataset['paymentDetails']["studentRegId"],
                    "Student Name": dataset['paymentDetails']["studentName"],
                    "Academic Year": dataset['paymentDetails']["academicYear"],
                    [batch]: dataset['paymentDetails']["class"],
                    "Paid Amount": this.formatCurrency(dataset['paymentDetails']["paidAmount"]),
                    "Paid On": moment(new Date(dataset['paymentDetails']["transactionDate"])).format(this.context.dateFormat),
                    "Status": dataset['paymentDetails']['status'],
                    "Item": JSON.stringify(dataset)
                })

                this.setState({ demandNoteID: dataset['paymentDetails'].primaryTransaction });
            });
        }
        this.setState({ RefundDetails: RefundDetails, parentName: parentName, parentMobile: parentMobile, parentEmail: parentEmail });
    }
    onCardSubmit = (txnNumber) => {
        this.setState({ paymentID: txnNumber });
        this.handleNext();
    }
    onCashSubmit = () => {
        this.handleNext();
    }
    confirmCash = () => {
        if (this.state.studentFeeID.length == 1) {
            this.setState({ nextLoader: true })
            // let a = this.state.activeStep
            setTimeout(() => {
                this.setState({ nextLoader: false })
                this.setState({ activeStep: 2 })
            }, 1000)
        }
        else if (this.state.studentFeeID.length == 0) {
            Alert.error("Please select atleast one receipt to proceed")
        }
        else {
            Alert.error("Please select only one receipt to proceed")
        }


    }
    confirmCard = () => {
        this.handleNext();
        this.setState({ paymentType: 'Card Payment' });
    }
    confirmRazorpay = () => {
        let payload = {
            "amount": Number(this.state.paymentAmount),
            "paisa": String(this.state.paymentAmount).includes('.') ? String(this.state.paymentAmount).split('.')[1] : "00",
            "paymentReferenceId": Date.now() + `#` + this.state.demandNoteID,
            "acceptPartial": true,
            "minPartialAmount": 100,
            "name": this.state.StudentName,
            "mobile": this.state.parentMobile,
            "email": this.state.parentEmail,
            "callBackUrl": window.location.href,
            "currencyCode": "INR",
            "description": "",
            "paidFor": "demandNote",
            "orgId": localStorage.getItem('orgId')
        }
        axios.post(`${this.state.env["zqBaseUri"]}/edu/payment`, payload, {
            headers: {
                "Authorization": localStorage.getItem('auth_token')
            }
        })
            .then(res => {
                window.open(res.data.data.short_url, '_self');
            }).catch(err => {
                Alert.error("Unable to Load RazorPay")
            })
    }
    onInputChanges = (value, e, item) => {
        this.setState({ refundAmount: value })
    }
    selectedItem = (e, value, index, si) => {
        console.log(value, si)
        var selectedInfo = this.state.selectedInfo;
        let value1 = JSON.parse(value.Item);
        let { selectedStudentItem } = this.state
        console.log(selectedStudentItem)
        let paymentAmount = this.state.paymentAmount;

        let selectedAmt = Number(value1['paymentDetails'].paidAmount);
        let items = {}
        items = selectedAmt
        if (value.checked == true) {
            let studentFeeID = [];
            studentFeeID = this.state.studentFeeID;
            studentFeeID.push(value1["paymentDetails"].transactionDisplayName)
            this.setState({ studentFeeID: studentFeeID });
            localStorage.setItem("studentFeeID", this.state.studentFeeID)
            selectedStudentItem.push(value1)
            console.log("selectedStudentItem", selectedStudentItem)
            let payableAmt = paymentAmount + Number(items)
            this.setState({ paymentAmount: payableAmt, selectedStudentItem: selectedStudentItem, viewFormData: false });
            console.log("payableAmt", payableAmt)
            this.state.pendingAmountData.map((data) => {
                if (data.name == "selectedAmount") {
                    data['defaultValue'] = this.formatCurrency(Number(payableAmt).toFixed(2))
                    data['readOnly'] = false
                    data['validation'] = true
                }
            })
            this.setState({ viewFormData: true, pendingAmountData: this.state.pendingAmountData })
        } else {
            let studentFeeID = [];
            studentFeeID = this.state.studentFeeID;
            selectedStudentItem.splice(index, 1)
            console.log('selectedStudentItem', selectedStudentItem)
            const indexVal = studentFeeID.indexOf(value1["paymentDetails"].transactionDisplayName)
            if (indexVal > -1) {
                studentFeeID.splice(indexVal, 1)
            }

            this.setState({ studentFeeID: studentFeeID });
            localStorage.setItem("studentFeeID", this.state.studentFeeID)
            let payableAmt = paymentAmount - Number(items)
            this.setState({ paymentAmount: payableAmt, viewFormData: false });
            console.log("payableAmt", payableAmt)
            this.state.pendingAmountData.map((data) => {
                if (data.name == "selectedAmount") {
                    data['defaultValue'] = this.formatCurrency(Number(payableAmt))
                    data['readOnly'] = false
                    data['validation'] = true
                }
            })
            this.setState({ viewFormData: true, selectedStudentItem, pendingAmountData: this.state.pendingAmountData, })
        }
        this.setState({ selectedInfo: selectedInfo });
    }
    selectAll = (allItems, a) => {
        let AmountValues = []
        allItems.map((item, index) => {
            AmountValues.push((JSON.parse(item.Item)))
        })
        console.log(AmountValues)
        let total = AmountValues.reduce((a, b) => a + b.paymentDetails.paidAmount, 0)
        console.log(total)
        let { selectedStudentItem } = this.state
        allItems.map(item => {
            selectedStudentItem.push(JSON.parse(item.Item))
        })
        console.log(selectedStudentItem)
        if (a == true) {
            let studentFeeID = []
            allItems.map(item => {
                studentFeeID.push(item['Receipt Id'])
            })
            this.setState({ studentFeeID: studentFeeID });
            localStorage.setItem("studentFeeID", this.state.studentFeeID)
            this.setState({ selectedInfo: allItems, enableBtn: true })
            this.state.pendingAmountData.map((data) => {
                if (data.name == "selectedAmount") {
                    data['defaultValue'] = this.formatCurrency(Number(total).toFixed(2))
                    data['readOnly'] = true
                    data['validation'] = true
                }
            })
            this.setState({ pendingAmountData: this.state.pendingAmountData, paymentAmount: Number(total), selectedStudentItem: selectedStudentItem })
        }
        else {
            let studentFeeID = []
            this.setState({ studentFeeID: [] });
            localStorage.setItem("studentFeeID", this.state.studentFeeID)
            this.setState({ selectedInfo: [], enableBtn: false })
            this.state.pendingAmountData.map((data) => {
                if (data.name == "selectedAmount") {
                    data['defaultValue'] = this.formatCurrency(Number(0))
                    data['readOnly'] = false
                    data['validation'] = true
                }
            })
            this.setState({ pendingAmountData: this.state.pendingAmountData, paymentAmount: Number(0) })
        }
    }
    rowClick = (item) => {
        console.log(item)
    }
    onRefundSubmit = (data, item, format) => {
        console.log(this.state.selectedStudentItem)
        if (this.state.paymentAmount > 0 && this.state.refundAmount > 0 && this.state.refundAmount <= this.state.paymentAmount) {
            this.setState({ nextLoader: true });
            let paymentItem = []
            this.state.selectedStudentItem.filter((item, index) => {
                if (item['paymentDetails'].transactionDisplayName == this.state.studentFeeID) {
                    paymentItem.push(item)
                }
                return paymentItem
            })
            console.log('final payment item', paymentItem)
            let refundPayload = {}
            // refundPayload = {
            //     "displayName": "",
            //     "transactionType": "",
            //     "transactionSubType": "",
            //     "transactionDate": "",
            //     "studentId": paymentItem[0]['paymentDetails'].studentId,
            //     "studentRegId": paymentItem[0]['paymentDetails'].studentRegId,
            //     "studentName": paymentItem[0]['paymentDetails'].studentName,
            //     "class": paymentItem[0]['paymentDetails'].class,
            //     "academicYear": paymentItem[0]['paymentDetails'].academicYear,
            //     "programPlan": paymentItem[0]['paymentDetails'].programPlan,
            //     "amount": Number(this.state.refundAmount),
            //     // "paymentTransactionId": item.payment.transactionId,
            //     "emailCommunicationRefIds": paymentItem[0]['demandNote'].data.emailCommunicationRefIds,
            //     "smsCommunicationRefIds": paymentItem[0]['demandNote'].data.smsCommunicationRefIds,
            //     "status": "",
            //     "paymentRefId": paymentItem[0]['paymentDetails'].transactionDisplayName,
            //     "paymentTransactionId": null,
            //     "relatedTransactions": [paymentItem[0]['paymentDetails'].transactionDisplayName],
            //     "orgId": paymentItem[0]['demandNote'].data.orgId,
            //     "transactionId": null,
            //     "data": {
            //         "paid": Number(paymentItem[0]['paymentDetails'].paidAmount),
            //         "orgId": paymentItem[0]['demandNote'].data.orgId,
            //         "displayName": paymentItem[0]['demandNote'].data.transactionDisplayName,
            //         "studentId": paymentItem[0]['demandNote'].data.studentId,
            //         "studentRegId": paymentItem[0]['demandNote'].data.studentRegId,
            //         "class": paymentItem[0]['demandNote'].data.class,
            //         "academicYear": paymentItem[0]['demandNote'].data.academicYear,
            //         "programPlan": paymentItem[0]['demandNote'].data.programPlan,
            //         "issueDate": "",
            //         "dueDate": paymentItem[0]['demandNote'].data.dueDate,
            //         "feesBreakUp": paymentItem[0]['demandNote'].data.feesBreakUp
            //     },
            //     "createdBy": paymentItem[0]['demandNote'].data.createdBy,
            //     "studentFeeMapId": this.state.studentFeeMapDetails[0]["displayName"]
            // }
            refundPayload = {
                "displayName": "",
                "transactionType": "",
                "transactionSubType": "",
                "transactionDate": "",
                "studentId": paymentItem[0]['paymentDetails'].studentId,
                "studentRegId": paymentItem[0]['paymentDetails'].studentRegId,
                "studentName": paymentItem[0]['paymentDetails'].studentName,
                "class": paymentItem[0]['paymentDetails'].class,
                "academicYear": paymentItem[0]['paymentDetails'].academicYear,
                "programPlan": paymentItem[0]['paymentDetails'].programPlan,
                "amount": Number(this.state.refundAmount),
                // "paymentTransactionId": item.payment.transactionId,
                "emailCommunicationRefIds": this.state.guardianDetails.email == 'NA'|| this.state.guardianDetails.email == '' || this.state.guardianDetails.email == '-' || this.state.guardianDetails.email == null ? this.state.studentData.studentDetails.email : this.state.guardianDetails.email,
                "smsCommunicationRefIds":'',
                "status": "",
                "paymentRefId": paymentItem[0]['paymentDetails'].transactionDisplayName,
                "paymentTransactionId": null,
                "relatedTransactions": [paymentItem[0]['paymentDetails'].transactionDisplayName],
                "orgId": localStorage.getItem('orgId'),
                "transactionId": null,
                "data": {
                    "paid": Number(paymentItem[0]['paymentDetails'].paidAmount),
                    "orgId": localStorage.getItem('orgId'),
                    "displayName": paymentItem[0]['paymentDetails'].transactionDisplayName,
                    "studentId": paymentItem[0]['paymentDetails'].studentId,
                    "studentRegId": paymentItem[0]['paymentDetails'].studentRegId,
                    "class": paymentItem[0]['paymentDetails'].class,
                    "academicYear": paymentItem[0]['paymentDetails'].academicYear,
                    "programPlan": paymentItem[0]['paymentDetails'].programPlan,
                    "issueDate": "",
                    "dueDate": this.state.studentFeeMapDetails[0]["dueDate"],
                    "feesBreakUp":this.state.feeTypeLists
                },
                "createdBy": localStorage.getItem('userId'),
                "studentFeeMapId": this.state.studentFeeMapDetails[0]["displayName"]
            }

            console.log('payload', refundPayload)

            let headers = {
                'Authorization': this.state.authToken
            };
            axios.post(`${this.state.env['zqBaseUri']}/edu/refund`, refundPayload, { headers })
                .then(res => {
                    console.log(res)
                    let a = this.state.activeStep;
                    this.setState({ activeStep: a + 1 })
                    //To remove amount after payment is confirmed
                    this.state.pendingAmountData.map((data) => {
                        if (data.name == "refundAmount") {
                            data['defaultValue'] = ''
                            data['required'] = true
                            data['readOnly'] = false
                        }
                    })
                    this.setState({ nextLoader: false });
                }).catch(err => {
                    Alert.error("Transaction Failed.")
                    this.setState({ nextLoader: false });
                })
        } else {
            this.setState({ nextLoader: true });
            this.state.pendingAmountData.map((data) => {
                if (data.name == "refundAmount") {
                    data['error'] = true
                    data['required'] = true
                    data['errorMsg'] = `Invalid Refund Amount`
                }
            })
            this.setState({ nextLoader: false });
        }
    }
    getStepContent = (stepIndex) => {
        switch (stepIndex) {
            case 0:
                return <React.Fragment>
                    {this.state.newCircular ?
                        <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                            <CircularProgress size={24} />
                        </div> : null}
                    <div className="receive-payment-main">
                        <div className="receive-payment-table">
                            <React.Fragment >
                                <div className="payment-form">
                                    <ZenForm inputData={this.state.RefundInputJsonData} onSubmit={this.onSubmit} />
                                </div>
                            </React.Fragment>
                        </div>
                    </div>
                </React.Fragment>
            case 1:
                return (<div className="receive-payment-main">
                    {this.state.nextLoader ?
                        <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                            <CircularProgress size={24} />
                        </div> : null}
                    <div className="receive-amount-table">
                        {/* {this.state.circularLoader == false ? */}
                        <React.Fragment >
                            <div className="reports-student-fees">
                                <div className="reports-body-section">
                                    <p className="statement-date-time" style={{ paddingLeft: 20 }}>Date: {moment().format(this.context.dateFormat)}</p>

                                    <div className="reports-data-print-table">

                                        <table className="transaction-table-review reports-tableRow-header" style={{
                                            width: '75%',
                                            margin: '0 auto'
                                        }}>
                                            <thead>
                                                <tr>
                                                    {this.state.refundHeader.map((data, i) => {
                                                        return <th style={{ width: `${String(data).includes('REGISTRATION ID') ? '170px' : ''}` }} className={"demand-note " + String(data).replace(' ', '')} key={i + 1}>{data}</th>
                                                    })}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.refundBody.map((data, i) => {
                                                    return (<tr key={i + 1} id={i + 1} className="reports-tableRow-body"
                                                        style={{ cursor: "pointer" }}>
                                                        <td className="transaction-vch-num" >{data['StudentRegId']}</td>
                                                        <td className="transaction-vch-num" >{data['StudentName']}</td>
                                                        <td className="transaction-vch-num" >{data['parentName']}</td>
                                                        <td className="transaction-vch-type">{data['StudentClass']}</td>
                                                        <td className="transaction-vch-type">{data['admittedOn']}</td>
                                                    </tr>
                                                    )
                                                })}
                                            </tbody>
                                        </table>

                                        <Button className="pay-cash-btn refund-txn-btn" onClick={this.confirmCash}>Submit</Button>
                                        <div className="student-details refundTable" style={{ width: '100%' }}>
                                            <br />
                                            <ZqTable variant='secondary' className="pending-amt-table"
                                                data={this.state.RefundDetails}
                                                onRowCheckBox={(e, value, index, si) => { this.selectedItem(e, value, index, si) }}
                                                rowClick={(item) => { this.rowClick(item) }}
                                                allSelect={(allItems, a) => { this.selectAll(allItems, a) }}
                                            />
                                            <br />
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </React.Fragment>


                    </div>
                    {/* : null} */}
                </div>)
            //  <PaymentPage paymentAmount={this.state.paymentAmount} paymentType={this.state.paymentType} studentID={this.state.studentID} onCardSubmit={this.onCardSubmit} onCashSubmit={this.onCashSubmit} />
            case 2:
                return <React.Fragment>
                    <div className="payment-form refundForm">
                        {this.state.viewFormData == true ?
                            <ZenForm inputData={this.state.pendingAmountData} onInputChanges={this.onInputChanges} onSubmit={this.onRefundSubmit} /> : null}
                    </div>
                    {this.state.nextLoader ?
                        <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                            <CircularProgress size={24} />
                        </div> : null}
                </React.Fragment>
            case 3:
                return <RefundConfirmation finishProcess={this.finishProcess} paymentID={this.state.paymentID} transactionId={"txt100"} refundAmount={this.state.refundAmount} studentName={this.state.StudentName} />
            default:
                return '***';
        }
    }
    handleNext = () => {
        let a = this.state.activeStep;
        this.setState({ activeStep: a + 1 })
    }
    handleBack = () => {
        let a = this.state.activeStep;
        this.setState({ activeStep: a - 1 })
    }
    finishProcess = () => {
        // this.setState({ activeStep: 0, circularLoader: false, payementForm: false, paymentAmount: Number(0) })
        window.location.reload();
    }
    formatAmount = (amount) => {
        let a = Number(amount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })
        let b = a.replace('₹', "")
        return b
    }
    onDownloadEvent = () => {
        this.setState({ isLoader: true })
        axios.get(`${this.state.env['zqBaseUri']}/edu/reports/refund?orgId=${this.state.orgId}`, {
            headers: {
                'Authorization': localStorage.getItem("auth_token")
            }
        })
            .then(resp => {
                console.log(resp);
                var createXlxsData = []
                let regId = this.context.reportLabel || "Reg ID"
                let batch = this.context.classLabel || "CLASS/BATCH"
                resp.data.data.map(item => {
                    var description = ""
                    item.description.map(desc => {
                        description = description + ' ' + desc.feeType
                    })
                    var demandNoteId = item.demandNoteId['0']['primaryTransaction']
                    // item.demandNoteId.map(dnId => {
                    //     demandNoteId = demandNoteId + ' ' + dnId.primaryTransaction
                    // })
                    createXlxsData.push({
                        "REFUND ID": item.refundId,
                        "RECEIPT ID": demandNoteId,
                        "REG ID": item.regId,
                        "STUDENT NAME": item.studentName,
                        "ACADEMIC YEAR": item.academicYear,
                        "CLASS/BATCH": item["class/Batch"],
                        "REMARKS": description != undefined ? description : "-",
                        // "REFUNDED": this.formatCurrency(item.refunded),
                        "REFUND AMOUNT": this.formatAmount(item.refunded),
                        "INITIATED ON": this.onDateFormat(item.refundedOn),
                        "COMPLETED ON": this.onDateFormat(item.refundedOn),
                        // "txnId": item.txnId,
                        "STATUS": item.status
                    })

                })
                var ws = xlsx.utils.json_to_sheet(createXlxsData);
                var wb = xlsx.utils.book_new();
                xlsx.utils.book_append_sheet(wb, ws, "Refund Reports");
                xlsx.writeFile(wb, "refund_reports.xlsx");
                this.setState({ isLoader: false })
            })
            .catch(err => {
                console.log(err, 'err0rrrrr')
                this.setState({ isLoader: false })
            })
    }
    onDataCall = () => {
        this.setState({ isLoader: true })
        this.setState({ printReportArr: [] }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/refund?orgId=${this.state.orgId}&page=${this.state.page}&limit=${this.state.limit}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token"),
                }
            }).then(resp => {
                var refundData = []
                let resData = resp.data.data;
                if (resData.length > 0) {
                    resData.map(item => {
                        refundData.push({
                            "REFUND ID": item.displayName,
                            "RECEIPT ID": item.paymentRefId != undefined ? item.paymentRefId : "-",
                            "REG ID": item.studentRegId,
                            "STUDENT NAME": item.studentName,
                            "ACADEMIC YEAR": item.academicYear,
                            "CLASS/BATCH": item.class,
                            "REMARKS": item.data.modeDetails != undefined ? item.data.modeDetails.remarks : "-",
                            "REFUND AMOUNT": this.formatCurrency(item.amount),
                            "INITIATED ON": new Date(item.createdAt).toLocaleDateString(),
                            "COMPLETED ON": new Date(item.updatedAt).toLocaleDateString(),
                            "STATUS": item.status,
                            "Item": JSON.stringify(item)
                        })
                    })
                    console.log("refundData", refundData)
                    this.setState({ printReportArr: refundData, totalPages: resp.data.totalPages, totalRecord: resp.data.totalRecord })
                }
                else {
                    this.setState({ RefundStatus: "No Data" });
                }
                this.setState({ isLoader: false })
            })
                .catch(err => {
                    console.log(err);
                    this.setState({ printReportArr: [], isLoader: false, noData: true, RefundStatus: "No Data" })
                })
        })
    }

    handleTabChange = (e, value) => {
        console.log(value); this.setState({ activeTab: value })
    }

    closePreview = () => {
        this.setState({ showPreview: false });
    }

    onPreviewRefund = (item) => {
        this.setState({ showPreview: true, previewRefundData: item, PaymentFormData: [], activeTab: 0 });
        let previewData = this.state.previewData;
        let parsedData = JSON.parse(item.Item)
        previewData.map(task => {
            if (task['name'] == "registerID") {
                task['defaultValue'] = item["REG ID"]
            }
            else if (task['name'] == "studentName") {
                task['defaultValue'] = item["STUDENT NAME"]
            }
            else if (task['name'] == "parentName") {
                task['defaultValue'] = "-"
            }
            else if (task['name'] == "classBatch") {
                task['defaultValue'] = item["CLASS/BATCH"]
            }
            else if (task['name'] == "admittedOn") {
                task['defaultValue'] = "-"
            }
            else if (task['name'] == "refundAmt") {
                task['defaultValue'] = item["REFUND AMOUNT"]
            }
        })
        let PaymentFormData = [];
        let paymentType = parsedData.data.mode != undefined ? parsedData.data.mode : "";
        if (paymentType != "") {
            if (paymentType === "Cheque") {
                paymentType = "Cheque/DD"
            }
            if (paymentType === "card") {
                paymentType = "Card"
            }
            if (paymentType === "Razorpay") {
                paymentType = "Netbanking"
            }
            console.log(paymentType)
            let payloadData = parsedData.data.modeDetails;
            if (paymentType.toLowerCase() === 'cash') {
                PaymentFormData.push({
                    date: payloadData != undefined ? payloadData.instrumentDate : "",
                    amount: parsedData.amount != undefined ? parsedData.amount : "",
                    remarks: payloadData != undefined ? payloadData.remarks : ""
                })
            }
            else if (paymentType.toLowerCase() === 'cheque/dd') {
                PaymentFormData.push({
                    date: payloadData != undefined ? payloadData.instrumentDate : "",
                    amount: parsedData.amount != undefined ? parsedData.amount : "",
                    ChequeNumber: payloadData != undefined ? payloadData.instrumentNo : "",
                    bankname: payloadData != undefined ? payloadData.bankName : "",
                    ['Branch Name']: payloadData != undefined ? payloadData.branchName : "",
                    remarks: payloadData != undefined ? payloadData.remarks : ""
                })
            }
            else if (paymentType.toLowerCase() === 'card') {
                PaymentFormData.push({
                    date: payloadData != undefined ? payloadData.instrumentDate : "",
                    amount: parsedData.amount != undefined ? parsedData.amount : "",
                    cardNumber: payloadData != undefined ? payloadData.cardNumber : "",
                    creditDebit: payloadData != undefined ? payloadData.creditDebit : "",
                    cardType: payloadData != undefined ? payloadData.cardType : "",
                    nameOnCard: payloadData != undefined ? payloadData.nameOnCard : "",
                    transactionNumber: payloadData != undefined ? payloadData.transactionId : "",
                    remarks: payloadData != undefined ? payloadData.remarks : ""
                })
            }
            else if (paymentType.toLowerCase() === 'netbanking') {
                PaymentFormData.push({
                    date: payloadData != undefined ? payloadData.instrumentDate : "",
                    amount: parsedData.amount != undefined ? parsedData.amount : "",
                    bankname: payloadData != undefined ? payloadData.bankName : "",
                    netBankingType: payloadData != undefined ? payloadData.netBankingType : "",
                    UTRNumber: payloadData != undefined ? payloadData.transactionId : "",
                    remarks: payloadData != undefined ? payloadData.remarks : ""
                })
            }
            else if (paymentType.toLowerCase() === 'wallet') {
                PaymentFormData.push({
                    date: payloadData != undefined ? payloadData.instrumentDate : "",
                    amount: parsedData.amount != undefined ? parsedData.amount : "",
                    walletType: payloadData != undefined ? payloadData.walletType : "",
                    transactionNumber: payloadData != undefined ? payloadData.transactionId : "",
                    remarks: payloadData != undefined ? payloadData.remarks : ""
                })
            }
        } else {
            PaymentFormData.push({ amount: parsedData.amount != undefined ? parsedData.amount : "-" })
        }
        this.setState({ paymentType: paymentType, previewData: previewData, PaymentFormData: PaymentFormData, StudentRegId: item["REG ID"], StudentName: item["STUDENT NAME"], StudentClass: item["CLASS/BATCH"] });
    }
    onPaymentType = (type) => {
        this.setState({ paymentType: type })
    }

    refundPayment = (data, formItem, format) => {
        this.setState({ circularLoader: true }, () => {
            console.log(this.state.PaymentFormData)
            // console.log("mode", this.props.paymentType)
            let payloadData = this.state.previewRefundData;
            let itemVal = JSON.parse(payloadData.Item)
            let paymentType = this.state.paymentType || "Cash";
            console.log("itemVal", itemVal)
            if (Number(data.amount.value) <= 0) {
                alert("Entered amount is not valid.")
                this.setState({ circularLoader: false })
            } else {
                let payload = {
                    "displayName": itemVal.displayName,
                    "transactionType": itemVal.transactionType,
                    "transactionSubType": itemVal.transactionSubType,
                    "transactionDate": itemVal.transactionDate,
                    "studentId": itemVal._id,
                    "studentRegId": itemVal.studentRegId,
                    "studentName": itemVal.studentName,
                    "class": itemVal.class,
                    "academicYear": itemVal.academicYear,
                    "programPlan": itemVal.programPlan,
                    "amount": itemVal.amount,
                    "status": itemVal.status,
                    "paid": itemVal.data.paid,
                    "paymentRefId": itemVal.paymentRefId,
                    "paymentTransactionId": null,
                    "relatedTransactions": [
                        itemVal.paymentRefId
                    ],
                    "orgId": this.state.orgId,
                    "transactionId": null,
                    "data": {
                        "orgId": this.state.orgId,
                        "studentId": itemVal.data.studentId,
                        "studentRegId": itemVal.data.studentRegId,
                        "class": itemVal.data.class,
                        "academicYear": itemVal.data.academicYear,
                        "issueDate": itemVal.data.issueDate,
                        "dueDate": itemVal.data.dueDate,
                        "mode": paymentType,
                        "modeDetails": {
                            "netBankingType": paymentType === 'Netbanking' ? format[3].defaultValue : null,
                            "walletType": paymentType === 'Wallet' ? format[2].defaultValue : null,
                            "instrumentNo": paymentType === 'Cheque/DD' ? data.ChequeNumber.value : null,
                            "creditDebit": paymentType === 'Card' ? format[3].defaultValue : null,
                            "cardType": paymentType === 'Card' ? format[4].defaultValue : null,
                            "nameOnCard": paymentType === 'Card' ? data.nameOnCard.value : null,
                            "cardNumber": paymentType === 'Card' ? data.cardNumber.value : null,
                            "instrumentDate": itemVal.updatedAt,
                            "bankName": paymentType === 'Cheque/DD' ? data.bankname.value : paymentType === 'Netbanking' ? data.bankname.value : null,
                            "branchName": paymentType === 'Cheque/DD' ? data["Branch Name"].value : null,
                            "transactionId": paymentType === 'Netbanking' ? data.UTRNumber.value : paymentType === 'Card' ? data.transactionNumber.value : paymentType === 'Wallet' ? data.transactionNumber.value : null,
                            "remarks": data.remarks.value != "" ? data.remarks.value : "-"
                        },
                        "feesBreakUp": [
                            {
                                "feeTypeId": itemVal.data.feesBreakUp[0].feeTypeId,
                                "feeTypeCode": itemVal.data.feesBreakUp[0].feeTypeCode,
                                "amount": itemVal.data.feesBreakUp[0].amount,
                                "feeType": itemVal.data.feesBreakUp[0].feeType
                            }
                        ]
                    },
                    "studentFeeMapId": null
                }
                console.log("************", payload)
                axios.put(`${this.state.env['zqBaseUri']}/edu/refund/${itemVal.displayName}?orgId=${this.state.orgId}`, payload, {
                    headers: {
                        "Authorization": localStorage.getItem('auth_token')
                    }
                })
                    .then(response => {
                        console.log('res', response)
                        if (response.data.success != false) {
                            Alert.success(response.data.message)
                            this.setState({ showPreview: false });
                            this.onDataCall()
                        } else {
                            Alert.error(response.data.message)
                            this.setState({ showPreview: false });
                            this.onDataCall()
                        }
                        this.setState({ circularLoader: false })
                    }).catch(err => {
                        console.log('eee', err)
                        this.setState({ circularLoader: false })
                    })

            }
        })
    }
    onDateFormat = (d) => {
        let dateField = new Date(String(d));
        let month = dateField.getMonth() + 1;
        month = String(month).length == 1 ? `0${String(month)}` : String(month);
        let date = dateField.getDate();
        date = String(date).length == 1 ? `0${String(date)}` : String(date);
        let year = dateField.getFullYear();
        return `${date}/${month}/${year}`;
    }
    addNewRefund = () => {
        this.setState({ viewRefundNew: true })
    }
    goBackRefundList = () => {
        this.setState({ viewRefundNew: false })
    }
    onGetFormInputData = (getpropsData) => {
        if (getpropsData != undefined) {
            if (getpropsData.paymentType = 'Card') {
                if (getpropsData.name === "cardType") {
                    this.setState({ cardType: getpropsData.defaultValue });
                    console.log("cardType", getpropsData.defaultValue)
                } else if (getpropsData.name === "creditDebit") {
                    this.setState({ creditDebit: getpropsData.defaultValue });
                    console.log("creditDebit", getpropsData.defaultValue)
                }
            } else if (getpropsData.paymentType === 'Netbanking') {
                if (getpropsData.name = "netBankingType") {
                    this.setState({ netBankingType: getpropsData.defaultValue });
                    console.log("netBankingType", getpropsData.defaultValue)
                }
            } else if (getpropsData.paymentType === 'Wallet') {
                if (getpropsData.name = "walletType") {
                    this.setState({ walletType: getpropsData.defaultValue });
                    console.log("walletType", getpropsData.defaultValue)
                }
            }
        }
    }
    render() {
        const ColorlibConnector = withStyles({
            alternativeLabel: { top: 22 },
            active: { '& $line': { backgroundColor: "#1359C1", marginTop: '-10px', transitionDelay: '2s' }, },
            completed: { '& $line': { backgroundColor: '#36B37E', marginTop: '-10px' }, },
            line: { height: 2, border: 0, backgroundColor: '#ccc', borderRadius: 1, marginTop: '-10px' },
        })(StepConnector);
        return (
            <React.Fragment>
                {!this.state.showPreview ?
                    <React.Fragment>
                        {this.state.viewRefundNew == true ?
                            <React.Fragment>
                                <div className="refund-mainDiv">
                                    <div className="trial-balance-header-title">
                                        <KeyboardBackspaceSharpIcon title="go back" className="keyboard-back-icon" onClick={this.goBackRefundList} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                        <p className="top-header-title" title="go back" onClick={this.goBackRefundList}>| Refund</p>
                                    </div>

                                    <div className="migration-main-div portal-login-div vendor-invoice-upload-wrap pdf-extraction-upload-wrap" style={{ height: "calc(100vh - 100px)" }}>
                                        <div className="migration-header-stepper-section">
                                            <div className="zenqore-stepper-section">
                                                <Steps current={this.state.activeStep} currentStatus="process" vertical={false} >
                                                    <Steps.Item title="Fetch Details" />
                                                    <Steps.Item title="Refund Information" />
                                                    <Steps.Item title="Refund" />
                                                    <Steps.Item title="Confirmation" />
                                                </Steps>
                                            </div>
                                        </div>
                                        <div className="migration-body-content-section" style={{ backgroundColor: "none !important", height: "calc(100vh - 215px)", overflowY: "auto", paddingBottom: "30px", border: "1px solid #dfe1e6" }}>
                                            {this.getStepContent(this.state.activeStep)}
                                        </div>
                                    </div>
                                </div>
                            </React.Fragment> :
                            <React.Fragment >
                                {this.state.isLoader && <Loader />}
                                <div className="reports-student-fees list-of-students-mainDiv">
                                    <div className="trial-balance-header-title">
                                        <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                        <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Refund</p>
                                    </div>
                                    <div className="reports-body-section print-hd">
                                        <React.Fragment>
                                            <ContainerNavbar containerNav={this.state.containerNav} onDownload={() => this.onDownloadEvent()} onAddNew={() => { this.addNewRefund(); }} />
                                            <div className="print-time">{this.state.printTime}</div>
                                        </React.Fragment>
                                        <div className="reports-data-print-table">
                                            <div className="">
                                                <React.Fragment>
                                                    <div className="refund-table-main" style={{ backgroundColor: "#fff", borderColor: "#dfe1e6", marginTop: "10px" }}>
                                                        {this.state.printReportArr.length == 0 ? <p className="noprog-txt">{this.state.RefundStatus}</p> :
                                                            <ZqTable data={this.state.printReportArr}
                                                                className="refund-listing-table"
                                                                rowClick={(item, index, hd) => { this.onPreviewRefund(item, index, hd); }}
                                                                onRowCheckBox={() => { return null }} />
                                                        }
                                                    </div>
                                                    <div>
                                                        {this.state.printReportArr.length == 0 ? null :
                                                            <PaginationUI
                                                                total={this.state.totalRecord}
                                                                onPaginationApi={this.onPaginationChange}
                                                                totalPages={this.state.totalPages}
                                                                limit={this.state.limit}
                                                                currentPage={this.state.page} />
                                                        }
                                                    </div>
                                                </React.Fragment>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </React.Fragment >}
                    </React.Fragment>
                    : <React.Fragment>
                        <div className="preview-table-wrap">
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon title="go back" className="keyboard-back-icon" onClick={this.closePreview} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title" title="go back" onClick={this.closePreview}>| Refund</p>
                            </div>
                            <div className="preview-main">
                                <Tabs
                                    value={this.state.activeTab}
                                    indicatorColor="primary"
                                    textColor="primary"
                                    onChange={this.handleTabChange}
                                    className='preview-tabs'>
                                    {this.state.tabData.map((tab, tabIndex) => {
                                        return <Tab label={tab} />
                                    })}
                                </Tabs>
                                <div className="preview-list-tab-data">
                                    {this.state.activeTab === 0 ?
                                        <React.Fragment>
                                            <ZenForm inputData={this.state.previewData} />
                                        </React.Fragment>
                                        : this.state.activeTab === 1 ?
                                            <React.Fragment>
                                                <div className="refund-preview-payment">
                                                    <Payment
                                                        StudentRegId={this.state.StudentRegId}
                                                        StudentName={this.state.StudentName}
                                                        StudentClass={this.state.StudentClass}
                                                        paymentType={this.state.paymentType}
                                                        PaymentFormData={this.state.PaymentFormData}
                                                        refundPayment={this.refundPayment}
                                                        onPaymentType={this.onPaymentType}
                                                        onInputChanges={this.onGetFormInputData}
                                                        // onPaymentPreview={false}
                                                        isSubmit={true} />
                                                </div>
                                            </React.Fragment>
                                            : null}
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                }
            </React.Fragment>
        )
    }
}
export default withRouter(RefundTxn) 