import React, { Component } from "react";
import '../../../../scss/cancel-transaction.scss';
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import Loader from "../../../../utils/loader/loaders";
import axios from "axios";
import PaginationUI from "../../../../utils/pagination/pagination";
import PaginationUIOne from "../../../../utils/pagination/pagination";
import ZqTable from "../../../../utils/Table/table-component";
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import cancelTransaction from './cancel-transactions.json';
import DateFormatter from '../../../date-formatter/date-formatter';
import Checkbox from "@material-ui/core/Checkbox";
import { Button } from 'rsuite';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import DateFormatContext from '../../../../gigaLayout/context';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
class CancelOption extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            tableHeader: ["ID", "RECEIPT ID", "REG ID", "STUDENT NAME", "ACADEMIC YEAR", "CLASS/BATCH", "FEE TYPE", "TOTAL FEES", "PAID", "BALANCE", "PAID ON", "STATUS"],
            tableHeaderNew: ["ID", "REG ID", "STUDENT NAME", "ACADEMIC YEAR", "CLASS/BATCH", "FEE TYPE", "TOTAL FEES", "PAID", "BALANCE", "PAID ON", "STATUS"],
            printReportActiveArr: [],
            printReportCancelArr: [],
            viewType: 'cancel',
            checked: false,
            openConfirmModel: false,
            selectedItem: [],
            checked: false,
            totalActiveRes: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 1,
            tableResTxt: "Fetching Data...",
            pageOne: 1,
            limitOne: 10,
            totalRecordOne: 0,
            totalPagesOne: 1,
            tableResTxtOne: "Fetching Data...",
            snackbar: { openNotification: false, NotificationMessage: "", status: "success" },
            openConfirmFinal: false,
            reasonText: "",
            buttonEnableReason: false
        }
    }
    static contextType = DateFormatContext;
    componentDidMount() {
        let tableHeader = ["ID", "RECEIPT ID", `${this.context.reportLabel ? this.context.reportLabel : "REG ID"}`, "STUDENT NAME", "ACADEMIC YEAR", `${this.context.classLabel ? this.context.classLabel : "CLASS/BATCH"}`, "TOTAL PAID", "CANCELLED AMOUNT", "REASON", "CANCELLED ON", "STATUS"];
        let tableHeaderNew = ["RECEIPT ID", `${this.context.reportLabel ? this.context.reportLabel : "REG ID"}`, "STUDENT NAME", "ACADEMIC YEAR", `${this.context.classLabel ? this.context.classLabel : "CLASS/BATCH"}`, "TOTAL FEES", "PAID", "BALANCE", "PAID ON", "STATUS"];
        this.setState({ tableHeader, tableHeaderNew })
        this.getCancelledList()
    }
    getCancelledList = () => {
        this.setState({ printReportCancelArr: [] }, () => {
            let headers = {
                'Authorization': this.state.authToken
            };
            axios.get(`${this.state.env["zqBaseUri"]}/edu/getCancelTransaction?page=${this.state.pageOne}&perPage=${this.state.limitOne}`, { headers })
                .then(res => {
                    console.log("cancel", res);
                    if (res.data.data.length !== 0) {
                        let listCancelData = [];
                        res.data.data.map((data) => {
                            let feeBreakData = data.feesBreakup === undefined ? [] : data.feesBreakup
                            let descriptionData = [];
                            let totalPaid = "";
                            feeBreakData.map((dataOne) => {
                                totalPaid = Number(dataOne.paidAmount) + Number(totalPaid); 
                                descriptionData.push({
                                    "name": dataOne.feeType,
                                    "due": Number(data.amount).toFixed(2),
                                    "paid": dataOne.paidAmount === undefined ? 0 : Number(dataOne.paidAmount).toFixed(2),
                                    "paidDate": data.transactionDate,
                                    "balance": dataOne.pendingAmount === undefined ? 0 : Number(dataOne.pendingAmount).toFixed(2),
                                    "status": data.status,
                                    "txnId": data.paymentTransactionId,
                                    "totalPaid":totalPaid
                                })
                            })
                            listCancelData.push({
                                "cancelId": data.cancellationId,
                                "displayName": data.displayName,
                                "studentName": data.studentName,
                                "regId": data.regId,
                                "academicYear": data.academicYear,
                                "classBatch": data.class,
                                "DemandId": data.relatedTransactions[0],
                                "refundAmount": 0,
                                "description": descriptionData,
                                "Item": JSON.stringify(data)
                            })
                        })
                        console.log(listCancelData);
                        this.tableComponentPrepareOne(listCancelData)
                        this.setState({ totalRecordOne: res.data.totalRecord, totalPagesOne: res.data.totalPages, pageOne: res.data.page }, () => {
                            this.getActiveList()
                        })
                    }
                    else {
                        this.setState({ tableResTxtOne: "No data" })
                    }
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ tableResTxtOne: "Loading error" })
                })
        })
    }
    getActiveList = () => {
        this.setState({ printReportActiveArr: [] }, () => {
            let headers = {
                'Authorization': this.state.authToken
            };
            axios.get(`${this.state.env["zqBaseUri"]}/edu/getActiveTransaction?page=${this.state.page}&perPage=${this.state.limit}`, { headers })
                .then(res => {
                    if (res.data.data.length !== 0) {
                        console.log(res);
                        let listActiveData = [];
                        res.data.data.map((data) => {
                            let descriptionData = [];
                            data.feesBreakup.map((dataOne) => {
                                descriptionData.push({
                                    "name": dataOne.feeType,
                                    "due": Number(data.amount).toFixed(2),
                                    "paid": dataOne.paidAmount === undefined ? 0 : Number(dataOne.paidAmount).toFixed(2),
                                    "paidDate": data.transactionDate,
                                    "balance": Number(dataOne.pendingAmount).toFixed(2),
                                    "status": dataOne.status
                                })
                            })
                            listActiveData.push({
                                "displayName": data.displayName,
                                "studentName": data.studentName,
                                "regId": data.regId,
                                "academicYear": data.academicYear,
                                "classBatch": data.class,
                                "DemandId": data.relatedTransactions[0],
                                "refundAmount": 0,
                                "description": descriptionData,
                                "Item": JSON.stringify(data)
                            })
                        })
                        console.log(listActiveData);
                        this.tableComponentPrepare(listActiveData)
                        this.setState({ totalActiveRes: res.data.data, totalRecord: res.data.totalRecord, totalPages: res.data.totalPages, page: res.data.page })
                    }
                    else {
                        this.setState({ tableResTxt: "No data" })
                    }
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ tableResTxt: "Loading error" })
                })
        })
    }
    tableComponentPrepareOne = (tabData) => {
        let data = tabData;
        let tableData = [];
        let tableHd = Object.keys(data[0])
        let showDate = false
        let dateOfTableData = {}
        data.map((item, index) => {
            let trItem = {};
            trItem["checked"] = false;
            trItem["rowId"] = index;

            tableHd.map((hd) => {
                if (hd == "action") {
                    trItem["action"] = {
                        action: false,
                        options: item[hd],
                    };
                } else {
                    trItem[hd] = item[hd];
                }
            });
            tableData.push(trItem);
            if (showDate != undefined) {
                dateOfTableData[item.Date] = dateOfTableData[item.Date] == undefined ? [] : dateOfTableData[item.Date]
                dateOfTableData[item.Date].push(trItem)
            }
        })
        console.log(tableData);
        this.setState({ printReportCancelArr: tableData, dateOfTableData: dateOfTableData });
    }
    tableComponentPrepare = (tabData) => {
        let data = tabData;
        let tableData = [];
        let tableHd = Object.keys(data[0])
        let showDate = false
        let dateOfTableData = {}
        data.map((item, index) => {
            let trItem = {};
            trItem["checked"] = false;
            trItem["rowId"] = index;

            tableHd.map((hd) => {
                if (hd == "action") {
                    trItem["action"] = {
                        action: false,
                        options: item[hd],
                    };
                } else {
                    trItem[hd] = item[hd];
                }
            });
            tableData.push(trItem);
            if (showDate != undefined) {
                dateOfTableData[item.Date] = dateOfTableData[item.Date] == undefined ? [] : dateOfTableData[item.Date]
                dateOfTableData[item.Date].push(trItem)
            }
        })
        console.log(tableData);
        this.setState({ printReportActiveArr: tableData, dateOfTableData: dateOfTableData });
    }
    onDownloadEvent = () => { }
    cancelNewTransaction = () => {
        this.setState({ page: 1, limit: 10, totalRecord: 0, totalPages: 1, printReportActiveArr: [] }, () => {
            this.setState({ viewType: 'add' })
            this.getActiveList()
        })
    }
    openModelConfirm = () => {
        console.log("true");
        this.setState({ openConfirmModel: true })
    }
    closeModal = () => {
        this.setState({ openConfirmModel: false })
    }
    closeConfirmFinal = () => {
        this.setState({ openConfirmFinal: false })
    }
    confirmReason = () => {
        this.setState({ openConfirmFinal: true, openConfirmModel: false })
    }
    confirmModal = () => {
        console.log(this.state.selectedItem);
        let arrangePayload = [];
        this.state.selectedItem.map((data) => {
            let parseData = JSON.parse(data.Item);
            parseData['reasonForCancel'] = this.state.reasonText;
            arrangePayload.push(parseData)
        })
        console.log(arrangePayload);
        let headers = {
            'Authorization': this.state.authToken
        };
        let payloadData = arrangePayload
        axios.post(`${this.state.env["zqBaseUri"]}/edu/cancelTransaction`, payloadData, { headers })
            .then(res => {
                console.log(res);
                this.setState({ LoaderStatus: true }, () => {
                    if (res.data.status === "failure") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Unable to cancel transactions. Please try again", status: "error", }
                        this.setState({ snackbar: snackbarUpdate, LoaderStatus: false, openConfirmModel: false, openConfirmFinal:false, pageOne: 1, limitOne: 10, totalRecordOne: 0, totalPagesOne: 1, printReportCancelArr: [] }, () => {
                            this.getCancelledList()
                            setTimeout(() => {
                                snackbarUpdate.openNotification = false
                                this.setState({ snackbar: snackbarUpdate })
                            }, 1500)
                        });
                    }
                    else if (res.data.status === "success") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Transactions cancelled successfully", status: "success", }
                        this.setState({ snackbar: snackbarUpdate, LoaderStatus: false, openConfirmModel: false, openConfirmFinal:false, viewType: 'cancel', pageOne: 1, limitOne: 10, totalRecordOne: 0, totalPagesOne: 1, printReportCancelArr: [] }, () => {
                            this.getCancelledList()
                            setTimeout(() => {
                                snackbarUpdate.openNotification = false
                                this.setState({ snackbar: snackbarUpdate })
                            }, 1500)
                        });
                    }
                })
            })
            .catch(err => {
                console.log(err);
            })
        // this.setState({ openConfirmModel: false, viewType: 'cancel' })
    }
    onCheckbox = (e, item, index) => {
        console.log(e, item, index);
        this.setState({ checked: !this.state.checked }, () => {
            let tableData = this.state.printReportActiveArr;
            tableData.map((item) => {
                item.checked = this.state.checked;
            });
            this.setState({ tableData: tableData }, () => {
                let selectedItem = []
                if (this.state.checked) {
                    tableData.map((item) => {
                        if (item.checked) {
                            selectedItem.push(item);
                        }
                    });
                }
                this.setState({ selectedItem: selectedItem }, () => {
                    console.log(this.state.checked, this.state.selectedItem);
                });
            })
        });
    }
    onRowCheckBox = (e, row, index) => {
        console.log(e);
        let tableData = this.state.printReportActiveArr;
        tableData[index].checked = !row.checked;
        let selectedItem = [];
        this.setState({ tableData: tableData, selectedItem: selectedItem }, () => {
            tableData.map((item) => {
                if (item.checked) {
                    selectedItem.push(item);
                }
            });
            this.setState({ selectedItem: selectedItem }, () => {
                if (this.state.printReportActiveArr.length == this.state.selectedItem.length) {
                    this.setState({ checked: true });
                } else {
                    this.setState({ checked: false });
                }
            });
            console.log('table selectedItem', this.state.selectedItem)
            console.log(e, row, index, this.state.selectedItem);
        });
    }
    onPaginationChange = (page, limit) => { this.setState({ page: page, limit: limit }); this.getActiveList() };
    onPaginationChangeOne = (page, limit) => { this.setState({ pageOne: page, limitOne: limit }); this.getCancelledList() };
    goBackFun = () => {
        this.setState({ pageOne: 1, limitOne: 10, totalRecordOne: 0, totalPagesOne: 1, printReportCancelArr: [] }, () => {
            this.setState({ viewType: 'cancel' })
            this.getCancelledList()
        })
    }
    validateReasonData = (e) => {
        console.log(e.currentTarget.value);
        this.setState({ reasonText: e.currentTarget.value }, () => {
            if (this.state.reasonText.length !== 0) {
                this.setState({ buttonEnableReason: true })
            }
            else {
                this.setState({ buttonEnableReason: false })
            }
        })
    }
    displayAlert = () => {
        alert('Please enter valid reason and click confirm')
    }
    render() {
        return (
            <div className="cancel-transaction-main-div">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.viewType === 'cancel' ?
                    < div className="reports-student-fees list-of-students-mainDiv" >
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Cancel Transactions</p>
                        </div>
                        <div className="reports-body-section print-hd">
                            <React.Fragment>
                                <ContainerNavbar containerNav={cancelTransaction.containerNav} onAddNew={this.cancelNewTransaction} onDownload={() => this.onDownloadEvent()} />
                            </React.Fragment>
                        </div>
                        <div className="print-cancelled-transaction">
                            <div className="table-main-div">
                                {this.state.printReportCancelArr.length !== 0 ?
                                    <table className="transaction-cancel-table">
                                        <thead>
                                            <tr>
                                                {this.state.tableHeader.map((data, i) => {
                                                    return <th className={'student-fee ' + String(data).replace(' ', '')} key={i + 1}>{data}</th>
                                                })}
                                            </tr>
                                        </thead>
                                        {this.state.printReportCancelArr.length !== 0 ?
                                            <tbody>
                                                {this.state.printReportCancelArr.map((data, i) => {
                                                    return (
                                                        <tr key={i + 1} id={i + 1} className="table-preview" >
                                                            <td className="transaction-vch-type">{data.cancelId}</td>
                                                            <td className="transaction-vch-type">{data.displayName}</td>
                                                            <td className="transaction-vch-num" >{data.regId}</td>
                                                            <td className="transaction-vch-num" >{data.studentName}</td>
                                                            <td className="transaction-vch-type">{data.academicYear}</td>
                                                            <td className="transaction-vch-type">{data.classBatch}</td>
                                                            {/* <td className="transaction-particulars">
                                                                {data.description.map((dataOne, c) => {
                                                                    return (
                                                                        <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}>{dataOne.name}</p>
                                                                    )
                                                                })}
                                                            </td> */} 
                                                            <td className="transaction-debit">
                                                                {data.description.map((dataOne, i) => {
                                                                    if (i === 0) {
                                                                        return (
                                                                            <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}>{Number(Math.abs(dataOne.totalPaid)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                                                        )
                                                                    }
                                                                    else { }
                                                                })}
                                                            </td>
                                                            <td className="transaction-debit" >
                                                                {data.description.map((dataOne, i) => {
                                                                    if (i === 0) {
                                                                        return (
                                                                            <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}>{Number(Math.abs(dataOne.paid)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                                                        )
                                                                    }
                                                                })}
                                                            </td>
                                                            <td className="transaction-debit">
                                                            <p>{JSON.parse(data.Item).reasonForCancel}</p>
                                                            </td>
                                                            <td className="transaction-debit">
                                                                {data.description.map((dataOne, i) => {
                                                                    if (i === 0) {
                                                                        return (
                                                                            <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}> {dataOne.paidDate !== '-' ? new Date(dataOne.paidDate).toLocaleDateString() : '-'} </p>
                                                                        )
                                                                    }
                                                                })}
                                                            </td>
                                                            {/* <td className="transaction-vch-type">{Number(Math.abs(data.refundAmount)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</td> */}
                                                            <td className="transaction-debit">
                                                                <p
                                                                    className="Status-Partial"
                                                                    style={{ color: "#000000", padding: "10px", textAlign: "center", borderBottom: "none" }}><span>Cancelled</span></p>
                                                            </td>
                                                        </tr>)
                                                })}
                                            </tbody> : ""}
                                    </table> : <p className="noprog-txt">{this.state.tableResTxtOne}</p>}
                            </div>
                            <div>
                                {this.state.printReportCancelArr.length !== 0 ?
                                    <PaginationUIOne
                                        total={this.state.totalRecordOne}
                                        onPaginationApi={this.onPaginationChangeOne}
                                        totalPages={this.state.totalPagesOne}
                                        limit={this.state.limitOne}
                                        currentPage={this.state.pageOne} />
                                    :
                                    null
                                }
                            </div>
                        </div>
                    </div> :
                    this.state.viewType === 'add' ?
                        <div className="reports-student-fees list-of-students-mainDiv">
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.goBackFun} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title" onClick={this.goBackFun}>| Cancel Transactions | Select Transactions</p>
                            </div>
                            <div className="add-new-cancel-transaction">
                                <div className="left-sec">
                                    <React.Fragment>
                                        <ContainerNavbar containerNav={cancelTransaction.containerNavNew} />
                                    </React.Fragment>
                                </div>
                                <div className="right-sec">
                                    <button
                                        onClick={this.state.selectedItem.length === 1 ? this.openModelConfirm : ""}
                                        style={{ opacity: this.state.selectedItem.length === 1 ? "1" : "0.70", cursor: this.state.selectedItem.length === 1 ? "pointer" : "no-drop" }}
                                    >
                                        Cancel</button>
                                </div>
                            </div>
                            <div className="print-cancelled-transaction">
                                <div className="table-main-div">
                                    {this.state.printReportActiveArr.length !== 0 ?
                                        <table className="transaction-cancel-table">
                                            <thead>
                                                <tr>
                                                    <th key={0}>
                                                        <Checkbox
                                                            checked={this.state.checkFromParent ? this.state.checkFromParent == "checked" ? true : false : this.state.checked}
                                                            name="checkedF"
                                                            onChange={(e) => this.onCheckbox(e, 0, this.state.printReportActiveArr)}
                                                            indeterminate={!this.state.checked}
                                                            color="primary"
                                                        />
                                                    </th>
                                                    {this.state.tableHeaderNew.map((data, i) => {
                                                        return (
                                                            <th className={'student-fee ' + String(data).replace(' ', '')} key={i + 1}>{data}</th>
                                                        )
                                                    })}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.printReportActiveArr.map((data, i) => {
                                                    return (
                                                        <tr key={i + 1} id={i + 1} className="table-preview" >
                                                            <td key={i + 1}>
                                                                <Checkbox
                                                                    name={i}
                                                                    id={i}
                                                                    color="primary"
                                                                    checked={
                                                                        data.checked != undefined ? data.checked : false
                                                                    }
                                                                    onChange={(e) => this.onRowCheckBox(e, data, i)}
                                                                />
                                                            </td>
                                                            <td className="transaction-vch-type">{data.displayName}</td>
                                                            <td className="transaction-vch-num" >{data.regId}</td>
                                                            <td className="transaction-vch-num" >{data.studentName}</td>
                                                            <td className="transaction-vch-type">{data.academicYear}</td>
                                                            <td className="transaction-vch-type">{data.classBatch}</td>
                                                            {/* <td className="transaction-particulars">
                                                                {data.description.map((dataOne, c) => {
                                                                    return (
                                                                        <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}>{dataOne.name}</p>
                                                                    )
                                                                })}
                                                            </td> */}
                                                            <td className="transaction-debit">
                                                                {data.description.map((dataOne, i) => {
                                                                    if (i === 0) {
                                                                        return (
                                                                            <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}>{Number(Math.abs(dataOne.due)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                                                        )
                                                                    }
                                                                    else { }
                                                                })}
                                                            </td>
                                                            <td className="transaction-debit" >
                                                                {data.description.map((dataOne, i) => {
                                                                    if (i === 0) {
                                                                        return (
                                                                            <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}>{Number(Math.abs(dataOne.paid)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                                                        )
                                                                    }
                                                                })}
                                                            </td>
                                                            <td className="transaction-debit">
                                                                {data.description.map((dataOne, i) => {
                                                                    if (i === 0) {
                                                                        return (
                                                                            <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}>{Number(Math.abs(dataOne.balance)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</p>
                                                                        )
                                                                    }
                                                                })}
                                                            </td>
                                                            <td className="transaction-debit">
                                                                {data.description.map((dataOne, i) => {
                                                                    if (i === 0) {
                                                                        return (
                                                                            <p style={{ fontWeight: dataOne.name == "Total" ? "bold" : '' }}> {dataOne.paidDate !== '-' ? new Date(dataOne.paidDate).toLocaleDateString() : '-'} </p>
                                                                        )
                                                                    }
                                                                })}
                                                            </td>
                                                            {/* <td className="transaction-debit">
                                                                {data.description.map((dataOne, c) => {
                                                                    return (
                                                                        <p>{dataOne.txnId == null ? "-" : dataOne.txnId}</p>
                                                                    )
                                                                })}
                                                            </td> */}
                                                            {/* <td className="transaction-vch-type">{Number(Math.abs(data.refundAmount)).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</td> */}
                                                            <td className="transaction-debit">
                                                                {data.description.map((dataOne, i) => {
                                                                    if (i === 0) {
                                                                        return (
                                                                            <p className={String(dataOne.status).toLowerCase().includes("pending") ? "Status-Pending" : String(dataOne.status).toLowerCase().includes("paid") ? "Status-Active" : String(dataOne.status).toLowerCase().includes("partial") ? "Status-Partial" : null} style={{ color: dataOne.status == "Pending" ? "#FF5630" : dataOne.status == "Paid" ? "#00875A" : "#000000", fontWeight: String(dataOne.name).includes("Total") ? "" : '' }}><span>{dataOne.status}</span></p>
                                                                        )
                                                                    }
                                                                })}
                                                            </td>
                                                        </tr>)
                                                })}
                                            </tbody>
                                        </table>
                                        : <p className="noprog-txt">{this.state.tableResTxt}</p>}
                                </div>
                                <div>
                                    {this.state.printReportActiveArr.length !== 0 ?
                                        <PaginationUI
                                            total={this.state.totalRecord}
                                            onPaginationApi={this.onPaginationChange}
                                            totalPages={this.state.totalPages}
                                            limit={this.state.limit}
                                            currentPage={this.state.page} />
                                        :
                                        null
                                    }
                                </div>
                            </div>
                        </div> : null
                }

                {this.state.openConfirmModel === true ?
                    <Dialog open={this.state.openConfirmModel} aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description" className='logout-menu-wrap' >
                        <DialogTitle id="alert-dialog-title" className="logout-header-text">Reason for Cancellation <span style={{ color: "red" }}>*</span></DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description" className="logout-text">
                                <textarea style={{ height: "90px", width: "100%" }} placeholder="Enter Reason" required={true} onChange={this.validateReasonData}></textarea>
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions className="logout-header-btns">
                            <Button onClick={this.closeModal} className="btns-submit btns-cancel" color="primary" style={{ marginRight: "15px" }}>Cancel</Button>
                            <Button onClick={this.state.buttonEnableReason === true ? this.confirmReason : this.displayAlert} className="btns-submit" color="primary" autoFocus >Confirm</Button>
                        </DialogActions>
                    </Dialog> : null}

                {this.state.openConfirmFinal === true ?
                    <Dialog open={this.state.openConfirmFinal} aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description" className='logout-menu-wrap' >
                        <DialogTitle id="alert-dialog-title" className="logout-header-text">Confirmation</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description" className="logout-text">
                                {this.state.selectedItem.length === 1 ? "Are you sure you want to cancel transaction ?" : "Are you sure you want to cancel transactions ?"}
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions className="logout-header-btns">
                            <Button onClick={this.closeConfirmFinal} className="btns-submit btns-cancel" color="primary" style={{ marginRight: "15px" }}>Cancel</Button>
                            <Button onClick={this.confirmModal} className="btns-submit" color="primary" autoFocus>Confirm</Button>
                        </DialogActions>
                    </Dialog> : null}

                <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={2500} onClose={this.closeNotification}>
                    <Alert onClose={this.closeNotification}
                        severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
                        {this.state.snackbar.NotificationMessage}
                    </Alert>
                </Snackbar>

            </div>
        )
    }
}
export default CancelOption;