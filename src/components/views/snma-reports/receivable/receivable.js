import React, { Component } from 'react';
import '../../../../scss/fees-type.scss';
import '../../../../scss/setup.scss';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../utils/Table/table-component";
import PaginationUI from "../../../../utils/pagination/pagination";
import Loader from "../../../../utils/loader/loaders";
import LoanDataJson from './receivable.json';
import ZenForm from "../../../../components/input/zqform";
import xlsx from 'xlsx';
class Receivable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            containerNav: {
                isBack: false,
                name: "List of Receivables",
                isName: true,
                isSearch: true,
                isSort: false,
                isPrint: true,
                isDownload: true,
                isShare: false,
                isNew: false,
                newName: "New",
                isSubmit: false,
                isSelectAppReport: true
            },
            tablePrintDetails: [],
            page: 1,
            limit: 10,
            selectMonth: "",
            totalRecord: 0,
            totalPages: 1,
            noProg: "Fetching Data ...",
            PreStudentName: "",
            previewList: false,
            monthData: ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"],
            totalReceivableData: [
                {
                    "Reg ID": "GHS10001",
                    "Student Name": "Aarohi Jena",
                    "Class/Batch": "`Bright Kids at Home`",
                    "Total": "0.00",
                },
                {
                    "Reg ID": "GHS10002",
                    "Student Name": "Ann Treasa Anil",
                    "Class/Batch": "Bright Kids at Home",
                    "Total": "0.00",

                },
                {
                    "Reg ID": "GHS10003",
                    "Student Name": "Aayush Laddha",
                    "Class/Batch": "Bright Kids at Home",
                    "Total": "0.00",

                }
            ],
            receivableData: [],
            previewListForm: LoanDataJson.formJson,
            appDatas: [
                {
                    label: "6 Months",
                    value: 6
                },
                {
                    label: "12 Months",
                    value: 12
                }
            ],
            filterKey: 6,
        }

    }
    componentDidMount() {
        let monthData = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"]
        let currentMonth = new Date().getMonth()
        let getListOfMonth = []

        let getJsonData = [{ "title": "Receivable" }, { "title": "Received" }]


        for (let i = currentMonth; i <= 6; i++) {
            getListOfMonth.push(monthData[i])
        }
        getJsonData.forEach(element1 => {
            getListOfMonth.forEach(element2 => {
                Object.assign(element1, { [element2]: "0.00" })
            })
        })

        // let receivableArray = []
        // receivableArray.push(getJsonData)
        this.setState({ receivableData: getJsonData })
    }
    onAddNewLoans = () => { }
    onPreviewLoan = (e) => {
        console.log(e)
        let obj = {
            "Reg ID": e["Reg ID"],
            "Class/Batch": e["Class/Batch"],
            "Student Name": e["Student Name"],
        }
        this.setState({ previewList: true })

    }
    onPaginationChange = () => { }
    onPreviewLoanList = () => {

    }
    moveBackTable = () => {
        this.setState({ previewList: false })
    }
    actionClickFun = (e) => {

        console.log(e);
    }
    handleTabChange = () => { }
    allSelect = () => { }
    onInputChanges = () => { }
    searchHandle = () => { }


    onSelectPrgmPlan = (value, item, label) => {

        let monthData = ["jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"]
        let currentMonth = new Date().getMonth()
        let getListOfMonth = []
        let getJsonData = [{ "title": "Receivable" }, { "title": "Received" }]
        let ReceivableArray = []

        this.setState({ selectMonth: value, filterKey: value, receivableData: [] }, () => {
            if (value === 6) {
                for (let i = currentMonth; i <= 6; i++) {
                    getListOfMonth.push(monthData[i])
                }
                getJsonData.forEach(element => {
                    getListOfMonth.forEach(element2 => {
                        Object.assign(element, { [element2]: "0.00" })
                    })
                })

                this.setState({ receivableData: getJsonData }, () => {
                    console.log("receivableData", this.state.receivableData)
                })
            }
            else if (value === 12) {
                for (let i = currentMonth; i <= 11; i++) {
                    getListOfMonth.push(monthData[i])
                }
                getListOfMonth.push(monthData[0])
                getJsonData.forEach(element => {
                    getListOfMonth.forEach(element2 => {
                        Object.assign(element, { [element2]: "0.00" })
                    })
                })

                this.setState({ receivableData: getJsonData })
            }


        })

        //this.onDataCall()
    }
    onSelectClean = () => {
        this.setState({ filterKey: 'All' })
    }
    onDownloadEvent = () => {
        this.setState({ isLoader: true })
        var createXlxsData = []
        var ws = xlsx.utils.json_to_sheet(createXlxsData);
        var wb = xlsx.utils.book_new();
        xlsx.utils.book_append_sheet(wb, ws, "Receivable Reports");
        xlsx.writeFile(wb, "Receivable_Reports.xlsx");
        //xlsx.writeFile(wb, "demand_note_reports.csv");
        this.setState({ isLoader: false })
    }

    goBack = () => {
        this.setState({ previewList: false })
    }
    render() {
        return (
            <div className="list-of-feeType-mainDiv">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.previewList == false ?
                    <React.Fragment>
                        <React.Fragment>
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Receivable</p>
                            </div>
                        </React.Fragment>
                        <div className="masters-body-div">
                            <React.Fragment>
                                <ContainerNavbar containerNav={this.state.containerNav} onAddNew={() => { this.onAddNewLoans(); }} searchValue={(searchValue) => this.searchHandle(searchValue)} Selectdata={this.state.appDatas} placeholder={"Duration"} onSelectDefaultValue={this.state.filterKey} onSelectClean={this.onSelectClean} onDownload={() => this.onDownloadEvent()} onSelectChange={this.onSelectPrgmPlan} />
                            </React.Fragment>
                            <div className="table-wrapper" style={{ marginTop: "10px" }}>
                                {this.state.receivableData.length == 0 ? <p className="noprog-txt">{this.state.noProg}</p> :
                                    <React.Fragment>
                                        <div className="remove-last-child-table" >
                                            <ZqTable
                                                data={this.state.receivableData}
                                                allSelect={this.allSelect}
                                                rowClick={(item) => { this.onPreviewLoan(item) }}
                                                onRowCheckBox={(item) => { this.allSelect(item) }}
                                                handleActionClick={(item) => { this.actionClickFun(item) }}
                                            />

                                        </div>
                                    </React.Fragment>
                                }
                                {this.state.receivableData.length !== 0 ? <PaginationUI onPaginationApi={this.onPaginationChange} totalPages={this.state.totalPages} limit={this.state.limit} total={this.state.totalRecord} /> : null}
                            </div>
                        </div>
                    </React.Fragment> :
                    <React.Fragment>
                        <React.Fragment>
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.goBack() }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title" style={{ cursor: 'pointer' }} onClick={() => { this.goBack() }}>| Receivable</p>
                            </div>
                        </React.Fragment>
                        <div className="table-wrapper" style={{ marginTop: "10px" }}>
                            {this.state.totalReceivableData.length == 0 ? <p className="noprog-txt">{this.state.noProg}</p> :
                                <React.Fragment>
                                    <div className="remove-last-child-table" >
                                        <ZqTable
                                            data={this.state.totalReceivableData}
                                            allSelect={this.allSelect}
                                            rowClick={(item) => { this.onPreviewLoan(item) }}
                                            onRowCheckBox={(item) => { this.allSelect(item) }}
                                            handleActionClick={(item) => { this.actionClickFun(item) }}
                                        />

                                    </div>
                                </React.Fragment>
                            }
                            {this.state.totalReceivableData.length !== 0 ? <PaginationUI onPaginationApi={this.onPaginationChange} totalPages={this.state.totalPages} limit={this.state.limit} total={this.state.totalRecord} /> : null}
                        </div>
                    </React.Fragment>
                }
            </div>
        )
    }
}
export default Receivable;