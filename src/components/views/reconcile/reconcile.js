import React, { Component } from "react";
import ZqTable from "../../../utils/Table/table-component";
import { withRouter } from "react-router-dom";
import Axios from "axios";
import PaginationUI from "../../../utils/pagination/pagination";
import Loader from "../../../utils/loader/loaders";
import "../../../scss/reconcile.scss";
import "../../../scss/student.scss";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";
import { IconButton, ButtonGroup, ButtonToolbar, SelectPicker } from "rsuite";
import SearchIcon from "@material-ui/icons/Search";
import GetAppIcon from "@material-ui/icons/GetApp";
import PrintIcon from "@material-ui/icons/Print";
import Tooltip from "@material-ui/core/Tooltip";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { Alert, Modal } from "rsuite";
import { withStyles } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import Button from "@material-ui/core/Button";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import PublishOutlinedIcon from "@material-ui/icons/PublishOutlined";
import KeyboardBackspaceSharpIcon from "@material-ui/icons/KeyboardBackspaceSharp";
import ReconcileDataFile from "./reconciliation-json.json";
import { Document, Page, pdfjs } from "react-pdf";
import Payment from "../../payment/payment";
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const ZqTooltip = withStyles((theme) => ({
    tooltip: {
        backgroundColor: "#FFC501",
        color: "black",
        boxShadow: theme.shadows[1],
        fontSize: 14,
        paddingTop: 6,
        paddingBottom: 6,
        paddingRight: 10,
        paddingLeft: 10,
        borderRadius: "0px",
    },
}))(Tooltip);

class Reconcile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            uploadPage: false,
            url: "",
            pageNumber: 1,
            numPages: null,
            env: JSON.parse(localStorage.getItem("env")),
            email: localStorage.getItem("email"),
            authToken: localStorage.getItem("auth_token"),
            noOfTransactionItem: "",
            selectedItem: "",
            noProg: "No Data",
            noOfItemList: 0,
            modalMessage: "Reminder send to Vendor",
            showAlert: false,
            page: 1,
            tab: 0,
            limit: 10,
            totalRecord: 1,
            totalPages: 1,
            totalReconciled: "",
            totalNonReconciled: "",
            totalRefundReconciled: "",
            totalDuplicates: "",
            openModal: false,
            FeesReconcileCheck: false,
            BankReconcileCheck: false,
            ShowReconcileData: true,
            PassbookCheckList: [],
            CashbookCheckList: [],
            feesReconciled: [],
            refundReconciled: [],
            duplicates: [],
            feesTransactionID: [],
            bankTransactionID: [],
            allFeeTransactions: [],
            allBankTransactions: [],
            nonReconciled: [],
            paymentType: "",
            tableIndex: [],
            PaymentFormData: [],
            DateDuration: [
                { label: "Today", value: "today" },
                { label: "This Week", value: "this week" },
                { label: "This Month", value: "this month" },
                { label: "This Quarter", value: "this quarter" },
                { label: "This Year", value: "this year" },
                { label: "Custom", value: "custom" },
            ],
        };
    }

    componentDidMount() {
        this.getReconcileData();
    }

    selectTabs = (event, newValue) => {
        let val = newValue;
        console.log("value", val);
        this.setState({ tab: val, loader: true, feesTransactionID: [], bankTransactionID: [], FeesReconcileCheck: false, BankReconcileCheck: false });
        setTimeout(() => {
            this.setState({ loader: false });
        }, 1000);
    };

    formatCurrency = (amount) => {
        return new Intl.NumberFormat("en-IN", {
            style: "currency",
            currency: "INR",
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
        }).format(amount);
    };

    dateFilter = (ev) => {
        var ts = new Date(ev);
        let getDate = `${String(ts.getDate()).length == 1 ? `0${ts.getDate()}` : ts.getDate()}`;
        let getMonth = `${String(ts.getMonth() + 1).length == 1 ? `0${ts.getMonth() + 1}` : ts.getMonth() + 1}`;
        let getYear = `${ts.getFullYear()}`;
        let today = `${getDate}/${getMonth}/${getYear}`;
        return today;
    };

    calculateAge = (dob) => {
        var diff_ms = Date.now() - dob.getTime();
        var age_dt = new Date(diff_ms);

        return Math.abs(age_dt.getUTCFullYear() - 1970);
    }

    getReconcileData = () => {
        if (this.props.previewData != undefined) {
            let data = this.props.previewData.Item != undefined ? JSON.parse(this.props.previewData.Item) : this.props.previewData.Item;
            this.setState({
                totalReconciled: data.reconciledTransactionsDetails != undefined ? data.reconciledTransactionsDetails.length : 0,
                totalNonReconciled: data.nonreconciledTransactionsDetails != undefined ? data.nonreconciledTransactionsDetails.length : 0,
                totalRefundReconciled: 0,
                totalDuplicates: 0,
            });
            // Reconciled
            let reconciledFees = [];
            let reconciledBankFees = [];
            if (data.reconciledTransactionsDetails.length != 0) {
                data.reconciledTransactionsDetails.map((item) => {
                    reconciledFees.push({
                        Date: item.transactionDate != undefined ? this.dateFilter(item.transactionDate) : "-",
                        "Student ID": item.studentRegId != undefined ? item.studentRegId : "-",
                        "Student Name": item.studentName != undefined ? item.studentName : "-",
                        "Parent Name": item.parentName != undefined ? item.parentName : "-",
                        Amount: item.amount != undefined ? this.formatCurrency(item.amount) : "-",
                        // "Type": "Fees",
                        "Fees Type": item.feeTypeCode != undefined ? item.feeTypeCode : "-",
                        Item: JSON.stringify(item)
                    });
                });
            } else { reconciledFees = [] }
            if (data.reconciledBankStmtEntryDetails.length != 0) {
                data.reconciledBankStmtEntryDetails.map((item) => {
                    reconciledBankFees.push({
                        Date: item.transactionDate != undefined ? this.dateFilter(item.transactionDate) : "-",
                        Description: item.description != undefined ? item.description : "-",
                        Debit: item.debitAmount != undefined ? this.formatCurrency(item.debitAmount) : "-",
                        Credit: item.creditAmount != undefined ? this.formatCurrency(item.creditAmount) : "-",
                        "Closing Balance": item.balance != undefined ? this.formatCurrency(item.balance) : "-",
                        Status: item.status != undefined ? item.status : "-",
                        Item: JSON.stringify(item)
                    });
                });
            } else { reconciledBankFees = [] }
            let feesReconciled = {
                feesTransaction: reconciledFees,
                bankTransaction: reconciledBankFees,
            };
            this.setState({ feesReconciled: feesReconciled, allFeeTransactions: reconciledFees, allBankTransactions: reconciledBankFees });

            // Non Reconciled
            let nonReconciledFees = [];
            let nonReconciledBankFees = [];
            if (data.nonreconciledTransactionsDetails.length != 0) {
                console.log(data)
                data.nonreconciledTransactionsDetails.map((item) => {
                    nonReconciledFees.push({
                        Date: item.transactionDate != undefined ? this.dateFilter(item.transactionDate) : "-",
                        "Student ID": item.studentRegId != undefined ? item.studentRegId : "-",
                        "Student Name": item.studentName != undefined ? item.studentName : "-",
                        "Parent Name": item.parentName != undefined ? item.parentName : "-",
                        Amount: item.amount != undefined ? this.formatCurrency(item.amount) : "-",
                        // "Type": "Fees",
                        "Fees Type": item.feeTypeCode != undefined ? item.feeTypeCode : "-",
                        Item: JSON.stringify(item)
                    });
                });
            } else { nonReconciledFees = [] }
            if (data.nonreconciledBankStmtEntryDetails.length != 0) {
                data.nonreconciledBankStmtEntryDetails.map((item) => {
                    nonReconciledBankFees.push({
                        Date: item.transactionDate != undefined ? this.dateFilter(item.transactionDate) : "-",
                        Description: item.description != undefined ? item.description : "-",
                        Debit: item.debitAmount != undefined ? this.formatCurrency(item.debitAmount) : "-",
                        Credit: item.creditAmount != undefined ? this.formatCurrency(item.creditAmount) : "-",
                        "Closing Balance": item.balance != undefined ? this.formatCurrency(item.balance) : "-",
                        Status: item.status != undefined ? item.status : "-",
                        Item: JSON.stringify(item)
                    });
                });
            } else { nonReconciledBankFees = [] }
            let nonReconciled = {
                feesTransaction: nonReconciledFees,
                bankTransaction: nonReconciledBankFees,
                bankTransaction1: nonReconciledBankFees,
            };
            this.setState({ nonReconciled: nonReconciled });
        }
    };

    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.onDataCall();
        });
        console.log(page, limit);
    };

    onReconciledFeesPagination = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.onDataCall();
        });
        console.log(page, limit);
    };

    branchSelection = (e, a, f) => {
        let selectedReconcileItem = e;
        this.setState({ selectedReconcileItem: selectedReconcileItem });
        this.onChangeTableItem(selectedReconcileItem);
    };

    onCheckBox = (e, value, index, item, tableData) => {
        let { nonReconciled, } = this.state;
        let feesTransactionID = [];
        feesTransactionID = this.state.feesTransactionID;
        let itemValue = JSON.parse(value.Item);
        if (value.checked === true) {
            let filterData = nonReconciled.bankTransaction.filter(item => item.Credit == value.Amount)
            nonReconciled["bankTransaction"] = []
            this.setState({ nonReconciled }, () => {
                nonReconciled["bankTransaction"] = filterData
                this.setState({ nonReconciled })
            })
            feesTransactionID.push(itemValue)
            if (feesTransactionID.length > 1) {
                tableData.forEach(item => {
                    item.checked = false
                })
                tableData = tableData[index].checked = true
                feesTransactionID.splice(feesTransactionID[0], 1)
                let filterData = nonReconciled["bankTransaction1"].filter(item => item.Credit == value.Amount)
                nonReconciled["bankTransaction"] = []
                this.setState({ nonReconciled }, () => {
                    nonReconciled["bankTransaction"] = filterData
                    this.setState({ nonReconciled })
                })
            }
        } else {
            nonReconciled['bankTransaction'] = []
            this.setState({ nonReconciled }, () => {
                nonReconciled['bankTransaction'] = nonReconciled["bankTransaction1"]
                const indexVal = feesTransactionID.findIndex(item => item.studentRegId == value["Student ID"])
                if (indexVal > -1) {
                    feesTransactionID.splice(indexVal, 1)
                }
                this.setState({ feesTransactionID: feesTransactionID, nonReconciled });
            })
        }
        let FeesReconcileCheck = feesTransactionID.some((item) => item)
        this.setState({ FeesReconcileCheck: FeesReconcileCheck });
    };
    onCheckBox1 = (e, value, index) => {
        console.log(value)
        let bankTransactionID = []
        bankTransactionID = this.state.bankTransactionID;
        let itemValue = JSON.parse(value.Item);
        if (value.checked === true) {
            bankTransactionID.push(itemValue)
        } else {
            const indexVal = bankTransactionID.indexOf(bankTransactionID[index])
            if (indexVal > -1) {
                bankTransactionID.splice(indexVal, 1)
            }
            this.setState({ bankTransactionID: bankTransactionID });
        }
        let BankReconcileCheck = bankTransactionID.some((item) => item)
        this.setState({ BankReconcileCheck: BankReconcileCheck });
    };
    getRowItems = (item, index, hd) => {
        if (item["GSTIN"]) {
            this.setState({ openModal: true });
        }
        console.log("item", item, index, hd);
        let selectedItem = item.Description;
    };
    closeModal = () => {
        this.setState({ openModal: false });
    };
    reconcileItems = () => {
        //Reconile btn
        this.setState({ loader: true });
        let transaction = this.state.feesTransactionID;
        let bankStatement = this.state.bankTransactionID;
        let payload = {
            transaction: transaction,
            bankStatement: bankStatement
        }
        console.log("payload", payload)
        let headers = {
            'Authorization': this.state.authToken
        };
        Axios.post(`${this.state.env['zqBaseUri']}/edu/manualReconciliation`, payload, { headers })
            .then(res => {
                setTimeout(() => {
                    Alert.success(res.data.message)
                    this.setState({ loader: false }, () => { this.props.finishReconcile(); });
                }, 1000)
            }).catch(err => {
                Alert.error("Manual Reconciliation Failed")
                this.setState({ loader: false });
            })
    };
    clickNew = () => {
        this.setState({ uploadPage: true, ShowReconcileData: false });
    };
    getUploadedFile = (e) => {
        e.stopPropagation();
        e.preventDefault();
        // console.log('selected files', e.target.files)
        if (e.target.files && e.target.files.length > 0) {
            const reader = new FileReader();
            reader.addEventListener("load", () => {
                this.setState({ uploadPage: false, isDataUpload: true }, () => {
                    this.setState({ url: reader.result }); //base64 of full pdf
                    console.log("url", reader.result);
                });
            });
            reader.readAsDataURL(e.target.files[0]);
        }
    };
    onDocumentLoadSuccess = ({ numPages }) => {
        this.setState({ numPages });
    };
    backToReconcile = () => {
        this.setState({
            uploadPage: false,
            isDataUpload: false,
            ShowReconcileData: true,
        });
    };

    finishReconcile = () => {
        this.setState({ loader: true });
        let allFeeTransactions = this.state.allFeeTransactions;
        let allBankTransactions = this.state.allBankTransactions;
        let transaction = [];
        let bankStatement = [];
        allFeeTransactions.map(item => {
            transaction.push(JSON.parse(item.Item))
        })
        allBankTransactions.map(item => {
            bankStatement.push(JSON.parse(item.Item))
        })
        let payload = {
            transactions: transaction,
            bankStatements: bankStatement
        }
        let headers = {
            'Authorization': this.state.authToken
        };
        Axios.post(`${this.state.env['zqBaseUri']}/edu/confirmReconciliation`, payload, { headers })
            .then(res => {
                console.log(res)
                setTimeout(() => {
                    Alert.success(res.data.message)
                    this.setState({ loader: false }, () => { this.props.finishReconcile(); });
                }, 1000)
            }).catch(err => {
                Alert.error("Failed to confirm. Please try again")
                this.setState({ loader: false });
            })
    };
    goBackMain = () => {
        this.props.goBackMain();
    };

    showPreviewForm = (item) => {
        console.log("Reconciled Detail", item);
        let selectedData = JSON.parse(item.Item)
        let PaymentFormData = [];
        let paymentType = selectedData.mode != undefined ? selectedData.mode[0].toUpperCase() + selectedData.mode.slice(1) : null;
        if (paymentType === "Cheque") {
            paymentType = "Cheque/DD"
        }
        if (paymentType === "Razorpay") {
            paymentType = "Netbanking"
        }
        console.log(paymentType)
        let data = selectedData.modeData;
        let amountData = data.amount.replaceAll("₹", "").replaceAll(",", "")
        if (paymentType != null) {
            this.setState({ previewPayment: true, openModal: true });
        } else {
            Alert.info("Payment information is not available")
        }
        this.setState({
            StudentRegId: selectedData.studentRegId,
            StudentClass: selectedData.class,
            StudentName: selectedData.studentName,
            paymentType: paymentType,
        });

        if (paymentType.toLowerCase() === 'cash') {
            PaymentFormData.push({
                date: data.date != undefined ? this.dateFilter(data.date) : "-",
                amount: Number(amountData) != undefined ? this.formatCurrency(amountData) : amountData,
                remarks: data.remarks != undefined ? data.remarks : "-"
            })
        }
        else if (paymentType.toLowerCase() === 'cheque/dd') {
            PaymentFormData.push({
                date: data.date != undefined ? this.dateFilter(data.date) : "-",
                amount: data.amount != undefined ? this.formatCurrency(data.amount) : "-",
                ChequeNumber: data.ChequeNumber != undefined ? data.ChequeNumber : "-",
                bankname: data.bankname != undefined ? data.bankname : "-",
                ['Branch Name']: '-',
                remarks: data.remarks != undefined ? data.remarks : "-"
            })
        }
        else if (paymentType.toLowerCase() === 'card') {
            PaymentFormData.push({
                date: data.date != undefined ? this.dateFilter(data.date) : "-",
                amount: data.amount != undefined ? this.formatCurrency(data.amount) : "-",
                cardNumber: data.cardNumber != undefined ? data.cardNumber : "-",
                creditDebit: '-',
                cardType: data.cardType != undefined ? data.cardType : "-",
                nameOnCard: data.nameOnCard != undefined ? data.nameOnCard : "-",
                transactionNumber: data.transactionId != undefined ? data.transactionId : "-",
                remarks: data.remarks != undefined ? data.remarks : "-"
            })
        }
        else if (paymentType.toLowerCase() === 'netbanking') {
            PaymentFormData.push({
                date: data.date != undefined ? this.dateFilter(data.date) : "-",
                amount: data.amount != undefined ? this.formatCurrency(data.amount) : "-",
                bankname: data.bankname != undefined ? data.bankname : "-",
                netBankingType: data.netBankingType != undefined ? data.netBankingType : "-",
                UTRNumber: data.UTRNumber != undefined ? data.UTRNumber : "-",
                remarks: data.remarks != undefined ? data.remarks : "-"
            })
        }
        else if (paymentType.toLowerCase() === 'wallet') {
            PaymentFormData.push({
                date: data.date != undefined ? this.dateFilter(data.date) : "-",
                amount: data.amount != undefined ? this.formatCurrency(data.amount) : "-",
                walletType: data.walletType != undefined ? data.walletType : "-",
                transactionNumber: data.transactionId != undefined ? data.transactionId : "-",
                remarks: data.remarks != undefined ? data.remarks : "-"
            })
        }
        this.setState({ PaymentFormData: PaymentFormData });
    };

    render() {
        return (
            <React.Fragment>
                {this.state.ShowReconcileData == true ? (
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <div className="Trialbalance-mainHeader">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.goBackMain} style={{ marginRight: 5, cursor: "pointer", marginTop: 2 }} />
                                <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem("baseURL")}/main/dashboard`); }}>| Reconciliation</p>
                            </div>
                        </div>
                        <React.Fragment>
                            <div className="reconcileTabs">
                                <div className="reconcileTabswrapper">
                                    <div className="invoice-all-tabs">
                                        <Tabs value={this.state.tab} indicatorColor="primary" textColor="primary" onChange={this.selectTabs} aria-label="tabs example">
                                            <Tab label={`Fees Reconciled [${this.state.totalReconciled}]`} className="invoice-tab"></Tab>
                                            <Tab label="Refund Reconciled" className="invoice-tab"></Tab>
                                            <Tab label="Duplicates" className="invoice-tab"></Tab>
                                            <Tab label={`Non-Reconciled [${this.state.totalNonReconciled}]`} className="invoice-tab"></Tab>
                                        </Tabs>
                                        <button className="reconcile-submit-btn" onClick={this.finishReconcile}>Done</button>
                                        {this.state.tab === 3 ? (
                                            this.state.FeesReconcileCheck == true &&
                                                this.state.BankReconcileCheck == true ? (
                                                    <button className="reconcile-btn" onClick={() => this.reconcileItems()} style={{ backgroundColor: "#0052CC", color: "white" }} > Reconcile</button>
                                                ) : (
                                                    <button className="reconcile-btn" disabled style={{ backgroundColor: "rgb(238 238 238)" }}>Reconcile</button>
                                                )
                                        ) : null}
                                    </div>
                                </div>
                                {this.state.tab != 2 ? (
                                    <div className="main-wrap" style={{ background: "#fff", borderTop: "1px solid #dfe1e6", }}>
                                        <div className="mainpage-headers">
                                            <div className="statement-head" style={{ width: "calc(46% - 2px)", borderRight: "1px solid #dfe1e6", }}>
                                                <p style={{ textTransform: "uppercase" }}> Fees Transactions </p>
                                            </div>
                                            <div className="mainpage-headers">
                                                <div className="statement-head" style={{ width: "100%" }}>
                                                    <p style={{ textTransform: "uppercase" }}>Bank Transactions</p>
                                                </div>
                                            </div>
                                            <div className="mainpage-headers"></div>
                                        </div>
                                    </div>
                                ) : null}
                                {this.state.tab === 0 ? ( // Fees Reconciled
                                    <React.Fragment>
                                        {this.state.loader ? <Loader /> : null}
                                        <div className="Reconcile-table-wrap">
                                            <div className="fee-collection-wrap" style={{ width: "45.9%", borderRight: "1px solid #dfe1e6" }}>
                                                {this.state.totalReconciled == 0 ? (
                                                    <p className="noprog-txt">{this.state.noProg}</p>
                                                ) : (
                                                        <React.Fragment>
                                                            <div className="remove-last-child-table">
                                                                <ZqTable data={this.state.feesReconciled.feesTransaction} rowClick={(item) => this.showPreviewForm(item)} />
                                                            </div>
                                                            <div className="pagination-ui">
                                                                {this.state.totalReconciled !== 0 ?
                                                                    (<PaginationUI total={this.state.totalRecord} onPaginationApi={this.onReconciledFeesPagination}
                                                                        totalPages={this.state.totalPages} limit={this.state.limit} currentPage={this.state.page} />
                                                                    ) : null}
                                                            </div>
                                                        </React.Fragment>
                                                    )}
                                            </div>
                                            <div className="invoice-page-wrapper new-table-wrap non-reconciledItem bank-collection-wrap" style={{ width: "54%" }}>
                                                {this.state.totalReconciled == 0 ? (
                                                    <p className="noprog-txt" style={{ backgroundColor: "#fff" }}>{this.state.noProg}</p>
                                                ) : (
                                                        <React.Fragment>
                                                            <div className="remove-last-child-table">
                                                                <ZqTable data={this.state.feesReconciled.bankTransaction} rowClick={() => { return null; }} className="second-table" />
                                                            </div>
                                                            <div className="pagination-ui">
                                                                {this.state.totalReconciled !== 0 ? (
                                                                    <PaginationUI
                                                                        total={this.state.totalRecord}
                                                                        onPaginationApi={this.onPaginationChange}
                                                                        totalPages={this.state.totalPages}
                                                                        limit={this.state.limit}
                                                                        currentPage={this.state.page}
                                                                    />
                                                                ) : null}
                                                            </div>
                                                        </React.Fragment>
                                                    )}
                                            </div>
                                        </div>
                                    </React.Fragment>
                                ) : this.state.tab === 1 ? ( // Refund-Reconciled
                                    <React.Fragment>
                                        {this.state.loader ? <Loader /> : null}
                                        <div className="Reconcile-table-wrap">
                                            <div
                                                className="fee-collection-wrap"
                                                style={{ width: "45.9%", borderRight: "1px solid #dfe1e6" }}
                                            >
                                                {this.state.totalRefundReconciled == 0 ? (
                                                    <p className="noprog-txt">{this.state.noProg}</p>
                                                ) : (
                                                        <React.Fragment>
                                                            <div className="remove-last-child-table">
                                                                <ZqTable data={this.state.refundReconciled.feesTransaction} rowClick={() => { return null; }} />
                                                            </div>
                                                            <div className="pagination-ui">
                                                                {this.state.totalRefundReconciled !== 0 ? (
                                                                    <PaginationUI
                                                                        total={this.state.totalRecord}
                                                                        onPaginationApi={this.onPaginationChange}
                                                                        totalPages={this.state.totalPages}
                                                                        limit={this.state.limit}
                                                                        currentPage={this.state.page}
                                                                    />
                                                                ) : null}
                                                            </div>
                                                        </React.Fragment>
                                                    )}
                                            </div>
                                            <div className="invoice-page-wrapper new-table-wrap non-reconciledItem bank-collection-wrap" style={{ width: "54%" }}>
                                                <div className={
                                                    this.state.selectedReconcileItem === "BRS" ? "brstable-wrapper" : "table-wrapper"}                                                >
                                                    {this.state.totalRefundReconciled == 0 ? (
                                                        <p className="noprog-txt" style={{ backgroundColor: "#fff" }}>{this.state.noProg}</p>
                                                    ) : (
                                                            <React.Fragment>
                                                                <div className="remove-last-child-table">
                                                                    <ZqTable data={this.state.refundReconciled.bankTransaction} rowClick={() => { return null; }} className="second-table" />
                                                                </div>
                                                                <div className="pagination-ui">
                                                                    {this.state.totalRefundReconciled !== 0 ? (
                                                                        <PaginationUI
                                                                            total={this.state.totalRecord}
                                                                            onPaginationApi={this.onPaginationChange}
                                                                            totalPages={this.state.totalPages}
                                                                            limit={this.state.limit}
                                                                            currentPage={this.state.page}
                                                                        />
                                                                    ) : null}
                                                                </div>
                                                            </React.Fragment>
                                                        )}
                                                </div>
                                            </div>
                                        </div>
                                    </React.Fragment>
                                ) : this.state.tab === 2 ? ( // Duplicates
                                    <React.Fragment>
                                        {this.state.loader ? <Loader /> : null}
                                        <div className="Reconcile-table-wrap duplicates" style={{ border: "0.5px solid #dfe1e6", backgroundColor: "#fff", }}                                        >
                                            <div className="Reconcile-table-wrap-table-one" style={{ width: "100%" }}                                            >
                                                <div className="table-wrapper">
                                                    {this.state.totalDuplicates == 0 ? (
                                                        <p className="noprog-txt">{this.state.noProg}</p>
                                                    ) : (
                                                            <React.Fragment>
                                                                <div className="remove-last-child-table remove-first-child-table">
                                                                    <ZqTable data={this.state.duplicates.duplicatesData} checked={false} rowClick={() => { return null; }} />
                                                                </div>
                                                                <div className="pagination-ui">
                                                                    {this.state.totalDuplicates !== 0 ? (
                                                                        <PaginationUI
                                                                            total={this.state.totalRecord}
                                                                            onPaginationApi={this.onPaginationChange}
                                                                            totalPages={this.state.totalPages}
                                                                            limit={this.state.limit}
                                                                            currentPage={this.state.page}
                                                                        />
                                                                    ) : null}
                                                                </div>
                                                            </React.Fragment>
                                                        )}
                                                </div>
                                            </div>
                                        </div>
                                    </React.Fragment>
                                ) : this.state.tab === 3 ? ( // Non-Reconciled
                                    <React.Fragment>
                                        {this.state.loader ? <Loader /> : null}
                                        <React.Fragment>
                                            <div className="Reconcile-table-wrap non-reconcile-table-wrap">
                                                <div className="fee-collection-wrap" style={{ width: "45.9%", borderRight: "1px solid #dfe1e6" }}>
                                                    {this.state.totalNonReconciled == 0 ? (
                                                        <p className="noprog-txt">{this.state.noProg}</p>
                                                    ) : (
                                                            <React.Fragment>
                                                                <div className="remove-last-child-table">
                                                                    <ZqTable data={this.state.nonReconciled.feesTransaction}
                                                                        onRowCheckBox={(e, row, index, item, tableData) => { this.onCheckBox(e, row, index, item, tableData); }}
                                                                        rowClick={(item) => this.showPreviewForm(item)} />
                                                                </div>
                                                                <div className="pagination-ui">
                                                                    {this.state.totalNonReconciled !== 0 ? (
                                                                        <PaginationUI
                                                                            total={this.state.totalRecord}
                                                                            onPaginationApi={this.onPaginationChange}
                                                                            totalPages={this.state.totalPages}
                                                                            limit={this.state.limit}
                                                                            currentPage={this.state.page}
                                                                        />
                                                                    ) : null}
                                                                </div>
                                                            </React.Fragment>
                                                        )}
                                                </div>
                                                <div className="invoice-page-wrapper new-table-wrap non-reconciledItem bank-collection-wrap" style={{ width: "54%" }}>
                                                    {this.state.nonReconciled.bankTransaction && this.state.nonReconciled.bankTransaction.length > 0 ? (
                                                        <React.Fragment>
                                                            <div className="remove-last-child-table">
                                                                <ZqTable data={this.state.nonReconciled.bankTransaction}
                                                                    className="second-table" onRowCheckBox={(e, row, index) => { this.onCheckBox1(e, row, index); }}
                                                                    rowClick={(item, index, hd) => { this.getRowItems(item, index, hd); }} />
                                                            </div>
                                                            <div className="pagination-ui">
                                                                {this.state.totalNonReconciled !== 0 ? (
                                                                    <PaginationUI
                                                                        total={this.state.totalRecord}
                                                                        onPaginationApi={this.onPaginationChange}
                                                                        totalPages={this.state.totalPages}
                                                                        limit={this.state.limit}
                                                                        currentPage={this.state.page}
                                                                    />
                                                                ) : null}
                                                            </div>
                                                        </React.Fragment>
                                                    ) : (
                                                            <p className="noprog-txt" style={{ backgroundColor: "#fff" }}>{this.state.noProg}</p>
                                                        )}
                                                </div>
                                            </div>
                                        </React.Fragment>
                                    </React.Fragment>
                                ) : null}
                            </div>
                        </React.Fragment>
                    </React.Fragment>
                ) : (
                        <React.Fragment>
                            <div className="trial-balance-header-title" style={{ display: "flex" }}                            >
                                <KeyboardBackspaceSharpIcon title="go back" className="keyboard-back-icon"
                                    onClick={() => { this.setState({ ShowReconcileData: true, uploadPage: false, isDataUpload: false, }); }}
                                    style={{ marginRight: 5, cursor: "pointer", margin: 2 }} />
                                <p className="top-header-title" title="go back">| Statement</p>
                            </div>

                            {this.state.uploadPage && (
                                <React.Fragment>
                                    <div class="reconcileTabs">
                                        <div className="reconcile-upload reconcile-flex">
                                            <div style={{ width: "150px" }}>
                                                <div className="reconcile-flex" style={{ marginBottom: "5px" }}>
                                                    <p className="reconcile-publish"><PublishOutlinedIcon /></p>
                                                </div>
                                                <p className="reconcile-center" style={{ marginBottom: "5px", fontWeight: "600", fontSize: "12px", }}> Upload Bank Statement as PDF or XLS</p>
                                                <p className="reconcile-center" style={{ fontSize: "10px", marginBottom: "5px" }}>
                                                    <label for="upload" style={{ textDecoration: "underline", color: "rgb(43, 70, 187)", cursor: "pointer", }}>Browse File</label>
                                                </p>
                                                <input id="upload" type="file" multiple onChange={this.getUploadedFile} accept="application/pdf,.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" style={{ display: "none" }} />
                                            </div>
                                        </div>
                                    </div>
                                </React.Fragment>
                            )}
                            {this.state.isDataUpload && (
                                <React.Fragment>
                                    <div className="demand-note-container">
                                        <div className="Recon-header-btns" tooltip-title="New" style={{ marginLeft: "92.5%" }}>
                                            <button className="submit-btn-new" onClick={this.backToReconcile}>Submit</button>
                                        </div>
                                    </div>
                                    <div className="reconcile-uploaded-data-container">
                                        <div style={{ height: "10%", borderBottom: "1px solid #eee" }}>
                                            <p className="reconcile-clear-button" style={{ cursor: "pointer" }} onClick={() => { this.setState({ isDataUpload: false, uploadPage: true, }); }}>Clear</p>
                                        </div>
                                        <div style={{ height: "90%" }}>
                                            <div className="reconcile-pdf-container" style={{ overflow: "auto", height: "97%", width: "50%", margin: "0 auto", marginTop: "10px", }}     >
                                                <Document file={this.state.url} onLoadSuccess={this.onDocumentLoadSuccess} onLoadError={console.error} >
                                                    <Page pageNumber={this.state.pageNumber} />
                                                </Document>
                                                {/* <p>Page {this.state.pageNumber} of {this.state.numPages}</p> */}
                                            </div>
                                        </div>
                                    </div>
                                </React.Fragment>
                            )}
                        </React.Fragment>
                    )}

                {this.state.previewPayment ? (
                    <Modal className="reconcile-preview-payment-wrap" style={{ top: "5%", borderRadius: "2px" }} onHide={this.closeModal} size="xs" show={this.state.openModal}>
                        <Modal.Header>
                            <Modal.Title></Modal.Title>
                        </Modal.Header>
                        <Modal.Body className="bodyContent" style={{ paddingBottom: "10px", marginTop: "10px" }} >
                            <Payment
                                StudentRegId={this.state.StudentRegId}
                                StudentName={this.state.StudentName}
                                StudentClass={this.state.StudentClass}
                                paymentType={this.state.paymentType}
                                PaymentFormData={this.state.PaymentFormData}
                                onPaymentPreview={true}
                            />
                        </Modal.Body>
                    </Modal>
                ) : null}
            </React.Fragment>
        );
    }
}
export default withRouter(Reconcile);
