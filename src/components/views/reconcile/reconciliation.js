import React, { Component } from 'react'
import ZqTable from '../../../utils/Table/table-component'
import { withRouter } from 'react-router-dom'
import Axios from 'axios'
import PaginationUI from '../../../utils/pagination/pagination';
import ContainerNavbar from "../../../gigaLayout/container-navbar";
import { CircularProgress } from "@material-ui/core";
import StepConnector from '@material-ui/core/StepConnector';
import Textract from '../../dashboard/new-extract-pdf';
import Loader from '../../../utils/loader/loaders'
import { Alert, Modal, Icon, Steps } from 'rsuite';
import "../../../scss/reconcile.scss";
import '../../../scss/student.scss';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import { IconButton, ButtonGroup, ButtonToolbar, SelectPicker } from 'rsuite';
import SearchIcon from '@material-ui/icons/Search';
import GetAppIcon from '@material-ui/icons/GetApp';
import PrintIcon from '@material-ui/icons/Print';
import Tooltip from '@material-ui/core/Tooltip';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
// import { Alert, Modal } from 'rsuite';
import { withStyles } from "@material-ui/core/styles";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Button from '@material-ui/core/Button';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import PublishOutlinedIcon from '@material-ui/icons/PublishOutlined';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import ReconcilePreview from './reconcile';
import euroKidsData from './rawData.json';
// import { Document, Page, pdfjs } from 'react-pdf'
// pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const ZqTooltip = withStyles((theme) => ({
    tooltip: {
        backgroundColor: '#FFC501',
        color: 'black',
        boxShadow: theme.shadows[1],
        fontSize: 14,
        paddingTop: 6,
        paddingBottom: 6,
        paddingRight: 10,
        paddingLeft: 10,
        borderRadius: '0px'
    },
}))(Tooltip);
class Reconcile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            uploadPage: false,
            url: '',
            pageNumber: 1,
            numPages: null,
            reconcileID: '',
            activeStep: 0,
            steps: ['Upload statement', 'Process File', 'Review Data', 'Reconciliation'],
            containerNav: {
                "isBack": false,
                "name": "List of Reconciliation",
                "isName": true,
                "isSearch": false,
                "isSort": false,
                "isPrint": true,
                "isDownload": false,
                "isShare": false,
                "isNew": true,
                "newName": "New",
                "isSubmit": false
            },
            env: JSON.parse(localStorage.getItem('env')),
            email: localStorage.getItem('email'),
            authToken: localStorage.getItem('auth_token'),
            noOfTransactionItem: '',
            selectedItem: '',
            noProg: 'Fetching...',
            noOfItemList: 0,
            modalMessage: "Reminder send to Vendor",
            showAlert: false,
            page: 1,
            tab: 0,
            limit: 10,
            totalRecord: 1,
            isLoader: false,
            pageLoader: false,
            totalPages: 1,
            openModal: false,
            nextLoader: false,
            PassReconcile: false,
            CashReconcile: false,
            ShowReconcileData: true,
            addReconcile: false,
            previewReconcile: false,
            selectedReconcileItem: "BRS",
            defaultSelectValue: "BRS",
            PassbookCheckList: [],
            CashbookCheckList: [],
            allReconciledDatas: [],
            previewData: [],
            extractedData: [],
            DateDuration: [{ 'label': 'Today', 'value': 'today' }, { 'label': 'This Week', 'value': 'this week' }, { 'label': 'This Month', 'value': 'this month' }, { 'label': 'This Quarter', 'value': 'this quarter' }, { 'label': 'This Year', 'value': 'this year' }, { 'label': 'Custom', 'value': 'custom' }],
        }
    }

    componentDidMount() {
        this.onReconciliation();
    }

    onReconciliation = () => {
        this.setState({ pageLoader: true, allReconciledDatas: [] });
        let headers = {
            'Authorization': this.state.authToken
        };
        Axios.get(`${this.state.env['zqBaseUri']}/edu/reconcileList?page=${this.state.page}&perPage=${this.state.limit}`, { headers })
            .then(res => {
                let sampledata = []
                this.setState({ pageLoader: false });
                if (res.data.status != "failure") {
                    let data = res.data.data;
                    if (data.length > 0) {
                        data.map(item => {
                            sampledata.push({
                                "ID": item.id,
                                "Date": item.date != undefined ? this.dateFilter(item.date) : "-",
                                "% Reconciliation": item['percentage'],
                                "No. of Fees Transaction Reconciled": 1,
                                "Fees Amount Reconciled": item.feesAmount != undefined ? this.formatCurrency(item.feesAmount) : "-",
                                "No. of Refund Transaction Reconciled": 1,
                                "Refund Amount Reconciled": item.refundAmount != undefined ? this.formatCurrency(item.refundAmount) : "-",
                                "Status": item.status,
                                "Item": JSON.stringify(item.Item)
                            })
                        })
                        console.log("sampledata", sampledata)
                        this.setState({ ShowReconcileData: true, allReconciledDatas: sampledata })
                    } else {
                        this.setState({ noProg: "No Data" });
                    }
                } else {
                    this.setState({ noProg: "Failed to load response." });
                }
            }).catch(err => {
                this.setState({ pageLoader: false });
                this.setState({ noProg: "No Data" });
            })
    }

    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }

    dateFilter = (ev) => {
        var ts = new Date(ev);
        let getDate = `${String(ts.getDate()).length == 1 ? `0${ts.getDate()}` : ts.getDate()}`;
        let getMonth = `${String(ts.getMonth() + 1).length == 1 ? `0${ts.getMonth() + 1}` : ts.getMonth() + 1}`;
        let getYear = `${ts.getFullYear()}`
        let today = `${getDate}/${getMonth}/${getYear}`
        return today
    }

    onpreviewReconcile = (item) => {
        this.setState({ reconcileID: item.ID, ShowReconcileData: false, addReconcile: false, previewReconcile: true, previewData: item });
    }

    onRowCheckBox = (item) => {
        return null
    }

    onAddReconcile = () => {
        this.setState({ showStepper: true, activeStep: 0, ShowReconcileData: false, addReconcile: true, previewReconcile: false });
    }
    reviewDataClick = (activeStep) => {
        this.setState({ activeStep: activeStep })
    }
    checkBankStatement = (data) => {
        if (data.activeStep === 1) {
            this.setState({ activeStep: 1 });
        } else {
            this.extractedData(JSON.stringify(euroKidsData));
        }
    }
    showPDFView = () => {
        this.setState({ activeStep: this.state.activeStep + 1 })
    }
    finishReconcile = () => {
        this.setState({ ShowReconcileData: true, pageLoader: true }, () => {
            this.onReconciliation();
            this.setState({ pageLoader: false });
        }, 1000)
    }
    goBackMain = () => {
        this.setState({ ShowReconcileData: true });
    }

    extractedData = async (data) => {
        this.setState({ isLoader: true });
        var payload = data;
        console.log("payload data", payload)
        let fd = new FormData();
        const jsonData = new Blob([payload], { type: "application/json" })
        fd.append('file', jsonData)
        let headers = {
            'Authorization': this.state.authToken
        };
        const res = await Axios.post(`${this.state.env['zqBaseUri']}/edu/systemReconciliation`, fd, { headers })
            .then(res => {
                console.log(res)
                setTimeout(() => {
                    Alert.success(res.data.message)
                    this.setState({ isLoader: false });
                    this.setState({ extractedData: res.data.data });
                    this.reviewDataClick(3);
                }, 1000);
            }).catch(err => {
                Alert.error("Failed to Reconcile. Please Try Again.")
                this.reviewDataClick(1);
                this.setState({ isLoader: false });
            })
    }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.onReconciliation();
        });
        console.log(page, limit);
    };

    getStepContent = (stepIndex) => {
        console.log('step', stepIndex)
        switch (stepIndex) {
            case 0:
            case 1:
                return <React.Fragment>
                    <div className="receive-payment-main">
                        {this.state.circularLoader ?
                            <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                                <CircularProgress size={24} />
                            </div> : null}
                        <div className="receive-amount-table">
                            <React.Fragment >
                                <Textract reviewData={(activeStep) => this.reviewDataClick(activeStep)} extractedData={this.extractedData} checkBankStatement={this.checkBankStatement} showPDFView={this.showPDFView} />
                            </React.Fragment>
                        </div>
                    </div>
                </React.Fragment>
            case 2:
                return <React.Fragment>
                    <div className="receive-payment-main">
                        {this.state.circularLoader ?
                            <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                                <CircularProgress size={24} />
                            </div> : null}
                        <div className="receive-amount-table">
                            <React.Fragment >
                                <Textract reviewData={(activeStep) => this.reviewDataClick(activeStep)} extractedData={this.extractedData} checkBankStatement={this.checkBankStatement} showPDFView={this.showPDFView} />
                            </React.Fragment>
                        </div>
                    </div>
                </React.Fragment>
            case 3:
                return <ReconcilePreview finishReconcile={this.finishReconcile} goBackMain={this.goBackMain} previewData={this.state.extractedData} />
            default:
                return '***';
        }
    }
    render() {
        const ColorlibConnector = withStyles({
            alternativeLabel: { top: 22 },
            active: { '& $line': { backgroundColor: "#1359C1", marginTop: '-10px', transitionDelay: '2s' }, },
            completed: { '& $line': { backgroundColor: '#36B37E', marginTop: '-10px' }, },
            line: { height: 2, border: 0, backgroundColor: '#ccc', borderRadius: 1, marginTop: '-10px' },
        })(StepConnector);
        return (
            <React.Fragment>
                {this.state.pageLoader == true ? <Loader /> : null}
                {this.state.isLoader ?
                    <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                        <CircularProgress size={24} />
                    </div> : null}
                {this.state.ShowReconcileData ?
                    <div className="list-of-students-mainDiv">

                        <React.Fragment>
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Reconciliation</p>

                            </div>
                            <div className="masters-body-div">
                                {this.state.containerNav == undefined ? null :
                                    <React.Fragment>
                                        <ContainerNavbar containerNav={this.state.containerNav} onAddNew={() => { this.onAddReconcile(); }} />
                                        <div className="print-time">{this.state.printTime}</div>
                                    </React.Fragment>
                                }
                                <div className="remove-last-child-table remove-first-child-table">
                                    {this.state.allReconciledDatas.length !== 0 ?
                                        < ZqTable
                                            allSelect={this.allSelectData}
                                            data={this.state.allReconciledDatas}
                                            rowClick={(item) => { this.onpreviewReconcile(item) }}
                                            onRowCheckBox={(item) => { this.onRowCheckBox(item) }} />
                                        :
                                        <p className="noprog-txt">{this.state.noProg}</p>
                                    }
                                </div>
                                <div>
                                    {this.state.allReconciledDatas.length !== 0 ?
                                        <PaginationUI
                                            total={this.state.totalRecord}
                                            onPaginationApi={this.onPaginationChange}
                                            totalPages={this.state.totalPages}
                                            limit={this.state.limit}
                                            currentPage={this.state.page} />
                                        :
                                        null
                                    }
                                </div>
                            </div>
                        </React.Fragment>
                    </div>
                    : (this.state.addReconcile ?
                        <React.Fragment>
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.setState({ ShowReconcileData: true }) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title" onClick={() => { this.setState({ ShowReconcileData: true }) }}>| Reconciliation </p>
                            </div>
                            <React.Fragment>
                                <div className="migration-main-div portal-login-div vendor-invoice-upload-wrap pdf-extraction-upload-wrap statement-reconcile" style={{ height: "calc(100vh - 110px)" }}>
                                    <div className="migration-header-stepper-section">
                                        <div className="zenqore-stepper-section">
                                            <Steps current={this.state.activeStep} currentStatus="process" vertical={false} >
                                                <Steps.Item title="Upload Statement" style={{ cursor: "pointer" }} />
                                                <Steps.Item title="Process File" style={{ cursor: "pointer" }} />
                                                <Steps.Item title="Review Data" style={{ cursor: "pointer" }} />
                                                <Steps.Item title="Reconciliation" style={{ cursor: "initial" }} />
                                            </Steps>
                                        </div>
                                    </div>
                                    <div className="migration-body-content-section" style={{ backgroundColor: "#fff", height: "calc(100vh - 177px)", overflowY: "auto", border: "1px solid #dfe1e6", marginTop: "20px" }}>
                                        {this.getStepContent(this.state.activeStep)}
                                    </div>
                                </div>
                            </React.Fragment>

                        </React.Fragment>
                        : this.state.previewReconcile ?
                            <React.Fragment>
                                <div className="trial-balance-header-title">
                                    <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.setState({ ShowReconcileData: true }) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                    <p className="top-header-title" onClick={() => { this.setState({ ShowReconcileData: true, }) }}>| Reconciliation | {this.state.reconcileID}  </p>
                                </div>
                                <ReconcilePreview finishReconcile={this.finishReconcile} goBackMain={this.goBackMain} previewData={this.state.previewData} />
                            </React.Fragment>
                            : null
                    )}
            </React.Fragment>
        )
    }
}
export default withRouter(Reconcile)
