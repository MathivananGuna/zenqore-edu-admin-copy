import React from 'react';
import { withRouter } from 'react-router-dom';
import '../../../../../scss/setup.scss';
import '../../../../../scss/student.scss';
import PreviewCampusJson from './preview-campus-data.json';
import FranchisJson from './add-campuses-data.json';
import Loader from "../../../../../utils/loader/loaders";
import ContainerNavbar from "../../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../../utils/Table/table-component";
import ZenTabs from '../../../../../components/input/tabs';
import PaginationUI from "../../../../../utils/pagination/pagination";
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp'
import axios from "axios";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import DateFormatContext from '../../../../../gigaLayout/context';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class Campuses extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            campusName: "",
            tableViewMode: "",
            newCampusJson: FranchisJson.AddNewCampusJson,
            previewCampusJson: PreviewCampusJson,
            containerNav: {
                isBack: false,
                name: "List of Franchises",
                isName: true,
                isSearch: false,
                isSort: false,
                isPrint: true,
                isDownload: false,
                isShare: false,
                isNew: false,
                newName: "New",
                isSubmit: false,
            },
            LoaderStatus: false,
            tabViewForm: false,
            viewFormLoad: true,
            viewHeader: true,
            sampleCampusesData: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 0,
            payloadId: "",
            snackbar: { openNotification: false, NotificationMessage: "", status: "success" },
            logoPicture: "",
            tableResTxt: "Fetching Data...",
            newDisplayName: "",
            logoPictureNew: "",
        }
    }
    static contextType = DateFormatContext;
    componentDidMount() {
        this.getTableData() // get all franchises data
    }
    getTableData = () => {
        this.setState({ sampleCampusesData: [] }, () => {
            let sampleCampusesData = []
            let headers = {
                'Authorization': this.state.authToken
            };
            axios.get(`${this.state.env["zqBaseUri"]}/edu/franchises?orgId=${this.state.orgId}&page=${this.state.page}&limit=${this.state.limit}`, { headers })
                .then(res => {
                    console.log(res);
                    if (res.data.data.length === 0) {
                        this.setState({ sampleCampusesData: [], totalRecord: res.data.totalRecord, totalPages: res.data.totalPages, page: res.data.currentPage, tableResTxt: "No Data" })
                    }
                    else {
                        res.data.data.map((data, i) => {
                            sampleCampusesData.push({
                                "Id": data.displayName,
                                "Franchise Name": data.franchiseName,
                                "GSTIN": data.financialDetails.GSTIN.toLocaleUpperCase(),
                                "PAN": data.financialDetails.PAN.toLocaleUpperCase(),
                                "Email": data.franchiseContact.emailAddress,
                                "Phone Number": data.franchiseContact.phoneNumber,
                                "State": data.legalAddress.state,
                                "Status": data.status,
                                "Item": JSON.stringify(data)
                            })
                        })
                        this.setState({ sampleCampusesData: sampleCampusesData, totalRecord: res.data.totalRecord, totalPages: res.data.totalPages, page: res.data.currentPage, tableResTxt: "" })
                    }
                })
                .catch(err => { console.log(err) })
        })
    }
    onInputChanges = (value, item, event, dataS) => { }
    onAddBranches = () => {
        let addNewCampusFormData = FranchisJson.AddNewCampusJson;
        this.setState({ newCampusJson: addNewCampusFormData, tabViewForm: true, campusName: "New Franchises", tableViewMode: "addnew", tableResTxt: "Fetching Data...", viewHeader: true, viewFormLoad: true }, () => {
            axios({
                method: 'get',
                url: `${this.state.env["zqBaseUri"]}/edu/franchisesDisplayName?orgId=${this.state.orgId}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            }).then(res => {
                console.log(res);
                this.resetform();
                Object.keys(addNewCampusFormData).forEach(tab => {
                    addNewCampusFormData[tab].forEach(task => {
                        if (task['name'] == 'franchisesId') {
                            task['defaultValue'] = String(res.data.displayName)
                            task['readOnly'] = true
                        }
                    })
                })
                this.setState({ newCampusJson: addNewCampusFormData, viewFormLoad: false, campusName: `New Franchises | ${res.data.displayName}`, newDisplayName: res.data.displayName })
            }).catch(error => {
                this.setState({ tableResTxt: "Error loading Data" })
            })
        })
    }
    onPreviewStudentList = (e) => {
        let details = JSON.parse(e.Item)
        console.log("parsedItem", details)
        let previewCampusFormData = FranchisJson.PreviewCampusJson;
        this.setState({ tabViewForm: true, viewHeader: true, viewFormLoad: true, tableViewMode: "preview", payloadId: e.Id }, () => {
            Object.keys(previewCampusFormData).forEach(tab => {
                previewCampusFormData[tab].forEach(task => {
                    // GENERAL
                    if (task['name'] == 'franchiseId') {
                        task['defaultValue'] = details.displayName
                        task['readOnly'] = true
                        task['required'] = false
                    }
                    else if (task['name'] == 'franchiseName') {
                        task['defaultValue'] = details.franchiseName
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'dateOfRegistration') {
                        task['defaultValue'] = details.dateOfRegistration
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'address') {
                        task['defaultValue'] = details.legalAddress.address1 + details.legalAddress.address2 + details.legalAddress.address3
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'cityTown') {
                        task['defaultValue'] = details.legalAddress.city
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'stateName') {
                        task['defaultValue'] = details.legalAddress.state
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'country') {
                        task['defaultValue'] = details.legalAddress.country
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'pincode') {
                        task['defaultValue'] = details.legalAddress.pincode
                        task['readOnly'] = false
                        task['required'] = false
                    }

                    //FINANCIAL DETAILS
                    else if (task['name'] == 'gstin') {
                        task['defaultValue'] = details.financialDetails.GSTIN
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'pan') {
                        task['defaultValue'] = details.financialDetails.PAN
                        task['readOnly'] = false
                        task['required'] = false
                    }

                    //BANK DETAILS
                    else if (task['name'] == 'bankName') {
                        task['defaultValue'] = details.bankDetails.bankName
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'bankAccountName') {
                        task['defaultValue'] = details.bankDetails.bankAccountName
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'bankAccountNumber') {
                        task['defaultValue'] = details.bankDetails.bankAccountNumber
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'bankIFSC') {
                        task['defaultValue'] = details.bankDetails.bankIFSC
                        task['readOnly'] = false
                        task['required'] = false
                    }

                    //CONTACT DETAILS
                    else if (task['name'] == 'contactName') {
                        task['defaultValue'] = details.franchiseContact.contactname
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'designation') {
                        task['defaultValue'] = details.franchiseContact.designation
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'mobileNumber') {
                        task['defaultValue'] = details.franchiseContact.mobileNumber
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'phoneNumber') {
                        task['defaultValue'] = details.franchiseContact.phoneNumber
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'emailAddress') {
                        task['defaultValue'] = details.franchiseContact.emailAddress
                        task['readOnly'] = false
                        task['required'] = false
                    }

                    //HEAD APPROVER
                    else if (task['name'] == 'approverName') {
                        task['defaultValue'] = details.headApprover.headApproverName
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'approverDesignation') {
                        task['defaultValue'] = details.headApprover.designation
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'approverEmailAddress') {
                        task['defaultValue'] = details.headApprover.emailAddress
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'approverMobileNumber') {
                        task['defaultValue'] = ""
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'approverPhoneNumber') {
                        task['defaultValue'] = details.headApprover.phoneNumber
                        task['readOnly'] = false
                        task['required'] = false
                    }

                    // ROYALITY
                    else if (task['name'] == 'royaltyPercentage') {
                        task['defaultValue'] = details.royalty === undefined ? "-" : details.royalty.royaltyPercentage
                        task['readOnly'] = false
                        task['required'] = false
                    }

                    //LOGO
                    else if (task['name'] == 'logoPicture') {
                        task['url'] = details.logo.logoData
                        task['readOnly'] = false
                        task['required'] = false
                    }
                })
            })
            this.setState({ newCampusJson: previewCampusFormData, viewFormLoad: false, campusName: `Franchise | ${e['Id']}` })
        })
    }
    handleTabChange = () => { }
    onFormSubmit = (data, item) => {
        this.setState({ LoaderStatus: true })
        //GETTING THE VALUES FROM TEXTBOXES
        let alltextBoxValues = {}
        let generalData = data['GENERAL']
        let financeDetailsData = data['FINANCIAL DETAILS']
        let bankDetailsData = data['BANK DETAILS']
        let contactDetailsData = data['CONTACT DETAILS']
        let headApproverData = data['HEAD APPROVER']
        let campusLogoData = data['Logo']
        let royalityData = data['ROYALTY']
        royalityData.map((item) => {
            if (item.name == "royaltyPercentage") {
                alltextBoxValues['royalityPercentage'] = item['defaultValue'] == "" ? "-" : item['defaultValue'];
            }
        })
        generalData.map(item => {
            if (item.name == "franchise Id") {
                alltextBoxValues['campusId'] = item['defaultValue'];
            }
            if (item.name == 'campusName') {
                alltextBoxValues['campusName'] = item['defaultValue'];
            }
            if (item.name == 'dateOfRegistration') {
                alltextBoxValues['dateOfRegistration'] = item['defaultValue'];
            }
            if (item.name == 'address') {
                alltextBoxValues['address'] = item['defaultValue'];
            }
            if (item.name == "stateName") {
                alltextBoxValues['stateName'] = item['defaultValue'];
            }
            if (item.name == 'cityTown') {
                alltextBoxValues['cityTown'] = item['defaultValue'];
            }
            if (item.name == 'country') {
                alltextBoxValues['country'] = item['defaultValue'];
            }
            if (item.name == 'pincode') {
                alltextBoxValues['pincode'] = item['defaultValue'];
            }
        })

        financeDetailsData.map(item => {
            if (item.name == "gstin") {
                alltextBoxValues['gstin'] = item['defaultValue'];
            }
            if (item.name == 'pan') {
                alltextBoxValues['pan'] = item['defaultValue'];
            }
        })

        bankDetailsData.map(item => {
            if (item.name == "bankName") {
                alltextBoxValues['bankName'] = item['defaultValue'];
            }
            if (item.name == 'bankAccountName') {
                alltextBoxValues['bankAccountName'] = item['defaultValue'];
            }
            if (item.name == "bankAccountNumber") {
                alltextBoxValues['bankAccountNumber'] = item['defaultValue'];
            }
            if (item.name == 'bankIFSC') {
                alltextBoxValues['bankIFSC'] = item['defaultValue'];
            }
        })


        contactDetailsData.map(item => {
            if (item.name == "contactName") {
                alltextBoxValues['contactName'] = item['defaultValue'];
            }
            if (item.name == 'designation') {
                alltextBoxValues['designation'] = item['defaultValue'];
            }
            if (item.name == "department") {
                alltextBoxValues['department'] = item['defaultValue'];
            }
            if (item.name == 'emailAddress') {
                alltextBoxValues['emailAddress'] = item['defaultValue'];
            }
            if (item.name == "mobileNumber") {
                alltextBoxValues['mobileNumber'] = item['defaultValue'];
            }
            if (item.name == 'phoneNumber') {
                alltextBoxValues['phoneNumber'] = item['defaultValue'];
            }
        })

        headApproverData.map(item => {
            if (item.name == "approverName") {
                alltextBoxValues['approverName'] = item['defaultValue'];
            }
            if (item.name == 'approverDesignation') {
                alltextBoxValues['approverDesignation'] = item['defaultValue'];
            }
            if (item.name == "approverEmailAddress") {
                alltextBoxValues['approverEmailAddress'] = item['defaultValue'];
            }
            if (item.name == 'approverMobileNumber') {
                alltextBoxValues['approverMobileNumber'] = item['defaultValue'];
            }
            if (item.name == "approverPhoneNumber") {
                alltextBoxValues['approverPhoneNumber'] = item['defaultValue'];
            }
        })

        //SETTING THE PAYLOAD
        let payload = {
            "legalAddress": {
                "address1": alltextBoxValues["address"] || "-",
                "address2": "-",
                "address3": "-",
                "city": alltextBoxValues["cityTown"] || "-",
                "state": alltextBoxValues["stateName"] || "-",
                "country": alltextBoxValues["country"] || "-",
                "pincode": Number(alltextBoxValues["pincode"]) || "-",
            },
            "financialDetails": {
                "GSTIN": alltextBoxValues["gstin"] || "-",
                "PAN": alltextBoxValues["pan"] || "-"
            },
            "royalty": {
                "royaltyPercentage": alltextBoxValues['royalityPercentage']
            },
            "logo": {
                "logoData": this.state.logoPictureNew
            },
            "organizationType": "Trust",
            "dateOfRegistration": alltextBoxValues["dateOfRegistration"] || "-",
            "bankDetails":
            {
                "bankName": alltextBoxValues["bankName"] || "-",
                "bankAccountName": alltextBoxValues["bankAccountName"] || "-",
                "bankAccountNumber": alltextBoxValues["bankAccountNumber"] || "-",
                "bankIFSC": alltextBoxValues["bankIFSC"] || "-",
                "status": "Active"
            },
            "franchiseContact":
            {
                "contactname": alltextBoxValues["contactName"] || "-",
                "designation": alltextBoxValues["designation"] || "-",
                "emailAddress": alltextBoxValues["emailAddress"] || "-",
                "phoneNumber": alltextBoxValues["phoneNumber"] || "-",
                "mobileNumber": alltextBoxValues["mobileNumber"] || "-"
            },
            "headApprover":
            {
                "headApproverName": alltextBoxValues["approverName"] || "-",
                "designation": alltextBoxValues["approverDesignation"] || "-",
                "emailAddress": alltextBoxValues["approverEmailAddress"] || "-",
                "phoneNumber": alltextBoxValues["approverPhoneNumber"] || "-"
            },
            "franchiseName": alltextBoxValues["campusName"] || "-",
            "displayName": this.state.newDisplayName
        }

        console.log("***Payload franchises***", payload);

        // CREATING NEW Franchise
        if (this.state.tableViewMode === 'addnew') {
            let headers = {
                'Authorization': this.state.authToken
            };
            let bodyData = payload;
            axios.post(`${this.state.env["zqBaseUri"]}/edu/franchises?orgId=${this.state.orgId}`, bodyData, { headers })
                .then(res => {
                    console.log(res);
                    if (res.data.status == true) {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Added successfully", status: "success" }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate, tabViewForm: false });
                        this.resetform();
                        this.refreshGetData()
                    }
                    else if (res.data.status == false) {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: res.message, status: "error" }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate, tabViewForm: false });
                        this.resetform();
                        this.refreshGetData()
                    }
                })
                .catch(err => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Error", status: "error" }
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate, tabViewForm: false });
                })
        }

        // //UPDATING CAMPUS
        // if (this.state.tableViewMode === 'preview') {
        //     let headers = {
        //         'Authorization': this.state.authToken
        //     };
        //     let bodyData = payload;
        //     // console.log("Preview payload", bodyData)
        //     axios.put(` ${this.state.env["zqBaseUri"]}/edu/campus/${this.state.payloadId}?orgId=${this.state.orgId}`, bodyData, { headers })
        //         .then(res => {
        //             let snackbarUpdate = { openNotification: true, NotificationMessage: "Updated Successfully", status: "success" }
        //             this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
        //             this.resetform();
        //             this.getTableData();
        //         })
        //         .catch(err => {
        //             let snackbarUpdate = { openNotification: true, NotificationMessage: "Error", status: "error" }
        //             this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
        //             this.getTableData();
        //         })
        // }
    }
    refreshGetData = () => {
        setTimeout(() => { 
            let snackbarUpdate = { openNotification: false, NotificationMessage: "", status: "" }
            this.setState({snackbar: snackbarUpdate})
            this.getTableData();
         }, 1000)
    }
    selectRowNew = () => { }
    allSelect = () => { }
    printScreen = () => { }
    moveBackTable = () => {
        this.setState({ tabViewForm: false })
        this.resetform();
        this.getTableData() // get all franchises data
    }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.getTableData()
        });
    };
    closeNotification = () => {
        let snackbarUpdate = { openNotification: false, NotificationMessage: "", status: "" }
        this.setState({ snackbar: snackbarUpdate })
    }
    resetform = () => {
        let campusFormData = FranchisJson.AddNewCampusJson;
        // let campusFormData1 = InstallmentJson.AddstudentsFormjson;
        Object.keys(campusFormData).forEach(tab => {
            campusFormData[tab].forEach(task => {
                delete task['defaultValue']
                task['readOnly'] = false
                task['error'] = false
                task['required'] = task['requiredBoolean']
                task['validation'] = false
                delete task['clear']
            })
        })
        this.setState({ FranchisJson: campusFormData, payloadId: "" })
    }
    formBtnHandle = () => {

    }
    cleanDatas = () => { }
    cancelViewData = () => {
        this.setState({ tabViewForm: false });
        this.resetform();
    }

    //Logo Handling functionalities
    handleLogo = (image) => {
        this.setState({ logoPictureNew: image })
        this.setState({ logoPicture: image })
    }
    getLogoData = (e) => { }


    render() {
        return (
            <div className="list-of-students-mainDiv">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.tabViewForm == false ?
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} >| Franchises</p>
                        </div>
                        <div className="masters-body-div">
                            {this.state.containerNav == undefined ? null :
                                <React.Fragment>
                                    <ContainerNavbar containerNav={this.state.containerNav} onAddNew={() => { this.onAddBranches() }} searchValue={(searchValue) => this.searchHandle(searchValue)} onDownload={this.onDownload} printScreen={this.printScreen} />
                                    <div className="print-time">{this.state.printTime}</div>
                                </React.Fragment>}
                            <div className="remove-last-child-table">
                                {this.state.sampleCampusesData.length !== 0 ?
                                    <ZqTable
                                        allSelect={this.allSelect}
                                        data={this.state.sampleCampusesData}
                                        rowClick={(item) => { this.onPreviewStudentList(item) }}
                                        onRowCheckBox={(item) => { this.onPreviewStudentList1(item) }}
                                    />
                                    : <p className="noprog-txt">{this.state.tableResTxt}</p>
                                }
                            </div>
                            <div>
                                {this.state.sampleCampusesData.length !== 0 ?
                                    <PaginationUI
                                        total={this.state.totalRecord}
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        currentPage={this.state.page}
                                    />
                                    : null}
                            </div>
                        </div>
                    </React.Fragment> :
                    <React.Fragment>
                        <div className="tab-form-wrapper tab-table remainderPlan">
                            <div className="preview-btns">
                                {this.state.viewHeader == true ?
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| {this.state.campusName}</h6>
                                    </div> :
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| {this.state.campusName}</h6>
                                    </div>
                                }
                            </div>
                            <div className="organisation-table">
                                {this.state.viewFormLoad === false ?
                                    <ZenTabs tabData={this.state.newCampusJson} className="preview-wrap" tabEdit={this.state.preview} form={this.state.newCampusJson} cleanData={this.cleanDatas} cancelViewData={this.cancelViewData} value={0} onInputChanges={this.onInputChanges} onFormBtnEvent={(item) => { this.formBtnHandle(item); }} onTabFormSubmit={this.onFormSubmit} handleTabChange={this.handleTabChange} key={0}
                                        logoPicture={this.state.logoPicture} handleLogo={this.handleLogo} getLogoData={(e) => { this.getLogoData(e) }} />
                                    : <p className="noprog-txt">{this.state.tableResTxt}</p>}
                            </div>
                        </div>
                    </React.Fragment>
                }
                <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={2000} onClose={this.closeNotification}>
                    <Alert onClose={this.closeNotification}
                        severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
                        {this.state.snackbar.NotificationMessage}
                    </Alert>
                </Snackbar>
            </div>
        )
    }
}

export default withRouter(Campuses);