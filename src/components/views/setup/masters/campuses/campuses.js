import React from 'react';
import { withRouter } from 'react-router-dom';
import '../../../../../scss/setup.scss';
import '../../../../../scss/student.scss';
import PreviewCampusJson from './preview-campus-data.json';
import CampusJson from './add-campuses-data.json';
import Loader from "../../../../../utils/loader/loaders";
import ContainerNavbar from "../../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../../utils/Table/table-component";
import ZenTabs from '../../../../../components/input/tabs';
import PaginationUI from "../../../../../utils/pagination/pagination";
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import RestoreOutlinedIcon from '@material-ui/icons/RestoreOutlined';
import ZenForm from "../../../../../components/input/zqform";
import axios from "axios";
import moment from 'moment';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import DateFormatContext from '../../../../../gigaLayout/context';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class Campuses extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            campusId: localStorage.getItem('campusId'),
            campusName: "",
            tableViewMode: "",
            newCampusJson: CampusJson.AddNewCampusJson,
            previewCampusJson: PreviewCampusJson,
            containerNav: {
                isBack: false,
                name: "List of Campuses",
                isName: true,
                isSearch: false,
                isSort: false,
                isPrint: true,
                isDownload: false,
                isShare: false,
                isNew: true,
                newName: "New",
                isSubmit: false,
            },
            LoaderStatus: false,
            tabViewForm: false,
            viewFormLoad: true,
            viewHeader: true,
            sampleCampusesData: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 0,
            payloadId: "",
            snackbar: { openNotification: false, NotificationMessage: "", status: "success" },

            //Logo
            logoPicture: "",

            //error
            tableResTxt: "Fetching Data..."
        }
    }
    static contextType = DateFormatContext;
    componentDidMount() {
        this.getTableData()
        this.getFranchisesData()
    }
    getFranchisesData = () => {
        let headers = {
            'Authorization': this.state.authToken
        };
        axios.get(`${this.state.env["zqBaseUri"]}/edu/franchises?orgId=6048cdccb82a7d0dd4683df3`, { headers })
            .then(res => {
                let formData = []
                res.data.data.map((data) => {
                    formData.push({
                        label: data.franchiseName,
                        value: data.displayName
                    })
                })
                console.log(res);
                let previewCampusFormData = CampusJson.PreviewCampusJson;
                Object.keys(previewCampusFormData).forEach(tab => {
                    previewCampusFormData[tab].forEach(task => {
                        if (task['name'] == 'franchsisName') {
                            task['options'] = formData
                            task['readOnly'] = false
                            task['required'] = false
                        }
                    })
                })
                this.setState({ newCampusJson: previewCampusFormData })
            })
            .catch(err => { console.log(err) })
    }
    getTableData = () => {
        // let sampleCampusesData = []
        // axios({
        //     method: 'get',
        //     url: `${this.state.env["zqBaseUri"]}/edu/campus?orgId=${this.state.orgId}&page=${this.state.page}&limit=${this.state.limit}&campusId=${this.state.campusId}`,
        //     headers: {
        //         'Authorization': this.state.authToken
        //     }
        // }).then(res => {
        //     console.log(res);
        //     let status = 1
        //     if (res.data.status == "success") {
        //         if (res.data.data.length == 0) {
        //             this.setState({ tableResTxt: "No Data" })
        //         }
        //         else {
        //             this.setState({ sampleCampusesData: [], tableResTxt: "Fetching Data..." })
        //             res.data.data.map((data, i) => {
        //                 sampleCampusesData.push({
        //                     "Campus Id": data.campusId,
        //                     "Legal Name": data.legalName,
        //                     "Date Of Registration": data.dateOfRegistration,
        //                     "Created On": moment(data.createdAt).format(this.context.dateFormat),
        //                     "Owned by": "-",
        //                     "Status": status == 1 ? "Active" : "Inactive",
        //                     "Item": JSON.stringify(data)
        //                 })
        //             })
        //             sampleCampusesData.reverse() //reversing the data
        //             this.setState({ sampleCampusesData:  CampusJson.CampusTableData, tableResTxt: "", tabViewForm: false, totalRecord: res.data.totalRecord, totalPages: res.data.totalPages, page: res.data.page, downloadTotalRecord: res.data.totalRecord })
        //         }
        //     }
        //     else {
        //         this.setState({ tableResTxt: "Error loading Data" })
        //     }
        // }).catch(error => {
        //     this.setState({ tableResTxt: "Error loading Data" })
        // })

        this.setState({ sampleCampusesData: CampusJson.CampusTableData   })
    }
    onInputChanges = (value, item, event, dataS) => { }
    onAddBranches = () => {
        let addNewCampusFormData = CampusJson.AddNewCampusJson;
        this.setState({ newCampusJson: addNewCampusFormData, tabViewForm: true, campusName: "New Campus", tableViewMode: "addnew", tableResTxt: "Fetching Data...", viewHeader: true, viewFormLoad: true }, () => {
            axios({
                method: 'get',
                url: `${this.state.env["zqBaseUri"]}/edu/campusdisplayName?orgId=${this.state.orgId}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            }).then(res => {
                this.resetform();
                Object.keys(addNewCampusFormData).forEach(tab => {
                    addNewCampusFormData[tab].forEach(task => {
                        if (task['name'] == 'campusId') {
                            task['defaultValue'] = String(res.data.campusId)
                            task['readOnly'] = true
                        }
                    })
                })
                this.setState({ newCampusJson: addNewCampusFormData, viewFormLoad: false })
            }).catch(error => {
                this.setState({ tableResTxt: "Error loading Data" })
            })
        })
    }
    onPreviewStudentList = (e) => {
        let details = JSON.parse(e.Item)
        console.log("parsedItem", details)
        let previewCampusFormData = CampusJson.PreviewCampusJson;
        this.setState({ tabViewForm: true, viewHeader: true, viewFormLoad: true, tableViewMode: "preview", payloadId: details["_id"] }, () => {
            Object.keys(previewCampusFormData).forEach(tab => {
                previewCampusFormData[tab].forEach(task => {
                    if (task['name'] == 'campusId') {
                        task['defaultValue'] = e['Campus Id']
                        task['readOnly'] = true
                        task['required'] = false
                    }
                    else if (task['name'] == 'campusName') {
                        task['defaultValue'] = e['Legal Name']
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'dateOfRegistration') {
                        task['defaultValue'] = e['Date Of Registration']
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'address') {
                        task['defaultValue'] = details['legalAddress']['address1'] + details['legalAddress']['address2'] + details['legalAddress']['address3'] || '-'
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'stateName') {
                        task['defaultValue'] = details['legalAddress']['state']
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'cityTown') {
                        task['defaultValue'] = details['legalAddress']['city']
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'country') {
                        task['defaultValue'] = details['legalAddress']['country']
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'pincode') {
                        task['defaultValue'] = details['legalAddress']['pincode']
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    //FINANCIAL DETAILS
                    else if (task['name'] == 'gstin') {
                        task['defaultValue'] = details['financialDetails']['GSTIN'] || "-"
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'pan') {
                        task['defaultValue'] = details['financialDetails']['PAN'] || "-"
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    //BANK DETAILS
                    // if (details['bankDetails'].length !== 0) {
                    //     details['bankDetails'].map(item => {

                    //     })
                    // }
                    else if (task['name'] == 'bankName') {
                        task['defaultValue'] = details['bankDetails'][0]['bankName'] || "-"
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'bankAccountName') {
                        task['defaultValue'] = details['bankDetails'][0]['bankAccountName'] || "-"
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'bankAccountNumber') {
                        task['defaultValue'] = details['bankDetails'][0]['bankAccountNumber'] || "-"
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'bankIFSC') {
                        task['defaultValue'] = details['bankDetails'][0]['bankIFSC'] || "-"
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    //INSTITUTE CONTACT DETAILS
                    else if (task['name'] == 'contactName') {
                        task['defaultValue'] = details['instituteContact'][0]['contactname'] || "-"
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'designation') {
                        task['defaultValue'] = details['instituteContact'][0]['designation'] || "-"
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'department') {
                        task['defaultValue'] = details['instituteContact'][0]['department'] || "-"
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'mobileNumber') {
                        task['defaultValue'] = details['instituteContact'][0]['mobileNumber'] || "-"
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'phoneNumber') {
                        task['defaultValue'] = details['instituteContact'][0]['phoneNumber'] || "-"
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'emailAddress') {
                        task['defaultValue'] = details['instituteContact'][0]['emailAddress'] || "-"
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    //HEAD APPROVER
                    else if (task['name'] == 'approverName') {
                        task['defaultValue'] = details['headApprover'][0]['headApproverName'] || "-"
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'approverDesignation') {
                        task['defaultValue'] = details['headApprover'][0]['designation'] || "-"
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'approverEmailAddress') {
                        task['defaultValue'] = details['headApprover'][0]['emailAddress'] || "-"
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'approverMobileNumber') {
                        task['defaultValue'] = details['headApprover'][0]['mobileNumber'] || "-"
                        task['readOnly'] = false
                        task['required'] = false
                    }
                    else if (task['name'] == 'approverPhoneNumber') {
                        task['defaultValue'] = details['headApprover'][0]['phoneNumber'] || "-"
                        task['readOnly'] = false
                        task['required'] = false
                    }
                })
            })
            this.setState({ newCampusJson: previewCampusFormData, viewFormLoad: false, campusName: `Campus | ${e['Campus Id']}` })
        })
    }
    handleTabChange = () => { }
    onFormSubmit = (data, item) => {
        this.setState({ LoaderStatus: true })

        //GETTING THE VALUES FROM TEXTBOXES
        let alltextBoxValues = {}
        let generalData = data['GENERAL']
        let financeDetailsData = data['FINANCIAL DETAILS']
        let bankDetailsData = data['BANK DETAILS']
        let contactDetailsData = data['INSTITUTE CONTACT DETAILS']
        let headApproverData = data['HEAD APPROVER']
        let campusLogoData = data['Logo']

        generalData.map(item => {
            if (item.name == "campusId") {
                alltextBoxValues['campusId'] = item['defaultValue'];
            }
            if (item.name == 'campusName') {
                alltextBoxValues['campusName'] = item['defaultValue'];
            }
            if (item.name == 'dateOfRegistration') {
                alltextBoxValues['dateOfRegistration'] = item['defaultValue'];
            }
            if (item.name == 'address') {
                alltextBoxValues['address'] = item['defaultValue'];
            }
            if (item.name == "stateName") {
                alltextBoxValues['stateName'] = item['defaultValue'];
            }
            if (item.name == 'cityTown') {
                alltextBoxValues['cityTown'] = item['defaultValue'];
            }
            if (item.name == 'country') {
                alltextBoxValues['country'] = item['defaultValue'];
            }
            if (item.name == 'pincode') {
                alltextBoxValues['pincode'] = item['defaultValue'];
            }
        })

        financeDetailsData.map(item => {
            if (item.name == "gstin") {
                alltextBoxValues['gstin'] = item['defaultValue'];
            }
            if (item.name == 'pan') {
                alltextBoxValues['pan'] = item['defaultValue'];
            }
        })

        bankDetailsData.map(item => {
            if (item.name == "bankName") {
                alltextBoxValues['bankName'] = item['defaultValue'];
            }
            if (item.name == 'bankAccountName') {
                alltextBoxValues['bankAccountName'] = item['defaultValue'];
            }
            if (item.name == "bankAccountNumber") {
                alltextBoxValues['bankAccountNumber'] = item['defaultValue'];
            }
            if (item.name == 'bankIFSC') {
                alltextBoxValues['bankIFSC'] = item['defaultValue'];
            }
        })


        contactDetailsData.map(item => {
            if (item.name == "contactName") {
                alltextBoxValues['contactName'] = item['defaultValue'];
            }
            if (item.name == 'designation') {
                alltextBoxValues['designation'] = item['defaultValue'];
            }
            if (item.name == "department") {
                alltextBoxValues['department'] = item['defaultValue'];
            }
            if (item.name == 'emailAddress') {
                alltextBoxValues['emailAddress'] = item['defaultValue'];
            }
            if (item.name == "mobileNumber") {
                alltextBoxValues['mobileNumber'] = item['defaultValue'];
            }
            if (item.name == 'phoneNumber') {
                alltextBoxValues['phoneNumber'] = item['defaultValue'];
            }
        })

        headApproverData.map(item => {
            if (item.name == "approverName") {
                alltextBoxValues['approverName'] = item['defaultValue'];
            }
            if (item.name == 'approverDesignation') {
                alltextBoxValues['approverDesignation'] = item['defaultValue'];
            }
            if (item.name == "approverEmailAddress") {
                alltextBoxValues['approverEmailAddress'] = item['defaultValue'];
            }
            if (item.name == 'approverMobileNumber') {
                alltextBoxValues['approverMobileNumber'] = item['defaultValue'];
            }
            if (item.name == "approverPhoneNumber") {
                alltextBoxValues['approverPhoneNumber'] = item['defaultValue'];
            }
        })

        //SETTING THE PAYLOAD
        // console.log("alltextBoxValues", alltextBoxValues)
        let payload = {
            "legalAddress": {
                "address1": alltextBoxValues["address"] || "-",
                // "address1": "154/1, Vijay Kiran Knowledge Park"
                // "address2": " 5th Main,Malleshpalya",
                // "address3": "New Thippasandra, CV Raman Nagar",
                "city": alltextBoxValues["cityTown"] || "-",
                "state": alltextBoxValues["stateName"] || "-",
                "country": alltextBoxValues["country"] || "-",
                "pincode": Number(alltextBoxValues["pincode"]) || "-",
            },
            "financialDetails": {
                "GSTIN": alltextBoxValues["gstin"] || "-",
                "PAN": alltextBoxValues["pan"] || "-"
            },
            "organizationType": "Trust",
            "dateOfRegistration": alltextBoxValues["dateOfRegistration"] || "-",
            "bankDetails": [
                {
                    "bankName": alltextBoxValues["bankName"] || "-",
                    "bankAccountName": alltextBoxValues["bankAccountName"] || "-",
                    "bankAccountNumber": alltextBoxValues["bankAccountNumber"] || "-",
                    "bankIFSC": alltextBoxValues["bankIFSC"] || "-",
                    "status": "Active"
                }
            ],
            "instituteContact": [
                {
                    "contactname": alltextBoxValues["contactName"] || "-",
                    "designation": alltextBoxValues["designation"] || "-",
                    "department": alltextBoxValues["department"] || "-",
                    "emailAddress": alltextBoxValues["emailAddress"] || "-",
                    "phoneNumber": alltextBoxValues["phoneNumber"] || "-",
                    "mobileNumber": alltextBoxValues["mobileNumber"] || "-"
                }
            ],
            "headApprover": [
                {
                    "required": true,
                    "headApproverName": alltextBoxValues["approverName"] || "-",
                    "designation": alltextBoxValues["approverDesignation"] || "-",
                    "emailAddress": alltextBoxValues["approverEmailAddress"] || "-",
                    "phoneNumber": alltextBoxValues["approverPhoneNumber"] || "-",
                    "mobileNumber": alltextBoxValues["approverMobileNumber"] || "-"
                }
            ],
            "campusId": alltextBoxValues["campusId"] || "-",
            "name": alltextBoxValues["campusName"] || "-",
            "legalName": alltextBoxValues["campusName"] || "-"
        }

        //CREATING NEW CAMPUS
        if (this.state.tableViewMode === 'addnew') {
            // console.log("Addnew payload", payload)
            let headers = {
                'Authorization': this.state.authToken
            };
            let bodyData = payload;
            axios.post(` ${this.state.env["zqBaseUri"]}/edu/campus?orgId=${this.state.orgId}`, bodyData, { headers })
                .then(res => {
                    if (res.success == true) {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Added successfully", status: "success" }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                        this.resetform();
                        this.getTableData();
                    }
                    else if (res.success == false) {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: res.message, status: "error" }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                        this.resetform();
                        this.getTableData();
                    }
                })
                .catch(err => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Error", status: "error" }
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                })
        }

        //UPDATING CAMPUS
        if (this.state.tableViewMode === 'preview') {
            let headers = {
                'Authorization': this.state.authToken
            };
            let bodyData = payload;
            // console.log("Preview payload", bodyData)
            axios.put(` ${this.state.env["zqBaseUri"]}/edu/campus/${this.state.payloadId}?orgId=${this.state.orgId}`, bodyData, { headers })
                .then(res => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Updated Successfully", status: "success" }
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                    this.resetform();
                    this.getTableData();
                })
                .catch(err => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Error", status: "error" }
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                    this.getTableData();
                })
        }
    }
    selectRowNew = () => { }
    allSelect = () => { }
    printScreen = () => { }
    moveBackTable = () => {
        this.setState({ tabViewForm: false })
        this.resetform()
    }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.getTableData()
        });
    };
    closeNotification = () => {
        let snackbarUpdate = { openNotification: false, NotificationMessage: "", status: "" }
        this.setState({ snackbar: snackbarUpdate })
    }
    resetform = () => {
        let campusFormData = CampusJson.AddNewCampusJson;
        // let campusFormData1 = InstallmentJson.AddstudentsFormjson;
        Object.keys(campusFormData).forEach(tab => {
            campusFormData[tab].forEach(task => {
                delete task['defaultValue']
                task['readOnly'] = false
                task['error'] = false
                task['required'] = task['requiredBoolean']
                task['validation'] = false
                delete task['clear']
            })
        })
        this.setState({ CampusJson: campusFormData, payloadId: "" })
    }
    formBtnHandle = () => {

    }
    cleanDatas = () => { }
    cancelViewData = () => {
        this.setState({ tabViewForm: false });
        this.resetform();
    }

    //Logo Handling functionalities
    handleLogo = (image) => {
        // console.log('called', image)
        this.setState({ logoPicture: image }, () => {
            // console.log(this.state.logoPicture)
        })
    }
    getLogoData = () => { }
    render() {
        return (
            <div className="list-of-students-mainDiv">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.tabViewForm == false ?
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} >| Campuses</p>
                        </div>
                        <div className="masters-body-div">
                            {this.state.containerNav == undefined ? null :
                                <React.Fragment>
                                    <ContainerNavbar containerNav={this.state.containerNav} onAddNew={() => { this.onAddBranches() }} searchValue={(searchValue) => this.searchHandle(searchValue)} onDownload={this.onDownload} printScreen={this.printScreen} />
                                    <div className="print-time">{this.state.printTime}</div>
                                </React.Fragment>}
                            <div className="remove-last-child-table">
                                {this.state.sampleCampusesData.length !== 0 ?
                                    <ZqTable
                                        allSelect={this.allSelect}
                                        data={this.state.sampleCampusesData}
                                        rowClick={(item) => { this.onPreviewStudentList(item) }}
                                        onRowCheckBox={(item) => { this.onPreviewStudentList1(item) }}
                                    />
                                    : <p className="noprog-txt">{this.state.tableResTxt}</p>
                                }
                            </div>
                            <div>
                                {this.state.sampleCampusesData.length !== 0 ?
                                    <PaginationUI
                                        total={this.state.totalRecord}
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        currentPage={this.state.page}
                                    />
                                    : null}
                            </div>
                        </div>
                    </React.Fragment> :
                    <React.Fragment>
                        <div className="tab-form-wrapper tab-table remainderPlan">
                            <div className="preview-btns">
                                {this.state.viewHeader == true ?
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| {this.state.campusName}</h6>
                                    </div> :
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| {this.state.campusName}</h6>
                                    </div>
                                }
                            </div>
                            <div className="organisation-table">
                                {this.state.viewFormLoad === false ?
                                    <ZenTabs tabData={this.state.newCampusJson} className="preview-wrap" tabEdit={this.state.preview} form={this.state.newCampusJson} cleanData={this.cleanDatas} cancelViewData={this.cancelViewData} value={0} onInputChanges={this.onInputChanges} onFormBtnEvent={(item) => { this.formBtnHandle(item); }} onTabFormSubmit={this.onFormSubmit} handleTabChange={this.handleTabChange} key={0}
                                        logoPicture={this.state.logoPicture} handleLogo={this.handleLogo} getLogoData={(e) => { this.getLogoData(e) }} />
                                    : <p className="noprog-txt">{this.state.tableResTxt}</p>}
                            </div>
                        </div>
                    </React.Fragment>
                }
                <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={2000} onClose={this.closeNotification}>
                    <Alert onClose={this.closeNotification}
                        severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
                        {this.state.snackbar.NotificationMessage}
                    </Alert>
                </Snackbar>
            </div>
        )
    }
}

export default withRouter(Campuses);