import React, { Component } from "react";
import '../../../../../scss/student.scss';
import '../../../../../scss/setup.scss';
import Loader from "../../../../../utils/loader/loaders";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ContainerNavbar from "../../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../../utils/Table/table-component";
import PaginationUI from "../../../../../utils/pagination/pagination";
import ZenTabs from '../../../../../components/input/tabs';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import RestoreOutlinedIcon from '@material-ui/icons/RestoreOutlined';
import paymentScheduleJSON from "./paymentSchedule.json";
import axios from 'axios';
import moment from 'moment';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import DateFormatContext from '../../../../../gigaLayout/context';
import xlsx from 'xlsx';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
class paymentSchedule extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            campusId: localStorage.getItem('campusId') === "null" || localStorage.getItem('campusId') === null ? undefined : localStorage.getItem('campusId'),
            containerNav: paymentScheduleJSON.containerNav, // installment.json
            sampleStudentsData: [],
            paymentSchedulejson: paymentScheduleJSON.paymentSchedule,
            newpaymentSchedulejson: paymentScheduleJSON.addPaymentSchedule,
            page: 1,
            limit: 10,
            totalRecord: 0,
            tabViewForm: false,
            viewFormLoad: true,
            totalPages: 0,
            tableView: true,
            tableResTxt: "Fetching Data ...",
            tableViewMode: "",
            payloadId: "",
            totalApiRes: null,
            snackbar: { openNotification: false, NotificationMessage: "", status: "success" },
            typeOfView: "",
            activeTab: 0,
            downloadTotalRecord: 0
        }
    }
    static contextType = DateFormatContext;
    componentDidMount() {
        this.getTableData()
        let addHalfyearChange = [
            {
                "category": "input",
                "type": "text",
                "name": "dueDate",
                "label": "Due Date",
                "class": "input-wrap col-2-input-first duedateSet",
                "readOnly": true,
                "required": true,
                "requiredBoolean": true,
                "clear": false,
                "defaultValue": "05/04/2021"
            },
            {
                "category": "input",
                "type": "text",
                "name": "percentage",
                "label": "Percentage",
                "class": "input-wrap col-2-input-last duedateSet",
                "readOnly": true,
                "required": true,
                "requiredBoolean": true,
                "clear": false,
                "defaultValue": "60%"
            },
            {
                "category": "input",
                "type": "text",
                "name": "dueDate",
                "label": "Due Date",
                "class": "input-wrap col-2-input-first duedateSet",
                "readOnly": true,
                "required": true,
                "requiredBoolean": true,
                "clear": false,
                "defaultValue": "05/10/2021"
            },
            {
                "category": "input",
                "type": "text",
                "name": "percentage",
                "label": "Percentage",
                "class": "input-wrap col-2-input-last duedateSet",
                "readOnly": true,
                "required": true,
                "requiredBoolean": true,
                "clear": false,
                "defaultValue": "40%"

            }]
        let removeDateRange = [
            {
                "type": "select",
                "name": "collectEvery",
                "class": "select-input-wrap setup-input-wrap",
                "label": "Collect Every",
                "readOnly": false,
                "required": true,
                "requiredBoolean": true,
                "clear": false,
                "defaultValue": "Half Year",
                "options": [
                    {
                        "label": "Year",
                        "value": "year"
                    },
                    {
                        "label": "Half Year",
                        "value": "Half Year"
                    },
                    {
                        "label": "Quarter",
                        "value": "quarter"
                    },
                    {
                        "label": "Two months",
                        "value": "two months"
                    },
                    {
                        "label": "Month",
                        "value": "month"
                    },
                    {
                        "label": "One Time Date",
                        "value": "one time"
                    },
                    {
                        "label": "One Time Date Range",
                        "value": "one time range"
                    }
                ]
            },
            {
                "type": "select",
                "name": "startMonth",
                "class": "select-input-wrap setup-input-wrap",
                "label": "Start Month",
                "readOnly": false,
                "required": true,
                "requiredBoolean": true,
                "clear": false,
                "options": [
                    {
                        "value": "january",
                        "label": "January"
                    },
                    {
                        "value": "february",
                        "label": "February"
                    },
                    {
                        "value": "march",
                        "label": "March"
                    },
                    {
                        "value": "april",
                        "label": "April"
                    },
                    {
                        "value": "may",
                        "label": "May"
                    },
                    {
                        "value": "june",
                        "label": "June"
                    },
                    {
                        "value": "july",
                        "label": "July"
                    },
                    {
                        "value": "august",
                        "label": "August"
                    },
                    {
                        "value": "september",
                        "label": "September"
                    },
                    {
                        "value": "october",
                        "label": "October"
                    },
                    {
                        "value": "november",
                        "label": "November"
                    },
                    {
                        "value": "december",
                        "label": "December"
                    }
                ]
            },
            {
                "type": "select",
                "name": "byDueDate",
                "class": "select-input-wrap setup-input-wrap",
                "label": "By Due Date",
                "readOnly": false,
                "required": true,
                "requiredBoolean": true,
                "clear": false,
                "defaultValue": 'fifth',
                "options": [
                    {
                        "label": "First",
                        "value": "first"
                    },
                    {
                        "label": "Second",
                        "value": "second"
                    },
                    {
                        "label": "Third",
                        "value": "third"
                    },
                    {
                        "label": "Fourth",
                        "value": "fourth"
                    },
                    {
                        "label": "Fifth",
                        "value": "fifth"
                    },
                    {
                        "label": "Last",
                        "value": "last"
                    },
                    {
                        "label": "Second Last",
                        "value": "second last"
                    },
                    {
                        "label": "Third Last",
                        "value": "third last"
                    },
                    {
                        "label": "Fourth Last",
                        "value": "fourth last"
                    },
                    {
                        "label": "Fifth Last",
                        "value": "fifth last"
                    }
                ]
            }
        ];
        let formData = paymentScheduleJSON.paymentSchedule;
        let formDataNew = paymentScheduleJSON.addPaymentSchedule;
        this.setState({ paymentSchedulejson: [], activeTab: 1, viewFormLoad: true }, () => {
            formData['Schedule Details'] = removeDateRange;
            formDataNew['Schedule Details'] = removeDateRange;
            formData['Fees Breakup'] = addHalfyearChange;
            formDataNew['Fees Breakup'] = addHalfyearChange;
            this.setState({ paymentSchedulejson: formData, viewFormLoad: false })
        })
    }
    getTableData = () => {
        this.setState({ sampleStudentsData: [], tabKeyData: 0 })
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/paymentSchedule?orgId=${this.state.orgId}&campusId=${this.state.campusId}&page=${this.state.page}&limit=${this.state.limit}`,
            headers: {
                'Authorization': this.state.authToken,
            }
        })
            .then(res => {
                let paymantSchedule = [];
                res.data.data.map((data) => {
                    let a = [];
                    data.feesBreakUp.map((dataOne) => {
                        a.push(`${dataOne}%`)
                    })
                    paymantSchedule.push(
                        {
                            "Id": data.displayName,
                            "TITLE": data.title,
                            "DESCRIPTION": data.description,
                            "Break Up": a.toString(),
                            // "COLLECTION PERIOD": data["scheduleDetails"]["collectEvery"] ? data["scheduleDetails"]["collectEvery"].charAt(0).toUpperCase() + data["scheduleDetails"]["collectEvery"].slice(1) : null,
                            "DUE DAY": data.scheduleDetails.dueDate,
                            "CAMPUS NAME":data.campusIdName,
                            "CREATED ON": data.createdAt !== "" ? this.context.dateFormat !== "" ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                            "CREATED BY": data.createdBy,
                            "Status": data.status == 1 ? "Active" : "Inactive",
                            "Item": JSON.stringify(data)
                        }
                    )
                })
                this.setState({ sampleStudentsData: paymantSchedule, totalApiRes: res.data.data, tabViewForm: false, totalRecord: res.data.totalRecord, totalPages: res.data.totalPages, page: res.data.currentPage, downloadTotalRecord: res.data.totalRecord })
            })
            .catch(err => {
                this.setState({ tableResTxt: "Loading Error" })
            })
    }
    handleBackFun = () => {
        this.setState({ tabViewForm: false, preview: false, activeTab: 0 })
    }
    onPreviewStudentList = (e) => {
        let details = JSON.parse(e.Item);
        if (e.Id == details.displayName) {
            console.log(details, e);
            this.setState({ tabViewForm: true, typeOfView: `Payment Schedule | ${e.Id}` })
            let collectEvery, byDueDate;
            let newStudentsFormjson = this.state.paymentSchedulejson;
            let formData = paymentScheduleJSON.paymentSchedule;
            this.state.totalApiRes.map((data) => {
                if (data.displayName === e.Id) {
                    this.setState({ tabViewForm: true, viewFormLoad: true, tableViewMode: "preview", payloadId: details["_id"] })
                    Object.keys(formData).forEach(tab => {
                        formData[tab].forEach(task => {
                            if (task['name'] == 'id') {
                                task['defaultValue'] = e.Id
                                task['readOnly'] = true
                                task['required'] = false
                            }
                            else if (task['name'] == 'title') {
                                task['defaultValue'] = details.title
                                task['readOnly'] = false
                                task['required'] = false
                            }
                            else if (task['name'] == 'description') {
                                task['defaultValue'] = details.description
                                task['readOnly'] = false
                                task['required'] = false
                            }
                            else if (task['name'] == 'collectEvery') {
                                task['defaultValue'] = String(details.scheduleDetails.collectEvery).toLowerCase()
                                task['readOnly'] = false
                                collectEvery = String(details.scheduleDetails.collectEvery).toLowerCase()
                                task['required'] = false
                            }
                            else if (task['name'] == 'byDueDate') {
                                task['defaultValue'] = String(details.scheduleDetails.dueDate).toLowerCase()
                                task['readOnly'] = false
                                byDueDate = String(details.scheduleDetails.dueDate).toLowerCase()
                                task['required'] = false
                            }
                        })
                    })
                    let newArrQ = [
                        {
                            "type": "heading",
                            "label": "Frequency",
                            "class": "form-title"
                        },
                        {
                            "type": "heading",
                            "label": "Percentage",
                            "class": "form-title-1"
                        }
                    ];
                    let newArrM = [
                        {
                            "type": "heading",
                            "label": "Frequency",
                            "class": "form-title"
                        },
                        {
                            "type": "heading",
                            "label": "Percentage",
                            "class": "form-title"
                        }
                    ]
                    let newArrY = [
                        {
                            "type": "heading",
                            "label": "Frequency",
                            "class": "form-title"
                        },
                        {
                            "type": "heading",
                            "label": "Percentage",
                            "class": "form-title"
                        }
                    ]
                    this.setState({ paymentSchedulejson: formData, viewFormLoad: false })
                    if (collectEvery == undefined || byDueDate == undefined) { }
                    else {
                        this.setState({ LoaderStatus: true });
                        axios({
                            method: 'get',
                            url: `${this.state.env["zqBaseUri"]}/edu/master/paymentSchedule/dueDateCalculation?collectEvery=${collectEvery}&dueDate=${byDueDate}`,
                            headers: {
                                'Authorization': this.state.authToken,
                                'Content-Type': 'application/json'

                            }
                        })
                            .then(res => {
                                let newArrQ = [
                                    {
                                        "type": "heading",
                                        "label": "Frequency",
                                        "class": "form-title"
                                    },
                                    {
                                        "type": "heading",
                                        "label": "Percentage",
                                        "class": "form-title-1"
                                    }
                                ];
                                let newArrM = [
                                    {
                                        "type": "heading",
                                        "label": "Frequency",
                                        "class": "form-title"
                                    },
                                    {
                                        "type": "heading",
                                        "label": "Percentage",
                                        "class": "form-title"
                                    }
                                ]
                                let newArrY = [
                                    {
                                        "type": "heading",
                                        "label": "Frequency",
                                        "class": "form-title"
                                    },
                                    {
                                        "type": "heading",
                                        "label": "Percentage",
                                        "class": "form-title"
                                    }
                                ]
                                res.data.data.map((data, i) => {
                                    console.log(res);
                                    if (details.scheduleDetails.collectEvery.toLowerCase() == "quarter") {
                                        newArrQ.push(
                                            {
                                                "category": "input",
                                                "type": "text",
                                                "name": "percentage",
                                                "label": `Quarter ${i + 1}`,
                                                "class": "input-wrap setup-input-wrap description-input-wrap rowInline",
                                                "readOnly": true,
                                                "required": false,
                                                "requiredBoolean": false,
                                                "clear": false,
                                                "defaultValue": String(Number(data.percentage.toFixed(2))) + "%"
                                            }
                                        )
                                        formData["Fees Breakup"] = newArrQ;
                                    }
                                    else if (details.scheduleDetails.collectEvery.toLowerCase() == "year") {
                                        newArrY.push(
                                            {
                                                "category": "input",
                                                "type": "text",
                                                "name": "percentage",
                                                "label": `Year ${i + 1}`,
                                                "class": "input-wrap setup-input-wrap description-input-wrap rowInline",
                                                "readOnly": true,
                                                "required": false,
                                                "requiredBoolean": false,
                                                "clear": false,
                                                "defaultValue": String(Number(data.percentage.toFixed(2))) + "%"
                                            }
                                        )
                                        formData["Fees Breakup"] = newArrY;
                                    }
                                    else if (details.scheduleDetails.collectEvery.toLowerCase() == "month") {
                                        newArrM.push(
                                            {
                                                "category": "input",
                                                "type": "text",
                                                "name": "percentage",
                                                "label": `Month ${i + 1}`,
                                                "class": "input-wrap setup-input-wrap description-input-wrap rowInline",
                                                "readOnly": true,
                                                "required": false,
                                                "requiredBoolean": false,
                                                "clear": false,
                                                "defaultValue": String(Number(data.percentage.toFixed(2))) + "%"
                                            }
                                        )
                                        formData["Fees Breakup"] = newArrM;
                                    }
                                })
                                this.setState({ paymentSchedulejson: formData, viewFormLoad: false, feesBreakUpArray: res.data.data })
                                this.setState({ paymentSchedulejson: newStudentsFormjson, viewFormLoad: false, LoaderStatus: false, feesBreakUpArray: res.data.data })
                            })
                            .catch(err => {
                                let snackbarUpdate = { openNotification: true, NotificationMessage: "Error loading payment Breakup Details", status: "error" };
                                this.setState({ snackbar: snackbarUpdate, viewFormLoad: true, LoaderStatus: false, tableResTxt: "Error loading Data" })
                            })
                    }

                }
                else { }
            })
        }
    }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.getTableData()
        });
    };
    moveBackTable = () => {
        this.setState({ tabViewForm: false, activeTab: 0 },
            () => {
                this.resetform('addNew');
                this.resetform('preview');
            })
    }
    handleTabChange = (tabName, formDatas) => { }
    onInputChanges = (value, item, event, dataS) => {
        // debugger
        console.log(dataS);
        if (dataS == undefined) {
        }
        else {
            setTimeout(() => {
                let commonName = dataS.name == undefined ? "" : dataS.name;
                let collectEvery, dueDate;
                let newStudentsFormjson = this.state.paymentSchedulejson;
                let formData = paymentScheduleJSON.paymentSchedule;
                let formDataNew = paymentScheduleJSON.addPaymentSchedule;
                let removeDateRange = [
                    {
                        "type": "select",
                        "name": "collectEvery",
                        "class": "select-input-wrap setup-input-wrap",
                        "label": "Collect Every",
                        "readOnly": false,
                        "required": true,
                        "requiredBoolean": true,
                        "clear": false,
                        "defaultValue": "Half Year",
                        "options": [
                            {
                                "label": "Year",
                                "value": "year"
                            },
                            {
                                "label": "Half Year",
                                "value": "Half Year"
                            },
                            {
                                "label": "Quarter",
                                "value": "quarter"
                            },
                            {
                                "label": "Two months",
                                "value": "two months"
                            },
                            {
                                "label": "Month",
                                "value": "month"
                            },
                            {
                                "label": "One Time Date",
                                "value": "one time"
                            },
                            {
                                "label": "One Time Date Range",
                                "value": "one time range"
                            }
                        ],
                        defaultValue: dataS.defaultValue
                    },
                    {
                        "type": "select",
                        "name": "startMonth",
                        "class": "select-input-wrap setup-input-wrap",
                        "label": "Start Month",
                        "readOnly": false,
                        "required": true,
                        "requiredBoolean": true,
                        "defaultValue": "january",
                        "clear": false,
                        "options": [
                            {
                                "value": "january",
                                "label": "January"
                            },
                            {
                                "value": "february",
                                "label": "February"
                            },
                            {
                                "value": "march",
                                "label": "March"
                            },
                            {
                                "value": "april",
                                "label": "April"
                            },
                            {
                                "value": "may",
                                "label": "May"
                            },
                            {
                                "value": "june",
                                "label": "June"
                            },
                            {
                                "value": "july",
                                "label": "July"
                            },
                            {
                                "value": "august",
                                "label": "August"
                            },
                            {
                                "value": "september",
                                "label": "September"
                            },
                            {
                                "value": "october",
                                "label": "October"
                            },
                            {
                                "value": "november",
                                "label": "November"
                            },
                            {
                                "value": "december",
                                "label": "December"
                            }
                        ]
                    },
                    {
                        "type": "select",
                        "name": "byDueDate",
                        "class": "select-input-wrap setup-input-wrap",
                        "label": "By Due Date",
                        "readOnly": false,
                        "required": true,
                        "requiredBoolean": true,
                        "clear": false,
                        "defaultValue": "fifth",
                        "options": [
                            {
                                "label": "First",
                                "value": "first"
                            },
                            {
                                "label": "Second",
                                "value": "second"
                            },
                            {
                                "label": "Third",
                                "value": "third"
                            },
                            {
                                "label": "Fourth",
                                "value": "fourth"
                            },
                            {
                                "label": "Fifth",
                                "value": "fifth"
                            },
                            {
                                "label": "Last",
                                "value": "last"
                            },
                            {
                                "label": "Second Last",
                                "value": "second last"
                            },
                            {
                                "label": "Third Last",
                                "value": "third last"
                            },
                            {
                                "label": "Fourth Last",
                                "value": "fourth last"
                            },
                            {
                                "label": "Fifth Last",
                                "value": "fifth last"
                            }
                        ],
                        defaultValue: dataS.defaultValue
                    }
                ];
                let removeDaterange1 = [
                    {
                        "type": "select",
                        "name": "startMonth",
                        "class": "select-input-wrap setup-input-wrap",
                        "label": "Start Month",
                        "readOnly": false,
                        "required": true,
                        "requiredBoolean": true,
                        "clear": false,
                        "options": [
                            {
                                "value": "january",
                                "label": "January"
                            },
                            {
                                "value": "february",
                                "label": "February"
                            },
                            {
                                "value": "march",
                                "label": "March"
                            },
                            {
                                "value": "april",
                                "label": "April"
                            },
                            {
                                "value": "may",
                                "label": "May"
                            },
                            {
                                "value": "june",
                                "label": "June"
                            },
                            {
                                "value": "july",
                                "label": "July"
                            },
                            {
                                "value": "august",
                                "label": "August"
                            },
                            {
                                "value": "september",
                                "label": "September"
                            },
                            {
                                "value": "october",
                                "label": "October"
                            },
                            {
                                "value": "november",
                                "label": "November"
                            },
                            {
                                "value": "december",
                                "label": "December"
                            }
                        ]
                    },
                    {
                        "type": "select",
                        "name": "byDueDate",
                        "class": "select-input-wrap setup-input-wrap",
                        "label": "By Due Date",
                        "readOnly": false,
                        "required": true,
                        "requiredBoolean": true,
                        "clear": false,
                        "options": [
                            {
                                "label": "First",
                                "value": "first"
                            },
                            {
                                "label": "Second",
                                "value": "second"
                            },
                            {
                                "label": "Third",
                                "value": "third"
                            },
                            {
                                "label": "Fourth",
                                "value": "fourth"
                            },
                            {
                                "label": "Fifth",
                                "value": "fifth"
                            },
                            {
                                "label": "Last",
                                "value": "last"
                            },
                            {
                                "label": "Second Last",
                                "value": "second last"
                            },
                            {
                                "label": "Third Last",
                                "value": "third last"
                            },
                            {
                                "label": "Fourth Last",
                                "value": "fourth last"
                            },
                            {
                                "label": "Fifth Last",
                                "value": "fifth last"
                            }
                        ]
                    }
                ]
                let addTwoDaterange1 = [
                    {
                        "type": "heading",
                        "label": "From",
                        "class": "form-hd",
                        "name": "oneTimeRangeFrom"
                    },
                    {
                        "type": "select",
                        "name": "oneTimeRangeFromStartMonth",
                        "class": "select-input-wrap setup-input-wrap",
                        "label": "Start Month",
                        "readOnly": false,
                        "required": true,
                        "requiredBoolean": true,
                        "clear": false,
                        "options": [
                            {
                                "value": "january",
                                "label": "January"
                            },
                            {
                                "value": "february",
                                "label": "February"
                            },
                            {
                                "value": "march",
                                "label": "March"
                            },
                            {
                                "value": "april",
                                "label": "April"
                            },
                            {
                                "value": "may",
                                "label": "May"
                            },
                            {
                                "value": "june",
                                "label": "June"
                            },
                            {
                                "value": "july",
                                "label": "July"
                            },
                            {
                                "value": "august",
                                "label": "August"
                            },
                            {
                                "value": "september",
                                "label": "September"
                            },
                            {
                                "value": "october",
                                "label": "October"
                            },
                            {
                                "value": "november",
                                "label": "November"
                            },
                            {
                                "value": "december",
                                "label": "December"
                            }
                        ]
                    },
                    // {
                    //     "type": "date",
                    //     "name": "oneTimeRangeDateFrom",
                    //     "label": "By Due Date",
                    //     "format": "DD/MM/YYYY",
                    //     "class": "input-wrap col-2-input-first",
                    //     "readOnly": false,
                    //     "required": true,
                    //     "requiredBoolean": true,
                    //     "clear": false
                    // },
                    {
                        "type": "select",
                        "name": "oneTimeRangeFrombyDueDate",
                        "class": "select-input-wrap setup-input-wrap",
                        "label": "By Due Date",
                        "readOnly": false,
                        "required": true,
                        "requiredBoolean": true,
                        "clear": false,
                        "options": [
                            {
                                "label": "First",
                                "value": "first"
                            },
                            {
                                "label": "Second",
                                "value": "second"
                            },
                            {
                                "label": "Third",
                                "value": "third"
                            },
                            {
                                "label": "Fourth",
                                "value": "fourth"
                            },
                            {
                                "label": "Fifth",
                                "value": "fifth"
                            },
                            {
                                "label": "Last",
                                "value": "last"
                            },
                            {
                                "label": "Second Last",
                                "value": "second last"
                            },
                            {
                                "label": "Third Last",
                                "value": "third last"
                            },
                            {
                                "label": "Fourth Last",
                                "value": "fourth last"
                            },
                            {
                                "label": "Fifth Last",
                                "value": "fifth last"
                            }
                        ]
                    },
                    {
                        "type": "heading",
                        "label": "To",
                        "class": "form-hd",
                        "name": "oneTimeRangeTo"
                    },
                    {
                        "type": "select",
                        "name": "oneTimeRangeToStartMonth",
                        "class": "select-input-wrap setup-input-wrap",
                        "label": "Start Month",
                        "readOnly": false,
                        "required": true,
                        "requiredBoolean": true,
                        "clear": false,
                        "options": [
                            {
                                "value": "january",
                                "label": "January"
                            },
                            {
                                "value": "february",
                                "label": "February"
                            },
                            {
                                "value": "march",
                                "label": "March"
                            },
                            {
                                "value": "april",
                                "label": "April"
                            },
                            {
                                "value": "may",
                                "label": "May"
                            },
                            {
                                "value": "june",
                                "label": "June"
                            },
                            {
                                "value": "july",
                                "label": "July"
                            },
                            {
                                "value": "august",
                                "label": "August"
                            },
                            {
                                "value": "september",
                                "label": "September"
                            },
                            {
                                "value": "october",
                                "label": "October"
                            },
                            {
                                "value": "november",
                                "label": "November"
                            },
                            {
                                "value": "december",
                                "label": "December"
                            }
                        ]
                    },
                    // {
                    //     "type": "date",
                    //     "name": "oneTimeRangeDateTo",
                    //     "format": "DD/MM/YYYY",
                    //     "label": "By Due Date",
                    //     "class": "input-wrap col-2-input-first",
                    //     "readOnly": false,
                    //     "required": true,
                    //     "requiredBoolean": true,
                    //     "clear": false
                    // }
                    {
                        "type": "select",
                        "name": "oneTimeRangeTobyDueDate",
                        "class": "select-input-wrap setup-input-wrap",
                        "label": "By Due Date",
                        "readOnly": false,
                        "required": true,
                        "requiredBoolean": true,
                        "clear": false,
                        "options": [
                            {
                                "label": "First",
                                "value": "first"
                            },
                            {
                                "label": "Second",
                                "value": "second"
                            },
                            {
                                "label": "Third",
                                "value": "third"
                            },
                            {
                                "label": "Fourth",
                                "value": "fourth"
                            },
                            {
                                "label": "Fifth",
                                "value": "fifth"
                            },
                            {
                                "label": "Last",
                                "value": "last"
                            },
                            {
                                "label": "Second Last",
                                "value": "second last"
                            },
                            {
                                "label": "Third Last",
                                "value": "third last"
                            },
                            {
                                "label": "Fourth Last",
                                "value": "fourth last"
                            },
                            {
                                "label": "Fifth Last",
                                "value": "fifth last"
                            }
                        ]
                    }
                ]
                let addTwoDaterange = [
                    {
                        "type": "select",
                        "name": "collectEvery",
                        "class": "select-input-wrap setup-input-wrap",
                        "label": "Collect Every",
                        "readOnly": false,
                        "required": true,
                        "requiredBoolean": true,
                        "clear": false,
                        "options": [
                            {
                                "label": "Year",
                                "value": "year"
                            },
                            {
                                "label": "Half Year",
                                "value": "Half Year"
                            },
                            {
                                "label": "Quarter",
                                "value": "quarter"
                            },
                            {
                                "label": "Two months",
                                "value": "two months"
                            },
                            {
                                "label": "Month",
                                "value": "month"
                            },
                            {
                                "label": "One Time Date",
                                "value": "one time"
                            },
                            {
                                "label": "One Time Date Range",
                                "value": "one time range"
                            }
                        ],
                        defaultValue: dataS.defaultValue
                    },
                    {
                        "type": "heading",
                        "label": "From",
                        "class": "form-hd"
                    },
                    {
                        "type": "select",
                        "name": "startMonth",
                        "class": "select-input-wrap setup-input-wrap",
                        "label": "Start Month",
                        "readOnly": false,
                        "required": true,
                        "requiredBoolean": true,
                        "clear": false,
                        "options": [
                            {
                                "value": "january",
                                "label": "January"
                            },
                            {
                                "value": "february",
                                "label": "February"
                            },
                            {
                                "value": "march",
                                "label": "March"
                            },
                            {
                                "value": "april",
                                "label": "April"
                            },
                            {
                                "value": "may",
                                "label": "May"
                            },
                            {
                                "value": "june",
                                "label": "June"
                            },
                            {
                                "value": "july",
                                "label": "July"
                            },
                            {
                                "value": "august",
                                "label": "August"
                            },
                            {
                                "value": "september",
                                "label": "September"
                            },
                            {
                                "value": "october",
                                "label": "October"
                            },
                            {
                                "value": "november",
                                "label": "November"
                            },
                            {
                                "value": "december",
                                "label": "December"
                            }
                        ]
                    },
                    {
                        "type": "date",
                        "name": "DateForm",
                        "label": "By Due Date",
                        "format": "DD/MM/YYYY",
                        "class": "input-wrap col-2-input-first",
                        "readOnly": false,
                        "required": true,
                        "requiredBoolean": true,
                        "clear": false
                    },
                    {
                        "type": "heading",
                        "label": "To",
                        "class": "form-hd"
                    },
                    {
                        "type": "select",
                        "name": "startMonth",
                        "class": "select-input-wrap setup-input-wrap",
                        "label": "Start Month",
                        "readOnly": false,
                        "required": true,
                        "requiredBoolean": true,
                        "clear": false,
                        "options": [
                            {
                                "value": "january",
                                "label": "January"
                            },
                            {
                                "value": "february",
                                "label": "February"
                            },
                            {
                                "value": "march",
                                "label": "March"
                            },
                            {
                                "value": "april",
                                "label": "April"
                            },
                            {
                                "value": "may",
                                "label": "May"
                            },
                            {
                                "value": "june",
                                "label": "June"
                            },
                            {
                                "value": "july",
                                "label": "July"
                            },
                            {
                                "value": "august",
                                "label": "August"
                            },
                            {
                                "value": "september",
                                "label": "September"
                            },
                            {
                                "value": "october",
                                "label": "October"
                            },
                            {
                                "value": "november",
                                "label": "November"
                            },
                            {
                                "value": "december",
                                "label": "December"
                            }
                        ]
                    },
                    {
                        "type": "date",
                        "name": "DateTo",
                        "format": "DD/MM/YYYY",
                        "label": "By Due Date",
                        "class": "input-wrap col-2-input-first",
                        "readOnly": false,
                        "required": true,
                        "requiredBoolean": true,
                        "clear": false
                    }
                ]
                newStudentsFormjson["Schedule Details"].map((data) => {
                    if (data.name == "collectEvery") {
                        collectEvery = commonName == "collectEvery" ? dataS.defaultValue : data.defaultValue
                    }
                    else if (data.name == "byDueDate") {
                        dueDate = commonName == "byDueDate" ? dataS.defaultValue : data.defaultValue
                    }
                })
                // debugger
                if (dataS.defaultValue == "one time range") {
                    // if (data['Schedule Details'][0]['defaultValue'] == "one time range") {
                    this.setState({ paymentSchedulejson: [], activeTab: 1, viewFormLoad: true }, () => {
                        formData['Schedule Details'] = formData['Schedule Details'].filter((item) => item.name !== "startMonth");
                        formData['Schedule Details'] = formData['Schedule Details'].filter((item) => item.name !== "byDueDate");
                        formDataNew['Schedule Details'] = formDataNew['Schedule Details'].filter((item) => item.name !== "startMonth");
                        formDataNew['Schedule Details'] = formDataNew['Schedule Details'].filter((item) => item.name !== "byDueDate");
                        formData['Schedule Details'] = formData['Schedule Details'].concat(addTwoDaterange1);
                        formData['Schedule Details'] = formDataNew['Schedule Details'].concat(addTwoDaterange1);
                        //previous code
                        // formData['Schedule Details'] = addTwoDaterange;
                        // formData['Schedule Details'] = addTwoDaterange;
                        this.setState({ paymentSchedulejson: formData, viewFormLoad: false })
                    })
                }
                else {
                    this.setState({ paymentSchedulejson: [], activeTab: 1, viewFormLoad: true }, () => {
                        //remove
                        formData['Schedule Details'] = formData['Schedule Details'].filter((item) => item.name !== "oneTimeRangeFromStartMonth");
                        formData['Schedule Details'] = formData['Schedule Details'].filter((item) => item.name !== "oneTimeRangeFrombyDueDate");
                        formData['Schedule Details'] = formData['Schedule Details'].filter((item) => item.name !== "oneTimeRangeToStartMonth");
                        formData['Schedule Details'] = formData['Schedule Details'].filter((item) => item.name !== "oneTimeRangeTobyDueDate");
                        formData['Schedule Details'] = formData['Schedule Details'].filter((item) => item.name !== "oneTimeRangeTo");
                        formData['Schedule Details'] = formData['Schedule Details'].filter((item) => item.name !== "oneTimeRangeFrom");

                        formDataNew['Schedule Details'] = formDataNew['Schedule Details'].filter((item) => item.name !== "oneTimeRangeFromStartMonth");
                        formDataNew['Schedule Details'] = formDataNew['Schedule Details'].filter((item) => item.name !== "oneTimeRangeFrombyDueDate");
                        formDataNew['Schedule Details'] = formDataNew['Schedule Details'].filter((item) => item.name !== "oneTimeRangeToStartMonth");
                        formDataNew['Schedule Details'] = formDataNew['Schedule Details'].filter((item) => item.name !== "oneTimeRangeTobyDueDate");
                        formDataNew['Schedule Details'] = formDataNew['Schedule Details'].filter((item) => item.name !== "oneTimeRangeTo");
                        formDataNew['Schedule Details'] = formDataNew['Schedule Details'].filter((item) => item.name !== "oneTimeRangeFrom");

                        //add
                        let index = formData['Schedule Details'].findIndex(item => item.name === "startMonth")
                        if (index === -1) {
                            formData['Schedule Details'] = formData['Schedule Details'].concat(removeDaterange1)
                            formDataNew['Schedule Details'] = formDataNew['Schedule Details'].concat(removeDaterange1);
                        }

                        // previous code
                        // formData['Schedule Details'] = removeDateRange;
                        // formDataNew['Schedule Details'] = removeDateRange;
                        this.setState({ paymentSchedulejson: formData, viewFormLoad: false })
                    })
                }
                //harcoded
                if (typeof dataS.defaultValue === 'string' && dataS.defaultValue.toLowerCase() == "two months") {
                    let data = [1, 2, 3, 4, 5, 6]
                    let newArrM = [
                        {
                            "type": "heading",
                            "label": "Frequency",
                            "class": "form-title"
                        },
                        {
                            "type": "heading",
                            "label": "Percentage",
                            "class": "form-title"
                        }
                    ]
                    data.map((item, i) => {
                        // let c = i + 1;
                        // if (c % 2 !== 0) c = i
                        // else c = i + 1
                        let c;
                        if (i === 0) c = "1, 2"
                        if (i === 1) c = "3, 4"
                        if (i === 2) c = "5, 6"
                        if (i === 3) c = "7, 8"
                        if (i === 4) c = "9, 10"
                        if (i === 5) c = "11, 12"
                        newArrM.push(
                            {
                                "category": "input",
                                "type": "text",
                                "name": "percentage",
                                "label": `Month ${c}`,
                                "class": "input-wrap setup-input-wrap description-input-wrap rowInline",
                                "readOnly": true,
                                "required": false,
                                "requiredBoolean": false,
                                "clear": false,
                                "defaultValue": "16.66%"
                            }
                        )
                        formData["Fees Breakup"] = newArrM;
                        formDataNew["Fees Breakup"] = newArrM;
                    })
                }

                if (typeof dataS.defaultValue === 'string' && dataS.defaultValue.toLowerCase() == "one time range") {
                    let data = [1]
                    let newArrM = [
                        {
                            "type": "heading",
                            "label": "Frequency",
                            "class": "form-title"
                        },
                        {
                            "type": "heading",
                            "label": "Percentage",
                            "class": "form-title"
                        }
                    ]
                    data.map((item, i) => {
                        newArrM.push(
                            {
                                "category": "input",
                                "type": "text",
                                "name": "percentage",
                                "label": `One Time`,
                                "class": "input-wrap setup-input-wrap description-input-wrap rowInline",
                                "readOnly": true,
                                "required": false,
                                "requiredBoolean": false,
                                "clear": false,
                                "defaultValue": "100%"
                            }
                        )
                        formData["Fees Breakup"] = newArrM;
                        formDataNew["Fees Breakup"] = newArrM;
                    })
                }

                this.setState({ paymentSchedulejson: newStudentsFormjson }, () => {
                    if (collectEvery == undefined || dueDate == undefined) { }
                    else {
                        this.setState({ LoaderStatus: true });
                        axios({
                            method: 'get',
                            url: `${this.state.env["zqBaseUri"]}/edu/master/paymentSchedule/dueDateCalculation?collectEvery=${collectEvery == "One Time" ? "year" : collectEvery}&dueDate=${dueDate}`,
                            headers: {
                                'Authorization': this.state.authToken,
                                'Content-Type': 'application/json'

                            }
                        })
                            .then(res => {
                                // debugger
                                let newArrQ = [
                                    {
                                        "type": "heading",
                                        "label": "Frequency",
                                        "class": "form-title"
                                    },
                                    {
                                        "type": "heading",
                                        "label": "Percentage",
                                        "class": "form-title-1"
                                    }
                                ];
                                let newArrM = [
                                    {
                                        "type": "heading",
                                        "label": "Frequency",
                                        "class": "form-title"
                                    },
                                    {
                                        "type": "heading",
                                        "label": "Percentage",
                                        "class": "form-title"
                                    }
                                ]
                                let newArrY = [
                                    {
                                        "type": "heading",
                                        "label": "Frequency",
                                        "class": "form-title"
                                    },
                                    {
                                        "type": "heading",
                                        "label": "Percentage",
                                        "class": "form-title"
                                    }
                                ]

                                res.data.data.map((data, i) => {
                                    // debugger
                                    if (dataS.defaultValue.toLowerCase() === "quarter") {
                                        newArrQ.push(
                                            {
                                                "category": "input",
                                                "type": "text",
                                                "name": "percentage",
                                                "label": `Quarter ${i + 1}`,
                                                "class": "input-wrap setup-input-wrap description-input-wrap rowInline",
                                                "readOnly": true,
                                                "required": false,
                                                "requiredBoolean": false,
                                                "clear": false,
                                                "defaultValue": String(Number(data.percentage.toFixed(2))) + "%"
                                            }
                                        )
                                        formData["Fees Breakup"] = newArrQ;
                                        formDataNew["Fees Breakup"] = newArrQ;
                                    }
                                    if (dataS.defaultValue.toLowerCase() === "half year") {
                                        // debugger
                                        newArrQ.push(
                                            {
                                                "category": "input",
                                                "type": "text",
                                                "name": "percentage",
                                                "label": `Half Year ${i + 1}`,
                                                "class": "input-wrap setup-input-wrap description-input-wrap rowInline",
                                                "readOnly": true,
                                                "required": false,
                                                "requiredBoolean": false,
                                                "clear": false,
                                                "defaultValue": String(Number(data.percentage.toFixed(2))) + "%"
                                            }
                                        )
                                        formData["Fees Breakup"] = newArrQ;
                                        formDataNew["Fees Breakup"] = newArrQ;
                                        // newArrQ.push(
                                        //     {
                                        //         "category": "input",
                                        //         "type": "text",
                                        //         "name": "dueDate",
                                        //         "label": "1/10/2021",
                                        //         "class": "input-wrap setup-input-wrap description-input-wrap rowInline",
                                        //         "readOnly": true,
                                        //         "required": false,
                                        //         "requiredBoolean": false,
                                        //         "clear": false
                                        //     },
                                        //     {
                                        //         "category": "input",
                                        //         "type": "text",
                                        //         "name": "percentage",
                                        //         "label": data.dueDate,
                                        //         "class": "input-wrap setup-input-wrap description-input-wrap rowInline",
                                        //         "readOnly": true,
                                        //         "required": false,
                                        //         "requiredBoolean": false,
                                        //         "clear": false,
                                        //         "defaultValue": "60%"
                                        //     },
                                        //     {
                                        //         "category": "input",
                                        //         "type": "text",
                                        //         "name": "dueDate",
                                        //         "label": "Date",
                                        //         "class": "input-wrap setup-input-wrap description-input-wrap rowInline",
                                        //         "readOnly": true,
                                        //         "required": false,
                                        //         "requiredBoolean": false,
                                        //         "clear": false,
                                        //         "defaultValue": "02/10/2021"
                                        //     },
                                        //     {
                                        //         "category": "input",
                                        //         "type": "text",
                                        //         "name": "percentage",
                                        //         "label": data.dueDate,
                                        //         "class": "input-wrap setup-input-wrap description-input-wrap rowInline",
                                        //         "readOnly": true,
                                        //         "required": false,
                                        //         "requiredBoolean": false,
                                        //         "clear": false
                                        //     }
                                        // )
                                        // formData["Fees Breakup"] = newArrQ;
                                        // formDataNew["Fees Breakup"] = newArrQ;
                                    }
                                    else if (dataS.defaultValue.toLowerCase() == "year") {
                                        newArrY.push(
                                            {
                                                "category": "input",
                                                "type": "text",
                                                "name": "percentage",
                                                "label": `Year ${i + 1}`,
                                                "class": "input-wrap setup-input-wrap description-input-wrap rowInline",
                                                "readOnly": true,
                                                "required": false,
                                                "requiredBoolean": false,
                                                "clear": false,
                                                "defaultValue": String(Number(data.percentage.toFixed(2))) + "%"
                                            }
                                        )
                                        formData["Fees Breakup"] = newArrY;
                                        formDataNew["Fees Breakup"] = newArrY;
                                    }
                                    else if (dataS.defaultValue.toLowerCase() == "month") {
                                        newArrM.push(
                                            {
                                                "category": "input",
                                                "type": "text",
                                                "name": "percentage",
                                                "label": `Month ${i + 1}`,
                                                "class": "input-wrap setup-input-wrap description-input-wrap rowInline",
                                                "readOnly": true,
                                                "required": false,
                                                "requiredBoolean": false,
                                                "clear": false,
                                                "defaultValue": String(Number(data.percentage.toFixed(2))) + "%"
                                            }
                                        )
                                        formData["Fees Breakup"] = newArrM;
                                        formDataNew["Fees Breakup"] = newArrM;
                                    }
                                    else if (dataS.defaultValue.toLowerCase() == "one time") {
                                        // debugger
                                        newArrY.push(
                                            {
                                                "category": "input",
                                                "type": "text",
                                                "name": "percentage",
                                                "label": `One Time`,
                                                "class": "input-wrap setup-input-wrap description-input-wrap rowInline",
                                                "readOnly": true,
                                                "required": false,
                                                "requiredBoolean": false,
                                                "clear": false,
                                                "defaultValue": String(Number(data.percentage.toFixed(2))) + "%"
                                            }
                                        )
                                        formData["Fees Breakup"] = newArrY;
                                        formDataNew["Fees Breakup"] = newArrY;
                                    }
                                })
                                setTimeout(() => {
                                    this.setState({ viewType: true, paymentSchedulejson: formData, viewFormLoad: false, feesBreakUpArray: res.data.data })
                                    this.setState({ viewType: true, paymentSchedulejson: newStudentsFormjson, viewFormLoad: false, LoaderStatus: false, feesBreakUpArray: res.data.data })
                                }, 500);
                            })
                            .catch(err => {
                                let snackbarUpdate = { openNotification: true, NotificationMessage: "Error loading payment Breakup Details", status: "error" };
                                this.setState({ snackbar: snackbarUpdate, viewFormLoad: true, LoaderStatus: false, tableResTxt: "Error loading Data" })
                            })
                    }
                })

            }, 300)
        }
    }
    onAddPaymentSchedule = () => {
        this.setState({ tabViewForm: true, viewHeader: false, viewFormLoad: true, tableViewMode: "addnew", tableResTxt: "Fetching Data...", typeOfView: "New Payment Schedule" }, () => {
            let paymentSchedule = paymentScheduleJSON.addPaymentSchedule;
            axios({
                method: 'get',
                url: `${this.state.env["zqBaseUri"]}/edu/master/getDisplayId/paymentSchedule?orgId=${this.state.orgId}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            })
                .then(res => {
                    Object.keys(paymentSchedule).forEach(tab => {
                        paymentSchedule[tab].forEach(task => {
                            if (task['name'] == 'id') {
                                task['defaultValue'] = String(res.data.data)
                                task['readOnly'] = true
                            }
                        })
                    })
                    this.setState({ paymentSchedulejson: paymentSchedule, viewFormLoad: false, typeOfView: `New Payment Schedule | ${res.data.data}` })
                })
                .catch(err => {
                    this.setState({ tableResTxt: "Error loading Data" })
                })
        })
    }
    onFormSubmit = (data, item) => {
        this.setState({ isLoader: true, LoaderStatus: true })
        let generalData = data['General'];
        let scheduleData = data['Schedule Details']
        let feeBreakUp = data['Fees Breakup']
        let defaultValue1 = {}
        generalData.map(item => {
            if (item.name == "id") {
                defaultValue1['id'] = item['defaultValue'];
            }
            if (item.name == 'title') {
                defaultValue1['title'] = item['defaultValue'];
            }
            if (item.name == 'description') {
                defaultValue1['description'] = item['defaultValue'];
            }
            if (item.name == 'noOfInstallments') {
                defaultValue1['noOfInstallments'] = item['defaultValue'];
            }
        })
        scheduleData.map(item => {
            if (item.name == "collectEvery") {
                defaultValue1['collectEvery'] = item['defaultValue'];
            }
            if (item.name == 'byDueDate') {
                defaultValue1['byDueDate'] = item['defaultValue'];
            }
        })
        feeBreakUp.map(item => {
            if (item.name == "dueDate") {
                defaultValue1['dueDate'] = item['defaultValue'];
            }
            if (item.name == 'percentage') {
                defaultValue1['percentage'] = item['defaultValue'];
            }
        })
        let feeBrk = [];
        this.state.feesBreakUpArray.map((dataTwo) => {
            feeBrk.push(dataTwo.percentage)
        })
        let payload = {
            "displayName": defaultValue1['id'],
            "title": defaultValue1['title'],
            "description": defaultValue1['description'] == undefined || defaultValue1['description'] == null ? "" : defaultValue1['description'],
            "scheduleDetails": {
                "collectEvery": defaultValue1['collectEvery'],
                "dueDate": defaultValue1['byDueDate'],
            },
            "parentId": this.state.payloadId,
            "feesBreakUp": feeBrk,
            "createdBy": this.state.orgId,
            "orgId": this.state.orgId
        }
        if (this.state.tableViewMode === "preview") {
            let headers = {
                'Authorization': this.state.authToken
            };
            let bodyData = payload;
            axios.put(`${this.state.env["zqBaseUri"]}/edu/master/paymentSchedule?id=${this.state.payloadId}`, bodyData, { headers })
                .then(res => {
                    if (res.data.status == "error") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Failed to update the payment schedule", status: "error" }
                        let snackbarUpdate2 = { openNotification: false }
                        setTimeout(() => { this.setState({ tabViewForm: false, snackbar: snackbarUpdate2 }) }, 1000)
                        this.resetform('preview');
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                        this.getTableData();
                    }
                    else {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Updated Successfully", status: "success" }
                        let snackbarUpdate2 = { openNotification: false }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                        setTimeout(() => { this.setState({ tabViewForm: false, snackbar: snackbarUpdate2 }) }, 1000)
                        this.resetform('preview');
                        this.getTableData();
                    }
                })
                .catch(err => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Failed to update the payment schedule", status: "error" }
                    let snackbarUpdate2 = { openNotification: false }
                    setTimeout(() => { this.setState({ tabViewForm: false, snackbar: snackbarUpdate2 }) }, 1000)
                    this.resetform('preview');
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                    this.getTableData();

                })
        }
        if (this.state.tableViewMode === "addnew") {
            let headers = {
                'Authorization': this.state.authToken
            };
            let bodyData = payload;
            axios.post(`${this.state.env["zqBaseUri"]}/edu/master/paymentSchedule`, bodyData, { headers })
                .then(res => {
                    if (res.data.status == "error") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Failed to add the payment schedule", status: "error" }
                        let snackbarUpdate2 = { openNotification: false }
                        setTimeout(() => { this.setState({ tabViewForm: false, snackbar: snackbarUpdate2 }) }, 1000)
                        this.resetform('addNew');
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                        this.getTableData();
                    }
                    else {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Added Successfully", status: "success" }
                        let snackbarUpdate2 = { openNotification: false }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                        setTimeout(() => { this.setState({ tabViewForm: false, snackbar: snackbarUpdate2 }) }, 1000)
                        this.resetform('addNew');
                        this.getTableData();
                    }
                })
                .catch(err => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Failed to add the payment schedule", status: "error" }
                    let snackbarUpdate2 = { openNotification: false }
                    setTimeout(() => { this.setState({ tabViewForm: false, snackbar: snackbarUpdate2 }) }, 1000)
                    this.resetform('addNew');
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                    this.getTableData();
                })
        }
    }
    closeNotification = () => {
        let snackbarUpdate = { openNotification: false, NotificationMessage: "", status: "" }
        this.setState({ snackbar: snackbarUpdate })
    }

    resetform = (type) => {
        let FormData = paymentScheduleJSON.addPaymentSchedule;
        let FormData1 = paymentScheduleJSON.paymentSchedule
        Object.keys(FormData).forEach(tab => {
            FormData[tab].forEach(task => {
                delete task['defaultValue']
                task['error'] = false
                task['required'] = task['requiredBoolean']
                task['validation'] = false
                delete task['clear']
            })
        })
        Object.keys(FormData1).forEach(tab => {
            FormData1[tab].forEach(task => {
                delete task['defaultValue']
                task['error'] = false
                task['required'] = task['requiredBoolean']
                task['validation'] = false
                delete task['clear']
            })
        })
        this.setState({ paymentSchedulejson: FormData, payloadId: "", status: "" })
    }
    cancelViewData = () => {
        this.setState({ tabViewForm: false });
        this.resetform('preview');
        this.resetform('addNew');
    }
    cleanDatas = () => { }
    allSelectData = () => { }
    onPreviewStudentList1 = () => { }
    searchHandle = (value) => { }
    printScreen = () => {
        let page = 1
        let limit = this.state.downloadTotalRecord || 1
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/paymentSchedule?orgId=${this.state.orgId}&page=${page}&limit=${limit}`,
            headers: {
                'Authorization': this.state.authToken,
            }
        })
            .then(res => {
                this.setState({ sampleStudentsData: [] })
                let paymantSchedule = [];
                res.data.data.map((data) => {
                    let a = [];
                    data.feesBreakUp.map((dataOne) => {
                        a.push(`${dataOne}%`)
                    })
                    paymantSchedule.push(
                        {
                            "Id": data.displayName,
                            "TITLE": data.title,
                            "DESCRIPTION": data.description,
                            "Break Up": a.toString(),
                            // "COLLECTION PERIOD": data["scheduleDetails"]["collectEvery"] ? data["scheduleDetails"]["collectEvery"].charAt(0).toUpperCase() + data["scheduleDetails"]["collectEvery"].slice(1) : null,
                            "DUE DAY": data.scheduleDetails.dueDate,
                            "CAMPUS NAME": data.campusIdName,
                            "CREATED ON": data.createdAt !== "" ? this.context.dateFormat !== "" ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                            "CREATED BY": data.createdBy,
                            "Status": data.status == 1 ? "Active" : "Inactive",
                            "Item": JSON.stringify(data)
                        }
                    )
                })
                this.setState({ sampleStudentsData: paymantSchedule }, () => {
                    setTimeout(() => {
                        window.print()
                        this.getTableData()
                    }, 1);
                })
            })
            .catch(err => {
                console.log("error", err)
            })
    }
    onDownload = () => {
        let createXlxsData = [];
        let page = 1
        let limit = this.state.downloadTotalRecord
        this.setState({ LoaderStatus: true }, () => {
            axios({
                method: 'get',
                url: `${this.state.env["zqBaseUri"]}/edu/master/paymentSchedule?orgId=${this.state.orgId}&page=${page}&limit=${limit}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            }).then(res => {
                if (res.status === 200) {
                    if (res.data) {
                        res.data.data.map((data) => {
                            let a = [];
                            data.feesBreakUp.map((dataOne) => {
                                a.push(`${dataOne}%`)
                            })
                            createXlxsData.push(
                                {
                                    "Id": data.displayName,
                                    "TITLE": data.title,
                                    "DESCRIPTION": data.description,
                                    "Break Up": a.toString(),
                                    "DUE DAY": data.scheduleDetails.dueDate,
                                    "CAMPUS NAME": data.campusIdName,
                                    "CREATED ON": data.createdAt !== "" ? this.context.dateFormat !== "" ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                    "CREATED BY": data.createdBy,
                                    "Status": data.status == 1 ? "Active" : "Inactive",
                                }
                            )
                        })
                        var ws = xlsx.utils.json_to_sheet(createXlxsData);
                        var wb = xlsx.utils.book_new();
                        xlsx.utils.book_append_sheet(wb, ws, "Payment Schedule");
                        xlsx.writeFile(wb, "Payment Schedule.xlsx");
                        this.setState({ LoaderStatus: false })
                    }
                }
            }).catch(err => {
                console.log(err, 'errorrrrr')
                this.setState({ LoaderStatus: false })
            })
        })
    }
    render() {
        return (
            <div className="list-of-students-mainDiv payment-input-gap">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.tabViewForm == false ?
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title">| Payment Schedule</p>
                        </div>
                        <div className="masters-body-div">
                            {this.state.containerNav == undefined ? null :
                                <React.Fragment>
                                    <ContainerNavbar containerNav={this.state.containerNav} onAddNew={() => { this.onAddPaymentSchedule(); }} searchValue={(searchValue) => this.searchHandle(searchValue)} onDownload={this.onDownload} printScreen={this.printScreen} />
                                    <div className="print-time">{this.state.printTime}</div>
                                </React.Fragment>}
                            <div className="remove-last-child-table">
                                {this.state.sampleStudentsData.length !== 0 ?
                                    <ZqTable
                                        allSelect={this.allSelectData}
                                        data={this.state.sampleStudentsData}
                                        rowClick={(item) => { this.onPreviewStudentList(item) }}
                                        onRowCheckBox={(item) => { this.onPreviewStudentList1(item) }}
                                    /> : <p className="noprog-txt">{this.state.tableResTxt}</p>
                                }
                            </div>
                            <div>
                                {this.state.sampleStudentsData.length !== 0 ?
                                    <PaginationUI
                                        total={this.state.totalRecord}
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        currentPage={this.state.page}
                                    /> : null}
                            </div>
                        </div>
                    </React.Fragment> :
                    <React.Fragment>
                        <div className="tab-form-wrapper tab-table">
                            <div className="preview-btns">
                                <div className="preview-header-wrap">
                                    < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                    <h6 className="preview-Id-content" onClick={this.moveBackTable}>| {this.state.typeOfView}</h6>
                                </div>
                                {/* <div className="notes-history-wrapper">
                                    <div className="history-icon-box" title="View History" onClick={this.openHistory}> <RestoreOutlinedIcon className="material-historyIcon" /> <p>History</p></div>
                                </div> */}
                            </div>
                            <div className="organisation-table payment-schedule-form">
                                {this.state.viewFormLoad === false ?
                                    <ZenTabs activeTab={this.state.activeTab} tabData={this.state.paymentSchedulejson} className="preview-wrap" tabEdit={this.state.preview} cleanData={this.cleanDatas} form={this.state.paymentSchedulejson} cancelViewData={this.cancelViewData} value={0} onInputChanges={this.onInputChanges} onFormBtnEvent={(item) => { this.formBtnHandle(item); }} onTabFormSubmit={this.onFormSubmit} handleTabChange={this.handleTabChange} key={0} />
                                    : <p className="noprog-txt">{this.state.tableResTxt}</p>}

                            </div>
                        </div>
                    </React.Fragment>
                }
                <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={2500} onClose={this.closeNotification}>
                    <Alert onClose={this.closeNotification}
                        severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
                        {this.state.snackbar.NotificationMessage}
                    </Alert>
                </Snackbar>
                {this.state.preview ?
                    <React.Fragment>
                        <div className="tab-form-wrapper tab-table">
                            <div className="preview-btns">
                                <div className="preview-header-wrap">
                                    < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} />
                                    <h6 className="preview-Id-content">{this.state.typeOfView}</h6>
                                </div>
                            </div>
                            <div className="organisation-table payment-schedule-form">
                                {this.state.viewFormLoad === false ?
                                    <ZenTabs activeTab={this.state.activeTab} tabData={this.state.paymentSchedulejson} className="preview-wrap" tabEdit={this.state.preview} form={this.state.paymentSchedulejson} value={0} cancelViewData={this.cancelViewData} onInputChanges={this.onInputChanges} onFormBtnEvent={(item) => { this.formBtnHandle(item); }} onTabFormSubmit={this.onFormSubmit} handleTabChange={this.handleTabChange} key={0} />
                                    : <p className="noprog-txt">{this.state.tableResTxt}</p>}
                            </div>
                        </div>
                    </React.Fragment> : null}


            </div>
        )
    }
}
export default paymentSchedule;

// {
//     "type": "select",
//     "name": "collectEveryOptions",
//     "class": "select-input-wrap setup-input-wrap clear-label-input",
//     "label": "By due Date",
//     "readOnly": false,
//     "required": true,
//     "requiredBoolean": true,
//     "clear": false,
//     "options": [
//         {
//             "label": "Date",
//             "value": "Date"
//         },
//         {
//             "label": "Working Day",
//             "value": "Working Day"
//         },
//         {
//             "label": "Day of the First",
//             "value": "Day of the First"
//         },
//         {
//             "label": "Week",
//             "value": "Week"
//         }
//     ]
// }