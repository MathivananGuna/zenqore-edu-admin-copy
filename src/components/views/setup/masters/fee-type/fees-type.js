import React, { Component } from "react";
import '../../../../../scss/fees-type.scss';
import '../../../../../scss/setup.scss';
import Loader from "../../../../../utils/loader/loaders";
import ContainerNavbar from "../../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../../utils/Table/table-component";
import PaginationUI from "../../../../../utils/pagination/pagination";
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import RestoreOutlinedIcon from '@material-ui/icons/RestoreOutlined';
import ZenForm from "../../../../../components/input/zqform";
import FeeTypeJson from './fees-types.json';
import AddNewFeeTypeJson from './new-fee-type.json';
import axios from "axios";
import moment from 'moment';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import DateFormatContext from '../../../../../gigaLayout/context';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
class ListofFeeTypes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            campusId: localStorage.getItem('campusId') === "null" || localStorage.getItem('campusId') === null ? undefined : localStorage.getItem('campusId'),
            containerNav: {
                isBack: false,
                name: "List of Fee Types",
                isName: true,
                isSearch: false,
                isSort: false,
                isPrint: true,
                isDownload: false,
                isShare: false,
                isNew: true,
                newName: "New",
                isSubmit: false,
            },
            sampleStudentsData: [],
            studentsFormjson: FeeTypeJson,
            addNewFeeTypejson: AddNewFeeTypeJson,
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 0,
            tabViewForm: false,
            studentName: "",
            tableResTxt: "Fetching Data...",
            viewType: '',
            PreviewData: "",
            snackbar: { openNotification: false, NotificationMessage: "", status: "success" },
        }
    }
    static contextType = DateFormatContext;
    componentDidMount() {
        this.getTableData()
    }
    getTableData = () => {
        this.setState({ sampleStudentsData: [] })
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/feeTypes?orgId=${this.state.orgId}&campusId=${this.state.campusId}&page=${this.state.page}&limit=${this.state.limit}`,
            headers: {
                'Authorization': this.state.authToken,
            }
        })
            .then(res => {
                if (res.data.status == "success") {
                    if (res.data.data.length == 0) {
                        this.setState({ tableResTxt: "No Data" })
                    }
                    else {
                        let paymantSchedule = [];
                        res.data.data.map((data) => {
                            paymantSchedule.push(
                                {
                                    "Id": data.displayName,
                                    "Title": data.title,
                                    "Description": data.description,
                                    "Campus Name": "-",
                                    "Role View": data.roleView ? data.roleView.length !== 0 ? data.roleView.map(item => item).join(", ") : "" : "",
                                    "Partial Allowed": data.partialAllowed,
                                    "CREATED ON": data.createdAt !== "" ? this.context.dateFormat !== "" ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                    "Created by": data.createdBy,
                                    "Status": data.status == 1 ? "Active" : "Inactive",
                                    "Item": JSON.stringify(data)
                                }
                            )
                        })
                        this.setState({ sampleStudentsData: paymantSchedule, totalPages: res.data.totalPages, page: res.data.currentPage })
                    }
                }
            })
            .catch(err => {
                console.log("error", err)
                this.setState({ tableResTxt: "Loading Error" })
            })
    }
    handleBackFun = () => {
        this.props.history.push(`${localStorage.getItem('baseURL')}/main/dashboard`);
    }
    onInputChanges = (e, a) => {
        if (e !== "") {
            let dataOne = this.state.addNewFeeTypejson;
            let dataTwo = this.state.studentsFormjson;
            dataOne.map((task) => {
                if (task['name'] == a.name) {
                    task['defaultValue'] = e
                    task['validation'] = false;
                    task['required'] = false
                }
            })
            dataTwo.map((task) => {
                if (task['name'] == a.name) {
                    task['defaultValue'] = e
                    task['validation'] = false;
                    task['required'] = false
                }
            })
            this.setState({ addNewFeeTypejson: dataOne, studentsFormjson: dataTwo })
        }
        else {
            let dataOne = this.state.addNewFeeTypejson;
            let dataTwo = this.state.studentsFormjson;
            dataOne.map((task) => {
                if (task['name'] == a.name) {
                    task['defaultValue'] = e
                    task['validation'] = false;
                    task['required'] = false
                    task['error'] = false
                }
            })
            dataTwo.map((task) => {
                if (task['name'] == a.name) {
                    task['defaultValue'] = e
                    task['validation'] = false;
                    task['required'] = false
                    task['error'] = false
                }
            })
            this.setState({ addNewFeeTypejson: dataOne, studentsFormjson: dataTwo })
        }
    }

    onSubmit = (e, formElts) => {
        if (this.state.viewType == "preview") {
            let newData = this.state.studentsFormjson; let apiCallData = true
            newData.map((item) => {
                if (item.requiredBoolean) {
                    if (item['defaultValue'] === "" || item['defaultValue'] === undefined) {
                        item["error"] = true;
                        item['errorMsg'] = `Invalid ${item.label}`
                        apiCallData = false
                    } else {
                        item["error"] = false;
                        item["validation"] = false;
                    }
                }
                // if (item.required) {
                //     if (!item.validation) {
                //         item["error"] = true;
                //         item['errorMsg'] = `Invalid ${item.label}`;
                //         apiCallData = false
                //     } else {
                //         item["error"] = false;
                //         item["validation"] = false;
                //     }
                // }
            })
            this.setState({ studentsFormjson: newData })
            if (apiCallData == true) {
                this.setState({ LoaderStatus: true })
                let bodyResData = {};
                newData.map((task) => {
                    if (task['name'] == 'feesId') {
                        bodyResData['displayName'] = task['defaultValue']
                    }
                    if (task['name'] == 'feesTitle') {
                        bodyResData['title'] = task['defaultValue']
                    }
                    if (task['name'] == 'feesDescription') {
                        bodyResData['description'] = task['defaultValue']
                    }
                    if (task['name'] == 'campusName') {
                        bodyResData['campusName'] = task['defaultValue']
                    }
                    if (task['name'] == 'roleView') {
                        bodyResData['roleView'] = task['defaultValue']
                    }
                    if (task['name'] == 'partialAllowed') {
                        bodyResData['partialAllowed'] = task['defaultValue']
                    }
                })
                let headers = {
                    'Authorization': this.state.authToken
                };
                let bodyData = {
                    "displayName": bodyResData.displayName,
                    "title": bodyResData.title,
                    "description": bodyResData.description,
                    // "campusName": bodyResData.campusName,
                    "roleView": [bodyResData.roleView],
                    "partialAllowed": bodyResData.partialAllowed,
                    "createdBy": this.state.orgId,
                    "orgId": this.state.orgId
                }
                axios.put(`${this.state.env["zqBaseUri"]}/edu/master/feeTypes?id=${this.state.PreviewData._id}`, bodyData, { headers })
                    .then(res => {
                        this.getTableData()
                        let a = this.state.snackbar;
                        a.openNotification = true;
                        a.NotificationMessage = "Updated Successfully";
                        a.status = "success";
                        this.setState({ LoaderStatus: false, tabViewForm: false, snackbar: a })
                        setTimeout(() => {
                            a.openNotification = false;
                            this.setState({ snackbar: a })
                        }, 2000)
                        this.resetForm("preview")
                    })
                    .catch(err => {
                        this.getTableData()
                        let a = this.state.snackbar;
                        a.openNotification = true;
                        a.NotificationMessage = "Failed to update";
                        a.status = "success";
                        this.setState({ LoaderStatus: false, tabViewForm: false, snackbar: a })
                        setTimeout(() => {
                            a.openNotification = false;
                            this.setState({ snackbar: a })
                        }, 2000)
                        this.resetForm("preview")
                    })
            }
            else { }
        } else if (this.state.viewType === "addNew") {
            let newData = this.state.addNewFeeTypejson; let apiNewCall = true
            newData.map((item) => {
                if (item.requiredBoolean) {
                    if (item['defaultValue'] === "" || item['defaultValue'] === undefined) {
                        item["error"] = true;
                        item['errorMsg'] = `Invalid ${item.label}`
                        apiNewCall = false
                    } else {
                        item["error"] = false;
                        item["validation"] = false;
                    }
                }
                // if (item.requiredBoolean) {
                //     if (!item.validation) {
                //         item["error"] = true;
                //         item['errorMsg'] = `Invalid ${item.label}`
                //         apiNewCall = false
                //     } else {
                //         item["error"] = false;
                //         item["validation"] = false;
                //     }
                // }
            })
            this.setState({ addNewFeeTypejson: newData })
            if (apiNewCall == true) {
                this.setState({ LoaderStatus: true })
                let bodyResData = {};
                newData.map((task) => {
                    if (task['name'] == 'feesId') {
                        bodyResData['displayName'] = task['defaultValue']
                    }
                    if (task['name'] == 'feesTitle') {
                        bodyResData['title'] = task['defaultValue']
                    }
                    if (task['name'] == 'feesDescription') {
                        bodyResData['description'] = task['defaultValue']
                    }
                    if (task['name'] == 'campusName') {
                        bodyResData['campusName'] = task['defaultValue']
                    }
                    if (task['name'] == 'roleView') {
                        bodyResData['roleView'] = task['defaultValue']
                    }
                    if (task['name'] == 'partialAllowed') {
                        bodyResData['partialAllowed'] = task['defaultValue']
                    }
                })
                let headers = {
                    'Authorization': this.state.authToken
                };
                let bodyData = {
                    "displayName": bodyResData.displayName,
                    "title": bodyResData.title,
                    "description": bodyResData.description,
                    // "campusName": bodyResData.campusName,
                    "roleView": [bodyResData.roleView],
                    "partialAllowed": bodyResData.partialAllowed,
                    "createdBy": this.state.orgId,
                    "orgId": this.state.orgId
                }
                axios.post(`${this.state.env["zqBaseUri"]}/edu/master/feeTypes`, bodyData, { headers })
                    .then(res => {
                        this.getTableData();
                        let a = this.state.snackbar;
                        a.openNotification = true;
                        a.NotificationMessage = "Added Successfully";
                        a.status = "success";
                        this.setState({ LoaderStatus: false, tabViewForm: false, snackbar: a })
                        setTimeout(() => {
                            a.openNotification = false;
                            this.setState({ snackbar: a })
                        }, 2000)
                        this.resetForm('addNew');
                    }).catch(err => {
                        this.getTableData();
                        let a = this.state.snackbar;
                        a.openNotification = true;
                        a.NotificationMessage = "Failed to Add";
                        a.status = "success";
                        this.setState({ LoaderStatus: false, tabViewForm: false, snackbar: a })
                        setTimeout(() => {
                            a.openNotification = false;
                            this.setState({ snackbar: a })
                        }, 2000)
                        this.resetForm('addNew');
                    })
            }
        }
    }
    formBtnHandle = (item) => {
        if (item !== undefined) {
            if (item["type"] === "cancel") {
                this.cancelViewData();
            }
        }
    }
    onPreviewStudentList = (e) => {
        let parsedValueData = JSON.parse(e.Item);
        this.setState({ tabViewForm: true, studentName: `Fee Types | ${e.Id}`, viewType: "preview", PreviewData: parsedValueData })
        FeeTypeJson.map((task) => {
            if (task['name'] == 'feesId') {
                task['defaultValue'] = parsedValueData.displayName
                task['readOnly'] = true
            }
            if (task['name'] == 'feesTitle') {
                task['defaultValue'] = parsedValueData.title
                task['readOnly'] = false
            }
            if (task['name'] == 'feesDescription') {
                task['defaultValue'] = parsedValueData.description
                task['readOnly'] = false
            }
            if (task['name'] == 'campusName') {
                task['defaultValue'] = "-"
                task['readOnly'] = false
            }
            if (task['name'] == 'roleView') {
                task['defaultValue'] = parsedValueData.roleView[0]
                task['readOnly'] = false
            }
            if (task['name'] == 'partialAllowed') {
                task['defaultValue'] = parsedValueData.partialAllowed
                task['readOnly'] = false
            }
            // if (task['name'] == 'feesUnits') {
            //     task['defaultValue'] = parsedValueData.unit
            //     task['readOnly'] = true
            // }
            // if (task['name'] == 'feesFrequency') {
            //     task['defaultValue'] = String(parsedValueData.frequency)
            //     task['readOnly'] = true
            // }
        })
    }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.getTableData()
        });
    };
    moveBackTable = () => {
        this.setState({ tabViewForm: false }, () => {
            this.resetForm('addNew');
            this.resetForm('preview');
        })
    }
    handleTabChange = (tabName, formDatas) => { }
    onFormSubmit = (data, item) => { }
    onAddBranches = (e) => {
        // this.resetForm("addNew")
        this.setState({ tabViewForm: true, studentName: "New Fee Type", viewType: "addNew", LoaderStatus: true }, () => {
            axios({
                method: 'get',
                url: `${this.state.env["zqBaseUri"]}/edu/master/getDisplayId/feeTypes?orgId=${this.state.orgId}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            }).then(res => {
                let newData = this.state.addNewFeeTypejson;
                newData.map(task => {
                    if (task['name'] == 'feesId') {
                        task['defaultValue'] = res.data.data
                        task['readOnly'] = true;
                        task['validation'] = false;
                        task['required'] = false;
                        task['requiredBoolean'] = true;
                    }
                })
                this.setState({ addNewFeeTypejson: newData, LoaderStatus: false, studentName: `New Fee Type | ${res.data.data}` })
            }).catch(err => { })
        })

    }
    selectRowNew = (e) => { }
    resetForm = (e) => {
        if (e == "addNew") {
            let newData = this.state.addNewFeeTypejson;
            newData.map((task) => {
                task["error"] = false;
                task['validation'] = false;
                task['errorMsg'] = "";
                task['defaultValue'] = "";
            })
            this.setState({ addNewFeeTypejson: newData })
        }
        else if (e == "preview") {
            let newData1 = this.state.studentsFormjson;
            newData1.map((task) => {
                task["error"] = false;
                task['validation'] = false;
                task['errorMsg'] = "";
                task['defaultValue'] = "";
            })
            this.setState({ studentsFormjson: newData1 })
        }
        else if (e == "both") {
            let newData = this.state.addNewFeeTypejson;
            newData.map((task) => {
                task["error"] = false;
                task['validation'] = false;
                task['errorMsg'] = "";
                task['defaultValue'] = "";
            })
            let newData1 = this.state.studentsFormjson;
            newData1.map((task) => {
                task["error"] = false;
                task['validation'] = false;
                task['errorMsg'] = "";
                task['defaultValue'] = "";
            })
            this.setState({ addNewFeeTypejson: newData, studentsFormjson: newData1 })
        }
    }
    cancelViewData = () => {
        this.setState({ tabViewForm: false });
        this.resetForm('preview');
        this.resetForm('addNew');
    }
    allSelect = () => { }
    printScreen = () => { }
    render() {
        return (
            <div className="list-of-feeType-mainDiv">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.tabViewForm == false ?
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title">| Fee Types</p>
                        </div>
                        <div className="masters-body-div">
                            {this.state.containerNav == undefined ? null :
                                <React.Fragment>
                                    <ContainerNavbar containerNav={this.state.containerNav} onAddNew={this.onAddBranches} printScreen={this.printScreen} />
                                    <div className="print-time">{this.state.printTime}</div>
                                </React.Fragment>}
                            <div className="remove-last-child-table">
                                {this.state.sampleStudentsData.length !== 0 ?
                                    <ZqTable
                                        allSelect={this.allSelect}
                                        data={this.state.sampleStudentsData}
                                        rowClick={(item) => { this.onPreviewStudentList(item) }}
                                        onRowCheckBox={(item) => { this.selectRowNew(item) }}
                                    /> :
                                    <p className="noprog-txt">{this.state.tableResTxt}</p>
                                }
                            </div>
                            <div>
                                {this.state.sampleStudentsData.length !== 0 ?
                                    <PaginationUI
                                        total={this.state.totalRecord}
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        currentPage={this.state.page}
                                    /> : null}
                            </div>
                        </div>
                    </React.Fragment> :
                    <React.Fragment>
                        <div className="tab-form-wrapper tab-table">
                            <div className="preview-btns">
                                <div className="preview-header-wrap">
                                    < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                    <h6 className="preview-Id-content" onClick={this.moveBackTable}>| {this.state.studentName}</h6>
                                </div>
                                {/* <div className="notes-history-wrapper">
                                    <div className="history-icon-box" title="View History" onClick={this.showHistory}> <RestoreOutlinedIcon className="material-historyIcon" /> <p>History</p></div>
                                </div> */}
                            </div>
                            <div className="goods-header">
                                <p className="info-input-label">BASIC DETAILS</p>
                            </div>
                            <div className="goods-wrapper goods-parent-wrap">
                                {this.state.viewType == "preview" ?
                                    <ZenForm
                                        formData={this.state.studentsFormjson}
                                        className="goods-wrap"
                                        onInputChanges={(e, a) => this.onInputChanges(e, a)}
                                        onFormBtnEvent={(item) => {
                                            this.formBtnHandle(item);
                                        }}
                                        clear={true}
                                        onSubmit={(e) => this.onSubmit("preview", e.target.elements)}
                                    /> : <ZenForm
                                        formData={this.state.addNewFeeTypejson}
                                        className="goods-wrap"
                                        onInputChanges={(e, a) => this.onInputChanges(e, a)}
                                        onFormBtnEvent={(item) => {
                                            this.formBtnHandle(item);
                                        }}
                                        clear={true}
                                        onSubmit={(e) => this.onSubmit("addNew", e.target.elements)}
                                    />}
                            </div>
                        </div>
                    </React.Fragment>
                }
                <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={2000} onClose={this.closeNotification}>
                    <Alert onClose={this.closeNotification}
                        severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
                        {this.state.snackbar.NotificationMessage}
                    </Alert>
                </Snackbar>
            </div>
        )
    }
}
export default ListofFeeTypes;