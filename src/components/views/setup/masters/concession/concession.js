import React, { Component } from "react";
import '../../../../../scss/student.scss';
import '../../../../../scss/setup.scss';
import Loader from "../../../../../utils/loader/loaders";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ContainerNavbar from "../../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../../utils/Table/table-component";
import PaginationUI from "../../../../../utils/pagination/pagination";
import ZenTabs from '../../../../../components/input/tabs';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import InstallmentJson from './concession-data.json';
import axios from "axios";
import moment from 'moment';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import DateFormatContext from '../../../../../gigaLayout/context';
import xlsx from 'xlsx';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
class Installment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            campusId: localStorage.getItem('campusId') === "null" || localStorage.getItem('campusId') === null ? undefined : localStorage.getItem('campusId'),
            containerNav: InstallmentJson.containerNav,
            studentsFormjson: InstallmentJson.studentsFormjson,
            sampleStudentsData: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 1,
            tabViewForm: false,
            studentName: "",
            tableResTxt: "Fetching Data...",
            viewHeader: true,
            totalApiRes: null,
            viewFormLoad: true,
            LoaderStatus: false,
            feesBreakUpArray: [],
            tableViewMode: "",
            payloadId: "",
            snackbar: { openNotification: false, NotificationMessage: "", status: "success" },
            numberOfIns: "1",
            frequencyData: "Week",
            dueDatas: "First Day",
            TotalCatData: "",
            concessionView: "",
            downloadTotalRecord: 0
        }
    }
    static contextType = DateFormatContext;
    componentDidMount() {
        this.getTableData()
    }
    getTableData = () => {
        this.setState({ sampleStudentsData: [] })
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/category?orgId=${this.state.orgId}&campusId=${this.state.campusId}`,
            headers: {
                'Authorization': this.state.authToken,
            }
        })
            .then(res => {
                this.setState({ TotalCatData: res.data.data })
            })
            .catch(err => { })
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/concession?orgId=${this.state.orgId}&campusId=${this.state.campusId}&page=${this.state.page}&limit=${this.state.limit}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                let installmentTableRes = [];
                if (res.data.status == "success") {
                    if (res.data.data.length == 0) {
                        this.setState({ tableResTxt: "No Data" })
                    }
                    else {
                        res.data.data.map((data) => {
                            installmentTableRes.push({
                                "Id": data.displayName,
                                "Title": data.title,
                                "Description": data.description,
                                "concession type": data.concessionType,
                                'concession value': data.concessionType == "Value" ? Number(data.concessionValue).toLocaleString('en-IN', { style: 'currency', currency: 'INR' }) : data.concessionType == "%" ? data.concessionValue : data.concessionValue,
                                'Campus Name': "-",
                                "Created on": data.createdAt !== "" ? this.context.dateFormat !== "" ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                "Created by": data.createdBy,
                                // "created on": data.createdAt !== "" ? moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                "Status": data.status == 1 ? "Active" : "Inactive",
                                "Item": JSON.stringify(data)
                            })
                        })
                        this.setState({ sampleStudentsData: installmentTableRes, totalApiRes: res.data.data, tabViewForm: false, totalRecord: res.data.totalRecord, totalPages: res.data.totalPages, page: res.data.currentPage, downloadTotalRecord: res.data.totalRecord })
                    }
                }
                else {
                    this.setState({ tableResTxt: "Loading error" })
                }
            })
            .catch(err => { this.setState({ tableResTxt: "Loading error" }) })
    }
    onAddInstallment = () => {
        this.setState({ tabViewForm: true, viewHeader: false, viewFormLoad: true, tableViewMode: "addnew", tableResTxt: "Fetching Data...", concessionView: "New Concession" }, () => {
            let installmentFormData = InstallmentJson.AddstudentsFormjson;
            axios({
                method: 'get',
                url: `${this.state.env["zqBaseUri"]}/edu/master/getDisplayId/concession?orgId=${this.state.orgId}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            })
                .then(res => {
                    this.resetform()
                    let optionsData = [];
                    this.state.TotalCatData.map((data) => {
                        optionsData.push({
                            "label": data.displayName,
                            "value": data._id
                        })
                    })
                    Object.keys(installmentFormData).forEach(tab => {
                        installmentFormData[tab].forEach(task => {
                            if (task['name'] == 'categoryId') {
                                task['defaultValue'] = String(res.data.data)
                                task['readOnly'] = true
                            }
                            if (task['name'] == 'categoryconcessionName') {
                                task['options'] = optionsData
                                task['readOnly'] = false
                            }
                            // if (task['name'] == 'categoryconcessionType') {
                            //     task['defaultValue'] = "%"
                            //     task['readOnly'] = false
                            // }
                            // if (task['name'] == 'categoryconcessionValueNew') {
                            //     task['label'] = "Value (%)"
                            //     task['readOnly'] = false
                            // }
                        })
                    })
                    this.setState({ studentsFormjson: installmentFormData, viewFormLoad: false, concessionView: `New Concession | ${res.data.data}` })
                })
                .catch(err => {
                    this.setState({ tableResTxt: "Loading error" })
                })
        })
    }
    handleBackFun = () => {
        this.props.history.push(`${localStorage.getItem('baseURL')}/main/dashboard`);
        this.resetform()
    }
    onPreviewStudentList = (e) => {
        let details = this.state.globalSearch ? e : JSON.parse(e.Item)
        let installmentFormData = InstallmentJson.studentsFormjson;
        let newCatId = []
        this.state.TotalCatData.map((data) => {
            newCatId.push({
                "label": data.displayName,
                "value": data._id
            })
        })
        this.state.totalApiRes.map((data) => {
            if (details.displayName === e.Id) {
                this.setState({ tabViewForm: true, studentName: e.ID, viewHeader: true, viewFormLoad: true, tableViewMode: "preview", payloadId: details['_id'] })
                Object.keys(installmentFormData).forEach(tab => {
                    installmentFormData[tab].forEach(task => {
                        if (task['name'] == 'categoryId') {
                            task['defaultValue'] = details.displayName
                            task['readOnly'] = true
                            task['required'] = false
                        }
                        else if (task['name'] == 'categoryTitle') {
                            task['defaultValue'] = details.title
                            task['readOnly'] = false
                            task['required'] = false
                        }
                        else if (task['name'] == 'categoryDescription') {
                            task['defaultValue'] = details.description
                            task['readOnly'] = false
                            task['required'] = false
                        }
                        else if (task['name'] == 'campusName') {
                            task['defaultValue'] = '-'
                            task['readOnly'] = false
                            task['required'] = false
                        }
                        else if (task['name'] == 'categoryconcessionType') {
                            task['defaultValue'] = details.concessionType
                            task['readOnly'] = false
                            task['required'] = false
                        }
                        else if (task['name'] == 'categoryconcessionValueNew') {
                            task['defaultValue'] = details.concessionType == "%" ? Number(details.concessionValue).toFixed(0) : details.concessionType == "value" ? Number(details.concessionValue).toFixed(2) : null
                            task['readOnly'] = false
                            task['required'] = false
                            task['label'] = details.concessionType == "%" ? "Value (%)" : "Value (₹)"
                        }
                        if (task['name'] == 'categoryconcessionName') {
                            task['options'] = newCatId;
                            task['defaultValue'] = details.categoryId;
                            task['readOnly'] = false;
                            task['required'] = false;
                            task['requiredBoolean'] = false;
                        }
                    })
                })
                this.setState({ studentsFormjson: installmentFormData, viewFormLoad: false, concessionView: `Concessions | ${e.Id}` })
            }
            else { }
        })
    }
    onInputChanges = (value, item, event, dataS) => {
        console.log(dataS);
        // debugger
        let installmentFormData = InstallmentJson.studentsFormjson;
        let installmentFormData1 = InstallmentJson.AddstudentsFormjson;
        if (dataS !== undefined) {
            if (dataS.defaultValue !== undefined) {
                if (dataS.name == 'categoryconcessionType') {
                    if (dataS.defaultValue.toLowerCase() == "value") {
                        Object.keys(installmentFormData).forEach(tab => {
                            installmentFormData[tab].forEach(task => {
                                if (task['name'] == 'categoryconcessionValueNew') {
                                    task['label'] = "Value (%)";
                                    // delete task['defaultValue']
                                    task['defaultValue'] = Number(task['defaultValue'])
                                }
                            })
                        })
                        Object.keys(installmentFormData1).forEach(tab => {
                            installmentFormData1[tab].forEach(task => {
                                if (task['name'] == 'categoryconcessionValueNew') {
                                    task['label'] = "Value (%)";
                                    // delete task['defaultValue']
                                    task['defaultValue'] = Number(task['defaultValue'])
                                }
                            })
                        })
                    }
                    else if (dataS.defaultValue.toLowerCase() == "percentage" || dataS.defaultValue == "%") {
                        Object.keys(installmentFormData1).forEach(tab => {
                            installmentFormData1[tab].forEach(task => {
                                if (task['name'] == 'categoryconcessionValueNew') {
                                    task['label'] = "Value (₹)";
                                    // delete task['defaultValue']
                                    task['defaultValue'] = Number(task['defaultValue'])
                                }
                            })
                        })
                        Object.keys(installmentFormData).forEach(tab => {
                            installmentFormData[tab].forEach(task => {
                                if (task['name'] == 'categoryconcessionValueNew') {
                                    task['label'] = "Value (₹)";
                                    // delete task['defaultValue']
                                    task['defaultValue'] = Number(task['defaultValue'])
                                }
                            })
                        })
                    }
                }
            }
            this.setState({ studentsFormjson: installmentFormData })
        }
    }
    onPreviewStudentList1 = (e) => { }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.getTableData()
        });
    };
    moveBackTable = () => {
        this.setState({ tabViewForm: false })
        this.resetform()
        this.getTableData()
    }
    handleTabChange = (tabName, formDatas) => {
        let noOfIns, freqData, dueDate;
        if (tabName == 2) {
            formDatas["General"].map((data) => {
                if (data.name == "noOfInstallments") {
                    noOfIns = data.defaultValue
                }
            })
            formDatas["Schedule Details"].map((data) => {
                if (data.name == "frequencySchedule") {
                    freqData = data.defaultValue
                }
                else if (data.name == "byDueDate") {
                    dueDate = data.defaultValue
                }
            })
        }
    }
    onFormSubmit = (data, item) => {
        this.setState({ LoaderStatus: true })
        if (this.state.tableViewMode == "addnew") {
            let payloadObj = {};
            let installmentFormData = InstallmentJson.AddstudentsFormjson;
            Object.keys(installmentFormData).forEach(tab => {
                installmentFormData[tab].forEach(item => {
                    if (item.name == "categoryId") {
                        payloadObj['id'] = item['defaultValue'];
                    }
                    if (item.name == "categoryTitle") {
                        payloadObj['title'] = item['defaultValue'];
                    }
                    if (item.name == "categoryDescription") {
                        payloadObj['description'] = item['defaultValue'];
                    }
                    if (item.name == "campusName") {
                        payloadObj['campusName'] = item['defaultValue'];
                    }
                    else if (item['name'] == 'categoryconcessionType') {
                        payloadObj['type'] = item['defaultValue']
                    }
                    else if (item['name'] == 'categoryconcessionValueNew') {
                        if (payloadObj['type'] == "value") {
                            payloadObj['value'] = Number(item['defaultValue']).toFixed(2)
                        }
                        else {
                            payloadObj['value'] = Number(item['defaultValue']).toFixed(0)
                        }
                    }
                    else if (item['name'] == 'categoryconcessionName') {
                        payloadObj['name'] = item['defaultValue']
                    }
                })
            })
            this.state.TotalCatData.map((dataOne) => {
                if (dataOne.displayName == payloadObj.name) {
                    payloadObj['categoryId'] = dataOne._id
                }
            })
            let payload = {
                "displayName": payloadObj.id,
                "title": payloadObj.title,
                "description": payloadObj.description,
                "categoryId": payloadObj.name,
                "concessionType": payloadObj.type,
                "concessionValue": Number(payloadObj.value),
                // "campusName": payloadObj.campusName,
                "createdBy": this.state.orgId,
                "orgId": this.state.orgId
            }
            let headers = {
                'Authorization': this.state.authToken
            };
            axios.post(`${this.state.env["zqBaseUri"]}/edu/master/concession`, payload, { headers })
                .then(res => {
                    if (res.data.status == "Database error") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: res.data.message, status: "error", }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate, tabViewForm: false });
                        this.resetform();
                        setTimeout(() => {
                            snackbarUpdate.openNotification = false
                            this.setState({ tabViewForm: false, snackbar: snackbarUpdate })
                        }, 1500)
                        this.getTableData();
                    }
                    else if (res.data.status == "success") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Added Successfully", status: "success", }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate, tabViewForm: false });
                        this.resetform();
                        setTimeout(() => {
                            snackbarUpdate.openNotification = false
                            this.setState({ tabViewForm: false, snackbar: snackbarUpdate })
                        }, 1500)
                        this.getTableData();
                    }
                    else if (res.data.status == "error") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: res.data.message, status: "error", }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate, tabViewForm: false });
                        this.resetform();
                        setTimeout(() => {
                            snackbarUpdate.openNotification = false
                            this.setState({ tabViewForm: false, snackbar: snackbarUpdate })
                        }, 1500)
                        this.getTableData();
                    }
                })
                .catch(err => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Error", status: "error" }
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate, tabViewForm: false });
                })
        }
        else if (this.state.tableViewMode == "preview") {
            let payloadObj = {};
            // let scheduleData = data['Concession Details']
            let installmentFormData = data;
            Object.keys(installmentFormData).forEach(tab => {
                installmentFormData[tab].forEach(item => {
                    if (item.name == "categoryId") {
                        payloadObj['id'] = item['defaultValue'];
                    }
                    if (item.name == "categoryTitle") {
                        payloadObj['title'] = item['defaultValue'];
                    }
                    if (item.name == "categoryDescription") {
                        payloadObj['description'] = item['defaultValue'];
                    }
                    if (item.name == "campusName") {
                        payloadObj['campusName'] = item['defaultValue'];
                    }
                    else if (item['name'] == 'categoryconcessionType') {
                        payloadObj['type'] = item['defaultValue']
                    }
                    else if (item['name'] == 'categoryconcessionValueNew') {
                        if (payloadObj['type'] == "%") {
                            payloadObj['value'] = Number(item['defaultValue']).toFixed(0)
                        }
                        else {
                            payloadObj['value'] = Number(item['defaultValue']).toFixed(2)
                        }
                    }
                    else if (item['name'] == 'categoryconcessionName') {
                        payloadObj['name'] = item['defaultValue']
                    }
                })
            })
            // scheduleData.map((item) => {
            //     if (item.name == "categoryId") {
            //         payloadObj['id'] = item['defaultValue'];
            //     }
            //     if (item.name == "categoryTitle") {
            //         payloadObj['title'] = item['defaultValue'];
            //     }
            //     if (item.name == "categoryDescription") {
            //         payloadObj['description'] = item['defaultValue'];
            //     }
            //     else if (item['name'] == 'categoryconcessionType') {
            //         payloadObj['type'] = item['defaultValue']
            //     }
            //     else if (item['name'] == 'categoryconcessionValueNew') {
            //         if (payloadObj['type'] == "%") {
            //             payloadObj['value'] = Number(item['defaultValue']).toFixed(0)
            //         }
            //         else {
            //             payloadObj['value'] = Number(item['defaultValue']).toFixed(2)
            //         }
            //     }
            //     else if (item['name'] == 'categoryconcessionName') {
            //         payloadObj['name'] = item['defaultValue']
            //     }
            // })
            this.state.TotalCatData.map((dataOne) => {
                if (dataOne.displayName == payloadObj.name) {
                    payloadObj['categoryId'] = dataOne._id
                }
            })
            let payload = {
                "displayName": payloadObj.id,
                "title": payloadObj.title,
                "description": payloadObj.description == undefined || payloadObj.description == null ? "-" : payloadObj.description,
                "categoryId": payloadObj.name,
                "concessionType": payloadObj.type,
                "concessionValue": Number(payloadObj.value),
                // "campusName":payloadObj.campusName,
                "createdBy": this.state.orgId,
                "orgId": this.state.orgId
            }
            let headers = {
                'Authorization': this.state.authToken
            };
            axios.put(`${this.state.env["zqBaseUri"]}/edu/master/concession?id=${this.state.payloadId}`, payload, { headers })
                .then(res => {
                    if (res.data.status == "Database error") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: res.data.message, status: "error" }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                        setTimeout(() => {
                            snackbarUpdate.openNotification = false
                            this.setState({ tabViewForm: false, snackbar: snackbarUpdate })
                        }, 1500)
                        this.resetform();
                        this.getTableData();
                    }
                    else if (res.data.status == "success") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Updated Successfully", status: "success" }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                        setTimeout(() => {
                            snackbarUpdate.openNotification = false
                            this.setState({ tabViewForm: false, snackbar: snackbarUpdate })
                        }, 1500)
                        this.resetform();
                        this.getTableData();
                    }
                    else if (res.data.status == "error") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: res.data.message, status: "error", }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate, tabViewForm: false });
                        this.resetform();
                        setTimeout(() => {
                            snackbarUpdate.openNotification = false
                            this.setState({ tabViewForm: false, snackbar: snackbarUpdate })
                        }, 1500)
                        this.getTableData();
                    }
                })
                .catch(err => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Error", status: "error" }
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate, tabViewForm: false });
                })
        }
    }
    closeNotification = () => {
        let snackbarUpdate = { openNotification: false, NotificationMessage: "", status: "" }
        this.setState({ snackbar: snackbarUpdate })
    }
    resetform = () => {
        let installmentFormData = InstallmentJson.studentsFormjson;
        let installmentFormData1 = InstallmentJson.AddstudentsFormjson;
        Object.keys(installmentFormData).forEach(tab => {
            installmentFormData[tab].forEach(task => {
                delete task['defaultValue']
                task['readOnly'] = false
                task['error'] = false
                task['required'] = task['requiredBoolean']
                task['validation'] = false
                delete task['clear']
            })
        })
        Object.keys(installmentFormData1).forEach(tab => {
            installmentFormData1[tab].forEach(task => {
                delete task['defaultValue']
                task['readOnly'] = false
                task['error'] = false
                task['required'] = task['requiredBoolean']
                task['validation'] = false
                delete task['clear']
            })
        })
        this.setState({ studentsFormjson: installmentFormData, payloadId: "", status: "" })
    }
    cancelViewData = () => {
        this.setState({ tabViewForm: false });
        this.resetform();
    }
    cleanDatas = () => { }
    allSelect = () => { }
    searchHandle = (value) => { }
    printScreen = () => {
        let page = 1
        let limit = this.state.downloadTotalRecord || 1
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/concession?orgId=${this.state.orgId}&page=${page}&limit=${limit}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                this.setState({ sampleStudentsData: [] })
                let installmentTableRes = [];
                if (res.data.status == "success") {
                    if (res.data.data.length == 0) {
                        this.setState({ tableResTxt: "No Data" })
                    }
                    else {
                        res.data.data.map((data) => {
                            installmentTableRes.push({
                                "Id": data.displayName,
                                "Title": data.title,
                                "Description": data.description,
                                "concession type": data.concessionType,
                                'concession value': data.concessionType == "Value" ? Number(data.concessionValue).toLocaleString('en-IN', { style: 'currency', currency: 'INR' }) : data.concessionType == "%" ? data.concessionValue : data.concessionValue,
                                'Campus Name': "-",
                                "Created on": data.createdAt !== "" ? this.context.dateFormat !== "" ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                "Created by": data.createdBy,
                                "Status": data.status == 1 ? "Active" : "Inactive",
                            })
                        })
                        this.setState({ sampleStudentsData: installmentTableRes }, () => {
                            setTimeout(() => {
                                window.print();
                                this.getTableData();
                            }, 1);
                        })
                    }
                }
            })
            .catch(err => {
                console.log("error", err)
            })
    }
    onDownload = () => {
        let createXlxsData = [];
        let page = 1
        let limit = this.state.downloadTotalRecord
        this.setState({ LoaderStatus: true }, () => {
            axios({
                method: 'get',
                url: `${this.state.env["zqBaseUri"]}/edu/master/concession?orgId=${this.state.orgId}&page=${page}&limit=${limit}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            })
                .then(res => {
                    if (res.data.status == "success") {
                        if (res.data.data.length !== 0) {
                            res.data.data.map((data) => {
                                createXlxsData.push({
                                    "Id": data.displayName,
                                    "Title": data.title,
                                    "Description": data.description,
                                    "concession type": data.concessionType,
                                    'concession value': data.concessionType == "Value" ? Number(data.concessionValue).toLocaleString('en-IN', { style: 'currency', currency: 'INR' }) : data.concessionType == "%" ? data.concessionValue : data.concessionValue,
                                    'Campus Name': "-",
                                    "Created on": data.createdAt !== "" ? this.context.dateFormat !== "" ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                    "Created by": data.createdBy,
                                    "Status": data.status == 1 ? "Active" : "Inactive",
                                })
                            })
                        }
                        var ws = xlsx.utils.json_to_sheet(createXlxsData);
                        var wb = xlsx.utils.book_new();
                        xlsx.utils.book_append_sheet(wb, ws, "Concession");
                        xlsx.writeFile(wb, "Concession.xlsx");
                        this.setState({ LoaderStatus: false })
                    }
                })
                .catch(err => {
                    console.log(err, 'errorrrrr')
                    this.setState({ LoaderStatus: false })
                })
        })
    }
    render() {
        return (
            <div className="list-of-students-mainDiv form-btn-con table-head-padding">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.tabViewForm == false ?
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Concessions</p>
                        </div>
                        <div className="masters-body-div">
                            {this.state.containerNav == undefined ? null :
                                <React.Fragment>
                                    <ContainerNavbar containerNav={this.state.containerNav} onAddNew={() => { this.onAddInstallment(); }} searchValue={(searchValue) => this.searchHandle(searchValue)} onDownload={this.onDownload} printScreen={this.printScreen} />
                                    <div className="print-time">{this.state.printTime}</div>
                                </React.Fragment>}
                            <div className="remove-last-child-table">
                                {this.state.sampleStudentsData.length !== 0 ?
                                    <ZqTable
                                        allSelect={this.allSelect}
                                        data={this.state.sampleStudentsData}
                                        rowClick={(item) => { this.onPreviewStudentList(item) }}
                                        onRowCheckBox={(item) => { this.onPreviewStudentList1(item) }}
                                    />
                                    : <p className="noprog-txt">{this.state.tableResTxt}</p>
                                }
                            </div>
                            <div>
                                {this.state.sampleStudentsData.length !== 0 ?
                                    <PaginationUI
                                        total={this.state.totalRecord}
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        currentPage={this.state.page}
                                    />
                                    : null}
                            </div>
                        </div>
                    </React.Fragment> :
                    <React.Fragment>
                        <div className="tab-form-wrapper tab-table">
                            <div className="preview-btns">
                                {this.state.viewHeader == true ?
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| {this.state.concessionView}</h6>
                                    </div> :
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| {this.state.concessionView}</h6>
                                    </div>
                                }
                            </div>
                            <div className="organisation-table">
                                {this.state.viewFormLoad === false ?
                                    <ZenTabs tabData={this.state.studentsFormjson} className="preview-wrap" cleanData={this.cleanDatas} tabEdit={this.state.preview} cancelViewData={this.cancelViewData} form={this.state.studentsFormjson} value={0} onInputChanges={this.onInputChanges} onFormBtnEvent={(item) => { this.formBtnHandle(item); }} onTabFormSubmit={this.onFormSubmit} handleTabChange={this.handleTabChange} key={0} />
                                    : <p className="noprog-txt">{this.state.tableResTxt}</p>}
                            </div>
                        </div>
                    </React.Fragment>
                }
                <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={2500} onClose={this.closeNotification}>
                    <Alert onClose={this.closeNotification}
                        severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
                        {this.state.snackbar.NotificationMessage}
                    </Alert>
                </Snackbar>
            </div>
        )
    }
}
export default Installment;