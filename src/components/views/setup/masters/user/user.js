import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import '../../../../../scss/common.scss';
import '../../../../../scss/user.scss';
import '../../../../../scss/fees-type.scss';
import '../../../../../scss/setup.scss';
import '../../../../../scss/common-table.scss';
import Loader from "../../../../../utils/loader/loaders";
import ContainerNavbar from "../../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../../utils/Table/table-component";
import PaginationUI from "../../../../../utils/pagination/pagination";
import axios from "axios";
import moment from 'moment';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import UserData from './add-user.json';
import ZenForm from "../../../../../components/input/form";
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import DateFormatContext from '../../../../../gigaLayout/context';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
class User extends Component {
    constructor(props) {
        super(props)
        this.state = {
            sampleUserData: [],
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            baseURL: String(localStorage.getItem('baseURL')).toUpperCase(),
            tableResTxt: 'Fetching Data',
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 1,
            containerNav: UserData.containerNav,
            userForm: UserData.userJSON["User Details"],
            tabView: false,
            newUser: false,
            isLoader: false,
            viewType: "",
            role: '',
            snackbar: { openNotification: false, NotificationMessage: "", status: "success" },
            previewEmail: "",
            activeStatus: "",
            previewpage: 1,
            previewlimit: 10,
            previewSampleUserData: [],
            previewtotalRecord: 0,
            previewtotalPages: 1,
            parsedValueData: {},
            totalPreviewRecord: 0
        }
    }
    componentDidMount() {
        this.getTableData();
    }

    getTableData = () => {
        this.setState({ sampleUserData: UserData.sampleUserList });
        // let payloadInsName = String(localStorage.getItem('baseURL')).toUpperCase()
        // axios({
        //     method: 'get',
        //     url: `${this.state.env["ken42"]}/zq/user/listuser?institute=${payloadInsName}`,
        //     headers: {
        //         'Authorization': this.state.authToken
        //     }
        // })
        //     .then(res => {
        //         this.setState({ isLoader: true });
        //         console.log(res);
        //         let sampleUserData = [];
        //         res.data.data.map((dataOne, i) => {

        //             if (res.data.message == "success") {
        //                 if (res.data.data.length == 0) {
        //                     this.setState({ tableResTxt: "No Data" })
        //                 }
        //                 else {
        //                     let createdOnDate = String(dataOne.registerDate === undefined ? dataOne.register_date : dataOne.registerDate).split('@');
        //                     let userId = `USR${String(i + 1).length == 1 ? `00${(i + 1)}` : (String(i + 1).length == 2 ? `0${(i + 1)}` : (i + 1))}`
        //                     this.state.baseURL === "MNRGROUP" || this.state.baseURL === "VKGI" ?
        //                         sampleUserData.push({
        //                             "ID": userId,
        //                             "Name": `${dataOne.firstName}  ${dataOne.lastName}`,
        //                             "Email": dataOne.email,
        //                             "franchise":"Zenqore Education",
        //                             "Role": dataOne.role,
        //                             "campus": dataOne.campusDisplayId.toUpperCase() || "-",
        //                             "Contact No.": dataOne.mobileNumber,
        //                             "Created on": createdOnDate[0],
        //                             "Status": dataOne.accountStatus == "Invitation-send" ? "Pending" : (dataOne.accountStatus == null ? 'Pending' : dataOne.accountStatus),
        //                             "action": dataOne.accountStatus == "Invitation-send" ? [{ "name": "Resend Invitation" }] : null || dataOne.accountStatus == "Active" ? [{ "name": "Deactivate" }] : null || dataOne.accountStatus == "Deactive" ? [{ "name": "Activate" }] : null || dataOne.accountStatus == null ? [{ "name": "Resend Invitation" }] : null,
        //                             "Item": JSON.stringify(dataOne)
        //                         })
        //                         :
        //                         sampleUserData.push({
        //                             "ID": userId,
        //                             "Name": `${dataOne.firstName}  ${dataOne.lastName}`,
        //                             "Email": dataOne.email,
        //                             "Franchise":"Zenqore Education",
        //                             "Role": dataOne.role,
        //                             "Campus":"All",
        //                             "Contact No.": dataOne.mobileNumber,
        //                             "Created on": createdOnDate[0],
        //                             "Status": dataOne.accountStatus == "Invitation-send" ? "Pending" : (dataOne.accountStatus == null ? 'Pending' : dataOne.accountStatus),
        //                             "action": dataOne.accountStatus == "Invitation-send" ? [{ "name": "Resend Invitation" }] : null || dataOne.accountStatus == "Active" ? [{ "name": "Deactivate" }] : null || dataOne.accountStatus == "Deactive" ? [{ "name": "Activate" }] : null || dataOne.accountStatus == null ? [{ "name": "Resend Invitation" }] : null,
        //                             "Item": JSON.stringify(dataOne)
        //                         })

        //                 }

        //             }
        //             else {
        //                 this.setState({ tableResTxt: "Loading Error" })
        //             }
        //         })
        //         localStorage.setItem('sampleUserData', JSON.stringify(sampleUserData))
        //         this.setState({ activeStatus: res.data.data.accountStatus, totalRecord: res.data.totalRecord, totalPages: res.data.totalPages, page: res.data.page });

        //         let current_page = Number(this.state.previewpage);
        //         let perPage = Number(this.state.previewlimit);
        //         let total_pages = Math.ceil(sampleUserData.length / perPage);

        //         current_page = total_pages < current_page ? total_pages : current_page
        //         // let offset = (current_page - 1) * perPage;
        //         let lastIndex = current_page * perPage;
        //         let startIndex = lastIndex - perPage;
        //         let paginatedItems = sampleUserData.slice(startIndex, lastIndex)
        //         this.setState({
        //             sampleUserData: paginatedItems,
        //             previewtotalPages: total_pages,
        //             previewtotalRecord: sampleUserData.length
        //         }, () => {
        //             this.setState({ isLoader: false });
        //         });
        //     })
        //     .catch(err => { console.log(err) })



    }
    onAddNew = () => {
        this.resetForm('addNew');
        this.setState({ tabView: true, viewType: "addNew", newUser: true, isLoader: false });
        this.state.userForm.map((task) => {
            if (task['name'] == task.name) {
                task['defaultValue'] = ''
                task['readOnly'] = false
            }
        })
        this.setState({ isLoader: false });
    }
    onPreviewUserList = (item) => {
        console.log("item", item)
        this.resetForm('preview');
        this.setState({ tabView: true, newUser: false, viewType: "preview", isLoader: false });
        let parsedValueData = JSON.parse(item.Item);
        console.log("parsedValueData", parsedValueData)
        this.setState({ previewEmail: item.Email, activeStatus: parsedValueData.accountStatus, parsedValueData })
        this.state.userForm.map((task) => {
            if (task['name'] == 'firstname') {
                task['defaultValue'] = parsedValueData.firstName
                task['readOnly'] = false
                task["required"] = false;
            }
            if (task['name'] == 'lastname') {
                task['defaultValue'] = parsedValueData.lastName
                task['readOnly'] = false
                task["required"] = false;
            }
            if (task['name'] == 'role') {
                task['defaultValue'] = item.Role
                task['readOnly'] = false;
                task["required"] = false;
            }
            if (task['name'] == 'campus') {
                task['defaultValue'] = item.campus
                task['readOnly'] = false;
                task["required"] = false;
            }

            if (task['name'] == 'email') {
                task['defaultValue'] = item.Email
                task['readOnly'] = true;
                task["required"] = false;
            }
            if (task['name'] == 'phonenumber1') {
                task['defaultValue'] = item["Contact No."]
                task['readOnly'] = false
                task["required"] = false;
            }
            if (task['name'] == 'secondaryEmail') {
                task['defaultValue'] = parsedValueData["secondaryEmail"]
                task['readOnly'] = false
                task["required"] = false;
            }
        })
        this.setState({ isLoader: false });
    }
    onInputChanges = (e, a) => {
        console.log("e", e)
        console.log("a", a)
        this.state.userForm.map((task) => {
            if (a['name'] === 'firstname') {
                a['defaultValue'] = e
            }
            if (a['name'] === 'lastname') {
                a['defaultValue'] = e
            }
            if (a['name'] === 'role') {
                a['defaultValue'] = e
            }
            if (a['name'] === 'campus') {
                a['defaultValue'] = e
            }
            if (a['name'] === 'email') {
                a['defaultValue'] = e
            }
            if (a['name'] === 'phonenumber1') {
                a['defaultValue'] = e
            }
            if (a['name'] === 'phonenumber2') {
                a['defaultValue'] = e
                a['type'] = "number"
            }
            if (a['name'] === 'secondaryEmail') {
                a['defaultValue'] = e
            }
        })
        console.log("form", this.state.userForm)
    }
    onRowCheckBox = (item) => {
        console.log("item", item)
    }
    selectAllData = (allItems, a) => { }

    handleActionClick = (item, index, hd, name) => {
        if (name == "Deactivate") {
            this.onDeactivateUser(item)
        }
        else if (name == "Activate") {
            this.onActivateUser(item)
        }
        else if (name == "Resend Invitation") {
            this.onResendInvitition(item)
        }
    }

    onResendInvitition = async (item) => {
        console.log(item)
        let payload = {
            "userName": item.Email,
            "orgName": localStorage.getItem('baseURL'),
            "firstName": JSON.parse(item.Item).firstName,
            "lastName": JSON.parse(item.Item).lastName,
        }
        try {
            const response = await axios.post(`${this.state.env['ken42']}/zq/user/resendinvite`, payload)
            console.log(response)
            let snackbarUpdate = { openNotification: true, NotificationMessage: response.data.message, status: "success", }
            this.setState({ snackbar: snackbarUpdate }, () => {
                setTimeout(() => {
                    snackbarUpdate.openNotification = false
                    this.setState({ snackbar: snackbarUpdate })
                }, 1500)
            })



        }
        catch (err) {
            // Alert.error(err)
            console.log(err)
        }


    }
    resetForm = (e) => {
        if (e == "addNew") {
            let newData = this.state.userForm;
            newData.map((task) => {
                task["error"] = false;
                task['validation'] = false;
                task['errorMsg'] = "";
                task['defaultValue'] = "";
            })
            this.setState({ userForm: newData })
        }
        else if (e == "preview") {
            let newData1 = this.state.userForm;
            newData1.map((task) => {
                task["error"] = false;
                task['validation'] = false;
                task['errorMsg'] = "";
                task['defaultValue'] = "";
            })
            this.setState({ userForm: newData1 })
        }
    }

    // handleActionClick = (item, index, hd, name) => {
    //     debugger
    // let rowValue = item;
    // this.setState({ rowValue: rowValue })
    // if (name == "Deactivate") {
    //     this.onDeactivateUser(item)
    // } else if (name == "Accept") {
    //     this.onPreviewUsers(item)
    //     this.setState({ onAccept: true, editPreview: false })
    // } else if (name == "Reject") {
    //     this.onRejectUser(item)
    // } else if (name == "Remove") {
    //     this.onRemoveUsers(item)
    // } else if (name == "Activate") {
    //     this.onActivateUsers(item)
    // }
    // }
    onSubmit = (inputData, item) => {
        console.log("inputdata", inputData)

        if (this.state.viewType === "addNew") {
            let apiNewCall = true;
            let newUserData = this.state.userForm;
            newUserData.map(item => {
                if (item.requiredBoolean) {
                    if (item['defaultValue'] === "" || item['defaultValue'] === undefined) {
                        item["error"] = true;
                        item["validation"] = true
                        item['errorMsg'] = `Invalid ${item.label}`
                        apiNewCall = false
                    } else {
                        item["error"] = false;
                        item["validation"] = false;
                    }
                }
            })

            let bodyResData = {};
            newUserData.map((task) => {
                if (task['name'] == 'role') {
                    bodyResData['type'] = task['defaultValue']
                }
            })
            let postBodyData = {
                "firstName": inputData.firstname.value,
                "lastName": inputData.lastname.value,
                "organiZation": this.state.baseURL,
                "orgId": this.state.orgId,
                "role": bodyResData.type,
                "email": inputData.email.value,
                "secondaryEmail": inputData.secondaryEmail.value,
                "mobileNumber": inputData.phonenumber1.value,
                "accountStatus": "Active",
                "campusDisplayId": null,
                "campusId": null,

            }
            //     organiZation: req.body.organizationName,
            //     campusDisplayId: req.body.campusDisplayId,
            //     campusId: req.body.campusId,
            console.log('postBodyData', postBodyData)
            this.setState({ userForm: newUserData })

            if (apiNewCall == true) {
                this.setState({ isLoader: true })

                axios.post(`${this.state.env["ken42"]}/zq/user/usersdata`, postBodyData)
                    .then(res => {
                        console.log("postres", res)
                        this.getTableData();
                        let a = this.state.snackbar;
                        a.openNotification = true;
                        a.NotificationMessage = "User has been Added successfully";
                        a.status = "success";
                        this.setState({ tabView: false, isLoader: false, snackbar: a })
                        setTimeout(() => {
                            a.openNotification = false;
                            this.setState({ snackbar: a })
                        }, 2000)
                        this.resetForm('addNew');


                    }).catch(err => {
                        console.log("error", err)
                        this.getTableData();
                        let a = this.state.snackbar;
                        a.openNotification = true;
                        a.NotificationMessage = "Failed to Add the User";
                        a.status = "success";
                        this.setState({ isLoader: false, tabView: false, snackbar: a })
                        setTimeout(() => {
                            a.openNotification = false;
                            this.setState({ snackbar: a })
                        }, 2000)
                        this.resetForm('addNew');
                    })
            }

        }
        else {
            let apiNewCall = true;
            let newUserData = this.state.userForm;
            newUserData.map(item => {
                if (item.requiredBoolean) {
                    if (item['defaultValue'] === "" || item['defaultValue'] === undefined) {
                        item["error"] = true;
                        item["validation"] = true
                        item['errorMsg'] = `Invalid ${item.label}`
                        apiNewCall = false
                    } else {
                        item["error"] = false;
                        item["validation"] = false;
                    }
                }
            })

            let bodyResData = {};
            newUserData.map((task) => {
                if (task['name'] == 'role') {
                    bodyResData['type'] = task['defaultValue']
                }
            })

            let putBodyData = {
                "firstName": inputData.firstname.value,
                "lastName": inputData.lastname.value,
                "role": bodyResData.type,
                "secondaryEmail": inputData.secondaryEmail.value,
                "mobileNumber": inputData.phonenumber1.value,
                "accountStatus": this.state.activeStatus,
                "campusDisplayId": null,
                "campusId": null,
            }

            console.log("putBodyData", putBodyData)
            this.setState({ userForm: newUserData })

            if (apiNewCall == true) {
                this.setState({ isLoader: true })
                axios.put(`${this.state.env["ken42"]}/zq/user/usersdata?userName=${this.state.previewEmail}`, putBodyData)
                    .then(res => {
                        console.log("res", res)
                        this.getTableData();
                        let a = this.state.snackbar;
                        a.openNotification = true;
                        a.NotificationMessage = "User has been Updated successfully";
                        a.status = "success";
                        this.setState({ tabView: false, isLoader: false, snackbar: a })
                        setTimeout(() => {
                            a.openNotification = false;
                            this.setState({ snackbar: a })
                        }, 2000)
                        this.resetForm('preview');


                    }).catch(err => {
                        console.log("error", err)
                        this.getTableData();
                        let a = this.state.snackbar;
                        a.openNotification = true;
                        a.NotificationMessage = "Failed to Update the user";
                        a.status = "success";
                        this.setState({ isLoader: false, tabView: false, snackbar: a })
                        setTimeout(() => {
                            a.openNotification = false;
                            this.setState({ snackbar: a })
                        }, 2000)
                        this.resetForm('preview');
                    })
            }
        }
    }
    cancelForm = () => {
        this.setState({ tabView: !this.state.tabView })
    }
    formBtnHandle = (item) => {
        if (item.label === 'Cancel') {
            this.cancelForm();
        }
    }
    onPaginationChange = (page, limit) => {
        console.log('onPaginationChange',page,limit)
        this.setState({ page: page, limit: limit }, () => {
            this.getTableData()
        });
    };

    onDeactivateUser = (item) => {
        let selectedItem = JSON.parse(item.Item);
        let previewEmail = selectedItem.email;
        console.log("item", previewEmail)

        let putBodyData = {
            "firstName": selectedItem.firstName,
            "lastName": selectedItem.lastName,
            "role": selectedItem.role,
            "secondaryEmail": selectedItem.secondaryEmail,
            "mobileNumber": selectedItem.mobileNumber,
            "accountStatus": "Deactive",
        }
        axios.put(`${this.state.env["ken42"]}/zq/user/usersdata?userName=${previewEmail}`, putBodyData)
            .then(response => {
                console.log("response", response)
                this.getTableData();
                let a = this.state.snackbar;
                a.openNotification = true;
                a.NotificationMessage = "User has been Deactivated";
                a.status = "success";
                this.setState({ isLoader: false, snackbar: a })
                setTimeout(() => {
                    a.openNotification = false;
                    this.setState({ snackbar: a })
                }, 2000)
            }).catch(err => {
                console.log("err", err)
                this.getTableData();
                let a = this.state.snackbar;
                a.openNotification = true;
                a.NotificationMessage = "Failed to Deactivate the user";
                a.status = "success";
                this.setState({ isLoader: false, snackbar: a })
                setTimeout(() => {
                    a.openNotification = false;
                    this.setState({ snackbar: a })
                }, 2000)
            })
    }

    onPaginationChange2 = (page, limit, datas) => {
        console.log(page, limit, datas)
        this.setState({ isLoader: true })
        let data = JSON.parse(datas)
        console.log("data", data)
        this.setState({ page: page, limit: limit, sampleUserData: [] }, () => {
            let current_page = Number(page);
            let perPage = Number(limit);
            let total_pages = Math.ceil(data.length / perPage);
            console.log("total", total_pages)
            current_page = total_pages < current_page ? total_pages : current_page
            let lastIndex = current_page * perPage;
            let startIndex = lastIndex - perPage;
            let paginatedItems = data.slice(startIndex, lastIndex)
            this.setState({ previewpage: current_page, previewlimit: perPage, previewtotalPages: total_pages, previewtotalRecord: data.length });
            this.setState({ sampleUserData: paginatedItems, isLoader: false });
        });
        console.log(page, limit, this.state.previewtotalPages);
    };

    onActivateUser = (item) => {
        let selectedItem = JSON.parse(item.Item);
        let previewEmail = selectedItem.email;
        console.log("item", previewEmail)

        let putBodyData = {
            "firstName": selectedItem.firstName,
            "lastName": selectedItem.lastName,
            "role": selectedItem.role,
            "secondaryEmail": selectedItem.secondaryEmail,
            "mobileNumber": selectedItem.mobileNumber,
            "accountStatus": "Active",
        }
        axios.put(`${this.state.env["ken42"]}/zq/user/usersdata?userName=${previewEmail}`, putBodyData)
            .then(response => {
                console.log("response", response)
                this.getTableData();
                let a = this.state.snackbar;
                a.openNotification = true;
                a.NotificationMessage = "User has been Activated";
                a.status = "success";
                this.setState({ isLoader: false, snackbar: a })
                setTimeout(() => {
                    a.openNotification = false;
                    this.setState({ snackbar: a })
                }, 2000)
            }).catch(err => {
                console.log("err", err)
                this.getTableData();
                let a = this.state.snackbar;
                a.openNotification = true;
                a.NotificationMessage = "Failed to Activate the user";
                a.status = "success";
                this.setState({ isLoader: false, snackbar: a })
                setTimeout(() => {
                    a.openNotification = false;
                    this.setState({ snackbar: a })
                }, 2000)
            })
    }

    render() {
        console.log("activeStatus: dataOne.accountStatus", this.state.activeStatus)
        return (
            <React.Fragment>
                {this.state.isLoader ? <Loader /> : null}
                <div className="list-of-students-mainDiv table-head-padding">
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            {this.state.tabView ? <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.setState({ tabView: false }) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                : <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            }
                            <p className="top-header-title">| Users</p>
                        </div>
                        {!this.state.tabView ?
                            
                            <div className= {this.state.baseURL === "MNRGROUP" || this.state.baseURL === "VKGI" ? "masters-body-div userTable" : "masters-body-div userTableOthers"  }  >
                                {this.state.containerNav === undefined ? null :
                                    <React.Fragment>
                                        <ContainerNavbar containerNav={this.state.containerNav} onAddNew={this.onAddNew} />
                                        <div className="print-time">{this.state.printTime}</div>
                                    </React.Fragment>
                                }
                                 <div className="remove-last-child-table remove-first-child-table">
                                    {this.state.sampleUserData.length !== 0 ?
                                        < ZqTable
                                            allSelect={(allItems, a) => { this.selectAllData(allItems, a) }}
                                            data={this.state.sampleUserData}
                                            rowClick={(item) => { this.onPreviewUserList(item) }}
                                            onRowCheckBox={(item) => { this.onRowCheckBox(item) }}
                                            handleActionClick={(item, index, hd, name) => { this.handleActionClick(item, index, hd, name) }}
                                        />
                                        :
                                        <p className="noprog-txt">{this.state.tableResTxt}</p>
                                    }
                                </div>
                                <div>
                                    {this.state.sampleUserData.length !== 0 ?
                                        <PaginationUI
                                            total={this.state.previewtotalRecord}
                                            onPaginationApi={this.onPaginationChange2}
                                            totalPages={this.state.previewtotalPages}
                                            limit={this.state.previewlimit}
                                            currentPage={this.state.previewpage}
                                            allData={localStorage.getItem('sampleUserData')}
                                        // total={this.state.totalRecord}
                                        // onPaginationApi={this.onPaginationChange}
                                        // totalPages={this.state.totalPages}
                                        // limit={this.state.limit}
                                        // currentPage={this.state.page} 
                                        />
                                        : null}
                                </div>
                            </div>
                            : <div className="masters-body-div">
                                <div className="organisation-table user-form">
                                    {this.state.viewType == "preview" ?
                                        <ZenForm
                                            inputData={this.state.userForm}
                                            className="user-form-wrap"
                                            onInputChanges={(e, a) => this.onInputChanges(e, a)}
                                            onFormBtnEvent={(item) => { this.formBtnHandle(item); }}
                                            clear={false}
                                            onSubmit={this.onSubmit}
                                        /> :
                                        <ZenForm
                                            inputData={this.state.userForm}
                                            className="user-form-wrap"
                                            onInputChanges={(e, a, event, dataS) => this.onInputChanges(e, a, event, dataS)}
                                            onFormBtnEvent={(item) => {
                                                this.formBtnHandle(item);
                                            }}
                                            clear={false}
                                            onSubmit={this.onSubmit}
                                        />
                                    }
                                </div>
                            </div>}
                    </React.Fragment>
                    <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={2000} onClose={this.closeNotification}>
                        <Alert onClose={this.closeNotification}
                            severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
                            {this.state.snackbar.NotificationMessage}
                        </Alert>
                    </Snackbar>
                </div>


            </React.Fragment>
        )
    }
}
export default withRouter(User)