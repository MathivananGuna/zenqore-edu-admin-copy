import React, { Component } from "react";
import '../../../../../scss/student.scss';
import '../../../../../scss/setup.scss';
import Loader from "../../../../../utils/loader/loaders";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ContainerNavbar from "../../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../../utils/Table/table-component";
import PaginationUI from "../../../../../utils/pagination/pagination";
import ZenTabs from '../../../../../components/input/tabs';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import InstallmentJson from './remainderplan.json';
import axios from "axios";
import moment from 'moment';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import DateFormatContext from '../../../../../gigaLayout/context';
import xlsx from 'xlsx';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
class LateFee extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            campusId: localStorage.getItem('campusId') === "null" || localStorage.getItem('campusId') === null ? undefined : localStorage.getItem('campusId'),
            containerNav: InstallmentJson.containerNav, // installment.json
            studentsFormjson: InstallmentJson.studentsFormjson, // installment.json
            sampleStudentsData: [], // API response(get)
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 1,
            tabViewForm: false,
            studentName: "",
            tableResTxt: "Fetching Data...",
            viewHeader: true,
            totalApiRes: null,
            viewFormLoad: true,
            LoaderStatus: false,
            feesBreakUpArray: [],
            tableViewMode: "",
            payloadId: "",
            snackbar: { openNotification: false, NotificationMessage: "", status: "success" },
            reminderView: "",
            downloadTotalRecord: 0
        }
    }
    static contextType = DateFormatContext;
    componentDidMount() {
        this.getTableData()
    }
    getTableData = () => {
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/reminders?orgId=${this.state.orgId}&campusId=${this.state.campusId}&page=${this.state.page}&limit=${this.state.limit}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                let installmentTableRes = [];
                console.log('status', res.data.status)
                if (res.data.status == "success") {
                    if (res.data.data.length == 0) {
                        this.setState({ tableResTxt: "No Data" })
                    }
                    else {
                        res.data.data.map((data) => {
                            let r = [];
                            // data.scheduleDetails.map((dataOne) => {
                            //     r.push(`${dataOne.days}th`)
                            // })
                            installmentTableRes.push({
                                "Id": data.displayName,
                                "Title": data.title,
                                "Description": data.description,
                                "no. of reminders": data.numberOfReminders,
                                "Campus Name": data.campusIdName,
                                // "dates":r.toString(),
                                "Created on": data.createdAt !== "" ? this.context.dateFormat !== "" ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                "Created By": data.createdBy,
                                // "Created on": data.createdAt !== "" ? moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                "Status": data.status == 1 ? "Active" : "Inactive",
                                "Item": JSON.stringify(data)
                            })
                        })
                        this.setState({ sampleStudentsData: installmentTableRes, totalApiRes: res.data.data, tabViewForm: false, totalRecord: res.data.totalRecord, totalPages: res.data.totalPages, page: res.data.currentPage, downloadTotalRecord: res.data.totalRecord })
                    }
                }
                else {
                    this.setState({ tableResTxt: "Error loading Data" })
                }
            })
            .catch(err => { console.log(err); this.setState({ tableResTxt: "Error loading Data" }) })
    }
    onAddInstallment = () => {
        this.setState({ tabViewForm: true, viewHeader: false, viewFormLoad: true, tableViewMode: "addnew", tableResTxt: "Fetching Data...", reminderView: "New Reminder" }, () => {
            let installmentFormData = InstallmentJson.AddstudentsFormjson;
            axios({
                method: 'get',
                url: `${this.state.env["zqBaseUri"]}/edu/master/getDisplayId/reminders?orgId=${this.state.orgId}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            })
                .then(res => {
                    this.resetform()
                    Object.keys(installmentFormData).forEach(tab => {
                        installmentFormData[tab].forEach(task => {
                            if (task['name'] == 'id') {
                                task['defaultValue'] = String(res.data.data)
                                task['readOnly'] = true
                            }
                        })
                    })
                    this.setState({ studentsFormjson: installmentFormData, viewFormLoad: false, reminderView: `New Reminder | ${res.data.data}` })
                })
                .catch(err => {
                    this.setState({ tableResTxt: "Error loading Data" })
                })
        })
    }
    handleBackFun = () => {
        this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`);
        this.resetform()
    }
    onPreviewStudentList = (e) => {
        let details = this.state.globalSearch ? e : JSON.parse(e.Item);
        console.log(e, details);
        console.log(details, e);
        let installmentFormData = InstallmentJson.studentsFormjson;
        installmentFormData['Schedule Details'] = [];
        let secondTabData = InstallmentJson.TotalReminder;
        this.state.totalApiRes.map((data) => {
            if (data.displayName == e.Id) {
                this.setState({ tabViewForm: true, studentName: e.ID, viewHeader: true, viewFormLoad: true, tableViewMode: "preview", payloadId: details["_id"] })
                Object.keys(installmentFormData).forEach(tab => {
                    installmentFormData[tab].forEach(task => {
                        if (task['name'] == 'id') {
                            task['defaultValue'] = e.Id
                            task['readOnly'] = true
                            task['required'] = false
                        }
                        else if (task['name'] == 'title') {
                            task['defaultValue'] = e.Title
                            task['readOnly'] = false
                            task['required'] = false
                        }
                        else if (task['name'] == 'description') {
                            task['defaultValue'] = e.Description
                            task['readOnly'] = false
                            task['required'] = false
                        }
                        else if (task['name'] == 'createdBy') {
                            task['defaultValue'] = details.createdBy
                            task['readOnly'] = false
                            task['required'] = false
                        }
                        else if (task['name'] == 'noOfReminders') {
                            task['defaultValue'] = String(details.numberOfReminders)
                            task['readOnly'] = false
                            task['required'] = false
                        }
                        else if (task['name'] == 'campusName') {
                            task['defaultValue'] = '-'
                            task['readOnly'] = false
                            task['required'] = false
                        }
                    })
                })
                secondTabData.map((data, i) => {
                    if (i <= Number(details.numberOfReminders)) {
                        if (data.name == "daysBeforeDueDate") {
                            data['defaultValue'] = String(details.demandNoteReminder) == undefined ? "0" : String(details.demandNoteReminder);
                            installmentFormData['Schedule Details'].push(data)
                        }
                        else if (data.name == "daysafterDueDate") {
                            data['defaultValue'] = String(details.otherReminders[0]) == undefined ? "0" : String(details.otherReminders[0]);
                            installmentFormData['Schedule Details'].push(data)
                        }
                        else if (data.name == "daysafterfirstRem") {
                            data['defaultValue'] = String(details.otherReminders[1]) == undefined ? "0" : String(details.otherReminders[1]);
                            installmentFormData['Schedule Details'].push(data)
                        }
                        else if (data.name == "daysafterSecondRem") {
                            data['defaultValue'] = String(details.otherReminders[2]) == undefined ? "0" : String(details.otherReminders[2]);
                            installmentFormData['Schedule Details'].push(data)
                        }
                        else if (data.name == "daysafterSecondRem1") {
                            // data['defaultValue'] = String(details.otherReminders[3]) == undefined ? "0" : String(details.otherReminders[3]);
                            installmentFormData['Schedule Details'].push(data)
                        }
                        else if (data.name == "daysafterSecondRem2") {
                            // data['defaultValue'] = String(details.scheduleDetails[5].days) == undefined ? "0" : String(details.scheduleDetails[5].days);
                            data['defaultValue'] = "0"
                            installmentFormData['Schedule Details'].push(data)
                        }
                        else if (data.name == "daysafterSecondRem3") {
                            // data['defaultValue'] = String(details.scheduleDetails[6].days) == undefined ? "0" : String(details.scheduleDetails[6].days);
                            data['defaultValue'] = "0"
                            installmentFormData['Schedule Details'].push(data)
                        }
                    }
                })
                this.setState({ studentsFormjson: installmentFormData, LoaderStatus: false, viewFormLoad: false, reminderView: `Reminders | ${e.Id}` })
            }
            else { }
        })
    }
    onInputChanges = (value, item, event, dataS) => {
        console.log(value, item, event, dataS);
        if (this.state.tableViewMode.toLowerCase() === "preview") {
            console.log(value, item, event, dataS);
            let installmentFormData = InstallmentJson.studentsFormjson;
            installmentFormData['Schedule Details'] = [];
            this.setState({ studentsFormjson: installmentFormData }, () => {
                let secondTabData = InstallmentJson.TotalReminder;
                secondTabData.map((data, i) => {
                    if (i <= Number(data.defaultValue)) {
                        if (data.name == "daysBeforeDueDate") {
                            data['defaultValue'] = data['defaultValue'];
                            installmentFormData['Schedule Details'].push(data)
                        }
                        else if (data.name == "daysafterDueDate") {
                            data['defaultValue'] = data['defaultValue'];
                            installmentFormData['Schedule Details'].push(data)
                        }
                        else if (data.name == "daysafterfirstRem") {
                            data['defaultValue'] = data['defaultValue'];
                            installmentFormData['Schedule Details'].push(data)
                        }
                        else if (data.name == "daysafterSecondRem") {
                            data['defaultValue'] = data['defaultValue'];
                            installmentFormData['Schedule Details'].push(data)
                        }
                        else if (data.name == "daysafterSecondRem1") {
                            data['defaultValue'] = data['defaultValue'];
                            installmentFormData['Schedule Details'].push(data)
                        }
                        else if (data.name == "daysafterSecondRem2") {
                            data['defaultValue'] = data['defaultValue'];
                            installmentFormData['Schedule Details'].push(data)
                        }
                        else if (data.name == "daysafterSecondRem3") {
                            data['defaultValue'] = data['defaultValue'];
                            installmentFormData['Schedule Details'].push(data)
                        }
                    }
                })
                this.setState({ studentsFormjson: installmentFormData })
            })
        }
    }
    onPreviewStudentList1 = (e) => { }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.getTableData()
        });
    };
    moveBackTable = () => {
        this.setState({ tabViewForm: false })
        this.resetform()
    }
    handleTabChange = (tabName, formDatas) => {
        let noOfIns, freqData, dueDate;
        let newStudentsFormjson = this.state.studentsFormjson;
        if (tabName == 2) {
            formDatas["General"].map((data) => {
                if (data.name == "noOfInstallments") {
                    noOfIns = data.defaultValue
                }
            })
            formDatas["Late Fee Details"].map((data) => {
                if (data.name == "frequencySchedule") {
                    freqData = data.defaultValue
                }
                else if (data.name == "byDueDate") {
                    dueDate = data.defaultValue
                }
            })
        }
    }
    onFormSubmit = (data, item) => {
        this.setState({ isLoader: true, LoaderStatus: true })
        if (this.state.tableViewMode === "preview") {
            let defaultValue1 = {}
            let installmentFormData = InstallmentJson.studentsFormjson;
            Object.keys(installmentFormData).forEach(tab => {
                installmentFormData[tab].forEach(task => {
                    if (task['name'] == 'id') {
                        defaultValue1['displayName'] = task['defaultValue']
                    }
                    if (task['name'] == 'title') {
                        defaultValue1['title'] = task['defaultValue']
                    }
                    if (task['name'] == 'description') {
                        defaultValue1['description'] = task['defaultValue']
                    }
                    if (task['name'] == 'noOfReminders') {
                        defaultValue1['numberOfReminders'] = task['defaultValue']
                    }
                    if (task['name'] == 'campusName') {
                        defaultValue1['campusName'] = task['defaultValue']
                    }
                    if (task['name'] == 'daysBeforeDueDate') {
                        defaultValue1['daysBeforeDueDate'] = task['defaultValue']
                    }
                    if (task['name'] == 'daysafterDueDate') {
                        defaultValue1['daysafterDueDate'] = task['defaultValue']
                    }
                    if (task['name'] == 'daysafterfirstRem') {
                        defaultValue1['daysafterfirstRem'] = task['defaultValue']
                    }
                    if (task['name'] == 'daysafterSecondRem') {
                        defaultValue1['daysafterSecondRem'] = task['defaultValue']
                    }
                    if (task['name'] == 'daysafterSecondRem1') {
                        defaultValue1['daysafterSecondRem1'] = task['defaultValue']
                    }
                    if (task['name'] == 'daysafterSecondRem2') {
                        defaultValue1['daysafterSecondRem2'] = task['defaultValue']
                    }
                })
            })
            let payload = {
                "displayName": defaultValue1.displayName,
                "title": defaultValue1.title,
                "description": defaultValue1.description == undefined || defaultValue1.description == null ? "-" : defaultValue1.description,
                "numberOfReminders": Number(defaultValue1.numberOfReminders),
                // "campusName": defaultValue1.campusName,
                "scheduleDetails": [
                    {
                        "name": "After Demand Note Day",
                        "days": Number(defaultValue1.daysBeforeDueDate == undefined ? "" : defaultValue1.daysBeforeDueDate)
                    },
                    {
                        "name": "1st Reminder Day",
                        "days": Number(defaultValue1.daysafterDueDate == undefined ? "" : defaultValue1.daysafterDueDate)
                    },
                    {
                        "name": "2nd Reminder Day",
                        "days": Number(defaultValue1.daysafterfirstRem == undefined ? "" : defaultValue1.daysafterfirstRem)
                    },
                    {
                        "name": "3rd Reminder Day",
                        "days": Number(defaultValue1.daysafterSecondRem == undefined ? "" : defaultValue1.daysafterSecondRem)
                    },
                    {
                        "name": "4th Reminder Day",
                        "days": Number(defaultValue1.daysafterSecondRem1 == undefined ? "" : defaultValue1.daysafterSecondRem1)
                    }
                ],
                "createdBy": this.state.orgId,
                "orgId": this.state.orgId
            }
            let headers = {
                'Authorization': this.state.authToken
            };
            let bodyData = payload;
            axios.put(`${this.state.env["zqBaseUri"]}/edu/master/reminders?id=${this.state.payloadId}`, bodyData, { headers })
                .then(res => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Updated Successfully", status: "success" }
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                    this.resetform();
                    this.getTableData();
                    setTimeout(() => {
                        snackbarUpdate.openNotification = false
                        this.setState({ tabViewForm: false, snackbar: snackbarUpdate })
                    }, 1500)
                })
                .catch(err => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Error", status: "error" }
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                    setTimeout(() => {
                        snackbarUpdate.openNotification = false
                        this.setState({ tabViewForm: false, snackbar: snackbarUpdate })
                    }, 1500)
                })
        }
        else if (this.state.tableViewMode === "addnew") {
            let defaultValue1 = {}
            let installmentFormData = InstallmentJson.AddstudentsFormjson;
            Object.keys(installmentFormData).forEach(tab => {
                installmentFormData[tab].forEach(task => {
                    if (task['name'] == 'id') {
                        defaultValue1['displayName'] = task['defaultValue']
                    }
                    if (task['name'] == 'title') {
                        defaultValue1['title'] = task['defaultValue']
                    }
                    if (task['name'] == 'description') {
                        defaultValue1['description'] = task['defaultValue']
                    }
                    if (task['name'] == 'campusName') {
                        defaultValue1['campusName'] = task['defaultValue']
                    }
                    if (task['name'] == 'noOfReminders') {
                        defaultValue1['numberOfReminders'] = task['defaultValue']
                    }
                    if (task['name'] == 'daysBeforeDueDate') {
                        defaultValue1['daysBeforeDueDate'] = task['defaultValue']
                    }
                    if (task['name'] == 'daysafterDueDate') {
                        defaultValue1['daysafterDueDate'] = task['defaultValue']
                    }
                    if (task['name'] == 'daysafterfirstRem') {
                        defaultValue1['daysafterfirstRem'] = task['defaultValue']
                    }
                    if (task['name'] == 'daysafterSecondRem') {
                        defaultValue1['daysafterSecondRem'] = task['defaultValue']
                    }
                })
            })
            let payload = {
                "displayName": defaultValue1.displayName,
                "title": defaultValue1.title,
                "description": defaultValue1.description == undefined || defaultValue1.description == null ? "-" : defaultValue1.description,
                "numberOfReminders": Number(defaultValue1.numberOfReminders),
                // "campusName": defaultValue1.campusName,
                "scheduleDetails": [
                    {
                        "name": "After Demand Note Day",
                        "days": Number(defaultValue1.daysafterDueDate == undefined ? "" : defaultValue1.daysafterDueDate)
                    },
                    {
                        "name": "1st Reminder Day",
                        "days": Number(defaultValue1.daysafterfirstRem == undefined ? "" : defaultValue1.daysafterfirstRem)
                    },
                    {
                        "name": "2nd Reminder Day",
                        "days": Number(defaultValue1.daysafterSecondRem == undefined ? "" : defaultValue1.daysafterSecondRem)
                    },
                    {
                        "name": "3rd Reminder Day",
                        "days": ""
                    },
                    {
                        "name": "4th Reminder Day",
                        "days": ""
                    }
                ],
                "createdBy": this.state.orgId,
                "orgId": this.state.orgId
            }
            let headers = {
                'Authorization': this.state.authToken
            };
            let bodyData = payload;
            axios.post(` ${this.state.env["zqBaseUri"]}/edu/master/reminders`, bodyData, { headers })
                .then(res => {
                    if (res.data.status == "success") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Added successfully", status: "success" }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                        this.resetform();
                        this.getTableData();
                        setTimeout(() => {
                            snackbarUpdate.openNotification = false
                            this.setState({ tabViewForm: false, snackbar: snackbarUpdate })
                        }, 1500)
                    }
                    else if (res.data.status == "error") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: res.data.message, status: "error" }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                        this.resetform();
                        this.getTableData();
                        setTimeout(() => {
                            snackbarUpdate.openNotification = false
                            this.setState({ tabViewForm: false, snackbar: snackbarUpdate })
                        }, 1500)
                    }
                })
                .catch(err => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Error", status: "error" }
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                    setTimeout(() => {
                        snackbarUpdate.openNotification = false
                        this.setState({ tabViewForm: false, snackbar: snackbarUpdate })
                    }, 1500)
                })
        }
    }
    closeNotification = () => {
        let snackbarUpdate = { openNotification: false, NotificationMessage: "", status: "" }
        this.setState({ snackbar: snackbarUpdate })
    }
    resetform = () => {
        let installmentFormData = InstallmentJson.studentsFormjson;
        let installmentFormData1 = InstallmentJson.AddstudentsFormjson;
        Object.keys(installmentFormData).forEach(tab => {
            installmentFormData[tab].forEach(task => {
                delete task['defaultValue']
                task['readOnly'] = false
                task['error'] = false
                task['required'] = task['requiredBoolean']
                task['validation'] = false
                delete task['clear']
            })
        })
        Object.keys(installmentFormData1).forEach(tab => {
            installmentFormData1[tab].forEach(task => {
                delete task['defaultValue']
                task['readOnly'] = false
                task['error'] = false
                task['required'] = task['requiredBoolean']
                task['validation'] = false
                delete task['clear']
            })
        })
        this.setState({ studentsFormjson: installmentFormData, payloadId: "", status: "" })
    }
    cancelViewData = () => {
        this.setState({ tabViewForm: false });
        this.resetform();
    }
    allSelect = () => { }
    cleanDatas = () => { }
    searchHandle = (value) => { }
    printScreen = () => {
        let page = 1
        let limit = this.state.downloadTotalRecord || 1
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/reminders?orgId=${this.state.orgId}&page=${page}&limit=${limit}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                this.setState({ sampleStudentsData: [] })
                let installmentTableRes = [];
                if (res.data.status == "success") {
                    if (res.data.data.length == 0) {
                        this.setState({ tableResTxt: "No Data" })
                    }
                    else {
                        res.data.data.map((data) => {
                            let r = [];
                            installmentTableRes.push({
                                "Id": data.displayName,
                                "Title": data.title,
                                "Description": data.description,
                                "no. of reminders": data.numberOfReminders,
                                "Campus Name": data.campusIdName,
                                // "dates":r.toString(),
                                "Created on": data.createdAt !== "" ? this.context.dateFormat !== "" ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                "Created By": data.createdBy,
                                // "Created on": data.createdAt !== "" ? moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                "Status": data.status == 1 ? "Active" : "Inactive",
                            })
                        })
                        this.setState({ sampleStudentsData: installmentTableRes }, () => {
                            setTimeout(() => {
                                window.print()
                                this.getTableData()
                            }, 1);
                        })
                    }
                }
            })
            .catch(err => {
                console.log(err);
            })
    }
    onDownload = () => {
        let createXlxsData = [];
        let page = 1
        let limit = this.state.downloadTotalRecord
        this.setState({ LoaderStatus: true }, () => {
            axios({
                method: 'get',
                url: `${this.state.env["zqBaseUri"]}/edu/master/reminders?orgId=${this.state.orgId}&page=${page}&limit=${limit}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            })
                .then(res => {
                    if (res.data.status == "success") {
                        if (res.data.data.length !== 0) {
                            res.data.data.map((data) => {
                                createXlxsData.push({
                                    "Id": data.displayName,
                                    "Title": data.title,
                                    "Description": data.description,
                                    "no. of reminders": data.numberOfReminders,
                                    "Campus Name": data.campusIdName,
                                    // "dates":r.toString(),
                                    "Created on": data.createdAt !== "" ? this.context.dateFormat !== "" ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                    "Created By": data.createdBy,
                                    // "Created on": data.createdAt !== "" ? moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                    "Status": data.status == 1 ? "Active" : "Inactive",
                                })
                            })
                            var ws = xlsx.utils.json_to_sheet(createXlxsData);
                            var wb = xlsx.utils.book_new();
                            xlsx.utils.book_append_sheet(wb, ws, "Reminder Plan");
                            xlsx.writeFile(wb, "Reminder Plan.xlsx");
                            this.setState({ LoaderStatus: false })
                        }
                    }
                }).catch(err => {
                    console.log(err, 'errorrrrr')
                    this.setState({ LoaderStatus: false })
                })
        })
    }
    render() {
        return (
            <div className="list-of-students-mainDiv">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.tabViewForm == false ?
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} >| Reminders</p>
                        </div>
                        <div className="masters-body-div">
                            {this.state.containerNav == undefined ? null :
                                <React.Fragment>
                                    <ContainerNavbar containerNav={this.state.containerNav} onAddNew={() => { this.onAddInstallment(); }} searchValue={(searchValue) => this.searchHandle(searchValue)} onDownload={this.onDownload} printScreen={this.printScreen} />
                                    <div className="print-time">{this.state.printTime}</div>
                                </React.Fragment>}
                            <div className="remove-last-child-table">
                                {this.state.sampleStudentsData.length !== 0 ?
                                    <ZqTable
                                        allSelect={this.allSelect}
                                        data={this.state.sampleStudentsData}
                                        rowClick={(item) => { this.onPreviewStudentList(item) }}
                                        onRowCheckBox={(item) => { this.onPreviewStudentList1(item) }}
                                    />
                                    : <p className="noprog-txt">{this.state.tableResTxt}</p>
                                }
                            </div>
                            <div>
                                {this.state.sampleStudentsData.length !== 0 ?
                                    <PaginationUI
                                        total={this.state.totalRecord}
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        currentPage={this.state.page}
                                    />
                                    : null}
                            </div>
                        </div>
                    </React.Fragment> :
                    <React.Fragment>
                        <div className="tab-form-wrapper tab-table remainderPlan">
                            <div className="preview-btns">
                                {this.state.viewHeader == true ?
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| {this.state.reminderView}</h6>
                                    </div> :
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| {this.state.reminderView}</h6>
                                    </div>
                                }
                            </div>
                            <div className="organisation-table">
                                {this.state.viewFormLoad === false ?
                                    <ZenTabs tabData={this.state.studentsFormjson} className="preview-wrap" tabEdit={this.state.preview} form={this.state.studentsFormjson} cleanData={this.cleanDatas} cancelViewData={this.cancelViewData} value={0} onInputChanges={this.onInputChanges} onFormBtnEvent={(item) => { this.formBtnHandle(item); }} onTabFormSubmit={this.onFormSubmit} handleTabChange={this.handleTabChange} key={0} />
                                    : <p className="noprog-txt">{this.state.tableResTxt}</p>}
                            </div>
                        </div>
                    </React.Fragment>
                }
                <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={6000} onClose={this.closeNotification}>
                    <Alert onClose={this.closeNotification}
                        severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
                        {this.state.snackbar.NotificationMessage}
                    </Alert>
                </Snackbar>
            </div>
        )
    }
}
export default LateFee;