import React, { Component } from 'react';
import '../../../../../scss/fees-type.scss';
import '../../../../../scss/setup.scss';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import ContainerNavbar from "../../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../../utils/Table/table-component";
import PaginationUI from "../../../../../utils/pagination/pagination";
import Loader from "../../../../../utils/loader/loaders";
import LoanDataJson from './loan-data-master.json';
import ZenForm from "../../../../../components/input/zqform";
import Axios from 'axios';
import moment from 'moment';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import DateFormatContext from '../../../../../gigaLayout/context';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
class Loans extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            containerNav: {
                isBack: false,
                name: "List of Loans",
                isName: true,
                isSearch: true,
                isSort: false,
                isPrint: true,
                isDownload: true,
                isShare: false,
                isNew: true,
                newName: "New",
                isSubmit: false,
            },
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            campusId: localStorage.getItem('campusId') === "null" || localStorage.getItem('campusId') === null ? undefined : localStorage.getItem('campusId'),
            viewType: "",
            snackbar: { openNotification: false, NotificationMessage: "", status: "success" },
            tablePrintDetails: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 1,
            noProg: "Fetching Data ...",
            PreStudentName: "",
            previewList: false,
            addNewMasterLoan: LoanDataJson.formJson,
            sampleLoanData: [],
            PreviewData: "",
            previewloadId: "",
            TotalApiResData: "",
        }
    }

    static contextType = DateFormatContext;

    componentDidMount() {
        this.getTableData();
        this.getInputOptions();
    }

    getTableData = () => {
        this.setState({ sampleLoanData: [] });
        Axios({
            method: 'GET',
            url: `${this.state.env["zqBaseUri"]}/edu/loans?orgId=${this.state.orgId}&campusId=${this.state.campusId}&page=${this.state.page}&limit=${this.state.limit}`,
            headers: {
                'Authorization': this.state.authToken,
            }
        }).then(res => {
            console.log("loanres", res)
            if (res.data.status == "success") {

                if (res.data.data.length == 0) {
                    this.setState({ noProg: "No Data" })
                }
                else {
                    let loanData = [];
                    res.data.data.map(item => {
                        loanData.push(
                            {
                                "Id": item.displayName,
                                "Provider": item.provider,
                                "Bank Name": item.bankDetails != null ? item.bankDetails.bankName : null,
                                "Bank Account Name": item.bankDetails != null ? item.bankDetails.bankAccountName : null,
                                "Bank IFSC": item.bankDetails != null ? item.bankDetails.bankIFSC : null,
                                //"Created By": item.createdBy,
                                "Created On": item.createdAt !== "" ? this.context.dateFormat !== "" ? moment(item.createdAt).format(this.context.dateFormat) : moment(item.createdAt).format('DD/MM/YYYY') : "-",
                                "Status": item.status == 1 ? "Active" : "Deactive",
                                "Item": JSON.stringify(item)
                            }
                        )
                    })
                    this.setState({ sampleLoanData: loanData, totalRecord: res.data.totalRecord, totalPages: res.data.totalPages, page: res.data.page })
                }
            }
            else {

            }

        }).catch(err => {
            console.log("loans err", err)
            this.setState({ noProg: "Loading Data Error" })
        })

    }

    getInputOptions = () => {
        let installmentFormData = LoanDataJson.formJson;
        Axios({
            method: 'GET',
            url: `${this.state.env["zqBaseUri"]}/edu/banks?orgId=${this.state.orgId}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                console.log("selectedres", res)
                let bankTypeOption = [];
                res.data.data.map((data) => {
                    bankTypeOption.push({
                        "label": data.bankName,
                        "value": data._id,
                        "accountName": data.bankAccountName,
                        "accountNumber": data.bankAccountNumber
                    })
                })
                console.log("bankTypeOption", bankTypeOption)

                installmentFormData.map(task => {
                    if (task['name'] == 'bankname') {
                        task['options'] = bankTypeOption
                    }
                })

                this.setState({ addNewMasterLoan: installmentFormData })
            })
            .catch(err => { })
    }
    onAddNewLoans = () => {
        this.setState({ addNewMasterLoan: this.state.addNewMasterLoan, LoaderStatus: true, viewType: "addNew", previewList: true, PreStudentName: " Loans" }, () => {
            let installmentFormData = LoanDataJson.formJson;
            Axios({
                method: 'GET',
                url: `${this.state.env["zqBaseUri"]}/edu/banks?orgId=${this.state.orgId}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            })
                .then(res => {
                    console.log("selectedres", res)
                    let bankTypeOption = [];
                    res.data.data.map((data) => {
                        bankTypeOption.push({
                            "label": data.bankName,
                            "value": data._id,
                            "accountName": data.bankAccountName,
                            "accountNumber": data.bankAccountNumber
                        })
                    })
                    console.log("bankTypeOption", bankTypeOption)

                    installmentFormData.map(task => {
                        if (task['name'] == 'bankname') {
                            task['options'] = bankTypeOption
                        }
                    })

                    this.setState({ addNewMasterLoan: installmentFormData })
                })
                .catch(err => { })
            Axios({
                method: 'GET',
                url: `${this.state.env["zqBaseUri"]}/edu/master/getDisplayId/loans?orgId=${this.state.orgId}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            }).then(res => {
                console.log("loanId", res)
                let addNewMasterLoan = LoanDataJson.formJson;
                addNewMasterLoan.map(item => {
                    if (item.name == 'loanID') {
                        item['defaultValue'] = res.data.data;
                        item['readOnly'] = true;

                    }

                })
                this.setState({ previewList: true, LoaderStatus: false, PreStudentName: ` Loans | ${res.data.data}` })

            }).catch(err => {
                console.log("err", err)
            })

        })
    }
    onPreviewLoan = (e) => {
        console.log(e)
        let parsedValueData = JSON.parse(e.Item);
        console.log("parsedValueData", parsedValueData)
        this.setState({ tabViewForm: true, PreStudentName: ` Loans | ${parsedValueData.displayName}`, viewType: "preview", PreviewData: parsedValueData })
        let previewListData = this.state.addNewMasterLoan;
        if (parsedValueData.displayName == e.Id) {
            this.setState({ previewloadId: parsedValueData["_id"] })
            previewListData.map((task) => {

                if (task['name'] == 'loanID') {
                    task['defaultValue'] = parsedValueData.displayName
                    task['readOnly'] = true
                }

                if (task['name'] == 'provider') {
                    task['defaultValue'] = parsedValueData.provider
                    task['readOnly'] = false
                    task['required'] = false
                }
                if (task['name'] == 'bankname') {
                    task['defaultValue'] = parsedValueData.bankDetails != null ? parsedValueData.bankDetails._id : ''
                    task['readOnly'] = false
                    task['required'] = false
                }
                if (task['name'] == 'bankaccountname') {
                    task['defaultValue'] = parsedValueData.bankDetails != null ? parsedValueData.bankDetails.bankAccountName : ''
                    task['readOnly'] = false
                    task['required'] = false
                }
                if (task['name'] == 'bankaccountnumber') {
                    task['defaultValue'] = parsedValueData.bankDetails != null ? parsedValueData.bankDetails.bankAccountNumber : ''
                    task['readOnly'] = false
                    task['required'] = false
                }

            })
        }
        else {

        }

        console.log(previewListData);
        this.setState({ previewList: true, PreStudentName: ` Loans | ${parsedValueData.displayName}`, addNewMasterLoan: previewListData })
    }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.getTableData()
        });
    };

    moveBackTable = () => {
        this.setState({ previewList: false }, () => {
            this.resetForm('addNew');
            this.resetForm('preview');
        })
    }
    actionClickFun = (e) => {
        console.log(e);
    }
    resetForm = (e) => {
        if (e == "addNew") {
            let newData = this.state.addNewMasterLoan;
            newData.map((task) => {
                task["error"] = false;
                task['validation'] = false;
                task['errorMsg'] = "";
                task['defaultValue'] = "";
            })
            this.setState({ addNewMasterLoan: newData })
        }
        else if (e == "preview") {
            let newData1 = this.state.addNewMasterLoan;
            newData1.map((task) => {
                task["error"] = false;
                task['validation'] = false;
                task['errorMsg'] = "";
                task['defaultValue'] = "";
            })
            this.setState({ addNewMasterLoan: newData1 })
        }
    }
    handleTabChange = () => { }
    allSelect = () => { }
    onInputChanges = (value, item, e, dataS) => {
        if (dataS != undefined) {
            console.log("datas", dataS)
            this.setState({ [dataS.name]: value })
            if (dataS.name == "bankname") {
                let addNewMasterLoan = this.state.addNewMasterLoan
                this.setState({ addNewMasterLoan: [] }, () => {
                    addNewMasterLoan.map(inputItem => {
                        inputItem['defaultValue'] = inputItem['name'] == 'bankname' ? value : inputItem['name'] == 'bankaccountname' ? item.accountName : inputItem['name'] == 'bankaccountnumber' ? item.accountNumber : inputItem['defaultValue']
                        inputItem['readOnly'] = (inputItem['name'] == 'bankaccountname' || inputItem['name'] == 'bankaccountnumber') ? true : false
                        inputItem['required'] = false
                        console.log('addNewMasterLoan', addNewMasterLoan)
                        this.setState({ addNewMasterLoan: addNewMasterLoan })
                    })

                })
            }

        }
        else {
            console.log("item", item)
            this.setState({ [item.name]: value })
        }
    }

    onSubmit = (inputData) => {
        console.log("preview", inputData)
        let newData = this.state.addNewMasterLoan;
        console.log("newData", newData)
        let apiNewCall = true;
        newData.map(item => {
            if (item.requiredBoolean) {
                if (item['defaultValue'] === "" || item['defaultValue'] === undefined) {
                    item["error"] = true;
                    item['errorMsg'] = `Invalid ${item.label}`
                    apiNewCall = false
                } else {
                    item["error"] = false;
                    item["validation"] = false;
                }
            }
        })
        // let bodyResData = {};

        // newData.map(item => {
        //     // debugger
        //     console.log("item", item)
        //     if (item['name'] == "loanID") {
        //         bodyResData['loanID'] = item['defaultValue']
        //     }
        //     if (item['name'] == "provider") {
        //         bodyResData['provider'] = item['defaultValue']
        //     }
        //     console.log(" bodyResData['provider']", bodyResData['provider'])
        //     if (item['name'] == 'bankname') {
        //         bodyResData['bankname'] = item['defaultValue']
        //     }
        //     console.log(" bodyResData['bankname']", bodyResData['bankname'])
        //     if (item['name'] == 'bankaccountname') {
        //         bodyResData['bankaccountname'] = item['defaultValue']
        //     }
        //     if (item['name'] == 'bankaccountnumber') {
        //         bodyResData['bankaccountnumber'] = item['defaultValue']
        //     }
        // })


        let headers = {
            'Authorization': this.state.authToken
        };
        let bodyData = {
            "provider": inputData[2].value,
            "displayName": inputData[1].value,
            "bankId": this.state.bankname,
            "createdBy": this.state.orgId,
            "orgId": this.state.orgId
        }
        console.log("bodydata", bodyData)
        if (this.state.viewType === "addNew") {

            this.setState({ addNewMasterLoan: newData })
            if (apiNewCall == true) {
                this.setState({ LoaderStatus: true })

                Axios.post(`${this.state.env["zqBaseUri"]}/edu/loans?orgId=${this.state.orgId}`, bodyData, { headers })
                    .then(res => {
                        console.log("res", res)
                        this.getTableData();
                        let a = this.state.snackbar;
                        a.openNotification = true;
                        a.NotificationMessage = "Loan Item has been added successfully";
                        a.status = "success";
                        this.setState({ previewList: false, LoaderStatus: false, snackbar: a })
                        setTimeout(() => {
                            a.openNotification = false;
                            this.setState({ snackbar: a })
                        }, 2000)
                        this.resetForm('addNew');


                    }).catch(err => {
                        console.log("error", err)
                        this.getTableData();
                        let a = this.state.snackbar;
                        a.openNotification = true;
                        a.NotificationMessage = "Failed to add the Loan Item";
                        a.status = "success";
                        this.setState({ LoaderStatus: false, tabViewForm: false, snackbar: a })
                        setTimeout(() => {
                            a.openNotification = false;
                            this.setState({ snackbar: a })
                        }, 2000)
                        this.resetForm('addNew');
                    })
            }

        }
        else {
            if (apiNewCall == true) {
                this.setState({ LoaderStatus: true })
                Axios.put(`${this.state.env["zqBaseUri"]}/edu/loans/${this.state.previewloadId}?orgId=${this.state.orgId}`, bodyData, { headers })
                    .then(res => {
                        console.log("res", res)
                        this.getTableData();
                        let a = this.state.snackbar;
                        a.openNotification = true;
                        a.NotificationMessage = "Loan Item has been updated successfully";
                        a.status = "success";
                        this.setState({ previewList: false, LoaderStatus: false, snackbar: a })
                        setTimeout(() => {
                            a.openNotification = false;
                            this.setState({ snackbar: a })
                        }, 2000)
                        this.resetForm('preview');


                    }).catch(err => {
                        console.log("error", err)
                        this.getTableData();
                        let a = this.state.snackbar;
                        a.openNotification = true;
                        a.NotificationMessage = "Failed to update the loan item";
                        a.status = "success";
                        this.setState({ LoaderStatus: false, tabViewForm: false, snackbar: a })
                        setTimeout(() => {
                            a.openNotification = false;
                            this.setState({ snackbar: a })
                        }, 2000)
                        this.resetForm('preview');
                    })
            }
        }
    }
    searchHandle = (value) => { }
    onDownload = () => { }
    printScreen = () => {
        let page = 1
        let limit = this.state.downloadTotalRecord || 1
        Axios({
            method: 'GET',
            url: `${this.state.env["zqBaseUri"]}/edu/loans?orgId=${this.state.orgId}&page=${page}&limit=${limit}`,
            headers: {
                'Authorization': this.state.authToken,
            }
        }).then(res => {
            this.setState({ sampleLoanData: [] })
            if (res.data.status == "success") {

                if (res.data.data.length == 0) {
                    this.setState({ noProg: "No Data" })
                }
                else {
                    let loanData = [];
                    res.data.data.map(item => {
                        loanData.push(
                            {
                                "Id": item.displayName,
                                "Provider": item.provider,
                                "Bank Name": item.bankDetails != null ? item.bankDetails.bankName : null,
                                "Bank Account Name": item.bankDetails != null ? item.bankDetails.bankAccountName : null,
                                "Bank IFSC": item.bankDetails != null ? item.bankDetails.bankIFSC : null,
                                "Created On": item.createdAt !== "" ? this.context.dateFormat !== "" ? moment(item.createdAt).format(this.context.dateFormat) : moment(item.createdAt).format('DD/MM/YYYY') : "-",
                                "Status": item.status == 1 ? "Active" : "Deactive",
                            }
                        )
                    })
                    this.setState({ sampleLoanData: loanData }, () => {
                        setTimeout(() => {
                            window.print()
                            this.getTableData()
                        }, 1);
                    })
                }
            }
        }).catch(err => {
            console.log("loans err", err)
        })
    }
    render() {
        return (
            <div className="list-of-feeType-mainDiv">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.previewList == false ?
                    <React.Fragment>
                        <React.Fragment>
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title">| Loans</p>
                            </div>
                        </React.Fragment>
                        <div className="masters-body-div">
                            <React.Fragment>
                                <ContainerNavbar containerNav={this.state.containerNav} onAddNew={() => { this.onAddNewLoans(); }} searchValue={(searchValue) => this.searchHandle(searchValue)} onDownload={this.onDownload} printScreen={this.printScreen} />
                            </React.Fragment>
                            <div className="table-wrapper" style={{ marginTop: "10px" }}>

                                <div className="remove-last-child-table" >
                                    {this.state.sampleLoanData.length !== 0 ?
                                        <ZqTable
                                            data={this.state.sampleLoanData}
                                            allSelect={this.allSelect}
                                            rowClick={(item) => { this.onPreviewLoan(item) }}
                                            onRowCheckBox={(item) => { this.allSelect(item) }}
                                            handleActionClick={(item) => { this.actionClickFun(item) }}
                                        />
                                        :
                                        <p className="noprog-txt">{this.state.noProg}</p>
                                    }
                                </div>
                                <div>
                                    {this.state.sampleLoanData.length !== 0 ?
                                        <PaginationUI
                                            onPaginationApi={this.onPaginationChange}
                                            totalPages={this.state.totalPages}
                                            limit={this.state.limit}
                                            total={this.state.totalRecord}
                                            currentPage={this.state.page} /> : null
                                    }
                                </div>
                            </div>
                        </div>
                    </React.Fragment> :
                    <React.Fragment>
                        <div className="tab-form-wrapper tab-table">
                            <div className="preview-btns">
                                <div className="preview-header-wrap">
                                    < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                    <h6 className="preview-Id-content" onClick={this.moveBackTable}>|{this.state.PreStudentName}</h6>
                                </div>
                            </div>
                            <div className="goods-header">
                                <p className="info-input-label">BASIC DETAILS</p>
                            </div>
                            <div className="goods-wrapper goods-parent-wrap">
                                {this.state.viewType == "preview" ?
                                    <ZenForm
                                        formData={this.state.addNewMasterLoan}
                                        className="goods-wrap"
                                        onInputChanges={(e, a, event, dataS) => this.onInputChanges(e, a, event, dataS)}
                                        onFormBtnEvent={(item) => {
                                            this.formBtnHandle(item);
                                        }}
                                        clear={true}
                                        onSubmit={(e) => this.onSubmit(e.target)}
                                    /> :
                                    <ZenForm
                                        formData={this.state.addNewMasterLoan}
                                        className="goods-wrap"
                                        onInputChanges={(e, a, event, dataS) => this.onInputChanges(e, a, event, dataS)}
                                        onFormBtnEvent={(item) => {
                                            this.formBtnHandle(item);
                                        }}
                                        clear={true}
                                        onSubmit={(e) => this.onSubmit(e.target)}
                                    />
                                }

                            </div>
                        </div>
                    </React.Fragment>
                }
                <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={2000} onClose={this.closeNotification}>
                    <Alert onClose={this.closeNotification}
                        severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
                        {this.state.snackbar.NotificationMessage}
                    </Alert>
                </Snackbar>
            </div>
        )
    }
}
export default Loans;