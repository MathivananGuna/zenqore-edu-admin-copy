import React, { Component } from "react";
import '../../../../../scss/student.scss';
import '../../../../../scss/setup.scss';
import Loader from "../../../../../utils/loader/loaders";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ContainerNavbar from "../../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../../utils/Table/table-component";
import PaginationUI from "../../../../../utils/pagination/pagination";
import ZenTabs from '../../../../../components/input/tabs';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import InstallmentJson from './lateFee.json';
import axios from "axios";
import moment from 'moment';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import DateFormatContext from '../../../../../gigaLayout/context';
import xlsx from 'xlsx';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
class LateFee extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            campusId: localStorage.getItem('campusId') === "null" || localStorage.getItem('campusId') === null ? undefined : localStorage.getItem('campusId'),
            containerNav: InstallmentJson.containerNav, // installment.json
            studentsFormjson: InstallmentJson.studentsFormjson, // installment.json
            sampleStudentsData: [], // API response(get
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 1,
            tabViewForm: false,
            studentName: "",
            tableResTxt: "Fetching Data...",
            viewHeader: true,
            totalApiRes: null,
            viewFormLoad: true,
            LoaderStatus: false,
            feesBreakUpArray: [],
            tableViewMode: "",
            payloadId: "",
            snackbar: { openNotification: false, NotificationMessage: "", status: "success" },
            latefeeViewData: "",
            downloadTotalRecord: 0
        }
    }
    static contextType = DateFormatContext;
    componentDidMount() {
        this.getTableData()
    }
    getTableData = () => {
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/lateFees?orgId=${this.state.orgId}&campusId=${this.state.campusId}&page=${this.state.page}&limit=${this.state.limit}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                let installmentTableRes = [];
                if (res.data.status == "success") {
                    if (res.data.data.length == 0) {
                        this.setState({ tableResTxt: "No Data" })
                    }
                    else {
                        res.data.data.map((data) => {
                            installmentTableRes.push({
                                "Id": data.displayName,
                                "Title": data.title,
                                "Description": data.description,
                                "Campus Name": data.campusIdName,
                                // "Type": data.type,
                                // "Frequency": data.every,
                                "Created on": data.createdAt !== "" ? this.context.dateFormat !== undefined ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                "Created By": data.createdBy,
                                // "Created on": data.createdAt !== "" ? moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                "Status": data.status == 1 ? "Active" : "Inactive",
                                "Item": JSON.stringify(data)
                            })
                        })
                        this.setState({ sampleStudentsData: installmentTableRes, totalApiRes: res.data.data, tabViewForm: false, totalRecord: res.data.totalRecord, totalPages: res.data.totalPages, page: res.data.currentPage, downloadTotalRecord: res.data.totalRecord })
                    }
                }
                else {
                    this.setState({ tableResTxt: "Error loading Data" })
                }
            })
            .catch(err => { this.setState({ tableResTxt: "Error loading Data" }) })
    }
    onAddInstallment = () => {
        this.setState({ tabViewForm: true, viewHeader: false, viewFormLoad: true, tableViewMode: "addnew", tableResTxt: "Fetching Data...", latefeeViewData: "New Late Fee" }, () => {
            let installmentFormData = InstallmentJson.studentsFormjson;
            axios({
                method: 'get',
                url: `${this.state.env["zqBaseUri"]}/edu/master/getDisplayId/lateFees?orgId=${this.state.orgId}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            })
                .then(res => {
                    this.resetform()
                    Object.keys(installmentFormData).forEach(tab => {
                        installmentFormData[tab].forEach(task => {
                            if (task['name'] == 'id') {
                                task['defaultValue'] = String(res.data.data)
                                task['readOnly'] = true
                            }
                            if (task['name'] == 'typeOfvalue') {
                                task['defaultValue'] = "%"
                            }
                            if (task['name'] == 'charges') {
                                task['label'] = "Charges (%)"
                            }
                        })
                    })
                    this.setState({ studentsFormjson: installmentFormData, viewFormLoad: false, latefeeViewData: `New Late Fee | ${res.data.data}` })
                })
                .catch(err => {
                    this.setState({ tableResTxt: "Error loading Data" })
                })
        })
    }
    handleBackFun = () => {
        this.props.history.push(`${localStorage.getItem('baseURL')}/main/dashboard`);
        this.resetform()
    }
    onPreviewStudentList = (e) => {
        let details = this.state.globalSearch ? e : JSON.parse(e.Item)
        let installmentFormData = InstallmentJson.studentsFormjson;
        this.state.totalApiRes.map((data) => {
            if (data.displayName == e.Id) {
                this.setState({ tabViewForm: true, studentName: e.ID, viewHeader: true, viewFormLoad: true, tableViewMode: "preview", payloadId: details["_id"] })
                Object.keys(installmentFormData).forEach(tab => {
                    installmentFormData[tab].forEach(task => {
                        if (task['name'] == 'id') {
                            task['defaultValue'] = e.Id
                            task['readOnly'] = true
                            task['required'] = false
                        }
                        else if (task['name'] == 'title') {
                            task['defaultValue'] = e.Title
                            task['readOnly'] = false
                            task['required'] = false
                        }
                        else if (task['name'] == 'description') {
                            task['defaultValue'] = e.Description
                            task['readOnly'] = false
                            task['required'] = false
                        }
                        else if (task['name'] == 'campusName') {
                            task['defaultValue'] = "-"
                            task['readOnly'] = false
                            task['required'] = false
                        }
                        else if (task['name'] == 'typeOfvalue') {
                            task['defaultValue'] = details.type
                            task['readOnly'] = false
                            task['required'] = false
                        }
                        else if (task['name'] == 'charges') {
                            task['defaultValue'] = details.type == "Value" || details.type == "value" ? Number(details.amount).toFixed(2) : details.type == "Percentage" ? Number(details.amount).toFixed(0) : details.type;
                            task['readOnly'] = false
                            task['required'] = false
                            task['label'] = details.type == "Value" || details.type == "value" ? "Charges (₹)" : "Charges (%)";
                        }
                        else if (task['name'] == 'per') {
                            task['defaultValue'] = details.every === "Daily" ? "Day" : String(details.every)
                            task['readOnly'] = false
                            task['required'] = false
                        }
                    })
                })
                this.setState({ studentsFormjson: installmentFormData, LoaderStatus: false, viewFormLoad: false, latefeeViewData: `Late Fees | ${e.Id}` })
            }
            else { }
        })
    }
    onInputChanges = (value, item, event, dataS) => {
        let installmentFormData = InstallmentJson.studentsFormjson;
        let installmentFormData1 = InstallmentJson.AddstudentsFormjson;
        if (dataS !== undefined) {
            if (dataS.name == 'typeOfvalue') {
                if (dataS.defaultValue.toLowerCase() == "value") {
                    Object.keys(installmentFormData).forEach(tab => {
                        installmentFormData[tab].forEach(task => {
                            if (task['name'] == 'charges') {
                                task['label'] = "Charges (%)";
                                task['defaultValue'] = Number(task['defaultValue'])
                                // delete task['defaultValue']
                            }
                        })
                    })
                    Object.keys(installmentFormData1).forEach(tab => {
                        installmentFormData1[tab].forEach(task => {
                            if (task['name'] == 'charges') {
                                task['label'] = "Charges (%)";
                                task['defaultValue'] = Number(task['defaultValue'])
                                // delete task['defaultValue']
                            }
                        })
                    })
                }
                else if (dataS.defaultValue.toLowerCase() == "percentage") {
                    Object.keys(installmentFormData1).forEach(tab => {
                        installmentFormData1[tab].forEach(task => {
                            if (task['name'] == 'charges') {
                                task['label'] = "Charges (₹)";
                                task['defaultValue'] = Number(task['defaultValue']).toFixed(2)
                            }
                        })
                    })
                    Object.keys(installmentFormData).forEach(tab => {
                        installmentFormData[tab].forEach(task => {
                            if (task['name'] == 'charges') {
                                task['label'] = "Charges (₹)";
                                task['defaultValue'] = Number(task['defaultValue']).toFixed(2)
                            }
                        })
                    })
                }
            }
            this.setState({ studentsFormjson: installmentFormData })
        }

    }
    onPreviewStudentList1 = (e) => { }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.getTableData()
        });
    };
    moveBackTable = () => {
        this.setState({ tabViewForm: false })
        this.resetform()
    }
    handleTabChange = (tabName, formDatas) => {
        let noOfIns, freqData, dueDate;
        let newStudentsFormjson = this.state.studentsFormjson;
        if (tabName == 2) {
            formDatas["Details"].map((data) => {
                if (data.name == "noOfInstallments") {
                    noOfIns = data.defaultValue
                }
            })
            formDatas["Late Fee Details"].map((data) => {
                if (data.name == "frequencySchedule") {
                    freqData = data.defaultValue
                }
                else if (data.name == "byDueDate") {
                    dueDate = data.defaultValue
                }
            })
        }
    }
    onFormSubmit = (data, item) => {
        this.setState({ isLoader: true, LoaderStatus: true })
        let generalData = data['Details'];
        let scheduleData = data['Late Fee Details']
        let defaultValue1 = {}
        generalData.map(item => {
            if (item.name == "id") {
                defaultValue1['id'] = item['defaultValue'];
            }
            if (item.name == 'title') {
                defaultValue1['title'] = item['defaultValue'];
            }
            if (item.name == 'description') {
                defaultValue1['description'] = item['defaultValue'];
            }
            if (item.name == 'campusName') {
                defaultValue1['campusName'] = item['defaultValue'];
            }
        })
        scheduleData.map(item => {
            if (item.name == 'feesType') {
                defaultValue1['feesType'] = item['defaultValue'];
            }
            if (item.name == "typeOfvalue") {
                defaultValue1['typeOfvalue'] = item['defaultValue'];
            }
            if (item.name == 'charges') {
                defaultValue1['charges'] = item['defaultValue'];
            }
            if (item.name == 'per') {
                defaultValue1['per'] = item['defaultValue'];
            }
        })
        let payload = {
            "displayName": defaultValue1['id'],
            "title": defaultValue1['title'],
            "description": defaultValue1['description'] == undefined || defaultValue1['description'] == null ? "-" : defaultValue1['description'],
            // "campusName": defaultValue1['campusName'] == undefined || defaultValue1['campusName'] == null ? "-" : defaultValue1['campusName'],
            "type": defaultValue1['typeOfvalue'],
            "feeType": defaultValue1['feesType'],
            "amount": Number(defaultValue1['charges']),
            "every": defaultValue1['per'],
            "createdBy": this.state.orgId,
            "orgId": this.state.orgId
        }
        if (this.state.tableViewMode === "preview") {
            let headers = {
                'Authorization': this.state.authToken
            };
            let bodyData = payload;
            axios.put(`${this.state.env["zqBaseUri"]}/edu/master/lateFees?id=${this.state.payloadId}`, bodyData, { headers })
                .then(res => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Updated Successfully", status: "success" }
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                    this.resetform();
                    this.getTableData();
                })
                .catch(err => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Error", status: "error" }
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                })
        }
        else if (this.state.tableViewMode === "addnew") {
            let headers = {
                'Authorization': this.state.authToken
            };
            let bodyData = payload;
            axios.post(` ${this.state.env["zqBaseUri"]}/edu/master/lateFees`, bodyData, { headers })
                .then(res => {
                    if (res.data.status == "success") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Added successfully", status: "success" }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                        this.resetform();
                        this.getTableData();
                    }
                    else if (res.data.status == "error") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: res.data.message, status: "error" }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                        this.resetform();
                        this.getTableData();
                    }
                })
                .catch(err => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Error", status: "error" }
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                })
        }
    }
    closeNotification = () => {
        let snackbarUpdate = { openNotification: false, NotificationMessage: "", status: "" }
        this.setState({ snackbar: snackbarUpdate })
    }
    resetform = () => {
        let installmentFormData = InstallmentJson.studentsFormjson;
        Object.keys(installmentFormData).forEach(tab => {
            installmentFormData[tab].forEach(task => {
                delete task['defaultValue']
                task['readOnly'] = false
                task['error'] = false
                task['required'] = task['requiredBoolean']
                task['validation'] = false
                delete task['clear']
            })
        })
        this.setState({ studentsFormjson: installmentFormData, payloadId: "", status: "" })
    }
    cancelViewData = () => {
        this.setState({ tabViewForm: false });
        this.resetform();
    }
    cleanDatas = () => { }
    allSelect = () => { }
    searchHandle = (value) => { }
    printScreen = () => {
        let page = 1
        let limit = this.state.downloadTotalRecord || 1
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/lateFees?orgId=${this.state.orgId}&page=${page}&limit=${limit}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                this.setState({ sampleStudentsData: [] })
                let installmentTableRes = [];
                if (res.data.status == "success") {
                    if (res.data.data.length == 0) {
                        this.setState({ tableResTxt: "No Data" })
                    }
                    else {
                        res.data.data.map((data) => {
                            installmentTableRes.push({
                                "Id": data.displayName,
                                "Title": data.title,
                                "Description": data.description,
                                "Campus Name": data.campusIdName,
                                "Created on": data.createdAt !== "" ? this.context.dateFormat !== undefined ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                "Created By": data.createdBy,
                                "Status": data.status == 1 ? "Active" : "Inactive",
                            })
                        })
                        this.setState({ sampleStudentsData: installmentTableRes }, () => {
                            setTimeout(() => {
                                window.print()
                                this.getTableData()
                            }, 1);
                        })
                    }
                }
            })
            .catch(err => {
                console.log("error", err)
            })
    }
    onDownload = () => {
        let createXlxsData = [];
        let page = 1
        let limit = this.state.downloadTotalRecord
        this.setState({ LoaderStatus: true }, () => {
            axios({
                method: 'get',
                url: `${this.state.env["zqBaseUri"]}/edu/master/lateFees?orgId=${this.state.orgId}&page=${page}&limit=${limit}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            })
                .then(res => {
                    if (res.data.status == "success") {
                        if (res.data.data.length !== 0) {
                            res.data.data.map((data) => {
                                createXlxsData.push({
                                    "Id": data.displayName,
                                    "Title": data.title,
                                    "Description": data.description,
                                    "Campus Name": data.campusIdName,
                                    "Created on": data.createdAt !== "" ? this.context.dateFormat !== undefined ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                    "Created By": data.createdBy,
                                    "Status": data.status == 1 ? "Active" : "Inactive",
                                })
                            })
                            var ws = xlsx.utils.json_to_sheet(createXlxsData);
                            var wb = xlsx.utils.book_new();
                            xlsx.utils.book_append_sheet(wb, ws, "Late Fee");
                            xlsx.writeFile(wb, "Late fee.xlsx");
                            this.setState({ LoaderStatus: false })
                        }
                    }
                }).catch(err => {
                    console.log(err, 'errorrrrr')
                    this.setState({ LoaderStatus: false })
                })
        })
    }
    render() {
        return (
            <div className="list-of-students-mainDiv">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.tabViewForm == false ?
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title">| Late Fees</p>
                        </div>
                        <div className="masters-body-div">
                            {this.state.containerNav == undefined ? null :
                                <React.Fragment>
                                    <ContainerNavbar containerNav={this.state.containerNav} onAddNew={() => { this.onAddInstallment(); }} searchValue={(searchValue) => this.searchHandle(searchValue)} onDownload={this.onDownload} printScreen={this.printScreen} />
                                    <div className="print-time">{this.state.printTime}</div>
                                </React.Fragment>}
                            <div className="remove-last-child-table">
                                {this.state.sampleStudentsData.length !== 0 ?
                                    <ZqTable
                                        allSelect={this.allSelect}
                                        data={this.state.sampleStudentsData}
                                        rowClick={(item) => { this.onPreviewStudentList(item) }}
                                        onRowCheckBox={(item) => { this.onPreviewStudentList1(item) }}
                                    />
                                    : <p className="noprog-txt">{this.state.tableResTxt}</p>
                                }
                            </div>
                            <div>
                                {this.state.sampleStudentsData.length !== 0 ?
                                    <PaginationUI
                                        total={this.state.totalRecord}
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        currentPage={this.state.page}
                                    />
                                    : null}
                            </div>
                        </div>
                    </React.Fragment> :
                    <React.Fragment>
                        <div className="tab-form-wrapper tab-table">
                            <div className="preview-btns">
                                {this.state.viewHeader == true ?
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| {this.state.latefeeViewData}</h6>
                                    </div> :
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| {this.state.latefeeViewData}</h6>
                                    </div>
                                }
                            </div>
                            <div className="organisation-table">
                                {this.state.viewFormLoad === false ?
                                    <ZenTabs tabData={this.state.studentsFormjson} className="preview-wrap" cleanData={this.cleanDatas} tabEdit={this.state.preview} form={this.state.studentsFormjson} value={0} cancelViewData={this.cancelViewData} onInputChanges={this.onInputChanges} onFormBtnEvent={(item) => { this.formBtnHandle(item); }} onTabFormSubmit={this.onFormSubmit} handleTabChange={this.handleTabChange} key={0} />
                                    : <p className="noprog-txt">{this.state.tableResTxt}</p>}
                            </div>
                        </div>
                    </React.Fragment>
                }
                <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={2000} onClose={this.closeNotification}>
                    <Alert onClose={this.closeNotification}
                        severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
                        {this.state.snackbar.NotificationMessage}
                    </Alert>
                </Snackbar>
            </div>
        )
    }
}
export default LateFee;