import React, { Component } from "react";
import '../../../../../scss/student.scss';
import '../../../../../scss/setup.scss';
import StudentFormJson from './student.json';
import axios from "axios";
import moment from 'moment';
import { Timeline, Drawer } from 'rsuite';
import Loader from "../../../../../utils/loader/loaders";
import ContainerNavbar from "../../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../../utils/Table/table-component";
import PaginationUI from "../../../../../utils/pagination/pagination";
import ZenTabs from '../../../../../components/input/tabs';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import ZqHistory from "../../../../../gigaLayout/change-history";
import DateFormatContext from '../../../../../gigaLayout/context';
import xlsx from 'xlsx';
class ListofStudents extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            campusId: localStorage.getItem('campusId') === "null" || localStorage.getItem('campusId') === null ? undefined : localStorage.getItem('campusId'),
            containerNav: StudentFormJson.containerNav,
            sampleStudentsData: [],
            tableResTxt: "Fetching Data...",
            studentsFormjson: StudentFormJson.studentJson,
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 0,
            tabViewForm: false,
            studentName: "",
            history: false,
            showHistory: false,
            isPreviewStudentData: true,
            historyData: StudentFormJson.HistoryData,
            formLoadStatus: false,
            downloadTotalRecord: 1,
            filterData: [],
            searchValue: ""
        }
    }
    static contextType = DateFormatContext;
    componentDidMount() {
        console.log("contextLabel", this.context.reportLabel)
        this.getDetailsData();
    }
    getDetailsData = () => {
        this.setState({ sampleStudentsData: [], LoaderStatus: true })
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/students?orgId=${this.state.orgId}&campusId=${this.state.campusId}&page=${this.state.page}&limit=${this.state.limit}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                let printArr = res.data.data;
                let studentsDataTable = [];
                let RegID = this.context.reportLabel || "Reg ID"
                let batch = this.context.classLabel || "CLASS/BATCH";
                if (res.data.status == "success") {
                    if (res.data.data.length == 0) {
                        this.setState({ tableResTxt: "No Data", LoaderStatus: false })
                    }
                    else {
                        printArr.map((data) => {
                            studentsDataTable.push({
                                [RegID]: data.regId,
                                // "HEDA Id": "-",
                                "First Name": data.firstName,
                                "Last Name": data.lastName,
                                [batch]: data.programPlan[0].title,
                                "parent name": data.guardianDetails[0].firstName,
                                "Phone no": data.phoneNo,
                                "Email": data.email,
                                "Admitted on": data.admittedOn ? data.admittedOn : "-",
                                "Campus Name": data.campuseDetails ? data.campuseDetails.length !== 0 ? data.campuseDetails.map(item => item['legalName']) : "Play school" : "",
                                "is Final Year": data.isFinalYear === true ? "Yes" : data.isFinalYear === false ? "No" : "-",
                                "Status": data.status == 1 ? "Active" : "Inactive",
                                "Item": JSON.stringify(data)
                            })
                        })
                        this.setState({ sampleStudentsData: studentsDataTable, filterData: studentsDataTable, totalApiRes: res.data, tabViewForm: false, totalRecord: res.data.total, totalPages: res.data.totalPages, page: res.data.currentPage, downloadTotalRecord: res.data.totalRecord, LoaderStatus: false })
                    }
                }
                else {
                    this.setState({ tableResTxt: "Loading Error", LoaderStatus: false })
                }
            })
            .catch(err => {
                this.setState({ tableResTxt: "Loading Error", LoaderStatus: false })
            })
    }
    handleBackFun = () => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`); }
    onPreviewStudentList = (e) => {
        let parsedValueData = JSON.parse(e.Item);
        console.log(e, parsedValueData);
        if (e['First Name'] == parsedValueData.firstName) {
            let RegID = this.context.reportLabel || "Reg ID"
            let batch = this.context.classLabel || "CLASS/BATCH";
            this.setState({ isPreviewStudentData: false, tabViewForm: true, studentName: e[`${RegID}`] });
            let PreviewStudentsJson = StudentFormJson.studentJson;
            let newDateTypeDob = parsedValueData.dob !== "" || parsedValueData.dob !== null ? this.context.dateFormat !== "" ? moment(parsedValueData.dob).format(this.context.dateFormat) : moment(parsedValueData.dob).format('DD/MM/YYYY') : "-";
            let newDateTypeaddOn = parsedValueData.admittedOn !== "" || parsedValueData.admittedOn !== null ? this.context.dateFormat !== "" ? moment(parsedValueData.admittedOn).format(this.context.dateFormat) : moment(parsedValueData.admittedOn).format('DD/MM/YYYY') : "-";
            PreviewStudentsJson['Student Details'].map((task) => {
                if (task['name'] == 'firstName') {
                    task['defaultValue'] = parsedValueData.firstName
                    task['readOnly'] = true
                }
                if (task['name'] == 'lastName') {
                    task['defaultValue'] = parsedValueData.lastName
                    task['readOnly'] = true
                }
                if (task['name'] == 'email') {
                    task['defaultValue'] = parsedValueData.email
                    task['readOnly'] = true
                }
                if (task['name'] == 'regId') {
                    task['label'] = this.context.reportLabel || 'Reg ID'
                    task['defaultValue'] = parsedValueData.regId
                    task['readOnly'] = true
                }
                // if (task['name'] == 'hedaId') {
                //     task['defaultValue'] = "-"
                //     task['readOnly'] = true
                // }
                if (task['name'] == 'campusName') {
                    task['defaultValue'] = e['Campus Name'] ? e['Campus Name'][0] ? e['Campus Name'][0] : "" : ""
                    task['readOnly'] = true
                }
                if (task['name'] == 'isFinalYear') {
                    task['defaultValue'] = parsedValueData.isFinalYear === true ? 'Yes' : parsedValueData.isFinalYear === false ? 'No' : "-"
                    task['readOnly'] = true
                }
                if (task['name'] == 'contactStudent') {
                    task['defaultValue'] = parsedValueData.phoneNo
                    task['readOnly'] = true
                }
                if (task['name'] == 'gender') {
                    task['defaultValue'] = parsedValueData.gender;
                    task['readOnly'] = true
                }
                if (task['name'] == 'dob') {
                    task['defaultValue'] = newDateTypeDob == "Invalid date" ? "-" : newDateTypeDob
                    task['readOnly'] = true
                }
                if (task['name'] == 'displayName') {
                    task['defaultValue'] = parsedValueData.programPlan[0].programCode
                    task['readOnly'] = true
                }
                if (task['name'] == 'programPlan') {
                    task['label'] = this.context.classLabel || "CLASS/BATCH"
                    task['defaultValue'] = parsedValueData.programPlan[0]["title"]
                    task['readOnly'] = true
                }
                if (task['name'] == 'CitizenshipData') {
                    task['defaultValue'] = parsedValueData.citizenship
                    task['readOnly'] = false
                }
                if (task['name'] == 'addmittedDate') {
                    task['defaultValue'] = newDateTypeaddOn == "Invalid date" ? "-" : newDateTypeaddOn
                    task['readOnly'] = true
                }
                if (task['name'] == 'currenyType') {
                    if (task['defaultValue'] == "INR") {
                        PreviewStudentsJson['Student Details'].map((dataTwo) => {
                            if (dataTwo['name'] == 'exchangeRate') {
                                dataTwo['defaultValue'] = "1.00"
                                dataTwo['readOnly'] = true
                            }
                        })
                    }
                    else if (task['defaultValue'] == "USD") {
                        PreviewStudentsJson['Student Details'].map((dataTwo) => {
                            if (dataTwo['name'] == 'exchangeRate') {
                                dataTwo['defaultValue'] = "74.00"
                                dataTwo['readOnly'] = true
                            }
                        })
                    }
                }
            })
            PreviewStudentsJson['parent Details'].map((task) => {
                if (task['name'] == 'fatherName') {
                    task['defaultValue'] = parsedValueData.guardianDetails[0].firstName
                    task['readOnly'] = true
                }
                if (task['name'] == 'contactParent') {
                    task['defaultValue'] = parsedValueData.guardianDetails[0].mobile
                    task['readOnly'] = true
                }
                if (task['name'] == 'addressParent-1') {
                    task['defaultValue'] = parsedValueData.addressDetails.address1
                    task['readOnly'] = true
                }
                if (task['name'] == 'addressParent-2') {
                    task['defaultValue'] = parsedValueData.addressDetails.address2
                    task['readOnly'] = true
                }
                if (task['name'] == 'addressParent-3') {
                    task['defaultValue'] = parsedValueData.addressDetails.address3
                    task['readOnly'] = true
                }
                if (task['name'] == 'City') {
                    task['defaultValue'] = parsedValueData.addressDetails.city
                    task['readOnly'] = true
                }
                if (task['name'] == 'country') {
                    task['defaultValue'] = parsedValueData.addressDetails.country
                    task['readOnly'] = true
                }
                if (task['name'] == 'pincode') {
                    task['defaultValue'] = parsedValueData.addressDetails.pincode
                    task['readOnly'] = true
                }
                if (task['name'] == 'state') {
                    task['defaultValue'] = parsedValueData.addressDetails.state
                    task['readOnly'] = true
                }
                if (task['name'] == 'emailParent') {
                    task['defaultValue'] = parsedValueData.guardianDetails[0].email
                    task['readOnly'] = true
                }
            })
            this.setState({ isPreviewStudentData: true })
        }
    }
    onPreviewStudentList1 = (e) => { }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            if (this.state.searchValue.length == 0) {
                this.getDetailsData()
            }
            else {
                this.searchHandle(this.state.searchValue)
            }
        })
    }
    moveBackTable = () => { this.setState({ tabViewForm: false }) }
    handleTabChange = (tabName, formDatas) => { }
    onFormSubmit = (data, item) => { }
    openHistory = () => { this.setState({ history: true, showHistory: true }); };
    onCloseForm = () => { this.setState({ showHistory: false }); };
    allSelectData = () => { }
    onInputChanges = (value, item, event, dataS) => {
        console.log(dataS);
        let studentFormJsonData = StudentFormJson.studentJson;
        if (dataS.name == "currenyType") {
            if (dataS.defaultValue == "INR") {
                Object.keys(studentFormJsonData).forEach(tab => {
                    studentFormJsonData[tab].forEach(task => {
                        if (task.name == "exchangeRate") {
                            task['defaultValue'] = "74.00";
                        }
                    })
                })
            }
            if (dataS.defaultValue == "USD") {
                Object.keys(studentFormJsonData).forEach(tab => {
                    studentFormJsonData[tab].forEach(task => {
                        if (task.name == "exchangeRate") {
                            task['defaultValue'] = "1.00";
                        }
                    })
                })
            }
        }
        this.setState({ formLoadStatus: false })
    }
    searchHandle = (searchValue) => {
        console.log('searchValue', searchValue)
        this.setState({ LoaderStatus: true })
        this.setState({ searchValue: searchValue });
        if (searchValue.length > 0) {
            return axios.get(`${this.state.env['zqBaseUri']}/edu/master/students?orgId=${this.state.orgId}&page=${this.state.page}&limit=${this.state.limit}&searchKey=${searchValue}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            }).then(res => {
                let printArr = res.data.data;
                let studentsDataTable = [];
                let RegID = this.context.reportLabel || "Reg ID"
                let batch = this.context.classLabel || "CLASS/BATCH";
                this.setState({ sampleStudentsData: [] }, () => {
                    if (res.data.status == "success") {
                        if (res.data.data.length == 0) {
                            this.setState({ tableResTxt: "No Data", sampleStudentsData: [], LoaderStatus: false })
                        }
                        else {
                            printArr.map((data) => {
                                studentsDataTable.push({
                                    [RegID]: data.regId,
                                    // "HEDA Id": "-",
                                    "First Name": data.firstName,
                                    "Last Name": data.lastName,
                                    [batch]: data.programPlan[0].title,
                                    "parent name": data.guardianDetails[0].firstName,
                                    "Phone no": data.phoneNo,
                                    "Email": data.email,
                                    "Admitted on": data.admittedOn ? data.admittedOn : "-",
                                    "Campus Name": data.campuseDetails ? data.campuseDetails.length !== 0 ? data.campuseDetails.map(item => item['legalName']) : "" : "",
                                    "is Final Year": data.isFinalYear === true ? "Yes" : data.isFinalYear === false ? "No" : "-",
                                    // "is Final Year": "-",
                                    "Status": data.status == 1 ? "Active" : "Inactive",
                                    "Item": JSON.stringify(data)
                                })
                            })
                            this.setState({
                                sampleStudentsData: studentsDataTable,
                                totalApiRes: res.data,
                                tabViewForm: false,
                                totalRecord: res.data.total,
                                totalPages: res.data.totalPages,
                                page: res.data.currentPage,
                                downloadTotalRecord: res.data.totalRecord,
                                LoaderStatus: false
                            })
                        }
                    }

                    else {
                        this.setState({ tableResTxt: "Loading Error", })
                    }
                })
            })
                .catch(err => {
                    this.setState({ tableResTxt: "Loading Error", sampleStudentsData: this.state.filterData, LoaderStatus: false })
                })
        }
        else {
            this.getDetailsData()
        }
    }
    printScreen = () => {
        let page = 1
        let limit = this.state.downloadTotalRecord || 1
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/students?orgId=${this.state.orgId}&page=${page}&limit=${limit}`,
            headers: {
                'Authorization': this.state.authToken
            }
        }).then(res => {
            console.log("res", res)
            this.setState({ sampleStudentsData: [] }, () => {
                let printArr = res.data.data;
                let studentsDataTable = [];
                let RegID = this.context.reportLabel || "Reg ID"
                let batch = this.context.classLabel || "CLASS/BATCH";
                if (res.data.status == "success") {
                    if (res.data.data.length == 0) {
                        this.setState({ tableResTxt: "No Data" })
                    }
                    else {
                        printArr.map((data) => {
                            studentsDataTable.push({
                                [RegID]: data.regId,
                                // "HEDA Id": "-",
                                "First Name": data.firstName,
                                "Last Name": data.lastName,
                                [batch]: data.programPlan[0].title,
                                "parent name": data.guardianDetails[0].firstName,
                                "Phone no": data.phoneNo,
                                "Email": data.email,
                                "Admitted on": data.admittedOn ? data.admittedOn : "-",
                                "Campus Name": data.campuseDetails ? data.campuseDetails.length !== 0 ? data.campuseDetails.map(item => item['legalName']) : "" : "",
                                "is Final Year": data.isFinalYear === true ? "Yes" : data.isFinalYear === false ? "No" : "-",
                                // "is Final Year": "-",
                                "Status": data.status == 1 ? "Active" : "Inactive",
                            })
                        })
                        this.setState({
                            sampleStudentsData: studentsDataTable,
                        }, () => {
                            setTimeout(() => {
                                window.print()
                                this.getDetailsData()
                            }, 1);
                        })
                    }
                }
            })
        }
        ).catch(err => {
            console.log(err, 'errorrrrr')
        })
    }
    onDownload = () => {
        let page = 1
        let limit = this.state.downloadTotalRecord || 1
        this.setState({ LoaderStatus: true }, () => {
            try {
                axios({
                    method: 'get',
                    url: `${this.state.env["zqBaseUri"]}/edu/master/students?orgId=${this.state.orgId}&page=${page}&limit=${limit}`,
                    headers: {
                        'Authorization': this.state.authToken
                    }
                }).then(res => {
                    var createXlxsData = [];
                    let RegID = this.context.reportLabel || "Reg ID"
                    let batch = this.context.classLabel || "CLASS/BATCH";
                    if (res.status === 200) {
                        res.data.data.map((data) => {
                            createXlxsData.push({
                                [RegID]: data.regId,
                                // "HEDA Id": "-",
                                "First Name": data.firstName,
                                "Last Name": data.lastName,
                                [batch]: data.programPlan[0].title,
                                "parent name": data.guardianDetails[0].firstName,
                                "Phone no": data.phoneNo,
                                "Email": data.email,
                                "Admitted on": data.admittedOn ? data.admittedOn : "-",
                                "Campus Name": data.campusIdName,
                                "is Final Year": data.isFinalYear === true ? "Yes" : data.isFinalYear === false ? "No" : "-",
                                // "is Final Year": "-",
                                "Status": data.status == 1 ? "Active" : "Inactive",
                            })
                        })
                        var ws = xlsx.utils.json_to_sheet(createXlxsData);
                        var wb = xlsx.utils.book_new();
                        xlsx.utils.book_append_sheet(wb, ws, "Students");
                        xlsx.writeFile(wb, "student.xlsx");
                        this.setState({ LoaderStatus: false })
                    }
                }).catch(err => {
                    console.log(err, 'errorrrrr')
                    this.setState({ LoaderStatus: false })
                })
            } catch (err) {
                console.log("try err", err)
            }
        })
    }
    render() {
        return (
            <div className="list-of-students-mainDiv table-head-padding">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.tabViewForm == false ?
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title">| Students</p>
                        </div>
                        <div className="masters-body-div">
                            {this.state.containerNav == undefined ? null :
                                <React.Fragment>
                                    <ContainerNavbar containerNav={this.state.containerNav} searchValue={(searchValue) => this.searchHandle(searchValue)} onDownload={this.onDownload} printScreen={this.printScreen} />
                                    <div className="print-time">{this.state.printTime}</div>
                                </React.Fragment>
                            }
                            <div className="remove-last-child-table remove-first-child-table">
                                {this.state.sampleStudentsData.length !== 0 ?
                                    < ZqTable
                                        allSelect={this.allSelectData}
                                        data={this.state.sampleStudentsData}
                                        rowClick={(item) => { this.onPreviewStudentList(item) }}
                                        onRowCheckBox={(item) => { this.onPreviewStudentList1(item) }} />
                                    :
                                    <p className="noprog-txt">{this.state.tableResTxt}</p>
                                }
                            </div>
                            <div>
                                {this.state.sampleStudentsData.length !== 0 ?
                                    <PaginationUI
                                        total={this.state.totalRecord}
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        currentPage={this.state.page} />
                                    :
                                    null
                                }
                            </div>
                        </div>
                    </React.Fragment> :
                    <>{this.state.isPreviewStudentData ?
                        <React.Fragment>
                            <div className="tab-form-wrapper tab-table">
                                <div className="preview-btns">
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| Students | {this.state.studentName}</h6>
                                    </div>
                                    {/* <div className="notes-history-wrapper" style={{ right: "86px" }}>
                                        <div className="history-icon-box" title="View History" onClick={this.openHistory}> <RestoreOutlinedIcon className="material-historyIcon" /> <p>History</p></div>
                                    </div> */}
                                </div>
                                <div className="organisation-table student-submit-none">
                                    {this.state.formLoadStatus == true ? <p className="noprog-txt">Loading...</p> :
                                        <ZenTabs tabData={this.state.studentsFormjson} className="preview-wrap" tabEdit={this.state.preview} form={this.state.studentsFormjson} value={0} onInputChanges={this.onInputChanges} onFormBtnEvent={(item) => { this.formBtnHandle(item); }} onTabFormSubmit={this.onFormSubmit} handleTabChange={this.handleTabChange} key={0} />
                                    }
                                </div>
                            </div>
                        </React.Fragment> : <><p className="noprog-txt">{this.state.tableResTxt}</p></>}</>

                }
                {this.state.history ?
                    <Drawer size='xs' placement='right' show={this.state.showHistory}>
                        <Drawer.Header style={{ display: 'flex', paddingRight: '0px' }}>
                            <Drawer.Title className="change-history-title">CHANGE HISTORY</Drawer.Title>
                            <HighlightOffIcon title="Close" onClick={this.onCloseForm} className="change-history-close-btn" />
                        </Drawer.Header>
                        <Drawer.Body>
                            <Timeline align='left'>
                                <ZqHistory historyData={this.state.historyData} onCloseForm={this.onCloseForm} />
                            </Timeline>
                        </Drawer.Body>
                    </Drawer>
                    : null
                }
            </div>
        )
    }
}
export default ListofStudents;