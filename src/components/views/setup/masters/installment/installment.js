import React, { Component } from "react";
import '../../../../../scss/student.scss';
import '../../../../../scss/setup.scss';
import Loader from "../../../../../utils/loader/loaders";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ContainerNavbar from "../../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../../utils/Table/table-component";
import PaginationUI from "../../../../../utils/pagination/pagination";
import ZenTabs from '../../../../../components/input/tabs';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import InstallmentJson from './installment.json';
import axios from "axios";
import moment from 'moment';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import DateFormatContext from '../../../../../gigaLayout/context';
import xlsx from 'xlsx';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
class Installment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            campusId: localStorage.getItem('campusId') === "null" || localStorage.getItem('campusId') === null ? undefined : localStorage.getItem('campusId'),
            containerNav: InstallmentJson.containerNav,
            studentsFormjson: InstallmentJson.studentsFormjson,
            sampleStudentsData: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 1,
            tabViewForm: false,
            studentName: "",
            tableResTxt: "Fetching Data...",
            viewHeader: true,
            totalApiRes: null,
            viewFormLoad: true,
            LoaderStatus: false,
            feesBreakUpArray: [],
            tableViewMode: "",
            payloadId: "",
            snackbar: { openNotification: false, NotificationMessage: "", status: "success" },
            numberOfIns: "1",
            frequencyData: "Week",
            dueDatas: "First Day",
            installmentViewData: "",
            downloadTotalRecord: 0
        }
    }
    static contextType = DateFormatContext;
    componentDidMount() {
        this.getTableData()
    }
    getTableData = () => {
        this.setState({ sampleStudentsData: [] })
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/installments?orgId=${this.state.orgId}&campusId=${this.state.campusId}&page=${this.state.page}&limit=${this.state.limit}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                let installmentTableRes = [];
                // this.getListofTypes()
                if (res.data.status == "success") {
                    if (res.data.data.length == 0) {
                        this.setState({ tableResTxt: "No Data" })
                    }
                    else {
                        res.data.data.map((data) => {
                            installmentTableRes.push({
                                "Id": data.displayName,
                                "Title": data.title,
                                "Description": data.description,
                                "NO. of Installments": data.numberOfInstallments,
                                "Frequency": data.frequency,
                                "Campus Name": data.campusIdName,
                                "Created on": data.createdAt !== "" ? this.context.dateFormat !== "" ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                "Created By": data.createdBy,
                                // "Created on": data.createdAt !== "" ? moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                "Status": data.status == 1 ? "Active" : "Inactive",
                                "Item": JSON.stringify(data)
                            })
                        })
                        this.setState({ sampleStudentsData: installmentTableRes, totalApiRes: res.data.data, tabViewForm: false, totalRecord: res.data.totalRecord, totalPages: res.data.totalPages, page: res.data.currentPage, downloadTotalRecord: res.data.totalRecord })
                    }
                }
                else {
                    this.setState({ tableResTxt: "Error loading Data" })
                }
            })
            .catch(err => { this.setState({ tableResTxt: "Error loading Data" }) })
    }
    onAddInstallment = () => {
        this.setState({ tabViewForm: true, viewHeader: false, viewFormLoad: true, tableViewMode: "addnew", tableResTxt: "Fetching Data...", installmentViewData: "New Installment" }, () => {
            let installmentFormData = InstallmentJson.studentsFormjson;
            axios({
                method: 'get',
                url: `${this.state.env["zqBaseUri"]}/edu/master/getDisplayId/installments?orgId=${this.state.orgId}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            })
                .then(res => {
                    this.resetform()
                    this.getListofTypes()
                    Object.keys(installmentFormData).forEach(tab => {
                        installmentFormData[tab].forEach(task => {
                            if (task['name'] == 'id') {
                                task['defaultValue'] = String(res.data.data)
                                task['readOnly'] = true
                            }
                            // if (task['name'] == 'byDueDate') {
                            //     task['defaultValue'] = "1"
                            //     task['readOnly'] = false
                            //     task['requiredBoolean'] = true
                            // }
                            // if (task['name'] == 'frequencySchedule') {
                            //     task['defaultValue'] = "Week"
                            //     task['readOnly'] = false
                            //     task['requiredBoolean'] = true
                            // }
                            // if (task['name'] == 'noOfInstallments') {
                            //     task['defaultValue'] = "1"
                            //     task['readOnly'] = false
                            //     task['requiredBoolean'] = true
                            // }
                        })
                    })
                    this.setState({ studentsFormjson: installmentFormData, viewFormLoad: false, installmentViewData: `New Installment | ${res.data.data}` })
                })
                .catch(err => {
                    this.setState({ tableResTxt: "Error loading Data" })
                })
        })
    }
    handleBackFun = () => {
        this.props.history.push(`${localStorage.getItem('baseURL')}/main/dashboard`);
        this.resetform()
    }
    onPreviewStudentList = (e) => {
        let details = this.state.globalSearch ? e : JSON.parse(e.Item)
        let noOfIns, freqData, dueDate;
        console.log(details, e);
        let installmentFormData = InstallmentJson.studentsFormjson;
        this.state.totalApiRes.map((data) => {
            installmentFormData["Payment Breakup"] = [
                {
                    "type": "heading",
                    "label": "Frequency",
                    "class": "form-title"
                },
                {
                    "type": "heading",
                    "label": "Percentage",
                    "class": "form-title"
                }
            ];
            details.percentageBreakup.map((dataOne, i) => {
                installmentFormData["Payment Breakup"].push(
                    {
                        "category": "input",
                        "type": "text",
                        "name": "percentage",
                        "label": `Installment ${i + 1}`,
                        "class": "input-wrap setup-input-wrap description-input-wrap rowInline",
                        "readOnly": true,
                        "required": false,
                        "requiredBoolean": false,
                        "clear": false,
                        "defaultValue": String(Number(dataOne.toFixed(2))) + "%"
                    }
                )
                // installmentFormData["Payment Breakup"] = newArr;
            })

            if (details.displayName === e.Id) {
                Object.keys(installmentFormData).forEach(tab => {
                    installmentFormData[tab].forEach(task => {
                        if (task['name'] == 'id') {
                            task['defaultValue'] = e.Id
                            task['readOnly'] = true
                            task['required'] = false
                        }
                        else if (task['name'] == 'title') {
                            task['defaultValue'] = e.Title
                            task['readOnly'] = false
                            task['required'] = false
                        }
                        else if (task['name'] == 'description') {
                            task['defaultValue'] = e.Description
                            task['readOnly'] = false
                            task['required'] = false
                        }
                        else if (task['name'] == 'campusName') {
                            task['defaultValue'] = '-'
                            task['readOnly'] = false
                            task['required'] = false
                        }
                        else if (task['name'] == 'noOfInstallments') {
                            task['defaultValue'] = String(e['NO. of Installments'])
                            task['readOnly'] = false
                            noOfIns = String(e['NO. of Installments'])
                            task['required'] = false
                            task['requiredBoolean'] = false
                        }
                        else if (task['name'] == 'frequencySchedule') {
                            task['defaultValue'] = "Monthly" ? String("Month") : String(e.Frequency)
                            task['readOnly'] = false
                            freqData = "Monthly" ? String("month") : String(e.Frequency)
                            task['required'] = false
                        }
                        else if (task['name'] == 'byDueDate') {
                            task['defaultValue'] = String(details.dueDate)
                            task['readOnly'] = false
                            dueDate = String(details.dueDate)
                            task['required'] = false
                        }
                    })
                })
                this.setState({ tabViewForm: true, studentName: e.ID, viewHeader: true, viewFormLoad: true, tableViewMode: "preview", payloadId: details["_id"] })
                this.setState({ studentsFormjson: installmentFormData, viewFormLoad: false, installmentViewData: `Installments | ${e.Id}` })
            }
            else { }
        })
    }
    onInputChanges = (value, item, event, dataS) => {
        console.log(dataS);
        if (dataS == undefined) { }
        else {
            setTimeout(() => {
                console.log(dataS);
                let commonName = dataS.name == undefined ? "" : dataS.name;
                let noOfIns, freqData, dueDate;
                let newStudentsFormjson = InstallmentJson.studentsFormjson;
                this.setState({ LoaderStatus: true })
                let installmentFormData = InstallmentJson.studentsFormjson;
                if (dataS.defaultValue == "Month") {
                    newStudentsFormjson["Schedule Details"].map(item => {
                        if (item.name == 'byDueDate') {
                            item['options'] = []
                            item['options'] = [
                                {
                                    "label": "5",
                                    "value": "5"
                                },
                                {
                                    "label": "10",
                                    "value": "10"
                                },
                                {
                                    "label": "15",
                                    "value": "15"
                                },
                                {
                                    "label": "20",
                                    "value": "20"
                                }
                            ]
                        }
                    })
                    this.setState({ studentsFormjson: newStudentsFormjson })
                }
                else if (dataS.defaultValue == "Week") {
                    newStudentsFormjson["Schedule Details"].map(item => {
                        if (item.name == 'byDueDate') {
                            item['options'] = []
                            item['options'] = [{
                                label: "Mon",
                                value: "1"
                            },
                            {
                                label: "Tue",
                                value: "2"
                            },
                            {
                                label: "Wed",
                                value: "3"
                            },
                            {
                                label: "Thu",
                                value: "4"
                            },
                            {
                                label: "Fri",
                                value: "5"
                            }]
                        }
                    })
                    this.setState({ studentsFormjson: newStudentsFormjson })
                }
                Object.keys(installmentFormData).forEach(tab => {
                    installmentFormData[tab].forEach(task => {
                        if (dataS.name == "noOfInstallments") {
                            noOfIns = commonName == "noOfInstallments" ? dataS.defaultValue : task.defaultValue
                            this.setState({ numberOfIns: dataS.defaultValue })
                        }
                    })
                })
                newStudentsFormjson["Schedule Details"].map((data) => {
                    if (dataS.name == "frequencySchedule") {
                        freqData = commonName == "frequencySchedule" ? dataS.defaultValue : data.defaultValue
                        this.setState({ frequencyData: freqData == "Monthly" ? "month" : freqData })
                    }
                    else if (dataS.name == "byDueDate") {
                        dueDate = commonName == "byDueDate" ? dataS.defaultValue : data.defaultValue
                        this.setState({ dueDatas: dueDate })
                    }
                })
                if (dataS.name == "noOfInstallments" || dataS.name == "frequencySchedule" || dataS.name == "byDueDate") {
                    if (commonName == "noOfInstallments") {
                        newStudentsFormjson["General"].map((data) => {
                            if (dataS.name == "noOfInstallments") {
                                noOfIns = String(dataS.defaultValue)
                            }
                        })
                    }
                    else if (commonName == "frequencySchedule") {
                        newStudentsFormjson["Schedule Details"].map((data) => {
                            if (dataS.name == "frequencySchedule") {
                                freqData = String(dataS.defaultValue)
                            }
                        })
                    }
                    else if (commonName == "byDueDate") {
                        newStudentsFormjson["Schedule Details"].map((data) => {
                            if (dataS.name == "byDueDate") {
                                dueDate = String(dataS.defaultValue)
                            }
                        })
                    }
                    setTimeout(() => {
                        axios({
                            method: 'get',
                            url: `${this.state.env["zqBaseUri"]}/edu/master/installments/installmentDueDateCalculation?noOfInst=${this.state.numberOfIns}&frequency=${this.state.frequencyData}&dueDate=${this.state.dueDatas}`,
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization': this.state.authToken
                            }
                        })
                            .then(res => {
                                let newArr = [
                                    {
                                        "type": "heading",
                                        "label": "Frequency",
                                        "class": "form-title"
                                    },
                                    {
                                        "type": "heading",
                                        "label": "Percentage",
                                        "class": "form-title"
                                    }
                                ];
                                res.data.data.map((data, i) => {
                                    newArr.push(
                                        // {
                                        //     "category": "input",
                                        //     "type": "text",
                                        //     "name": "dueOn",
                                        //     "label": "Due On",
                                        //     "class": "input-wrap input-wrap-col-2 col-2-first",
                                        //     "readOnly": true,
                                        //     "required": false,
                                        //     "requiredBoolean": false,
                                        //     "clear": false,
                                        //     "defaultValue": data.dueDate !== "" ? this.context.dateFormat !== "" ? moment(data.dueDate).format(this.context.dateFormat) : moment(data.dueDate).format('DD/MM/YYYY') : "-"
                                        // },
                                        // {
                                        //     "type": "heading",
                                        //     "label": "Installment",
                                        //     "class": "form-title"
                                        // },
                                        // {
                                        //     "type": "heading",
                                        //     "label": "Percentage",
                                        //     "class": "form-title"
                                        // },
                                        {
                                            "category": "input",
                                            "type": "text",
                                            "name": "percentage",
                                            "label": `Installment ${i + 1}`,
                                            "class": "input-wrap setup-input-wrap description-input-wrap rowInline",
                                            "readOnly": true,
                                            "required": false,
                                            "requiredBoolean": false,
                                            "clear": false,
                                            "defaultValue": String(Number(data.percentage.toFixed(2))) + "%"
                                        }
                                    )
                                })
                                newStudentsFormjson["Payment Breakup"] = [];
                                newStudentsFormjson["Payment Breakup"] = newArr;
                                this.setState({ studentsFormjson: newStudentsFormjson, LoaderStatus: false, feesBreakUpArray: res.data.data })

                            })
                            .catch(err => { this.setState({ LoaderStatus: false }) })
                    }, 1000)
                }
                else { }
            }, 300)
        }
    }
    onPreviewStudentList1 = (e) => { }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.getTableData()
        });
    };
    moveBackTable = () => {
        this.setState({ tabViewForm: false })
        this.resetform()
        // this.getTableData()
    }
    handleTabChange = (tabName, formDatas) => {
        let noOfIns, freqData, dueDate;
        if (tabName == 2) {
            formDatas["General"].map((data) => {
                if (data.name == "noOfInstallments") {
                    noOfIns = data.defaultValue
                }
            })
            formDatas["Schedule Details"].map((data) => {
                if (data.name == "frequencySchedule") {
                    freqData = data.defaultValue
                }
                else if (data.name == "byDueDate") {
                    dueDate = data.defaultValue
                }
            })
        }
    }
    getListofTypes = () => {
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/installments/installmentDueDateCalculation?noOfInst=${this.state.numberOfIns}&frequency=${this.state.frequencyData}&dueDate=${this.state.dueDatas}%20Day#/`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                let newStudentsFormjson = this.state.studentsFormjson;
                let newArr = [
                    {
                        "type": "heading",
                        "label": "Frequency",
                        "class": "form-title"
                    },
                    {
                        "type": "heading",
                        "label": "Percentage",
                        "class": "form-title"
                    }
                ];
                res.data.data.map((data, i) => {
                    newArr.push(
                        // {
                        //     "category": "input",
                        //     "type": "text",
                        //     "name": "dueOn",
                        //     "label": "Due On",
                        //     "class": "input-wrap col-2-first",
                        //     "readOnly": true,
                        //     "required": false,
                        //     "requiredBoolean": false,
                        //     "clear": false,
                        //     "defaultValue": data.dueDate !== "" ? this.context.dateFormat !== "" ? moment(data.dueDate).format(this.context.dateFormat) : moment(data.dueDate).format('DD/MM/YYYY') : "-",
                        //     // "defaultValue": data.dueDate !== "" ? moment(data.dueDate).format('DD/MM/YYYY') : "-"
                        // },
                        // {
                        //     "type": "heading",
                        //     "label": "Installment",
                        //     "class": "form-title"
                        // },
                        // {
                        //     "type": "heading",
                        //     "label": "Percentage",
                        //     "class": "form-title"
                        // },
                        {
                            "category": "input",
                            "type": "text",
                            "name": "percentage",
                            "label": `Installment ${i + 1}`,
                            "class": "input-wrap setup-input-wrap description-input-wrap rowInline",
                            "readOnly": true,
                            "required": false,
                            "requiredBoolean": false,
                            "clear": false,
                            "defaultValue": String(data.percentage)
                        }
                    )
                })
                newStudentsFormjson["Payment Breakup"] = newArr;
                this.setState({ studentsFormjson: newStudentsFormjson })

            })
            .catch(err => { })
    }
    onFormSubmit = (data, item) => {
        this.setState({ isLoader: true, LoaderStatus: true })
        let generalData = data['General'];
        let scheduleData = data['Schedule Details']
        let defaultValue1 = {}
        generalData.map(item => {
            if (item.name == "id") {
                defaultValue1['id'] = item['defaultValue'];
            }
            if (item.name == 'title') {
                defaultValue1['title'] = item['defaultValue'];
            }
            if (item.name == 'description') {
                defaultValue1['description'] = item['defaultValue'];
            }
            if (item.name == 'campusName') {
                defaultValue1['campusName'] = item['defaultValue'];
            }
            if (item.name == 'noOfInstallments') {
                defaultValue1['noOfInstallments'] = item['defaultValue'];
            }
        })
        scheduleData.map(item => {
            if (item.name == "frequencySchedule") {
                defaultValue1['frequencySchedule'] = item['defaultValue'];
            }
            if (item.name == 'byDueDate') {
                defaultValue1['byDueDate'] = item['defaultValue'];
            }
        })
        let payload = {
            "displayName": defaultValue1['id'],
            "title": defaultValue1['title'],
            "description": defaultValue1['description'] == undefined || defaultValue1['description'] == null ? "-" : defaultValue1['description'],
            // "campusName": defaultValue1['campusName'] == undefined || defaultValue1['campusName'] == null ? "-" : defaultValue1['campusName'],
            "numberOfInstallments": defaultValue1['noOfInstallments'],
            "frequency": defaultValue1['frequencySchedule'],
            "dueDate": defaultValue1['byDueDate'],
            "feesBreakUp": this.state.feesBreakUpArray,
            "createdBy": this.state.orgId,
            "orgId": this.state.orgId,
        }
        if (this.state.tableViewMode === "preview") {
            let headers = {
                'Authorization': this.state.authToken
            };
            let bodyData = payload;
            axios.put(`${this.state.env["zqBaseUri"]}/edu/master/installments?id=${this.state.payloadId}`, bodyData, { headers })
                .then(res => {
                    if (res.data.status == "Database error") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: res.data.message, status: "error", }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate, tabViewForm: false });
                        this.resetform();
                        this.getTableData();
                        setTimeout(() => {
                            snackbarUpdate.openNotification = false
                            this.setState({ snackbar: snackbarUpdate })
                        }, 1500)
                    }
                    else if (res.data.status == "success") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Updated Successfully", status: "success", }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate, tabViewForm: false });
                        this.resetform();
                        this.getTableData();
                        setTimeout(() => {
                            snackbarUpdate.openNotification = false
                            this.setState({ snackbar: snackbarUpdate })
                        }, 1500)
                    }
                })
                .catch(err => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Error", status: "error" }
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate, tabViewForm: false });
                    setTimeout(() => {
                        snackbarUpdate.openNotification = false
                        this.setState({ snackbar: snackbarUpdate })
                    }, 1500)
                })
        }
        else if (this.state.tableViewMode === "addnew") {
            let headers = {
                'Authorization': this.state.authToken
            };
            let bodyData = payload;
            axios.post(`${this.state.env["zqBaseUri"]}/edu/master/installments`, bodyData, { headers })
                .then(res => {
                    if (res.data.status == "Database error") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: res.data.message, status: "error" }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate, tabViewForm: false });
                        setTimeout(() => {
                            snackbarUpdate.openNotification = false
                            this.setState({ snackbar: snackbarUpdate })
                        }, 1500)
                        this.resetform();
                        this.getTableData();
                    }
                    else if (res.data.status == "success") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Added Successfully", status: "success" }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate, tabViewForm: false });
                        setTimeout(() => {
                            snackbarUpdate.openNotification = false
                            this.setState({ snackbar: snackbarUpdate })
                        }, 1500)
                        this.resetform();
                        this.getTableData();
                    }
                })
                .catch(err => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Error", status: "error" }
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate, tabViewForm: false });
                    setTimeout(() => {
                        snackbarUpdate.openNotification = false
                        this.setState({ snackbar: snackbarUpdate })
                    }, 1500)
                })
        }
    }
    closeNotification = () => {
        let snackbarUpdate = { openNotification: false, NotificationMessage: "", status: "" }
        this.setState({ snackbar: snackbarUpdate })
    }
    resetform = () => {
        let installmentFormData = InstallmentJson.studentsFormjson;
        let a = [
            {
                "category": "input",
                "type": "text",
                "name": "percentage",
                "label": "Percentage",
                "class": "input-wrap setup-input-wrap description-input-wrap rowInline",
                "readOnly": true,
                "required": false,
                "requiredBoolean": false,
                "clear": false
            }
        ]
        InstallmentJson.AddstudentsFormjson = a;
        Object.keys(installmentFormData).forEach(tab => {
            installmentFormData[tab].forEach(task => {
                delete task['defaultValue']
                task['readOnly'] = false
                task['error'] = false
                task['required'] = task['requiredBoolean']
                task['validation'] = false
                delete task['clear']
            })
        })
        this.setState({ studentsFormjson: installmentFormData, payloadId: "", status: "" })
    }
    cancelViewData = () => {
        this.setState({ tabViewForm: false });
        this.resetform();
    }
    cleanDatas = () => { }
    allSelect = () => { }
    searchHandle = (value) => { }
    printScreen = () => {
        let page = 1
        let limit = this.state.downloadTotalRecord || 1
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/installments?orgId=${this.state.orgId}&page=${page}&limit=${limit}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                this.setState({ sampleStudentsData: [] })
                let installmentTableRes = [];
                if (res.data.status == "success") {
                    if (res.data.data.length == 0) {
                        this.setState({ tableResTxt: "No Data" })
                    }
                    else {
                        res.data.data.map((data) => {
                            installmentTableRes.push({
                                "Id": data.displayName,
                                "Title": data.title,
                                "Description": data.description,
                                "NO. of Installments": data.numberOfInstallments,
                                "Frequency": data.frequency,
                                "Campus Name": data.campusIdName,
                                "Created on": data.createdAt !== "" ? this.context.dateFormat !== "" ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                "Created By": data.createdBy,
                                // "Created on": data.createdAt !== "" ? moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                "Status": data.status == 1 ? "Active" : "Inactive",
                                "Item": JSON.stringify(data)
                            })
                        })
                        this.setState({ sampleStudentsData: installmentTableRes }, () => {
                            setTimeout(() => {
                                window.print()
                                this.getTableData()
                            }, 1);
                        })
                    }
                }
            })
            .catch(err => {
                console.log("error", err)
            })
    }
    onDownload = () => {
        let createXlxsData = [];
        let page = 1
        let limit = this.state.downloadTotalRecord
        this.setState({ LoaderStatus: true }, () => {
            axios({
                method: 'get',
                url: `${this.state.env["zqBaseUri"]}/edu/master/installments?orgId=${this.state.orgId}&page=${page}&limit=${limit}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            })
                .then(res => {
                    if (res.data.status == "success") {
                        if (res.data.data.length !== 0) {
                            res.data.data.map((data) => {
                                createXlxsData.push({
                                    "Id": data.displayName,
                                    "Title": data.title,
                                    "Description": data.description,
                                    "NO. of Installments": data.numberOfInstallments,
                                    "Frequency": data.frequency,
                                    "Campus Name": data.campusIdName,
                                    "Created on": data.createdAt !== "" ? this.context.dateFormat !== "" ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                    "Created By": data.createdBy,
                                    "Status": data.status == 1 ? "Active" : "Inactive",
                                })
                            })
                            var ws = xlsx.utils.json_to_sheet(createXlxsData);
                            var wb = xlsx.utils.book_new();
                            xlsx.utils.book_append_sheet(wb, ws, "Installment");
                            xlsx.writeFile(wb, "Installment.xlsx");
                            this.setState({ LoaderStatus: false })
                        }
                    }
                }).catch(err => {
                    console.log(err, 'errorrrrr')
                    this.setState({ LoaderStatus: false })
                })
        })
    }
    render() {
        return (
            <div className="list-of-students-mainDiv installment-inputGap">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.tabViewForm == false ?
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title">| Installments</p>
                        </div>
                        <div className="masters-body-div">
                            {this.state.containerNav == undefined ? null :
                                <React.Fragment>
                                    <ContainerNavbar containerNav={this.state.containerNav} onAddNew={() => { this.onAddInstallment(); }} searchValue={(searchValue) => this.searchHandle(searchValue)} onDownload={this.onDownload} printScreen={this.printScreen} />
                                    <div className="print-time">{this.state.printTime}</div>
                                </React.Fragment>}
                            <div className="remove-last-child-table">
                                {this.state.sampleStudentsData.length !== 0 ?
                                    <ZqTable
                                        allSelect={this.allSelect}
                                        data={this.state.sampleStudentsData}
                                        rowClick={(item) => { this.onPreviewStudentList(item) }}
                                        onRowCheckBox={(item) => { this.onPreviewStudentList1(item) }}
                                    />
                                    : <p className="noprog-txt">{this.state.tableResTxt}</p>
                                }
                            </div>
                            <div>
                                {this.state.sampleStudentsData.length !== 0 ?
                                    <PaginationUI
                                        total={this.state.totalRecord}
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        currentPage={this.state.page}
                                    />
                                    : null}
                            </div>
                        </div>
                    </React.Fragment> :
                    <React.Fragment>
                        <div className="tab-form-wrapper tab-table">
                            <div className="preview-btns">
                                {this.state.viewHeader == true ?
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| {this.state.installmentViewData}</h6>
                                    </div> :
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| {this.state.installmentViewData}</h6>
                                    </div>
                                }
                            </div>
                            <div className="organisation-table">
                                {this.state.viewFormLoad === false ?
                                    <ZenTabs tabData={this.state.studentsFormjson} className="preview-wrap" tabEdit={this.state.preview} cleanData={this.cleanDatas} cancelViewData={this.cancelViewData} form={this.state.studentsFormjson} value={0} onInputChanges={this.onInputChanges} onFormBtnEvent={(item) => { this.formBtnHandle(item); }} onTabFormSubmit={this.onFormSubmit} handleTabChange={this.handleTabChange} key={0} />
                                    : <p className="noprog-txt">{this.state.tableResTxt}</p>}
                            </div>
                        </div>
                    </React.Fragment>
                }
                <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={2500} onClose={this.closeNotification}>
                    <Alert onClose={this.closeNotification}
                        severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
                        {this.state.snackbar.NotificationMessage}
                    </Alert>
                </Snackbar>
            </div>
        )
    }
}
export default Installment;