import React, { Component } from "react";
import '../../../../../scss/student.scss';
import '../../../../../scss/setup.scss';
import Loader from "../../../../../utils/loader/loaders";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ContainerNavbar from "../../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../../utils/Table/table-component";
import PaginationUI from "../../../../../utils/pagination/pagination";
import ZenTabs from '../../../../../components/input/tabs';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import InstallmentJson from './feeStructure.json';
import axios from "axios";
import moment from 'moment';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import DateFormatContext from '../../../../../gigaLayout/context';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
class ListofFeeStructure extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            campusId: localStorage.getItem('campusId') === "null" || localStorage.getItem('campusId') === null ? undefined : localStorage.getItem('campusId'),
            containerNav: InstallmentJson.containerNav,
            studentsFormjson: InstallmentJson.studentsFormjson,
            sampleStudentsData: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 1,
            tabViewForm: false,
            studentName: "",
            tableResTxt: "Fetching Data...",
            viewHeader: true,
            totalApiRes: null,
            viewFormLoad: true,
            LoaderStatus: false,
            feesBreakUpArray: [],
            tableViewMode: "",
            payloadId: "",
            snackbar: { openNotification: false, NotificationMessage: "", status: "success" },
            TotalApiResData: [],
            viewIdData: ""
        }
    }
    static contextType = DateFormatContext;
    componentDidMount() {
        this.getTableData()
        this.getInputOptions()
    }
    getTableData = () => {
        this.setState({ LoaderStatus: true })
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/feeStructure?orgId=${this.state.orgId}&campusId=${this.state.campusId}&page=${this.state.page}&limit=${this.state.limit}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                let installmentTableRes = [];
                this.setState({ TotalApiResData: res.data.data })
                if (res.data.status == "success") {
                    if (res.data.data.length == 0) {
                        this.setState({ tableResTxt: "No Data" })
                    }
                    else {

                        res.data.data.map((data) => {
                            let m = [];
                            data.feeTypes.map((dataTwo) => {
                                m.push(dataTwo)
                            })
                            installmentTableRes.push(
                                {
                                    "Id": data.displayName,
                                    "TITLE": data.title,
                                    "DESCRIPTION": data.description,
                                    "Constiting of": m.toString(),
                                    "Campus Name": "-",
                                    "CREATED ON": data.createdAt !== "" ? this.context.dateFormat !== "" ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                    "CREATED BY": data.createdBy,
                                    "Status": data.status == 1 ? "Active" : "Inactive",
                                    "Item": JSON.stringify(data)
                                }
                            )
                        })

                        this.setState({
                            sampleStudentsData: installmentTableRes, totalApiRes: res.data.data, tabViewForm: false, totalRecord: res.data.total, totalPages: res.data.totalPages, page: res.data.currentPage
                            , LoaderStatus: false
                        })
                    }
                }
                else {
                    this.setState({ tableResTxt: "Error loading Data", LoaderStatus: false })
                }
            })
            .catch(err => { this.setState({ tableResTxt: "Error loading Data", LoaderStatus: false }) })
    }
    onAddInstallment = () => {
        this.setState({ tabViewForm: true, viewHeader: false, viewFormLoad: true, tableViewMode: "addnew", tableResTxt: "Fetching Data...", viewIdData: "New Fee Structure" }, () => {
            let installmentFormData = InstallmentJson.studentsFormjson;
            axios({
                method: 'get',
                url: `${this.state.env["zqBaseUri"]}/edu/master/feeTypes?orgId=${this.state.orgId}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            })
                .then(res => {
                    let feeTypeOption = [];
                    res.data.data.map((data) => {
                        feeTypeOption.push({
                            "label": data.title,
                            "value": data._id
                        })
                    })
                    Object.keys(installmentFormData).forEach(tab => {
                        installmentFormData[tab].forEach(task => {
                            if (task['name'] == 'feeTypes') {
                                task['options'] = feeTypeOption
                            }
                        })
                    })
                    this.setState({ studentsFormjson: installmentFormData, viewFormLoad: false })
                })
                .catch(err => { })
            axios({
                method: 'get',
                url: `${this.state.env["zqBaseUri"]}/edu/master/getDisplayId/feeStructure?orgId=${this.state.orgId}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            })
                .then(res => {
                    this.resetform()
                    Object.keys(installmentFormData).forEach(tab => {
                        installmentFormData[tab].forEach(task => {
                            if (task['name'] == 'Id') {
                                task['defaultValue'] = String(res.data.data)
                                task['readOnly'] = true
                            }
                        })
                    })
                    this.setState({ studentsFormjson: installmentFormData, viewFormLoad: false, viewIdData: `New Fee Structure | ${res.data.data}` })
                })
                .catch(err => {
                    this.setState({ tableResTxt: "Error loading Data" })
                })
        })
    }
    handleBackFun = () => {
        this.props.history.push(`${localStorage.getItem('baseURL')}/main/dashboard`);
        this.resetform()
    }
    getInputOptions = () => {
        let installmentFormData = InstallmentJson.studentsFormjson;
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/feeTypes?orgId=${this.state.orgId}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                this.setState({ TotalApiResData: res.data.data })
                let feeTypeOption = [];
                res.data.data.map((data) => {
                    feeTypeOption.push({
                        "label": data.title,
                        "value": data._id
                    })
                })
                Object.keys(installmentFormData).forEach(tab => {
                    installmentFormData[tab].forEach(task => {
                        if (task['name'] == 'feeTypes') {
                            task['options'] = feeTypeOption
                        }
                    })
                })
                this.setState({ studentsFormjson: installmentFormData })
            })
            .catch(err => { })
    }
    onPreviewStudentList = (e) => {
        this.setState({ viewIdData: `Fee Structures | ${e.Id}` })
        let details = this.state.globalSearch ? e : JSON.parse(e.Item)
        let installmentFormData = InstallmentJson.studentsFormjson;
        this.state.totalApiRes.map((data) => {
            if (details.displayName == e.Id) {
                this.setState({ tabViewForm: true, studentName: e.ID, viewHeader: true, viewFormLoad: true, tableViewMode: "preview", payloadId: details["_id"] })
                Object.keys(installmentFormData).forEach(tab => {
                    installmentFormData[tab].forEach(task => {
                        if (task['name'] == 'Id') {
                            task['defaultValue'] = details.displayName
                            task['readOnly'] = true
                            task['required'] = false
                        }
                        else if (task['name'] == 'title') {
                            task['defaultValue'] = details.title
                            task['readOnly'] = false
                            task['required'] = false
                        }
                        else if (task['name'] == 'description') {
                            task['defaultValue'] = details.description
                            task['readOnly'] = false
                            task['required'] = false
                        }
                        else if (task['name'] == 'feeTypes') {
                            task['defaultValue'] = details.feeTypeIds
                            task['readOnly'] = false
                            task['required'] = false
                            task['requiredBoolean'] = false
                        }
                        else if (task['name'] == 'campusName') {
                            task['defaultValue'] = "-"
                            task['readOnly'] = false
                            task['required'] = false
                            task['requiredBoolean'] = false
                        }
                    })
                })
                this.setState({ studentsFormjson: installmentFormData, LoaderStatus: false, viewFormLoad: false })
            }
            else { }
        })
    }
    onInputChanges = (value, item, event, dataS) => { }
    onPreviewStudentList1 = (e) => { }
    onPaginationChange = (page, limit) => {

        this.setState({ page: page, limit: limit }, () => {
            this.getTableData()
        });
    };
    moveBackTable = () => {
        this.setState({ tabViewForm: false })
        this.resetform()
    }
    handleTabChange = (tabName, formDatas) => { }
    onFormSubmit = (data, item) => {
        this.setState({ isLoader: true, LoaderStatus: true })
        let installmentFormData = InstallmentJson.studentsFormjson;
        let defaultValue = {};
        Object.keys(installmentFormData).forEach(tab => {
            installmentFormData[tab].forEach(item => {
                if (item.name == "Id") {
                    defaultValue['Id'] = item['defaultValue'];
                }
                if (item.name == "title") {
                    defaultValue['title'] = item['defaultValue'];
                }
                if (item.name == "description") {
                    defaultValue['description'] = item['defaultValue'];
                }
                if (item.name == "campusName") {
                    defaultValue['campusName'] = item['defaultValue'];
                }
                if (item.name == "feeTypes") {
                    defaultValue['feeTypes'] = [];
                    defaultValue['feeTypes'].push(item['defaultValue']);
                }
            })
        })
        let finalTypeId = [];
        this.state.TotalApiResData.map((data) => {
            if (data['feeTypes'] !== undefined) {
                defaultValue['feeTypes'][0].map((dataOne) => {
                    if (data.title == dataOne) {
                        finalTypeId.push(data._id)
                    }
                })
            }
        })
        let payload = {
            "displayName": defaultValue['Id'],
            "title": defaultValue['title'],
            "description": defaultValue['description'] == undefined || defaultValue['description'] == null ? "-" : defaultValue['description'],
            "feeTypeIds": defaultValue.feeTypes[0],
            // "campusName": defaultValue['campusName'] == undefined || defaultValue['campusName'] == null ? "-" : defaultValue['campusName'],
            "createdBy": this.state.orgId,
            "orgId": this.state.orgId
        }
        if (this.state.tableViewMode === "preview") {
            let headers = {
                'Authorization': this.state.authToken
            };
            let bodyData = payload;
            axios.put(`${this.state.env["zqBaseUri"]}/edu/master/feeStructure?id=${this.state.payloadId}`, bodyData, { headers })
                .then(res => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Updated Successfully", status: "success" }
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                    this.resetform();
                    this.getTableData();
                    let snackbarUpdate1 = { openNotification: false, NotificationMessage: "", status: "" }
                    setTimeout(() => { this.setState({ snackbar: snackbarUpdate1 }) }, 1500)
                })
                .catch(err => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Error", status: "error" }
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                    let snackbarUpdate1 = { openNotification: false, NotificationMessage: "", status: "" }
                    setTimeout(() => { this.setState({ snackbar: snackbarUpdate1 }) }, 1500)
                })
        }
        else if (this.state.tableViewMode === "addnew") {
            let headers = {
                'Authorization': this.state.authToken
            };
            let bodyData = payload;
            axios.post(` ${this.state.env["zqBaseUri"]}/edu/master/feeStructure`, bodyData, { headers })
                .then(res => {
                    if (res.data.status == "success") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Added successfully", status: "success" }
                        let snackbarUpdate1 = { openNotification: false, NotificationMessage: "", status: "" }
                        setTimeout(() => { this.setState({ snackbar: snackbarUpdate1 }) }, 1500)
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                        this.resetform();
                        this.getTableData();
                    }
                    else if (res.data.status == "error") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: res.data.message, status: "error" }
                        this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                        this.resetform();
                        this.getTableData();
                        let snackbarUpdate1 = { openNotification: false, NotificationMessage: "", status: "" }
                        setTimeout(() => { this.setState({ snackbar: snackbarUpdate1 }) }, 1500)
                    }
                })
                .catch(err => {
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Error", status: "error" }
                    this.setState({ payloadId: "", LoaderStatus: false, snackbar: snackbarUpdate });
                    let snackbarUpdate1 = { openNotification: false, NotificationMessage: "", status: "" }
                    setTimeout(() => { this.setState({ snackbar: snackbarUpdate1 }) }, 1500)
                })
        }
    }
    closeNotification = () => {
        let snackbarUpdate = { openNotification: false, NotificationMessage: "", status: "" }
        this.setState({ snackbar: snackbarUpdate })
    }
    resetform = () => {
        let installmentFormData = InstallmentJson.studentsFormjson;
        Object.keys(installmentFormData).forEach(tab => {
            installmentFormData[tab].forEach(task => {
                delete task['defaultValue']
                task['readOnly'] = false
                task['error'] = false
                task['required'] = task['requiredBoolean']
                task['validation'] = false
                delete task['clear']
            })
        })
        this.setState({ studentsFormjson: installmentFormData, payloadId: "", status: "" })
    }
    cancelViewData = () => {
        this.setState({ tabViewForm: false });
        this.resetform();
    }
    allSelect = () => { }
    render() {
        return (
            <div className="list-of-students-mainDiv concession-data">
                {this.state.LoaderStatus ? <Loader /> : null}
                {this.state.tabViewForm == false ?
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title">| Fee Structures</p>
                        </div>
                        <div className="masters-body-div">
                            {this.state.containerNav == undefined ? null :
                                <React.Fragment>
                                    <ContainerNavbar containerNav={this.state.containerNav} onAddNew={() => { this.onAddInstallment(); }} />
                                    <div className="print-time">{this.state.printTime}</div>
                                </React.Fragment>}
                            <div className="remove-last-child-table">
                                {this.state.sampleStudentsData.length == 0 || this.state.LoaderStatus ?

                                    <p className="noprog-txt">{this.state.tableResTxt}</p> :
                                    <ZqTable
                                        allSelect={this.allSelect}
                                        data={this.state.sampleStudentsData}
                                        rowClick={(item) => { this.onPreviewStudentList(item) }}
                                        onRowCheckBox={(item) => { this.onPreviewStudentList1(item) }}
                                    />
                                }
                            </div>
                            <div>
                                {this.state.sampleStudentsData.length == 0 || this.state.LoaderStatus ? null :
                                    <PaginationUI
                                        total={this.state.totalRecord}
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        currentPage={this.state.page}
                                    />}
                            </div>
                        </div>
                    </React.Fragment> :
                    <React.Fragment>
                        <div className="tab-form-wrapper tab-table">
                            <div className="preview-btns">
                                {this.state.viewHeader == true ?
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| {this.state.viewIdData}</h6>
                                    </div> :
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| {this.state.viewIdData}</h6>
                                    </div>
                                }
                            </div>
                            <div className="organisation-table">
                                {this.state.viewFormLoad === false ?
                                    <ZenTabs tabData={this.state.studentsFormjson} className="preview-wrap" tabEdit={this.state.preview} form={this.state.studentsFormjson} value={0} cancelViewData={this.cancelViewData} onInputChanges={this.onInputChanges} onFormBtnEvent={(item) => { this.formBtnHandle(item); }} onTabFormSubmit={this.onFormSubmit} handleTabChange={this.handleTabChange} key={0} />
                                    : <p className="noprog-txt">{this.state.tableResTxt}</p>}
                            </div>
                        </div>
                    </React.Fragment>
                }
                <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={6000} onClose={this.closeNotification}>
                    <Alert onClose={this.closeNotification}
                        severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
                        {this.state.snackbar.NotificationMessage}
                    </Alert>
                </Snackbar>
            </div>
        )
    }
}
export default ListofFeeStructure;