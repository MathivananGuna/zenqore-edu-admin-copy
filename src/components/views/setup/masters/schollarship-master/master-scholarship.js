import React, { Component } from 'react';
import '../../../../../scss/fees-type.scss';
import '../../../../../scss/setup.scss';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import ContainerNavbar from "../../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../../utils/Table/table-component";
import PaginationUI from "../../../../../utils/pagination/pagination";
import Loader from "../../../../../utils/loader/loaders";
import ScholarshipData from './scholarship-data-master.json';
import ZenForm from "../../../../input/zqform";
import axios from 'axios';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import moment from 'moment';
import DateFormatContext from '../../../../../gigaLayout/context';
import xlsx from 'xlsx';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
class Loans extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orgName: localStorage.getItem('orgName'),
            orgId: localStorage.getItem('orgId'),
            authToken: localStorage.getItem('auth_token'),
            campusId: localStorage.getItem('campusId') === "null" || localStorage.getItem('campusId') === null ? undefined : localStorage.getItem('campusId'),
            // orgId: '5fa8daece3eb1f18d4250e98',
            // authToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoibXVuaXlhcmFqLm5lZWxhbWVnYW1AemVucW9yZS5jb20iLCJVc2VySWQiOiI1ZmY2OTliMTFiMjY1ZmY4ZDM1ZmUzYTEiLCJpbnN0aXR1dGUiOiJ2a2dyb3VwIiwicm9sZSI6IkFkbWluIiwiYWNjb3VudF9TdGF0dXMiOiJBY3RpdmUiLCJpYXQiOjE2MTAxMDExNzR9.2UBdcSvBVdtxZacMtn-SRwW29WA-px26fxVoRyw4Jxc',
            env: JSON.parse(localStorage.getItem('env')),
            containerNav: {
                isBack: false,
                name: "List of Scholarships",
                isName: true,
                isSearch: true,
                isSort: false,
                isPrint: true,
                isDownload: true,
                isShare: false,
                isNew: true,
                newName: "New",
                isSubmit: false,
            },
            tablePrintDetails: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 1,
            noProg: "Fetching Data ...",
            PrevScholarShipId: "",
            previewList: false,
            allBankDatas: [],
            previewListForm: ScholarshipData.formJson,
            viewType: '',
            scholarProvider: '',
            scholarType: '',
            selBankId: '',
            scholarbankId: '',
            selectedID: '',
            snackbar: { openNotification: false, NotificationMessage: "", status: "success" },
            downloadTotalRecord: 0
        }
    }

    static contextType = DateFormatContext;
    componentDidMount() {
        this.getTableData()
        this.getAllBanks()
    }
    getTableData = () => {
        this.setState({ previewList: false, LoaderStatus: true, tablePrintDetails: [] })
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/scholarships?orgId=${this.state.orgId}&campusId=${this.state.campusId}&page=${this.state.page}&limit=${this.state.limit}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                console.log(res.data)
                if (res.data.status == "success") {
                    if (res.data.data.length == 0) {
                        this.setState({ noProg: "No Data", LoaderStatus: false, tablePrintDetails: [] })
                    }
                    else {
                        let datas = [];
                        res.data.data.sort((a, b) => Number(new Date(b.createdAt)) - Number(new Date(a.createdAt))).map((data) => {
                            datas.push({
                                "ID": data.displayName,
                                "Type": data.type,
                                "Provider": data.provider,
                                "Created On": data.createdAt !== "" ? this.context.dateFormat !== "" ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                // "Created by": data.createdBy,
                                "Status": data.status == 1 ? "Active" : "Inactive",
                                "Item": JSON.stringify(data)
                            })
                        })
                        this.setState({
                            tablePrintDetails: datas,
                            previewList: false,
                            totalPages: res.data.totalPages,
                            page: res.data.page,
                            LoaderStatus: false,
                            totalRecord: res.data.totalRecord,
                            downloadTotalRecord: res.data.totalRecord,
                        })
                    }
                }
                else {
                    this.setState({
                        noProg: "No Data", tablePrintDetails: [],
                        previewList: false, LoaderStatus: false
                    })
                }
            })
            .catch(err => {
                this.setState({ noProg: "No Data", tablePrintDetails: [], previewList: false, LoaderStatus: false })
            })
    }

    onAddNewScholarship = () => {
        this.resetForm('addNew')
        this.setState({ previewList: true, viewType: "addNew", LoaderStatus: true }, () => {
            axios({
                method: 'get',
                url: `${this.state.env["zqBaseUri"]}/edu/master/getDisplayId/scholarships?orgId=${this.state.orgId}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            }).then(res => {
                let newData = this.state.previewListForm;
                newData.map(task => {
                    if (task['name'] == 'scholarshipID') {
                        task['defaultValue'] = res.data.data
                        task['readOnly'] = true;
                        task['validation'] = false;
                        task['required'] = false;
                        task['requiredBoolean'] = true;
                    }
                })
                this.setState({ previewListForm: newData, LoaderStatus: false, viewType: 'addNew', PrevScholarShipId: res.data.data })
            }).catch(err => { })
        })

    }
    onPreviewStudentList = (item) => {
        this.getAllBanks();
        console.log(item);
        let e = JSON.parse(item.Item)
        let previewListData = this.state.previewListForm
        previewListData.map((task) => {
            if (task['name'] == 'scholarshipID') {
                task['defaultValue'] = e.displayName
                task['readOnly'] = true
                task['required'] = false
            }
            if (task['name'] == 'scholarshiptype') {
                task['defaultValue'] = e.type
                task['readOnly'] = false
                task['required'] = false
            }
            if (task['name'] == 'scholarshipProvider') {
                task['defaultValue'] = e.provider
                task['readOnly'] = false
                task['required'] = false
            }
            if (task['name'] == "bankname") {
                task['defaultValue'] = e.bankDetails['_id']
                task['readOnly'] = false
                task['required'] = false
                this.setState({ selBankId: e.bankDetails['_id'] });
            }
            if (task['name'] == "bankaccountnumber") {
                task['defaultValue'] = e.bankDetails['bankAccountNumber']
                task['readOnly'] = true
                task['required'] = false
            }
            if (task['name'] == "bankaccountname") {
                task['defaultValue'] = e.bankDetails['bankAccountName']
                task['readOnly'] = true
                task['required'] = false
            }
        })


        console.log(previewListData);
        this.setState({ previewList: true, selectedID: e._id, PrevScholarShipId: e.displayName, viewType: 'preview', previewListForm: previewListData })
    }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            this.getTableData()
        });
    }
    onPreviewLoanList = () => { }
    moveBackTable = () => {
        this.setState({ previewList: false })
    }
    onPreviewStudentList1 = () => { }
    actionClickFun = (e) => {
        console.log(e);
    }
    allSelectData = () => { }
    handleTabChange = () => { }
    onInputChanges = (value, item, event, dataS) => {
        console.log(value, item, event, dataS)
        if (dataS && dataS.name == "bankname") {
            dataS['defaultValue'] = value
            this.setState({ selBankId: value, selectedBank: item['bankDetails'] }, () => {
                let form = this.state.previewListForm
                form.map(formItem => {
                    if (formItem.name == "bankaccountname") {
                        formItem['readOnly'] = true
                        formItem['requiredBoolean'] = false
                        formItem['error'] = false
                        formItem['defaultValue'] = item['bankDetails']['bankAccountName']
                    }
                    else if (formItem.name == "bankaccountnumber") {
                        formItem['readOnly'] = true
                        formItem['requiredBoolean'] = false
                        formItem['error'] = false
                        formItem['defaultValue'] = item['bankDetails']['bankAccountNumber']
                    }
                })
                this.setState({ previewListForm: form })
            });
        }
        else if (dataS == undefined) {
            if (item.name == "scholarshipProvider") {
                item['defaultValue'] = value
                item['error'] = false
                this.setState({ scholarProvider: value });
            }
            else if (item.name == "scholarshiptype") {
                item['defaultValue'] = value
                item['error'] = false
                this.setState({ scholarType: value });
            }
        }
    }

    getAllBanks = () => {
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/banks?orgId=${this.state.orgId}`,
            headers: {
                'Authorization': this.state.authToken
            }
        }).then(res => {
            let bankData = res.data.data
            let newData = this.state.previewListForm;
            newData.map(task => {
                if (task['name'] == 'bankname') {
                    let options = []
                    bankData.map(bankITem => {
                        options.push({
                            'label': bankITem.bankName + ' - ' + bankITem.bankAccountNumber,
                            'value': bankITem._id,
                            'bankDetails': bankITem
                        })
                    })
                    task['options'] = options
                }
            })
            this.setState({ previewListForm: newData, allBankDatas: bankData })
        }).catch(err => { })
    }
    onSubmit = (e, formElts) => {
        if (this.state.viewType == "preview") {
            let newData = this.state.previewListForm;
            let apiCallData = true
            newData.map((item) => {
                if (item.requiredBoolean) {
                    if (item['defaultValue'] === "" || item['defaultValue'] === undefined) {
                        item["error"] = true;
                        item['errorMsg'] = `Invalid ${item.label}`
                        apiCallData = false
                    } else {
                        item["error"] = false;
                        item["validation"] = false;
                    }
                }

            })
            this.setState({ previewListForm: newData })
            if (apiCallData == true) {
                this.setState({ LoaderStatus: true })
                let bodyResData = {};
                newData.map((task) => {
                    if (task['name'] == 'scholarshipID') {
                        bodyResData['displayName'] = task['defaultValue']
                    }
                    if (task['name'] == 'scholarshiptype') {
                        bodyResData['type'] = task['defaultValue']
                    }
                    if (task['name'] == 'scholarshipProvider') {
                        bodyResData['provider'] = task['defaultValue']
                    }
                })
                let headers = {
                    'Authorization': this.state.authToken
                };
                let bodyData = {
                    "displayName": bodyResData['displayName'],
                    "type": bodyResData.type,
                    "provider": bodyResData.provider,
                    "bankId": this.state.selBankId,
                    "createdBy": this.state.orgId,
                    // "createdBy": this.state.orgName,
                    "orgId": this.state.orgId,
                }
                axios.put(`${this.state.env['zqBaseUri']}/edu/scholarships/${this.state.selectedID}?orgId=${this.state.orgId}`,
                    bodyData,
                    { headers }
                )
                    // axios.put(`${this.state.env["zqBaseUri"]}/edu/master/feeTypes?id=${this.state.PreviewData._id}`, bodyData, { headers })
                    .then(res => {
                        if (res.data.type !== "error") {
                            let a = this.state.snackbar;
                            a.openNotification = true;
                            a.NotificationMessage = "Scholarship Item has been updated successfully";
                            a.status = "success";
                            this.setState({ LoaderStatus: false, tabViewForm: false, snackbar: a }, () => {
                                this.getTableData()
                            })
                            setTimeout(() => {
                                a.openNotification = false;
                                this.setState({ snackbar: a })
                            }, 2000)
                            this.resetForm("preview")
                        }
                        else {
                            let a = this.state.snackbar;
                            a.openNotification = true;
                            a.NotificationMessage = "Failed to update the scholarship item";
                            a.status = "error";
                            this.setState({ LoaderStatus: false, tabViewForm: false, snackbar: a }, () => {
                                this.getTableData()
                            })
                            setTimeout(() => {
                                a.openNotification = false;
                                this.setState({ snackbar: a })
                            }, 2000)
                            this.resetForm("preview")
                        }
                    })
                    .catch(err => {
                        let a = this.state.snackbar;
                        a.openNotification = true;
                        a.NotificationMessage = "Failed to update the scholarship item";
                        a.status = "error";
                        this.setState({ LoaderStatus: false, tabViewForm: false, snackbar: a }, () => {
                            this.getTableData()
                        })
                        setTimeout(() => {
                            a.openNotification = false;
                            this.setState({ snackbar: a })
                        }, 2000)
                        this.resetForm("preview")
                    })
            }
            else { }
        } else if (this.state.viewType === "addNew") {
            let newData = this.state.previewListForm;
            let apiNewCall = true
            newData.map((item) => {
                if (item.requiredBoolean) {
                    if (item['defaultValue'] === "" || item['defaultValue'] === undefined) {
                        item["error"] = true;
                        item['errorMsg'] = `Invalid ${item.label}`
                        apiNewCall = false
                    } else {
                        item["error"] = false;
                        item["validation"] = false;
                    }
                }
            })
            this.setState({ previewListForm: newData })
            if (apiNewCall == true) {
                this.setState({ LoaderStatus: true })
                let bodyResData = {};
                newData.map((task) => {
                    if (task['name'] == 'scholarshipID') {
                        bodyResData['displayName'] = task['defaultValue']
                    }
                    if (task['name'] == 'scholarshiptype') {
                        bodyResData['type'] = task['defaultValue']
                    }
                    if (task['name'] == 'scholarshipProvider') {
                        bodyResData['provider'] = task['defaultValue']
                    }
                })
                let headers = {
                    'Authorization': this.state.authToken
                };
                let bodyData = {
                    "displayName": bodyResData['displayName'],
                    "type": bodyResData.type,
                    "provider": bodyResData.provider,
                    "bankId": this.state.selBankId,
                    "createdBy": this.state.orgId,
                    // "createdBy": this.state.orgName,
                    "orgId": this.state.orgId,
                }
                axios.post(`${this.state.env['zqBaseUri']}/edu/scholarships?orgId=${this.state.orgId}`,
                    bodyData,
                    {
                        headers
                    })
                    .then(res => {
                        if (res.data.type !== 'error') {
                            let a = this.state.snackbar;
                            a.openNotification = true;
                            a.NotificationMessage = "Scholarship Item has been added successfully";
                            a.status = "success";
                            this.setState({ LoaderStatus: false, snackbar: a }, () => {
                                this.getTableData();
                            })
                            setTimeout(() => {
                                a.openNotification = false;
                                this.setState({ snackbar: a })
                            }, 2000)
                            this.resetForm('addNew');
                        }
                        else {
                            let a = this.state.snackbar;
                            a.openNotification = true;
                            a.NotificationMessage = "Failed to add the Scholarship Item..";
                            a.status = "error";
                            this.setState({ LoaderStatus: false, snackbar: a }, () => {
                                this.getTableData();
                            })
                            setTimeout(() => {
                                a.openNotification = false;
                                this.setState({ snackbar: a })
                            }, 2000)
                            this.resetForm('addNew');
                        }
                    }).catch(err => {
                        let a = this.state.snackbar;
                        a.openNotification = true;
                        a.NotificationMessage = "Failed to add the Scholarship Item..";
                        a.status = "error";
                        this.setState({ LoaderStatus: false, snackbar: a }, () => {
                            this.getTableData();
                        })
                        setTimeout(() => {
                            a.openNotification = false;
                            this.setState({ snackbar: a })
                        }, 2000)
                        this.resetForm('addNew');
                    })
            }
        }
    }
    resetForm = (e) => {
        if (e == "addNew") {
            let newData = this.state.previewListForm;
            newData.map((task) => {
                task["error"] = false;
                task['validation'] = false;
                task['errorMsg'] = "";
                task['defaultValue'] = "";
            })
            this.setState({ previewListForm: newData })
        }
        else if (e == "preview") {
            let newData1 = this.state.previewListForm;
            newData1.map((task) => {
                task["error"] = false;
                task['validation'] = false;
                task['errorMsg'] = "";
                task['defaultValue'] = "";
            })
            this.setState({ previewListForm: newData1 })
        }

    }
    formBtnHandle = (item) => {
        if (item !== undefined) {
            if (item["type"] === "cancel") {
                this.cancelViewData();
            }
        }
    }
    cancelViewData = () => {
        this.setState({ previewList: false });
        this.resetForm('preview');
        this.resetForm('addNew');
    }

    onClean = (e) => {

    }
    searchHandle = (value) => { }
    printScreen = () => {
        let page = 1
        let limit = this.state.downloadTotalRecord || 1
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/scholarships?orgId=${this.state.orgId}&page=${page}&limit=${limit}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                this.setState({ tablePrintDetails: [] })
                if (res.data.status == "success") {
                    if (res.data.data.length == 0) {

                    }
                    else {
                        let datas = [];
                        res.data.data.sort((a, b) => Number(new Date(b.createdAt)) - Number(new Date(a.createdAt))).map((data) => {
                            datas.push({
                                "ID": data.displayName,
                                "Type": data.type,
                                "Provider": data.provider,
                                "Created On": data.createdAt !== "" ? this.context.dateFormat !== "" ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                // "Created by": data.createdBy,
                                "Status": data.status == 1 ? "Active" : "Inactive",
                                "Item": JSON.stringify(data)
                            })
                        })
                        this.setState({ tablePrintDetails: datas }, () => {
                            setTimeout(() => {
                                window.print()
                                this.getTableData()
                            }, 1);
                        })
                    }
                }
            })
            .catch(err => {
                console.log("error", err)
            })
    }
    onDownload = () => {
        let createXlxsData = [];
        let page = 1
        let limit = this.state.downloadTotalRecord
        this.setState({ LoaderStatus: true }, () => {
            axios({
                method: 'get',
                url: `${this.state.env["zqBaseUri"]}/edu/scholarships?orgId=${this.state.orgId}&page=${page}&limit=${limit}`,
                headers: {
                    'Authorization': this.state.authToken
                }
            })
                .then(res => {
                    console.log(res.data)
                    if (res.data.status == "success") {
                        if (res.data.data.length !== 0) {
                            res.data.data.sort((a, b) => Number(new Date(b.createdAt)) - Number(new Date(a.createdAt))).map((data) => {
                                createXlxsData.push({
                                    "ID": data.displayName,
                                    "Type": data.type,
                                    "Provider": data.provider,
                                    "Created On": data.createdAt !== "" ? this.context.dateFormat !== "" ? moment(data.createdAt).format(this.context.dateFormat) : moment(data.createdAt).format('DD/MM/YYYY') : "-",
                                    "Status": data.status == 1 ? "Active" : "Inactive",
                                })
                            })
                            var ws = xlsx.utils.json_to_sheet(createXlxsData);
                            var wb = xlsx.utils.book_new();
                            xlsx.utils.book_append_sheet(wb, ws, "Scholarships");
                            xlsx.writeFile(wb, "Scholarships.xlsx");
                            this.setState({ LoaderStatus: false })
                        }
                    }
                }).catch(err => {
                    console.log(err, 'errorrrrr')
                    this.setState({ LoaderStatus: false })
                })
        })
    }
    render() {
        return (
            <div className="list-of-feeType-mainDiv">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.previewList == false ?
                    <React.Fragment>
                        <React.Fragment>
                            <div className="trial-balance-header-title">
                                <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                                <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Scholarships</p>
                            </div>
                        </React.Fragment>
                        <div className="masters-body-div">
                            <React.Fragment>
                                <ContainerNavbar containerNav={this.state.containerNav} onAddNew={() => { this.onAddNewScholarship(); }} searchValue={(searchValue) => this.searchHandle(searchValue)} onDownload={this.onDownload} printScreen={this.printScreen} />
                            </React.Fragment>
                            <div className="table-wrapper" style={{ marginTop: "10px" }}>
                                {this.state.tablePrintDetails.length == 0 ? <p className="noprog-txt">{this.state.noProg}</p> :
                                    <React.Fragment>
                                        <div className="remove-last-child-table" >
                                            <ZqTable
                                                allSelect={this.allSelectData}
                                                data={this.state.tablePrintDetails}
                                                rowClick={(item) => { this.onPreviewStudentList(item) }}
                                                onRowCheckBox={(item) => { this.onPreviewStudentList1(item) }}
                                                handleActionClick={(item) => { this.actionClickFun(item) }}
                                            />
                                            {/* <p className="noprog-txt">No Data</p> */}
                                        </div>
                                    </React.Fragment>
                                }
                                {this.state.tablePrintDetails.length !== 0 ?
                                    <PaginationUI
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        total={this.state.totalRecord}
                                        currentPage={this.state.page}
                                    /> : null}
                            </div>
                        </div>
                    </React.Fragment> :
                    <React.Fragment>
                        <div className="tab-form-wrapper tab-table">
                            <div className="preview-btns">
                                <div className="preview-header-wrap">
                                    < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                    <h6 className="preview-Id-content" onClick={this.moveBackTable}>| Scholarships | {this.state.PrevScholarShipId}</h6>
                                </div>
                            </div>
                            <div className="goods-header">
                                <p className="info-input-label">BASIC DETAILS</p>
                            </div>
                            <div className="goods-wrapper goods-parent-wrap">
                                {this.state.viewType == "preview" ?
                                    <ZenForm
                                        formData={this.state.previewListForm}
                                        className="goods-wrap"
                                        onInputChanges={this.onInputChanges}
                                        onFormBtnEvent={(item) => {
                                            this.formBtnHandle(item);
                                        }}
                                        onClean={this.onClean}
                                        clear={true}
                                        onSubmit={(e) => this.onSubmit("preview", e.target.elements)}
                                    /> :
                                    <ZenForm
                                        formData={this.state.previewListForm}
                                        className="goods-wrap"
                                        onInputChanges={this.onInputChanges}
                                        onFormBtnEvent={(item) => {
                                            this.formBtnHandle(item);
                                        }}
                                        onClean={this.onClean}
                                        clear={true}
                                        onSubmit={(e) => this.onSubmit("addNew", e.target.elements)}
                                    />
                                }
                            </div>

                        </div>
                    </React.Fragment>
                }
                <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={2000} onClose={this.closeNotification}>
                    <Alert onClose={this.closeNotification}
                        severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
                        {this.state.snackbar.NotificationMessage}
                    </Alert>
                </Snackbar>
            </div>
        )
    }
}
export default Loans;