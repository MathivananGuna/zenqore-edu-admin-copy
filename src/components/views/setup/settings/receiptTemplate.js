import React, { Component } from "react";
import './template.scss'

class ReceiptTemplate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    render() {
        return (
            <React.Fragment>
                <div class="container">
                    <div className="logo" style={{ float: "left" }}>
                        <img id="img" alt="" width="148" height="148" />
                    </div>
                    <div className="address" style={{ float: "left", paddingTop: "15px", width: "320px", paddingLeft: "25px", fontSize: "15px" }}>
                        <h3 style={{ padding: "0px", margin: "0px" }} id="insname"></h3>
                        <p style={{ lineHeight: 1.5, margin: 0, padding: 0 }} id="city"></p>
                        <p style={{ lineHeight: 1.5, margin: 0, padding: 0 }} id="District"></p>
                        <p style={{ lineHeight: 1.5, margin: 0, padding: 0 }} id="state"></p>
                        <p style={{ lineHeight: 1.5, margin: 0, padding: 0 }}>Contact: <span id="ph"></span> </p>
                    </div>
                </div>
                <div className="clearfix" style={{ clear: "both", display: "table", overflow: "auto" }}></div>
            </React.Fragment>
        )
    }
}