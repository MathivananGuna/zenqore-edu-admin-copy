import React from 'react';
import TextField from '@material-ui/core/TextField';

class EmailServer extends React.Component {
    componentDidMount() {
        console.log("r=email-server", this.props)
    }
    onInputChanges = (e) => {
        this.props.onInputChanges(e);
    }
    render() {
        return (
            <React.Fragment>
                <div className="settings-email-first-right-wrapper">
                    <p className="settings-email-right-header">{`Email Details ${this.props.header}`}</p>
                    <div className="settings-email-right-container" style={{ marginLeft: "15px" }}>
                        <div className="settings-email-right-textfield">
                            <p className="settings-email-form-label">Enter Email Address</p>
                            <TextField
                                className="settings-email-form-textfield"
                                name="email"
                                // rows={this.state.inputData.row != undefined ? this.state.inputData.row : null}
                                // multiline={this.state.inputData.row != undefined ? true : false}
                                // id={this.state.inputData.name}
                                placeholder="Enter Email Address"
                                // label={this.state.inputData.label}
                                value={this.props.emailServerEmail}
                                defaultValue={this.props.emailServerEmail}
                                variant="filled"
                                // required={this.state.inputData.required}
                                // InputProps={{
                                //   readOnly: this.state.inputData.readOnly,
                                // }}
                                type="emailph"
                                helperText={this.props.emailServerHelperText}
                                error={this.props.emailServerError}
                                onChange={(e) => this.onInputChanges(e)}
                                // autoFocus={this.state.inputData.focus === undefined ? false : this.state.inputData.focus}
                                autoComplete="off"
                            >
                            </TextField>
                        </div>
                        <div className="settings-email-right-button">
                            <input type="file" id="email-config" style={{ display: 'none' }} name="Email Config File" alt="config file" onChange={this.uploadEmailConfigFile} />
                            <div style={{ width: "100%", paddingTop: "30px", paddingLeft: "444px" }}>
                                <label htmlFor="email-config" className="email-config-btn">Select Files</label>
                            </div>
                        </div>
                    </div>

                    <div className="input-file-config">
                        <p class="form-hd settings-form-hd" style={{ paddingLeft: "0px" }}>Config JSON File</p>
                        <div className="inline-upload-box" style={{ display: "inline-flex", width: '100%' }}>
                            <div style={{ width: "20%" }}>
                                <p class="settings-form-hd" style={{ paddingLeft: "0px" }}>File Name</p>
                                <span style={{ display: "grid", width: "100%" }}>{this.props.newConfigFileName}</span>
                            </div>
                            <div style={{ width: "80%" }}>
                                <p class="settings-form-hd" style={{ textAlign: "center" }}>Content</p>
                                <div style={{ width: '100%', overflow: 'hidden', height: '200px' }} >
                                    <textarea className="text-area-json-file" id="emailconfig"
                                        value={this.props.newEmailConfigJson === "No data" ? this.props.newEmailConfigJson : JSON.stringify(this.props.newEmailConfigJson)} disabled
                                        style={{ width: 'calc(100% - 60px)', minHeight: '140px', position: 'relative', left: '20px' }}
                                    ></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default EmailServer;