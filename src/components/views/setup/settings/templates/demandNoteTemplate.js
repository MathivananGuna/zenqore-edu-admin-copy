import React, { Component } from "react";
import './template.scss';
import Logo from '../../../../../assets/images/logo1.png';
import Axios from 'axios';
import { logDOM } from "@testing-library/react";


class DemandNote extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            email: "",
            institutionName: "",
            address1: "",
            address2: "",
            address3: "",
            institutionCity: "",
            institutionPincode: "",
            institutionState: "",
            institutionContact: "",
            instituteLogo: ""
        }
    }
    componentDidMount() {

        let requestOptions = {
            method: 'GET',
            redirect: 'follow',
            headers: {
                "Authorization": this.state.authToken
            }
        };
        let queryUrl = (localStorage.getItem('campusId') == undefined || localStorage.getItem('campusId') == "") ? `instituteid=${this.state.orgId}` : `instituteid=${this.state.orgId}&campusId=${localStorage.getItem('campusId')}`
        Axios.get(`${this.state.env['zqBaseUri']}/setup/settings?${queryUrl}`, requestOptions)
            .then(res => {
                console.log("res", res)
                if (res.data.campus !== "" && res.data.campus !== null) {
                    let resData = res.data.campus;
                    let institutionName = resData.legalName;
                    let address1 = resData.legalAddress.address1;
                    let address2 = resData.legalAddress.address2;
                    let address3 = resData.legalAddress.address3;
                    let institutionCity = resData.legalAddress.city;
                    let institutionPincode = resData.legalAddress.pincode;
                    let institutionState = resData.legalAddress.state;
                    let institutionContact = resData.instituteContact[0].mobileNumber;
                    let instituteLogo = resData.logo;
                    this.setState({ institutionName, address1, address2, address3, institutionCity, institutionPincode, institutionState, institutionContact, instituteLogo })
                }
                else {
                    let respData = res.data.settings[0];
                    let institutionName = respData.instituteDetails.instituteName;
                    let address1 = respData.instituteDetails.address1;
                    let address2 = respData.instituteDetails.address2;
                    let address3 = respData.instituteDetails.address3;
                    let institutionCity = respData.instituteDetails.cityTown;
                    let institutionPincode = respData.instituteDetails.pinCode;
                    let institutionState = respData.instituteDetails.stateName;
                    let institutionContact = respData.instituteDetails.phoneNumber1;
                    let instituteLogo = respData.logo.logo;
                    this.setState({ institutionName, address1, address2, address3, institutionCity, institutionPincode, institutionState, institutionContact, instituteLogo })
                }
            }).catch(err => {
                console.log("err", err)
            })

        // fetch(`${this.state.env["zqBaseUri"]}/setup/settings?instituteid=${this.state.orgId}&campusId=605b089e828d962d045d1ce6`, requestOptions)
        //     .then(response => response.text())
        //     .then(result => {

        //         console.log("result", result)
        //         var results = JSON.parse(result)
        //         console.log("iii", results['campus']['legalAddress']['address1'])

        //         let address1 = results['campus']['legalAddress']['address1'] || ""
        //         let address2 = results['campus']['legalAddress']['address2'] || ""
        //         let address3 = results['campus']['legalAddress']['address3'] || ""
        //         let cityname = results['campus']['legalAddress']['city'] || ""
        //         let pincode = results['campus']['legalAddress']['pincode'] || ""
        //         let state = results['campus']['legalAddress']['state'] || ""
        //         // document.getElementById("insname").innerHTML = results[0]['instituteDetails']['instituteName'] || ""
        //         // document.getElementById("inssname").innerHTML = results[0]['instituteDetails']['instituteName'].toLowerCase()
        //         //     .replace(/\b[a-z]/g, function (letter) { return letter.toUpperCase() }) + " " + 'Accounts Team' || ""
        //         document.getElementById("city").innerHTML = address1 + " " + address2 + " " + address3
        //         document.getElementById("District").innerHTML = `${results['campus']['legalAddress']['city']}, PIN: ${results['campus']['legalAddress']['pincode']} ,${results['campus']['legalAddress']['state']}` || ""
        //         document.getElementById("state").innerHTML = `${results['campus']['legalAddress']['state']}, India` || ""

        //         // if (results[0]['instituteDetails']['phoneNumber1'] !== "" && results[0]['instituteDetails']['phoneNumber1'] !== null && results[0]['instituteDetails']['phoneNumber2'] !== "" && results[0]['instituteDetails']['phoneNumber2'] !== null) {
        //         //     document.getElementById("ph").innerHTML = `${results[0]['instituteDetails']['phoneNumber1']}, ${results[0]['instituteDetails']['phoneNumber2']}`
        //         // } else if (results[0]['instituteDetails']['phoneNumber1'] !== "" && results[0]['instituteDetails']['phoneNumber1'] !== null && (results[0]['instituteDetails']['phoneNumber2'] === "" || results[0]['instituteDetails']['phoneNumber2'] === null)) {
        //         //     document.getElementById("ph").innerHTML = `${results[0]['instituteDetails']['phoneNumber1']}`
        //         // } else if ((results[0]['instituteDetails']['phoneNumber1'] === "" || results[0]['instituteDetails']['phoneNumber1'] === null) && results[0]['instituteDetails']['phoneNumber2'] !== "" && results[0]['instituteDetails']['phoneNumber2'] !== null) {
        //         //     document.getElementById("ph").innerHTML = `${results[0]['instituteDetails']['phoneNumber2']}`
        //         // }

        //         // document.getElementById("img").src = typeof results[0]['logo'] === 'string' ? results[0]['logo'].split("?")[0] : results[0]['logo']
        //         document.getElementById("img").src = typeof results['campus']['logo'] === 'string' ? results['campus']['logo'].split("?")[0] : results['campus']['logo']
        //         console.log(results[0]['logo']['logo'][0])
        //         // this.setState({ email: results[0]['instituteDetails']['email'] }, () => {
        //         // console.log('email', this.state.email)
        //         // })
        //     })
        //     .catch(error => console.log('error', error));
    }






    render() {
        let { institutionName, address1, address2, address3, institutionCity, institutionPincode, institutionState, institutionContact, instituteLogo } = this.state;
        return (
            <React.Fragment>
                <div class="body demandNoteTemplate">
                    <div class="mainList" style={{ textAlign: "center" }}>
                        <img title="logo" id="img" alt="college logo" style={{ width: "148px", height: "148px", }} src={instituteLogo} />
                        <div class="mainList-text" style={{ marginleft: "20px" }}>
                            <p class="mainList-para" style={{ margin: "5px" }}>
                                <h3 id="insname" style={{
                                    fontFamily: "sans-serif", fontSize: "18px",
                                    margin: "-5px"
                                }}>{institutionName}</h3>

                                <span id="city" style={{ fontFamily: "sans-serif", fontSize: "13px" }}>{address1 ? `${address1},` : ""} {address2 ? `${address2},` : ""} {address3 ? `${address3}` : ""}
                                </span>
                                <br />
                                <span id="District" style={{ fontFamily: "sans-serif", fontSize: "13px" }}>{institutionCity} {institutionPincode} | {institutionState}, Contact: {institutionContact}</span>
                            </p>

                        </div>

                    </div>


                    <p style={{ marginTop: "30px" }}><strong>Dear Parent,</strong></p>

                    <p style={{ marginTop: "10px" }}> Greetings from NCFE! We are happy to announce the commencement of the academic year 2021- 22.</p>
                    <p style={{ marginTop: "10px" }}>Your ward XXXXXX is in Grade-1 and the annual fee for the academic year  2021- 22 is <b>XXXXXX </b><br /></p>
                    <p style={{ marginTop: "14px" }}>These fees have to be paid in 2 installments as follows:</p>
                    <table style={{ borderCollapse: "collapse", width: "60%", height: "84px", border: "1px solid #000", marginTop: "15px" }}>
                        <thead>

                            <th class="tableRowData">TERM</th>
                            <th class="tableRowData">DUE DATE</th>

                            <th class="tableRowData">AMOUNT</th>
                            <th class="tableRowData">STATUS</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="tableBodyData">XXXXXX</td>
                                <td class="tableBodyData">XXXXXX</td>

                                <td class="tableBodyData">XXXXXX</td>
                                <td class="tableBodyData">XXXXXX</td>

                            </tr>
                            <tr>
                                <td class="tableBodyData">XXXXXX</td>
                                <td class="tableBodyData">XXXXXX</td>

                                <td class="tableBodyData">XXXXXX</td>
                                <td class="tableBodyData">XXXXXX</td>
                            </tr>
                            <tr>
                                <td class="tableBodyData"><b>TOTAL</b></td>
                                <td class="tableBodyData">-</td>

                                <td class="tableBodyData"><b>XXXXXX</b></td>
                                <td class="tableBodyData"><b>XXXXXX</b></td>
                            </tr>
                        </tbody>
                    </table>

                    <p style={{ marginTop: "25px" }}>The details for the Term1 fees are as follows:</p>
                    <table style={{ borderCollapse: "collapse", width: "80%", height: "84px", border: "1px solid #000", marginTop: "15px" }}>
                        <tbody>
                            <tr>
                                <td class="tableRowData">Student Admission Number</td>
                                <td class="tableRowData">Student Name</td>
                                <td class="tableRowData">Grade</td>
                                <td class="tableRowData">Demand Note ID</td>
                                <td class="tableRowData">Annual Fees</td>
                                <td class="tableRowData">Term1 Fees</td>
                                <td class="tableRowData">Term2 Fees</td>

                            </tr>
                            <tr>
                                <td class="tableBodyData">XXXXXX</td>
                                <td class="tableBodyData">XXXXXX</td>
                                <td class="tableBodyData">XXXXXX</td>
                                <td class="tableBodyData">XXXXXX</td>
                                <td class="tableBodyData1">XXXXXX</td>
                                <td class="tableBodyData1">XXXXXX</td>
                                <td class="tableBodyData1">XXXXXX</td>

                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                                <td style={{ padding: "0px", paddingRight: "5px" }} class="tableFooterData">PAY NOW</td>
                                <td class="tableFooterData">XXXXXX</td>
                            </tr>
                            <tr style={{ border: "1px solid #000" }}>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                                <td style={{ padding: "0px", paddingRight: "5px" }} class="tableFooterData">BALANCE</td>
                                <td class="tableFooterData">XXXXXX</td>
                            </tr>

                        </tbody>
                    </table>


                    <p style={{ fontWeight: "bold", marginTop: "10px" }}>Payment due date for this Demand note is <span style={{ textDecoration: "underline" }}>15/APR/2021
            </span>.</p>
                    <p>Please click on the following button to initiate the payment before the due date.</p>

                    <p> <button class="button button1 payNowButton">Pay Now</button></p>
                    <p> Please note that a penalty of ₹25 per day will be charged if the fees is not paid by <u> DD/MM/YYYY</u>.</p>
                    <p style={{ marginTop: "20px" }}>Regards,</p>
                    <p><b><span id="inssname"></span>Finance Team</b></p>
                    <p>NATIONAL CENTRE FOR EXCELLENCE </p>
                </div>
            </React.Fragment>




        )
    }
}
export default DemandNote;



{/* 
            // <React.Fragment>
                    //     <div className="body demandNoteTemplate">
                        //         <div className="mainList" style={{ paddingLeft: '25px' }}>
                            //             <img title="logo" id="img" alt="" style={{ width: "148px", height: "148px" }} src="" />
            //             <p className="mainList-para"><h3 id="insname"></h3>
            //                 <span id="city"></span>
            //                 <br /><span id="District"></span>
            //                 <br /><span id="state"></span>
            //                 <br /><span id="state"></span>
            //                 Email: <span id="email">{this.state.email}</span>
            //                 <br />Contact: <span id="ph"></span></p>
            //         </div>
            //         <hr />

            //         <p><strong>Dear Parent,</strong></p>
            //         <p>Please find the fee details of your ward(s) as follows:</p>
            //         <table style={{ borderCollapse: "collapse", width: '100%', height: "84px", border: '1px solid #000' }}>
                            //             <tbody>
                                //                 <tr>
                                    //                     <td className="tableRowData">Student Reg. ID</td>
            //                     <td className="tableRowData">Student Name</td>
            //                     <td className="tableRowData">class/Batch</td>
            //                     <td className="tableRowData">Demand Note ID</td>
            //                     <td className="tableRowData">Tution Fees</td>
            //                     <td className="tableRowData">TOTAL</td>
            //                 </tr>
            //                 <tr>
                                    //                     <td className="tableBodyData">XXXXXXX</td>
            //                     <td className="tableBodyData">XXXXXXX</td>
            //                     <td className="tableBodyData">XXXXXXX</td>
            //                     <td className="tableBodyData">XXXXXXX</td>
            //                     <td className="tableBodyData1">XXXXXXX</td>
            //                     <td className="tableBodyData1">XXXXXXX</td>
            //                 </tr>
            //                 <tr>
                                    //                     <td></td>
            //                     <td></td>
            //                     <td></td>
            //                     <td></td>
            //                     <td style={{ padding: '0px', paddingRight: '5px' }} className="tableFooterData">GRAND TOTAL</td>
            //                     <td className="tableFooterData">XXXXXXX</td>
            //                 </tr>
            //             </tbody>
            //         </table>

            //         <p><strong>The Payment due date for this Demand note is <u>DD/MM/YYYY</u>.&nbsp;</strong></p>
            //         <p>Please click the button to initiate the payment:&nbsp; &nbsp;</p>
            //         <p> <button className="button button1 payNowButton">Pay
            // Now</button></p>
            //         <p>Regards,</p>
            //         <p><b><span id="inssname"></span></b></p>
            //         <p>&nbsp;</p>
            //     </div>
            // </React.Fragment> */}