import React from 'react';
import Axios from 'axios';

class RefundTemplate extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            insId: localStorage.getItem("instituteId"),
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            email: "",
            institutionName: "",
            address1: "",
            address2: "",
            address3: "",
            institutionCity: "",
            institutionPincode: "",
            institutionState: "",
            institutionContact: "",
            instituteLogo: "",
            instituteEmail: ""
        }
    }
    componentDidMount() {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow',
            headers: {
                "Authorization": this.state.authToken
            }
        };

        let queryUrl = (localStorage.getItem('campusId') == undefined || localStorage.getItem('campusId') == "") ? `instituteid=${this.state.orgId}` : `instituteid=${this.state.orgId}&campusId=${localStorage.getItem('campusId')}`
        Axios.get(`${this.state.env['zqBaseUri']}/setup/settings?${queryUrl}`, requestOptions)
            .then(res => {
                console.log("res", res)
                if (res.data.campus !== "" && res.data.campus !== null) {
                    let resData = res.data.campus;
                    let institutionName = resData.legalName;
                    let address1 = resData.legalAddress.address1;
                    let address2 = resData.legalAddress.address2;
                    let address3 = resData.legalAddress.address3;
                    let institutionCity = resData.legalAddress.city;
                    let institutionPincode = resData.legalAddress.pincode;
                    let institutionState = resData.legalAddress.state;
                    let institutionContact = resData.instituteContact[0].mobileNumber;
                    let instituteLogo = resData.logo;
                    let instituteEmail = resData.instituteContact[0].emailAddress;
                    this.setState({ institutionName, address1, address2, address3, institutionCity, institutionPincode, institutionState, institutionContact, instituteLogo, instituteEmail })
                }
                else {
                    let respData = res.data.settings[0];
                    let institutionName = respData.instituteDetails.instituteName;
                    let address1 = respData.instituteDetails.address1;
                    let address2 = respData.instituteDetails.address2;
                    let address3 = respData.instituteDetails.address3;
                    let institutionCity = respData.instituteDetails.cityTown;
                    let institutionPincode = respData.instituteDetails.pinCode;
                    let institutionState = respData.instituteDetails.stateName;
                    let institutionContact = respData.instituteDetails.phoneNumber1;
                    let instituteLogo = respData.logo.logo;
                    let instituteEmail = respData.instituteDetails.email;
                    this.setState({ institutionName, address1, address2, address3, institutionCity, institutionPincode, institutionState, institutionContact, instituteLogo, instituteEmail })

                }
            }).catch(err => {
                console.log("err", err)
            })

        // fetch(`${this.state.env["zqBaseUri"]}/setup/settings?instituteid=${this.state.insId}`, requestOptions)
        //     .then(response => response.text())
        //     .then(result => {
        //         var results = JSON.parse(result)
        //         // console.log(results[0]['instituteDetails']['phoneNumber1'] + results[0]['instituteDetails']['phoneNumber2'])

        //         let address1 = results[0]['instituteDetails']['address1'] || ""
        //         let address2 = results[0]['instituteDetails']['address2'] || ""
        //         let address3 = results[0]['instituteDetails']['address3'] || ""

        //         document.getElementById("insname").innerHTML = results[0]['instituteDetails']['instituteName'] || ""

        //         document.getElementById("insnames").innerHTML = results[0]['instituteDetails']['instituteName'].toLowerCase()
        //             .replace(/\b[a-z]/g, function (letter) { return letter.toUpperCase() }) + " " + 'Accounts Team' || ""

        //         document.getElementById("city").innerHTML = address1 + " " + address2 + " " + address3
        //         document.getElementById("District").innerHTML = `${results[0]['instituteDetails']['cityTown']}, PIN: ${results[0]['instituteDetails']['pinCode']}` || ""
        //         document.getElementById("state").innerHTML = `${results[0]['instituteDetails']['stateName']}, India` || ""
        //         // document.getElementById("email").innerHTML = results[0]['instituteDetails']['email']

        //         // if (results[0]['instituteDetails']['phoneNumber1'] !== "" && results[0]['instituteDetails']['phoneNumber2'] !== "") {
        //         //     document.getElementById("ph").innerHTML = `${results[0]['instituteDetails']['phoneNumber1']}, ${results[0]['instituteDetails']['phoneNumber2']}`
        //         // } else if (results[0]['instituteDetails']['phoneNumber1'] !== "" && results[0]['instituteDetails']['phoneNumber2'] === "") {
        //         //     document.getElementById("ph").innerHTML = `${results[0]['instituteDetails']['phoneNumber1']}`
        //         // } else if (results[0]['instituteDetails']['phoneNumber1'] === "" && results[0]['instituteDetails']['phoneNumber2'] !== "") {
        //         //     document.getElementById("ph").innerHTML = `${results[0]['instituteDetails']['phoneNumber2']}`
        //         // } else {
        //         //     document.getElementById("ph").innerHTML = `${results[0]['instituteDetails']['phoneNumber1']}, ${results[0]['instituteDetails']['phoneNumber2']}`
        //         // }

        //         if (results[0]['instituteDetails']['phoneNumber1'] !== "" && results[0]['instituteDetails']['phoneNumber1'] !== null && results[0]['instituteDetails']['phoneNumber2'] !== "" && results[0]['instituteDetails']['phoneNumber2'] !== null) {
        //             document.getElementById("ph").innerHTML = `${results[0]['instituteDetails']['phoneNumber1']}, ${results[0]['instituteDetails']['phoneNumber2']}`
        //         } else if (results[0]['instituteDetails']['phoneNumber1'] !== "" && results[0]['instituteDetails']['phoneNumber1'] !== null && (results[0]['instituteDetails']['phoneNumber2'] === "" || results[0]['instituteDetails']['phoneNumber2'] === null)) {
        //             document.getElementById("ph").innerHTML = `${results[0]['instituteDetails']['phoneNumber1']}`
        //         } else if ((results[0]['instituteDetails']['phoneNumber1'] === "" || results[0]['instituteDetails']['phoneNumber1'] === null) && results[0]['instituteDetails']['phoneNumber2'] !== "" && results[0]['instituteDetails']['phoneNumber2'] !== null) {
        //             document.getElementById("ph").innerHTML = `${results[0]['instituteDetails']['phoneNumber2']}`
        //         }

        //         /* document.getElementById("ph").innerHTML = `${results[0]['instituteDetails']['phoneNumber1']},${results[0]['instituteDetails']['phoneNumber2']}` */

        //         // document.getElementById("img").src = typeof results[0]['logo'] === 'string' ? results[0]['logo'].split("?")[0] : results[0]['logo']
        //         document.getElementById("img").src = typeof results[0]['logo']['logo'] === 'string' ? results[0]['logo']['logo'].split("?")[0] : results[0]['logo']
        //         // console.log(results[0]['logo']['logo'][0])
        //         this.setState({ email: results[0]['instituteDetails']['email'] }, () => {
        //             // console.log('email', this.state.email)
        //         })
        //     })
        //     .catch(error => console.log('error', error));
    }
    render() {
        let { institutionName, address1, address2, address3, institutionCity, institutionPincode, institutionState, institutionContact, instituteLogo, instituteEmail } = this.state;
        return (
            <React.Fragment>
                <div className="refund-container" style={{ margin: "10px" }}>
                    <div class="container" style={{ width: "100%", textAlign: "center" }}>
                        <div class="logo">
                            <img src={instituteLogo} style={{ width: "120px" }} />
                        </div>

                        <div class="address" style={{ width: "100%", paddingLeft: "25px", fontSize: "10px", textAlign: "center" }}>

                            <h3 style={{ fontFamily: "sans-serif", margin: "-5px", padding: "0", fontSize: "18px" }}>{institutionName}</h3>


                            <p style={{
                                lineHeight: "1.5", fontFamily: "sans-serif", margin: "0", padding: "0", fontSize: "13px"
                            }}>
                                {address1 ? `${address1},` : ""} {address2 ? `${address2},` : ""} {address3 ? `${address3}` : ""}
                            </p>

                            < p style={{ lineHeight: "1.5", fontFamily: "sans-serif", fontSize: "13px", margin: "0", padding: "0" }}>{institutionCity} {institutionPincode} | {institutionState}, Contact: {institutionContact}</p>

                        </div>
                    </div>​<div class="clearfix" style={{ clear: "both", display: "table", overflow: "auto" }}></div>
                    <div class="line" style={{ border: "1px solid", marginBottom: "5px" }}></div>

                    <div>
                        <p style={{ marginTop: "16px" }}><strong>Dear Parent,</strong></p>
                        <p>Your refund for the Payment <strong>XXXXXX</strong> has been initiated and your payment reference ID is <strong>XXXXXX</strong>.</p>
                        <p>This may take around 2 working days</p>
                        <p style={{ marginTop: "18px" }}>Regards,</p>
                        <strong><span style={{ marginLeft: "6px" }} id="insnames">XXXXXX</span></strong>
                        <p>XXXXXX</p>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default RefundTemplate;



{/* <div class="container">
                        <div class="logo" style={{ float: "left" }}>
                            <img id="img"
                                alt="" width="148px" height="148px" src={instituteLogo} />
                        </div>
                        <div class="address" style={{
                            float: "left",
                            width: "71%",
                        }}>
                            <h3 style={{ paddingLeft: "10px", margin: 0, fontSize: "18px", fontFamily: "sans-serif" }} id="insname">{institutionName}</h3>

                            <p style={{
                                lineHeight: 1.5, margin: 0,
                                paddingLeft: "10px", fontFamily: "sans-serif"
                            }} id="city">{address1}, {address2}, {address3}</p>
                            <p style={{
                                lineHeight: 1.5, margin: 0,
                                paddingLeft: "10px", fontFamily: "sans-serif"
                            }} id="state">{institutionCity} {institutionPincode} | {institutionState}</p>
                            <p style={{
                                lineHeight: 1.5, margin: 0,
                                paddingLeft: "10px", fontFamily: "sans-serif"
                            }}>Email: <span id="email">{instituteEmail}</span> </p>
                            <p style={{
                                lineHeight: 1.5, margin: 0,
                                paddingLeft: "10px", fontFamily: "sans-serif"
                            }}>Contact: <span id="ph">{institutionContact}</span> </p>

                        </div>
                    </div>

                    <div class="clearfix" style={{
                        clear: "both",
                        display: "table",
                        overflow: "auto"
                    }}></div> */}