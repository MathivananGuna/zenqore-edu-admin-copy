import React, { Component } from "react";
import receiptSvg from './receiptsvg.svg'
import './template.scss'
import Logo from '../../../../../assets/images/logo1.png';
import Axios from 'axios';


class Receipt extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            imgData: undefined,
            email: "",
            institutionName: "",
            address1: "",
            address2: "",
            address3: "",
            institutionCity: "",
            institutionPincode: "",
            institutionState: "",
            institutionContact: "",
            instituteLogo: ""
        }
    }
    componentDidMount() {
        let requestOptions = {
            method: 'GET',
            redirect: 'follow',
            headers: {
                "Authorization": this.state.authToken
            }
        }
        let queryUrl = (localStorage.getItem('campusId') == undefined || localStorage.getItem('campusId') == "") ? `instituteid=${this.state.orgId}` : `instituteid=${this.state.orgId}&campusId=${localStorage.getItem('campusId')}`
        Axios.get(`${this.state.env['zqBaseUri']}/setup/settings?${queryUrl}`, requestOptions)
            .then(res => {
                console.log("res", res)
                if (res.data.campus !== "" && res.data.campus !== null) {
                    let resData = res.data.campus;
                    let institutionName = resData.legalName;
                    let address1 = resData.legalAddress.address1;
                    let address2 = resData.legalAddress.address2;
                    let address3 = resData.legalAddress.address3;
                    let institutionCity = resData.legalAddress.city;
                    let institutionPincode = resData.legalAddress.pincode;
                    let institutionState = resData.legalAddress.state;
                    let institutionContact = resData.instituteContact[0].mobileNumber;
                    let instituteLogo = resData.logo;
                    this.setState({ institutionName, address1, address2, address3, institutionCity, institutionPincode, institutionState, institutionContact, instituteLogo })
                }
                else {
                    let respData = res.data.settings[0];
                    let institutionName = respData.instituteDetails.instituteName;
                    let address1 = respData.instituteDetails.address1;
                    let address2 = respData.instituteDetails.address2;
                    let address3 = respData.instituteDetails.address3;
                    let institutionCity = respData.instituteDetails.cityTown;
                    let institutionPincode = respData.instituteDetails.pinCode;
                    let institutionState = respData.instituteDetails.stateName;
                    let institutionContact = respData.instituteDetails.phoneNumber1;
                    let instituteLogo = respData.logo.logo;
                    this.setState({ institutionName, address1, address2, address3, institutionCity, institutionPincode, institutionState, institutionContact, instituteLogo })

                }
            }).catch(err => {
                console.log("err", err)
            })
        // fetch(`${this.state.env["zqBaseUri"]}/setup/settings?instituteid=${this.state.orgId}&campusId=605b089e828d962d045d1ce6`, requestOptions)
        //     .then(response => response.text())
        //     .then(result => {

        //          console.log("result", result)
        //         var results = JSON.parse(result)
        //         // console.log(results[0]['instituteDetails'])

        //         // let address1 = results[0]['instituteDetails']['address1'] || ""
        //         // let address2 = results[0]['instituteDetails']['address2'] || ""
        //         // let address3 = results[0]['instituteDetails']['address3'] || ""

        //         // document.getElementById("insname").innerHTML = results[0]['instituteDetails']['instituteName'] || ""
        //         // // document.getElementById("inssname").innerHTML = results[0]['instituteDetails']['instituteName'].toLowerCase()
        //         // //     .replace(/\b[a-z]/g, function (letter) { return letter.toUpperCase() }) + " " + 'Accounts Team'
        //         // document.getElementById("city").innerHTML = address1 + " " + address2 + " " + address3
        //         // document.getElementById("District").innerHTML = `${results[0]['instituteDetails']['cityTown']}, PIN: ${results[0]['instituteDetails']['pinCode']}` || ""
        //         // document.getElementById("state").innerHTML = `${results[0]['instituteDetails']['stateName']}, India` || ""
        //         // // document.getElementById("email").innerHTML = results[0]['instituteDetails']['email']

        //         // if (results[0]['instituteDetails']['phoneNumber1'] !== "" && results[0]['instituteDetails']['phoneNumber1'] !== null && results[0]['instituteDetails']['phoneNumber2'] !== "" && results[0]['instituteDetails']['phoneNumber2'] !== null) {
        //         //     document.getElementById("ph").innerHTML = `${results[0]['instituteDetails']['phoneNumber1']}, ${results[0]['instituteDetails']['phoneNumber2']}`
        //         // } else if (results[0]['instituteDetails']['phoneNumber1'] !== "" && results[0]['instituteDetails']['phoneNumber1'] !== null && (results[0]['instituteDetails']['phoneNumber2'] === "" || results[0]['instituteDetails']['phoneNumber2'] === null)) {
        //         //     document.getElementById("ph").innerHTML = `${results[0]['instituteDetails']['phoneNumber1']}`
        //         // } else if ((results[0]['instituteDetails']['phoneNumber1'] === "" || results[0]['instituteDetails']['phoneNumber1'] === null) && results[0]['instituteDetails']['phoneNumber2'] !== "" && results[0]['instituteDetails']['phoneNumber2'] !== null) {
        //         //     document.getElementById("ph").innerHTML = `${results[0]['instituteDetails']['phoneNumber2']}`
        //         // }

        //         // document.getElementById("ph").innerHTML = `${results[0]['instituteDetails']['phoneNumber1']}, ${results[0]['instituteDetails']['phoneNumber2']}`

        //         // this.setState({imgData:results[0]['logo']['logo']})
        //         // document.getElementById("img").src = typeof results[0]['logo'] === 'string' ? results[0]['logo'].split("?")[0] : results[0]['logo']
        //         // document.getElementById("img").src = typeof results[0]['logo']['logo'] === 'string' ? results[0]['logo']['logo'].split("?")[0] : results[0]['logo']
        //         // console.log(results[0]['logo']['logo'][0])

        //         this.setState({ email: results[0]['instituteDetails']['email'] }, () => {
        //             // console.log('email', this.state.email)
        //         })

        //     })
        //     .catch(error => console.log('error', error));
    }






    render() {
        let { institutionName, address1, address2, address3, institutionCity, institutionPincode, institutionState, institutionContact, instituteLogo } = this.state;
        return (
            <React.Fragment>
                <div style={{ padding: "15px", paddingTop: "5px" }}>
                    <div class="container" style={{ width: "100%", textAlign: "center" }}>
                        <div class="logo">
                            <img src={instituteLogo} style={{ width: "120px" }} />
                        </div>

                        <div class="address" style={{ width: "100%", paddingLeft: "25px", fontSize: "10px", textAlign: "center" }}>

                            <h3 style={{ fontFamily: "sans-serif", margin: "-5px", padding: "0", fontSize: "18px" }}>{institutionName}</h3>


                            <p style={{
                                lineHeight: "1.5", fontFamily: "sans-serif", margin: "0", padding: "0", fontSize: "13px"
                            }}>
                                {address1 ? `${address1},` : ""} {address2 ? `${address2},` : ""} {address3 ? `${address3}` : ""}
                            </p>

                            < p style={{ lineHeight: "1.5", fontFamily: "sans-serif", fontSize: "13px", margin: "0", padding: "0" }}>{institutionCity} {institutionPincode} | {institutionState}, Contact: {institutionContact}</p>

                        </div>
                    </div>​<div class="clearfix" style={{ clear: "both", display: "table", overflow: "auto" }}></div>
                    <div class="line" style={{ border: "1px solid", marginBottom: "5px" }}></div>

                    <h2 style={{ fontSize: "15px", textAlign: "center", margin: "20px", lineHeight: "0" }}>Fees Receipt</h2>
                    <div style={{ fontSize: "10px", fontFamily: "sans-serif", marginTop: "5px" }}>
                        <div style={{ width: "33.33%", float: "left", textAlign: "left", paddingBottom: "5px", fontSize: "13px" }}>
                            <div>Date:</div>
                            <div style={{ fontWeight: "bold" }}>DD/MM/YYYY</div>
                        </div>
                        <div style={{ width: "33.33%", float: "left", textAlign: "center", paddingBottom: "5px", fontSize: "13px" }}>
                            <div>Transaction ID:</div>
                            <div style={{ fontWeight: "bold" }}>XXXXXX</div>
                        </div>
                        <div style={{ width: "33.33%", float: "left", textAlign: "right", paddingBottom: "5px", fontSize: "13px" }}>
                            <div>Mode:</div>
                            <div style={{ fontWeight: "bold", textTransform: "uppercase" }}>XXXXXX</div>
                        </div>
                    </div>
                    <p style={{
                        fontSize: "14px",
                        marginTop: "81px",
                        fontFamily: "sans-serif",
                        textAlign: "center",
                        fontWeight: "bold",
                        marginBottom: "0px",
                        height: "0px"
                    }}>STUDENT DETAILS</p>
                    <table
                        style={{ width: "100%", border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", marginTop: "33px" }}>
                        <tbody>
                            <tr>
                                <td
                                    style={{ padding: "5px", textAlign: "center", backgroundColor: "#f2f2f2", fontWeight: "bold", fontFamily: "sans-serif", border: "1px solid #000", fontSize: "12px" }}>
                                    Academic Year</td>
                                <td
                                    style={{ padding: "5px", textAlign: "center", backgroundColor: "#f2f2f2", fontWeight: "bold", fontFamily: "sans-serif", border: "1px solid #000", fontSize: "12px" }}>
                                    Student Admission Number</td>
                                <td
                                    style={{ padding: "5px", textAlign: "center", backgroundColor: "#f2f2f2", fontWeight: "bold", fontFamily: "sans-serif", border: "1px solid #000", fontSize: "12px" }}>
                                    Student Name</td>
                                <td
                                    style={{ padding: "5px", textAlign: "center", backgroundColor: "#f2f2f2", fontWeight: "bold", fontFamily: "sans-serif", border: "1px solid #000", fontSize: "12px" }}>
                                    Class/Batch</td>
                            </tr>
                            <tr>
                                <td
                                    style={{ border: "1px solid black", textAlign: "center", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", padding: "5px" }}>
                                    XXXXXX</td>
                                <td
                                    style={{ border: "1px solid black", textAlign: "center", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", padding: "5px" }}>
                                    XXXXXX</td>
                                <td
                                    style={{ border: "1px solid black", textAlign: "center", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", padding: "5px" }}>
                                    XXXXXX</td>
                                <td
                                    style={{ border: "1px solid black", textAlign: "center", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", padding: "5px" }}>
                                    XXXXXX</td>
                            </tr>
                        </tbody>
                    </table>




                    <p style={{
                        fontSize: "14px",
                        margin: "21px 0 0",
                        fontFamily: "sans-serif",
                        textAlign: "center",
                        fontWeight: "bold",
                        marginTop: "10px",
                        marginBottom: "0px",
                        height: "0px"
                    }}>TRANSACTION DETAILS</p>
                    <table
                        style={{ width: "100%", border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", marginTop: "30px" }}>
                        <tbody>
                            <td
                                style={{ width: "20%", padding: "5px", backgroundColor: "#f2f2f2", textAlign: "center", fontWeight: "bold", fontFamily: "sans-serif", border: "1px solid #000", fontSize: "12px" }}>
                                S.No</td>
                            <td
                                style={{ width: "40%", padding: "5px", backgroundColor: "#f2f2f2", textAlign: "center", fontWeight: "bold", fontFamily: "sans-serif", border: "1px solid #000", fontSize: "12px" }}>
                                Particulars</td>
                            <td
                                style={{ width: "40%", padding: "5px", backgroundColor: "#f2f2f2", textAlign: "center", fontWeight: "bold", fontFamily: "sans-serif", border: "1px solid #000", fontSize: "12px" }}>
                                Paid Amount</td>
                            <tr style={{ border: "1px solid black" }}>
                                <td style={{ width: "10%", textAlign: "center", border: "1px solid black", borderCollapse: "collapse", fontSize: "10px", fontFamily: "sans-serif", padding: "5px" }}>
                                    XXXXXX</td>
                                <td style={{ width: "45%", textAlign: "center", border: "1px solid black", borderCollapse: "collapse", fontSize: "10px", fontFamily: "sans-serif", padding: "5px", textAlign: "center" }}>
                                    XXXXXX </td>
                                <td style={{ width: "45%", textAlign: "right", border: "1px solid black", borderCollapse: "collapse", fontSize: "10px", fontFamily: "sans-serif", padding: "5px" }}>
                                    XXXXXX</td>
                            </tr>
                            <tr>
                                <td colSpan="2"
                                    style={{ textAlign: "right", fontWeight: "bold", border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", padding: "5px" }}>
                                    TOTAL</td>
                                <td
                                    style={{ textAlign: "right", fontWeight: "bold", border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", padding: "5px" }}>
                                    XXXXXX</td>
                            </tr>
                            <tr>
                                <td colSpan="6"
                                    style={{ fontWeight: "bold", border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", padding: "5px" }}>
                                    AMOUNT IN WORDS: XXXXXX</td>
                            </tr>​ <tr>
                                <td colSpan="6"
                                    style={{ height: "auto", border: "1px solid black", borderCollapse: "collapse", padding: "5px", textAlign: "center" }}>
                                    <img src={receiptSvg} style={{ height: "100px" }} />
                                    <p style={{ textAlign: "center", margin: "0", padding: "0px" }}>Please scan the QR code to access this receipt from our portal</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <p style={{ textAlign: "center", margin: "3px", fontSize: "10px", height: "0px" }}>
                        The validity of this receipt is subjected to the realization of this transaction with our bank account
        </p>
                    {/* 
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br /> */}
                    <p style={{
                        fontSize: "14px",
                        margin: "0",
                        fontFamily: "sans-serif",
                        textAlign: "center",
                        fontWeight: "bold",
                        marginTop: "30px",
                        marginBottom: "5px",
                        height: "0px"
                    }}>STATEMENT OF ACCOUNT</p>
                    <table style={{ borderCollapse: "collapse", width: "100%", marginTop: "30px", border: "1px solid #000" }}>
                        <thead>

                            <th class="tableRowData" style={{ textAlign: "center", padding: "5px", backgroundColor: "#f2f2f2", fontWeight: "bold", fontFamily: "sans-serif", border: "1px solid #000", fontSize: "12px" }}>TERM</th>
                            <th class="tableRowData" style={{ textAlign: "center", padding: "5px", backgroundColor: "#f2f2f2", fontWeight: "bold", fontFamily: "sans-serif", border: "1px solid #000", fontSize: "12px" }}>DUE DATE</th>
                            <th class="tableRowData" style={{ textAlign: "center", padding: "5px", backgroundColor: "#f2f2f2", fontWeight: "bold", fontFamily: "sans-serif", border: "1px solid #000", fontSize: "12px" }}>PAID DATE</th>


                            <th class="tableRowData" style={{ textAlign: "center", padding: "5px", backgroundColor: "#f2f2f2", fontWeight: "bold", fontFamily: "sans-serif", border: "1px solid #000", fontSize: "12px" }}>AMOUNT</th>
                            <th class="tableRowData" style={{ textAlign: "center", padding: "5px", backgroundColor: "#f2f2f2", fontWeight: "bold", fontFamily: "sans-serif", border: "1px solid #000", fontSize: "12px" }}>STATUS</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="tableBodyData" style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", textAlign: "center", padding: "5px" }}>XXXXXX</td>
                                <td class="tableBodyData" style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", textAlign: "center", padding: "5px" }}>XXXXXX</td>
                                <td class="tableBodyData" style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", textAlign: "center", padding: "5px" }}>XXXXXX</td>

                                <td class="tableBodyData" style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", textAlign: "center", padding: "5px" }}>XXXXXX</td>
                                <td class="tableBodyData" style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", textAlign: "center", padding: "5px" }}>XXXXXX</td>

                            </tr>
                            <tr>
                                <td class="tableBodyData" style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", textAlign: "center", padding: "5px" }}>XXXXXX</td>
                                <td class="tableBodyData" style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", textAlign: "center", padding: "5px" }}>XXXXXX</td>
                                <td class="tableBodyData" style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", textAlign: "center", padding: "5px" }}>XXXXXX</td>

                                <td class="tableBodyData" style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", textAlign: "center", padding: "5px" }}>XXXXXX</td>
                                <td class="tableBodyData" style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", textAlign: "center", padding: "5px" }}>XXXXXX</td>
                            </tr>
                            <tr>
                                <td class="tableBodyData" style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", textAlign: "center", padding: "5px" }}><b>TOTAL</b></td>
                                <td class="tableBodyData" style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", textAlign: "center", padding: "5px" }}>-</td>

                                <td class="tableBodyData" style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", textAlign: "center", padding: "5px" }}>-</td>
                                <td class="tableBodyData" style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", textAlign: "center", padding: "5px" }}><b>XXXXXX</b></td>
                                <td class="tableBodyData" style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "12px", fontFamily: "sans-serif", textAlign: "center", padding: "5px" }}><b>XXXXXX</b></td>
                            </tr>
                        </tbody>
                    </table>



                    <p class="signature" style={{ textAlign: "center", margin: "0px", fontSize: "8px", marginBottom: "20px", marginTop: "10px" }}>
                        This is a computer generated fee receipt and does not require any signature.
    </p>
                </div>
            </React.Fragment >




        )
    }
}
export default Receipt;


 //         <React.Fragment>
            //             <div className="container">
            //                 <div className="logo" style={{ float: "left" }}>
            //                     <img id="img" alt="" width="148" height="148" />
            //                 </div>
            //                 <div className="address" style={{ float: "left", paddingTop: "15px", width: "320px", paddingLeft: "25px", fontSize: "15px" }}>
            //                     <h3 style={{ padding: "0px", margin: "0px" }} id="insname"></h3>
            //                     <p style={{ lineHeight: 1.5, margin: 0, padding: 0 }} id="city"></p>
            //                     <p style={{ lineHeight: 1.5, margin: 0, padding: 0 }} id="District"></p>
            //                     <p style={{ lineHeight: 1.5, margin: 0, padding: 0 }} id="state"></p>
            //                     <p style={{ lineHeight: 1.5, margin: 0, padding: 0 }}>Email: <span id="email">{this.state.email}</span> </p>
            //                     <p style={{ lineHeight: 1.5, margin: 0, padding: 0 }}>Contact: <span id="ph"></span> </p>
            //                 </div>
            //             </div>
            //             <div className="clearfix" style={{ clear: "both", display: "table", overflow: "auto" }}></div>
            //             <hr />
            //             <h2 style={{ textAlign: "center", margin: 0 }}>Fees Receipt</h2>
            //             <div style={{ fontSize: "12px", fontFamily: "sans-serif", marginTop: "10px" }}>
            //                 <div style={{ width: "33.33%", float: "left", textAlign: "left", marginBottom: "10px" }}>
            //                     <div>Date:</div>
            //                     <div style={{ fontWeight: "bold", marginTop: "5px" }}>DD/MM/YYYY</div>
            //                 </div>
            //                 <div style={{ width: "33.33%", float: "left", textAlign: "left", marginBottom: "10px" }}>
            //                     <div>Receipt No:</div>
            //                     <div style={{ fontWeight: "bold", marginTop: "5px" }}>YYYY/XXXXXX</div>
            //                 </div>
            //                 <div style={{ width: "33.33%", float: "left", textAlign: "left", marginBottom: "10px" }}>
            //                     <div>Mode:</div>
            //                     <div style={{ fontWeight: "bold", marginTop: "5px" }}>XXXXXX</div>
            //                 </div>
            //             </div>
            //             <table style={{ width: "100%", border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif" }}>
            //                 <tr>
            //                     <td style={{ width: "25%", border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>Academic Year</td>
            //                     <td style={{ width: "25%", border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>Application Id</td>
            //                     <td style={{ width: "25%", border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>Student Name</td>
            //                     <td style={{ width: "25%", border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>Class/Section</td>
            //                 </tr>
            //                 <tr>
            //                     <td style={{ width: "25%", border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>YYYY-YY</td>
            //                     <td style={{ width: "25%", border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>XXXXXX</td>
            //                     <td style={{ width: "25%", border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>XXXXXX</td>
            //                     <td style={{ width: "25%", border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>XXXXXX</td>
            //                 </tr>
            //             </table>
            //             <br />
            //             <table style={{ width: "100%", border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif" }}>
            //                 <tr>
            //                     <td style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>Sr. No.</td>
            //                     <td style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>Particulars</td>
            //                     <td style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>Previous Dues</td>
            //                     <td style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>Current Dues</td>
            //                     <td style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>Total Due Amount</td>
            //                     <td style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>Paid Amount</td>
            //                 </tr>
            //                 <tr>
            //                     <td style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>1</td>
            //                     <td style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>Application Fees</td>
            //                     <td style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px", textAlign: 'right' }}>0.00</td>
            //                     <td style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px", textAlign: 'right' }}>XXXXXX</td>
            //                     <td style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>XXXXXX</td>
            //                     <td style={{ border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>XXXXXX</td>
            //                 </tr>
            //                 <tr>
            //                     <td colSpan="4" style={{ width: "100%", border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>TOTAL</td>
            //                     <td style={{ width: "100%", border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>XXXXXX</td>
            //                     <td style={{ width: "100%", border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>XXXXXX</td>
            //                 </tr>
            //                 <tr>
            //                     <td colSpan="6" style={{ width: "100%", border: "1px solid black", borderCollapse: "collapse", fontSize: "14px", fontFamily: "sans-serif", padding: "10px" }}>AMOUNT IN WORDS: INR XXXXXX ONLY</td>
            //                 </tr>
            //             </table>
            //             <p className="signature" style={{ textAlign: "center", marginTop: "20px", fontSize: "13px" }}>
            //                 This is a computer generated fee receipt and does not require any signature
            // </p>
            //         </React.Fragment>