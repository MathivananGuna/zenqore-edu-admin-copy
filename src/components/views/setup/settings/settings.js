import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Radio from "@material-ui/core/Radio";
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../utils/Table/table-component";
// import "../../../../scss/billing.scss";
import "../../../../scss/settings.scss";
import '../../../../scss/student.scss';
import '../../../../scss/student-reports.scss';
import '../../../../scss/common-table.scss';
import Avatar from "react-avatar-edit";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import RestoreOutlinedIcon from "@material-ui/icons/RestoreOutlined";
import removeCircle from "../../../../assets/images/remove_circle.png";
import addCircle from "../../../../assets/images/add_circle.png";
import { SelectPicker } from "rsuite";
import PublishOutlinedIcon from "@material-ui/icons/PublishOutlined";
import LinearProgress from "@material-ui/core/LinearProgress";
import ZqHistory from "../../../../gigaLayout/change-history";
// import { Drawer, DatePicker } from "rsuite";
import moment from "moment";
import axios from "axios";
import ZenForm from '../../../input/form';
import AddData from "./settings.json";
import AddressForm from "./input_name_address.json";
import AcademicDurationForm from './input_academic_duration.json';
import DateFormatForm from './input_date_format.json';
import GSTINForm from './input_gstin.json';
import emailServerForm from './input_email-server.json';
import paymentGatewayForm from './input_payment-gateway.json';
import smsGatewayForm from './input_sms-gateway.json';
import headApproverForm from './head-approver.json'
import receiptForm from './input_receipts.json'
import receiptAmountForm from './input_amount_receipt.json';
import logoPositionsForm from './input_logo_position.json';
import portalLoginForm from './input_portal_login.json';
import feesMappingForm from './input_fees_mapping.json';
import receiptContentForm from './input_content.json';
import demandNoteForm from './input_demand_note.json';
import logosForm from './input_logos.json';
import Button from "@material-ui/core/Button";
import { Alert } from 'rsuite';
import Loader from "../../../../utils/loader/loaders";
import UploadSVGIcon from "../../../../assets/icons/settings-logo-upload.svg";
// import profilePicture from '../../../assets/images/profile-large.png';
// import templateCode from './temp';
import CircularProgress from '@material-ui/core/CircularProgress';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import { Timeline, Drawer } from 'rsuite';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import HistoryIcon from '@material-ui/icons/History';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import profilePicture from '../../../../assets/images/profile-large.png';
import DateFormatContext from '../../../../gigaLayout/context';
import DragAndDrop from '../../../../utils/dragDrop';
import DemandNoteTemplate from './templates/demandNoteTemplate';
import ReceiptTemplate from './templates/receiptTemplate';
import RefundTemplate from './templates/refundTemplate';
import StatementTemplate from './templates/statementTemplate';
import TextField from '@material-ui/core/TextField';
import EmailServer from './emailserver-component/settings-email-server'
// import DatePicker from 'react-datepicker'
// import "react-datepicker/dist/react-datepicker.css";
class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      labelTableHeader: ["LABEL", "VALUE"],
      labelTableData: [{ "regId": "REG ID", "ClassBatch": "CLASS/BATCH", "programPlan": "PROGRAM PLAN" }],
      //receipts
      receiptData: {},
      receiptValue: "",
      partialAmount: null,
      content: "",
      demandNoteContent: "1",
      bank: "",
      //
      isLogoTab: false,
      baseData: " ",
      src: null,
      favSrc: null,
      crop: {
        unit: '%',
        width: 30,
        height: 30
      },
      favCrop: {
        unit: '%',
        width: 30,
        height: 30
      },
      logoName: "None",
      shareHoldingVersion: " ",
      instituteAddressVersion: "0",
      logoPictureVersion: "0",
      emailServerVersion: "0",
      smsGatewayVersion: "0",
      paymentGatewayVersion: "0",
      logoText: " ",
      defaultnumberFormat: true,
      defaultImage: true,
      faviconImage: true,
      filterNumbering: " ",
      previewNumberName: "Demand Note Template",
      previewVersion: " ",
      companyLogo: " ",
      getNumArray: [],
      DemandNoteNumber: " ",

      ReceiptNumber: " ",
      StatementNumber: " ",
      StatementNumberVendor: " ",
      RefundNumberVendor: " ",
      RefundNumber: " ",
      showDefaultLogo: true,
      defNumber: "DN",
      //email tab value
      emailSelect: [
        {
          "value": "none",
          "label": "None"
        },
        {
          "value": "gmail",
          "label": "Gmail"
        },
        {
          "value": "sendgrid",
          "label": "SendGrid"
        }
      ],
      emailSelectValue: "",
      emailServerEmail1: "",
      emailServerEmail2: "",
      emailServerEmail3: "",
      emailServerEmail4: "",
      emailServerEmail5: "",
      newEmailConfigJson1: "",
      newEmailConfigJson2: "",
      newEmailConfigJson3: "",
      newEmailConfigJson4: "",
      newEmailConfigJson5: "",
      newConfigFileName: "",
      newConfigFileName2: "",
      newConfigFileName3: "",
      newConfigFileName4: "",
      newConfigFileName5: "",
      emailServerHelperText: "",
      emailServerError: false,
      emailServerHelperText2: "",
      emailServerError2: false,
      emailServerHelperText3: "",
      emailServerError3: false,
      emailServerHelperText4: "",
      emailServerError4: false,
      emailServerHelperText5: "",
      emailServerError5: false,
      currentJsonUpload: "",
      currentEmailServer: "",
      //
      slash: " ",
      defYear: " ",
      slash2: " ",
      defDigits: " ",
      previousLogoPicture: " ",
      logoPicture2: " ",
      faviconImagePic: "https://uploads-ssl.webflow.com/5ee891a3891ce1f991bcd2d6/5ef71159da6e9d49c1e2748c_favicon-32x32.png",
      logoPosition: "4",
      logos: "",
      feesMapping: "",
      loader: false,
      number: " ",
      slash: " ",
      year: " ",
      slash2: " ",
      digits: " ",
      logoPicture: "",
      favPicture: "",
      env: JSON.parse(localStorage.getItem("env")),
      email: localStorage.getItem("email"),
      channel: localStorage.getItem("channel"),
      authToken: localStorage.getItem("auth_token"),
      orgId: localStorage.getItem("orgId"),
      userId: localStorage.getItem("userID"),
      instituteId: localStorage.getItem("instituteId"),
      tab: 0,
      fileSize: "",
      pdfLoaded: 0,
      filename: "",
      // history: false,
      value: new Date(),
      instituteSwitch: true,
      configInfo: false,
      instituteTab: 0,
      receiptTab: 0,
      generalTab: 0,
      sendReceiptTab: 0,
      template: "",
      userName: "vijay",
      userEmail: localStorage.getItem("channel"),
      toasterOpen: false,
      message: '',
      historyData: [],
      demandNoteTemplate: '',
      receiptTemplate: '',
      statementTemplate: '',
      refundTemplate: '',
      isdemandNoteTemplate: false,
      isreceiptTemplate: false,
      isstatementTemplate: false,
      isrefundTemplate: false,
      uploadConfig: false,
      academicDurationFrom: '',
      academicDurationTo: '',
      academicSelectYear: '',
      dateFormat: "",
      startDate: '',
      toDate: '',
      showLogo: false,
      imageHash: '',
      onLogoError: true,
      // errMessage: 'alert-red',
      // successMessage: 'alert-green',
      toasterMessage: '',
      // templateData: [
      //   {
      //     createdAt: "2020-06-30T12:16:54",
      //     created_at: "23/07/2020 (02:14 PM)",
      //     currentData: "",
      //     description: ["Temaplate has been changed"],
      //     entity: "Setting",
      //     entityInstanceId: "5efb2d36540313064d84f526",
      //     orgEmail: "demouser233@demoautoparts.com",
      //     orgId: "5eeb331b4ada2a1f00727980",
      //     updatedAt: "2020-06-30T12:16:54",
      //     userId: "justin@webdesignmagics.com",
      //     userDetails: 'prashanth@wdm.com',
      //     version: 2,
      //     __v: 0,
      //     _id: "5efb2d36540313064d84f527",
      //   },
      //   {
      //     createdAt: "2020-06-30T12:16:54",
      //     created_at: "23/07/2020 (12:14 PM)",
      //     currentData: "",
      //     description: ["Template was added"],
      //     entity: "Setting",
      //     entityInstanceId: "5efb2d36540313064d84f526",
      //     orgEmail: "demouser233@demoautoparts.com",
      //     orgId: "5eeb331b4ada2a1f00727980",
      //     updatedAt: "2020-06-30T12:16:54",
      //     userId: "justin@webdesignmagics.com",
      //     userDetails: 'prashanth@wdm.com',
      //     version: 1,
      //     __v: 0,
      //     _id: "5efb2d36540313064d84f527",
      //   },
      // ],
      history: false,
      resText: "Fetching Data...",
      errText: "No Data",
      isErrorText: false,
      showHistory: false,
      containerNav: {
        isBack: false,
        name: "",
        isName: false,
        total: 0,
        isTotalCount: false,
        isSearch: false,
        isSort: false,
        isPrint: false,
        isShare: false,
        isNew: false,
        isDownload: false,
        newName: "Send",
        preview: false,
      },
      addressFormData: AddressForm,
      academicDurationFormData: AcademicDurationForm,
      dateFormatFormData: DateFormatForm,
      smsGatewayData: smsGatewayForm,
      emailServerFormData: emailServerForm,
      paymentGatewayData: paymentGatewayForm,
      headApproverData: headApproverForm,
      receiptFormData: receiptForm,
      receiptAmountFormData: receiptAmountForm,
      logoPositionsFormData: logoPositionsForm,
      portalLoginFormData: portalLoginForm,
      feesMappingFormData: feesMappingForm,
      logosFormData: logosForm,
      receiptContentFormData: receiptContentForm,
      demandNoteFormData: demandNoteForm,
      dataDN: [
        {
          label: "None",
          value: "None",
        },
        {
          label: "DN",
          value: "DN",
        },
        {
          label: "DMN",
          value: "DMN",
        }
      ],
      dataRCPT: [
        {
          label: "None",
          value: "None",
        },
        {
          label: "RCPT",
          value: "RCPT",
        },
        {
          label: "RCT",
          value: "RCT",
        },
      ],
      dataSTMT: [
        {
          label: "None",
          value: "None",
        },
        {
          label: "STMT",
          value: "STMT",
        }
      ],
      dataRefund: [
        {
          label: "None",
          value: "None",
        },
        {
          label: "RERCT",
          value: "RERCT",
        }
      ],
      dataSlash: [
        {
          label: "None",
          value: "None",
        },
        {
          label: "/",
          value: "/",
        },
        {
          label: "-",
          value: "-",
        },
      ],
      dataYear: [
        {
          label: "None",
          value: "None",
        },
        {
          label: "YYYY-YY",
          value: "YYYY-YY",
        },
        {
          label: "YY-YY",
          value: "YY-YY",
        },
        {
          label: "yyyy",
          value: "yyyy",
        },
        {
          label: "yy",
          value: "yy",
        },
        {
          label: "mm-yy",
          value: "mm-yy",
        },
        {
          label: "Mmm-yy",
          value: "Mmm-yy",
        },
        {
          label: "ddmmyy",
          value: "ddmmyy",
        },
        {
          label: "ddmmyyyy",
          value: "ddmmyyyy",
        },
        {
          label: "ddMmmyyyy",
          value: "ddMmmyyyy",
        },
      ],
      dataDgts: [
        {
          label: "2",
          value: "2",
        },
        {
          label: "3",
          value: "3",
        },
        {
          label: "4",
          value: "4",
        },
        {
          label: "5",
          value: "5",
        },
      ],

      tablerows: [
        { name: "Shareholder Name 1", sharesHeld: "5000", "%held": "20" },
        { name: "Shareholder Name 2", sharesHeld: "5000", "%held": "24" },
        { name: "Shareholder Name 3", sharesHeld: "5000", "%held": "30" },
        { name: "Shareholder Name 3", sharesHeld: "5000", "%held": "30" },

        // { name: "Shareholder Name 4", "%held": "5000", heldpercentage: "40" },
      ],
      opendialog: false,
      addData: AddData,
      numberingData: 0,
      emailConfigJson: "",
      emailConfigJsonOne: "",
      showPreviewConfig: false,
      configFileName: '',
      institutestateName: '',
      institutestateCode: '',
      portalLogin: '',
      allState: {},
      loaderStatus: false,
      selectedPaymentGateway: '',
      selectedEmailServer: '',
      selectedEmailServerOne: '',
      selectedSMSGateway: '',
      bankTableData: [],
      bankDetails: []
    };
    this.inputFile = React.createRef(this.inputFile);
    this.instituteRef = React.createRef(this.instituteRef);
    this.logoRef = React.createRef(this.logoRef);
    this.faviconRef = React.createRef(this.faviconRef);
    this.emailServerRef = React.createRef(this.emailServerRef);
    this.smsGatewayRef = React.createRef(this.smsGatewayRef);
    this.paymentGatewayRef = React.createRef(this.paymentGatewayRef);
    this.textAreaInputRef = React.createRef(this.textAreaInputRef);
    this.academicDurationRef = React.createRef(this.academicDurationRef);
    this.dateFormatRef = React.createRef(this.dateFormatRef)
    this.bankAccRef = React.createRef(this.bankAccRef);
    this.headApproverRef = React.createRef(this.headApproverRef)
    this.receiptRef = React.createRef(this.receiptRef)
    this.receiptContentRef = React.createRef(this.receiptContentRef)
    this.receiptAmountRef = React.createRef(this.receiptAmountRef)
    this.logoPositionsRef = React.createRef(this.logoPositionsRef)
    this.portalLoginRef = React.createRef(this.portalLoginRef)
    this.demandNoteRef = React.createRef(this.demandNoteRef)
    this.feesMappingRef = React.createRef(this.feesMappingRef);
    this.logosRef = React.createRef(this.logosRef)
  }
  static contextType = DateFormatContext;
  toasterClick = (message, toasterMessage) => {
    this.setState({ message, toasterMessage }, () => {
      this.setState({ toasterOpen: true })
    })
  };
  toasterClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({ toasterOpen: false })
  };

  openLogo = (value) => {
    let data;
    if (value === "address") {
      data = true;
    } else if (value === "logo") {
      data = false;
    }
    this.setState({ instituteSwitch: data })
    // this.setState({
    //   openshareholders: false,
    //   openESign: false,
    //   history: true,
    //   // shareHoldersHistory: false,
    // });
  };
  selectTabs = (event, newValue) => {
    this.setState({ tab: newValue }, () => {
      if (newValue === 1) {
        this.setState({ template: "Demand Note Template", previewNumberName: "Demand Note Template" })
      }
      // if (newValue === 1 || newValue === 2) {
      //   this.setState({ isLogoTab: true })
      // }
      // if (newValue === 0 || newValue === 3 || newValue === 4 || newValue === 5) {
      //   this.setState({ isLogoTab: false })
      // }
    })
  };
  selectInstituteTabs = (event, newValue) => {
    this.setState({ instituteTab: newValue })
  }
  selectGeneralTabs = (event, newValue) => {
    this.setState({ generalTab: newValue })
  }
  selectReceiptTabs = (event, newValue) => {
    this.setState({ receiptTab: newValue })
  }
  selectSendReceiptTab = (event, newValue) => {
    this.setState({ sendReceiptTab: newValue })
  }

  fileSelected = (ev, i) => {
    console.log(ev.target.files);
    let file = ev.target.files;
    this.setState({ filename: file[0].name, upload: true }, () => {
      this.onLoad(this.state.pdfLoaded);
    });
  };
  uploadEmailConfigFile = (event) => {
    const jsonFile = event.target.files[0];

    if (!jsonFile) {
      alert("Please select JSON format file.");
      return false;
    }

    if (!jsonFile.name.match(/\.(json)$/)) {
      alert("Selected File is invalid. Please Select .json file")
      return false;
    }

    if (event.target.files.length > 0) {
      var reader = new FileReader();
      console.log(jsonFile.name)
      reader.onload = (event) => {
        console.log(event)
        var jsonObj = JSON.parse(event.target.result);
        console.log(jsonObj)
        if (this.state.currentJsonUpload === 1) this.setState({ newEmailConfigJson1: jsonObj, newConfigFileName: jsonFile.name });
        if (this.state.currentJsonUpload === 2) this.setState({ newEmailConfigJson2: jsonObj, newConfigFileName2: jsonFile.name });
        if (this.state.currentJsonUpload === 3) this.setState({ newEmailConfigJson3: jsonObj, newConfigFileName3: jsonFile.name });
        if (this.state.currentJsonUpload === 4) this.setState({ newEmailConfigJson4: jsonObj, newConfigFileName4: jsonFile.name });
        if (this.state.currentJsonUpload === 5) this.setState({ newEmailConfigJson5: jsonObj, newConfigFileName5: jsonFile.name });
      }
      reader.readAsText(jsonFile);
    }
    else return false
  }

  onLoad = (load) => {
    let loadValue = this.state.pdfLoaded + 5;
    if (this.state.pdfLoaded < 100) {
      this.setState({ pdfLoaded: loadValue }, () => {
        setTimeout(() => {
          this.onLoad(loadValue);
        }, 100);
      });
    } else {
      setTimeout(() => {
        this.props.onUploadConfirm();
      }, 1000);
    }
  };
  selectValueInv = (value, item, event) => {
    console.log(value, "select options");
    this.setState({ number: value })
  };
  selectValueSlash = (value, item, event) => {
    console.log(value, "select slash in INV");
    this.setState({ slash: value })

  }
  selectValueYear = (value, item, event) => {
    console.log(value, "select year format in INV");
    this.setState({ year: value })

  }
  selectValueSlash2 = (value, item, event) => {
    console.log(value, "select slash2 in INV");
    this.setState({ slash2: value })

  }
  selectValueDigits = (value, item, event) => {
    console.log(value, "select no. of digits in INV");
    this.setState({ digits: value })

  }
  openHistory = (name) => {
    this.getChangeHistory(name);
    // this.setState({ history: true, showHistory: true });
  };
  onCloseForm = () => {
    this.setState({ showHistory: false });
  };
  // getChangeHistory = (entityInstanceId) => {

  //   let api_data = this.state.historyData;
  //   let historyData = [];
  //   api_data.sort((data1, data2) => {
  //     return Number(data2.created_at) - Number(data1.created_at);
  //   });
  //   api_data.map((item) => {
  //     let date = new Date(
  //       new Intl.DateTimeFormat("en-US", {
  //         month: "2-digit",
  //         day: "numeric",
  //         year: "numeric",
  //         hour: "2-digit",
  //         minute: "2-digit",
  //       }).format(item["created_at"])
  //     );
  //     let descriptionData = item.description["0"].split(",").map((desc, i) => {
  //       return (
  //         <p key={i}>
  //           <div style={{ display: "flex" }}>
  //             <span className="span-hist" style={{ width: "5%" }}>
  //               -
  //             </span>
  //             {desc}
  //           </div>
  //         </p>
  //       );
  //     });
  //     console.log(descriptionData);
  //     historyData.push({
  //       description: descriptionData,
  //       time: moment(date).format("hh:mm"),
  //       created_at: item["created_at"]
  //         ? moment(date).format("DD/MM/YYYY (hh:mm A)")
  //         : "-",
  //       version: item.version,
  //       btn: false,
  //     });
  //   });
  //   this.setState({ historyData: historyData });
  // };

  closeModel = () => {
    this.setState({ opendialog: false });
  };

  changeNumbering = (e, item, i) => {
    // let { demandNoteTemplate, receiptTemplate } = this.state;
    this.setState({ defaultnumberFormat: false, defNumber: " ", numberingData: -1, template: item["name"] }, () => {
      // if (item["name"] === "Demand Note") {
      //   this.setState({ demandNoteTemplate })
      // } else if (item["name"] === "Receipt") {
      //   this.setState({ receiptTemplate })
      // }
      var defaultNumberName = item.value.split("/");
      // console.log(defaultNumberName[0])
      this.setState({ dataDN: [] }, () => {
        let dataDN = [{ label: "none", value: "none" }]
        let obj = {}
        obj.label = defaultNumberName[0]
        obj.value = defaultNumberName[0]
        dataDN.push(obj)
        // console.log(obj)
        // console.log(dataDN)
        // console.log(dataDN[1].value)

        this.setState({ dataDN: dataDN, previewNumberName: item.name, previewVersion: item.version, defYear: defaultNumberName[1], defDigits: defaultNumberName[2] },
          () => {
            // console.log("split name before", this.state.defNumber)
            this.setState({
              defNumber: String(dataDN[1].value)
            }, () => {
              // console.log("split name after", this.state.defNumber)
              this.setState({ numberingData: i });
            })
          })
        // console.log(i, ":index");
      })



      // e.preventDefault();

    })
  };
  // shouldComponentUpdate(nextProps, nextState) {
  //   if (nextState != this.state) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }
  componentDidMount() {
    console.log('this.state.en', this.state.env['zqBaseUri'])
    this.getData('preview');
    console.log("settingsContext", this.context)
  }
  getData = async (type) => {

    if (type === 'preview') {
      this.setState({ loader: true, loaderStatus: false, template: 'Demand Note Template' });
    }
    if (type === "submit") {
      this.setState({ loaderStatus: false, template: '' });
    }
    if (type === 'email-preview') {
      this.setState({ template: 'Demand Note Template' });
    }
    axios.get(`${this.state.env['zqBaseUri']}/setup/settings?instituteid=${this.state.instituteId}`, {
      headers: {
        'Authorization': localStorage.getItem("auth_token")
      }
    })
      .then(res => {
        if (res.data.length !== 0) {
          // this.setState({ loader: true, loaderStatus: false });
          console.log("settings get api response", res.data);
          this.setState({ companyLogo: "A" })
          let instituteDetails = res.data[0]['instituteDetails']
          let emailServer = res.data[0]['emailServer']
          let paymentGateway = res.data[0]['paymentGateway']
          let smsGateway = res.data[0]['smsGateway']
          let logoData;
          if (res.data[0].logo) {
            logoData = typeof res.data[0].logo === "string" ? res.data[0].logo + '?' + Math.random() : res.data[0].logo['logo'] + '?' + Math.random();
          } else logoData = profilePicture
          // let logoData = res.data[0].logo !== "" ? res.data[0].logo + '?' + Math.random() : profilePicture
          let logoPicture;
          // let emailConfigJson = emailServer['config'][0] !== null || emailServer['config'][0] !== "" ? emailServer['config'][0] : "";
          let configFileName = "";
          if (emailServer['fileName']) {
            configFileName = emailServer['fileName'] !== null || emailServer['fileName'] !== "" || emailServer['fileName'] !== "undefined" ? emailServer['fileName'] : ""
          }
          console.log("configFileName", configFileName)
          // let emailConfigJsonOne = emailConfigJson;
          let instituteAddressVersion = instituteDetails["version"]
          let logoPictureVersion = res.data[0].logo["version"]
          let emailServerVersion = emailServer["version"]
          let smsGatewayVersion = smsGateway["version"]
          let paymentGatewayVersion = paymentGateway["version"]
          let stateArray = [];
          let configInfo;
          let uploadConfig;
          let demandNoteTemplate = `https://templatehosting.s3.amazonaws.com/template.html?id=${this.state.instituteId}`;
          let receiptTemplate = `https://receipthosting.s3.ap-south-1.amazonaws.com/receipt.html?id=${this.state.instituteId}`;
          let refundTemplate = `https://refundhosting.s3.ap-south-1.amazonaws.com/refund.html?id=${this.state.instituteId}`;

          let labelTableData;
          if (res.data[0]['label']) {
            labelTableData = res.data[0]['label'][0] ? res.data[0]['label'][0].hasOwnProperty("regId") && res.data[0]['label'][0].hasOwnProperty("classBatch") ? res.data[0]['label'] : [{ "regId": "REG ID", "classBatch": "CLASS/BATCH" }]
              : [{ "regId": "REG ID", "classBatch": "CLASS/BATCH" }];
          } else {
            labelTableData = [{ "regId": "REG ID", "classBatch": "CLASS/BATCH" }];
          }

          let institutestateName, institutestateCode, selectedEmailServer, selectedEmailServerOne, selectedSMSGateway, selectedPaymentGateway,
            academicSelectYear, academicDurationFrom, academicDurationTo, dateFormat;

          let headApprover = res.data[0]['headApprover'];
          if (headApprover.length !== 0) {
            headApproverForm.map(item => {
              if (item.name == "name") {
                item["defaultValue"] = headApprover[0].name

              }
              if (item.name == "designation") {
                item["defaultValue"] = headApprover[0].designation

              }
              if (item.name == "email") {
                item["defaultValue"] = headApprover[0].email

              }
              if (item.name == "phoneNumber3") {
                item["defaultValue"] = headApprover[0].phoneNumber

              }
            })
          }

          // -------- RECEIPTS --------- //
          let receiptData = res.data[0]["receipts"]
          let receiptValue, partialAmount, feesMapping, logos, content,
            demandNoteContent;
          if (receiptData) {
            if (Object.keys(receiptData).length !== 0) {
              receiptForm.map(item => {
                if (item.name === "receipts") {
                  item["defaultValue"] = receiptData["send"]
                  receiptValue = receiptData["send"]
                }
              })
              receiptAmountForm.map(item => {
                if (item.name === "receiptsAmount") {
                  item["defaultValue"] = String(receiptData["partialAmount"])
                  partialAmount = String(receiptData["partialAmount"])
                }
              })
              //feeMapping
              feesMappingForm.map(item => {
                if (item.name === "feesMapping") {
                  item["defaultValue"] = receiptData["feesMapping"]
                  feesMapping = receiptData["feesMapping"]
                }
              })
              //logos
              logosForm.map(item => {
                if (item.name === "logos") {
                  item["defaultValue"] = receiptData["logos"]
                  logos = receiptData["logos"]
                }
              })
              //content
              receiptContentForm.map(item => {
                if (item.name === "receiptsContent") {
                  item['defaultValue'] = receiptData['content']
                  content = receiptData['content']
                }
              })
              //demand note -- content
              demandNoteForm.map(item => {
                if (item.name === 'demandNoteContent') {
                  item['defaultValue'] = receiptData['demandNoteContent']
                  demandNoteContent = receiptData['demandNoteContent']
                  if (receiptData['demandNoteContent'] === "") {
                    item['defaultValue'] = "1"
                    demandNoteContent = "1"
                  }
                  //demand note -- bank
                  this.renderBank(demandNoteContent, receiptData['bank'])
                }
              })
              //---------------------//
            }
          }

          //portallogin
          let portalLogin = "";
          if (res.data[0]["portalLogin"]) {
            portalLoginForm.map(item => {
              if (item["name"] === 'portalLogin') {
                item["defaultValue"] = res.data[0]["portalLogin"]
                portalLogin = res.data[0]["portalLogin"]
              }
            })
          }

          let faviconImagePic, favPicture
          if (res.data[0]['favicon']) {
            faviconImagePic = res.data[0]['favicon']['favicon'] ? res.data[0]['favicon']['favicon'] : ''
            favPicture = faviconImagePic
          }

          let logoPosition = res.data[0]['logoPositions'] ? res.data[0]['logoPositions']['position'] ? res.data[0]['logoPositions']['position'] : '0' : "0"
          if (res.data[0]['logoPositions']) {
            if (res.data[0]['logoPositions']['position']) {
              logoPositionsForm.map(item => {
                if (item["name"] === "logoPosition") {
                  item["defaultValue"] = logoPosition
                } else {
                  item["defaultValue"] = "4"
                }
              })
            }
          }

          // let receiptAmount = res.data[0]["partialAmount"]
          // if (Object.keys(receiptAmount).length !== 0) {
          //   receiptAmountForm.map(item => {
          //     if (item.name === "receiptsAmount") {
          //       item["defaultValue"] = receiptAmount["partialAmount"]
          //     }
          //   })
          // }

          let bankTableData = []
          let bankDetails = res.data[0]['bankDetails']
          if (bankDetails) {
            bankDetails.map(item => {
              bankTableData.push({
                "Bank Name": item.bankName,
                "Bank Account Name": item.bankAccountName,
                "Bank Account Number": item.bankAccountNumber,
                "Bank IFSC": item.bankIFSC,
                // "Bank Email Address": item.bankEmailAddress
              })
            })
          }

          // if (res.data[0].logo !== "") {
          //   this.toDataURL(res.data[0].logo['logo'])
          //     .then(dataUrl => {
          //       // console.log('RESULT:', dataUrl)
          //       this.setState({ logoPicture: dataUrl }, () => {
          //         // console.log(this.state.logoPicture)
          //       })
          //     })
          // }

          if (typeof res.data[0].logo === "string") {
            this.setState({ logoPicture: res.data[0].logo })
          } else {
            this.setState({ logoPicture: res.data[0].logo["logo"] })
          }

          // let startDate, toDate;
          // let dates = instituteDetails['academicYear'].split('-')
          // startDate = new Date(dates[0]);
          // toDate = new Date(dates[1]);
          // if (emailServer["emailServer"] && emailServer["config"][0]) {
          //   configInfo = true;
          // } else {
          //   configInfo = false;
          // }
          //new
          // if (emailConfigJson !== "") {
          //   configInfo = true;
          //   uploadConfig = true;
          // } else {
          //   configInfo = false;
          //   uploadConfig = true;
          // }
          //  ------------------------------------
          let institudeDetailsObj = Object.keys(instituteDetails)
          let addressFormArray = [];
          AddressForm.map(item => {
            if (item["name"] !== undefined) {
              addressFormArray.push(item["name"])
            }
          })
          AddressForm.map((item) => {
            institudeDetailsObj.map(objItem => {
              if (item['name'] == objItem) {
                if (item['name'] === "stateName" && objItem === "stateName") {
                  item['defaultValue'] = instituteDetails["stateCode"]
                  institutestateName = instituteDetails["stateName"]
                  institutestateCode = instituteDetails["stateCode"]
                }
                else {
                  if (instituteDetails[objItem]) item['defaultValue'] = instituteDetails[objItem].replace(/\,/g, "")
                }
              }
            })
            return
          })
          let result = addressFormArray.filter(item => institudeDetailsObj.indexOf(item) == -1)
          AddressForm.map(item => {
            result.map(i => {
              if (item["name"] === i) {
                item['defaultValue'] = "";
              }
            })
          })
          // AcademicDurationForm.map(item => {
          //   institudeDetailsObj.map(objItem => {
          //     if (item['name'] === "academicYear" && objItem === "academicYear") {
          //       if (instituteDetails["academicYear"] !== null) {
          //         let data = instituteDetails["academicYear"].split("-");
          //         if (item["label"] === "From Month") {
          //           item['defaultValue'] = data[0]
          //           academicDurationFrom = data[0]
          //         } else if (item["label"] === "To Month") {
          //           item['defaultValue'] = data[1]
          //           academicDurationTo = data[1]
          //         } else if (item["label"] === "To Year") {
          //           item['defaultValue'] = data[2]
          //           academicSelectYear = data[2]
          //         }
          //       }
          //     }
          //   })
          // })
          DateFormatForm.map(item => {
            institudeDetailsObj.map(objItem => {
              if (item['name'] === "dateFormat" && objItem === "dateFormat") {
                if (instituteDetails["dateFormat"] == null || instituteDetails["dateFormat"] == "") {
                  item['defaultValue'] = "DD-MM-YYYY"
                  // dateFormat = "DD-MM-YYYY"
                  this.calcDateFormatExample("DD-MM-YYYY")
                }
                else {
                  item['defaultValue'] = instituteDetails["dateFormat"]
                  dateFormat = instituteDetails["dateFormat"]
                  this.calcDateFormatExample(dateFormat)
                }
              }
            })
          })
          //  ------------------------------------
          // let smsGatewayDetailsObj = Object.keys(smsGateway)
          // smsGatewayForm.map((item, key) => {
          //   smsGatewayDetailsObj.map(objItem => {
          //     if (item['name'] == "smsGateway") {
          //       if (smsGateway[objItem] == null) {
          //         objItem['defaultValue'] = "none"
          //       }
          //       else {
          //         objItem['defaultValue'] = smsGateway[item]
          //       }
          //     }
          //     else if (objItem['name'] === "senderName") {
          //       selectedSMSGateway = smsGateway[item];
          //     }
          //     else if (objItem['name'] === "apiKey") {
          //       selectedSMSGateway = smsGateway[item];
          //     }
          //     else if (objItem['name'] === "phoneNumber") {
          //       selectedSMSGateway = smsGateway[item];
          //     }
          //   })
          //   return
          // })

          smsGatewayForm.map((data, i) => {
            if (data['name'] == "smsGateway") {
              if (smsGateway.smsGateway == null) {
                data['defaultValue'] = "none"
              }
              else {
                data['defaultValue'] = smsGateway.smsGateway
              }
            }
            else if (data['name'] == "senderName") {
              data['defaultValue'] = smsGateway.senderName
            }
            else if (data['name'] == "apiKey") {
              data['defaultValue'] = smsGateway.apiKey
            }
            else if (data['name'] == "phoneNumber") {
              data['defaultValue'] = smsGateway.phoneNumber
            }
          })

          //  ------------------------------------
          let emailServerDetailsObj = Object.keys(emailServer)
          emailServerForm.map((item, key) => {
            emailServerDetailsObj.map(objItem => {
              if (item['name'] == "senderEmail") {
                item['defaultValue'] = emailServer["emailAddress"]
              }
              else if (item['name'] == "emailServer") {
                item['defaultValue'] = emailServer["emailServer"]
                selectedEmailServer = emailServer["emailServer"]
                selectedEmailServerOne = emailServer["emailServer"]
              }
            })
            return
          })

          //new email server tab
          let emailSelectValue, emailServerEmail1, emailServerEmail2, emailServerEmail3, emailServerEmail4, emailServerEmail5,
            newEmailConfigJson1, newEmailConfigJson2, newEmailConfigJson3, newEmailConfigJson4, newEmailConfigJson5,
            newConfigFileName, newConfigFileName2, newConfigFileName3, newConfigFileName4, newConfigFileName5,
            currentEmailServer;

          if (emailServer[0]) {
            this.state.emailSelect.map(item => {
              if (item.value === emailServer[0]["emailServer"]) {
                emailSelectValue = emailServer[0]["emailServer"]
                currentEmailServer = emailServer[0]["emailServer"]
              }
            })
          }

          if (emailServer[0]) {
            if (emailServer[0]["emailAddress"]) emailServerEmail1 = emailServer[0]["emailAddress"]
            else emailServerEmail1 = ""
          }
          if (emailServer[1]) {
            if (emailServer[1]["emailAddress"]) emailServerEmail2 = emailServer[1]["emailAddress"]
            else emailServerEmail2 = ""
          }
          if (emailServer[2]) {
            if (emailServer[2]["emailAddress"]) emailServerEmail3 = emailServer[2]["emailAddress"]
            else emailServerEmail3 = ""
          }
          if (emailServer[3]) {
            if (emailServer[3]["emailAddress"]) emailServerEmail4 = emailServer[3]["emailAddress"]
            else emailServerEmail4 = ""
          }
          if (emailServer[4]) {
            if (emailServer[4]["emailAddress"]) emailServerEmail5 = emailServer[4]["emailAddress"]
            else emailServerEmail5 = ""
          }

          if (emailServer[0]) {
            if (emailServer[0]["config"]) {
              if (emailServer[0]["config"][0]) newEmailConfigJson1 = emailServer[0]["config"][0]
              else newEmailConfigJson1 = ""
            }
            else newEmailConfigJson1 = ""
          }
          if (emailServer[1]) {
            if (emailServer[1]["config"]) {
              if (emailServer[1]["config"][0]) newEmailConfigJson2 = emailServer[1]["config"][0]
              else newEmailConfigJson2 = ""
            }
            else newEmailConfigJson2 = ""
          }
          if (emailServer[2]) {
            if (emailServer[2]["config"]) {
              if (emailServer[2]["config"][0]) newEmailConfigJson3 = emailServer[2]["config"][0]
              else newEmailConfigJson3 = ""
            }
            else newEmailConfigJson3 = ""
          }
          if (emailServer[3]) {
            if (emailServer[3]["config"]) {
              if (emailServer[3]["config"][0]) newEmailConfigJson4 = emailServer[3]["config"][0]
              else newEmailConfigJson4 = ""
            }
            else newEmailConfigJson4 = ""
          }
          if (emailServer[4]) {
            if (emailServer[4]["config"]) {
              if (emailServer[4]["config"][0]) newEmailConfigJson5 = emailServer[4]["config"][0]
              else newEmailConfigJson5 = ""
            }
            else newEmailConfigJson5 = ""
          }

          if (emailServer[0]) {
            if (emailServer[0]["fileName"]) newConfigFileName = emailServer[0]["fileName"]
            else newConfigFileName = ""
          }
          if (emailServer[1]) {
            if (emailServer[1]["fileName"]) newConfigFileName2 = emailServer[1]["fileName"]
            else newConfigFileName2 = ""
          }
          if (emailServer[2]) {
            if (emailServer[2]["fileName"]) newConfigFileName3 = emailServer[2]["fileName"]
            else newConfigFileName3 = ""
          }
          if (emailServer[3]) {
            if (emailServer[3]["fileName"]) newConfigFileName4 = emailServer[3]["fileName"]
            else newConfigFileName4 = ""
          }
          if (emailServer[4]) {
            if (emailServer[4]["fileName"]) newConfigFileName5 = emailServer[4]["fileName"]
            else newConfigFileName5 = ""
          }
          //  ------------------------------------

          //  ------------------------------------
          let paymentGatewayDetailsObj = Object.keys(paymentGateway)
          paymentGatewayForm.map((item, key) => {
            paymentGatewayDetailsObj.map(objItem => {
              if (item['name'] == objItem) {
                item['defaultValue'] = paymentGateway[objItem] == null ? "" : paymentGateway[objItem]
                if (item['name'] == "paymentGateway") {
                  selectedPaymentGateway = paymentGateway["paymentGateway"] == null ? "" : paymentGateway["paymentGateway"]
                }
              }
            })
            return
          })

          //  -------------------------------------------------------
          this.setState({
            addressFormData: AddressForm,
            smsGatewayData: smsGatewayForm,
            emailServerFormData: emailServerForm,
            paymentGatewayData: paymentGatewayForm, academicDurationFormData: AcademicDurationForm, headApproverData: headApproverForm,
            showPreviewConfig: true, academicDurationTo, academicDurationFrom, academicSelectYear,
            loaderStatus: true, institutestateName, institutestateCode, selectedEmailServer, selectedSMSGateway, selectedPaymentGateway,
            defaultImage: true, faviconImage: true, configInfo, instituteAddressVersion, logoPictureVersion, emailServerVersion, smsGatewayVersion,
            paymentGatewayVersion, configFileName, labelTableData, bankTableData, bankDetails,
            demandNoteTemplate, receiptTemplate, refundTemplate,
            isdemandNoteTemplate: true, isreceiptTemplate: true, isstatementTemplate: true, isrefundTemplate: true, uploadConfig,
            selectedEmailServerOne, previewNumberName: "Demand Note Template",
            dateFormat, imageHash: Date.now(), onLogoError: true, portalLogin,
            //email server tab
            emailSelectValue, emailServerEmail1, emailServerEmail2, emailServerEmail3, emailServerEmail4, emailServerEmail5,
            newEmailConfigJson1, newEmailConfigJson2, newEmailConfigJson3, newEmailConfigJson4, newEmailConfigJson5,
            newConfigFileName, newConfigFileName2, newConfigFileName3, newConfigFileName4, newConfigFileName5, currentEmailServer,
            //receipt tab
            receiptData, receiptValue, partialAmount, feesMapping, logos, logoPosition, content, demandNoteContent
          }, () => {
            console.log(smsGatewayForm, this.state.smsGatewayData)
            this.setState({ logoPicture2: logoData, faviconImagePic, favPicture }, () => {
              this.setState({ showLogo: true })
            });
            this.getSettingsData();
            // this.getChangeHistory("instituteDetails");
          })
          setTimeout(() => {
            this.setState({ showLogo: false })
          }, 1000);
        } else {
          this.setState({
            isErrorText: true, loaderStatus: true, defaultImage: true, faviconImage: true, configInfo: false,
            logoPicture2: profilePicture, faviconImagePic: 'https://uploads-ssl.webflow.com/5ee891a3891ce1f991bcd2d6/5ef71159da6e9d49c1e2748c_favicon-32x32.png',
            logoPosition: "4"
          }, () => {
            this.getSettingsData();
            console.log('called from else part')
            this.resetForm();

          })
        }
      }).catch(err => {
        this.setState({ isErrorText: true, loaderStatus: true, defaultImage: true, faviconImage: true, configInfo: false, }, () => {
          this.getSettingsData();
          console.log('called from catch part', err)
          this.resetForm();
        })
      })
  }
  getSettingsData = () => {
    // this.setState({ addressFormData: AddressForm });
    // axios({
    //   url: `${this.state.env["zqBaseUri"]}/zq/settings?orgId=${this.state.orgId}`,
    //   method: 'GET'
    // }).then(res => {
    //   console.log("settings get api response", res.data);
    //   var numberingDta = res.data.Numbering;
    //   var companyData = res.data.company;
    var numDataArray = [];
    this.setState({
      DemandNoteNumber: 'DN',
      ReceiptNumber: 'RCPT',
      StatementNumber: 'STMT',
      RefundNumber: 'REFND'
    }, () => {
      numDataArray.push(
        {
          "name": "Demand Note Template",
          "value": this.state.DemandNoteNumber,
          "version": "v0"
        },
        {
          "name": "Receipt Template",
          "value": this.state.ReceiptNumber,
          "version": "v0"
        },
        {
          "name": "Statement Template",
          "value": this.state.StatementNumber,
          "version": "v0"
        },
        {
          "name": "Refund Template",
          "value": this.state.RefundNumber,
          "version": "v0"
        })
      this.setState({ getNumArray: numDataArray, companyLogo: "A", logoText: "Change Logo", loader: false })
      // console.log(numDataArray)
    });
    //   this.setState({
    //     DemandNoteNumber: numberingDta.InvoiceNumber.value, PDemandNoteNumber: numberingDta.PurchaseInvoiceNumber.value, ReceiptNumber: numberingDta.Quotation.value, StatementNumber: numberingDta.DebitNoteCustomer.value, StatementNumberVendor: numberingDta.DebitNoteVendor.value, RefundNumber: numberingDta.CreditNoteCustomer.value, RefundNumberVendor: numberingDta.CreditNoteVendor.value
    //     , PurOrder: numberingDta.PurchaseOrder.value, JournalNum: numberingDta.Journal.value, PayrollNum: numberingDta.Payroll.value, PaymentNum: numberingDta.payment.value, ReceiptNum: numberingDta.receipt.value,
    //     ReimNum: numberingDta.reimbursement.value, ReversalNum: numberingDta.reversal.value, SettleNum: numberingDta.settlement.value, ContraNum: numberingDta.Contra.value,
    //     logoPicture2: companyData.logo.value, logoPictureVersion: companyData.logo.version, previousLogoPicture: companyData.logo.prev_value, logoText: "Change Logo"
    //   })
  }
  formatDate = (date) => {
    let arr = [String(date)];
    let a = arr[0].split("/")
    return `${a[1]}/${a[0]}/${a[2]}`
  }
  formatText = (text) => {
    let result = text.replace(/([A-Z])/g, " $1");
    result = result.charAt(0).toUpperCase() + result.slice(1);
    return result
  }
  getChangeHistory = (name) => {
    let historyData = [];
    axios.get(`${this.state.env['zqBaseUri']}/setup/feescollectionhistory?orgid=${this.state.instituteId}&fieldname=${name}`)
      .then(res => {
        // console.log("change history response", res)
        if (res.data.length !== 0) {
          let data = res.data;
          data = data.reverse();
          data.forEach(item => {
            historyData.push({
              createdAt: this.formatDate(item["updateAt"]),
              created_at: this.formatDate(item["updateAt"]),
              currentData: "",
              description: [`${this.formatText(item["fieldName"])} changed from ${item["oldValue"]} to ${item["newValue"]} by ${item["updatedBy"]}`],
              entity: item["nameOfField"],
              // entityInstanceId: "5efb2d36540313064d84f526",
              orgEmail: item["userEmail"],
              orgId: String(this.state.instituteId),
              updatedAt: this.formatDate(item["updateAt"]),
              userId: item["userEmail"],
              userDetails: item["userEmail"],
              version: item["version"],
              __v: item["__v"],
              _id: item["_id"],
            })
          })
          this.setState({ historyData }, () => {
            this.setState({ history: true, showHistory: true });
          })
        } else {
          this.setState({ historyData }, () => {
            this.setState({ history: true, showHistory: true });
          })
        }
      })
  }

  onBeforeFileLoad(elem) {
    //File size is set to 1.5MB
    if (elem.target.files[0].size > 1500000) {
      Alert.error("Image size cannot be greater than 1.5MB !")
      elem.target.value = "";
    };
  }
  submitReply = () => {
    this.setState({ loader: true }, () => { console.log(this.state.logoPicture) })

    console.log(this.state.tablerows)
    let payloadObject = {
      "user_id": this.state.userId,
      "company": {
        "OrgId": this.state.orgId,
        "logo": this.state.logoPicture,
        "E-signature": "signature",
        "ShareholdingPattern": this.state.tablerows
      },
      "Numbering": {
        "InvoiceNumber": "INV/YYYY-YY/001",
        "PurchaseInvoiceNumber": "EXP/YYYY-YY/001",
        "Quotation": "QO/YYYY-YY/001",
        "DebitNoteCustomer": "DBNC/YYYY-YY/001",
        "DebitNoteVendor": "DBNV/YYYY-YY/001",
        "CreditNoteCustomer": "CRNC/YYYY-YY/001",
        "CreditNoteVendor": "CRNV/YYYY-YY/001",
        "PurchaseOrder": "PO/YYYY-YY/001",
        "Journal": "JRNL/YYYY-YY/001",
        "Payroll": "PYRL/YYYY-YY/001",
        "payment": "PYT/YYYY-YY/001",
        "receipt": "RCT/YYYY-YY/001",
        "reimbursement": "ADV/YYYY-YY/001",
        "reversal": "RINV/YYYY-YY/001",
        "settlement": "STL/YYYY-YY/001",
        "Contra": "CNTR/YYYY-YY/001",
      },
      "General": {
        "Currency": "US",
        "Units": "MKS",
        "FinancialYear": "APR-MAR",
        "Termsandcondition": {

          "1": "Terms and conditions Apply as per our Master sales Agreement",
          "2": "Payment is due date mentioned in this invoice. A delay of each day will attract 0.25% interest per day",
          "3": "The third line here"

        }
      }
    };
    console.log(JSON.stringify(payloadObject))
    axios({
      url: `${this.state.env["zqBaseUri"]}/zq/settings`,
      method: 'PUT',
      data: JSON.stringify(payloadObject),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': localStorage.getItem("auth_token")

      }
    }).then(res => {
      console.log("change settings API response", res)
      setTimeout(() => {
        // this.setState({ loader: false });
        window.location.reload();
        this.getSettingsData();
      }, 1500)
      this.setState({ defaultImage: false })
    }).catch(err => {
      this.setState({ loader: false, });
      Alert.error("Failed to upload profile picture. Please try again !")
    })

  }
  changeLogo = () => {
    this.setState({ showDefaultLogo: false })
  }
  clearReply = () => {
    this.setState({ showDefaultLogo: true })

  }
  toDataURL = url => fetch(url)
    .then(response => response.blob())
    .then(blob => new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.onloadend = () => resolve(reader.result)
      reader.onerror = reject
      reader.readAsDataURL(blob)
    }))
  // toDataURL = async (src, callback, outputFormat) => {
  //   var img = new Image();
  //   img.crossOrigin = 'anonymous';
  //   img.onload = function () {
  //     var canvas = document.createElement('CANVAS');
  //     var ctx = canvas.getContext('2d');
  //     var dataURL;
  //     canvas.height = this.naturalHeight;
  //     canvas.width = this.naturalWidth;
  //     ctx.drawImage(this, 0, 0);
  //     dataURL = canvas.toDataURL(outputFormat);
  //     callback(dataURL);
  //   };
  //   img.src = src;
  //   if (img.complete || img.complete === undefined) {
  //     img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
  //     img.src = src;
  //   }
  // }
  //ONCE THE IMG FILE TRIGGERED, THE BELOW FUNCTION TRIGGERS
  onSelectFile = e => {
    console.log(e.target.files[0])
    //getting logo name
    // let logoName = e.target.files[0]["name"];
    // let index = logoName.lastIndexOf(".")
    // logoName = logoName.substr(0, index)
    //get file size
    // let logoSize = e.target.files[0]["size"] / 1000;
    this.setState({ defaultImage: false })

    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => {
        this.setState({ src: reader.result }) // bae64 format of full image
      });
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  onSelectFavicon = e => {
    this.setState({ faviconImage: false })
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => {
        this.setState({ favSrc: reader.result }) // bae64 format of full image
      });
      reader.readAsDataURL(e.target.files[0]);
    }
  }


  // If you setState the crop in here you should return false.
  onImageLoaded = image => {
    console.log('called')
    this.imageRef = image; // the image tag
    let imgRef = this.logoRef.current["imageRef"]
    let height = imgRef.offsetWidth;
    let width = imgRef.offsetHeight;
    console.log(width, height)
    // if (width >= "643" || height >= "343") {
    //   this.toasterClick('the img is wwrong', 'alert-red')
    //   setTimeout(() => {
    //     this.setState({defaultImage: true})
    //   }, 1000);
    // }
  };

  onCropComplete = crop => {
    this.makeClientCrop(crop);
  };

  onCropChange = (crop, percentCrop) => {
    // You could also use percentCrop:
    this.setState({ crop }); // cropped part of the whole image
  };

  async makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      const croppedImageUrl = await this.getCroppedImg(
        this.imageRef,
        crop,
        'logo.jpeg'
      );
      this.setState({ croppedImageUrl }, () => {
        console.log(croppedImageUrl);
        // this.imageRef.src = ''
      });

    }
  }

  getCroppedImg(image, crop, fileName) {
    const canvas = document.createElement('canvas');
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );

    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          console.error('Canvas is empty');
          return;
        }
        blob.name = fileName;
        window.URL.revokeObjectURL(this.fileUrl);
        this.fileUrl = window.URL.createObjectURL(blob);
        resolve(this.fileUrl);
        var reader = new FileReader();
        reader.readAsDataURL(blob);
        const scope = this
        reader.onloadend = function () {
          var base64data = reader.result;
          // console.log(base64data);
          scope.setState({ logoPicture: base64data })
        }
      }, 'image/jpg');

    });
  }

  //FAVICON
  onImageLoadedFav = image => {
    console.log('called')
    this.imageRef = image; // the image tag
    let imgRef = this.faviconRef.current["imageRef"]
    let height = imgRef.offsetWidth;
    let width = imgRef.offsetHeight;
    console.log(width, height)
  };

  onCropCompleteFav = favCrop => {
    this.makeClientCropFav(favCrop);
  };

  onCropChangeFav = (favCrop, percentCrop) => {
    // You could also use percentCrop:
    this.setState({ favCrop }); // cropped part of the whole image
  };

  async makeClientCropFav(favCrop) {
    if (this.imageRef && favCrop.width && favCrop.height) {
      const croppedImageUrl = await this.getCroppedImgFav(
        this.imageRef,
        favCrop,
        'logo.jpeg'
      );
      this.setState({ croppedImageUrl }, () => {
        console.log(croppedImageUrl);
      });

    }
  }

  getCroppedImgFav(image, favCrop, fileName) {
    const canvas = document.createElement('canvas');
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = favCrop.width;
    canvas.height = favCrop.height;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
      image,
      favCrop.x * scaleX,
      favCrop.y * scaleY,
      favCrop.width * scaleX,
      favCrop.height * scaleY,
      0,
      0,
      favCrop.width,
      favCrop.height
    );

    return new Promise((resolve, reject) => {
      canvas.toBlob(blob => {
        if (!blob) {
          console.error('Canvas is empty');
          return;
        }
        blob.name = fileName;
        window.URL.revokeObjectURL(this.fileUrl);
        this.fileUrl = window.URL.createObjectURL(blob);
        resolve(this.fileUrl);
        var reader = new FileReader();
        reader.readAsDataURL(blob);
        const scope = this
        reader.onloadend = function () {
          var base64data = reader.result;
          // console.log(base64data);
          scope.setState({ favPicture: base64data })
        }
      }, 'image/jpg');

    });
  }

  calcDateFormatExample = (format) => {
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"]
    let dateFormat;
    let date = new Date();
    let day = (date.getDate() < 10 ? '0' : '') + date.getDate();
    let month = ((date.getMonth() + 1) < 10 ? '0' : '') + (date.getMonth() + 1);
    let year = date.getFullYear();
    if (format === 'DD/MM/YYYY') {
      dateFormat = `${day}/${month}/${year}`
    } else if (format === 'DD-MM-YYYY') {
      dateFormat = `${day}-${month}-${year}`
    } else if (format === 'DD/MMM/YYYY') {
      dateFormat = `${day}/${months[date.getMonth()]}/${year}`
    } else if (format === 'DD-MMM-YYYY') {
      dateFormat = `${day}-${months[date.getMonth()]}-${year}`
    }
    DateFormatForm.map(item => {
      if (item["name"] === "example") {
        item["defaultValue"] = dateFormat
      }
    })
    this.setState({ dateFormatFormData: DateFormatForm })
  }

  renderBank = (demandNoteValue, bankValue) => {
    let withBank = [{
      "type": "select",
      "name": "selectBank",
      "class": "select-input-wrap setup-input-wrap settings-address-input demand-note-select",
      "label": "Select Bank",
      "options": [
        {
          "value": "0",
          "label": "SBI"
        }
      ],
      "placement": "bottom",
      "readOnly": false,
      "requiredBoolean": true
    }]

    let demandNoteJSON = this.state.demandNoteFormData;
    this.setState({ loaderStatus: false, resText: "Loading..." }, () => {
      if (demandNoteValue === '0') {
        let index = demandNoteJSON.findIndex(item => item.name === "selectBank")
        if (index === -1) {
          demandNoteJSON = demandNoteJSON.concat(withBank)
          demandNoteJSON.map(item => {
            if (item['name'] === 'selectBank') {
              item['defaultValue'] = bankValue
            }
          })
        }
      }
      else if (demandNoteValue === '1') {
        demandNoteJSON = demandNoteJSON.filter(item => item.name !== "selectBank")
      }
      this.setState({ demandNoteFormData: demandNoteJSON, loaderStatus: true, bank: bankValue })
    })
  }

  onInputChanges = (value, item, event, dataS) => {
    // console.log("value", value)
    // console.log("item", item)
    // console.log("event", event)
    // console.log("dataS", dataS)
    let { emailConfigJsonOne } = this.state;

    item['defaultValue'] = value

    if (dataS !== undefined) {
      if (dataS.name == "stateName") {
        this.setState({ institutestateName: item.label, institutestateCode: item.value });
      }
      else if (dataS.name == "paymentGateway") {
        this.setState({ selectedPaymentGateway: item.value });
      }
      else if (dataS.name == "smsGateway") {
        this.setState({ selectedSMSGateway: item.value });
      }
      else if (dataS.name == "emailServer") {
        this.setState({ selectedEmailServer: item.value });
      }
    }

    if (dataS !== undefined) {
      if (dataS.name == "academicYear" && dataS.label === "From Month") {
        this.setState({ academicDurationFrom: item.value });
      } else if (dataS.name == "academicYear" && dataS.label === "To Month") {
        this.setState({ academicDurationTo: item.value });
      } else if (dataS.name == "academicYear" && dataS.label === "To Year") {
        this.setState({ academicSelectYear: item.value });
      }
    }

    //new
    if (dataS !== undefined) {
      if (dataS["name"] == "emailServer") {
        if (value !== this.state.selectedEmailServerOne) {
          this.setState({ configInfo: false, uploadConfig: false, emailConfigJson: emailConfigJsonOne }, () => {
            this.toasterClick(`Please upload ${dataS['defaultValue']} config.json file`, 'alert-red')
          })
        }
        else {
          this.setState({ configInfo: true, uploadConfig: true, emailConfigJson: emailConfigJsonOne })
        }
      }
    }

    //Date Format
    if (dataS !== undefined) {
      if (dataS["name"] === "dateFormat") {
        this.calcDateFormatExample(value)
        this.setState({ dateFormat: item.value });
      }
    }

    //Receipts
    if (dataS !== undefined) {
      if (dataS["name"] === "receipts") {
        this.setState({ receiptValue: item.value });
      }
    }

    if (dataS !== undefined) {
      if (dataS["name"] === "receiptsAmount") {
        this.setState({ partialAmount: item.value });
      }
    }

    //fees mapping
    if (dataS !== undefined) {
      if (dataS["name"] === "feesMapping") {
        this.setState({ feesMapping: item.value });
      }
    }

    //logos
    if (dataS !== undefined) {
      if (dataS['name'] === "logos") {
        this.setState({ logos: item.value })
      }
    }

    //content
    if (dataS !== undefined) {
      if (dataS["name"] === "receiptsContent") {
        this.setState({ content: item.value });
      }
    }

    //Portal Login
    if (dataS !== undefined) {
      if (dataS["name"] === "portalLogin") {
        this.setState({ portalLogin: item.value });
      }
    }

    //Logo Positions
    if (dataS !== undefined) {
      if (dataS["name"] === "logoPosition") {
        this.setState({ logoPosition: item.value })
      }
    }

    //Demand Note
    if (dataS !== undefined) {
      let withBank = [{
        "type": "select",
        "name": "selectBank",
        "class": "select-input-wrap setup-input-wrap settings-address-input demand-note-select",
        "label": "Select Bank",
        "options": [
          {
            "value": "0",
            "label": "SBI"
          }
        ],
        "placement": "bottom",
        "readOnly": false,
        "required": false,
        "requiredBoolean": true
      }]

      let demandNoteJSON = this.state.demandNoteFormData;

      if (dataS['name'] === "demandNoteContent") {
        this.setState({ loaderStatus: false, resText: "Loading..." }, () => {
          if (item['label'] === 'With Bank') {
            let index = demandNoteJSON.findIndex(item => item.name === "selectBank")
            if (index === -1) {
              demandNoteJSON = demandNoteJSON.concat(withBank)
            }
          }
          else if (item['label'] === 'Without Bank') {
            demandNoteJSON = demandNoteJSON.filter(item => item.name !== "selectBank")
            this.setState({ bank: "" })
          }
          this.setState({ demandNoteFormData: demandNoteJSON, loaderStatus: true, demandNoteContent: item.value })
        })
      }
    }

    if (dataS !== undefined) {
      if (dataS["name"] === "selectBank") {
        this.setState({ bank: item.value })
      }
    }

    // if (dataS !== undefined) {
    //   if (dataS["name"] === "receipts") {
    //     this.setState({ receiptValue: item.value });
    //   }
    // }

    // if (dataS !== undefined) {
    //   if (dataS["name"] == "emailServer") {
    //     this.setState({ configInfo: true }, () => {
    //       this.toasterClick(`Please upload ${dataS['defaultValue']} config.json file`, 'alert-red')
    //     })
    //   } else {
    //     this.setState({ configInfo: false })
    //   }
    // }

    // if (value === "gmail" || value === "yahoo" || value === "outlook" || value === "gmail") {
    //   this.setState({ configInfo: true })
    // } else {
    //   this.setState({ configInfo: false })
    // }

    // this.props.onInputChanges(value, item, event, dataS)
  }

  onInputChanges1 = (e) => {
    this.setState({ emailServerEmail1: e.target.value, emailServerError: false, emailServerHelperText: "" })
  }
  onInputChanges2 = (e) => {
    this.setState({ emailServerEmail2: e.target.value, emailServerError2: false, emailServerHelperText2: "" })
  }
  onInputChanges3 = (e) => {
    this.setState({ emailServerEmail3: e.target.value, emailServerError3: false, emailServerHelperText3: "" })
  }
  onInputChanges4 = (e) => {
    this.setState({ emailServerEmail4: e.target.value, emailServerError4: false, emailServerHelperText4: "" })
  }
  onInputChanges5 = (e) => {
    this.setState({ emailServerEmail5: e.target.value, emailServerError5: false, emailServerHelperText5: "" })
  }

  onEmailSelectChanges = (value, item, event) => {
    if (value) {
      this.setState({ emailSelectValue: value }, () => {
        if (this.state.emailSelectValue !== this.state.currentEmailServer) {
          if (value !== "none") this.toasterClick(`Please Enter ${item['label']} Server details`, 'alert-red')
          this.setState({
            emailServerEmail1: "", newEmailConfigJson1: "", newConfigFileName: "",
            emailServerEmail2: "", newEmailConfigJson2: "", newConfigFileName2: "",
            emailServerEmail3: "", newEmailConfigJson3: "", newConfigFileName3: "",
            emailServerEmail4: "", newEmailConfigJson4: "", newConfigFileName4: "",
            emailServerEmail5: "", newEmailConfigJson5: "", newConfigFileName5: "",
          })
        } else {
          this.getData('email-preview');
        }
      })
    }
  }

  submitForm = () => {
    // this.setState({ loader: true, loaderStatus: false });
    // if (this.state.emailConfigJson !== undefined) {
    console.log("tab", this.state.tab, this.state.instituteTab)
    let allRefs = {
      "instituteData": this.instituteRef.current,
      "smsGatewayData": this.smsGatewayRef.current,
      "paymentGatewayData": this.paymentGatewayRef.current,
      "emailServerData": this.emailServerRef.current,
      "headApproverData": this.headApproverRef.current,
    }
    let instituteForm = allRefs.instituteData.formRef.current;
    let smsGatewayFormOne = allRefs.smsGatewayData.formRef.current;
    let paymentGatewayFormOne = allRefs.paymentGatewayData.formRef.current;
    // let emailServerFormOne = allRefs.emailServerData.formRef.current;
    let headApproverFormOne = allRefs.headApproverData.formRef.current;
    // let textAreaInput = [this.textAreaInputRef.current]
    const allForms = [...instituteForm, ...smsGatewayFormOne, ...paymentGatewayFormOne, ...headApproverFormOne];
    // console.log(allForms)
    //checking null values
    let errorArray = [];
    let validateArray = []
    //store the mandatory fields
    let errorArrayOne = []
    let errorArrayTwo = []
    allForms.forEach(element => {
      if (!element.value) {
        errorArray.push(element.id)
      }
      if (element["id"] === "email" || element["id"] === "senderEmail" || element["id"] === "firstName" || element["id"] === "lastName" ||
        element["id"] === "phoneNumber" || element["id"] === "phoneNumber1" || element["id"] === "phoneNumber2" || element["id"] === "pinCode" || element["id"] === "gstin" ||
        element["id"] === "secretKey" || element["id"] === "emailconfig" || element["id"] === "pan" || element["id"] === "phoneNumber3") {
        validateArray.push(element["id"])
      }
    })
    //Address Form
    // Object.keys(AddressForm).forEach(item => {
    //   errorArray.forEach(element => {
    //     if (element === AddressForm[item]["name"]) {
    //       if (element === "firstName" || element === "lastName" || element === "stateName" || element === "email" || element === "phoneNumber1") {
    //         AddressForm[item]["error"] = true
    //         errorArrayTwo.push(element)
    //       }
    //     }
    //   })
    // })

    //Address Form -- Institute Name
    let apiCall = false;
    let emailId = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    // var phoneNum = /^(\+\d{1,3}[- ]?)?\d{10}$/;
    let phoneNum = new RegExp(/^[0123456789]\d{9,14}$/);
    var pincode = new RegExp("^[1-9][0-9]{5}$");
    let gstin = /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/;
    let pwd = /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{8,20})/;
    let alphabets = /^[a-zA-Z\s]+$/
    let pan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/

    //Validating email,phno,gstin,pincode for Institue Details
    if (this.state.tab === 0 && this.state.instituteTab === 0) {
      Object.keys(AddressForm).forEach(item => {
        validateArray.forEach(element => {
          if (element === AddressForm[item]["name"]) {
            if (element === "email") {
              if (AddressForm[item]["defaultValue"] !== "" && AddressForm[item]["defaultValue"] !== null && AddressForm[item]["defaultValue"] !== undefined) {
                if (!emailId.test(AddressForm[item]["defaultValue"])) {
                  AddressForm[item]["error"] = true
                  errorArrayOne.push(element)
                }
              }
            }
            if (AddressForm[item]["defaultValue"] !== "" && AddressForm[item]["defaultValue"] !== null && AddressForm[item]["defaultValue"] !== undefined) {
              if (element === "phoneNumber1" || element === "phoneNumber2") {
                if (!phoneNum.test(AddressForm[item]["defaultValue"])) {
                  AddressForm[item]["error"] = true
                  AddressForm[item]["errorMsg"] = "Invalid Phone Number"
                  errorArrayOne.push(element)
                }
              }
            }
            if (element === "pinCode") {
              if (AddressForm[item]["defaultValue"] !== "" && AddressForm[item]["defaultValue"] !== null && AddressForm[item]["defaultValue"] !== undefined) {
                if (!pincode.test(AddressForm[item]["defaultValue"])) {
                  console.log(AddressForm[item]["defaultValue"])
                  AddressForm[item]["error"] = true
                  AddressForm[item]["errorMsg"] = "Invalid Pincode"
                  errorArrayOne.push(element)
                }
              }
            }
            if (element === "gstin") {
              if (!gstin.test(AddressForm[item]["defaultValue"]) && AddressForm[item]["defaultValue"] !== "" && AddressForm[item]["defaultValue"] !== null && AddressForm[item]["defaultValue"] !== undefined) {
                AddressForm[item]["error"] = true
                AddressForm[item]["errorMsg"] = "Invalid GSTIN"
                errorArrayOne.push(element)
              }
            }
            if (element === "pan") {
              if (!pan.test(AddressForm[item]["defaultValue"]) && AddressForm[item]["defaultValue"] !== "" && AddressForm[item]["defaultValue"] !== null && AddressForm[item]["defaultValue"] !== undefined) {
                AddressForm[item]["error"] = true
                AddressForm[item]["errorMsg"] = "Invalid PAN"
                errorArrayOne.push(element)
              }
            }
            // if (element === "firstName") {
            //   if (AddressForm[item]["defaultValue"] !== "" && AddressForm[item]["defaultValue"] !== null) {
            //     if (!alphabets.test(AddressForm[item]["defaultValue"])) {
            //       AddressForm[item]["errorMsg"] = "The First Name should not contain numeric or special characters"
            //       AddressForm[item]["error"] = true
            //       errorArrayOne.push(element)
            //     }
            //   }
            // }
            // if (element === "lastName") {
            //   if (AddressForm[item]["defaultValue"] !== "" && AddressForm[item]["defaultValue"] !== null) {
            //     if (!alphabets.test(AddressForm[item]["defaultValue"])) {
            //       AddressForm[item]["error"] = true
            //       AddressForm[item]["errorMsg"] = "Invalid Last Name"
            //       errorArrayOne.push(element)
            //     }
            //   }
            // }
          }
        })
      })
      AddressForm.forEach(item => {
        if (item["requiredBoolean"]) {
          if (item["defaultValue"] === "" || item["defaultValue"] === null) {
            item["error"] = true
            apiCall = true
          }
        }
      })
    }

    //validating headApprover - Email & Ph.no
    if (this.state.tab === 0 && this.state.instituteTab === 1) {
      Object.keys(headApproverForm).forEach(item => {
        validateArray.forEach(element => {
          if (element === headApproverForm[item]["name"]) {
            if (element === "email") {
              if (headApproverForm[item]["defaultValue"] !== "" && headApproverForm[item]["defaultValue"] !== null && headApproverForm[item]["defaultValue"] !== undefined) {
                if (!emailId.test(headApproverForm[item]["defaultValue"])) {
                  headApproverForm[item]["error"] = true
                  headApproverForm[item]["errorMsg"] = "Invalid Email Address"
                  errorArrayOne.push(element)
                }
              }
            }
            if (element === "phoneNumber3") {
              if (headApproverForm[item]["defaultValue"] !== "" && headApproverForm[item]["defaultValue"] !== null && headApproverForm[item]["defaultValue"] !== undefined) {
                if (!phoneNum.test(headApproverForm[item]["defaultValue"])) {
                  headApproverForm[item]["error"] = true
                  headApproverForm[item]["errorMsg"] = "Invalid Phone Number"
                  console.log(headApproverForm[item])
                  errorArrayOne.push(element)
                }
              }
            }
          }
        })
      })
    }

    //VALIDATING DEMAND NOTE CONTENT AND BANK
    if (this.state.tab === 2 && this.state.receiptTab === 0) {
      if (!this.state.bank) {
        this.state.demandNoteFormData.map(item => {
          if (item.name === 'selectBank') {
            item["error"] = true
            item['errorMsg'] = "Please select the bank"
            console.log("from bank")
            apiCall = true
          }
        })
      }
    }

    //Email Form
    // Object.keys(emailServerForm).forEach(item => {
    //   errorArray.forEach(element => {
    //     if (element === emailServerForm[item]["name"]) {
    //       emailServerForm[item]["error"] = true
    //       emailServerForm[item]["errorMsg"] = "Invalid Email Address"
    //       errorArrayTwo.push(element)
    //     }
    //   })
    // })

    //validating email configuration
    // if (this.state.emailConfigJson === undefined) {
    //   errorArrayTwo.push("emailconfig")
    //   // Alert.error("Please check your Config JSON");
    //   let textarea = document.getElementById('emailconfig')
    //   console.log(textarea, textarea['defaultValue'])
    //   textarea['error'] = true
    // }

    //validating email for Email server
    if (this.state.tab === 4) {
      // Object.keys(emailServerForm).forEach(item => {
      //   validateArray.forEach(element => {
      //     if (element === emailServerForm[item]["name"]) {
      //       if (element === "senderEmail") {
      //         if (emailServerForm[item]["defaultValue"] !== "" && emailServerForm[item]["defaultValue"] !== null) {
      //           if (!emailId.test(emailServerForm[item]["defaultValue"])) {
      //             emailServerForm[item]["error"] = true
      //             errorArrayOne.push(element)
      //           }
      //         }
      //       }
      //     }
      //   })
      // })
      if (this.state.emailServerEmail1) {
        if (!emailId.test(this.state.emailServerEmail1)) {
          this.setState({ emailServerError: true, emailServerHelperText: "Invalid Email Address" })
          errorArrayOne.push("senderEmail")
        }
      }
      if (this.state.emailServerEmail2) {
        if (!emailId.test(this.state.emailServerEmail2)) {
          this.setState({ emailServerError2: true, emailServerHelperText2: "Invalid Email Address" })
          errorArrayOne.push("senderEmail2")
        }
      }
      if (this.state.emailServerEmail3) {
        if (!emailId.test(this.state.emailServerEmail3)) {
          this.setState({ emailServerError3: true, emailServerHelperText3: "Invalid Email Address" })
          errorArrayOne.push("senderEmail3")
        }
      }
      if (this.state.emailServerEmail4) {
        if (!emailId.test(this.state.emailServerEmail4)) {
          this.setState({ emailServerError4: true, emailServerHelperText4: "Invalid Email Address" })
          errorArrayOne.push("senderEmail4")
        }
      }
      if (this.state.emailServerEmail5) {
        if (!emailId.test(this.state.emailServerEmail5)) {
          this.setState({ emailServerError5: true, emailServerHelperText5: "Invalid Email Address" })
          errorArrayOne.push("senderEmail5")
        }
      }
      //Validating textarea
      // if (this.state.emailConfigJson === "") {
      //   errorArrayOne.push("emailConfig")
      // }
    }

    //Payment Gateway Form
    // Object.keys(paymentGatewayForm).forEach(item => {
    //   errorArray.forEach(element => {
    //     if (element === paymentGatewayForm[item]["name"]) {
    //       paymentGatewayForm[item]["error"] = true
    //       paymentGatewayForm[item]["errorMsg"] = `Invalid ${paymentGatewayForm[item]["label"]}`
    //       errorArrayTwo.push(element)
    //     }
    //   })
    // })
    //Validating Password for Payment Gateway
    // Object.keys(paymentGatewayForm).forEach(item => {
    //   validateArray.forEach(element => {
    //     if (element === paymentGatewayForm[item]["name"]) {
    //       if (element === "secretKey") {
    //         if (paymentGatewayForm[item]["defaultValue"] !== "" && paymentGatewayForm[item]["defaultValue"] !== undefined) {
    //           if (!pwd.test(paymentGatewayForm[item]["defaultValue"])) {
    //             paymentGatewayForm[item]["error"] = true
    //             errorArrayOne.push(element)
    //           }
    //         }
    //       }
    //     }
    //   })
    // })

    //SMS Gateway Form
    // Object.keys(smsGatewayForm).forEach(item => {
    //   errorArray.forEach(element => {
    //     if (element === smsGatewayForm[item]["name"]) {
    //       smsGatewayForm[item]["error"] = true
    //       errorArrayTwo.push(element)
    //     }
    //   })
    // })

    //Validating phno for SMS Gateway
    if (this.state.tab === 5) {
      Object.keys(smsGatewayForm).forEach(item => {
        validateArray.forEach(element => {
          if (element === smsGatewayForm[item]["name"]) {
            if (element === "phoneNumber") {
              if (smsGatewayForm[item]["defaultValue"] !== "" && smsGatewayForm[item]["defaultValue"] !== null && smsGatewayForm[item]["defaultValue"] !== undefined) {
                if (!phoneNum.test(smsGatewayForm[item]["defaultValue"])) {
                  smsGatewayForm[item]["error"] = true
                  smsGatewayForm[item]["errorMsg"] = "Invalid Phone Number"
                  errorArrayOne.push(element)
                }
              }
            }
          }
        })
      })
    }

    this.setState({
      addressFormData: AddressForm, emailServerFormData: emailServerForm, paymentGatewayData: paymentGatewayForm,
      smsGatewayData: smsGatewayForm, headApproverData: headApproverForm
    })
    let settingsPayLoad = {
      userName: String(this.state.userName),
      userEmail: String(this.state.userEmail),
      instituteDetails: {
        instituteName: instituteForm['instituteName'].value,
        gstin: instituteForm['gstin'].value,
        pan: instituteForm['pan'].value,
        address1: instituteForm['address1'].value,
        address2: instituteForm['address2'].value,
        address3: instituteForm['address3'].value,
        cityTown: instituteForm['cityTown'].value,
        stateName: this.state.institutestateName,
        stateCode: this.state.institutestateCode,
        pinCode: instituteForm['pinCode'].value,
        firstName: instituteForm['firstName'].value,
        lastName: instituteForm['lastName'].value,
        email: instituteForm['email'].value,
        phoneNumber1: instituteForm['phoneNumber1'].value,
        phoneNumber2: instituteForm['phoneNumber2'].value,
        // academicYear: `${this.state.academicDurationFrom}-${this.state.academicDurationTo}-${this.state.academicSelectYear}`,
        academicYear: null,
        dateFormat: `${this.state.dateFormat}`
      },
      headApprover:
        [{
          name: headApproverFormOne['name'].value,
          designation: headApproverFormOne['designation'].value,
          email: headApproverFormOne['email'].value,
          phoneNumber: headApproverFormOne['phoneNumber3'].value
        }],
      bankDetails: this.state.bankDetails,
      // [
      //   {
      //     bankName: "HDFC",
      //     bankAccountName: "Paul Martin",
      //     bankAccountNumber: "34343432323",
      //     bankIFSC: "HDFC0000293"
      //   },
      //   {
      //     bankName: "AXIS Bank",
      //     bankAccountName: "Malco",
      //     bankAccountNumber: "50192292283",
      //     bankIFSC: "AXIS0000293"
      //   }],
      logo: {
        logo: this.state.logoPicture !== undefined ? this.state.logoPicture : ''
      },
      favicon: {
        favicon: this.state.favPicture !== undefined ? this.state.favPicture : ''
      },
      logoPositions: {
        position: this.state.logoPosition
      },
      receipts: {
        send: this.state.receiptValue,
        partialAmount: this.state.partialAmount === "true" ? true : false,
        feesMapping: this.state.feesMapping,
        logos: this.state.logos,
        content: this.state.content,
        demandNoteContent: this.state.demandNoteContent,
        bank: this.state.bank
      },
      portalLogin: this.state.portalLogin,
      // label: [{ "regId": "REG ID", "classBatch": "CLASS/BATCH" }],
      // label: this.state.labelTableData,
      label: [{ "regId": this.state.labelTableData[0]["regId"], "classBatch": this.state.labelTableData[0]["classBatch"] }],
      // emailServer: {
      //   emailServer: this.state.emailSelectValue,
      //   emailAddress: this.state.emailServerEmail1,
      //   config: [this.state.newEmailConfigJson1],
      //   fileName: this.state.newConfigFileName
      // },
      emailServer: [
        {
          emailServer: this.state.emailSelectValue,
          emailAddress: this.state.emailServerEmail1,
          config: [this.state.newEmailConfigJson1],
          fileName: this.state.newConfigFileName
        },
        {
          emailServer: this.state.emailSelectValue,
          emailAddress: this.state.emailServerEmail2,
          config: [this.state.newEmailConfigJson2],
          fileName: this.state.newConfigFileName2
        },
        {
          emailServer: this.state.emailSelectValue,
          emailAddress: this.state.emailServerEmail3,
          config: [this.state.newEmailConfigJson3],
          fileName: this.state.newConfigFileName3
        },
        {
          emailServer: this.state.emailSelectValue,
          emailAddress: this.state.emailServerEmail4,
          config: [this.state.newEmailConfigJson4],
          fileName: this.state.newConfigFileName4
        },
        {
          emailServer: this.state.emailSelectValue,
          emailAddress: this.state.emailServerEmail5,
          config: [this.state.newEmailConfigJson5],
          fileName: this.state.newConfigFileName5
        }
        //   {
        //     emailServer: "gmail",
        //     emailAddress: "vijaykiran@gmail.com",
        //     config: [
        //       {
        //         "accessKeyId": "AKIAR6HU7QOXIVHWCXAL",
        //         "secretAccessKey": "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"
        //       }
        //     ],
        //     fileName: "config.json"
        //   },
        //   {
        //     emailServer: "gmail",
        //     emailAddress: "vijaykiran@gmail.com",
        //     config: [
        //       {
        //         "accessKeyId": "AKIAR6HU7QOXIVHWCXAL",
        //         "secretAccessKey": "6qXWD0mCYRhZdArZqZW0ke9KXue7d1EYYlzscSp12"
        //       }
        //     ],
        //     fileName: "config.json"
        //   },
        //   {
        //     emailServer: "gmail",
        //     emailAddress: "vijaykiran@gmail.com",
        //     config: [
        //       {
        //         "accessKeyId": "AKIAR6HU7QOXIVHWCXAL",
        //         "secretAccessKey": "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"
        //       }
        //     ],
        //     fileName: "config.json"
        //   },
        //   {
        //     emailServer: "gmail",
        //     emailAddress: "vijaykiran@gmail.com",
        //     config: [
        //       {
        //         "accessKeyId": "AKIAR6HU7QOXIVHWCXAL",
        //         "secretAccessKey": "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"
        //       }
        //     ],
        //     fileName: "config.json"
        //   },
        //   {
        //     emailServer: "gmail",
        //     emailAddress: "vijaykiran@gmail.com",
        //     config: [
        //       {
        //         "accessKeyId": "AKIAR6HU7QOXIVHWCXAL",
        //         "secretAccessKey": "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"
        //       }
        //     ],
        //     fileName: "config.json"
        //   }
      ],
      smsGateway: {
        smsGateway: this.state.selectedSMSGateway,
        phoneNumber: smsGatewayFormOne['phoneNumber'].value,
        senderName: smsGatewayFormOne['senderName'].value,
        apiKey: smsGatewayFormOne['apiKey'].value
      },
      paymentGateway: {
        paymentGateway: this.state.selectedPaymentGateway,
        accessKey: paymentGatewayFormOne['accessKey'].value,
        secretKey: paymentGatewayFormOne['secretKey'].value,
      }
    }

    if (errorArrayOne.length > 0 || apiCall) {
      console.log(errorArrayOne)
      if (errorArrayOne.includes("emailConfig")) {
        this.toasterClick("Please upload config.json file", 'alert-red')
      } else {
        this.toasterClick("Please check your Entered data", 'alert-red')
      }
    } else {
      console.log(settingsPayLoad);
      this.setState({ loader: true })
      axios.put(`${this.state.env['zqBaseUri']}/setup/settings/${this.state.instituteId}`,
        settingsPayLoad, {
        headers: {
          'Authorization': localStorage.getItem("auth_token")
        }
      })
        .then(res => {
          console.log("settings get api response", res);
          this.setState({ loader: false }, () => {
            this.toasterClick('Settings Data updated Successfully', 'alert-green')
          });
          this.getData('submit');
          this.context.updateDateFormat(this.state.dateFormat)
          this.context.updateOrgName(instituteForm['instituteName'].value)
          this.context.updateProfilePicture(this.state.logoPicture)
          this.context.updateReportLabel(this.state.labelTableData[0]["regId"])
          this.context.updateClassLabel(this.state.labelTableData[0]["classBatch"])
          this.context.updateLogoPositions(this.state.logoPosition)
          this.updateFavicon();
          // this.context.updateProfilefirstName(instituteForm['firstName'].value)
          // this.context.updateProfileLastName(instituteForm['lastName'].value)
        }).catch(err => {
          console.log(err)
          this.setState({ loader: false }, () => {
            this.toasterClick(`No Data updated`, 'alert-red')
          });
        })
    }
    // } 
    // else {
    //   Alert.error("Please upload config.json file");
    // }
  }
  removeExtra = (data) => {
    if (data !== null) {
      data = data.split("?");
      return data[0]
    }
  }
  cancelForm = () => {
    this.getData('submit');
    // this.setState({ loaderStatus: false }, () => {
    //   AddressForm.map(item => {
    //     item["defaultValue"] = ""
    //   })
    //   console.log("addressForm", AddressForm)
    //   this.setState({ addressFormData: AddressForm })
    // })

    // setTimeout(() => {
    //   this.setState({ loaderStatus: true })
    // }, 1000);
  }
  // cleanData = (value, b, c, d) => {
  //   this.setState({ institutestateCode: "" })
  // }
  resetForm = () => {
    console.log('reset')
    AddressForm.map(item => {
      item['defaultValue'] = ''
    })
    emailServerForm.map(item => {
      item['defaultValue'] = ''
    })
    smsGatewayForm.map(item => {
      item['defaultValue'] = ''
    })
    paymentGatewayForm.map(item => {
      item['defaultValue'] = ''
    })
    // allForms.map(item => {
    //   item['defaultValue'] = ''
    // })
  }
  onError = () => {
    this.setState({ logoPicture2: profilePicture })
  }
  onErrorFav = () => {
    this.setState({ faviconImagePic: 'https://uploads-ssl.webflow.com/5ee891a3891ce1f991bcd2d6/5ef71159da6e9d49c1e2748c_favicon-32x32.png' })
  }
  setStartDate = (date, e) => {
    this.setState({ startDate: date })
  }
  setToDate = (date) => {
    this.setState({ toDate: date })
  }
  updateFavicon = () => {
    let namespace = localStorage.getItem("baseURL") ? localStorage.getItem("baseURL") : "default"
    var link = document.querySelector("link[rel~='icon']");
    if (link) {
      link.href = this.state.favPicture
      localStorage.setItem(`${namespace}-link`, this.state.favPicture)
    }
  }
  handleDrop = (files) => {
    this.setState({ defaultImage: false })
    if (files && files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => {
        this.setState({ src: reader.result }) // bae64 format of full image
      });
      reader.readAsDataURL(files[0]);
    }
  }
  handleDropFav = (files) => {
    this.setState({ faviconImage: false })
    if (files && files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => {
        this.setState({ favSrc: reader.result }) // bae64 format of full image
      });
      reader.readAsDataURL(files[0]);
    }
  }

  onPreviewScholarship = (item) => { }
  onPreviewStudentList1 = (item) => { }

  handleLabelTable = (value, label) => {
    let { labelTableData } = this.state;
    labelTableData[0][label] = value
    this.setState({ labelTableData })
  }
  render() {

    // console.log('confis', JSON.stringify(this.state.emailConfigJson))

    const { crop, favCrop, croppedImageUrl, src, favSrc, labelTableData } = this.state;
    // console.log("labe", labelTableData)

    const onButtonClick = () => {
      this.inputFile.current.click();
    };

    const value = 66;
    const borderStyle = {
      border: "1px dotted #1359c1",
      borderRadius: "10px",
    };
    const styleImageLogo = {
      height: "343px",
      width: "643px",
      marginTop: "6px",
      borderRadius: "10px"
    };
    const label = (
      <div className="setting-logo-card">
        <div style={{ position: "relative" }}>
          {this.state.logoPicture2 == "" ?

            <React.Fragment>
              {/* {this.state.loader ? <CircularProgressbar value={value} maxValue={1} text={`${value * 100}%`} /> : null} */}

              <p className="onboard-card-header logo-img-letter" onClick={this.changeLogo}>{this.state.companyLogo}</p>

              <p className="onboard-card-title">
                <span className="browse-content logo-change">Change Logo </span>
              </p>
            </React.Fragment> :
            <React.Fragment>

              <img className="onboard-card-header logo-img-letter logo-image" id="settings-logo" src={`${this.state.logoPicture2}`} />
              <p className="onboard-card-title">
                <span className="browse-content logo-change image-text">{this.state.logoText} </span>
              </p>
            </React.Fragment>}
        </div>
      </div>
    );

    return (
      <React.Fragment>
        {this.state.loader ? <Loader /> : null}

        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          open={this.state.toasterOpen}
          autoHideDuration={1500}
          onClose={this.toasterClose}
          message={`${this.state.message}`}
          className={`info-snackbar ${this.state.toasterMessage}`}
        />

        {/* <DateFormatter date={new Date()} format={"DD-MM-YYYY"} /> */}

        <div className="settings-wrap">
          <div className="trial-balance-header-title">
            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 10, cursor: 'pointer' }} />
            <p className="top-header-title">| Settings</p>
          </div>
          {/* {this.state.isLogoTab ? <div className="settings-form-submit">
            <Button type="submit" class="primary-btn setting-btn floatRight" onClick={() => this.submitForm()}>SUBMIT</Button>
            <Button type="submit" class="secondary-btn  setting-btn floatRight" style={{ marginRight: 10 }} onClick={this.cancelForm}>CANCEL</Button>
          </div> : null} */}
          <div className="settings-form-submit">
            <Button type="submit" class=" setting-btn setting-btn-submit floatRight" onClick={() => this.submitForm()} style={{ background: "#0052CC" }}>Submit</Button>
            <Button type="submit" class="secondary-btn  setting-btn floatRight" onClick={this.cancelForm} style={{ border: "1px solid #0052CC", color: "#0052CC", padding: "8px 12px", marginRight: "10px" }}>Cancel</Button>
          </div>
          <div className="invoice-page-wrapper new-table-wrap setting-content">
            {this.state.containerNav == undefined ? null : (
              <ContainerNavbar containerNav={this.state.containerNav} />
            )}
          </div>
        </div>
        <div className="settings-tab-wrap">
          <div className="settings-content">
            <Tabs
              value={this.state.tab}
              indicatorColor="primary"
              style={{ border: "none", borderRight: "1px solid #dfe1e6" }}
              textColor="primary"
              onChange={this.selectTabs}
              className="vertical-tab-cont settings-tab-content"
              orientation="vertical" >
              <Tab label="Institute Details" className="tab-list"></Tab>
              <Tab label="Templates" className="tab-list"></Tab>
              <Tab label="Fees" className="tab-list"></Tab>
              <Tab label="Labels" className="tab-list"></Tab>
              <Tab label="Email Server" className="tab-list"></Tab>
              <Tab label="SMS Gateway" className="tab-list"></Tab>
              <Tab label="Payment Gateway" className="tab-list"></Tab>
            </Tabs>
          </div>
          <div className="tab-details">

            {/* <div className={this.state.tab === 0 ? "tab-cont op-1" : "tab-cont op-0"}>
              <div className="tab-one-content-key  full-setting-body">
                <div className="tab-one-content-header">
                  <p>Institute details</p>
                </div>
                <div
                  className="history-icon-box"
                  title="View History"
                  onClick={this.openHistory}
                  style={{ width: '100px', float: 'right', position: "absolute", top: "19px", right: "16px" }}
                >
                  <RestoreOutlinedIcon className="material-historyIcon" />
                  <p>History</p>
                </div>
                <div className="address-form-wrapper ">
                  {this.state.loaderStatus == true ?
                    <ZenForm ref={this.instituteRef} inputData={this.state.addressFormData} onInputChanges={this.onInputChanges} onSubmit={this.onSubmit} />
                    : <p className="noprog-txt">{this.state.resText}</p>}
                </div>
              </div>
            </div> */}

            <div className={this.state.tab === 0 ? "tab-cont op-1 logo-cont" : "tab-cont op-0"}>
              <div className="tab-one-content-key" style={{ width: "19%", padding: "0px" }}>
                <Tabs
                  value={this.state.instituteTab}
                  indicatorColor="primary"
                  textColor="primary"
                  style={{ border: "none" }}
                  onChange={this.selectInstituteTabs}
                  className="vertical-tab-cont-one vertical-tab-cont settings-tab-content"
                  orientation="vertical" >
                  <Tab label="Name & Address" className="tab-list"></Tab>
                  <Tab label="Head Approver" className="tab-list"></Tab>
                  <Tab label="Bank Accounts" className="tab-list"></Tab>
                  <Tab label="Logo" className="tab-list"></Tab>
                  {/* <Tab label="Academic Duration" className="tab-list"></Tab> */}
                  <Tab label="Logo Positions" className="tab-list"></Tab>
                  <Tab label="Portal Login" className="tab-list"></Tab>
                  <Tab label="Favicon" className="tab-list"></Tab>
                  <Tab label="Date format" className="tab-list"></Tab>
                </Tabs>
              </div>
              <div className="tab-one-content-value" style={{ width: '81%' }}>
                <div className="tab-one-content-value-content">
                  <div className={`${this.state.instituteTab === 0 ? "tab-cont op-1" : "tab-cont op-0"} setting-institute-scroll`} style={{ width: "81%" }}>
                    <div className="tab-one-content-value-header">
                      <div className="content-value-header-name">
                        <p className="value-header-name">Institute Details - Name & Address</p>
                        {/* <span className="logo-version">v{this.state.instituteAddressVersion}</span> */}
                      </div>
                      {/* <div
                        className="history-icon-box"
                        title="View History"
                        onClick={() => this.openHistory("instituteDetails")}
                      >
                        <RestoreOutlinedIcon className="material-historyIcon" />
                        <p>History</p>
                      </div> */}
                    </div>
                    <div className="address-form-wrapper ">
                      {this.state.loaderStatus == true ?
                        <ZenForm ref={this.instituteRef} inputData={this.state.addressFormData} onInputChanges={this.onInputChanges} onSubmit={this.onSubmit} />
                        : <p className="noprog-txt">{this.state.resText}</p>
                      }
                    </div>
                  </div>
                  {/* HEAD APPROVER */}
                  <div className={`${this.state.instituteTab === 1 ? "tab-cont op-1" : "tab-cont op-0"}`} style={{ width: "81%" }}>
                    <div className="tab-one-content-value-header">
                      <div className="content-value-header-name">
                        <p className="value-header-name">Head Approver</p>
                      </div>
                    </div>
                    <div className="address-form-wrapper settings-head-approver">
                      {this.state.loaderStatus == true ?
                        <ZenForm ref={this.headApproverRef} inputData={this.state.headApproverData} onInputChanges={this.onInputChanges} onSubmit={this.onSubmit} />
                        : <p className="noprog-txt">{this.state.resText}</p>
                      }
                    </div>
                  </div>
                  <div className={`${this.state.instituteTab === 2 ? "tab-cont op-1" : "tab-cont op-0"}`} style={{ width: "81%" }}>
                    <div className="tab-one-content-value-header">
                      <div className="content-value-header-name">
                        <p className="value-header-name">Institute Details - Bank Accounts</p>
                      </div>
                    </div>
                    <div className="bankinstitutedetails" style={{ padding: "20px" }}>
                      {this.state.bankTableData.length > 0 ? <React.Fragment>
                        <div className="remove-last-child-table remove-first-child-table table-head-padding">
                          <ZqTable
                            allSelect={this.allSelect}
                            data={this.state.bankTableData}
                            rowClick={(item) => { this.onPreviewScholarship(item) }}
                            onRowCheckBox={(item) => { this.onPreviewStudentList1(item) }}
                          />
                        </div>
                      </React.Fragment> : <><p className="noprog-txt" style={{ paddingLeft: '0px', }}>No Data</p></>}
                    </div>

                  </div>
                  <div className={this.state.instituteTab === 3 ? "tab-cont op-1" : "tab-cont op-0"} style={{ width: "81%" }}>
                    <React.Fragment>
                      <div className="tab-one-content-value-header">
                        <div className="content-value-header-name">
                          <p className="value-header-name">Institute Details - Logo</p>
                        </div>
                      </div>

                      <div className="tab-one-content-value-body parent-react-crop">
                        <DragAndDrop handleDrop={this.handleDrop} >
                          <div className="avatar-wrap avatar-wrap-insititute-logo">
                            {this.state.defaultImage == true ?
                              <React.Fragment>
                                <div className="parent-react-crop2">
                                </div>
                                <div className="image-div">
                                  {this.state.showLogo == true ?
                                    <CircularProgress style={{ width: '100%', height: '20px', width: '20px', float: 'right', marginRight: '10px' }} />
                                    :
                                    <div className="logo-image-tag-parent" >
                                      <img className="logo-image-tag" alt="No Logo" src={`${this.state.logoPicture2}`} onError={this.onError} />
                                      <p className="dragDropText">Drag and Drop</p>
                                      <p className="dragOrText">or</p>
                                      <p className="browse-btn">Browse</p>
                                      <input id="files" className="select-image-input" type="file" accept="image/*" onChange={this.onSelectFile} style={{ width: "91px" }} />
                                    </div>
                                  }
                                </div>
                              </React.Fragment>
                              : <React.Fragment>
                                {src && (
                                  <ReactCrop
                                    style={styleImageLogo}
                                    src={src}
                                    crop={crop}
                                    ruleOfThirds
                                    onImageLoaded={this.onImageLoaded}
                                    onComplete={this.onCropComplete}
                                    onChange={this.onCropChange}
                                    ref={this.logoRef}
                                  />
                                )}
                              </React.Fragment>}



                          </div>
                        </DragAndDrop>
                      </div>

                    </React.Fragment>
                  </div>
                  {/* LOGO POSITIONS */}
                  <div className={this.state.instituteTab === 4 ? "tab-cont op-1" : "tab-cont op-0"} style={{ width: "81%" }}>
                    <div className="tab-one-content-value-header">
                      <div className="content-value-header-name">
                        <p className="value-header-name">Institute Details - Logo Positions</p>
                      </div>
                    </div>
                    <div className="address-form-wrapper">
                      {this.state.loaderStatus == true ?
                        <ZenForm ref={this.logoPositionsRef} inputData={this.state.logoPositionsFormData} onInputChanges={this.onInputChanges} />
                        : <p className="noprog-txt">{this.state.resText}</p>
                      }
                    </div>
                  </div>
                  {/* PORTAL LOGIN */}
                  <div className={this.state.instituteTab === 5 ? "tab-cont op-1" : "tab-cont op-0"} style={{ width: "81%" }}>
                    <div className="tab-one-content-value-header">
                      <div className="content-value-header-name">
                        <p className="value-header-name">Institute Details - Portal Login</p>
                      </div>
                    </div>
                    <div className="address-form-wrapper input-date-example">
                      {this.state.loaderStatus == true ?
                        <ZenForm ref={this.portalLoginRef} inputData={this.state.portalLoginFormData} onInputChanges={this.onInputChanges} />
                        : <p className="noprog-txt">{this.state.resText}</p>
                      }
                    </div>
                  </div>
                  {/* FAVICON */}
                  <div className={this.state.instituteTab === 6 ? "tab-cont op-1" : "tab-cont op-0"} style={{ width: "81%" }}>
                    <div className="tab-one-content-value-header">
                      <div className="content-value-header-name">
                        <p className="value-header-name">Institute Details - Favicon</p>
                      </div>
                    </div>
                    <div className="tab-one-content-value-body parent-react-crop">
                      <DragAndDrop handleDrop={this.handleDropFav} >
                        <div className="avatar-wrap avatar-wrap-insititute-favicon">
                          {this.state.faviconImage == true ?
                            <React.Fragment>
                              <div className="parent-react-crop2">
                              </div>
                              <div className="image-div">
                                {this.state.showLogo == true ?
                                  <CircularProgress style={{ width: '100%', height: '20px', width: '20px', float: 'right', marginRight: '10px' }} />
                                  :
                                  <div className="logo-image-tag-parent" >
                                    <img className="logo-image-tag-fav" alt="No Logo" src={`${this.state.faviconImagePic}`} onError={this.onErrorFav} />
                                    <p className="dragDropText">Drag and Drop</p>
                                    <p className="dragOrTextFav">or</p>
                                    <label className="browse-btn-fav" for="fav-files">Browse</label>
                                    <input id="fav-files" className="select-image-input" type="file" accept="image/*" onChange={this.onSelectFavicon} style={{ width: "91px" }} />
                                  </div>
                                }
                              </div>
                            </React.Fragment>
                            : <React.Fragment>
                              {favSrc && (
                                <ReactCrop
                                  style={styleImageLogo}
                                  src={favSrc}
                                  crop={favCrop}
                                  ruleOfThirds
                                  onImageLoaded={this.onImageLoadedFav}
                                  onComplete={this.onCropCompleteFav}
                                  onChange={this.onCropChangeFav}
                                  ref={this.faviconRef}
                                />
                              )}
                            </React.Fragment>}
                        </div>
                      </DragAndDrop>
                    </div>
                  </div>
                  {/* // */}
                  {/* DATE FORMAT */}
                  <div className={this.state.instituteTab === 7 ? "tab-cont op-1" : "tab-cont op-0"} style={{ width: "81%" }}>
                    <div className="tab-one-content-value-header">
                      <div className="content-value-header-name">
                        <p className="value-header-name">Institute Details - Date format</p>
                      </div>
                    </div>
                    <div className="address-form-wrapper input-date-example">
                      {this.state.loaderStatus == true ?
                        <ZenForm ref={this.dateFormatRef} inputData={this.state.dateFormatFormData} onInputChanges={this.onInputChanges} />
                        : <p className="noprog-txt">{this.state.resText}</p>
                      }
                    </div>
                  </div>
                  {/* // */}
                </div>
              </div>
              {/* <div className="tab-one-content-value">
                <div className="tab-one-content-value-content">
                  {this.state.instituteSwitch ? <>
                    <div className="tab-one-content-value-header">
                      <div className="content-value-header-name">
                        <p className="value-header-name">Name & Address</p>
                        <span className="logo-version">v{this.state.logoPictureVersion}</span>
                      </div>
                      <div
                        className="history-icon-box"
                        title="View History"
                        onClick={this.openHistory}
                      >
                        <RestoreOutlinedIcon className="material-historyIcon" />
                        <p>History</p>
                      </div>
                    </div>
                    <div className="address-form-wrapper ">
                      {this.state.loaderStatus == true ?
                        <ZenForm ref={this.instituteRef} inputData={this.state.addressFormData} onInputChanges={this.onInputChanges} onSubmit={this.onSubmit} />
                        : <p className="noprog-txt">{this.state.resText}</p>}
                    </div>
                  </> :
                    <React.Fragment>
                      <div className="tab-one-content-value-header">
                        <div className="content-value-header-name">
                          <p className="value-header-name">Logo</p>
                          <span className="logo-version">v{this.state.logoPictureVersion}</span>
                        </div>
                        <div
                          className="history-icon-box"
                          title="View History"
                          onClick={this.openHistory}
                        >
                          <RestoreOutlinedIcon className="material-historyIcon" />
                          <p>History</p>
                        </div>
                      </div>

                      <div className="tab-one-content-value-body parent-react-crop">
                        <div className="avatar-wrap">
                          {this.state.defaultImage == true ?
                            <React.Fragment>
                              <div className="parent-react-crop2">
                                <label className="input-label-text" for="files" >Change Logo</label>
                                <input id="files" className="select-image-input" type="file" accept="image/*" onChange={this.onSelectFile} />
                              </div>
                              <div className="image-div">
                                {this.state.loader == true ?
                                  <CircularProgress style={{ width: '100%', height: '20px', width: '20px', float: 'right', marginRight: '10px' }} />
                                  :
                                  <div>
                                    <img className="logo-image-tag" alt="Loading..." src={this.state.logoPicture2} />
                                    <p className="browse-btn">Browse</p>
                                  </div>
                                }
                              </div>
                            </React.Fragment>
                            : <React.Fragment>
                              {src && (
                                <ReactCrop
                                  style={styleImageLogo}
                                  src={src}
                                  crop={crop}
                                  ruleOfThirds
                                  onImageLoaded={this.onImageLoaded}
                                  onComplete={this.onCropComplete}
                                  onChange={this.onCropChange}
                                />
                              )}
                            </React.Fragment>}



                        </div>
                      </div>

                    </React.Fragment>
                  }
                </div>
              </div> */}
            </div>
            <div className={this.state.tab === 1 ? "tab-cont op-1 template-cont " : "tab-cont op-0"}>
              <div className="tab-one-content-key settings-numbering" >
                <div className="tab-one-content-header">
                  <p>Parameter</p>
                  {/* <p>Value</p> */}
                </div>
                {this.state.getNumArray.map((item, i) => {
                  return (
                    <div
                      className="versions"
                      onClick={(e) => this.changeNumbering(e, item, i)}
                    >
                      <div className={`tab-one-content-key-list ${item.name == this.state.previewNumberName ? 'active' : ''}`}>
                        <p>{item.name}</p>
                        {/* <p className="choose-format" >
                          {item.value}
                          <ArrowForwardIosIcon className="setting-right-icon angle-icon-space" />
                        </p> */}
                      </div>
                    </div>
                  );
                })}
              </div>
              <div className="tab-one-content-value">
                <div className="tab-one-content-value-header">
                  <div className="content-value-header-name">
                    {this.state.defaultnumberFormat == true ?
                      <React.Fragment>
                        <p className="value-header-name">Demand Note Template</p>
                        {/* <span className="logo-version">v0</span> */}
                      </React.Fragment> :
                      <React.Fragment>
                        <p className="value-header-name">{this.state.previewNumberName}</p>
                        {/* <span className="logo-version">{this.state.previewVersion}</span> */}
                      </React.Fragment>}
                  </div>
                  {/* <div
                    className="history-icon-box"
                    title="View History"
                    onClick={this.openHistory}
                  >
                    <RestoreOutlinedIcon className="material-historyIcon" />
                    <p>History</p>
                  </div> */}
                </div>
                {this.state.template === "Demand Note Template" &&
                  // <>{this.state.isdemandNoteTemplate ?
                  <div className="tab-one-content-value-body" style={{ overflow: 'auto' }}>
                    {/* <iframe style={{
                      width: '100%',
                      height: '100%',
                      overflow: 'auto'
                    }} src={} ></iframe> */}
                    <DemandNoteTemplate />
                  </div>
                  // : <p className="noprog-txt">No Template</p>}</>
                }
                {this.state.template === "Receipt Template" &&
                  <div className="tab-one-content-value-body" style={{ overflow: 'auto' }}>
                    {/* <iframe style={{
                      width: '100%',
                      height: '100%',
                      overflow: 'auto',
                    }} src={`https://receipthosting.s3.ap-south-1.amazonaws.com/receipt.html?id=${this.state.instituteId}`}></iframe> */}
                    <ReceiptTemplate />
                  </div>
                  // : <p className="noprog-txt">No Template</p>}</>
                }
                {this.state.template === "Statement Template" &&
                  // <>{this.state.isdemandNoteTemplate ?
                  <div className="tab-one-content-value-body" style={{ overflow: "auto" }}>
                    {/* <iframe style={{
                      width: '100%',
                      height: '100%',
                      overflow: 'auto'
                    }} src={`https://statementhosting.s3.ap-south-1.amazonaws.com/statement.html?id=${this.state.instituteId}`}></iframe> */}
                    <StatementTemplate />
                  </div>
                  // : <p className="noprog-txt">No Template</p>}</>
                }
                {this.state.template === "Refund Template" &&
                  // <>{this.state.isdemandNoteTemplate ?
                  <div className="tab-one-content-value-body">
                    {/* <iframe style={{
                      width: '100%',
                      height: '100%',
                    }} src={`https://refundhosting.s3.ap-south-1.amazonaws.com/refund.html?id=${this.state.instituteId}`}></iframe> */}
                    <RefundTemplate />
                  </div>
                  // : <p className="noprog-txt">No Template</p>}</>
                }
                {/* <div className="tab-one-content-value-body">
                  {this.state.numberingData == 0 || this.state.numberingData > 0 ? (
                    <>
                      <p className="digits">No. of Digits</p>
                      <div className="select-values">
                        <SelectPicker
                          className="select-input-wrap"
                          searchable={false}
                          data={this.state.dataDN}
                          style={{ width: 120 }}
                          defaultValue={this.state.defNumber}
                          onSelect={(value, item, event) =>
                            this.selectValueInv(value, item, event)
                          }
                        />
                        <SelectPicker
                          className="select-input-wrap"
                          searchable={false}
                          data={this.state.dataSlash}
                          style={{ width: 70 }}
                          defaultValue="/"
                          onSelect={(value, item, event) =>
                            this.selectValueSlash(value, item, event)
                          }
                        />
                        <SelectPicker
                          className="select-input-wrap"
                          searchable={false}
                          data={this.state.dataYear}
                          style={{ width: 150 }}
                          defaultValue="YYYY-YY"
                          onSelect={(value, item, event) =>
                            this.selectValueYear(value, item, event)
                          }
                        />
                        <SelectPicker
                          className="select-input-wrap"
                          searchable={false}
                          data={this.state.dataSlash}
                          style={{ width: 70 }}
                          defaultValue="/"
                          onSelect={(value, item, event) =>
                            this.selectValueSlash2(value, item, event)
                          }
                        />
                        <SelectPicker
                          className="select-input-wrap"
                          searchable={false}
                          data={this.state.dataDgts}
                          style={{ width: 70 }}
                          defaultValue="3"
                          onSelect={(value, item, event) =>
                            this.selectValueDigits(value, item, event)
                          }
                        />
                      </div>{" "}
                      <p>Example: {this.state.defNumber}/2020-21/001</p>
                    </>
                  ) : " "}
                </div>
            */}
              </div>
            </div>
            {/* General */}
            {/* <div className={this.state.tab === 2 ? "tab-cont op-1 logo-cont" : "tab-cont op-0"}>
              <div className="tab-one-content-key" style={{ width: "20%", padding: "0px" }}>
                <Tabs
                  value={this.state.generalTab}
                  indicatorColor="primary"
                  textColor="primary"
                  style={{ border: "none" }}
                  onChange={this.selectGeneralTabs}
                  className="vertical-tab-cont-one vertical-tab-cont settings-tab-content"
                  orientation="vertical" >
                  <Tab label="Date format" className="tab-list"></Tab>
                  <Tab label="Currency" className="tab-list"></Tab>
                  <Tab label="Financial year" className="tab-list"></Tab>
                </Tabs>
              </div>
              <div className="tab-one-content-value" style={{ width: '80%' }}>
                <div className="tab-one-content-value-content">
                  <div className={this.state.generalTab === 0 ? "tab-cont op-1" : "tab-cont op-0"} style={{ width: "80%" }}>
                    <div className="tab-one-content-value-header">
                      <div className="content-value-header-name">
                        <p className="value-header-name">Date format</p>
                      </div>
                      <div className="address-form-wrapper ">

                      </div>
                    </div>
                  </div>
                  <div className={this.state.generalTab === 1 ? "tab-cont op-1" : "tab-cont op-0"} style={{ width: "80%" }}>
                    <div className="tab-one-content-value-header">
                      <div className="content-value-header-name">
                        <p className="value-header-name">Currency</p>
                      </div>
                      <div className="address-form-wrapper ">

                      </div>
                    </div>
                  </div>
                  <div className={this.state.generalTab === 2 ? "tab-cont op-1" : "tab-cont op-0"} style={{ width: "80%" }}>
                    <div className="tab-one-content-value-header">
                      <div className="content-value-header-name">
                        <p className="value-header-name">Financial year</p>
                      </div>
                      <div className="address-form-wrapper ">

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> */}
            {/* Receipts */}
            <div className={this.state.tab === 2 ? "tab-cont op-1 logo-cont" : "tab-cont op-0"}>
              <div className="tab-one-content-key" style={{ width: "19%", padding: "0px" }}>
                <Tabs
                  value={this.state.receiptTab}
                  indicatorColor="primary"
                  textColor="primary"
                  style={{ border: "none" }}
                  onChange={this.selectReceiptTabs}
                  className="vertical-tab-cont-one vertical-tab-cont settings-tab-content"
                  orientation="vertical" >
                  <Tab label="Demand Note" className="tab-list"></Tab>
                  <Tab label="Fees Mapping" className="tab-list"></Tab>
                  <Tab label="Send Receipt" className="tab-list"></Tab>
                  <Tab label="Partial Amount" className="tab-list"></Tab>
                  <Tab label="Logos" className="tab-list"></Tab>
                </Tabs>
              </div>
              <div className="tab-one-content-value receipt-tab" style={{ width: '81%' }}>
                <div className="tab-one-content-value-content">
                  {/* DEMAND NOTE */}
                  <div className={`${this.state.receiptTab === 0 ? "tab-cont op-1" : "tab-cont op-0"}`} style={{ width: "81%" }}>
                    <div className="tab-one-content-value-header">
                      <div className="content-value-header-name">
                        <p className="value-header-name">Select</p>
                      </div>
                    </div>
                    <div className="address-form-wrapper ">
                      <div className="tab-one-content-key full-setting-body settings-receipt" style={{ overflow: "auto" }}>
                        <div className="address-form-wrapper seperateData">
                          {this.state.loaderStatus == true ?
                            <ZenForm ref={this.demandNoteRef} inputData={this.state.demandNoteFormData} onInputChanges={this.onInputChanges} />
                            : <p className="noprog-txt">{this.state.resText}</p>
                          }
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* //////////// */}
                  {/* FEES MAPPING */}
                  <div className={`${this.state.receiptTab === 1 ? "tab-cont op-1" : "tab-cont op-0"}`} style={{ width: "81%" }}>
                    <div className="tab-one-content-value-header">
                      <div className="content-value-header-name">
                        <p className="value-header-name">Select</p>
                      </div>
                    </div>
                    <div className="address-form-wrapper ">
                      <div className="tab-one-content-key full-setting-body settings-receipt" style={{ overflow: "auto" }}>
                        <div className="address-form-wrapper seperateData">
                          {this.state.loaderStatus == true ?
                            <ZenForm ref={this.feesMappingRef} inputData={this.state.feesMappingFormData} onInputChanges={this.onInputChanges} />
                            : <p className="noprog-txt">{this.state.resText}</p>
                          }
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* SEND RECEIPT */}
                  <div className={`${this.state.receiptTab === 2 ? "tab-cont op-1 logo-cont" : "tab-cont op-0"}`} style={{ width: "81%" }}>
                    <div className="tab-one-content-key" style={{ width: "19%", padding: "0px" }}>
                      <Tabs
                        value={this.state.sendReceiptTab}
                        indicatorColor="primary"
                        textColor="primary"
                        style={{ border: "none" }}
                        onChange={this.selectSendReceiptTab}
                        className="vertical-tab-cont-one vertical-tab-cont settings-tab-content"
                        orientation="vertical" >
                        <Tab label="Timing" className="tab-list"></Tab>
                        <Tab label="Content" className="tab-list"></Tab>
                      </Tabs>
                    </div>
                    <div className="tab-one-content-value receipt-tab" style={{ width: '81%' }}>
                      <div className="tab-one-content-value-content">
                        {/* TIMING */}
                        <div className={`${this.state.sendReceiptTab === 0 ? "tab-cont op-1" : "tab-cont op-0"}`} style={{ width: "81%" }}>
                          <div className="tab-one-content-value-header">
                            <div className="content-value-header-name">
                              <p className="value-header-name">Select</p>
                            </div>
                          </div>
                          <div className="address-form-wrapper ">
                            <div className="tab-one-content-key full-setting-body settings-receipt" style={{ overflow: "auto" }}>
                              <div className="address-form-wrapper seperateData">
                                {this.state.loaderStatus === true ?
                                  <ZenForm ref={this.receiptRef} inputData={this.state.receiptFormData} onInputChanges={this.onInputChanges} />
                                  : <p className="noprog-txt">{this.state.resText}</p>}
                              </div>
                            </div>
                          </div>
                        </div>
                        {/* /////// */}
                        {/* CONTENT */}
                        <div className={`${this.state.sendReceiptTab === 1 ? "tab-cont op-1" : "tab-cont op-0"}`} style={{ width: "81%" }}>
                          <div className="tab-one-content-value-header">
                            <div className="content-value-header-name">
                              <p className="value-header-name">Select</p>
                            </div>
                          </div>
                          <div className="address-form-wrapper ">
                            <div className="tab-one-content-key full-setting-body settings-receipt" style={{ overflow: "auto" }}>
                              <div className="address-form-wrapper seperateData">
                                {this.state.loaderStatus === true ?
                                  <ZenForm ref={this.receiptContentRef} inputData={this.state.receiptContentFormData} onInputChanges={this.onInputChanges} />
                                  : <p className="noprog-txt">{this.state.resText}</p>}
                              </div>
                            </div>
                          </div>
                        </div>
                        {/* /////// */}
                      </div>
                    </div>
                  </div>
                  {/* // */}
                  {/* PARTIAL AMOUNT */}
                  <div className={`${this.state.receiptTab === 3 ? "tab-cont op-1" : "tab-cont op-0"} `} style={{ width: "81%" }}>
                    <div className="tab-one-content-value-header">
                      <div className="content-value-header-name">
                        <p className="value-header-name">Allowed</p>
                      </div>
                    </div>
                    <div className="address-form-wrapper ">
                      <div className="tab-one-content-key full-setting-body settings-receipt" style={{ overflow: "auto" }}>
                        <div className="address-form-wrapper seperateData">
                          {this.state.loaderStatus === true ?
                            <ZenForm ref={this.receiptAmountRef} inputData={this.state.receiptAmountFormData} onInputChanges={this.onInputChanges} />
                            : <p className="noprog-txt">{this.state.resText}</p>}
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* // */}
                  {/* LOGOS */}
                  <div className={`${this.state.receiptTab === 4 ? "tab-cont op-1" : "tab-cont op-0"}`} style={{ width: "81%" }}>
                    <div className="tab-one-content-value-header">
                      <div className="content-value-header-name">
                        <p className="value-header-name">Select</p>
                      </div>
                    </div>
                    <div className="address-form-wrapper ">
                      <div className="tab-one-content-key full-setting-body settings-receipt" style={{ overflow: "auto" }}>
                        <div className="address-form-wrapper seperateData">
                          {this.state.loaderStatus == true ?
                            <ZenForm ref={this.logosRef} inputData={this.state.logosFormData} onInputChanges={this.onInputChanges} />
                            : <p className="noprog-txt">{this.state.resText}</p>
                          }
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* // */}
                </div>
              </div>

            </div>

            {/* Labels */}
            <div className={this.state.tab === 3 ? "tab-cont op-1" : "tab-cont op-0"}>
              <div className="tab-one-content-key full-setting-body" style={{ overflow: "auto" }}>
                <div className="tab-one-content-header">
                  <p style={{ fontSize: "14px", color: "#061938" }}>LABELS</p>
                </div>

                <div className="address-form-wrapper seperateData">
                  {this.state.loaderStatus === true ?
                    <table className="settings-label-table">
                      <thead>
                        <tr>
                          {this.state.labelTableHeader.map((data, i) => {
                            return <th key={i + 1}>{data}</th>
                          })}
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>REG ID</td>
                          <td><input type="text" value={labelTableData[0]["regId"]} onChange={(e) => { this.handleLabelTable(e.target.value, "regId") }} /></td>
                        </tr>
                        <tr>
                          <td>CLASS/BATCH</td>
                          <td><input type="text" value={labelTableData[0]["classBatch"]} onChange={(e) => { this.handleLabelTable(e.target.value, "classBatch") }} /></td>
                        </tr>
                        {/* <tr>
                          <td>PROGRAM PLAN</td>
                          <td><input type="text" value={labelTableData[0]['programPlan']} onChange={(e) => { this.handleLabelTable(e.target.value, "programPlan") }} /></td>
                        </tr> */}
                      </tbody>
                    </table> : <p className="noprog-txt">{this.state.resText}</p>}
                </div>

              </div>
            </div>
            {/* /////////// */}
            <div className={this.state.tab === 4 ? "tab-cont op-1" : "tab-cont op-0"}>
              <div className="tab-one-content-key full-setting-body settings-email-new-content">
                {/* <div
                  className="history-icon-box"
                  title="View History"
                  onClick={() => this.openHistory("emailServer")}
                  style={{ width: '100px', float: 'right', position: "relative", top: "-30px", right: "15px" }}
                >
                  <RestoreOutlinedIcon className="material-historyIcon" />
                  <p>History</p>
                </div> */}
                <div className="settings-email-left">
                  <div className="tab-one-content-header">
                    <p style={{ fontSize: "14px", color: "#061938" }}>EMAIL SERVER</p>
                  </div>
                  <div className="settings-email-form">
                    {this.state.loaderStatus === true ?
                      <>
                        <p className="settings-email-form-label">Select Email Server</p>
                        <SelectPicker
                          className="select-input-wrap"
                          data={this.state.emailSelect}
                          style={{ width: 292, marginLeft: 11, marginTop: 5 }}
                          defaultValue={this.state.emailSelectValue}
                          searchable={true}
                          onSelect={(value, item, event) =>
                            this.onEmailSelectChanges(value, item, event)
                          }
                        />
                      </> : <><p className="noprog-txt">{this.state.resText}</p></>
                    }
                  </div>
                </div>

                <div className="seperateData settings-email-right settings-email-scroll">
                  {this.state.loaderStatus ?
                    <>
                      {/* Email Details 1 */}
                      < div className="settings-email-first-right-wrapper">
                        <p className="settings-email-right-header">Email Details 1</p>
                        <div className="settings-email-right-container" style={{ marginLeft: "15px" }}>
                          <div className="settings-email-right-textfield">
                            <p className="settings-email-form-label">Enter Email Address</p>
                            <TextField
                              className="settings-email-form-textfield"
                              name="email"
                              // rows={this.state.inputData.row != undefined ? this.state.inputData.row : null}
                              // multiline={this.state.inputData.row != undefined ? true : false}
                              // id={this.state.inputData.name}
                              placeholder="Enter Email Address"
                              // label="Select Email Address"
                              value={this.state.emailServerEmail1}
                              defaultValue={this.state.emailServerEmail1}
                              variant="filled"
                              // required={this.state.inputData.required}
                              // InputProps={{
                              //   readOnly: this.state.inputData.readOnly,
                              // }}
                              type="emailph"
                              helperText={this.state.emailServerHelperText}
                              error={this.state.emailServerError}
                              onChange={(e) => this.onInputChanges1(e)}
                              // autoFocus={this.state.inputData.focus === undefined ? false : this.state.inputData.focus}
                              autoComplete="off"
                            >
                            </TextField>
                          </div>
                          <div className="settings-email-right-button">
                            <input type="file" id="email-config" style={{ display: 'none' }} name="Email Config File" alt="config file" onClick={event => event.target.value = null}
                              onChange={(e) => { this.setState({ currentJsonUpload: 1 }, () => { this.uploadEmailConfigFile(e) }); }} />
                            <div style={{ width: "100%", paddingTop: "28px" }}>
                              <label htmlFor="email-config" className="email-config-btn">Select Files</label>
                            </div>
                          </div>
                        </div>

                        <div className="input-file-config">
                          <p class="form-hd settings-form-hd" style={{ paddingLeft: "0px" }}>Config JSON File</p>
                          <div className="inline-upload-box" style={{ display: "inline-flex", width: '100%' }}>
                            <div style={{ width: "20%" }}>
                              <p class="settings-form-hd" style={{ paddingLeft: "0px" }}>File Name</p>
                              <span style={{ display: "grid", width: "100%" }}>{this.state.newConfigFileName}</span>
                            </div>
                            <div style={{ width: "80%" }}>
                              <p class="settings-form-hd" style={{ textAlign: "center" }}>Content</p>
                              <div style={{ width: '100%', overflow: 'hidden', height: '200px' }} >
                                <textarea className="text-area-json-file" id="emailconfig" value={this.state.newEmailConfigJson1 === "No data" || this.state.newEmailConfigJson1 === "" ? this.state.newEmailConfigJson1 : JSON.stringify(this.state.newEmailConfigJson1)} disabled style={{ width: 'calc(100% - 60px)', minHeight: '140px', position: 'relative', left: '20px' }}
                                  ref={this.textAreaInputRef} ></textarea>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      {/* Email Details 2 */}
                      <div className="settings-email-second-right-wrapper">
                        <p className="settings-email-right-header">Email Details 2</p>
                        <div className="settings-email-right-container" style={{ marginLeft: "15px" }}>
                          <div className="settings-email-right-textfield">
                            <p className="settings-email-form-label">Enter Email Address</p>
                            <TextField
                              className="settings-email-form-textfield"
                              name=""
                              // rows={this.state.inputData.row != undefined ? this.state.inputData.row : null}
                              // multiline={this.state.inputData.row != undefined ? true : false}
                              // id={this.state.inputData.name}
                              placeholder="Enter Email Address"
                              // label={this.state.inputData.label}
                              value={this.state.emailServerEmail2}
                              defaultValue={this.state.emailServerEmail2}
                              variant="filled"
                              // required={this.state.inputData.required}
                              // InputProps={{
                              //   readOnly: this.state.inputData.readOnly,
                              // }}
                              type="emailph"
                              helperText={this.state.emailServerHelperText2}
                              error={this.state.emailServerError2}
                              onChange={(e) => this.onInputChanges2(e)}
                              // autoFocus={this.state.inputData.focus === undefined ? false : this.state.inputData.focus}
                              autoComplete="off"
                            >
                            </TextField>
                          </div>
                          <div className="settings-email-right-button">
                            <input type="file" id="email-config2" style={{ display: 'none' }} name="Email Config File" alt="config file"
                              onClick={event => event.target.value = null}
                              onChange={(e) => {
                                this.setState({ currentJsonUpload: 2 }, () => { this.uploadEmailConfigFile(e) })
                              }} />
                            <div style={{ width: "100%", paddingTop: "28px" }}>
                              <label htmlFor="email-config2" className="email-config-btn">Select Files</label>
                            </div>
                          </div>
                        </div>

                        <div className="input-file-config">
                          <p class="form-hd settings-form-hd" style={{ paddingLeft: "0px" }}>Config JSON File</p>
                          <div className="inline-upload-box" style={{ display: "inline-flex", width: '100%' }}>
                            <div style={{ width: "20%" }}>
                              <p class="settings-form-hd" style={{ paddingLeft: "0px" }}>File Name</p>
                              <span style={{ display: "grid", width: "100%" }}>{this.state.newConfigFileName2}</span>
                            </div>
                            <div style={{ width: "80%" }}>
                              <p class="settings-form-hd" style={{ textAlign: "center" }}>Content</p>
                              <div style={{ width: '100%', overflow: 'hidden', height: '200px' }} >
                                <textarea className="text-area-json-file" id="emailconfig" value={this.state.newEmailConfigJson2 === "No data" || this.state.newEmailConfigJson2 === "" ? this.state.newEmailConfigJson2 : JSON.stringify(this.state.newEmailConfigJson2)} disabled style={{ width: 'calc(100% - 60px)', minHeight: '140px', position: 'relative', left: '20px' }}
                                  ref={this.textAreaInputRef} ></textarea>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="settings-email-third-right-wrapper">
                        <p className="settings-email-right-header">Email Details 3</p>
                        <div className="settings-email-right-container" style={{ marginLeft: "15px" }}>
                          <div className="settings-email-right-textfield">
                            <p className="settings-email-form-label">Enter Email Address</p>
                            <TextField
                              className="settings-email-form-textfield"
                              name=""
                              // rows={this.state.inputData.row != undefined ? this.state.inputData.row : null}
                              // multiline={this.state.inputData.row != undefined ? true : false}
                              // id={this.state.inputData.name}
                              placeholder="Enter Email Address"
                              // label={this.state.inputData.label}
                              value={this.state.emailServerEmail3}
                              defaultValue={this.state.emailServerEmail3}
                              variant="filled"
                              // required={this.state.inputData.required}
                              // InputProps={{
                              //   readOnly: this.state.inputData.readOnly,
                              // }}
                              type="emailph"
                              helperText={this.state.emailServerHelperText3}
                              error={this.state.emailServerError3}
                              onChange={(e) => this.onInputChanges3(e)}
                              // autoFocus={this.state.inputData.focus === undefined ? false : this.state.inputData.focus}
                              autoComplete="off"
                            >
                            </TextField>
                          </div>
                          <div className="settings-email-right-button">
                            <input type="file" id="email-config3" style={{ display: 'none' }} name="Email Config File" alt="config file" onClick={event => event.target.value = null}
                              onChange={(e) => { this.setState({ currentJsonUpload: 3 }, () => { this.uploadEmailConfigFile(e) }) }} />
                            <div style={{ width: "100%", paddingTop: "28px" }}>
                              <label htmlFor="email-config3" className="email-config-btn">Select Files</label>
                            </div>
                          </div>
                        </div>

                        <div className="input-file-config">
                          <p class="form-hd settings-form-hd" style={{ paddingLeft: "0px" }}>Config JSON File</p>
                          <div className="inline-upload-box" style={{ display: "inline-flex", width: '100%' }}>
                            <div style={{ width: "20%" }}>
                              <p class="settings-form-hd" style={{ paddingLeft: "0px" }}>File Name</p>
                              <span style={{ display: "grid", width: "100%" }}>{this.state.newConfigFileName3}</span>
                            </div>
                            <div style={{ width: "80%" }}>
                              <p class="settings-form-hd" style={{ textAlign: "center" }}>Content</p>
                              <div style={{ width: '100%', overflow: 'hidden', height: '200px' }} >
                                <textarea className="text-area-json-file" id="emailconfig" value={this.state.newEmailConfigJson3 === "No data" || this.state.newEmailConfigJson3 === "" ? this.state.newEmailConfigJson3 : JSON.stringify(this.state.newEmailConfigJson3)} disabled style={{ width: 'calc(100% - 60px)', minHeight: '140px', position: 'relative', left: '20px' }}
                                  ref={this.textAreaInputRef} ></textarea>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="settings-email-fourth-right-wrapper">
                        <p className="settings-email-right-header">Email Details 4</p>
                        <div className="settings-email-right-container" style={{ marginLeft: "15px" }}>
                          <div className="settings-email-right-textfield">
                            <p className="settings-email-form-label">Enter Email Address</p>
                            <TextField
                              className="settings-email-form-textfield"
                              name=""
                              // rows={this.state.inputData.row != undefined ? this.state.inputData.row : null}
                              // multiline={this.state.inputData.row != undefined ? true : false}
                              // id={this.state.inputData.name}
                              placeholder="Enter Email Address"
                              // label={this.state.inputData.label}
                              value={this.state.emailServerEmail4}
                              defaultValue={this.state.emailServerEmail4}
                              variant="filled"
                              // required={this.state.inputData.required}
                              // InputProps={{
                              //   readOnly: this.state.inputData.readOnly,
                              // }}
                              type="emailph"
                              helperText={this.state.emailServerHelperText4}
                              error={this.state.emailServerError4}
                              onChange={(e) => this.onInputChanges4(e)}
                              // autoFocus={this.state.inputData.focus === undefined ? false : this.state.inputData.focus}
                              autoComplete="off"
                            >
                            </TextField>
                          </div>
                          <div className="settings-email-right-button">
                            <input type="file" id="email-config4" style={{ display: 'none' }} name="Email Config File" alt="config file" onClick={event => event.target.value = null}
                              onChange={(e) => { this.setState({ currentJsonUpload: 4 }, () => { this.uploadEmailConfigFile(e) }) }} />
                            <div style={{ width: "100%", paddingTop: "28px" }}>
                              <label htmlFor="email-config4" className="email-config-btn">Select Files</label>
                            </div>
                          </div>
                        </div>

                        <div className="input-file-config">
                          <p class="form-hd settings-form-hd" style={{ paddingLeft: "0px" }}>Config JSON File</p>
                          <div className="inline-upload-box" style={{ display: "inline-flex", width: '100%' }}>
                            <div style={{ width: "20%" }}>
                              <p class="settings-form-hd" style={{ paddingLeft: "0px" }}>File Name</p>
                              <span style={{ display: "grid", width: "100%" }}>{this.state.newConfigFileName4}</span>
                            </div>
                            <div style={{ width: "80%" }}>
                              <p class="settings-form-hd" style={{ textAlign: "center" }}>Content</p>
                              <div style={{ width: '100%', overflow: 'hidden', height: '200px' }} >
                                <textarea className="text-area-json-file" id="emailconfig" value={this.state.newEmailConfigJson4 === "No data" || this.state.newEmailConfigJson4 === "" ? this.state.newEmailConfigJson4 : JSON.stringify(this.state.newEmailConfigJson4)} disabled style={{ width: 'calc(100% - 60px)', minHeight: '140px', position: 'relative', left: '20px' }}
                                  ref={this.textAreaInputRef} ></textarea>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="settings-email-fifth-right-wrapper">
                        <p className="settings-email-right-header">Email Details 5</p>
                        <div className="settings-email-right-container" style={{ marginLeft: "15px" }}>
                          <div className="settings-email-right-textfield">
                            <p className="settings-email-form-label">Enter Email Address</p>
                            <TextField
                              className="settings-email-form-textfield"
                              name=""
                              // rows={this.state.inputData.row != undefined ? this.state.inputData.row : null}
                              // multiline={this.state.inputData.row != undefined ? true : false}
                              // id={this.state.inputData.name}
                              placeholder="Enter Email Address"
                              // label={this.state.inputData.label}
                              value={this.state.emailServerEmail5}
                              defaultValue={this.state.emailServerEmail5}
                              variant="filled"
                              // required={this.state.inputData.required}
                              // InputProps={{
                              //   readOnly: this.state.inputData.readOnly,
                              // }}
                              type="emailph"
                              helperText={this.state.emailServerHelperText5}
                              error={this.state.emailServerError5}
                              onChange={(e) => this.onInputChanges5(e)}
                              // autoFocus={this.state.inputData.focus === undefined ? false : this.state.inputData.focus}
                              autoComplete="off"
                            >
                            </TextField>
                          </div>
                          <div className="settings-email-right-button">
                            <input type="file" id="email-config5" style={{ display: 'none' }} name="Email Config File" alt="config file" onClick={event => event.target.value = null}
                              onChange={(e) => { this.setState({ currentJsonUpload: 5 }, () => { this.uploadEmailConfigFile(e) }) }} />
                            <div style={{ width: "100%", paddingTop: "28px" }}>
                              <label htmlFor="email-config5" className="email-config-btn">Select Files</label>
                            </div>
                          </div>
                        </div>

                        <div className="input-file-config">
                          <p class="form-hd settings-form-hd" style={{ paddingLeft: "0px" }}>Config JSON File</p>
                          <div className="inline-upload-box" style={{ display: "inline-flex", width: '100%' }}>
                            <div style={{ width: "20%" }}>
                              <p class="settings-form-hd" style={{ paddingLeft: "0px" }}>File Name</p>
                              <span style={{ display: "grid", width: "100%" }}>{this.state.newConfigFileName5}</span>
                            </div>
                            <div style={{ width: "80%" }}>
                              <p class="settings-form-hd" style={{ textAlign: "center" }}>Content</p>
                              <div style={{ width: '100%', overflow: 'hidden', height: '200px' }} >
                                <textarea className="text-area-json-file" id="emailconfig" value={this.state.newEmailConfigJson5 === "No data" || this.state.newEmailConfigJson5 === "" ? this.state.newEmailConfigJson5 : JSON.stringify(this.state.newEmailConfigJson5)} disabled style={{ width: 'calc(100% - 60px)', minHeight: '140px', position: 'relative', left: '20px' }}
                                  ref={this.textAreaInputRef} ></textarea>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      {/* <div className="settings-email-fifth-right-wrapper">
                    <EmailServer header={6} emailServerEmail={this.state.emailServerEmail1}
                      emailServerHelperText={this.state.emailServerHelperText} emailServerError={this.state.emailServerError}
                      onInputChanges={this.onInputChanges1} newConfigFileName={this.state.newConfigFileName} newEmailConfigJson={this.state.newEmailConfigJson1} />
                  </div> */}



                      {/* <div className="settings-email-form">
                    {this.state.loaderStatus === true ?
                      // <ZenForm ref={this.emailServerRef} inputData={this.state.emailServerFormData} onInputChanges={this.onInputChanges} onSubmit={this.onSubmit} />
                      <SelectPicker
                        className="select-input-wrap"
                        searchable={false}
                        data={this.state.emailSelect}
                        style={{ width: 270 }}
                        defaultValue={this.state.defNumber}
                        onSelect={(value, item, event) =>
                          this.selectValueInv(value, item, event)
                        }
                      />
                      : <p className="noprog-txt">{this.state.resText}</p>
                    }
                  </div> */}

                      {/* <div className="settings-email-form">
                    {this.state.loaderStatus === true ?
                      <ZenForm ref={this.emailServerRef} inputData={this.state.emailServerFormData} onInputChanges={this.onInputChanges} onSubmit={this.onSubmit} />
                      : <p className="noprog-txt">{this.state.resText}</p>
                    }
                    <input type="file" id="email-config" style={{ display: 'none' }} name="Email Config File" alt="config file" onChange={this.uploadEmailConfigFile} />
                    <div style={{ width: "15%", marginTop: "2%" }}>
                      <label htmlFor="email-config" className="email-config-btn">Select Files</label>
                    </div>
                  </div> */}

                      {/* <div className="input-file-config">
                    <p class="form-hd settings-form-hd" style={{ paddingLeft: "0px" }}>Config JSON File</p>
                    <div className="inline-upload-box" style={{ display: "inline-flex", width: '100%' }}>
                      <div style={{ width: "20%" }}>
                        <p class="settings-form-hd" style={{ paddingLeft: "0px" }}>File Name</p>
                        <span style={{ display: "grid", width: "100%" }}>{this.state.configFileName}</span>
                      </div>
                      <div style={{ width: "80%" }}>
                        <p class="settings-form-hd" style={{ textAlign: "center" }}>Content</p>
                        <div style={{ width: '100%', overflow: 'hidden', height: '225px' }} >
                          <textarea className="text-area-json-file" id="emailconfig" value={JSON.stringify(this.state.emailConfigJson)} disabled style={{ width: 'calc(100% - 60px)', minHeight: '220px', position: 'relative', left: '20px' }}
                            ref={this.textAreaInputRef} ></textarea>
                        </div>
                      </div>
                    </div>
                  </div> */}

                      {/* <div className="input-file-config">
                    <p class="form-hd settings-form-hd" style={{ paddingLeft: "0px" }}>Config JSON File</p>
                    <div className="inline-upload-box" style={{ display: "inline-flex", width: '100%' }}>
                      <div style={{ width: "20%" }}>
                        <p class="settings-form-hd" style={{ paddingLeft: "0px" }}>File Name</p>
                        <span style={{ display: "grid", width: "100%" }}>{this.state.configFileName}</span>
                      </div>
                      <div style={{ width: "80%" }}>
                        <p class="settings-form-hd" style={{ textAlign: "center" }}>Content</p>
                        <div style={{ width: '100%', overflow: 'hidden', height: '225px' }} >
                          <textarea className="text-area-json-file" id="emailconfig" value={JSON.stringify(this.state.emailConfigJson)} disabled style={{ width: 'calc(100% - 60px)', minHeight: '220px', position: 'relative', left: '20px' }}
                            ref={this.textAreaInputRef} ></textarea>
                        </div>
                      </div>
                    </div>
                  </div> */}

                      {/* <>{this.state.uploadConfig ? <div className="input-file-config">
                    <p class="form-hd settings-form-hd" style={{ paddingLeft: "0px" }}>Config JSON File</p>
                    <div className="inline-upload-box" style={{ display: "inline-flex", width: '100%' }}>
                      <div style={{ width: "20%" }}>
                        <p class="settings-form-hd" style={{ textAlign: "center" }}>File Name</p>
                        {this.state.configFileName.length > 0 ?
                          <>
                            <span style={{ textAlign: "center", display: "grid", width: "100%", paddingLeft: "10px" }}>{this.state.configFileName}</span>
                          </> : null}
                      </div>
                      <div style={{ width: "80%" }}>
                        <p class="settings-form-hd" style={{ textAlign: "center" }}>Content</p>
                        <>{this.state.configInfo ? <div style={{ width: '100%', overflow: 'hidden', height: '225px' }} >
                          <textarea className="text-area-json-file" id="emailconfig" value={JSON.stringify(this.state.emailConfigJson)} disabled style={{ width: 'calc(100% - 60px)', minHeight: '220px', position: 'relative', left: '20px' }}
                            ref={this.textAreaInputRef} ></textarea>
                        </div> : <></>}
                        </>
                      </div>
                    </div>
                  </div> : <>

                    </>}
                    <input type="file" id="email-config" style={{ display: 'none' }} name="Email Config File" alt="config file" onChange={this.uploadEmailConfigFile} />
                    <div style={{ width: "100%", textAlign: "end", padding: "30px 30px" }}>
                      <label htmlFor="email-config" className="email-config-btn">Select Files</label>
                    </div>
                  </> */}
                    </> : <><p className="noprog-txt">{this.state.resText}</p></>}
                </div>
              </div>

            </div>
            <div className={this.state.tab === 5 ? "tab-cont op-1" : "tab-cont op-0"}>
              <div className="tab-one-content-key full-setting-body">
                <div className="tab-one-content-header">
                  <p style={{ fontSize: "14px", color: "#061938" }}>SMS GATEWAY</p>
                  {/* <span style={{ position: "absolute", top: "19px", left: "123px", fontWeight: "normal" }}>v{this.state.smsGatewayVersion}</span> */}
                </div>
                {/* <div
                  className="history-icon-box"
                  title="View History"
                  onClick={() => this.openHistory("smsGateway")}
                  style={{ width: '100px', float: 'right', position: "relative", top: "-30px", right: "15px" }}
                >
                  <RestoreOutlinedIcon className="material-historyIcon" />
                  <p>History</p>
                </div> */}
                <div className="address-form-wrapper settings-sms-gateway">
                  {/* {this.state.smsGatewayData.length > 0 ? */}
                  {this.state.loaderStatus === true ?
                    <ZenForm ref={this.smsGatewayRef} inputData={this.state.smsGatewayData} onInputChanges={this.onInputChanges} onSubmit={this.onSubmit} />
                    : <p className="noprog-txt">{this.state.resText}</p>
                  }
                </div>
              </div>
            </div>
            <div className={this.state.tab === 6 ? "tab-cont op-1" : "tab-cont op-0"}>
              <div className="tab-one-content-key full-setting-body">
                <div className="tab-one-content-header">
                  <p style={{ fontSize: "14px", color: "#061938" }}>PAYMENT GATEWAY</p>
                  {/* <span style={{ position: "absolute", top: "19px", left: "154px", fontWeight: "normal" }}>v{this.state.paymentGatewayVersion}</span> */}
                </div>
                {/* <div
                  className="history-icon-box"
                  title="View History"
                  onClick={() => this.openHistory("paymentGateway")}
                  style={{ width: '100px', float: 'right', position: "relative", top: "-30px", right: "15px" }}
                >
                  <RestoreOutlinedIcon className="material-historyIcon" />
                  <p>History</p>
                </div> */}
                <div className="address-form-wrapper settings-payment-gateway">
                  {/* {this.state.paymentGatewayData.length > 0 ? */}
                  {this.state.loaderStatus === true ?
                    <ZenForm ref={this.paymentGatewayRef} inputData={this.state.paymentGatewayData} onInputChanges={this.onInputChanges} onSubmit={this.onSubmit} />
                    : <p className="noprog-txt">{this.state.resText}</p>
                  }
                  {/* : null} */}
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* </div> */}
        {
          this.state.history ?
            <Drawer size='xs' placement='right' show={this.state.showHistory}>
              <Drawer.Header style={{ display: 'flex', paddingRight: '0px' }}>
                <Drawer.Title className="change-history-title">CHANGE HISTORY</Drawer.Title>
                <HighlightOffIcon title="Close" onClick={this.onCloseForm} className="change-history-close-btn" />
              </Drawer.Header>
              <Drawer.Body>
                <Timeline align='left'>
                  <ZqHistory historyData={this.state.historyData} onCloseForm={this.onCloseForm} />
                </Timeline>
              </Drawer.Body>
            </Drawer>
            : null
        }
      </React.Fragment >
    );
  }
}
export default withRouter(Settings);
