import React, { Component } from "react";
import '../../../../scss/student.scss';
import '../../../../scss/fees-type.scss';
import '../../../../scss/setup.scss';
import Loader from "../../../../utils/loader/loaders";
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../utils/Table/table-component";
import PaginationUI from "../../../../utils/pagination/pagination";
import ZenTabs from '../../../input/tabs';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import InstallmentJson from './fees-manager.json';
import axios from "axios";
import moment from 'moment';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import DateFormatContext from '../../../../gigaLayout/context';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
class FeesManager extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            campusId: localStorage.getItem('campusId') === "null" || localStorage.getItem('campusId') === null ? undefined : localStorage.getItem('campusId'),
            containerNav: InstallmentJson.containerNav,
            studentsFormjson: InstallmentJson.feeManager,
            sampleStudentsData: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 1,
            tabViewForm: false,
            studentName: "",
            tableResTxt: "Fetching Data...",
            viewHeader: true,
            totalApiRes: null,
            viewFormLoad: false,
            LoaderStatus: false,
            feesBreakUpArray: [],
            previewID: '',
            tableViewMode: "",
            payloadId: "",
            snackbar: { openNotification: false, NotificationMessage: "", status: "success" },
            AddInstallmentFormData: InstallmentJson.AddNewFeeManager,
            addNewFeePayloadId: { programPln: "", feeTyp: "", paymentSch: "", reminderPln: "", lateFee: "" }
        }
    }

    static contextType = DateFormatContext;
    componentDidMount() {
        this.resetform()
        setTimeout(() => { this.getTableData() }, 500)
        console.log(this.context)
    }
    commonFormatter = (format, date) => {
        if (format !== null && date !== null) {
            let m = moment(new Date(date)).format(this.context.dateFormat)
            return m
        }
    }
    getTableData = () => {
        this.setState({ sampleStudentsData: [] })
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/feesManager?orgId=${this.state.orgId}&campusId=${this.state.campusId}&page=${this.state.page}&limit=${this.state.limit}`,
            headers: {
                'Authorization': this.state.authToken,
            }
        })
            .then(res => {
                console.log(res);
                if (res.data.status == "success") {
                    if (res.data.data.length == 0) {
                        this.setState({ tableResTxt: "No Data" })
                    }
                    else {
                        let studentTableDetails = [];
                        let resp = res.data.data;
                        console.log(res);
                        resp.map((data) => {
                            studentTableDetails.push({
                                "id": data.displayName,
                                "title": data.title,
                                "description": data.description,
                                "program plan": data.programPlan.title,
                                "Fee type": data.feeType.title,
                                "annual amount": Number(data.feeDetails.totalAmount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' }),
                                "Campus Name": "-",
                                "created by": data.createdBy !== undefined ? data.createdBy : 'Vijay Kiran',
                                "created on": data.createdAt !== "" ? this.commonFormatter(this.context.dateFormat, new Date(data.createdAt)) : "-",
                                "Item": JSON.stringify(data)
                            })
                        })
                        this.setState({ sampleStudentsData: studentTableDetails, totalApiRes: res.data.data, totalRecord: res.data.totalRecord, totalPages: res.data.totalPages, page: res.data.currentPage })
                        let feeTypeId = []; let lateFeeId = []; let PaymentScheduleId = []; let programPlanId = []; let reminderPlanId = [];
                        res.data.allFeeType.map((dataOne) => { // fee type
                            feeTypeId.push({
                                "label": dataOne.displayName,
                                "value": dataOne.displayName,
                                "allData": dataOne,
                                "typeOflist": "feeType"
                            })
                        })
                        res.data.allLateFee.map((dataOne) => { // late fee
                            lateFeeId.push({
                                "label": dataOne.displayName,
                                "value": dataOne.displayName,
                                "allData": dataOne,
                                "typeOflist": "lateFee"
                            })
                        })
                        res.data.allPaymentSchedule.map((dataOne) => { // payment schedule
                            PaymentScheduleId.push({
                                "label": dataOne.displayName,
                                "value": dataOne.displayName,
                                "allData": dataOne,
                                "typeOflist": "paymentSchedule"
                            })
                        })
                        res.data.allProgramPlan.map((dataOne) => { // program plan
                            programPlanId.push({
                                "label": dataOne.displayName,
                                "value": dataOne.displayName,
                                "allData": dataOne,
                                "typeOflist": "programPlan"
                            })
                        })
                        res.data.allReminderPlan.map((dataOne) => { // reminder plan
                            reminderPlanId.push({
                                "label": dataOne.displayName,
                                "value": dataOne.displayName,
                                "allData": dataOne,
                                "typeOflist": "reminderPlan"
                            })
                        })
                        console.log(feeTypeId, lateFeeId, PaymentScheduleId, programPlanId, reminderPlanId);
                        let installmentFormData = InstallmentJson.feeManager;
                        let NewInstallmentFormData = InstallmentJson.AddNewFeeManager;
                        Object.keys(installmentFormData).forEach(tab => {
                            installmentFormData[tab].forEach(task => {
                                if (task['name'] == 'PaymentScheduleOption') { // payment schedule
                                    task['readOnly'] = false
                                    task['options'] = PaymentScheduleId;
                                }
                                if (task['name'] == 'ReminderPlanOption') { // reminder
                                    task['readOnly'] = false
                                    task['options'] = reminderPlanId
                                }
                                if (task['name'] == 'LateFeesOptions') { // Late fees
                                    task['readOnly'] = false
                                    task['options'] = lateFeeId
                                }
                                if (task['name'] == 'FeetypeOption') { // fees type
                                    task['options'] = feeTypeId
                                    task['readOnly'] = false
                                }
                                if (task['name'] == 'ProgramPlanOption') { // program plan
                                    task['options'] = programPlanId
                                    task['readOnly'] = false
                                }
                            })
                        })
                        Object.keys(NewInstallmentFormData).forEach(tab => {
                            NewInstallmentFormData[tab].forEach(task => {
                                if (task['name'] == 'PaymentScheduleOption') { // payment schedule
                                    task['readOnly'] = false
                                    task['options'] = PaymentScheduleId;
                                }
                                if (task['name'] == 'ReminderPlanOption') { // reminder
                                    task['readOnly'] = false
                                    task['options'] = reminderPlanId
                                }
                                if (task['name'] == 'LateFeesOptions') { // Late fees
                                    task['readOnly'] = false
                                    task['options'] = lateFeeId
                                }
                                if (task['name'] == 'FeetypeOption') { // fees type
                                    task['options'] = feeTypeId
                                    task['readOnly'] = false
                                }
                                if (task['name'] == 'ProgramPlanOption') { // program plan
                                    task['options'] = programPlanId
                                    task['readOnly'] = false
                                }
                            })
                        })
                    }
                }
            })
            .catch(err => {
                console.log(err);
            })
    }
    onAddFeeManager = () => {
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/feesManager/displayname`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                console.log(res);
                this.setState({
                    tabViewForm: true,
                    viewHeader: false,
                    viewFormLoad: true,
                    previewID: 'FM024'
                    // tableResTxt: "Fetching Data..."
                }, () => {
                    let installmentFormData = InstallmentJson.AddNewFeeManager;
                    Object.keys(installmentFormData).forEach(tab => {
                        installmentFormData[tab].forEach(task => {
                            if (task['name'] == 'Id') {
                                task['readOnly'] = true
                                task['defaultValue'] = res.data.data
                            }
                            if (task['name'] == 'title') {
                                task['readOnly'] = false
                                task['required'] = true
                                task['requiredBoolean'] = true
                            }
                            if (task['name'] == 'description') {
                                task['readOnly'] = false
                            }
                            if (task['name'] == 'annualAmount') {
                                task['readOnly'] = false
                                task['required'] = true
                                task['requiredBoolean'] = true
                            }
                            // ------------- Payment Schedule --------
                            if (task['name'] == 'PaymentScheduleOption') { // options // defaultValue
                                task['readOnly'] = false
                            }
                            if (task['name'] == 'PaymentScheduleTitle') { // title
                                task['readOnly'] = false
                            }
                            if (task['name'] == 'PaymentScheduleDescription') { // description
                                task['readOnly'] = false
                            }
                            // ------------- Reminder ----------------- 
                            if (task['name'] == 'ReminderPlanOption') { // options // defaultValue
                                task['readOnly'] = false
                            }
                            if (task['name'] == 'ReminderPlanTitle') { // title
                                task['readOnly'] = false
                            }
                            if (task['name'] == 'ReminderPlanDescription') { // description
                                task['readOnly'] = false
                            }
                            // ------------- Late fees -----------------    
                            if (task['name'] == 'LateFeesOptions') { // options // defaultValue
                                task['readOnly'] = false
                            }
                            if (task['name'] == 'LateFeesTitle') { // title
                                task['readOnly'] = false
                            }
                            if (task['name'] == 'LateFeesDescription') { // description
                                task['readOnly'] = false
                            }
                            // ------------- Fee type ----------------- 
                            if (task['name'] == 'FeetypeOption') { // options // defaultValue
                                task['readOnly'] = false
                            }
                            if (task['name'] == 'FeetypeTitle') { // title
                                task['readOnly'] = false
                            }
                            if (task['name'] == 'FeetypeDescription') { // description
                                task['readOnly'] = false
                            }
                            // ------------- Program plan ----------------- 
                            if (task['name'] == 'ProgramPlanOption') { // options // defaultValue
                                task['readOnly'] = false
                            }
                            if (task['name'] == 'ProgramPlanTitle') { // title
                                task['readOnly'] = false
                            }
                            if (task['name'] == 'ProgramPlanDescription') { // description
                                task['readOnly'] = false
                            }
                        })
                    })
                    this.setState({ viewFormLoad: false, addNewId: res.data.data, tableViewMode: "addNew", });
                })
            })
            .catch(err => { console.log(err); })
    }
    handleBackFun = () => {
        this.setState({ tabViewForm: false, tableView: false })
        this.resetform()
    }
    cancelViewData = () => {
        this.setState({ tableView: false });
        this.moveBackTable();
        this.resetform();
    }
    onPreviewStudentList = (e) => {
        let details = this.state.globalSearch ? e : JSON.parse(e.Item)
        console.log(e, details);
        let installmentFormData = InstallmentJson.feeManager;
        // addNewFeePayloadId: { programPln: "", feeTyp: "", paymentSch: "", reminderPln: "", lateFee: "" }
        let setPayloadId = this.state.addNewFeePayloadId;
        setPayloadId.programPln = details.programPlan === undefined ? "" : details.programPlan._id;
        setPayloadId.feeTyp = details.feeType === undefined ? "" : details.feeType._id;
        setPayloadId.paymentSch = details.paymentSchedule === undefined ? "" : details.paymentSchedule._id;
        setPayloadId.reminderPln = details.reminderPlan === undefined ? "" : details.reminderPlan._id;
        setPayloadId.lateFee = details.lateFeePlan === undefined ? "" : details.lateFeePlan._id;
        this.setState({ previewID: details.displayName });
        if (details.displayName === e.id) {
            this.setState({ tabViewForm: true, studentName: e.id, viewHeader: true, viewFormLoad: true, tableViewMode: "preview", payloadId: details["_id"] })
            Object.keys(installmentFormData).forEach(tab => {
                installmentFormData[tab].forEach(task => {
                    if (task['name'] == 'Id') {
                        task['defaultValue'] = details.displayName
                        task['readOnly'] = true
                    }
                    if (task['name'] == 'title') {
                        task['defaultValue'] = details.title
                        task['readOnly'] = false
                    }
                    if (task['name'] == 'description') {
                        task['defaultValue'] = details.description
                        task['readOnly'] = false
                    }
                    if (task['name'] == 'annualAmount') {
                        task['defaultValue'] = Number(details.feeDetails.totalAmount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })
                        task['readOnly'] = false
                    }
                    // ------------- Payment Schedule --------
                    if (task['name'] == 'PaymentScheduleOption') { // options // defaultValue
                        task['defaultValue'] = details.paymentSchedule === undefined ? "" : details.paymentSchedule.displayName
                        task['readOnly'] = false
                        task['required'] = false
                        task['requiredBoolean'] = false
                    }
                    if (task['name'] == 'PaymentScheduleTitle') { // title
                        task['defaultValue'] = details.paymentSchedule === undefined ? "" : details.paymentSchedule.title
                        task['readOnly'] = true
                    }
                    if (task['name'] == 'PaymentScheduleDescription') { // description
                        task['defaultValue'] = details.paymentSchedule === undefined ? "" : details.paymentSchedule.description
                        task['readOnly'] = true
                    }
                    // ------------- Reminder ----------------- 
                    if (task['name'] == 'ReminderPlanOption') { // options // defaultValue
                        task['defaultValue'] = details.paymentSchedule === undefined ? "" : details.reminderPlan.displayName
                        task['readOnly'] = false
                        task['required'] = false
                        task['requiredBoolean'] = false
                    }
                    if (task['name'] == 'ReminderPlanTitle') { // title
                        task['defaultValue'] = details.paymentSchedule === undefined ? "" : details.reminderPlan.title
                        task['readOnly'] = true
                    }
                    if (task['name'] == 'ReminderPlanDescription') { // description
                        task['defaultValue'] = details.paymentSchedule === undefined ? "" : details.reminderPlan.description
                        task['readOnly'] = true
                    }
                    // ------------- Late fees -----------------    
                    if (task['name'] == 'LateFeesOptions') { // options // defaultValue
                        task['defaultValue'] = details.paymentSchedule === undefined ? "" : details.lateFeePlan.displayName
                        task['readOnly'] = false
                        task['required'] = false
                        task['requiredBoolean'] = false
                    }
                    if (task['name'] == 'LateFeesTitle') { // title
                        task['defaultValue'] = details.paymentSchedule === undefined ? "" : details.lateFeePlan.title
                        task['readOnly'] = true
                    }
                    if (task['name'] == 'LateFeesDescription') { // description
                        task['defaultValue'] = details.paymentSchedule === undefined ? "" : details.lateFeePlan.description
                        task['readOnly'] = true
                    }
                    // ------------- Fee type ----------------- 
                    if (task['name'] == 'FeetypeOption') { // options // defaultValue
                        task['defaultValue'] = details.paymentSchedule === undefined ? "" : details.feeType.displayName
                        task['readOnly'] = false
                        task['required'] = false
                        task['requiredBoolean'] = false
                    }
                    if (task['name'] == 'FeetypeTitle') { // title
                        task['defaultValue'] = details.paymentSchedule === undefined ? "" : details.feeType.title
                        task['readOnly'] = true
                    }
                    if (task['name'] == 'FeetypeDescription') { // description
                        task['defaultValue'] = details.paymentSchedule === undefined ? "" : details.feeType.description
                        task['readOnly'] = true
                    }
                    // ------------- Program plan ----------------- 
                    if (task['name'] == 'ProgramPlanOption') { // options // defaultValue
                        task['defaultValue'] = details.paymentSchedule === undefined ? "" : details.programPlan.displayName
                        task['readOnly'] = false
                        task['required'] = false
                        task['requiredBoolean'] = false
                    }
                    if (task['name'] == 'ProgramPlanTitle') { // title
                        task['defaultValue'] = details.paymentSchedule === undefined ? "" : details.programPlan.title
                        task['readOnly'] = true
                    }
                    if (task['name'] == 'ProgramPlanDescription') { // description
                        task['defaultValue'] = details.paymentSchedule === undefined ? "" : details.programPlan.description
                        task['readOnly'] = true
                    }
                })
            })
            this.setState({ studentsFormjson: installmentFormData, loaderStatus: false, viewFormLoad: false })
        }
        else { }
    }
    onInputChanges = (value, item, event, dataS) => {
        if (dataS == undefined) { }
        else {
            if (this.state.tableViewMode == "addNew") {
                if (dataS.name == "PaymentScheduleOption" || dataS.name == "ReminderPlanOption" || dataS.name == "LateFeesOptions" || dataS.name == "FeetypeOption" || dataS.name == "ProgramPlanOption") {
                    let AddInstallmentFormData = InstallmentJson.AddNewFeeManager;
                    let printData = [];
                    dataS.options.map((data) => {
                        if (data.label === value) {
                            printData = data;
                        }
                    })
                    if (printData.typeOflist === "programPlan") {
                        this.setState({ AddInstallmentFormData: [] }, () => {
                            Object.keys(InstallmentJson.AddNewFeeManager).forEach(tab => {
                                InstallmentJson.AddNewFeeManager[tab].forEach(task => {
                                    if (task['name'] == 'ProgramPlanTitle') { // program plan title
                                        task['defaultValue'] = printData.allData.title
                                        task['readOnly'] = true
                                    }
                                    if (task['name'] == 'ProgramPlanDescription') { // program plan description
                                        task['defaultValue'] = printData.allData.description
                                        task['readOnly'] = true
                                    }
                                })
                                let addDatas = this.state.addNewFeePayloadId;
                                addDatas['programPln'] = printData.allData._id
                                this.setState({ AddInstallmentFormData: InstallmentJson.AddNewFeeManager, addNewFeePayloadId: addDatas })
                            })
                        })
                    }
                    if (printData.typeOflist === "feeType") {
                        this.setState({ AddInstallmentFormData: [] }, () => {
                            Object.keys(InstallmentJson.AddNewFeeManager).forEach(tab => {
                                InstallmentJson.AddNewFeeManager[tab].forEach(task => {
                                    if (task['name'] == 'FeetypeTitle') { // fee type title
                                        task['defaultValue'] = printData.allData.title
                                        task['readOnly'] = true
                                    }
                                    if (task['name'] == 'FeetypeDescription') { // fee type description
                                        task['defaultValue'] = printData.allData.description
                                        task['readOnly'] = true
                                    }
                                })
                                let addDatas = this.state.addNewFeePayloadId;
                                addDatas['feeTyp'] = printData.allData._id
                                this.setState({ AddInstallmentFormData: InstallmentJson.AddNewFeeManager, addNewFeePayloadId: addDatas })
                            })
                        })
                    }
                    if (printData.typeOflist === "paymentSchedule") {
                        this.setState({ AddInstallmentFormData: [] }, () => {
                            Object.keys(InstallmentJson.AddNewFeeManager).forEach(tab => {
                                InstallmentJson.AddNewFeeManager[tab].forEach(task => {
                                    if (task['name'] == 'PaymentScheduleTitle') { // payment schedule title
                                        task['defaultValue'] = printData.allData.title
                                        task['readOnly'] = true
                                    }
                                    if (task['name'] == 'PaymentScheduleDescription') { // payment schedule description
                                        task['defaultValue'] = printData.allData.description
                                        task['readOnly'] = true
                                    }
                                })
                                let addDatas = this.state.addNewFeePayloadId;
                                addDatas['paymentSch'] = printData.allData._id
                                this.setState({ AddInstallmentFormData: InstallmentJson.AddNewFeeManager, addNewFeePayloadId: addDatas })
                            })
                        })
                    }
                    if (printData.typeOflist === "reminderPlan") {
                        this.setState({ AddInstallmentFormData: [] }, () => {
                            Object.keys(InstallmentJson.AddNewFeeManager).forEach(tab => {
                                InstallmentJson.AddNewFeeManager[tab].forEach(task => {
                                    if (task['name'] == 'ReminderPlanTitle') { // reminder plan title
                                        task['defaultValue'] = printData.allData.title
                                        task['readOnly'] = true
                                    }
                                    if (task['name'] == 'ReminderPlanDescription') { // reminder plan description
                                        task['defaultValue'] = printData.allData.description
                                        task['readOnly'] = true
                                    }
                                })
                                let addDatas = this.state.addNewFeePayloadId;
                                addDatas['reminderPln'] = printData.allData._id
                                this.setState({ AddInstallmentFormData: InstallmentJson.AddNewFeeManager, addNewFeePayloadId: addDatas })
                            })
                        })
                    }
                    if (printData.typeOflist === "lateFee") {
                        this.setState({ AddInstallmentFormData: [] }, () => {
                            Object.keys(InstallmentJson.AddNewFeeManager).forEach(tab => {
                                InstallmentJson.AddNewFeeManager[tab].forEach(task => {
                                    if (task['name'] == 'LateFeesTitle') { // late fee title
                                        task['defaultValue'] = printData.allData.title
                                        task['readOnly'] = true
                                    }
                                    if (task['name'] == 'LateFeesDescription') { // late fee description
                                        task['defaultValue'] = printData.allData.description
                                        task['readOnly'] = true
                                    }
                                })
                                let addDatas = this.state.addNewFeePayloadId;
                                addDatas['lateFee'] = printData.allData._id
                                this.setState({ AddInstallmentFormData: InstallmentJson.AddNewFeeManager, addNewFeePayloadId: addDatas })
                            })
                        })
                    }
                }
            }
            else if (this.state.tableViewMode == "preview") {
                if (dataS.name == "PaymentScheduleOption" || dataS.name == "ReminderPlanOption" || dataS.name == "LateFeesOptions" || dataS.name == "FeetypeOption" || dataS.name == "ProgramPlanOption") {
                    let AddInstallmentFormData = InstallmentJson.feeManager;
                    let printData = [];
                    dataS.options.map((data) => {
                        if (data.label === value) {
                            printData = data;
                        }
                    })
                    if (printData.typeOflist === "programPlan") {
                        this.setState({ AddInstallmentFormData: [] }, () => {
                            Object.keys(InstallmentJson.feeManager).forEach(tab => {
                                InstallmentJson.feeManager[tab].forEach(task => {
                                    if (task['name'] == 'ProgramPlanTitle') { // program plan title
                                        task['defaultValue'] = printData.allData.title
                                        task['readOnly'] = true
                                    }
                                    if (task['name'] == 'ProgramPlanDescription') { // program plan description
                                        task['defaultValue'] = printData.allData.description
                                        task['readOnly'] = true
                                    }
                                })
                                let addDatas = this.state.addNewFeePayloadId;
                                addDatas['programPln'] = printData.allData._id
                                this.setState({ AddInstallmentFormData: InstallmentJson.feeManager, addNewFeePayloadId: addDatas })
                            })
                        })
                    }
                    if (printData.typeOflist === "feeType") {
                        this.setState({ AddInstallmentFormData: [] }, () => {
                            Object.keys(InstallmentJson.feeManager).forEach(tab => {
                                InstallmentJson.feeManager[tab].forEach(task => {
                                    if (task['name'] == 'FeetypeTitle') { // fee type title
                                        task['defaultValue'] = printData.allData.title
                                        task['readOnly'] = true
                                    }
                                    if (task['name'] == 'FeetypeDescription') { // fee type description
                                        task['defaultValue'] = printData.allData.description
                                        task['readOnly'] = true
                                    }
                                })
                                let addDatas = this.state.addNewFeePayloadId;
                                addDatas['feeTyp'] = printData.allData._id
                                this.setState({ AddInstallmentFormData: InstallmentJson.feeManager, addNewFeePayloadId: addDatas })
                            })
                        })
                    }
                    if (printData.typeOflist === "paymentSchedule") {
                        this.setState({ AddInstallmentFormData: [] }, () => {
                            Object.keys(InstallmentJson.feeManager).forEach(tab => {
                                InstallmentJson.feeManager[tab].forEach(task => {
                                    if (task['name'] == 'PaymentScheduleTitle') { // payment schedule title
                                        task['defaultValue'] = printData.allData.title
                                        task['readOnly'] = true
                                    }
                                    if (task['name'] == 'PaymentScheduleDescription') { // payment schedule description
                                        task['defaultValue'] = printData.allData.description
                                        task['readOnly'] = true
                                    }
                                })
                                let addDatas = this.state.addNewFeePayloadId;
                                addDatas['paymentSch'] = printData.allData._id
                                this.setState({ AddInstallmentFormData: InstallmentJson.feeManager, addNewFeePayloadId: addDatas })
                            })
                        })
                    }
                    if (printData.typeOflist === "reminderPlan") {
                        this.setState({ AddInstallmentFormData: [] }, () => {
                            Object.keys(InstallmentJson.feeManager).forEach(tab => {
                                InstallmentJson.feeManager[tab].forEach(task => {
                                    if (task['name'] == 'ReminderPlanTitle') { // reminder plan title
                                        task['defaultValue'] = printData.allData.title
                                        task['readOnly'] = true
                                    }
                                    if (task['name'] == 'ReminderPlanDescription') { // reminder plan description
                                        task['defaultValue'] = printData.allData.description
                                        task['readOnly'] = true
                                    }
                                })
                                let addDatas = this.state.addNewFeePayloadId;
                                addDatas['reminderPln'] = printData.allData._id
                                this.setState({ AddInstallmentFormData: InstallmentJson.feeManager, addNewFeePayloadId: addDatas })
                            })
                        })
                    }
                    if (printData.typeOflist === "lateFee") {
                        this.setState({ AddInstallmentFormData: [] }, () => {
                            Object.keys(InstallmentJson.feeManager).forEach(tab => {
                                InstallmentJson.feeManager[tab].forEach(task => {
                                    if (task['name'] == 'LateFeesTitle') { // late fee title
                                        task['defaultValue'] = printData.allData.title
                                        task['readOnly'] = true
                                    }
                                    if (task['name'] == 'LateFeesDescription') { // late fee description
                                        task['defaultValue'] = printData.allData.description
                                        task['readOnly'] = true
                                    }
                                })
                                let addDatas = this.state.addNewFeePayloadId;
                                addDatas['lateFee'] = printData.allData._id
                                this.setState({ AddInstallmentFormData: InstallmentJson.feeManager, addNewFeePayloadId: addDatas })
                            })
                        })
                    }
                }
            }
        }
    }
    onPreviewStudentList1 = (e) => {
        console.log(e);
    }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            console.log(page, limit);
            this.getTableData()
        });
    };
    moveBackTable = () => {
        this.setState({ tabViewForm: false, tableView: false })
        this.resetform();
    }
    handleTabChange = (tabName, formDatas) => { }
    onFormSubmit = (data, item) => {
        console.log('submit clicked', data, item);
        let payloadData = {
            "displayName": "",
            "title": "",
            "description": "",
            "feeTypeId": this.state.addNewFeePayloadId.feeTyp,
            "programPlanId": this.state.addNewFeePayloadId.programPln,
            "reminderPlanId": this.state.addNewFeePayloadId.reminderPln,
            "paymentScheduleId": this.state.addNewFeePayloadId.paymentSch,
            "concessionPlanId": null,
            "lateFeePlanId": this.state.addNewFeePayloadId.lateFee,
            "installmentPlanId": null,
            "feeDetails": {
                "units": null,
                "perUnitAmount": null,
                "totalAmount": ""
            },
            "status": "active",
            "createdBy": localStorage.getItem('baseURL')
        }
        if (this.state.tableViewMode == "addNew") {
            this.setState({ LoaderStatus: true }, () => {

            })
            let installmentFormData = InstallmentJson.AddNewFeeManager;
            Object.keys(installmentFormData).forEach(tab => {
                installmentFormData[tab].forEach(task => {
                    if (task['name'] == 'Id') {
                        payloadData.displayName = task['defaultValue']
                    }
                    if (task['name'] == 'title') {
                        payloadData.title = task['defaultValue']
                    }
                    if (task['name'] == 'description') {
                        payloadData.description = task['defaultValue']
                    }
                    if (task['name'] == 'annualAmount') {
                        payloadData.feeDetails.totalAmount = Number(task['defaultValue'])
                    }
                })
            })
            let cancelPayload = { programPln: "", feeTyp: "", paymentSch: "", reminderPln: "", lateFee: "" }
            let headers = {
                'Authorization': this.state.authToken
            };
            axios.post(`${this.state.env["zqBaseUri"]}/edu/feesManager`, payloadData, { headers })
                .then(res => {
                    console.log(res);
                    if (res.data.type === "success") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Added Successfully", status: "success", }
                        this.setState({ addNewFeePayloadId: cancelPayload, snackbar: snackbarUpdate, LoaderStatus: false })
                        this.getTableData()
                        this.cancelNotification()
                    }
                    else if (res.data.type === "error") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Failed to add fee manager", status: "error", }
                        this.setState({ addNewFeePayloadId: cancelPayload, snackbar: snackbarUpdate, LoaderStatus: false })
                        this.getTableData()
                        this.cancelNotification()
                    }
                })
                .catch(err => {
                    console.log(err);
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Error", status: "error" }
                    this.setState({ snackbar: snackbarUpdate, tabViewForm: false, tableView: false, LoaderStatus: false })
                    this.cancelNotification()
                })
        }
        else if (this.state.tableViewMode == "preview") {
            let installmentFormData = InstallmentJson.feeManager;
            Object.keys(installmentFormData).forEach(tab => {
                installmentFormData[tab].forEach(task => {
                    if (task['name'] == 'Id') {
                        payloadData.displayName = task['defaultValue']
                    }
                    if (task['name'] == 'title') {
                        payloadData.title = task['defaultValue']
                    }
                    if (task['name'] == 'description') {
                        payloadData.description = task['defaultValue']
                    }
                    if (task['name'] == 'annualAmount') {
                        let a = task['defaultValue'].replace('₹', '');
                        let b = a.replace(/,/g, '');
                        payloadData.feeDetails.totalAmount = Number(b)
                    }
                })
            })
            let headers = {
                'Authorization': this.state.authToken
            };
            let cancelPayload = { programPln: "", feeTyp: "", paymentSch: "", reminderPln: "", lateFee: "" }
            axios.put(`${this.state.env["zqBaseUri"]}/edu/feesManager/${this.state.orgId}`, payloadData, { headers })
                .then(res => {
                    console.log(res);
                    if (res.data.status === "success") {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Updated Successfully", status: "success", }
                        this.setState({ addNewFeePayloadId: cancelPayload, snackbar: snackbarUpdate })
                        this.getTableData()
                        this.cancelNotification()
                    }
                    else {
                        let snackbarUpdate = { openNotification: true, NotificationMessage: "Failed to Update fee manager", status: "error", }
                        this.setState({ addNewFeePayloadId: cancelPayload, snackbar: snackbarUpdate })
                        this.getTableData()
                        this.cancelNotification()
                    }
                })
                .catch(err => {
                    console.log(err);
                    let snackbarUpdate = { openNotification: true, NotificationMessage: "Failed to Update fee manager", status: "error", }
                    this.setState({ addNewFeePayloadId: cancelPayload, snackbar: snackbarUpdate })
                    this.getTableData()
                    this.cancelNotification()
                })
        }
    }
    cancelNotification = () => {
        setTimeout(() => {
            let snackbarUpdate = this.state.snackbar;
            snackbarUpdate.openNotification = false
            this.setState({ snackbar: snackbarUpdate, tabViewForm: false, tableView: false })
        }, 1500)
    }
    closeNotification = () => {
        let snackbarUpdate = { openNotification: false, NotificationMessage: "", status: "" }
        this.setState({ snackbar: snackbarUpdate })
    }
    resetform = () => {
        let installmentFormData = InstallmentJson.feeManager;
        let AddInstallmentFormData = InstallmentJson.AddNewFeeManager;
        Object.keys(installmentFormData).forEach(tab => {
            installmentFormData[tab].forEach(task => {
                delete task['defaultValue']
                task['readOnly'] = false
                task['error'] = false
                task['required'] = task['requiredBoolean']
                task['validation'] = false
                delete task['clear']
            })
        })
        Object.keys(AddInstallmentFormData).forEach(tab => {
            AddInstallmentFormData[tab].forEach(task => {
                delete task['defaultValue']
                task['readOnly'] = false
                task['error'] = false
                task['required'] = task['requiredBoolean']
                task['validation'] = false
                delete task['clear']
            })
        })
        this.setState({ studentsFormjson: installmentFormData, payloadId: "", status: "" })
    }
    cleanDatas = () => { }
    render() {
        return (
            <div className="list-of-students-mainDiv fee-manager-new-input">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.tabViewForm == false ?
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Fee Inventory</p>
                        </div>
                        <div className="masters-body-div">
                            {this.state.containerNav == undefined ? null :
                                <React.Fragment>
                                    <ContainerNavbar containerNav={this.state.containerNav} onAddNew={() => { this.onAddFeeManager(); }} />
                                    <div className="print-time">{this.state.printTime}</div>
                                </React.Fragment>}
                            <div className="remove-last-child-table">
                                {this.state.sampleStudentsData.length !== 0 ?
                                    <ZqTable
                                        data={this.state.sampleStudentsData}
                                        rowClick={(item) => { this.onPreviewStudentList(item) }}
                                        onRowCheckBox={(item) => { this.onPreviewStudentList1(item) }}
                                    />
                                    : <p className="noprog-txt">{this.state.tableResTxt}</p>
                                }
                            </div>
                            <div>
                                {this.state.sampleStudentsData.length !== 0 ?
                                    <PaginationUI
                                        total={this.state.totalRecord}
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        currentPage={this.state.page}
                                    /> : null}
                            </div>
                        </div>
                    </React.Fragment> :
                    (this.state.tableViewMode == "addNew" ?
                        <React.Fragment>
                            <div className="tab-form-wrapper tab-table">
                                <div className="preview-btns">
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| Fee Manager | {this.state.addNewId}</h6>
                                    </div>
                                </div>
                                <div className="organisation-table">
                                    {this.state.viewFormLoad === false ? <React.Fragment>
                                        <ZenTabs tabData={this.state.AddInstallmentFormData} cleanData={this.cleanDatas} className="preview-wrap" tabEdit={this.state.preview} form={this.state.AddInstallmentFormData} cancelViewData={this.cancelViewData} value={0} onInputChanges={this.onInputChanges} onFormBtnEvent={(item) => { this.formBtnHandle(item); }} onTabFormSubmit={this.onFormSubmit} handleTabChange={this.handleTabChange} key={0} />
                                    </React.Fragment> : <p className="noprog-txt">{this.state.tableResTxt}</p>}
                                </div>
                            </div>
                        </React.Fragment> :
                        this.state.tableViewMode == "preview" ?
                            <React.Fragment>
                                <div className="tab-form-wrapper tab-table">
                                    <div className="preview-btns">
                                        <div className="preview-header-wrap">
                                            < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                            <h6 className="preview-Id-content" onClick={this.moveBackTable}>| Fee Manager | {this.state.previewID}</h6>
                                        </div>
                                    </div>
                                    <div className="organisation-table">
                                        {this.state.viewFormLoad === false ?
                                            <ZenTabs tabData={InstallmentJson.feeManager} className="preview-wrap" cleanData={this.cleanDatas} tabEdit={this.state.preview} form={InstallmentJson.feeManager} cancelViewData={this.cancelViewData} value={0} onInputChanges={this.onInputChanges} onFormBtnEvent={(item) => { this.formBtnHandle(item); }} onTabFormSubmit={this.onFormSubmit} handleTabChange={this.handleTabChange} key={0} />
                                            : <p className="noprog-txt">{this.state.tableResTxt}</p>}
                                    </div>
                                </div>
                            </React.Fragment> : null)}
                <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={2500} onClose={this.closeNotification}>
                    <Alert onClose={this.closeNotification}
                        severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
                        {this.state.snackbar.NotificationMessage}
                    </Alert>
                </Snackbar>
            </div>
        )
    }
}
export default FeesManager;