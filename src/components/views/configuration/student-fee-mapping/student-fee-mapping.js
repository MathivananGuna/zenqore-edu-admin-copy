import React, { Component } from "react";
import '../../../../scss/student.scss';
import '../../../../scss/setup.scss';
import Loader from "../../../../utils/loader/loaders";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ContainerNavbar from "../../../../gigaLayout/container-navbar";
import ZqTable from "../../../../utils/Table/table-component";
import PaginationUI from "../../../../utils/pagination/pagination";
import ZenTabs from '../../../input/tabs';
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import InstallmentJson from './student-fee.json';
import axios from "axios";
import moment from 'moment';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Installment from "../../setup/masters/installment/installment";
import { ButtonGroup, ButtonToolbar, CheckTree, Modal, Drawer, Timeline, TreePicker, Icon } from 'rsuite';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import CircularProgress from '@material-ui/core/CircularProgress';
import xlsx from 'xlsx';

import DateFormatter from '../../../date-formatter/date-formatter'
import DateFormatContext from '../../../../gigaLayout/context'
function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}
class FeesManager extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            channel: localStorage.getItem('channel'),
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            campusId: localStorage.getItem('campusId') === "null" || localStorage.getItem('campusId') === null ? undefined : localStorage.getItem('campusId'),
            containerNav: InstallmentJson.containerNav,
            studentsFormjson: InstallmentJson.studentsFormjson,
            sampleStudentsData: [],
            page: 1,
            limit: 10,
            totalRecord: 0,
            totalPages: 1,
            tabViewForm: false,
            studentName: "",
            tableResTxt: "Fetching Data...",
            viewHeader: true,
            totalApiRes: null,
            searchValue: '',
            viewFormLoad: true,
            LoaderStatus: false,
            feesBreakUpArray: [],
            tableViewMode: "",
            payloadId: "",
            snackbar: { openNotification: false, NotificationMessage: "", status: "success" },
            modelboxDetails: { openNotificationModel: false, modelHeading: '', modelStatus: "", modelBodyContent: '', modelFooterSubBtn: true, modelFooterCancelBtn: true },
            payloadDetails: null,
            previewId: ""
        }
    }
    static contextType = DateFormatContext;
    componentDidMount() {
        this.getTableData()
    }
    getTableData = () => {
        this.setState({ sampleStudentsData: [], LoaderStatus: true })
        axios({
            method: 'get',
            url: `${this.state.env["zqBaseUri"]}/edu/master/studentFeesMapping?orgId=${this.state.orgId}&campusId=${this.state.campusId}&page=${this.state.page}&limit=${this.state.limit}`,
            headers: {
                'Authorization': this.state.authToken
            }
        })
            .then(res => {
                console.log(res, res.data.data);
                let installmentTableRes = [];
                let regId = this.context.reportLabel || "Reg ID"
                let batch = this.context.classLabel || "CLASS/BATCH"
                if (res.data.status == "success" && res !== {}) {
                    res.data.data.map((data) => {
                        installmentTableRes.push({
                            [regId]: data.regId,
                            "Name": String(data.studentName).replace('.', ''),
                            [batch]: data.programPlanDetails[0].description,
                            "Fees Structure": data.feeStructureId,
                            "Total Fees": Number(data.totalAmount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' }),
                            // "Amount till Date": data["Amount till Date"],
                            "Paid": Number(data.paidAmount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' }),
                            "Pending": data.pendingAmount == null ? "₹0.00" : Number(data.pendingAmount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' }),
                            "Campus Name": data.campusIdName,
                            "Created On": moment(new Date(data.createdAt)).format(this.context.dateFormat),
                            "Created By": data.createdBy,
                            "Item": JSON.stringify(data),
                        })
                    })
                    this.setState({ sampleStudentsData: installmentTableRes, LoaderStatus: false, totalApiRes: res.data.data, tabViewForm: false, totalPages: res.data.totalPages, page: res.data.currentPage })
                }
                else {
                    this.setState({ sampleStudentsData: [], tableResTxt: "No Data", LoaderStatus: false })
                }
            })
            .catch(err => {
                console.log(err)
                this.setState({ sampleStudentsData: [], tableResTxt: "Error Loading Data", LoaderStatus: false })
            })
    }
    onAddInstallment = () => {
        this.setState({ tabViewForm: true, viewHeader: false, viewFormLoad: false, tableViewMode: "addnew", tableResTxt: "Fetching Data..." })

    }
    formatAmount = (amount) => {
        let a = Number(amount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })
        let b = a.replace('₹', "")
        return b
    }
    onDownloadEvent = () => {
        this.setState({ LoaderStatus: true })
        axios.get(`${this.state.env['zqBaseUri']}/edu/master/studentFeesMapping?orgId=${this.state.orgId}&pagination=false`, {
            headers: {
                'Authorization': localStorage.getItem("auth_token")
            }
        })
            .then(resp => {
                console.log("student fee", resp);
                var createXlxsData = [];
                let regId = this.context.reportLabel || "REG ID";
                let batch = this.context.classLabel || "CLASS/BATCH";
                resp.data.data.map(item => {
                    createXlxsData.push({
                        [regId]: item.regId,
                        "STUDENT NAME": item.studentName,
                        [batch]: item.programPlan,
                        "Fees Structure": item.feeStructureId,
                        "TOTAL FEES (INR)": this.formatAmount(item.totalAmount),
                        "PAID (INR)": this.formatAmount(item.paidAmount),
                        "BALANCE (INR)": this.formatAmount(item.pendingAmount),
                        "Created On": moment(new Date(item.createdAt)).format(this.context.dateFormat),
                        "Created By": item.createdBy,
                    })
                    console.log('**DATA**', item)
                })
                var ws = xlsx.utils.json_to_sheet(createXlxsData);
                var wb = xlsx.utils.book_new();
                xlsx.utils.book_append_sheet(wb, ws, "Student Fee Mapping");
                xlsx.writeFile(wb, "Students_fee_mapping.xlsx");
                // xlsx.writeFile(wb, "demand_note_reports.csv");
                this.setState({ LoaderStatus: false })
            })
            .catch(err => {
                console.log(err, 'err0rrrrr')
                this.setState({ LoaderStatus: false })
            })


    }
    handleBackFun = () => {
        this.props.history.push(`${localStorage.getItem('baseURL')}/main/dashboard`);
        this.resetform()
    }

    onPreviewStudentList = (e) => {
        let details = this.state.globalSearch ? e : JSON.parse(e.Item);
        console.log(details);
        this.setState({ tabViewForm: true, previewId: details.displayName, studentName: details.displayName, viewHeader: true, viewFormLoad: false, tableViewMode: "preview", payloadId: details["_id"] })
        let installmentFormData = InstallmentJson.studentsPreviewFormjson;
        // installmentFormData['Student Fee Mapping'] = [];
        installmentFormData['General'].map((task) => {
            if (task['name'] == 'feeStructure') {
                task['defaultValue'] = details.feeStructureId + "-" + details.feeStructure
                task['readOnly'] = true
                task['required'] = false
            }
            if (task['name'] == 'regId') {
                task['label'] = this.context.reportLabel || "Reg ID"
                task['defaultValue'] = details.regId
                task['readOnly'] = true
                task['required'] = false
            }
            if (task['name'] == 'name') {
                task['defaultValue'] = String(details.studentDetails[0].firstName + " " + details.studentDetails[0].lastName).replace('.', '')
                task['readOnly'] = true
                task['required'] = false
            }
            if (task['name'] == 'batchClass') {
                task['label'] = this.context.classLabel || "Class/Batch"
                task['defaultValue'] = details.programPlanDetails[0].description
                task['readOnly'] = true
                task['required'] = false
            }
            if (task['name'] == 'programPlan') {
                task['defaultValue'] = details.programPlanDetails[0].title
                // task['defaultValue'] = details.programPlanDetails.programCode
                task['readOnly'] = true
                task['required'] = false
            }
            if (task['name'] == 'campusName') {
                task['defaultValue'] = "-"
                task['readOnly'] = true
                task['required'] = false
            }
            if (task['name'] == 'total') {
                task['defaultValue'] = this.formatCurrency(Number(details.totalAmount))
                task['readOnly'] = true
                task['required'] = false
            }
        })
        this.setState({ studentsFormjson: installmentFormData })
    }
    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    onInputChanges = (value, item, event, dataS) => {
        console.log(value, item, event, dataS);
    }
    onPreviewStudentList1 = (e) => {
        console.log(e);
    }
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit }, () => {
            console.log(page, limit);
            if (this.state.searchValue.length == 0) {
                this.getTableData()
            }
            else {
                this.searchHandle(this.state.searchValue)
            }
        });
    };
    moveBackTable = () => {
        this.setState({ tabViewForm: false })
        this.resetform()
    }
    searchHandle = (searchValue) => {
        this.setState({ LoaderStatus: true, sampleStudentsData: [], tableResTxt: "Fetching Data" })
        console.log('searchValue', searchValue)

        if (String(searchValue).length > 0) {
            this.setState({ searchValue: searchValue });
            return axios.get(`${this.state.env['zqBaseUri']}/edu/master/studentFeesMapping?orgId=${this.state.orgId}&page=${this.state.page}&limit=${this.state.limit}&searchKey=${searchValue}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token")
                }
            })
                .then(res => {
                    let installmentTableRes = [];
                    let regId = this.context.reportLabel || "Reg ID"
                    let batch = this.context.classLabel || "CLASS/BATCH"
                    if (res.data.status == "success") {
                        res.data.data.map((data) => {
                            installmentTableRes.push({
                                [regId]: data.regId,
                                "Name": String(data.studentName).replace('.', ''),
                                [batch]: data.programPlanDetails[0].description,
                                "Fees Structure": data.feeStructureId,
                                "Total Fees": Number(data.totalAmount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' }),
                                // "Amount till Date": data["Amount till Date"],
                                "Paid": Number(data.paidAmount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' }),
                                "Pending": data.pendingAmount == null ? "₹0.00" : Number(data.pendingAmount).toLocaleString('en-IN', { style: 'currency', currency: 'INR' }),
                                "Campus Name": data.campusIdName,
                                "Created On": moment(new Date(data.createdAt)).format(this.context.dateFormat),
                                "Created By": data.createdBy,
                                "Item": JSON.stringify(data),
                            })
                        })
                        this.setState({ sampleStudentsData: installmentTableRes, tabViewForm: false, page: res.data.currentPage, totalPages: res.data.totalPages, })
                        this.setState({ LoaderStatus: false })
                    }
                })
                .catch(err => {
                    console.log(err);
                    this.setState({ LoaderStatus: false, sampleStudentsData: [], tabViewForm: false, tableResTxt: "No Data" })
                })

        } else {
            this.getTableData()
        }
    }
    handleTabChange = (tabName, formDatas) => {

    }
    onFormSubmit = (data, item) => {

    }
    closeNotification = () => {
        let snackbarUpdate = { openNotification: false, NotificationMessage: "", status: "" }
        this.setState({ snackbar: snackbarUpdate })
    }
    resetform = () => {
        let installmentFormData = InstallmentJson.studentsFormjson;
        Object.keys(installmentFormData).forEach(tab => {
            installmentFormData[tab].forEach(task => {
                delete task['defaultValue']
                task['readOnly'] = false
                task['error'] = false
                task['required'] = task['requiredBoolean']
                task['validation'] = false
                delete task['clear']
            })
        })
        this.setState({ studentsFormjson: installmentFormData, payloadId: "", status: "" })
    }
    handleActionClick = (item, index, hd, name) => {
        // console.log(item, index, hd, name);
        let a = this.state.modelboxDetails;
        a.openNotificationModel = true;
        a.modelHeading = 'Confirm';
        a.modelBodyContent = 'Are you sure want to send Demand Note?';
        a.modelFooterSubBtn = true;
        a.modelFooterCancelBtn = true;
        a.modelStatus = "";
        this.setState({ modelboxDetails: a })
        let payloadDetails = JSON.parse(item.Item);
        if (name === "Send demand Note") {
            console.log("Demand note", JSON.parse(item.Item));
            const totalAmount = payloadDetails.feeDetails.reduce((a, b) => a + b.annualAmount, 0);
            let payloadData = {
                "displayName": "",
                "transactionType": "",
                "transactionSubType": "",
                "transactionDate": "",
                "studentId": payloadDetails.studentId,
                "studentRegId": payloadDetails.studentDetails.regId,
                "studentName": payloadDetails.studentDetails.firstName + " " + String(payloadDetails.studentDetails.lastName).replace('.', ''),
                "class": payloadDetails.programPlanDetails.title,
                "academicYear": payloadDetails.programPlanDetails.academicYear,
                "programPlan": payloadDetails.programPlanDetails.programCode,
                "amount": payloadDetails.pendingAmount,
                "dueDate": payloadDetails.dueDate.dueDate,
                "emailCommunicationRefIds": payloadDetails.guardianDetails[0].email,
                "smsCommunicationRefIds": payloadDetails.guardianDetails[0].mobile,
                "status": "",
                "relatedTransactions": [],
                "data": {
                    "orgId": this.state.orgId,
                    "displayName": "",
                    "studentId": payloadDetails.studentId,
                    "studentRegId": payloadDetails.studentDetails.regId,
                    "class": payloadDetails.programPlanDetails.title,
                    "academicYear": payloadDetails.programPlanDetails.academicYear,
                    "programPlan": payloadDetails.programPlanDetails.displayName,
                    "issueDate": "",
                    "dueDate": "",
                    "feesBreakUp": [
                        {
                            "feeTypeId": payloadDetails.feeDetails[0]._id,
                            "feeTypeCode": payloadDetails.feeDetails[0].displayName,
                            "amount": payloadDetails.feeDetails[0].annualAmount,
                            "feeType": payloadDetails.feeDetails[0].title
                        }
                    ]
                },
                "createdBy": this.state.orgId,
                "studentFeeMapId": payloadDetails.displayName,
            }
            console.log(payloadData);
            this.setState({ payloadDetails: payloadData })
        }
    }
    cancelAddMapModel = () => {
        let a = this.state.modelboxDetails;
        a.openNotificationModel = false;
        a.modelHeading = '';
        a.modelBodyContent = '';
        a.modelFooterSubBtn = false;
        a.modelFooterCancelBtn = false;
        a.modelStatus = "";
        this.setState({ modelboxDetails: a })
    }
    confirmSendDemandNote = () => {
        let a = this.state.modelboxDetails;
        let b = this.state.snackbar;
        a.modelFooterSubBtn = false;
        a.modelStatus = "loading";
        a.modelBodyContent = 'Sending Demand Note';
        this.setState({ modelboxDetails: a })
        let headers = {
            'client': "ken42",
            'Authorization': this.state.authToken
        };
        console.log(this.state.payloadDetails);
        axios.post(`${this.state.env["zqBaseUri"]}/edu/demandNote`, this.state.payloadDetails, { headers })
            .then(res => {
                console.log(res);
                a.modelHeading = 'Status';
                a.modelBodyContent = 'Demand Note Sent Successfully';
                a.modelFooterSubBtn = false;
                a.modelStatus = "success";
                setTimeout(() => {
                    a.openNotificationModel = false;
                    this.setState({ modelboxDetails: a })
                }, 1000)
                this.setState({ modelboxDetails: a, snackbar: b })
            })
            .catch(err => {
                a.modelHeading = 'Status';
                a.modelBodyContent = 'Failed to send';
                a.modelFooterSubBtn = false;
                a.modelStatus = "error";
                setTimeout(() => {
                    a.openNotificationModel = false;
                    this.setState({ modelboxDetails: a })
                }, 1000)
                this.setState({ modelboxDetails: a, snackbar: b })
                console.log(err);
            })
    }
    render() {
        return (
            <div className="list-of-students-mainDiv">
                {this.state.LoaderStatus == true ? <Loader /> : null}
                {this.state.tabViewForm == false ?
                    <React.Fragment>
                        <div className="trial-balance-header-title">
                            <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }} style={{ marginRight: 5, cursor: 'pointer', marginTop: 2 }} />
                            <p className="top-header-title" onClick={() => { this.props.history.push(`/${localStorage.getItem('baseURL')}/main/dashboard`) }}>| Student Fee Mapping</p>
                        </div>
                        <div className="masters-body-div">
                            {this.state.containerNav == undefined ? null :
                                <React.Fragment>
                                    <ContainerNavbar containerNav={this.state.containerNav} onDownload={() => this.onDownloadEvent()} searchValue={(searchValue) => this.searchHandle(searchValue)} onAddNew={() => { this.onAddInstallment(); }} />
                                    <div className="print-time">{this.state.printTime}</div>
                                </React.Fragment>}
                            <div className="remove-last-child-table">
                                {this.state.sampleStudentsData.length !== 0 ?
                                    <ZqTable
                                        data={this.state.sampleStudentsData}
                                        rowClick={(item) => { this.onPreviewStudentList(item) }}
                                        onRowCheckBox={(item) => { this.onPreviewStudentList1(item) }}
                                        handleActionClick={(item, index, hd, name) => { this.handleActionClick(item, index, hd, name) }}
                                    />
                                    : <p className="noprog-txt">{this.state.tableResTxt}</p>
                                }
                            </div>
                            <div>
                                {this.state.sampleStudentsData.length !== 0 ?
                                    <PaginationUI
                                        total={this.state.totalRecord}
                                        onPaginationApi={this.onPaginationChange}
                                        totalPages={this.state.totalPages}
                                        limit={this.state.limit}
                                        currentPage={this.state.page}
                                    />
                                    : null}
                            </div>
                        </div>
                    </React.Fragment> :
                    <React.Fragment>
                        <div className="tab-form-wrapper tab-table">
                            <div className="preview-btns">
                                {this.state.viewHeader == true ?
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| Student Fee Mapping | {this.state.previewId}</h6>
                                    </div> :
                                    <div className="preview-header-wrap">
                                        < KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={this.moveBackTable} style={{ marginRight: 5, cursor: 'pointer', marginTop: 1 }} />
                                        <h6 className="preview-Id-content" onClick={this.moveBackTable}>| Add New</h6>
                                    </div>
                                }
                            </div>
                            <div className="organisation-table">
                                {this.state.viewFormLoad === false ?
                                    <ZenTabs tabData={this.state.studentsFormjson} className="preview-wrap" tabEdit={this.state.preview} cancelViewData={this.moveBackTable} form={this.state.studentsFormjson} value={0} onInputChanges={this.onInputChanges} onFormBtnEvent={(item) => { this.formBtnHandle(item); }} onTabFormSubmit={this.onFormSubmit} handleTabChange={this.handleTabChange} key={0} />
                                    : <p className="noprog-txt">{this.state.tableResTxt}</p>}
                            </div>
                        </div>
                    </React.Fragment>
                }
                <Snackbar open={this.state.snackbar.openNotification} autoHideDuration={6000} onClose={this.closeNotification}>
                    <Alert onClose={this.closeNotification}
                        severity={this.state.snackbar.status === "success" ? "success" : this.state.snackbar.status === "error" ? "error" : "success"}>
                        {this.state.snackbar.NotificationMessage}
                    </Alert>
                </Snackbar>
                <Modal className="coa-model-box" style={{ top: '16%', borderRadius: '2px' }} onHide={this.cancelAddMapModel} size="xs" show={this.state.modelboxDetails.openNotificationModel}>
                    <Modal.Header> <Modal.Title>{this.state.modelboxDetails.modelHeading}</Modal.Title> </Modal.Header>
                    <Modal.Body className="bodyContent" style={{ paddingBottom: '10px', marginTop: '10px' }}>
                        <div>
                            {this.state.modelboxDetails.modelBodyContent}
                            {this.state.modelboxDetails.modelStatus == "error" ? <span>{<Icon icon='exclamation-triangle' />}</span> : this.state.modelboxDetails.modelStatus == "success" ? <span><CheckCircleIcon className="check-circle" /></span> : this.state.modelboxDetails.modelStatus == "loading" ? <span><CircularProgress className="circular-loader-coa" /></span> : null}
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        {this.state.modelboxDetails.modelFooterSubBtn == true ?
                            <React.Fragment>
                                <Button className="process-cancel-button" onClick={this.cancelAddMapModel}>Cancel</Button>
                                <Button className="process-confirm-button" onClick={this.confirmSendDemandNote}>Confirm</Button>
                            </React.Fragment> : null}
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
export default FeesManager;