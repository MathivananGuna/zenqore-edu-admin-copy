import React, { Component } from 'react';
import axios from 'axios';
// import zq_api from '../../utils/api-service';
import PublishOutlinedIcon from '@material-ui/icons/PublishOutlined';
import LinearProgress from '@material-ui/core/LinearProgress';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import '../../scss/invoice-template.scss';
import '../../scss/create-invoice.scss';
import '../../scss/extract-pdf.scss';
import Loader from '../../utils/loader/loaders';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
// import pdfURL from './Google_3757242237.pdf'
// import pdfURL from './WDM-Invoice.pdf'
// import pdfURL from './AxisBankStatementFormat.pdf'
import pdfURL from './hdfc_stmt.pdf'
import Viewer, { Worker } from '@phuocng/react-pdf-viewer';
import '@phuocng/react-pdf-viewer/cjs/react-pdf-viewer.css';
// import CreateInvoice from './create-invoice';
import DragResizeContainer from 'react-drag-resize';
// import TransactionForm from '../../utils/transaction/transaction-form';
// import removeCircle from '../../assets/images/remove_circle.png';
// import addCircle from '../../assets/images/add_circle.png';
import { withStyles, Theme, createStyles } from '@material-ui/core/styles';
import Switch, { SwitchClassKey, SwitchProps } from '@material-ui/core/Switch';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import Snackbar from '@material-ui/core/Snackbar';
import gigaHeader from '../views/reconcile/input-password.json';
import ZenForm from '../../components/input/form';
import euroKidsData from '../views/reconcile/rawData.json';
var textractData = require('./textract.json')
// var textractIndex = require('./textractIndex.json')
// var zqMapping = require('./zq_mapping.json')
// var zqTextract = require('./zqextract.json')
// var zqMapping = require('./zq_mapping_AXIS_STMT.json')
// var zqTextract = require('./zq_extract_AXIS_STMT.json')
var zqTextract = require('./J&KBankStatement.json')
var textractformData = new FormData();
const renderPage: RenderPage = (props: RenderPageProps) => (
    <>
        {props.canvasLayer.children}
        <div
            style={{
                alignItems: 'center',
                display: 'flex',
                height: '100%',
                justifyContent: 'center',
                left: 0,
                position: 'absolute',
                top: 0,
                width: '100%',
            }
            }>
            <div
                style={{
                    color: 'rgba(0, 0, 0, 0.2)',
                    fontSize: `${5 * props.scale}rem`,
                    fontWeight: 'bold',
                    textTransform: 'uppercase',
                    transform: 'rotate(-45deg)',
                    userSelect: 'none',
                    textAlign: 'center'
                }}
            >
                {/* ZENQORE TEXTRACT */}
            </div>
        </div>
        {props.annotationLayer.children}
        {props.textLayer.children}
    </>
);


const AntSwitch = withStyles((theme) =>
    createStyles({
        root: {
            width: 28,
            height: 16,
            padding: 0,
            display: 'flex',
        },
        switchBase: {
            padding: 2,
            color: '#0052CC',
            '&$checked': {
                transform: 'translateX(12px)',
                color: '0052CC',
                '& + $track': {
                    opacity: 1,
                    backgroundColor: theme.palette.common.white,
                    borderColor: `1px solid ${theme.palette.grey[500]}`,
                },
            },
        },
        thumb: {
            width: 12,
            height: 12,
            boxShadow: 'none',
        },
        track: {
            border: `1px solid ${theme.palette.grey[500]}`,
            borderRadius: 16 / 2,
            opacity: 1,
            backgroundColor: theme.palette.common.white,
        },
        checked: {},
    }),
)(Switch);

class ExtractPurchaseInvoice extends Component {
    constructor(props) {
        super(props);
        // this.zq_api = new zq_api()
        this.state = {
            env: JSON.parse(localStorage.getItem('env')),
            email: localStorage.getItem('email'),
            authToken: localStorage.getItem("auth_token"),
            orgId: localStorage.getItem('orgId'),
            extractData: false,
            filename: '',
            upload: false,
            showPasswordModal: false,
            pdfUpload: false,
            files: [],
            error: '',
            formData: '',
            fileSize: '',
            pdfLoaded: 0,
            processLoaded: 0,
            pdfName: undefined,
            showViewer: false,
            pdfExtract: false,
            textractData: {},
            textractIndex: {},
            poNo: '',
            textractCompleted: false,
            view: undefined,
            vendor_name: '',
            vendor_address: '',
            vendor_gstin: '',
            invoice_number: '',
            invoice_date: '',
            getProductRow: {},
            vendorInfoPanel: true,
            invoiceInfoPanel: true,
            purchaseInfoPanel: true,
            orgData: undefined,
            igstRemove: true,
            tableRowReady: false,
            expenseId: '',
            templateAdj: undefined,
            withProduct: true,
            subTotal: 0,
            total: 0,
            taxRate: 0,
            taxAmount: 0,
            scale: 1,
            hsn_value: "",
            formRead: true,
            dragWrapWidth: "0px",
            dragWrapHeight: "0px",
            dragDivShow: false,
            isTemplate: true,
            dragItem: "",
            dragIndex: 0,
            createTemplate: {},
            isTableClick: false,
            pageNumber: 1,
            attachmentFileData: "",
            attachmentFileUrl: "",
            isPortal: false,
            portalData: undefined,
            isStatement: false,
            loader: false,
            snackBar: false,
            processMessage: "",
            textractedData: [],
            pdfPwdFlow: false,
            pdfParams: {}
        }
        this.inputFile = React.createRef(this.inputFile)
    }
    componentWillMount() {
        this.setState({ profileData: gigaHeader })
    }
    componentDidMount = () => {
        // this.setState({ expenseId: this.props.location.expenseId })
        let apiDetails = {
            url: `${this.state.env['transactionUri']}/getOrgData`,
            headers: {
                'Content-Type': 'application/json'
            }
        }
        // this.zq_api.get(apiDetails).then(res => {
        //     this.setState({ orgData: res })
        // })
        // let getNextIDpayload = {
        //     "key": this.state.email,
        //     "type": "EXP"
        // }
        // let apiDetails2 = {
        //     url: `${this.state.env['zqBaseUri']}/txn/nextId/2/${localStorage.getItem('orgId')}`
        // }
        // this.zq_api.get(apiDetails2).then(res => {
        //     if (res.failed == undefined) {
        this.setState({ expenseId: null }, () => {
            this.setState({
                templateAdj: {
                    docIdType: "EXP",
                    txtPurposeId: "41",
                    txnPurposeText: "Buy on credit & pay later",
                    type: 'Expense',
                    dueDate: false,
                    shippingAddress: false,
                    eWayDetails: false,
                    itemTransaction: "getExpenseItemDetail",
                    invoiceNo: null,
                    gstPrefilled: false
                }
            })
        })
        //     }
        // })

        // if (this.props.location.portalData != undefined) {
        //     let portalData = this.props.location.portalData
        //     let parseItems = JSON.parse(portalData.previewItem.Item)
        //     const pdfParams = {
        //         "bucket": "textractfiles",
        //         "key": parseItems.fileName
        //     }
        //     this.setState({ filename: parseItems.fileName, upload: true, isPortal: true, portalData: this.props.location.portalData }, () => {
        //         axios.get(`${this.state.env['upload_doc']}/get-pdf-url?bucket=${pdfParams.bucket}&key=${pdfParams.key}`)
        //             .then(response => {
        //                 let data = response.data.url.replace(/"/g, '')
        //                 console.log('url', data)
        //                 this.setState({ showViewer: true, view: "PdfView", attachmentFileUrl: data }, () => {
        //                     console.log('document view 0')
        //                     // let loadingTask = pdfjsLib.getDocument(data);
        //                     // console.log('loadingTask', loadingTask)
        //                     // loadingTask.promise.then((doc) => {
        //                     //     console.log('doc', doc)
        //                     //     this.viewer.setState({
        //                     //         doc,
        //                     //     }, () => {
        //                     console.log('document view')
        //                     this.setState({ pdfExtract: true, pdfLoaded: 100, processLoaded: 0 }, () => {
        //                         this.setState({ processLoaded: 75 }, () => {
        //                             var viewer = document.getElementsByClassName('pdf-extract-container')
        //                             var page = document.getElementsByClassName('viewer-page-layer')
        //                             console.log('viewer', viewer)
        //                             console.log('page', page)
        //                             this.setState({ processLoaded: 100, extractData: true })
        //                             var detailsParse = JSON.parse(parseItems.details)
        //                             var textract = JSON.parse(detailsParse)
        //                             console.log('textract', typeof textract)
        //                             console.log('textract', textract)
        //                             this.setState({ textractData: textract.textractBlocks, textractIndex: textract.texractIndex, isPortal: false }, () => {
        //                                 console.log('inside', this.state.textractData)
        //                                 console.log('inside', this.state.textractIndex)
        //                                 let getData = this.state.textractData
        //                                 let textractIndex = this.state.textractIndex
        //                                 console.log('inside', this.state.textractData)
        //                                 console.log('inside', this.state.textractIndex)
        //                                 if (textractIndex['doc_type'] != undefined) {
        //                                     this.setState({ isStatement: true }, () => {
        //                                         this.setState({ withProduct: true }, () => {
        //                                             this.onStatementTable()
        //                                         })
        //                                     })
        //                                 } else {
        //                                     this.setState({ isStatement: false })
        //                                     let address = this.fetchDatas(textractIndex['vendor_address'])
        //                                     let invoice_date = this.fetchDatas(textractIndex['invoice_date'])
        //                                     let invoice_number = this.fetchDatas(textractIndex['invoice_number'])
        //                                     let vendor_gstin = this.fetchDatas(textractIndex['vendor_gstin'])
        //                                     let vendor_name = this.fetchDatas(textractIndex['vendor_name'])
        //                                     this.setState({
        //                                         invoice_number: invoice_number,
        //                                         invoice_date: invoice_date,
        //                                         vendor_gstin: vendor_gstin,
        //                                         vendor_name: vendor_name,
        //                                         vendor_address: address,
        //                                         poNo: parseItems.poNumber
        //                                     }, () => {
        //                                         let orgGstin = this.state.orgData.branchListData[0]['branchgstin']
        //                                         this.setState({ igstRemove: String(this.state.vendor_gstin).replace(/ /g, '').substr(0, 2) == orgGstin.substr(0, 2) ? true : (this.state.vendor_gstin.length == 0 ? true : false) }, () => {
        //                                             if (textractIndex['table_value'] != undefined) {
        //                                                 this.setState({ withProduct: true }, () => {
        //                                                     this.onProductTable()
        //                                                 })
        //                                             } else {
        //                                                 this.setState({ withProduct: false }, () => {
        //                                                     let subTotal = this.fetchDatas(textractIndex['sub_total'])
        //                                                     let total = this.fetchDatas(textractIndex['total'])
        //                                                     let taxRate = this.fetchDatas(textractIndex['total_gst_rate'])
        //                                                     let taxAmount = this.fetchDatas(textractIndex['total_gst_amount'])
        //                                                     let HSN_SAC = textractIndex['HSN_SAC'] != undefined ? this.fetchDatas(textractIndex['HSN_SAC']) : this.setState({ hsn_value: undefined })
        //                                                     this.setState({
        //                                                         subTotal: subTotal,
        //                                                         total: total,
        //                                                         taxRate: taxRate,
        //                                                         taxAmount: taxAmount,
        //                                                         hsn_value: HSN_SAC
        //                                                     })
        //                                                 })
        //                                             }
        //                                         })
        //                                     })
        //                                 }
        //                             })
        //                         })
        //                     })
        //                 });
        //             })
        //         //     }, (reason) => {
        //         //         console.error(`Error during ${data} loading: ${reason}`);
        //         //     });
        //         // });
        //     })
        // }

    }
    bytesToSize(bytes) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Byte';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        this.setState({ fileSize: Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i] })
    }
    fileSelected = (ev, i) => {
        let file = ev.target.files
        console.log('file', file)
        if (file[0].type == 'application/vnd.ms-excel') {
            let data = {
                activeStep: 3,
                data: euroKidsData
            }
            this.props.checkBankStatement(data)
        } else {
            this.setState({ pdfLoaded: 10 })
            this.setState({ filename: file[0].name, upload: true, attachmentFileData: ev.target.files[0] }, () => {
                // this.onLoad(0)
                const pdfData = new FormData()
                this.setState({ pdfLoaded: 25 })
                pdfData.append('file', file[0])
                this.bytesToSize(file[0]['size'])
                let pdfDetails = {
                    bucket: 'textractfiles',
                    key: file[0]['name']
                }
                this.setState({ pdfName: file[0].name, pdfPwdFlow: true, pdfParams: pdfDetails }, () => {
                    let data = {
                        activeStep: 1
                    }
                    this.props.checkBankStatement(data)
                })
            })
        }
    }
    pdfUpload = (pdfDetails) => {
        let deleteFileName = []
        const pdfParams = {
            "bucket": pdfDetails.bucket,
            "key": pdfDetails.key
        }
        deleteFileName.push(pdfParams.key)
        this.setState({ bucketName: pdfDetails.bucket })
        let name = String(pdfParams.key)
        name = name.substr(0, name.length - 4)
        this.setState({ pdfFileName: name })
        // axios.get(`${this.state.env['upload_doc']}/get-pdf-url?bucket=${pdfParams.bucket}&key=${pdfParams.key}`)
        //     .then(response => {
        //         let data = response.data.url.replace(/"/g, '')
        this.setState({ showViewer: true, view: "PdfView" }, () => {
            this.setState({ pdfExtract: true, pdfLoaded: 100, processLoaded: 0 }, () => {
                this.onTextract(pdfParams)
            })
        });
        // });
    }
    onTextract = (pdfParams) => {
        if (this.state.processLoaded < 75) {
            this.setState({ processLoaded: Number(this.state.processLoaded) + 10 }, () => {
                setTimeout(() => {
                    this.onTextract(pdfParams)
                }, 100);

            })
        } else {
            // axios.post(`${this.state.env['createInvoiceTemplate']}/startDocumentAnalysis`, pdfParams)
            //     .then(resp => {
            this.setState({ processLoaded: 75 })
            // axios.get(`${this.state.env['upload_doc']}/get-templates`)
            //     .then(tempResp => {
            // const data = { block: resp.data, temp: tempResp.data };
            // let textPayload = new FormData();
            // textPayload.append('resData', JSON.stringify(data))
            var viewer = document.getElementsByClassName('pdf-extract-container')
            var page = document.getElementsByClassName('viewer-page-layer')
            // axios.post(`${this.state.env['invoicePdfExtract']}/textract`, textPayload)
            //     .then(response => {
            this.setState({ processLoaded: 100, extractData: true })
            console.log('zqTextract', zqTextract)
            this.setState({ textractData: zqTextract }, () => {
                // setTimeout(() => {
                this.setState({ isStatement: true }, () => {
                    this.setState({ withProduct: true }, () => {
                        // this.onStatementTable()
                        var statementData = {}
                        statementData = {
                            bank: zqTextract.bank,
                            account_number: zqTextract["account_number"],
                            entries: []
                        }
                        zqTextract['entries'].map(item => {
                            statementData['entries'].push({
                                "particular": item['3'] != undefined ? item['3'] : null,
                                "transactionDate": item['1'] != undefined ? item['1'] : null,
                                "balance": item['6'] != undefined ? item['6'] : null,
                                "chequeNumber": item['2'] != undefined ? item['2'] : null,
                                "credit": item['4'] != undefined ? item['4'] : null,
                                "debit": item['5'] != undefined ? item['5'] : null
                                // "init": getProductRow[item]["Init."].value != undefined ? getProductRow[item]["Init."].value : null
                            })
                        })
                        console.log("statementData", JSON.stringify(statementData))
                        this.setState({ textractedData: JSON.stringify(statementData) });
                        this.props.reviewData(2)
                    })
                })
                // }, 1000);


            })
            // }).catch(err => {
            //     this.setState({ processLoaded: 100, extractData: true }, () => {
            //         setTimeout(() => {
            //             var pagestyle = Array.from(document.getElementsByClassName('page'))
            //             console.log('state', this.state)
            //             this.onCreateJiraTemplateTicket()
            //             this.setState({
            //                 dragWrapWidth: pagestyle["0"].clientWidth,
            //                 dragWrapHeight: pagestyle["0"].clientHeight
            //             }, () => {
            //                 this.setState({ isTemplate: false, textractData: resp.data }, () => { })
            //             })
            //         }, 1000);
            //     })
            // })
            // })

            // })
            // .catch(err => {
            //     this.onTextract(pdfParams)
            // })
        }
    }
    onCreateJiraTemplateTicket = () => {
        let formdata = new FormData();
        formdata.append("subject", "Template Creation");
        formdata.append("description", "Template Creation");
        formdata.append("contact_name", localStorage.getItem("username"));
        formdata.append("company", this.state.orgData.branchListData[0].name);
        formdata.append("current_url", "-");
        formdata.append("contact_email", localStorage.getItem("channel"));
        formdata.append("type", "Template");
        formdata.append("impact", "-");
        formdata.append("priority", "High");
        formdata.append("next_follow_up", "Never");
        formdata.append("user_id", localStorage.getItem("userID"));
        formdata.append("issue_type", "10013");
        formdata.append("attachments", this.state.attachmentFileData);
        formdata.append("attachment_url", this.state.attachmentFileUrl);
        console.log(formdata, "formadata");
        this.setState({ loader: true });
        axios.post(`${this.state.env["support"]}/issue`, formdata)
            .then((res) => {
                console.log(res, "CreateIssue");
            })
            .catch((err) => {
                this.setState({ loader: false });
                console.log(err, "errCreateIssue");
            });
    }
    onLoad = (load) => {
        if (this.state.pdfLoaded == 100) {
            if (this.state.processLoaded < 100) {
                let loadValue = this.state.processLoaded + 5
                this.setState({ processLoaded: loadValue }, () => {
                    setTimeout(() => {
                        this.onLoad(loadValue)
                    }, 100);
                })
            } else {
                this.setState({ extractData: true }, () => {
                    this.setState({ showViewer: true, view: "PdfView" }, () => {
                        setTimeout(() => {
                            var pagestyle = Array.from(document.getElementsByClassName('viewer-page-layer'))
                            this.setState({
                                dragWrapWidth: pagestyle["0"].clientWidth,
                                dragWrapHeight: pagestyle["0"].clientHeight
                            }, () => {
                                this.setState({ isTemplate: false, textractData: textractData }, () => { })
                            })
                        }, 1000);
                    })
                    // this.setState({ textractData: textractData, textractIndex: textractIndex }, () => {
                    //     let getData = this.state.textractData
                    //     let textractIndex = this.state.textractIndex
                    //     console.log(getData)
                    //     console.log(textractIndex)
                    //     let address = this.fetchDatas(textractIndex['vendor_address'])
                    //     let invoice_date = this.fetchDatas(textractIndex['invoice_date'])
                    //     let invoice_number = this.fetchDatas(textractIndex['invoice_number'])
                    //     let vendor_gstin = this.fetchDatas(textractIndex['vendor_gstin'])
                    //     let vendor_name = this.fetchDatas(textractIndex['vendor_name'])
                    //     this.setState({
                    //         invoice_number: invoice_number,
                    //         invoice_date: invoice_date,
                    //         vendor_gstin: vendor_gstin,
                    //         vendor_name: vendor_name,
                    //         vendor_address: address
                    //     }, () => {
                    //         let orgGstin = this.state.orgData.branchListData[0]['branchgstin']
                    //         this.setState({ igstRemove: this.state.vendor_gstin.substr(0, 2) == orgGstin.substr(0, 2) ? true : (this.state.vendor_gstin.length == 0 ? true : false) }, () => {
                    //             console.log(this.state.igstRemove)
                    //             this.onProductTable()
                    //         })
                    //     })
                    // })
                })
            }

        } else {
            let loadValue = this.state.pdfLoaded + 5
            this.setState({ pdfLoaded: loadValue }, () => {
                setTimeout(() => {
                    this.onLoad(loadValue)
                }, 100);
            })
        }
    }
    fetchDatas = (blockItems) => {
        let address = ""
        if (Array.isArray(blockItems)) {
            blockItems.map(index => {
                address = address + ' ' + this.state.textractData['textNodes'][index].text
            })
        } else {
            address = this.state.textractData['textNodes'][blockItems].text
        }
        return address
    }
    fetchDragDatas = (blockItems) => {
        let address = ""
        if (Array.isArray(blockItems)) {
            blockItems.map(item => {
                address = address + ' ' + item.text
            })
        } else {
            address = this.state.textractData['textNodes'][blockItems].text
        }
        return address
    }
    handleView = (event, newAlignment) => {
        // if (this.state.view != newAlignment) {
        this.setState({
            view: newAlignment, templateAdj: {
                docIdType: "EXP",
                txtPurposeId: "41",
                txnPurposeText: "Buy on credit & pay later",
                type: 'Expense',
                dueDate: false,
                shippingAddress: false,
                eWayDetails: false,
                itemTransaction: "getExpenseItemDetail",
                invoiceNo: this.state.expenseId,
                gstPrefilled: false,
                extractData: this.state,
                defaultDatas: this.state
            }
        });
        // }
    };
    // displayScaleChanged(e) {
    //     this.viewer.setState({
    //         scale: this.state.scale
    //     });
    // }
    onInputHanldleChanges = (key, e) => {
        this.setState({ [key]: e.target.value })
    }
    onInputChange = (key, ev) => { }
    onInputClick = (key, ev) => {
        if (this.state.isTemplate) {
            var value = ev.target.value
            if (Array.isArray(this.state.textractIndex[key])) {
                this.onAddressBlock(this.state.textractIndex[key])
            } else {
                let page = this.state.textractData['textNodes'][this.state.textractIndex[key]].page - 1
                let parentblock = document.getElementsByClassName('viewer-layout-main')
                parentblock['0'].style.scrollTop = `${Number(1122) * Number(page)}`
                var blockArr = this.state.textractData['textNodes'][this.state.textractIndex[key]]
                this.onHighlightParam(blockArr, page)
            }
        } else {
            this.setState({ dragDivShow: true, dragItem: key, isTableClick: false })
        }
    }

    onAddressBlock = (blockIndex) => {
        let firstBlock = this.state.textractData['textNodes'][blockIndex[0]]
        let blocks = this.state.textractData['textNodes']
        let lastBlock = this.state.textractData['textNodes'][blockIndex[blockIndex.length - 1]]
        // firstBlock['height'] = height
        let width = 0, height = 0, left = 0, blockType = "";
        blockIndex.map((item, ii) => {
            // console.log(blocks[item])
            let blockIndexItemGeo = blocks[item]
            let bbWidth = blockIndexItemGeo['width']
            let bbHeight = blockIndexItemGeo['height']
            let bbLeft = blockIndexItemGeo['left']
            blockType = blocks[item]['type']
            if (blockType == 'WORD') {
                height = Number(bbHeight)
                width = bbWidth + width
                if (ii == 0) {
                    left = bbLeft
                }
            }
            else {
                height = Number(height) + Number(bbHeight)
                if (width < bbWidth) {
                    width = bbWidth
                    left = bbLeft
                }
            }
        })
        firstBlock['width'] = width
        firstBlock['height'] = blockType == 'WORD' ? height : lastBlock['top'] - firstBlock['top']
        firstBlock['left'] = left
        this.onHighlightParam(firstBlock, 0)
    }

    onHighlightParam = (params, pi, multipleObj) => {
        console.log('***Highlight Params***', params, pi, multipleObj)
        console.log('***Blocks***', this.state.textractData)
        var pageIndex = params.page == null ? 0 : (params.page - 1)
        var viewer = document.getElementsByClassName('viewer-layout-main')
        var pagestyle = Array.from(document.getElementsByClassName('viewer-page-layer'))
        if (viewer[0].clientHeight < Number(params.top) && pageIndex == 0) {

            viewer[0].scrollTo({
                top: 150
            })
        } else {
            viewer[0].scrollTo({
                top: Number(pagestyle["0"].scrollHeight) * Number(pageIndex)
            })
        }
        pagestyle.map((page, pageI) => {
            pagestyle[pageI].setAttribute('class', 'viewer-page-layer pdfPageViewer')
        })
        console.log('***viewer***', viewer)
        console.log('***pagestyle***', pagestyle)
        console.log(pagestyle["0"]["clientWidth"])
        console.log(pagestyle["0"]["clientHeight"])
        var pdfWrap = Array.from(document.getElementsByClassName("pdfPageViewer"))
        var highlight = document.getElementById(`highlight${pageIndex}`)
        if (highlight == undefined) {
            var createHighlight = document.createElement('p')
            createHighlight.setAttribute('id', `highlight${pageIndex}`)
            pdfWrap[pageIndex].appendChild(createHighlight)
            highlight = document.getElementById(`highlight${pageIndex}`)
            for (let highlight_index = 0; highlight_index < 4; highlight_index++) {
                var createH = document.createElement('p')
                createH.setAttribute('id', `highlight${pageIndex}${highlight_index}`)
                createH.setAttribute('class', 'highlight')
                pdfWrap[pageIndex].appendChild(createH)
            }
        }
        this.setState({ pageWidth: pagestyle["0"]["clientWidth"], pageHeight: pagestyle["0"]["clientHeight"] }, () => {
            var RatioWidth = this.state.pageWidth / this.state.textractData.pageDimNodes[pageIndex].width
            var RatioHeight = this.state.pageHeight / this.state.textractData.pageDimNodes[pageIndex].height
            var WidthStartOffset = this.state.textractData.pageDimNodes[pageIndex].left - this.state.textractData.bodyDim.left
            var HeightStartOffset = this.state.textractData.pageDimNodes[pageIndex].top - this.state.textractData.bodyDim.top

            var HTMLItemx1 = (params.left - WidthStartOffset) * RatioWidth
            var HTMLItemy1 = (params.top - HeightStartOffset) * RatioHeight
            var HTMLItemx2 = Number(this.state.pageHeight) - HTMLItemy1
            var HTMLItemy2 = (params.bottom - HeightStartOffset) * RatioHeight
            var textWidthRatio = params.width / this.state.textractData.pageDimNodes[pageIndex].width
            var textWidth = textWidthRatio * this.state.pageWidth
            var extra = pageIndex == 0 ? 3 : 6
            var textHeight = (Number(Number(params.height) + HTMLItemy1) - Number(HTMLItemy1 - 10)) - extra
            console.log('****Diamention***', HTMLItemx1, HTMLItemy1, HTMLItemx2, HTMLItemy2)
            console.log(RatioWidth, RatioHeight, WidthStartOffset, HeightStartOffset, HTMLItemx1, HTMLItemy1)
            console.log(Number(this.state.pageHeight) - (Number(HTMLItemy1 - 10) + Number(HTMLItemx2)), Number(this.state.pageHeight), Number(HTMLItemy1 - 10), Number(HTMLItemx2))
            let highlightDiv = Array.from(document.getElementsByClassName("highlight"))
            highlightDiv.map(highlightItem => {
                highlightItem.setAttribute('style', 'display:none')
            })
            // highlight.setAttribute('style', `top:${Number(HTMLItemy1 - 10)}px; left:${Number(HTMLItemx1 - 10)}px; width:${Number(width_d)}px; height:${Number(HTMLItemx2)}px;display: block`)

            var highlightTop = document.getElementById(`highlight${pageIndex}0`)
            highlightTop.setAttribute('style', `width:100%;height:${Number(HTMLItemy1 - 10)}px;top:0;display: block`)

            var highlightLeft = document.getElementById(`highlight${pageIndex}1`)
            highlightLeft.setAttribute('style', `width:${Number(HTMLItemx1 - 10)}px;height:${Number(HTMLItemx2)}px;top:${Number(HTMLItemy1 - 10)}px;display: block;left:0`)

            var highlightRight = document.getElementById(`highlight${pageIndex}2`)
            highlightRight.setAttribute('style', `width:${(Number(this.state.pageWidth) - (Number(HTMLItemx1) - 11))}px;height:${Number(HTMLItemx2) - Number(textHeight)}px;top:${(Number(params.height) + HTMLItemy1) - extra}px;right:0;display: block`)

            var highlightBottom = document.getElementById(`highlight${pageIndex}3`)
            highlightBottom.setAttribute('style', `width:${Number(this.state.pageWidth) - Number(HTMLItemx1 + textWidth - 2)}px;height:${textHeight}px;top:${Number(HTMLItemy1 - 10)}px;display: block;right:0;`)
            var partWidth = Number(this.state.pageWidth) - (Number(HTMLItemx1 - 10) + (Number(this.state.pageWidth) - Number(HTMLItemx1 + textWidth - 2)))
            var highlightPart = document.getElementById(`highlight${pageIndex}`)
            highlightPart.setAttribute('style', `width:${partWidth}px;height:${textHeight}px;top:${Number(HTMLItemy1 - 10)}px;left:${Number(HTMLItemx1 - 10)}px;display: block;z-index: 1; box-shadow: none; background: transparent;
            position: absolute;
            border: 2px solid blue;
            margin: 0;`)

        })
    }

    onProductTable = () => {
        let identifyDoc = {
            "product": ['item', 'subscription', 'description', 'products', 'services', 'particulars', 'product', 'service'],
            "qty": ['qty', 'quantity'],
            "unit": ['unit', 'units'],
            "both": ['Qty/Units'],
            "hsn_sac": ['HSN', 'SAC', "hsn/sac", "HSN/SAC:"],
            "rate": ["rate", "UnitPrice+"],
            "per": ['per'],
            "amount": ['amount', 'price', 'total', 'taxable value', 'Amount(R)', 'AmountR'],
            "cgst": ['cgst'],
            "sgst": ['sgst'],
            "igst": ['igst'],
            "cess": ['cess']
        }
        let getData = this.state.textractData['textNodes']
        let textractIndex = this.state.textractIndex
        let productTableIndex = {}
        Object.keys(identifyDoc).map(key => {
            productTableIndex[key] = []
            textractIndex['table_value'].map((i, tvi) => {
                if (i[0] == undefined) {
                    let blockTxt = getData[i].text.replace(/ /g, '').toLowerCase()
                    identifyDoc[key].map(id => {
                        if (blockTxt.includes(id.replace(/ /g, '').toLowerCase())) {
                            if (productTableIndex[key].length == 0) {
                                productTableIndex[key].push({
                                    blockId: i,
                                    tableId: tvi
                                })
                            }
                        }
                    })
                }
            })
        })
        var tableRowItem = {}
        textractIndex["table_value"].map(tvi => {
            if (tvi.length != undefined) {
                tvi.map((tri, ri) => {
                    tableRowItem[ri] = {}
                    Object.keys(productTableIndex).map(pti => {
                        if (productTableIndex[pti].length > 0) {
                            tableRowItem[ri][pti] = []
                            productTableIndex[pti].map(ptItem => {
                                if (tri[ptItem["tableId"]] != null) {
                                    tableRowItem[ri][pti].push(tri[ptItem["tableId"]])
                                }
                            })
                        }
                    })
                })
            }
        })
        var getProductRow = {}
        Object.keys(tableRowItem).map(tableRow => {
            getProductRow[tableRow] = {}
            Object.keys(tableRowItem[tableRow]).map(tri => {
                getProductRow[tableRow][tri] = []
                if (tableRowItem[tableRow][tri][0] != undefined) {
                    if (tableRowItem[tableRow][tri][0].length > 1) {
                        let multipleData = this.onMulipleData(tableRowItem[tableRow][tri][0])
                        getProductRow[tableRow][tri] = {
                            "value": multipleData.text,
                            "blocks": multipleData,
                            "page": multipleData.page
                        }
                    } else {
                        getProductRow[tableRow][tri] = {
                            "value": getData[tableRowItem[tableRow][tri][0]].text,
                            "blocks": getData[tableRowItem[tableRow][tri][0]],
                            "page": getData[tableRowItem[tableRow][tri][0]].page
                        }
                    }
                } else {
                    if (tableRowItem[tableRow][tri][0] != undefined) {
                        getProductRow[tableRow][tri] = {
                            "value": getData[tableRowItem[tableRow][tri][0]].text,
                            "blocks": getData[tableRowItem[tableRow][tri][0]],
                            "page": getData[tableRowItem[tableRow][tri][0]].page
                        }
                    }
                }
            })
        })
        this.setState({ getProductRow: getProductRow }, () => {
            if (productTableIndex['product'].length > 0) {
                if (productTableIndex['hsn_sac'].length == 0) {
                    Object.keys(getProductRow).map(rowIndex => {
                        if (getProductRow[rowIndex]['hsn_sac'] != undefined) {
                            getProductRow[rowIndex]['hsn_sac'] = {
                                "value": getData[textractIndex["HSN_SAC"]].text,
                                "blocks": getData[textractIndex["HSN_SAC"]],
                                "page": getData[textractIndex["HSN_SAC"]].page
                            }
                        } else {
                            getProductRow[rowIndex]['hsn_sac'] = {
                                "value": '-',
                                "blocks": undefined,
                                "page": 0
                            }
                        }
                    })
                }
                if (productTableIndex['rate'].length == 0) {
                    Object.keys(getProductRow).map(rowIndex => {
                        var f = ''
                        String(getProductRow[rowIndex]['amount']["value"]).split('').map(item => {
                            if (!isNaN(item) || item == '.') {
                                f = f + item
                            }
                        })
                        getProductRow[rowIndex]['rate'] = {
                            "value": Number(Number(f) / Number(getProductRow[rowIndex]['qty']["value"])),
                            "blocks": [],
                            "page": null
                        }
                    })
                }
                if (productTableIndex['sgst'].length == 0 && productTableIndex['cgst'].length == 0 && productTableIndex['igst'].length == 0) {
                    Object.keys(getProductRow).map(rowIndex => {
                        if (!this.state.igstRemove) {
                            let taxRate = ''
                            let amount = ''
                            let amountBoolean = true
                            if (getProductRow[rowIndex]['amount']['value'] != undefined) {
                                amount = String(getProductRow[rowIndex]['amount']["value"].replace(',', ''))
                                // getProductRow[rowIndex]['amount']["value"].split('').map(item => {
                                //     if (item != '.') {
                                //         if (!isNaN(item) && amountBoolean) {
                                //             amount = amount + item
                                //         }
                                //     } else {
                                //         amountBoolean = false
                                //     }
                                // })
                            }
                            getData[textractIndex["igst_rate"]].text.split('').map(item => {
                                if (!isNaN(item)) {
                                    taxRate = taxRate + item
                                }
                            })
                            getProductRow[rowIndex]['igst'] = {
                                "value": taxRate,
                                "blocks": getData[textractIndex["igst_rate"]],
                                "page": getData[textractIndex["igst_rate"]].page,
                                "amount": Number(Number(Number(amount) * Number(taxRate)) / 100)
                            }
                            var qtyValue = (getProductRow[rowIndex]['qty'] != undefined && getProductRow[rowIndex]['qty']['value'] != undefined) ? String(getProductRow[rowIndex]['qty']['value']).replace(',', '') : 1
                            getProductRow[rowIndex]['total'] = { value: (Number(amount)) + Number(getProductRow[rowIndex]['igst']['amount']) }
                        } else {
                            let taxRate = ''
                            getData[textractIndex["sgst_rate"]].text.split('').map(item => {
                                if (!isNaN(item)) {
                                    taxRate = taxRate + item
                                }
                            })
                            let amount = ''
                            let amountBoolean = true
                            if (getProductRow[rowIndex]['amount']['value'] != undefined) {
                                amount = String(getProductRow[rowIndex]['amount']["value"].replace(',', ''))
                                // getProductRow[rowIndex]['amount']["value"].split('').map(item => {
                                //     if (item != '.') {
                                //         if (!isNaN(item) && amountBoolean) {
                                //             amount = amount + item
                                //         }
                                //     } else {
                                //         amountBoolean = false
                                //     }
                                // })
                            }
                            getProductRow[rowIndex]['sgst'] = {
                                "value": taxRate,
                                "blocks": getData[textractIndex["sgst_rate"]],
                                "page": getData[textractIndex["sgst_rate"]].page,
                                "amount": Number(Number(Number(amount) * Number(taxRate)) / 100)
                            }
                            getProductRow[rowIndex]['cgst'] = {
                                "value": taxRate,
                                "blocks": getData[textractIndex["cgst_rate"]],
                                "page": getData[textractIndex["cgst_rate"]].page,
                                "amount": Number(Number(Number(amount) * Number(taxRate)) / 100)
                            }
                            var qtyValue = (getProductRow[rowIndex]['qty'] != undefined && getProductRow[rowIndex]['qty']['value'] != undefined) ? String(getProductRow[rowIndex]['qty']['value']).replace(',', '') : 1
                            getProductRow[rowIndex]['total'] = { value: (Number(amount)) + Number(getProductRow[rowIndex]['sgst']['amount']) + Number(getProductRow[rowIndex]['cgst']['amount']) }
                        }
                    })
                }
                this.setState({ getProductRow: getProductRow }, () => {
                    Object.keys(getProductRow).map(rowIndex => {
                        if (textractIndex['HSN_SAC'] != undefined) {
                            let HSN_SAC = textractIndex['HSN_SAC'] != undefined ? this.fetchDatas(textractIndex['HSN_SAC']) : this.setState({ hsn_value: undefined })
                            if (getProductRow[rowIndex]['hsn_sac']['value'] == '-') {
                                getProductRow[rowIndex]['hsn_sac'] = {
                                    "value": HSN_SAC,
                                    "blocks": getData[textractIndex['HSN_SAC']],
                                    "page": getData[textractIndex['HSN_SAC']].page
                                }
                            }
                        }
                    })
                    this.setState({ getProductRow: getProductRow, tableRowReady: true })
                })
            }
        })
    }
    onStatementTable = () => {
        let identifyDoc = {}
        let getData = this.state.textractData['textNodes']
        let textractIndex = this.state.textractIndex
        textractIndex["table_value"].map((item, index) => {
            if (textractIndex["table_value"].length != (index + 1)) {
                identifyDoc[getData[item]['text']] = []
                identifyDoc[getData[item]['text']].push(getData[item]['text'])
            }
        })
        console.log('***identifyDoc***', identifyDoc)
        let productTableIndex = {}
        Object.keys(identifyDoc).map(key => {
            productTableIndex[key] = []
            textractIndex['table_value'].map((i, tvi) => {
                if (i[0] == undefined) {
                    let blockTxt = getData[i].text.replace(/ /g, '').toLowerCase()
                    identifyDoc[key].map(id => {
                        if (blockTxt.includes(id.replace(/ /g, '').toLowerCase())) {
                            if (productTableIndex[key].length == 0) {
                                productTableIndex[key].push({
                                    blockId: i,
                                    tableId: tvi
                                })
                            }
                        }
                    })
                }
            })
        })
        var tableRowItem = {}
        textractIndex["table_value"].map(tvi => {
            if (tvi.length != undefined) {
                tvi.map((tri, ri) => {
                    tableRowItem[ri] = {}
                    Object.keys(productTableIndex).map(pti => {
                        if (productTableIndex[pti].length > 0) {
                            tableRowItem[ri][pti] = []
                            productTableIndex[pti].map(ptItem => {
                                if (tri[ptItem["tableId"]] != null) {
                                    tableRowItem[ri][pti].push(tri[ptItem["tableId"]])
                                }
                            })
                        }
                    })
                })
            }
        })
        var getProductRow = {}
        Object.keys(tableRowItem).map((tableRow, ri) => {
            console.log('ri', ri)
            console.log('tableRow', tableRow)
            getProductRow[tableRow] = {}
            Object.keys(tableRowItem[tableRow]).map(tri => {
                console.log('tri', tri)
                getProductRow[tableRow][tri] = []
                if (tableRowItem[tableRow][tri][0] != undefined) {
                    if (tableRowItem[tableRow][tri][0].length > 1) {
                        console.log('row data', tableRowItem[tableRow][tri][0])
                        let multipleData = this.onMulipleStatementData(tableRowItem[tableRow][tri][0],)
                        console.log('value', multipleData.text)
                        getProductRow[tableRow][tri] = {
                            "value": multipleData.text,
                            "blocks": multipleData,
                            "page": multipleData.page
                        }
                    } else {
                        getProductRow[tableRow][tri] = {
                            "value": getData[tableRowItem[tableRow][tri][0]].text,
                            "blocks": getData[tableRowItem[tableRow][tri][0]],
                            "page": getData[tableRowItem[tableRow][tri][0]].page
                        }
                    }
                } else {
                    if (tableRowItem[tableRow][tri][0] != undefined) {
                        getProductRow[tableRow][tri] = {
                            "value": getData[tableRowItem[tableRow][tri][0]].text,
                            "blocks": getData[tableRowItem[tableRow][tri][0]],
                            "page": getData[tableRowItem[tableRow][tri][0]].page
                        }
                    }
                }
            })
        })
        console.log('getProductRow', getProductRow)
        var statementData = []
        Object.keys(getProductRow).map(item => {
            statementData.push({
                "particular": getProductRow[item].Particulars.value != undefined ? getProductRow[item].Particulars.value : null,
                "transactionDate": getProductRow[item]["Tran Date"].value != undefined ? getProductRow[item]["Tran Date"].value : null,
                "balance": getProductRow[item].Balance.value != undefined ? getProductRow[item].Balance.value : null,
                "chequeNumber": getProductRow[item]['Chq No'].value != undefined ? getProductRow[item]['Chq No'].value : null,
                "credit": getProductRow[item].Credit.value != undefined ? getProductRow[item].Credit.value : null,
                "debit": getProductRow[item]['Debit'].value != undefined ? getProductRow[item]['Debit'].value : null,
                "init": getProductRow[item]["Init."].value != undefined ? getProductRow[item]["Init."].value : null
            })
        })
        console.log("statementData", JSON.stringify(statementData))
        this.setState({ getProductRow: getProductRow, tableRowReady: true }, () => {
        })
    }

    onMulipleData = (multipleData) => {
        let getData = this.state.textractData['textNodes']
        let firstBlock = getData[multipleData[0]]
        let text = ''
        let width = 0
        let height = 0
        let left = 1
        multipleData.map(itemIndex => {
            text = text + ' ' + getData[itemIndex].text
            width = width + getData[itemIndex]['width']
            height = height + getData[itemIndex]['height']
            left = left > getData[itemIndex]['left'] ? getData[itemIndex]['left'] : getData[itemIndex]['left']
        })
        firstBlock['text'] = text
        firstBlock['width'] = width
        firstBlock['height'] = height + (getData[multipleData[0]]['height'] / 2)
        firstBlock['left'] = left
        return firstBlock
    }
    onMulipleStatementData = (multipleData) => {
        let getData = this.state.textractData['textNodes']
        let firstBlock = getData[multipleData[0]]
        let text = ''
        let width = 0
        let height = 0
        let left = 1
        let prevMinX = 0
        let currentMinX = 0
        let textNodeData = []
        multipleData.map((itemIndex, i) => {
            textNodeData.push(getData[itemIndex])
        })
        textNodeData.sort(function (a, b) { return a.left - b.left }).map((item, i) => {
            text = text + (isNaN(Number(text)) ? ' ' : '') + item.text
            width = width + item['width']
            height = i == 0 ? item['height'] : getData[multipleData[i - 1]]['top'] < getData[multipleData[i]]['top'] ? (height + item['height']) : height
            left = i == 0 ? item['left'] : left
        })

        firstBlock['text'] = text
        firstBlock['width'] = width
        firstBlock['height'] = height
        firstBlock['left'] = left
        return firstBlock
    }
    onTableInputClick = (key, rowIndex, event) => {
        if (this.state.isTemplate) {
            let blocks = this.state.getProductRow[rowIndex][key]
            if (blocks.blocks != undefined) {
                this.onHighlightParam(blocks['blocks'], (Number(blocks['page']) - 1))
            }
        } else {
            this.setState({ dragDivShow: true, dragItem: key, dragIndex: rowIndex, isTableClick: true })
        }

    }
    onTableInputChange = (key, rowIndex, event) => { }
    onTableInputHanldleChanges = (key, rowIndex, event) => {
        let getProductRow = this.state.getProductRow
        getProductRow[rowIndex][key] = {
            value: event.target.value,
            blocks: getProductRow[rowIndex][key]['blocks'],
            page: getProductRow[rowIndex][key]['page'],
            amount: event.target.value
        }
        this.setState({ getProductRow: getProductRow }, () => {
            let createTemplate = this.state.createTemplate
            createTemplate['product_row'] = this.state.getProductRow
            this.setState({ dragDivShow: false, createTemplate: createTemplate }, () => {
            })
        })
    }
    handlePanelChange = (key) => {
        this.setState({ [key]: !this.state[key] })
    }
    onSubmit = () => {
        this.setState({ loader: true }, () => {
            setTimeout(() => {
                localStorage.setItem("notifCount", 2)
                this.setState({
                    loader: false,
                    snackBar: true,
                    processMessage: "The bank statement has been reconciled successfully."
                }, () => {
                    setTimeout(() => {
                        this.setState({
                            loader: false,
                            snackBar: false,
                            processMessage: ""
                        })
                        this.props.history.push('/main/processing')
                    }, 500);
                })
            }, 1000)

        })
        // this.setState({ formRead: false }, () => {
        //     this.setState({
        //         templateAdj: {
        //             docIdType: "EXP",
        //             txtPurposeId: "41",
        //             txnPurposeText: "Buy on credit & pay later",
        //             type: 'Expense',
        //             dueDate: false,
        //             shippingAddress: false,
        //             eWayDetails: false,
        //             itemTransaction: "getExpenseItemDetail",
        //             invoiceNo: this.state.expenseId,
        //             gstPrefilled: false,
        //             extractData: this.state,
        //             defaultDatas: this.state
        //         }
        //     }, () => {
        //         this.props.history.push({
        //             pathname: '/main/purchases/create-expense', pdfExtractDatas: this.state
        //         })
        //     })
        // })
        this.setState({})

    }
    onDataConfirm = (e) => {
        let wrap = document.getElementsByClassName('react-draggable')
        let transform = String(wrap[0].style.transform).split('translate')
        let pxSplit = String(transform[1]).split('px')
        let wrapLeft = Number(String(pxSplit[0]).substr(1))
        let wrapTop = Number(String(pxSplit[1]).substr(2))
        let wrapHeight = wrapTop + wrap[0].clientHeight
        let wrapWidth = wrapLeft + wrap[0].clientWidth
        let blocks = this.state.textractData['textNodes']
        let dataItems = []
        blocks.map(item => {
            let bb = item
            let w = this.state.dragWrapWidth
            let h = this.state.dragWrapHeight
            if (item['type'] == "WORD") {
                if (((bb.left * w) > wrapLeft && (bb.left * w) < wrapWidth) && ((bb.top * h) > wrapTop && ((bb.top * h) < wrapHeight))) {
                    if (item.page == this.state.pageNumber) {
                        dataItems.push(item)
                    }
                }
            }
        })
        if (this.state.isTableClick) {
            let getProductRow = this.state.getProductRow
            getProductRow[this.state.dragIndex][this.state.dragItem] = {
                value: this.fetchDragDatas(dataItems),
                blocks: dataItems,
                page: dataItems[0].page != undefined ? dataItems[0].page : this.state.pageNumber,
                amount: this.fetchDragDatas(dataItems)
            }
            this.setState({ getProductRow: getProductRow }, () => {
                let createTemplate = this.state.createTemplate
                createTemplate['product_row'] = this.state.getProductRow
                this.setState({ dragDivShow: false, createTemplate: createTemplate }, () => {
                })
            })
        } else {
            this.setState({ [this.state.dragItem]: this.fetchDragDatas(dataItems) }, () => {
                let createTemplate = this.state.createTemplate
                createTemplate[this.state.dragItem] = dataItems
                this.setState({ dragDivShow: false, createTemplate: createTemplate }, () => {
                    if (this.state.dragItem == 'vendor_gstin') {
                        let orgGstin = this.state.orgData.branchListData[0]['branchgstin']
                        this.setState({ igstRemove: String(this.state.vendor_gstin).replace(/ /g, '').substr(0, 2) == orgGstin.substr(0, 2) ? true : (this.state.vendor_gstin.length == 0 ? true : false) })
                    }
                })
            })
        }
    }
    onAddProduct = () => {
        let getProductRow = {
            "qty": {
                value: '',
                blocks: [],
                page: 0
            },
            "product": {
                value: '',
                blocks: [],
                page: 0
            },
            "hsn_sac": {
                value: '',
                blocks: [],
                page: 0
            },
            "rate": {
                value: '',
                blocks: [],
                page: 0
            },
            "sgst": {
                value: '',
                blocks: [],
                page: 0
            },
            "cgst": {
                value: '',
                blocks: [],
                page: 0
            },
            "igst": {
                value: '',
                blocks: [],
                page: 0
            },
            "amount": {
                value: '',
                blocks: [],
                page: 0
            },
            "total": {
                value: '',
                blocks: [],
                page: 0
            }
        }
        let getProductItem = this.state.getProductRow[0] != undefined ? this.state.getProductRow : []
        getProductItem.push(getProductRow)
        this.setState({ getProductRow: getProductItem, tableRowReady: true })
    }
    onRemoveProduct = (rowIndex) => {
        let getProductItem = []
        this.state.getProductRow.map((item, index) => {
            if (rowIndex != index) {
                getProductItem.push(item)
            }
        })
        this.setState({ getProductRow: [] }, () => {
            this.setState({ getProductRow: getProductItem })
        })
    }
    onPageNumber = (e) => {
        this.setState({ pageNumber: e.target.value })
    }
    onCancel = () => {
        this.setState({
            extractData: false,
            filename: '',
            upload: false,
            files: [],
            error: '',
            formData: '',
            fileSize: '',
            pdfLoaded: 0,
            processLoaded: 0,
            pdfName: undefined,
            showViewer: false,
            pdfExtract: false,
            textractData: {},
            textractIndex: {},
            textractCompleted: false,
            view: undefined,
            vendor_name: '',
            vendor_address: '',
            vendor_gstin: '',
            invoice_number: '',
            invoice_date: '',
            getProductRow: {},
            vendorInfoPanel: true,
            invoiceInfoPanel: true,
            purchaseInfoPanel: true,
            igstRemove: true,
            tableRowReady: false,
            expenseId: '',
            templateAdj: undefined,
            withProduct: true,
            subTotal: 0,
            total: 0,
            taxRate: 0,
            taxAmount: 0,
            scale: 1,
            hsn_value: "",
            formRead: true,
            dragWrapWidth: "0px",
            dragWrapHeight: "0px",
            dragDivShow: false,
            isTemplate: true,
            dragItem: "",
            dragIndex: 0,
            createTemplate: {},
            isTableClick: false,
            pageNumber: 1,
            attachmentFileData: "",
            attachmentFileUrl: ""
        })
    }
    formatCurrency = (amount) => {
        console.log('amount', amount)
        var removeSpace = String(amount).replace(/ /g, '')
        if (String(removeSpace) != 'NaN') {
            return (new Intl.NumberFormat('en-IN', {
                style: 'currency',
                currency: 'INR',
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }).format(removeSpace))
        } else {
            return '-'
        }
    }

    sendPDFData = () => {
        let data = this.state.textractedData;
        console.log("sendPDFData", data)
        this.props.extractedData(data)
    }
    showPasswordModal = () => {
        // console.log(item)
        this.setState({ showPasswordModal: true })
    }
    removePasswordModal = () => {
        this.setState({ showPasswordModal: false })
    }
    noPassword = () => {
        this.props.showPDFView();
        this.setState({ pdfLoaded: 50, pdfPwdFlow: false }, () => {
            setTimeout(() => {
                this.pdfUpload(this.state.pdfParams)
            }, 100);
        })
    }
    processBankStatement = () => {
        this.props.showPDFView();
        this.setState({ pdfLoaded: 50, pdfPwdFlow: false }, () => {
            setTimeout(() => {
                this.pdfUpload(this.state.pdfParams)
            }, 100);
        })
    }
    render() {

        const onButtonClick = () => {
            if (!this.state.pdfPwdFlow) {
                if (this.state.pdfLoaded == 0) {
                    this.inputFile.current.click();
                }
            }
        };
        const { filename } = this.state;
        const layout = [{ key: 'test', x: 0, y: 0, width: 200, height: 100, zIndex: 1 }]

        const canResizable = (isResize) => {
            return { top: isResize, right: isResize, bottom: isResize, left: isResize, topRight: isResize, bottomRight: isResize, bottomLeft: isResize, topLeft: isResize };
        };

        return (<React.Fragment>
            {this.state.loader ? <Loader /> : null}
            <div className="invoice-page-wrapper new-textract-wrap">

                {/* <div className="create-invoice-page-header"> */}
                {/* <div className="header-title">
                        <div className="icon-back-arrow-box"><KeyboardBackspaceIcon className="icon-back-arrow" onClick={() => { this.props.history.goBack() }} /></div>
                        <p className="header-txt">Reconcile</p>
                    </div> */}

                {this.state.processLoaded != 100 ? null :
                    <div className="footer-btn-grps">
                        {/* <button className="cancel-btn" onClick={this.onCancel}>Clear</button> */}
                        {/* <button className="generate-btn">Save</button> */}
                        <button className="generate-btn fixed-pdf-submit-btn" onClick={this.sendPDFData}>Submit</button>
                    </div>}
                {/* </div> */}
                <React.Fragment>
                    <div className="create-invoice-wrapper">
                        {this.state.processLoaded != 100 ? null :
                            <div className="invoice-options-wrapper">
                                <div className="invoice-selectoption-wrap">
                                    <div className="invoice-view-wrap">
                                        {/* <div className="switch-tab">
                                            <span style={{ paddingRight: '8px' }} className={this.state.view == "PdfView" ? 'invoicedocview active' : 'invoicedocview'}>PDF + Data</span>
                                            <AntSwitch
                                                checked={this.state.invoiceViewChecked}
                                                onChange={this.handleView}
                                                name="invoiceViewChecked"
                                                color="primary"
                                                disabled={this.state.invoiceViewDisabled}
                                            />
                                            <span style={{ paddingLeft: '5px' }} className={this.state.view == "PdfData" ? 'invoicedocview active' : 'invoicedocview'}>Only Data</span>
                                        </div> */}
                                    </div>
                                </div>
                            </div>}
                        {/* {this.state.extractData ? */}
                        <React.Fragment>
                            <div className="panel-wrap" style={{ "visibility": this.state.view == 'PdfView' ? 'visible' : 'hidden' }}>
                                <div className={this.state.isStatement ? "pdf-extract-container statement-wrap" : "pdf-extract-container"}>
                                    {this.state.dragDivShow ?
                                        <div className="dragble-wrap" style={{ width: this.state.dragWrapWidth, height: this.state.dragWrapHeight, top: `${this.state.dragWrapHeight * (this.state.pageNumber - 1)}px` }}>
                                            <DragResizeContainer
                                                className='resize-container'
                                                resizeProps={{
                                                    minWidth: 10,
                                                    minHeight: 10,
                                                    enable: canResizable(10)
                                                }}
                                                // onDoubleClick={clickScreen}
                                                layout={layout}
                                                dragProps={{ disabled: false }}
                                                // onLayoutChange={onLayoutChange}
                                                scale={0.7}
                                            >
                                                {layout.map((single, si) => {
                                                    return (

                                                        <div key={single.key}>
                                                            {layout.length == (si + 1) ? <button className="drag-confirm-btn" onClick={(e) => this.onDataConfirm(e)}>Confirm</button> : null}
                                                        </div>

                                                    );
                                                })}
                                            </DragResizeContainer>
                                        </div> : null}
                                    <Worker workerUrl={`https://unpkg.com/pdfjs-dist@2.5.207/build/pdf.worker.min.js`}>
                                        <div className="new-pdf-wrap">
                                            <Viewer
                                                fileUrl={pdfURL}
                                                scale={1}
                                                isSidebarOpened={false}
                                                renderPage={renderPage} />
                                        </div>
                                    </Worker>
                                </div>
                                <div className={this.state.isStatement ? "pdf-extract-input-wrap statement-wrap" : "pdf-extract-input-wrap"}>
                                    {this.state.isStatement ? <React.Fragment>
                                        {this.state.withProduct ?
                                            <React.Fragment>
                                                <ExpansionPanel expanded={this.state.vendorInfoPanel} onChange={() => this.handlePanelChange('vendorInfoPanel')}>
                                                    <ExpansionPanelSummary
                                                        expandIcon={<ExpandMoreIcon />}
                                                        aria-controls="panel1a-content"
                                                        id="panel1a-header"
                                                    >
                                                        <h4 className="expanse-panel-hd">Bank Informations</h4>
                                                    </ExpansionPanelSummary>
                                                    <ExpansionPanelDetails>
                                                        <Typography>
                                                            <div className="extract-pdf-input-wrap">
                                                                <p className="extract-pdf-label-box">Bank Name: </p>
                                                                <input type="text" className="extract-pdf-input" value={this.state.textractData.bank}></input>
                                                            </div>
                                                            <div className="extract-pdf-input-wrap">
                                                                <p className="extract-pdf-label-box">Account Number: </p>
                                                                <input type="text" className="extract-pdf-input" value={this.state.textractData.account_number}></input>
                                                            </div>
                                                        </Typography>
                                                    </ExpansionPanelDetails>
                                                </ExpansionPanel>
                                                <table className="zq-table-wrap" id="table-to-xls">
                                                    <thead className="zq-table-heading">
                                                        <tr>
                                                            <th>SL</th>
                                                            {this.state.textractData.header.map((hd) => {
                                                                return <th
                                                                    key={hd}
                                                                >{hd}</th>
                                                            })}
                                                        </tr>
                                                    </thead>
                                                    <tbody className="zq-table-body">
                                                        {this.state.textractData.entries.map((index, entry) => {
                                                            return (
                                                                <tr key={index} className="statement-row">
                                                                    {Object.keys(index).map((hd, i) => {
                                                                        return i == 0 ? <>
                                                                            <td
                                                                                key={hd + 'sl'}
                                                                            >
                                                                                {Number(entry) + 1}
                                                                            </td>
                                                                            <td
                                                                                key={hd}
                                                                            >
                                                                                {index[hd]}
                                                                            </td>
                                                                        </> :
                                                                            <td
                                                                                key={index + hd}
                                                                                style={{ 'overflowWrap': (String(hd).toLowerCase().includes('particular')) ? 'breakWord' : 'normal', 'textAlign': (String(hd).toLowerCase().includes('credit') || String(hd).toLowerCase().includes('debit') || String(hd).toLowerCase().includes('balance')) ? 'right' : 'left' }}
                                                                            >
                                                                                {index[hd]}
                                                                            </td>
                                                                    })}
                                                                </tr>
                                                            );
                                                        })}
                                                    </tbody>
                                                </table>
                                            </React.Fragment> : null}
                                    </React.Fragment> :
                                        <React.Fragment>
                                            <ExpansionPanel expanded={this.state.vendorInfoPanel} onChange={() => this.handlePanelChange('vendorInfoPanel')}>
                                                <ExpansionPanelSummary
                                                    expandIcon={<ExpandMoreIcon />}
                                                    aria-controls="panel1a-content"
                                                    id="panel1a-header"
                                                >
                                                    <h4 className="expanse-panel-hd">Vendor Informations</h4>
                                                </ExpansionPanelSummary>
                                                <ExpansionPanelDetails>
                                                    <Typography>
                                                        <div className="extract-pdf-input-wrap">
                                                            <p className="extract-pdf-label-box">Name: </p>
                                                            <input type="text" className="extract-pdf-input" name="vendor_name" onChange={(event) => this.onInputHanldleChanges('vendor_name', event)} value={this.state.vendor_name}
                                                                onClick={(event) => this.onInputClick('vendor_name', event)} onBlur={(event) => this.onInputChange('vendor_name', event)}></input>
                                                        </div>
                                                        <div className="extract-pdf-input-wrap">
                                                            <p className="extract-pdf-label-box">Address: </p>
                                                            <textarea type="text" className="extract-pdf-input" name="vendor_address" onChange={(event) => this.onInputHanldleChanges('vendor_address', event)} value={this.state.vendor_address}
                                                                onClick={(event) => this.onInputClick('vendor_address', event)} onBlur={(event) => this.onInputChange('vendor_address', event)}></textarea>
                                                        </div>
                                                        <div className="extract-pdf-input-wrap">
                                                            <p className="extract-pdf-label-box">GSTIN: </p>
                                                            <input type="text" className="extract-pdf-input" name="vendor_gstin" onChange={(event) => this.onInputHanldleChanges('vendor_gstin', event)} value={this.state.vendor_gstin}
                                                                onClick={(event) => this.onInputClick('vendor_gstin', event)} onBlur={(event) => this.onInputChange('vendor_gstin', event)}></input>
                                                        </div>
                                                    </Typography>
                                                </ExpansionPanelDetails>
                                            </ExpansionPanel>
                                            <ExpansionPanel expanded={this.state.invoiceInfoPanel} onChange={() => this.handlePanelChange('invoiceInfoPanel')}>
                                                <ExpansionPanelSummary
                                                    expandIcon={<ExpandMoreIcon />}
                                                    aria-controls="panel2a-content"
                                                    id="panel2a-header"
                                                >
                                                    <h4 className="expanse-panel-hd">Invoice Details</h4>
                                                </ExpansionPanelSummary>
                                                <ExpansionPanelDetails>
                                                    <Typography>
                                                        <div className="extract-pdf-input-wrap">
                                                            <p className="extract-pdf-label-box">Invoice No: </p>
                                                            <input type="text" className="extract-pdf-input" name="invoice_number" value={this.state.invoice_number} onChange={(event) => this.onInputHanldleChanges('invoice_number', event)}
                                                                onClick={(event) => this.onInputClick('invoice_number', event)} onBlur={(event) => this.onInputChange('invoice_number', event)}></input>
                                                        </div>
                                                        <div className="extract-pdf-input-wrap">
                                                            <p className="extract-pdf-label-box">Invoice Date: </p>
                                                            <input type="text" className="extract-pdf-input" name="invoice_date" value={this.state.invoice_date} onChange={(event) => this.onInputHanldleChanges('invoice_date', event)}
                                                                onClick={(event) => this.onInputClick('invoice_date', event)} onBlur={(event) => this.onInputChange('invoice_date', event)}></input>
                                                        </div>
                                                    </Typography>
                                                </ExpansionPanelDetails>
                                            </ExpansionPanel>

                                            <ExpansionPanel expanded={this.state.purchaseInfoPanel} onChange={() => this.handlePanelChange('purchaseInfoPanel')}>
                                                <ExpansionPanelSummary
                                                    expandIcon={<ExpandMoreIcon />}
                                                    aria-controls="panel-ex"
                                                    id="panel-ex"
                                                >
                                                    {/* <h4 className="expanse-panel-hd">Purchase Details {!this.state.isTemplate ? <img src={addCircle} className="product-add-icon" onClick={e => {
                                                        e.stopPropagation()
                                                        this.onAddProduct()
                                                    }} /> : null}</h4> */}
                                                </ExpansionPanelSummary>
                                                {this.state.withProduct ?
                                                    <React.Fragment>
                                                        {Object.keys(this.state.getProductRow).map(rowIndex => {
                                                            return <React.Fragment>{this.state.getProductRow[rowIndex]['amount']['value'] != undefined ? <ExpansionPanelDetails>
                                                                <Typography>
                                                                    {this.state.tableRowReady ?
                                                                        <ExpansionPanel>
                                                                            <ExpansionPanelSummary
                                                                                expandIcon={<ExpandMoreIcon />}
                                                                                aria-controls={"panel-ex-row" + (rowIndex + 1)}
                                                                                id={"panel-ex-row" + (rowIndex + 1)}
                                                                            >
                                                                                <h4 className="expanse-panel-hd">Row {Number(rowIndex) + Number(1)} {!this.state.isTemplate ?
                                                                                    <React.Fragment>
                                                                                        {/* <img src={removeCircle} className="product-add-icon" onClick={e => {
                                                                                            e.stopPropagation()
                                                                                            this.onRemoveProduct(rowIndex)
                                                                                        }} /> */}

                                                                                        <input type="number" value={this.state.pageNumber} onChange={(e) => {
                                                                                            e.stopPropagation()
                                                                                            this.onPageNumber(e)
                                                                                        }} className="product-add-page" onClick={e => {
                                                                                            e.stopPropagation()
                                                                                        }} />
                                                                                        <label className="product-add-label">Page</label>
                                                                                    </React.Fragment> : null}</h4>
                                                                            </ExpansionPanelSummary>
                                                                            <ExpansionPanelDetails>
                                                                                <Typography>
                                                                                    <div className="extract-pdf-input-wrap">
                                                                                        <p className="extract-pdf-label-box">SL</p>
                                                                                        <input type="text" className="extract-pdf-input" name="sl" value={Number(rowIndex) + Number(1)} readOnly={true} disabled={true}></input>
                                                                                    </div>
                                                                                    {this.state.getProductRow[rowIndex]['qty'] != undefined ?
                                                                                        <div className="extract-pdf-input-wrap">
                                                                                            <p className="extract-pdf-label-box">Qty</p>
                                                                                            <input type="text" className="extract-pdf-input" name="qty" value={this.state.getProductRow[rowIndex]['qty']['value']} onChange={(event) => this.onTableInputHanldleChanges('qty', rowIndex, event)}
                                                                                                onClick={(event) => this.onTableInputClick('qty', rowIndex, event)} onBlur={(event) => this.onTableInputChange('qty', rowIndex, event)}></input>
                                                                                        </div> : <div className="extract-pdf-input-wrap">
                                                                                            <p className="extract-pdf-label-box">Qty</p>
                                                                                            <input type="text" className="extract-pdf-input" name="qty" onChange={(event) => this.onTableInputHanldleChanges('qty', rowIndex, event)}></input>
                                                                                        </div>}
                                                                                    <div className="extract-pdf-input-wrap">
                                                                                        <p className="extract-pdf-label-box">Product</p>
                                                                                        <input type="text" className="extract-pdf-input" name="product" value={this.state.getProductRow[rowIndex]['product']['value']} onChange={(event) => this.onTableInputHanldleChanges('product', rowIndex, event)}
                                                                                            onClick={(event) => this.onTableInputClick('product', rowIndex, event)} onBlur={(event) => this.onTableInputChange('product', rowIndex, event)}></input>
                                                                                    </div>
                                                                                    <div className="extract-pdf-input-wrap">
                                                                                        <p className="extract-pdf-label-box">HSN/SAC</p>
                                                                                        <input type="text" className="extract-pdf-input" name="hsn_sac" value={this.state.getProductRow[rowIndex]['hsn_sac']['value']} onChange={(event) => this.onTableInputHanldleChanges('hsn_sac', rowIndex, event)}
                                                                                            onClick={(event) => this.onTableInputClick('hsn_sac', rowIndex, event)} onBlur={(event) => this.onTableInputChange('hsn_sac', rowIndex, event)}></input>
                                                                                    </div>
                                                                                    <div className="extract-pdf-input-wrap">
                                                                                        <p className="extract-pdf-label-box">Rate</p>
                                                                                        <input type="text" className="extract-pdf-input" name="rate" value={this.state.getProductRow[rowIndex]['rate']['value']} onChange={(event) => this.onTableInputHanldleChanges('rate', rowIndex, event)}
                                                                                            onClick={(event) => this.onTableInputClick('rate', rowIndex, event)} onBlur={(event) => this.onTableInputChange('rate', rowIndex, event)}></input>
                                                                                    </div>
                                                                                    <div className="extract-pdf-input-wrap">
                                                                                        <p className="extract-pdf-label-box">Discount</p>
                                                                                        <input type="text" className="extract-pdf-input" name="discount"></input>
                                                                                    </div>
                                                                                    {this.state.igstRemove ?
                                                                                        <React.Fragment>
                                                                                            <div className="extract-pdf-input-wrap">
                                                                                                <p className="extract-pdf-label-box">SGST</p>
                                                                                                <input type="text" className="extract-pdf-input" name="sgst" value={this.state.getProductRow[rowIndex]['sgst']['amount']} onChange={(event) => this.onTableInputHanldleChanges('sgst', rowIndex, event)}
                                                                                                    onClick={(event) => this.onTableInputClick('sgst', rowIndex, event)} onBlur={(event) => this.onTableInputChange('sgst', rowIndex, event)}></input>
                                                                                            </div>
                                                                                            <div className="extract-pdf-input-wrap">
                                                                                                <p className="extract-pdf-label-box">CGST</p>
                                                                                                <input type="text" className="extract-pdf-input" name="cgst" value={this.state.getProductRow[rowIndex]['cgst']['amount']} onChange={(event) => this.onTableInputHanldleChanges('cgst', rowIndex, event)}
                                                                                                    onClick={(event) => this.onTableInputClick('cgst', rowIndex, event)} onBlur={(event) => this.onTableInputChange('cgst', rowIndex, event)}></input>
                                                                                            </div>
                                                                                        </React.Fragment> :
                                                                                        <React.Fragment>
                                                                                            <div className="extract-pdf-input-wrap">
                                                                                                <p className="extract-pdf-label-box">IGST</p>
                                                                                                <input type="text" className="extract-pdf-input" name="igst" value={this.state.getProductRow[rowIndex]['igst']['amount']} onChange={(event) => this.onTableInputHanldleChanges('igst', rowIndex, event)}
                                                                                                    onClick={(event) => this.onTableInputClick('igst', rowIndex, event)} onBlur={(event) => this.onTableInputChange('igst', rowIndex, event)}></input>
                                                                                            </div>
                                                                                        </React.Fragment>}
                                                                                    <div className="extract-pdf-input-wrap">
                                                                                        <p className="extract-pdf-label-box">Subtotal</p>
                                                                                        <input type="text" className="extract-pdf-input" name="amount" value={this.state.getProductRow[rowIndex]['amount']['value']} onChange={(event) => this.onTableInputHanldleChanges('amount', rowIndex, event)}
                                                                                            onClick={(event) => this.onTableInputClick('amount', rowIndex, event)} onBlur={(event) => this.onTableInputChange('amount', rowIndex, event)}></input>
                                                                                    </div>
                                                                                    <div className="extract-pdf-input-wrap">
                                                                                        <p className="extract-pdf-label-box">Total</p>
                                                                                        <input type="text" className="extract-pdf-input" name="total"
                                                                                            value={Number(this.state.getProductRow[rowIndex]['total']['value'])}></input>
                                                                                    </div>
                                                                                </Typography>
                                                                            </ExpansionPanelDetails>
                                                                        </ExpansionPanel> : null}

                                                                </Typography>
                                                            </ExpansionPanelDetails> : null}</React.Fragment>
                                                        })}</React.Fragment> :
                                                    <React.Fragment>
                                                        {this.state.hsn_value == undefined ? null :
                                                            <div className="extract-pdf-input-wrap">
                                                                <p className="extract-pdf-label-box">HSN/SAC</p>
                                                                <input type="text" className="extract-pdf-input" name="HSN_SAC" value={this.state.hsn_value} onChange={(event) => this.onInputHanldleChanges('HSN_SAC', event)}
                                                                    onClick={(event) => this.onInputClick('HSN_SAC', event)} onBlur={(event) => this.onInputChange('HSN_SAC', event)}></input>
                                                            </div>}
                                                        <div className="extract-pdf-input-wrap">
                                                            <p className="extract-pdf-label-box">Tax Rate </p>
                                                            <input type="text" className="extract-pdf-input" name="total_gst_rate" value={this.state.taxRate} onChange={(event) => this.onInputHanldleChanges('total_gst_rate', event)}
                                                                onClick={(event) => this.onInputClick('total_gst_rate', event)} onBlur={(event) => this.onInputChange('total_gst_rate', event)}></input>
                                                        </div>
                                                        <div className="extract-pdf-input-wrap">
                                                            <p className="extract-pdf-label-box">Taxable Amount </p>
                                                            <input type="text" className="extract-pdf-input" name="sub_total" value={this.state.subTotal} onChange={(event) => this.onInputHanldleChanges('sub_total', event)}
                                                                onClick={(event) => this.onInputClick('sub_total', event)} onBlur={(event) => this.onInputChange('sub_total', event)}></input>
                                                        </div>
                                                        <div className="extract-pdf-input-wrap">
                                                            <p className="extract-pdf-label-box">Tax Amount </p>
                                                            <input type="text" className="extract-pdf-input" name="total_gst_amount" value={this.state.taxAmount} onChange={(event) => this.onInputHanldleChanges('total_gst_amount', event)}
                                                                onClick={(event) => this.onInputClick('total_gst_amount', event)} onBlur={(event) => this.onInputChange('total_gst_amount', event)}></input>
                                                        </div>
                                                        <div className="extract-pdf-input-wrap">
                                                            <p className="extract-pdf-label-box">Total </p>
                                                            <input type="text" className="extract-pdf-input" name="total" value={this.state.total} onChange={(event) => this.onInputHanldleChanges('total', event)}
                                                                onClick={(event) => this.onInputClick('total', event)} onBlur={(event) => this.onInputChange('total', event)}></input>
                                                        </div>
                                                    </React.Fragment>}

                                            </ExpansionPanel>
                                        </React.Fragment>
                                    }
                                </div>
                            </div>
                            <div className="panel-wrap" style={{ "visibility": this.state.view == 'PdfData' ? 'visible' : 'hidden' }}>
                                {/* {this.state.view == 'PdfData' ? <TransactionForm templateAdj={this.state.templateAdj} /> : null} */}
                            </div>
                        </React.Fragment>
                        {!this.state.extractData ? <div className="invoice-custom-table-wrapper invoice-extract-wrap">
                            <div className="pdf-extract-wrap">
                                <div className={!this.state.pdfPwdFlow ? "onboard-card dot-card" : "onboard-card dot-card pwd-pdf-wrap"} onClick={onButtonClick}>

                                    {this.state.upload ?
                                        !this.state.pdfPwdFlow ? <React.Fragment>
                                            {/* {this.state.isPortal ? */}
                                            {/* <div className="file-upload-wrap">
                                                    <p className="file-process-hd" style={{ textAlign: 'center' }}>Fetching Data...</p>
                                                </div> : */}
                                            <div className="file-upload-wrap" onClick={(event) => { event.stopPropagation() }}>
                                                {this.state.pdfLoaded !== 100 ?
                                                    <span className="file-loaded-percentage">{this.state.pdfLoaded}%</span>
                                                    :
                                                    <span className="file-loaded-percentage" style={{
                                                        fontSize: '14px',
                                                        color: "#00b8d9"
                                                    }}><i class="fa fa-check" aria-hidden="true"></i></span>}
                                                <p className="file-process-hd">Uploading File ({this.state.filename})</p>
                                                <LinearProgress color="primary" variant={"determinate"} value={this.state.pdfLoaded} />
                                                {this.state.processLoaded !== 100 ?
                                                    <span className="file-loaded-percentage process-percentage">{this.state.processLoaded}%</span>
                                                    :
                                                    <span className="file-loaded-percentage process-percentage" style={{
                                                        fontSize: '14px',
                                                        color: "#00b8d9"
                                                    }}><i class="fa fa-check" aria-hidden="true"></i></span>}
                                                <p className="file-process-hd">Processing File</p>
                                                <LinearProgress color="primary" variant={"determinate"} value={this.state.processLoaded < 75 ? this.state.processLoaded : 75} />
                                            </div>
                                            {/* } */}
                                        </React.Fragment> :
                                            <div className="confirm-PDF-wrap" style={{ width: "32%" }}>
                                                {!this.state.showPasswordModal ?
                                                    <React.Fragment>
                                                        <p style={{ textAlign: "center" }}>Is this bank statement password protected ?</p>
                                                        <div style={{ textAlign: "center" }}>
                                                            <button className="show-password-btn" onClick={this.showPasswordModal}>Yes</button>
                                                            <button className="hide-password-btn" style={{ marginLeft: "20px" }} onClick={this.noPassword}>No</button>
                                                        </div>
                                                    </React.Fragment>
                                                    :
                                                    <React.Fragment>
                                                        <div className="profile-pwd-form" style={{ zIndex: "999" }}>
                                                            <ZenForm inputData={this.state.profileData} onFormBtnEvent={this.removePasswordModal} onSubmit={this.processBankStatement} />
                                                        </div>
                                                    </React.Fragment>
                                                }
                                            </div>
                                        :
                                        <div>
                                            <input type='file' id='file' disabled={this.state.pdfLoaded == 0 ? false : true} ref={this.inputFile} style={{ display: 'none' }} onChange={this.fileSelected} />
                                            <div className="card-third-round  " ><PublishOutlinedIcon className="onboard-icons" /> </div>
                                            <h6 className="onboard-card-header">Upload Bank Statement as PDF or XLS</h6>
                                            <p className="onboard-card-title" style={{ color: "#000", textAlign: "center" }} ><span className="browse-content" >Browse File</span> </p>
                                        </div>
                                    }

                                </div>
                            </div>
                        </div> : null}
                    </div>
                </React.Fragment>
                <Snackbar
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                    open={this.state.snackBar}
                    autoHideDuration={1000}
                    message={this.state.processMessage}
                    className="success-snackbar"

                />
            </div>
        </React.Fragment>)
    }
}
export default ExtractPurchaseInvoice;