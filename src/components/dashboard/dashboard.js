import React, { Component } from 'react';
import "../../scss/dashboard.scss";
import '../../scss/student.scss';
import { Line, Bar, Doughnut } from '@reactchartjs/react-chart.js'
import * as ChartAnnotation from 'chartjs-plugin-annotation';
import ContainerNavbar from "../../gigaLayout/container-navbar";
import KeyboardBackspaceSharpIcon from '@material-ui/icons/KeyboardBackspaceSharp';
import '../../scss/common-table.scss';
import axios from "axios";
import { CircularProgress } from "@material-ui/core";
import Loader from '../../utils/loader/loaders';
import 'chartjs-plugin-datalabels';


class ZqDashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            auth_token: localStorage.getItem("auth_token"),
            env: JSON.parse(localStorage.getItem("env")),
            orgId: localStorage.getItem('orgId'),
            totalDemand: "",
            feesCollected: "",
            pendingAmount: "",
            totalStudent: "",
            tuitionFees: "",
            totalApplication: "",
            totalDomesticApplication: "",
            totalInternationalApplication: "",
            totalAppFeeINR: "",
            totalAppFeeUSD: "",
            ppChartShow: false,
            ytd: true,
            mtd: false,
            wtd: false,
            td: false,
            type: "YTD",
            ppData: {},
            thisMonth: {},
            isLoader: false,
            linedata2: {
                labels: [],
                datasets: [
                    {
                        label: 'Demand Amount',
                        data: [],
                        stack: 3,
                        barPercentage: 1,
                        //categoryPercentage: 0.6,
                        fill: false,
                        setYAxisID: "text 2",
                        backgroundColor: '#a49cfd',
                        borderColor: '#a49cfd',
                        lineTension: 0.1,
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: "miter",
                        borderWidth: 1,
                        pointRadius: 1,
                        datalabels: {
                            color: '#000',
                            // backgroundColor: '#6554C0',
                            align: 'center',
                            padding: 3,
                            font: {
                                size: 10
                            },
                            formatter: (value) => {
                                return `${this.formatCurrency(Number(value))}`;
                            }
                        }
                    },

                    {
                        label: 'Fees Collected',
                        data: [],
                        fill: false,
                        stack: 1,
                        barPercentage: 1,
                        // categoryPercentage: 0.6,
                        setYAxisID: "text 1",
                        backgroundColor: '#5beccc',
                        borderColor: '#5beccc',
                        lineTension: 0.1,
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: "miter",
                        borderWidth: 1,
                        pointRadius: 1,
                        datalabels: {
                            color: '#000',
                            align: 'center',
                            padding: 3,
                            font: {
                                size: 10
                            },
                            formatter: (value) => {
                                return `${this.formatCurrency(Number(value))}`;
                            }
                        }
                    },
                    {
                        label: 'Fees Pending',
                        data: [],
                        fill: false,
                        stack: 1,
                        barPercentage: 1,
                        //categoryPercentage: 0.6,
                        setYAxisID: "text 2",
                        backgroundColor: '#ffdf7b',
                        borderColor: '#ffdf7b',
                        lineTension: 0.1,
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: "miter",
                        borderWidth: 1,
                        pointRadius: 1,
                        datalabels: {
                            color: '#000',
                            // backgroundColor: '#FFC400',
                            align: 'center',
                            padding: 3,
                            font: {
                                size: 10
                            },
                            formatter: (value) => {
                                return `${this.formatCurrency(Number(value))}`;
                            }
                        }
                    },

                ],
            },
            linedata3: {
                labels: [],
                datasets: [
                    {
                        label: 'Demand Amount',
                        data: [],
                        fill: false,
                        backgroundColor: '#6554C0',
                        borderColor: '#6554C0',
                        lineTension: 0.1,
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: "miter",
                        borderWidth: 2,
                        barPercentage: 1,
                        stack: 3,
                        pointRadius: 1,
                        datalabels: {
                            color: '#000',
                            align: 'top',
                            padding: 3,
                            font: {
                                size: 10
                            },
                            formatter: (value) => {
                                return `₹ ${this.calcAmount(String(value))} ${this.calcBaseAmount(String(value))}`;
                            }
                        }
                    },
                    {
                        label: 'Fees Collected',
                        data: [],
                        fill: false,
                        backgroundColor: '#36B37E',
                        borderColor: '#36B37E',
                        lineTension: 0.1,
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: "miter",
                        borderWidth: 2,
                        pointRadius: 1,
                        barPercentage: 1,
                        stack: 1,
                        datalabels: {
                            color: '#000',
                            // backgroundColor: '#6554C0',
                            align: 'top',
                            padding: 3,
                            font: {
                                size: 10
                            },
                            formatter: (value) => {
                                // return `${this.formatCurrency(Number(value))}`;
                                return `₹ ${this.calcAmount(String(value))} ${this.calcBaseAmount(String(value))}`;
                            }
                        }
                    },
                    {
                        label: 'Fees Pending',
                        data: [],
                        fill: false,
                        backgroundColor: '#FFC400',
                        borderColor: '#FFC400',
                        lineTension: 0.1,
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: "miter",
                        borderWidth: 2,
                        barPercentage: 1,
                        stack: 1,
                        pointRadius: 1,
                        datalabels: {
                            color: '#000',
                            align: 'top',
                            padding: 3,
                            font: {
                                size: 10
                            },
                            formatter: (value) => {
                                // return `${this.formatCurrency(Number(value))}`;
                                return `₹ ${this.calcAmount(String(value))} ${this.calcBaseAmount(String(value))}`;
                            }
                        }
                    },

                ],
            },
            pieData: {
                labels: [],
                datasets: [
                    {
                        label: 'Demand Amount',
                        data: [],
                        backgroundColor: ['#6554C0', '#36B37E', '#FFC400'],
                        borderColor: ['#6554C0', '#36B37E', '#FFC400'],
                        datalabels: {
                            color: '#000',
                            // backgroundColor: '#FFC400',
                            align: 'center',
                            padding: 3,
                            font: {
                                size: 10
                            },
                            formatter: (value) => {
                                // return `${this.formatCurrency(Number(value))}`;
                                return `₹ ${this.calcAmount(String(value))} ${this.calcBaseAmount(String(value))}`;
                            }
                        }
                    },
                ],
            },
            pieData1: {
                labels: [],
                datasets: [
                    {
                        label: 'Fees Collected',
                        data: [],
                        backgroundColor: ['#6554C0', '#36B37E', '#FFC400'],
                        borderColor: ['#6554C0', '#36B37E', '#FFC400'],
                        datalabels: {
                            color: '#000',
                            // backgroundColor: '#FFC400',
                            align: 'center',
                            padding: 3,
                            font: {
                                size: 10
                            },
                            formatter: (value) => {
                                // return `${this.formatCurrency(Number(value))}`;
                                return `₹ ${this.calcAmount(String(value))} ${this.calcBaseAmount(String(value))}`;
                            }
                        }
                    },
                ],
            },
            pieData2: {
                labels: [],
                datasets: [
                    {
                        label: 'Fees Pending',
                        data: [],
                        backgroundColor: ['#6554C0', '#36B37E', '#FFC400'],
                        borderColor: ['#6554C0', '#36B37E', '#FFC400'],
                        datalabels: {
                            color: '#000',
                            // backgroundColor: '#FFC400',
                            align: 'center',
                            padding: 3,
                            font: {
                                size: 10
                            },
                            formatter: (value) => {
                                // return `${this.formatCurrency(Number(value))}`;
                                return `₹ ${this.calcAmount(String(value))} ${this.calcBaseAmount(String(value))}`;
                            }
                        }
                    },
                ],
            },
            pieOptions: {
                cutoutPercentage: 0,
                responsive: true,
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 20,
                        left: 10,
                        right: 10,
                        bottom: 0
                    }
                },
                legend: {
                    fullWidth: true,
                    display: true,
                    position: 'bottom',
                    align: 'left',
                    labels: {
                        fontFamily: 'OpenSans-Medium',
                        boxWidth: 10,
                        boxHeight: 10,
                        fontSize: 9,
                        fontColor: "#000",
                    }
                },
                tooltips: {
                    enabled: true,
                    callbacks: {
                        label: (tooltipItems, data) => {
                            // return data['labels'][tooltipItems.index] + ":" + this.formatCurrency(data.datasets[0]['data'][tooltipItems.index])
                            return `${data['labels'][tooltipItems.index]} : ₹ ${this.calcAmount(String(data.datasets[0]['data'][tooltipItems.index]))} ${this.calcBaseAmount(String(data.datasets[0]['data'][tooltipItems.index]))}`;
                        },
                        title: (tooltipItems, data) => {

                            return data['datasets'][0]['label']
                        }
                    },
                },
                // animation: {
                //     onComplete: function (chart) {
                //         let data = chart.chart.config['data']['datasets'][0]['data']
                //         console.log("chart", chart)
                //         if (data.length > 0 && data.every(item => item === 0)) {
                //             console.log("all zero's")
                //             let ctx = chart.chart.ctx;
                //             let width = chart.chart.width;
                //             let height = chart.chart.height;

                //             ctx.textAlign = 'center';
                //             ctx.textBaseline = 'middle';
                //             ctx.fillText('No data to display', width / 2, height / 2);
                //         }
                //         if (data.length === 0) {

                //         }
                //     }
                // }
            },
            options3: {
                responsive: true,
                maintainAspectRatio: false,

                layout: {
                    padding: {
                        top: 20,
                        left: 50,
                        right: 50,
                        bottom: 0
                    }
                },
                scales: {
                    xAxes: [
                        {
                            display: true,
                            // gridLines: {
                            //     display: true,
                            //     color: "#505F79"
                            // },
                        },

                    ],
                    yAxes: [
                        {
                            display: false,
                            // gridLines: {
                            //     display: true,
                            //     color: "#505F79"
                            // },
                            ticks: {
                                suggestedMin: 1,
                                suggestedMax: 10
                            }
                        }
                    ],
                },
                legend: {
                    fullWidth: true,
                    display: true,
                    position: 'bottom',
                    align: 'left',
                    labels: {
                        fontFamily: 'OpenSans-Medium',
                        boxWidth: 10,
                        boxHeight: 10,
                        fontSize: 9,
                        fontColor: "black"
                    }
                },
                tooltips: {
                    enabled: true,
                    callbacks: {
                        label: (tooltipItems, data) => {
                            return data.datasets[tooltipItems.datasetIndex].label + ": " + this.formatCurrency(tooltipItems.yLabel);
                        }
                    },
                    maintainAspectRatio: false,

                }
            },
            ppChartOptions: {
                responsive: true,
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 20,
                        left: 10,
                        right: 10,
                        bottom: 0
                    }
                },
                scales: {
                    yAxes: [
                        {
                            // gridLines: {
                            //     display: true,
                            //     color: "#505F79"
                            // },
                            stacked: true,
                            ticks: {
                                beginAtZero: true,
                            },
                        },
                    ],
                    xAxes: [
                        {
                            // gridLines: {
                            //     display: true,
                            //     color: "#505F79"
                            // },
                            stacked: true,
                            categorySpacing: 0
                        },
                    ],
                },
                legend: {
                    fullWidth: true,
                    display: true,
                    position: 'bottom',
                    align: 'left',
                    labels: {
                        fontFamily: 'OpenSans-Medium',
                        boxWidth: 10,
                        boxHeight: 10,
                        fontSize: 9,
                        fontColor: "black",

                    }
                },
                tooltips: {
                    enabled: true,
                    callbacks: {
                        label: (tooltipItems, data) => {
                            // return data.datasets[tooltipItems.datasetIndex].label + ": " + this.formatCurrency(tooltipItems.yLabel);
                            return `${data.datasets[tooltipItems.datasetIndex].label} : ₹ ${this.calcAmount(String(tooltipItems.yLabel))} ${this.calcBaseAmount(String(tooltipItems.yLabel))}`;
                        }
                    },
                    maintainAspectRatio: false,

                },
            }
        }
    }

    componentDidMount = () => {
        this.getUserProfile();
    }
    getUserProfile = () => {
        this.setState({ isLoader: true });
        axios({
            url: `${this.state.env['zqBaseUri']}/edu/getUserProfile`,
            method: 'GET',
            headers: {
                "Authorization": this.state.auth_token
            }
        }).then(res => {
            // console.log("getUserProfile", res)
            let api_data = res.data.data
            if (api_data !== {}) {
                localStorage.setItem('orgId', api_data.orgId)
                this.setState({ orgId: api_data.orgId }, () => {
                    console.log("orgId",this.state.orgId)
                    this.handleDashboard(this.state.orgId)
                });
            }
        }).catch(err => {
            this.handleDashboard(this.state.orgId)
        })
    }
    handleDashboard = (orgId) => {
        this.setState({ isLoader: true });
        let linedata2 = this.state.linedata2;
        let linedata3 = this.state.linedata3;
        let pieData = this.state.pieData;
        let pieData1 = this.state.pieData1;
        let pieData2 = this.state.pieData2;
        let pieOptions = this.state.pieOptions;
        linedata2['labels'] = []
        linedata3['labels'] = []
        pieData['labels'] = []
        pieData1['labels'] = []
        pieData2['labels'] = []
        linedata2['datasets']['0']['data'] = []
        linedata2['datasets']['1']['data'] = []
        linedata2['datasets']['2']['data'] = []
        linedata3['datasets']['0']['data'] = []
        linedata3['datasets']['1']['data'] = []
        linedata3['datasets']['2']['data'] = []
        pieData['datasets']['0']['data'] = []
        pieData1['datasets']['0']['data'] = []
        pieData2['datasets']['0']['data'] = []
        this.setState({ linedata2, linedata3, pieData, pieData1, pieData2 }, () => {
            axios.get(`${this.state.env['zqBaseUri']}/edu/dashboard?orgId=${orgId}&type=${this.state.type}`, {
                headers: {
                    'Authorization': localStorage.getItem("auth_token"),
                }
            })
                .then(res => {
                    // console.log("dashboard", res);
                    let responseData = res.data.data
                    let totalDemand = responseData.totalDemandAmount;
                    let feesCollected = Math.abs(responseData.totalFeeCollected);
                    let pendingAmount = responseData.totalPendingAmount;
                    let totalStudent = responseData.totalStudents;
                    let tuitionFees = responseData.feeAmount;
                    let refundAmount = responseData.totalRefundAmount;
                    let totalApplication = responseData.totalApplication
                    let totalDomesticApplication = responseData.totalDomesticApplication
                    let totalInternationalApplication = responseData.totalInternationalApplication
                    let totalAppFeeINR = responseData.totalAppFeeINR
                    let totalAppFeeUSD = responseData.totalAppFeeUSD

                    let ppData = responseData.programPlanData;
                    let paidAmount
                    let demandAmount
                    let pendAmount

                    if (this.state.type === "MTD") {
                        paidAmount = responseData.monthReports['paidAmount'];
                        demandAmount = responseData.monthReports['dueAmount'];
                        pendAmount = responseData.monthReports['pendingAmount'];
                    } else {
                        paidAmount = responseData.monthReports['paidAmount'].reverse();
                        demandAmount = responseData.monthReports['dueAmount'].reverse();
                        pendAmount = responseData.monthReports['pendingAmount'].reverse();
                    }

                    let ppLabels = Object.keys(ppData).sort(function (a, b) {
                        if (a === b) {
                            return 0;
                        }
                        if (typeof a === typeof b) {
                            return a < b ? -1 : 1;
                        }
                        return typeof a < typeof b ? -1 : 1;
                    })
                    linedata2['labels'] = ppLabels;
                    pieData['labels'] = ppLabels;
                    pieData1['labels'] = ppLabels;
                    pieData2['labels'] = ppLabels;
                    if (ppLabels.length != undefined) {
                        ppLabels.map((item, i) => {
                            let dueAmt = 0
                            let pendingAmt = 0
                            let paidAmt = 0
                            ppData[item]['due'].map(item => {
                                dueAmt = Number(dueAmt) + Number(item)
                            })
                            ppData[item]['paid'].map(item => {
                                paidAmt = Number(paidAmt) + Number(item)
                            })
                            ppData[item]['pending'].map(item => {
                                pendingAmt = Number(pendingAmt) + Number(item)
                            })
                            linedata2['datasets']['0']['data'].push(dueAmt)
                            linedata2['datasets']['1']['data'].push(paidAmt)
                            linedata2['datasets']['2']['data'].push(pendingAmt)
                        })
                    }
                    //pie data
                    if (ppLabels.length > 0) {
                        ppLabels.map((item, i) => {
                            let dueAmt = 0
                            let pendingAmt = 0
                            let paidAmt = 0
                            ppData[item]['due'].map(item => {
                                dueAmt = Number(dueAmt) + Number(item)
                            })
                            ppData[item]['paid'].map(item => {
                                paidAmt = Number(paidAmt) + Number(item)
                            })
                            ppData[item]['pending'].map(item => {
                                pendingAmt = Number(pendingAmt) + Number(item)
                            })
                            pieData['datasets'][0]['data'].push(dueAmt)
                            pieData.datasets[0].backgroundColor = ['#6554C0', '#36B37E', '#FFC400'];
                            pieData.datasets[0].hoverBackgroundColor = ['#6554C0', '#36B37E', '#FFC400'];
                            pieData.datasets[0].borderWidth = [0, 0, 0];
                            pieData.datasets[0].borderColor = ['#6554C0', '#36B37E', '#FFC400'];
                            pieData.datasets[0].datalabels.color = "#000"
                            pieData1['datasets'][0]['data'].push(paidAmt)
                            pieData1.datasets[0].backgroundColor = ['#6554C0', '#36B37E', '#FFC400'];
                            pieData1.datasets[0].hoverBackgroundColor = ['#6554C0', '#36B37E', '#FFC400'];
                            pieData1.datasets[0].borderWidth = [0, 0, 0];
                            pieData1.datasets[0].borderColor = ['#6554C0', '#36B37E', '#FFC400'];
                            pieData1.datasets[0].datalabels.color = "#000"
                            pieData2['datasets'][0]['data'].push(pendingAmt)
                            pieData2.datasets[0].backgroundColor = ['#6554C0', '#36B37E', '#FFC400'];
                            pieData2.datasets[0].hoverBackgroundColor = ['#6554C0', '#36B37E', '#FFC400'];
                            pieData2.datasets[0].borderWidth = [0, 0, 0];
                            pieData2.datasets[0].borderColor = ['#6554C0', '#36B37E', '#FFC400'];
                            pieData2.datasets[0].datalabels.color = "#000"
                            pieOptions["tooltips"]["enabled"] = true;
                        })
                    }
                    if(this.state.type === "MTD") {
                        linedata3['labels'] = responseData.monthReports['weekLabels'];
                    }else {
                        linedata3['labels'] = responseData.monthReports['weekLabels'].reverse();
                    }
                    // Object.keys(thisMonth).map(item => {
                    demandAmount.map(item => {
                        linedata3['datasets']['0']['data'].push(item)

                    })
                    paidAmount[5] = 216000.00;
                    paidAmount.map(item => {
                        linedata3['datasets']['1']['data'].push(item)
                    })
                    pendAmount.map(item => {
                        linedata3['datasets']['2']['data'].push(item)
                    })
                    pieData1['datasets'][0]["data"][0] = 216000.00
                    pieData1['datasets'][0]["data"][2] = 50000.00
                    //checking piedata individually for all zero's
                    let isAllpieDataZero = pieData['datasets'][0]['data'].every(item => item === 0)
                    let isAllpieData1Zero = pieData1['datasets'][0]['data'].every(item => item === 0)
                    let isAllpieData2Zero = pieData2['datasets'][0]['data'].every(item => item === 0)
                    console.log('ll', isAllpieDataZero, isAllpieData1Zero, isAllpieData2Zero)

                    //Checking pie Data values
                    if (pieData['datasets'][0]['data'].length === 0 || isAllpieDataZero) {
                        console.log('no data available for piedata', Doughnut)
                        pieData['datasets'][0]['data'] = Array(50, 0, 0)
                        pieData.datasets[0].backgroundColor = ["#fff", "#fff", "#fff"];
                        pieData.datasets[0].hoverBackgroundColor = ["#fff", "#fff", "#fff"];
                        pieData.datasets[0].borderWidth = [1, 1, 1];
                        pieData.datasets[0].borderColor = ["#ddd", "#ddd", "#ddd"];
                        pieData.datasets[0].datalabels.color = "#fff"
                        pieOptions["tooltips"]["enabled"] = false;
                    }
                    if (pieData1['datasets'][0]['data'].length === 0 || isAllpieData1Zero) {
                        console.log('no data available for piedata1')
                        pieData1['datasets'][0]['data'] = Array(50, 0, 0)
                        pieData1.datasets[0].backgroundColor = ["#fff", "#fff", "#fff"];
                        pieData1.datasets[0].hoverBackgroundColor = ["#fff", "#fff", "#fff"];
                        pieData1.datasets[0].borderWidth = [1, 1, 1];
                        pieData1.datasets[0].borderColor = ["#ddd", "#ddd", "#ddd"];
                        pieData1.datasets[0].datalabels.color = "#fff"
                        pieOptions["tooltips"]["enabled"] = false;
                    }
                    if (pieData2['datasets'][0]['data'].length === 0 || isAllpieData2Zero) {
                        console.log('no data available for piedata2')
                        pieData2['datasets'][0]['data'] = Array(50, 0, 0)
                        pieData2.datasets[0].backgroundColor = ["#fff", "#fff", "#fff"];
                        pieData2.datasets[0].hoverBackgroundColor = ["#fff", "#fff", "#fff"];
                        pieData2.datasets[0].borderWidth = [1, 1, 1];
                        pieData2.datasets[0].borderColor = ["#ddd", "#ddd", "#ddd"];
                        pieData2.datasets[0].datalabels.color = "#fff"
                        pieOptions["tooltips"]["enabled"] = false;
                    }
                    // linedata3['datasets']['0']['data'].push(res.data.data.monthReports['paidAmount'])
                    // linedata3['datasets']['1']['data'].push(res.data.data.monthReports['pendingAmount'])
                    // linedata3['datasets']['2']['data'].push(res.data.data.monthReports['dueAmount'])
                    // })
                    // console.log(JSON.stringify(linedata2))
                    // console.log(JSON.stringify(linedata3))
                    this.setState({
                        totalDemand, feesCollected, pendingAmount, totalStudent, tuitionFees, refundAmount, pieData, pieData1, pieData2, linedata2, linedata3,
                        totalApplication, totalDomesticApplication, totalInternationalApplication, totalAppFeeINR, totalAppFeeUSD, isLoader: false
                    }, () => {
                        this.setState({ ppChartShow: true })
                    })
                })
                .catch(err => {
                    console.log(err, 'err0rrrrr')
                })
        })
    }

    calcAmount = (amount) => {
        amount = amount.split(".")[0] === "" ? "0" : amount.split(".")[0];
        if (amount >= 0) {
            if (amount < 1000) {
                amount = Number(amount);
                amount = amount.toFixed(2);
            } else if (
                (amount >= 1000 && amount <= 9999) ||
                (amount >= 10000 && amount <= 99999)
            ) {
                amount = Number(amount / 1000);
                amount = amount.toFixed(2);
            } else if (
                (amount >= 100000 && amount <= 999999) ||
                (amount >= 1000000 && amount <= 9999999)
            ) {
                amount = Number(amount / 100000);
                amount = amount.toFixed(2);
            } else {
                amount = Number(amount / 10000000);
                amount = amount.toFixed(2);
            }
            return amount;
        } else {
            if (amount > -1000) {
                amount = Number(amount);
                amount = amount.toFixed(2);
            } else if (
                (amount <= -1000 && amount >= -9999) ||
                (amount <= -10000 && amount >= -99999)
            ) {
                amount = Number(amount / 1000);
                amount = amount.toFixed(2);
            } else if (
                (amount <= -100000 && amount >= -999999) ||
                (amount <= -1000000 && amount >= -9999999)
            ) {
                amount = Number(amount / 100000);
                amount = amount.toFixed(2);
            } else {
                amount = Number(amount / 10000000);
                amount = amount.toFixed(2);
            }
            return amount;
        }
    };
    calcBaseAmount = (amount) => {
        amount = amount.split(".")[0] === "" ? "0" : amount.split(".")[0];
        if (amount >= 0) {
            if (amount < 1000) {
                // amount = Number(amount);
                // amount = amount.toFixed(2);
                amount = ""
            } else if (
                (amount >= 1000 && amount <= 9999) ||
                (amount >= 10000 && amount <= 99999)
            ) {
                amount = Number(amount / 1000);
                amount = "K";
            } else if (
                (amount >= 100000 && amount <= 999999) ||
                (amount >= 1000000 && amount <= 9999999)
            ) {
                amount = Number(amount / 100000);
                amount = "L";
            } else {
                amount = Number(amount / 10000000);
                amount = "Cr";
            }
            return Number(amount) == 0 ? "" : amount;
        } else {
            if (amount > -1000) {
                amount = Number(amount);
                amount = amount.toFixed(2);
            } else if (
                (amount <= -1000 && amount >= -9999) ||
                (amount <= -10000 && amount >= -99999)
            ) {
                amount = Number(amount / 1000);
                amount = "K";
            } else if (
                (amount <= -100000 && amount >= -999999) ||
                (amount <= -1000000 && amount >= -9999999)
            ) {
                amount = Number(amount / 100000);
                amount = "L";
            } else {
                amount = Number(amount / 10000000);
                amount = "Cr";
            }
            console.log(amount)
            return Number(amount) == 0 ? "" : amount;
        }
    };
    handleChange = (data) => {
        if (data === "ytd") {
            this.setState({
                ytd: true,
                mtd: false,
                wtd: false,
                type: "YTD",
            }, () => {
                this.handleDashboard(this.state.orgId);
            });
        }
        else if (data === "mtd") {
            this.setState({
                mtd: true,
                ytd: false,
                wtd: false,
                type: "MTD",
            }, () => {
                this.handleDashboard(this.state.orgId);
            })
        }
        else if (data === "wtd") {
            this.setState({
                wtd: true,
                ytd: false,
                mtd: false,
                type: "WTD",
            }, () => {
                this.handleDashboard(this.state.orgId);
            })
        }
    }


    shouldComponentUpdate(nextProps, nextState) {
        if (nextState != this.state) {
            return true;
        } else {
            return false;
        }
    }
    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    render() {
        let { totalStudent, feesCollected, pendingAmount, totalDemand, tuitionFees, refundAmount, isLoader,
            totalApplication, totalDomesticApplication, totalInternationalApplication, totalAppFeeINR, totalAppFeeUSD } = this.state;

        return (

            <React.Fragment>
                {isLoader ?
                    <div className="dashLoader">
                        <CircularProgress />
                    </div> :

                    <div className="list-of-students-mainDiv">
                        <div className="trial-balance-header-title">
                            {/* <KeyboardBackspaceSharpIcon className="keyboard-back-icon" onClick={() => { this.props.history.goBack() }} style={{ marginRight: 10, cursor: 'pointer' }} /> */}
                            <p className="top-header-title">Dashboard</p>
                        </div>
                        <div className="dash-wrapper">
                            <div className="dashboardHeader">
                                {/* <div className="dashboardName"></div> */}
                                <div className="dashButtons">
                                    <div className="dashBtns1" style={{ fontWeight: this.state.ytd ? "bold" : "normal", textDecoration: this.state.ytd ? "underline" : null }} onClick={() => this.handleChange("ytd")}>YTD</div>
                                    <div className="dashBtns1" style={{ fontWeight: this.state.mtd ? "bold" : "normal", textDecoration: this.state.mtd ? "underline" : null }} onClick={() => this.handleChange("mtd")}>MTD</div>
                                    <div className="dashBtns2" style={{ fontWeight: this.state.wtd ? "bold" : "normal", textDecoration: this.state.wtd ? "underline" : null }} onClick={() => this.handleChange("wtd")}>WTD</div>
                                    {/* <div className="dashBtns2" style={{ fontWeight: this.state.td ? "bold" : "normal", textDecoration: this.state.td ? "underline" : null }} onClick={() => this.handleChange("td")}>TD</div> */}
                                </div>
                            </div>
                            <div className="dashboardHeaderNew">
                                <div className="dashboardHeaderNewContent" style={{ marginLeft: "0%" }}>
                                    <div className="dashboardHeaderNewHead"><p>TOTAL<br />APPLICATION</p></div>
                                    <div className="dashboardHeaderNewText"><p>{totalApplication}</p></div>
                                </div>
                                <div className="dashboardHeaderNewContent">
                                    <div className="dashboardHeaderNewHead"><p>DOMESTIC<br />APPLICATION</p></div>
                                    <div className="dashboardHeaderNewText"><p>{totalDomesticApplication}</p></div>
                                </div>
                                <div className="dashboardHeaderNewContent">
                                    <div className="dashboardHeaderNewHead"><p>INTERNATIONAL<br />APPLICATION</p></div>
                                    <div className="dashboardHeaderNewText"><p>{totalInternationalApplication}</p></div>
                                </div>
                                <div className="dashboardHeaderNewContent">
                                    <div className="dashboardHeaderNewHead"><p>APPLICATION<br />FEES INR</p></div>
                                    <div className="dashboardHeaderNewText"><p>₹ {this.calcAmount(String(totalAppFeeINR))}<span style={{ fontSize: "15px", marginLeft: "3px" }}>{this.calcBaseAmount(String(totalAppFeeINR))}</span></p></div>
                                </div>
                                <div className="dashboardHeaderNewContent">
                                    <div className="dashboardHeaderNewHead"><p>APPLICATION<br />FEES USD</p></div>
                                    <div className="dashboardHeaderNewText"><p>$ {this.calcAmount(String(totalAppFeeUSD))}<span style={{ fontSize: "15px", marginLeft: "3px" }}>{this.calcBaseAmount(String(totalAppFeeUSD))}</span></p></div>
                                </div>
                            </div>
                            <div className="dashboardWrapper">
                                <div className="dashboardContainer">
                                    <div className="box box-1" >
                                        <div className="dashboardTotalAmount">Total<br></br>Students</div>
                                        <div className="dashboardTotalStudent">
                                            <div className="dash-tstudent">{totalStudent}</div>
                                        </div>
                                    </div>
                                    <div className="box box-2">
                                        {(Object.keys(this.state.tuitionFees).length == 0) ?
                                            <>
                                                <div className="dashboardTotalAmount">Total<br />Fees</div>
                                                <div className="dashboardAmountPrice">
                                                    {/* <div className="dash-inr">₹</div> */}
                                                    <div className="dashprice"><span className="dash-inr">₹</span>0.00</div>
                                                </div>
                                            </> :
                                            <>
                                                <div className="dashboardTotalAmount">Total <br />Fees</div>
                                                <div className="dashboardAmountPrice">
                                                    {/* <div> */}
                                                    {/* <div className="dash-inr">₹</div> */}
                                                    <div className="dashprice"><span className="dash-inr">₹</span>{this.calcAmount(String((Object.values(tuitionFees)).reduce((a, b) => a + b, 0)))}<span className="amount-base">{this.calcBaseAmount(String((Object.values(tuitionFees)).reduce((a, b) => a + b, 0)))}</span></div>
                                                    {/* </div> */}
                                                </div>
                                            </>
                                        }
                                    </div>
                                    {/* <div className="box box-2">
                                        {(Object.keys(this.state.tuitionFees).length == 0) ?
                                            <>
                                                <div className="dashboardTotalAmount">Total<br></br>Tuition Fees</div>
                                                <div className="dashboardAmountPrice">
                                                    <div className="dash-inr">₹</div>
                                                    <div className="dashprice">0.00</div>

                                                </div>
                                            </> :
                                            <>
                                                {Object.keys(tuitionFees).map(item => {
                                                    return (
                                                        <>
                                                            <div className="dashboardTotalAmount">Total <br></br>{item}</div>
                                                            <div className="dashboardAmountPrice">
                                                                <div className="dash-inr">₹</div>
                                                                <div className="dashprice">{this.calcAmount(String(tuitionFees[item]))}<span className="amount-base">{this.calcBaseAmount(String(tuitionFees[item]))}</span></div>

                                                            </div>
                                                        </>
                                                    )

                                                })}
                                            </>
                                        }

                                    </div> */}

                                    <div className="box box-3">
                                        <div className="dashboardTotalAmount">Total<br></br> Demand Amount</div>
                                        <div className="dashboardAmountPrice">
                                            {/* <div className="dash-inr">₹</div> */}
                                            <div className="dashprice"><span className="dash-inr">₹</span>{this.calcAmount(String(totalDemand))}<span className="amount-base">{this.calcBaseAmount(String(totalDemand))}</span></div>
                                            {/* <div className="dash-inr">Cr</div> */}
                                        </div>
                                    </div>
                                    <div className="box box-4">
                                        <div className="dashboardTotalAmount">Total <br></br>Fees Collected</div>
                                        <div className="dashboardAmountPrice">
                                            {/* <div className="dash-inr">₹</div> */}
                                            <div className="dashprice"><span className="dash-inr">₹</span>{this.calcAmount(String(feesCollected))}<span className="amount-base">{this.calcBaseAmount(String(feesCollected))}</span></div>
                                            {/* <div className="dash-inr">Cr</div> */}
                                        </div>
                                    </div>
                                    <div className="box box-5">
                                        <div className="dashboardTotalAmount">Total <br></br>fees Pending</div>
                                        <div className="dashboardAmountPrice">
                                            {/* <div className="dash-inr">₹</div> */}
                                            <div className="dashprice"><span className="dash-inr">₹</span>{this.calcAmount(String(pendingAmount))}<span className="amount-base">{this.calcBaseAmount(String(pendingAmount))}</span></div>
                                            {/* <div className="dash-inr">Cr</div> */}
                                        </div>
                                    </div>
                                    <div className="box box-6">
                                        <div className="dashboardTotalAmount">Total <br></br>fees Refunded</div>
                                        <div className="dashboardAmountPrice">
                                            {/* <div className="dash-inr">₹</div> */}
                                            <div className="dashprice"><span className="dash-inr">₹</span>{this.calcAmount(String(refundAmount))}<span className="amount-base">{this.calcBaseAmount(String(refundAmount))}</span></div>
                                        </div>
                                    </div>

                                </div>
                                <div className="dashboard-graph-wrapper">
                                    <div className="dashboardChartContainer1">
                                        <div className="chartBox2 chart-month">
                                            {/* <Line data={this.state.linedata3} options={this.state.options3} /> */}
                                            <Bar data={this.state.linedata3} options={this.state.ppChartOptions} />
                                        </div>

                                        <div className="dashname-container">
                                            <div className="feeText1">Fees Trend</div>
                                        </div>
                                    </div>
                                    <div className="dashboardChartContainer2">
                                        <div className="chartBox chart-proplan" style={{ display: "flex", justifyContent: "space-evenly" }}>
                                            <div style={{ width: "27%", height: "314px", textAlign: "center" }}>
                                                <div className="pie-chart-title">Total Demand<br />Amount</div>
                                                {/* <div className="dash-piechart-nodata">No graph available</div> */}
                                                <div style={{ height: "250px" }}><Doughnut data={this.state.pieData} options={this.state.pieOptions} /></div>
                                            </div>
                                            <div style={{ width: "27%", height: "314px", textAlign: "center" }}>
                                                <div className="pie-chart-title">Total Fees<br />Collected</div>
                                                <div style={{ height: "250px" }}><Doughnut data={this.state.pieData1} options={this.state.pieOptions} /></div>
                                            </div>
                                            <div style={{ width: "27%", height: "314px", textAlign: "center" }}>
                                                <div className="pie-chart-title">Total Fees<br />Pending</div>
                                                <div style={{ height: "250px" }}><Doughnut data={this.state.pieData2} options={this.state.pieOptions} /></div>
                                            </div>
                                        </div>
                                        <div className="dashname-container2">
                                            <div className="feeText2">Fees By Program Plans</div>
                                        </div>
                                    </div>
                                </div>
                                {/* <div className="dashboard-graph-wrapper" style={{ marginTop: "5%" }}>
                                    <div className="dashboardChartContainer1">
                                        <div className="chartBox2 chart-month">
                                            <Line data={this.state.linedata3} options={this.state.options3} />
                                        </div>

                                        <div className="dashname-container">
                                            <div className="feeText1">Fees Trend</div>
                                        </div>
                                    </div>
                                    <div className="dashboardChartContainer2">
                                        <div className="chartBox chart-proplan">
                                            <Bar data={this.state.linedata2} options={this.state.ppChartOptions} />
                                        </div>
                                        <div className="dashname-container2">
                                            <div className="feeText2">Fees By Program Plans</div>
                                        </div>
                                    </div>
                                </div> */}
                            </div >
                        </div>
                    </div>
                }
            </React.Fragment >
        );
    }
}

export default ZqDashboard;