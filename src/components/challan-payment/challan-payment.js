import React from 'react';
import '../../scss/challan-payment.scss';
import { Steps } from 'rsuite';
import './challan.scss';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
class ChallanPayment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tableContent: [
                {
                    "name": "Term-1",
                    "amount": 57300
                },
                {
                    "name": "Bank Charges",
                    "amount": 20
                }
            ],
            txnFormValue: "885",
            activeStep: 0
        }
    }
    getTxnData = (e) => {
        this.setState({ txnFormValue: e.target.value })
    }
    getTotalValue = () => {
        let totalValue = "";
        this.state.tableContent.forEach((data) => {
            totalValue = totalValue + data.amount
        })
        return totalValue
    }
    changeStepperNext = () => {
        this.setState({ activeStep: this.state.activeStep + 1 })
    }
    changeStepperFinish = () => {
        this.setState({ activeStep: 0 })
    }
    render() {
        return (
            <div className="challan-main-section">
                <div style={{ margin: "20px 0px 10px 0px", display: "flex" }}>
                    <div style={{ width: "6%", textAlign: "center" }}>
                        <img style={{ height: "38px", width: "40px" }} src="https://uploads-ssl.webflow.com/5ee891a3891ce1f991bcd2d6/5ef71159da6e9d49c1e2748c_favicon-32x32.png" />
                    </div>
                    <div className="zenqore-stepper-section" style={{ width: "90%" }}>
                        <Steps current={this.state.activeStep} currentStatus="process" vertical={false} >
                            <Steps.Item title="View Details" />
                            <Steps.Item title="Enter Transaction ID" />
                            <Steps.Item title="Confirmation" />
                        </Steps>
                    </div>
                </div>
                {this.state.activeStep === 0 ?
                    <>
                        <div className="challan-header-section">
                            <p className="header-data">Fee Collection details</p>
                        </div>
                        <div className="challan-body-section">
                            <table className="challan-table">
                                <tr>
                                    <td>Reg ID: <span>396</span></td>
                                    <td>Name: <span> Natasha Sasnur </span></td>
                                    <td>Class: <span>Class 9</span></td>
                                    <td>Date: <span>19/03/2021</span></td>
                                </tr>
                            </table>
                            <table className="challan-amount">
                                <thead>
                                    <tr>
                                        <td className="top-table-data">S.No.</td>
                                        <td className="top-table-data">ITEM</td>
                                        <td className="top-table-data">AMOUNT (₹)</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.tableContent.map((data, i) => {
                                        return (
                                            <tr>
                                                <td style={{ width: "10%" }}>{i + 1}</td>
                                                <td style={{ width: "60%", textAlign: "left" }}>{data.name}</td>
                                                <td style={{ width: "30%", textAlign: "right" }}>{Number(data.amount).toFixed(2)}</td>
                                            </tr>
                                        )
                                    })}
                                    <tr>
                                        <td className="table-last-row" colSpan="2">Total (INR)</td>
                                        <td className="table-last-row">{Number(57320).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div style={{ textAlign: "center", width: "100%" }}>
                                <p className="txn-header-name">Transaction Id<span style={{ color: "red", paddingLeft: "3px" }}>*</span></p>
                                <input type="text" className="trasaction-number-input" onChange={this.getTxnData} disabled={true} />
                            </div>
                            {/* <div style={{ width: "100%", textAlign: "center", margin: "10px 0px 0px 0px" }}>
                            <button className="challan-submit-btn" style={{ opacity: this.state.txnFormValue == "" ? 0.7 : 1.0 }} disabled={this.state.txnFormValue == "" ? true : false} >Submit</button>
                        </div> */}
                        </div>
                    </> : null}
                {this.state.activeStep === 1 ?
                    <>
                        <div className="challan-header-section">
                            <p className="header-data">Fee Collection details</p>
                        </div>
                        <div className="challan-body-section">
                            <table className="challan-table">
                                <tr>
                                    <td>Reg ID: <span>396</span></td>
                                    <td>Name: <span> Natasha Sasnur </span></td>
                                    <td>Class: <span>Class 9</span></td>
                                    <td>Date: <span>23/03/2021</span></td>
                                </tr>
                            </table>
                            <table className="challan-amount">
                                <thead>
                                    <tr>
                                        <td className="top-table-data">S.No.</td>
                                        <td className="top-table-data">ITEM</td>
                                        <td className="top-table-data">AMOUNT (₹)</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.tableContent.map((data, i) => {
                                        return (
                                            <tr>
                                                <td style={{ width: "10%" }}>{i + 1}</td>
                                                <td style={{ width: "60%", textAlign: "left" }}>{data.name}</td>
                                                <td style={{ width: "30%", textAlign: "right" }}>{Number(data.amount).toFixed(2)}</td>
                                            </tr>
                                        )
                                    })}
                                    <tr>
                                        <td className="table-last-row" colSpan="2">Total (INR)</td>
                                        <td className="table-last-row">{Number(57320).toLocaleString('en-IN', { style: 'currency', currency: 'INR' })}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div style={{ textAlign: "center", width: "100%" }}>
                                <p className="txn-header-name">Transaction Id<span style={{ color: "red", paddingLeft: "3px" }}>*</span></p>
                                <input type="text" className="trasaction-number-input" onChange={this.getTxnData} disabled={true} defaultValue={885} />
                            </div>
                            <div style={{ width: "100%", textAlign: "center", margin: "10px 0px 0px 0px" }}>
                                <button className="challan-submit-btn" style={{ opacity: this.state.txnFormValue == "" ? 0.7 : 1.0 }} disabled={this.state.txnFormValue == "" ? true : false} onClick={this.changeStepperNext}>Submit</button>
                            </div>
                        </div>
                    </> : null}
                {this.state.activeStep === 2 ?
                    <>
                        <div className="confirmation-response" style={{padding:"100px 0px 20px 0px"}}>
                            <CheckCircleIcon className="check-circle" style={{fontSize:"50px"}} />
                            <p className="success-para">Challan added successfully</p>
                            <p className="success-para">Student ID: <strong>396</strong></p>
                            <p className="success-para">Student Name: <strong>Natasha Sasnur</strong></p>
                        </div>
                    </>
                    : null}
                <div className="footer-button">
                    <div className="footer-left">
                        {this.state.activeStep == 1 || this.state.activeStep == 2 ? null : <button className="stepper-next-button" onClick={this.changeStepperNext}>Next</button>}
                    </div>
                </div>
                <div className="footer-button">
                    <div className="footer-left">
                        {this.state.activeStep == 2 ? <button className="stepper-cancel-button1" onClick={this.changeStepperFinish}>Finish</button> : null}
                    </div>
                </div>

            </div>
        )
    }
}
export default ChallanPayment;