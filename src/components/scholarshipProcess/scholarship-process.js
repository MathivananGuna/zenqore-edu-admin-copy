import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router-dom';
import Loader from '../../utils/loader/loaders';
import ScholarshipForm from './scholarshipForm.json';
import Axios from 'axios';
import '../../scss/login.scss';
import ZenForm from '../input/form';
import '../../scss/receive-payment.scss';
import '../../scss/student-reports.scss';
import '../../scss/transaction-scholarship.scss';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { Alert } from 'rsuite';

class ScholarshipProcess extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ScholarshipForm: ScholarshipForm,
            env: JSON.parse(localStorage.getItem('env')),
            orgId: localStorage.getItem('orgId'),
            userId: localStorage.getItem('userId'),
            type: localStorage.getItem('type'),
            scholarshipID: localStorage.getItem('scholaId'),
            isLoader: false,
            name: '',
            amount: 0,
            adjustCurrentAmount: '',
            adjustNextAmount: '',
            refundAmount: '',
            dueDate: '',
            regId: '',
            paymentTransactions: {
                modeofPayment:'',
                transactionId:'',
                branchType:'',
                bankName:'',
                chequeStatus:'',
                utrNumber:'',
                netBankingType:''
            },
            taskId:''
        }
    }
    componentDidMount() {
        console.log(window.location.href)
        if (window.location.href.includes('?')) {
            let { newpath } = this.state;
            let location = window.location.href;
            const scholarshipID = location.split("?")
            let params = scholarshipID[1].split('&')
            var id = params[0].split('scholarshipId=')
            var type = params[1].split('type=')
            var orgId = params[2].split('orgId=')
            var userId = params[3].split('userId=')

            this.props.history.push(newpath)
            if (scholarshipID[1] != undefined) {
                this.setState({ scholarshipID: id[1], type: type[1], orgId: orgId[1] ? orgId[1] : orgId[0] ,userId :userId[1]});
                localStorage.setItem('orgId', orgId[1] ? orgId[1] : orgId[0])
                localStorage.setItem('type', type[1])
                localStorage.setItem('scholarId', id[1])
                localStorage.setItem('userId', userId[1])
            }
        }
        let amount = ''
        Axios.get(`${this.state.env['zqBaseUri']}/edu/scholarshipDetails/${localStorage.getItem('scholarId')}?orgId=${localStorage.getItem('orgId')}`)
            .then(response => {
                console.log('scholarship details', response);
                const { paymentTransactions } = this.state;
                let respData = response.data
                let regId = respData.scholarshipDetails.studentRegId
                let name = respData.scholarshipDetails.studentName
                amount = respData.scholarshipDetails.amount
                // let userId = respData.scholarshipDetails.createdBy
                paymentTransactions['modeofPayment'] = respData.scholarshipDetails.data.modeofPayment
                paymentTransactions['transactionId'] = respData.scholarshipDetails.data.paymentTransactionId
                paymentTransactions['branchType'] = respData.scholarshipDetails.data.branch
                paymentTransactions['bankName'] = respData.scholarshipDetails.data.issuedBank
                paymentTransactions['chequeStatus'] = respData.status
                paymentTransactions['utrNumber'] = respData.scholarshipDetails.data.utrNumber
                paymentTransactions['netBankingType'] = ''
                this.amountDetails(regId,amount)
                this.setState({ regId, name, amount,paymentTransactions })
                this.setState({ isLoader: false })
            })
            .catch(err => {
                console.log('scholarship details', err)
                this.setState({ isLoader: false })
            })
            /// For Task Id
            Axios.get(`${this.state.env['zqBaseUri']}/edu/tasks/displayName?orgId=${localStorage.getItem('orgId')}`)
            .then(response => {
                console.log("taskId",response)
                this.setState({taskId:response.data.taskId})
            }).catch(error =>{
                console.log("taskId",error)
            })

        Object.keys(this.state.ScholarshipForm).map(key => {
            this.state.ScholarshipForm[key].map(data => {
                if (data.name == 'amount') {
                    data['defaultValue'] = amount !== undefined ? String(Number(amount).toFixed(2)) : null
                    data['validation'] = true
                    data['errorMsg'] = ""
                }
            })
        })
        this.setState(this.state.ScholarshipForm);
     }

    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    amountDetails = (regId, amount) => {
        this.setState({ isLoader: true })
        let studentID = []
        studentID.push({
            'regId': regId,
            'sanctionedAmount': amount 
        })
        Axios.post(`${this.state.env['zqBaseUri']}/edu/transactions/getStudentStatus?orgId=${localStorage.getItem('orgId')}`, studentID
        ).then(response => {
            console.log("post response", response.data)
            let respData = response.data[0]
            let adjustCurrentAmount = ''
            let adjustNextAmount = ''
            let refundAmount = ''
            let dueDate = ''
            dueDate = respData.dueDate 
            if (this.state.type == 'adjustRefund') {
                adjustNextAmount = respData.status[0].adjust.adjust
                adjustCurrentAmount = respData.status[0].refund.adjust
                refundAmount = respData.status[0].adjust.refund
            }
            else if (this.state.type == 'refundAdjust') {
                adjustNextAmount = respData.status[0].refund.adjust
                adjustCurrentAmount = respData.status[0].refund.adjust
                refundAmount = respData.status[0].refund.refund
            }
            else {
                dueDate = respData.dueDate
            }
            this.setState({ isLoader: false })
            console.log("***dueDate", dueDate)
            this.setState({ adjustCurrentAmount, refundAmount, adjustNextAmount, dueDate })
        }).catch(err => {
            console.log("post response", err)
            this.setState({ isLoader: false })
        })
    }
    onSubmit = (data) => {
        let  { paymentTransactions } = this.state
        this.setState({ isLoader: true })
        let payload = {
            "taskId": this.state.taskId,
            "title": `Process for ${localStorage.getItem('scholarId')}`,
            "description": `task is created for ${localStorage.getItem('scholarId')}`,
            "action": "eduFees",
            "status": "New",
            "type": localStorage.getItem('type'),
            "dueDate": this.state.dueDate,
            "assignedTo": this.state.userId,
            "data": {
                "yearAdjust": 'CurrentYear',
                "studentId": this.state.regId,
                "studentName": this.state.name,
                "bankName": data.bankName !== undefined ? data.bankName.value : '',
                "bankIfsc": data.bankifsc !== undefined ? data.bankifsc.value : '',
                "bankAccountNo": data.accountnumber !== undefined ? data.accountnumber.value : '',
                "bankAccountName": data.accountName !== undefined ? data.accountName.value : '',
                "amount": this.state.amount,
                "adjustCurrentAmount": this.state.adjustCurrentAmount,
                "adjustNextAmount": this.state.adjustNextAmount,
                "refundAmount": this.state.refundAmount,
                "modeofPayment": paymentTransactions.modeofPayment,
                "transactionId": paymentTransactions.transactionId,
                "branchType": paymentTransactions.branchType,
                // "bankName":paymentTransactions.bankName,
                "chequeStatus": paymentTransactions.chequeStatus,
                "utrNumber": paymentTransactions.utrNumber,
                "netBankingType": paymentTransactions.netBankingType

            }
            
            // "status": "New",
            // "title": `Process for ${localStorage.getItem('scholarId')}`,
            // "type": localStorage.getItem('type'),
            // "taskId": this.state.taskId,
            // "desc": `task is created for ${localStorage.getItem('scholarId')}`,
            // "assignedTo": this.state.userId,
            // "dueDate": this.state.dueDate,
            // "orgId": this.state.orgId,
            // "taskDetails": {
            //     "yearAdjust": 'CurrentYear',
            //     "studentId": this.state.regId,
            //     "studentName": this.state.name,
            //     "bankName": data.bankName !== undefined ? data.bankName.value : '',
            //     "bankIfsc": data.bankifsc !== undefined ? data.bankifsc.value : '',
            //     "bankAccountNo": data.accountnumber !== undefined ? data.accountnumber.value : '',
            //     "bankAccountName": data.accountName !== undefined ? data.accountName.value : '',
            //     "amount": this.state.amount,
            //     "adjustCurrentAmount": this.state.adjustCurrentAmount,
            //     "adjustNextAmount": this.state.adjustNextAmount,
            //     "refundAmount": this.state.refundAmount,
            //     "modeofPayment": paymentTransactions.modeofPayment,
            //     "transactionId": paymentTransactions.transactionId, 
            //     "branchType" : paymentTransactions.branchType,
            //     // "bankName":paymentTransactions.bankName,
            //     "chequeStatus": paymentTransactions.chequeStatus,
            //     "utrNumber": paymentTransactions.utrNumber,
            //     "netBankingType" : paymentTransactions.netBankingType
            //  }
        }
        Axios.post(`${this.state.env['zqBaseUri']}/edu/tasks?orgId=${this.state.orgId}`, payload, {
            headers: {
                // "Authorization": localStorage.getItem('auth_token')
            }
        }).then(response => {
            console.log("post response", response)
            this.setState({ type: 'afterSubmit' })
            this.setState({ isLoader: false })

        }).catch(err => {
            console.log("post response", err)
            Alert.error("api response Failed !")
            this.setState({ isLoader: false })
        })
    }
    finishProcess = () => {
        this.setState({ isLoader: true }, () => {
            window.location.replace('https://www.hkbk.edu.in')
        });

    }
    formInputChanges = (value, item, e, dataS) => {
    }
    resetForm = () => {
        let newData = this.state.ScholarshipForm
        newData.map((task) => {
            task["error"] = false;
            task['validation'] = false;
            task['errorMsg'] = "";
            task['defaultValue'] = "";
        })
        this.setState({ ScholarshipForm: newData })
    }

    render() {
        return (
            <React.Fragment>
                {this.state.isLoader ? <Loader /> : null}
                {this.state.type == 'adjust' ? <React.Fragment>
                    <div className="adjust-div-wrapper scholarship-process">
                        <div>
                            <div className="textCenter">
                                <h5 className="login-form-hd">Hello {this.state.name},</h5> <br />
                                <p className="payment-confirm-text" style={{ fontSize: 17 }}><b> Your request for carry forward as part of the processing of scholarship ID {this.state.scholarshipID} is as follows:</b> </p><br />
                            </div>
                            < div style={{ paddingBottom: "20px" }}>
                                <table className="receive-pay-tableFormat">
                                    <thead className="receive-pay-tableFormatHead">
                                        <th>Action</th>
                                        <th>Year</th>
                                        <th >Amount (INR)</th>
                                    </thead>
                                    <tbody >
                                        <tr>
                                            <td className="scholarship-action-type">Adjust</td>
                                            <td className="scholarship-action-type">Current Year</td>
                                            <td className="scholarship-num-type">{Number(this.state.amount).toFixed(2)}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="textCenter">
                                <p className="payment-confirm-text" style={{ fontSize: 17 }}><b>Please click on the following button to confirm your Adjustment request</b> </p>
                            </div>
                                 <div className="confirm-btns textCenter">
                                   <Button className="confirm-payment-btn" style={{ marginTop: 20 }} onClick={this.onSubmit}>Adjust</Button>
                                 </div>

                        </div>
                    </div>
                </React.Fragment> : null}

                {this.state.type == 'refund' ? <React.Fragment>
                    <div className='refundWrapper' style={{ height: '100vh', overflowY: 'scroll', paddingBottom: "30px" }}>
                        <div className="zenqore-leftside-wrap scholarship-process">
                            <div style={{ textAlign: 'Center' }}>
                                <h5 className="login-form-hd">Hello {this.state.name},</h5>
                                <p style={{ fontSize: "17px", fontWeight: "600" }} >Your request for refund as part of the processing of scholarship ID {this.state.scholarshipID} is as follows: </p>
                            </div>
                            < div style={{ paddingTop: "20px", width: '45%', margin: '0 auto' }}>
                                <table className="receive-pay-tableFormat">
                                    <thead className="receive-pay-tableFormatHead">
                                        <th>Action</th>
                                        <th>Year</th>
                                        <th >Amount (INR)</th>
                                    </thead>
                                    <tbody >
                                        <tr>
                                            <td className="scholarship-action-type">Refund</td>
                                            <td className="scholarship-action-type">Current Year</td>
                                            <td className="scholarship-num-type">{Number(this.state.amount).toFixed(2)}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="scholarship-action-type scholarship-padding-wrap">  <p style={{ fontSize: "17px", fontWeight: "600" }} >Please provide your bank and account details and click on the following button to confirm your refund request</p></div>
                        </div>
                        <div className="finalYear-box">
                            <ZenForm inputData={ScholarshipForm.RefundData} onInputChanges={this.formInputChanges} onSubmit={this.onSubmit} />
                        </div>
                    </div>
                </React.Fragment> : null}

                {this.state.type == 'adjustRefund' ? <React.Fragment>
                    <div className='refundWrapper' style={{ height: '100vh', overflowY: 'scroll', paddingBottom: "20px" }}>
                        <div className="zenqore-leftside-wrap scholarship-process">
                            <div className="scholarship-action-type scholarship-padding-wrap">
                                <h5 className="login-form-hd">Hello {this.state.name},</h5>
                                <p style={{ fontSize: "17px", fontWeight: "600" }} >Your request for carry forward as part of the processing of scholarship ID {this.state.scholarshipID} is as follows: </p>
                            </div>
                            < div style={{ paddingBottom: "20px", width: '45%', margin: '0 auto' }}>
                                <table className="receive-pay-tableFormat">
                                    <thead className="receive-pay-tableFormatHead">
                                        <th>Action</th>
                                        <th>Year</th>
                                        <th>Amount (INR)</th>
                                    </thead>
                                    <tbody >
                                        <tr>
                                            <td className="scholarship-action-type">Refund</td>
                                            <td className="scholarship-action-type">Current Year</td>
                                            <td className="scholarship-num-type">{Number(this.state.refundAmount).toFixed(2)}</td>
                                        </tr>
                                        <tr>
                                            <td className="scholarship-action-type">Adjust</td>
                                            <td className="scholarship-action-type">Current Year</td>
                                            <td className="scholarship-num-type">{Number(this.state.adjustCurrentAmount).toFixed(2)}</td>
                                        </tr>
                                        <tr>
                                            <td className="scholarship-action-type">Adjust</td>
                                            <td className="scholarship-action-type">Next Year</td>
                                            <td className="scholarship-num-type">{Number(this.state.adjustNextAmount).toFixed(2)}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="scholarship-action-type scholarship-padding-wrap">  <p style={{ fontSize: "17px", fontWeight: "600" }} >Please provide your bank and account details and click on the following button to  confirm your Adjustment request</p></div>
                        </div>
                        <div className="finalYear-box">
                            <ZenForm inputData={ScholarshipForm.RefundData} onInputChanges={this.formInputChanges} onSubmit={this.onSubmit} />
                        </div>
                    </div>
                </React.Fragment> : null}

                {this.state.type == 'refundAdjust' ? <React.Fragment>
                    <div className='refundWrapper' style={{ height: '100vh', overflowY: 'scroll', paddingBottom: "20px" }}>
                        <div className="zenqore-leftside-wrap scholarship-process">
                            <div className="scholarship-action-type scholarship-padding-wrap">
                                <h5 className="login-form-hd">Hello {this.state.name},</h5>
                                <p style={{ fontSize: "17px", fontWeight: "600" }} >Your request for refund as part of the processing of scholarship ID {this.state.scholarshipID} is as follows: </p>
                            </div>
                            < div style={{ paddingBottom: "20px", width: '45%', margin: '0 auto' }}>
                                <table className="receive-pay-tableFormat">
                                    <thead className="receive-pay-tableFormatHead">
                                        <th>Action</th>
                                        <th>Year</th>
                                        <th>Amount (INR)</th>
                                    </thead>
                                    <tbody >
                                        <tr>
                                            <td className="scholarship-action-type">Refund</td>
                                            <td className="scholarship-action-type">Current Year</td>
                                            <td className="scholarship-num-type">{Number(this.state.refundAmount).toFixed(2)}</td>
                                        </tr>
                                        <tr>
                                            <td className="scholarship-action-type">Adjust</td>
                                            <td className="scholarship-action-type">Current Year</td>
                                            <td className="scholarship-num-type">{Number(this.state.adjustCurrentAmount).toFixed(2)}</td>
                                        </tr>
                                        <tr>
                                            <td className="scholarship-action-type">Adjust</td>
                                            <td className="scholarship-action-type">Next Year</td>
                                            <td className="scholarship-num-type">{Number(this.state.adjustNextAmount).toFixed(2)}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="scholarship-action-type scholarship-padding-wrap">  <p style={{ fontSize: "17px", fontWeight: "600" }} >Please provide your bank and account details and click on the following button to confirm your refund request</p></div>
                        </div>
                        <div className="finalYear-box">
                            <ZenForm inputData={ScholarshipForm.RefundData} onInputChanges={this.formInputChanges} onSubmit={this.onSubmit} />
                        </div>
                    </div>
                </React.Fragment> : null}

                {this.state.type == 'afterSubmit' ?
                    <div className="receive-payment-main">
                        <div className="payment-confirm-page">
                            <CheckCircleIcon className="confirm-icon" />
                            <p className="payment-confirm-text" style={{ fontSize: 17 }}><b>Your request for {localStorage.getItem('type') == 'adjust' || localStorage.getItem('type') == 'adjustRefund' ? "carry forward" : "refund"}  as part of the processing of scholarship ID {this.state.scholarshipID} has been registered.</b> </p>
                            <br />
                            <p className="payment-confirm-text" style={{ fontSize: 17 }}><b>We will notify you shortly.</b> </p>
                            <div className="confirm-btns">
                                <Button className="confirm-payment-btn" style={{ marginTop: 20 }} onClick={this.finishProcess}>DONE</Button>
                            </div>
                        </div>

                    </div> : null}
            </React.Fragment >
        )
    }
}
export default withRouter(ScholarshipProcess);