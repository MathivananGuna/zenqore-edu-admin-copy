import React, { Component } from 'react';
import PaymentForm from '../../utils/payment/payment';
import PaymentData from './payment_form.json';
import axios from 'axios';
// import moment from 'moment';
// import { Alert } from 'rsuite';
import '../../scss/common-payment.scss';
import '../../scss/receive-payment.scss';
import { CircularProgress } from "@material-ui/core";
// import { getWeekYearWithOptions } from 'date-fns/fp';
import DateFormatContext from '../../gigaLayout/context';
class Payment extends Component {

    constructor(props) {
        super(props);
        this.state = {
            paymentType: this.props.paymentType != "" ? this.props.paymentType : "Cash",
            env: JSON.parse(localStorage.getItem('env')),
            email: localStorage.getItem('email'),
            channel: localStorage.getItem('channel'),
            authToken: localStorage.getItem('auth_token'),
            orgId: localStorage.getItem('orgId'),
            orgName: localStorage.getItem('orgName'),
            PaymentFormData: [],
            paymentTypes: ['Cash', "Cheque/DD", "Card", "Netbanking", "Wallet"],
            txnReceiptPaymentBank: '',
            paymentItem: {},
            date: new Date(),
            amount: '0',
            dueAmount: '0',
            paid: '0',
            paidPer: '1%',
            duePer: '99%',
            progressStatus: false,
            cardType: '',
            transactionType: '',
            walletType: '',
            payload: undefined,
            modal: false,
            paymentResponse: false,
            errorOccured: false,
            withoutAttachment: true,
            addAttachment: undefined,
            ViewpaymentDetails: [],
            formChanges: false,
            BankName: '',
            gstin: '',
            zqAccountNo: '',
            txnforBranchID: '',
            studentData: this.props.studentData,
            paymentAmount: this.props.paymentAmount,
            studentID: this.props.studentID,
            studentid: "",
            studentFeeID: [],
            transactionNumber: '',
            bankName: '',
            BranchType: '',
            chequeStatus: '',
            netBankingType: '',
            paidAmount: '',
            circularLoader: false,
            academicYear: '',
            StudentClass: '',
            studentName: '',
            programPlan: '',
            typeReceipt: '',
            creditDebit: '',

        }
    }
    static contextType = DateFormatContext;
    componentWillMount() {
        let studentData = this.props.studentData
        console.log(studentData)
        if (this.props.studentData !== undefined) {
            // let id = studentData[0].studentFeeMapDetails.studentId;
            let id = studentData.studentDetails['_id']
            let academicYear = studentData.programPlanDetails.academicYear;
            let StudentClass = studentData.programPlanDetails.title;
            let studentName = studentData.studentDetails.firstName + " " + studentData.studentDetails.lastName;
            let studentRegId = studentData.studentDetails.regId;
            let programPlan = studentData.studentDetails.programPlanId
            let typeReceipt = studentData.receiptStatus == '' ? 'immediately' : studentData.receiptStatus
            let relatedStudentFeeId = []
            if (studentData.demandNoteDetails.length !== 0) {
                studentData.demandNoteDetails.map(item => {
                    relatedStudentFeeId.push(item.displayName)
                })
            }
            this.setState({
                studentid: id, academicYear: academicYear, StudentClass: StudentClass, typeReceipt: typeReceipt,
                studentName: studentName, studentRegId, studentRegId, programPlan: programPlan, studentFeeID: relatedStudentFeeId
            })
        }
    }
    componentDidMount = () => {
        console.log("paymentDate", this.props.paymentDate)
        let today = new Date().toDateString()
        let paymentType = this.props.paymentType != "" ? this.props.paymentType : "Cash";
        var PaymentFormData = this.props.PaymentFormData != undefined ? this.props.PaymentFormData[0] : null;
        this.setState({ paymentType: paymentType })
        PaymentData.forEach(task => {
            delete task['defaultValue']
            task['readOnly'] = false

        })
        PaymentData.map((item) => {
            if (item.name === "date") {
                item["defaultValue"] = this.props.paymentDate || today;
            }
            if (item.name === "amount") {
                item["defaultValue"] = Number(this.props.paymentAmount).toFixed(2);
                item['required'] = false
                item['readOnly'] = true
            }
        });
        if (PaymentFormData != undefined) {
            let objKeys = Object.keys(PaymentFormData);
            objKeys.map(key => {
                PaymentData.map((item) => {
                    let itemName = item.name != undefined ? item.name.toLowerCase().replace(/ /g, "") : "";
                    let keyName = key.toLowerCase().replace(/ /g, "");
                    if (keyName == itemName) {
                        item["defaultValue"] = PaymentFormData[key];
                        item["readOnly"] = true;
                        item["required"] = false;
                    }
                    if (item.name === "amount") {
                        item["defaultValue"] = PaymentFormData.amount;
                        item['readOnly'] = true
                        item['type'] = "text"
                    }
                    if (item.name === "date") {
                        item["defaultValue"] = this.props.paymentDate || today;
                        item['readOnly'] = true
                    }

                });
            })
        }
        this.setState({ PaymentFormData: PaymentData, paidAmount: Number(this.props.paymentAmount).toFixed(2) })
    }
    dateFilter = (ev) => {
        var ts = new Date(ev);
        let getDate = `${String(ts.getDate()).length == 1 ? `0${ts.getDate()}` : ts.getDate()}`;
        let getMonth = `${String(ts.getMonth() + 1).length == 1 ? `0${ts.getMonth() + 1}` : ts.getMonth() + 1}`;
        let getYear = `${ts.getFullYear()}`;
        let today = `${getDate}/${getMonth}/${getYear}`;
        return today;
    };

    onPaymentModeSelection = (type) => {
        console.log(type)
        this.setState({ paymentType: type })
        if (this.props.isSubmit) {
            this.props.onPaymentType(type)
        }
    }

    onPaymentSubmit = (data, formItem, format) => {
        console.log(data)
        let endPoint = this.props.endPoint ? this.props.endPoint : "otcPayments"
        if (this.props.isSubmit) {
            this.props.refundPayment(data, formItem, format)
        }
        else {
            this.setState({ circularLoader: true }, () => {
                console.log(this.state.PaymentFormData)
                let payloadData = this.state.studentData;
                if (data.amount.value <= 0) {
                    alert("Entered amount is not valid.")
                    this.setState({ circularLoader: false })
                } else {
                    var formdata = new FormData()
                    let payload = {
                        "transactionDate": String(this.state.date),
                        "relatedTransactions": this.state.studentFeeID,
                        "studentId": this.state.studentid,
                        // "emailCommunicationRefIds": payloadData[0].demandNote.emailCommunicationRefIds[0],
                        "emailCommunicationRefIds": payloadData.guardianDetails.email == 'NA' || payloadData.guardianDetails.email == '' || payloadData.guardianDetails.email == '-' || payloadData.guardianDetails.email == null ? payloadData.studentDetails.email : payloadData.guardianDetails.email,
                        "transactionType": "eduFees",
                        "transactionSubType": "feePayment",
                        "studentFeeMap": payloadData.studentFeeMapId,
                        "amount": Number(data.amount.value),
                        "status": "initiated",
                        "data": {
                            "feesBreakUp": this.props.feeTypeLists,
                            "orgId": localStorage.getItem('orgId'),
                            "transactionType": "eduFees",
                            "transactionSubType": "feePayment",
                            "mode": this.state.paymentType == "Cheque/DD" ? "cheque" : this.state.paymentType.toLowerCase(),
                            "method": "otc",
                            "modeDetails": {
                                "netBankingType": null,
                                "walletType": null,
                                "instrumentNo": null,
                                "cardType": null,
                                "nameOnCard": null,
                                "cardNumber": null,
                                "instrumentDate": String(this.state.date),
                                "bankName": null,
                                "branchName": null,
                                "transactionId": null,
                                "remarks": data.remarks.value
                            },
                            // "amount": Number(data.amount.value)
                        },

                        "paymentTransactionId": null,
                        "createdBy": localStorage.getItem('userId'),
                        "campusId": localStorage.getItem('campusId') ? localStorage.getItem('campusId') : 'all',
                        "academicYear": this.state.academicYear,
                        "class": this.state.StudentClass,
                        "studentName": this.state.studentName,
                        "studentRegId": this.state.studentRegId,
                        "programPlanId": this.state.programPlan,
                        "parentName": payloadData.guardianDetails.firstName + " " + payloadData.guardianDetails.lastName,
                        "type": this.state.typeReceipt.toLowerCase() == 'immediately' ? 'receipt' : 'acknowledgement',
                    }
                    console.log("************", payload)
                    if (this.state.paymentType === 'Cash') {
                        console.log(payload)
                    } else if (this.state.paymentType == 'Cheque/DD') {
                        payload['paymentTransactionId'] = this.state.transactionNumber
                        payload.data['modeDetails']['transactionId'] = this.state.transactionNumber
                        payload.data['modeDetails']['bankName'] = data.bankname.value
                        payload.data['modeDetails']['branchName'] = data["Branch Name"].value
                        // payload.data['modeDetails']['chequeStatus'] = data.remarks.value
                        payload.data['modeDetails']['remarks'] = data.remarks.value

                    } else if (this.state.paymentType == 'Card') {
                        payload['paymentTransactionId'] = data.transactionNumber.value
                        payload.data['modeDetails']['transactionId'] = data.transactionNumber.value
                        payload.data['modeDetails']['cardType'] = this.state.cardType
                        payload.data['modeDetails']['cardNumber'] = data.cardNumber.value
                        payload.data['modeDetails']['creditDebit'] = this.state.creditDebit
                        payload.data['modeDetails']['nameOnCard'] = data.nameOnCard.value
                        payload.data['modeDetails']['remarks'] = data.remarks.value
                    }
                    else if (this.state.paymentType == 'Netbanking') {
                        payload['paymentTransactionId'] = data.UTRNumber.value
                        payload.data['modeDetails']['transactionId'] = data.UTRNumber.value
                        payload.data['modeDetails']['bankName'] = data.bankname.value
                        payload.data['modeDetails']['netBankingType'] = this.state.netBankingType
                        payload.data['modeDetails']['instrumentNo'] = data.UTRNumber.value
                        payload.data['modeDetails']['remarks'] = data.remarks.value

                    } else if (this.state.paymentType == 'Wallet') {
                        payload['paymentTransactionId'] = data.transactionNumber.value
                        payload.data['modeDetails']['transactionId'] = data.transactionNumber.value
                        payload.data['modeDetails']['walletType'] = this.state.walletType
                        payload.data['modeDetails']['remarks'] = data.remarks.value

                    }
                    this.setState({ payload: payload })
                    if (this.state.withoutAttachment) {
                        this.setState({
                            modal: true,
                            paymentResponse: false,
                            errorOccured: false
                        })
                        this.setState({
                            modal: true,
                            paymentResponse: false,
                            errorOccured: false,
                            payload: payload
                        })
                        formdata.append('json', JSON.stringify(payload));
                        formdata.append("file", this.state.file);
                        formdata.append("orgId", this.state.orgId);
                        axios.post(`${this.state.env['zqBaseUri']}/edu/${endPoint}`, payload, {
                            headers: {
                                "Authorization": localStorage.getItem('auth_token')
                            }
                        })
                            .then(response => {
                                // Alert.success("Successful Fee Payment")
                                console.log('res', response)
                                let txnNumber = response.data.data.data.displayName;
                                let receiptID = response.data.receiptKey;
                                let nextProps = {
                                    receiptID: receiptID,
                                    txnNumber: txnNumber,
                                    checkPaymentStatus: true,
                                    paidAmount: this.state.paidAmount
                                }
                                this.setState({ circularLoader: false })
                                this.props.goNextFun(nextProps)
                            }).catch(err => {
                                console.log('eee', err)
                                this.props.goNextErrFun()
                                this.setState({ circularLoader: false })
                            })
                    }

                    else {
                        return payload
                    }
                }
            })
        }
    }
    onPayment = () => {
        this.setState({
            modal: true,
            paymentResponse: false,
            errorOccured: false,
        })
    }
    onInputChanges = (value, item, e, dataS) => {
        if (this.props.isSubmit) {
            this.props.onInputChanges(dataS) 
        }
        this.setState({ formChanges: true })
        let selectedValue = dataS != undefined ? dataS.name == 'date' ? value : item.value : e.target.value;
        let type = dataS != undefined ? dataS.name : item.name
        if (dataS != undefined) {
            if (dataS.name == 'date') {
                this.setState({ date: selectedValue })
            }
            if (dataS.name == 'cardType') {
                this.setState({ cardType: selectedValue })
            }
            if (dataS.name == 'creditDebit') {
                this.setState({ creditDebit: selectedValue })
            }
            if (dataS.name == 'bankname') {
                this.setState({ txnReceiptPaymentBank: selectedValue, BankName: item.label })
            }
            if (dataS.name == 'netBankingType') {
                this.setState({ netBankingType: selectedValue })
            }

            if (dataS.name == 'walletType') {
                this.setState({ walletType: selectedValue })
            }
            if (dataS.name == "ChequeStatus") {
                this.setState({ ChequeStatus: value });
            }
        } else {
            if (item.name == "ChequeNumber") {
                this.setState({ transactionNumber: value });
            }
            if (item.name == "BranchType") {
                this.setState({ BranchType: value });
            }
            if (item.name == 'amount') {
                item['required'] = true
                this.setState({ paidAmount: value })
            }

        }
        this.onSetValue(type, selectedValue, true)
    }

    onSetValue = (type, value, focus) => {
        let PaymentFormData = this.state.PaymentFormData

        this.setState({ PaymentFormData: PaymentFormData, paymentType: this.state.paymentType }, () => {
            PaymentFormData.map(item => {
                if (this.state.paymentType == item.paymentType) {
                    if (item.name == type) {
                        item['defaultValue'] = value
                        // item['validation'] = true
                        item['focus'] = focus != undefined ? focus : false
                        this.setState({ [item.name]: value })
                    } else if (item['defaultValue'] != undefined) {
                        item['defaultValue'] = item['defaultValue']
                        item['focus'] = false
                        this.setState({ [item.name]: item['defaultValue'] })
                    }
                    else {
                        item['focus'] = false
                    }
                }
            })
            this.setState({ PaymentFormData: PaymentFormData, paymentType: this.state.paymentType })
        })
    }
    onGoToExpenseList = () => {
        if (this.state.errorOccured) {
            this.setState({
                modal: false,
                paymentResponse: false,
                errorOccured: false
            })
        } else {
            this.setState({ PaymentFormData: [] }, () => {
                PaymentData.map(pd => {
                    pd['defaultValue'] = undefined
                    pd['readOnly'] = false
                    pd['validation'] = false
                })
                this.setState({ PaymentFormData: PaymentData }, () => {
                    // this.props.goNextFun()

                })

            })

        }
    }

    onPaymentRowClick = (item) => {
        console.log('selected item', item)
        let paymentType = ''
        this.state.paymentTypes.map(type => {
            type = type == 'Netbanking' ? 'Bank' : type
            if (String(item.source).includes(type)) {
                paymentType = type
            }
        })
        this.setState({ addAttachment: item, paymentType: paymentType, PaymentFormData: [] }, () => {
            PaymentData.map(pd => {
                if (pd.name == 'date') {
                    pd['defaultValue'] = item.createdOn
                }
                if (pd.name == 'amount') {
                    pd['defaultValue'] = item.amount
                }
                if (pd.name == 'transactionNumber') {
                    pd['defaultValue'] = item.txnNo != undefined ? item.txnNo : undefined
                }
                if (pd.name == 'branchname') {
                    pd['defaultValue'] = item.branchName != undefined ? item.branchName : undefined
                }
                if (pd.name == 'bankname') {
                    pd.options.map(bank => {
                        if (bank.label == item.BankName) {
                            pd['defaultValue'] = bank.value != undefined ? bank.value : undefined
                        }
                    })

                }
                if (pd.name == 'remarks') {
                    pd['defaultValue'] = item.remarks != undefined ? item.remarks : undefined
                }
                pd['readOnly'] = true
                pd['validation'] = true
            })
            this.setState({ PaymentFormData: PaymentData })
        })
    }
    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }

    render() {

        return (<React.Fragment>
            {this.state.circularLoader ?
                <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                    <CircularProgress size={24} />
                </div> : null}
            <div className="receive-amount-table" style={{ width: "70%" }} >
                <div className="student-txt">
                    {/* <h3 className="student-details-header" style={{ height: "40px" }}></h3> */}
                    <div className='student-details' >
                        <p className="student-paraText"><b className="bold-text">Student Name:</b> {this.props.StudentName}</p>
                        {this.props.appId ? <> <p className="student-paraText"><b className="bold-text">App ID:</b> {this.props.appId}</p>
                            <p className="student-paraText"><b className="bold-text">{`${this.context.reportLabel ? this.context.reportLabel : "Reg. ID"}`}:</b> - </p>
                        </>
                            :
                            <p className="student-paraText"><b className="bold-text">{`${this.context.reportLabel ? this.context.reportLabel : "Reg. ID"}`}:</b> {this.props.StudentRegId} </p>
                        }
                        <p className="student-paraText"><b className="bold-text">Program Plan:</b> {this.props.StudentClass}</p>
                    </div>
                </div>
            </div>
            <div className="payment-tab receipt-payment">
                <div className="payment-current">
                    {this.state.PaymentFormData.length ? <PaymentForm receiptFormData={this.state.PaymentFormData} paymentType={this.state.paymentType} paymentTypes={this.state.paymentTypes} onPaymentModeSelection={this.onPaymentModeSelection} onPaymentSubmit={this.onPaymentSubmit} onInputChanges={this.onInputChanges} onPaymentPreview={this.props.onPaymentPreview} /> : null}
                </div>

            </div>
        </React.Fragment>
        )
    }
}
export default Payment;