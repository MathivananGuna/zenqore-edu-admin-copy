import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Axios from 'axios';
import "../../../scss/student-portal.scss";
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import LinearProgress from '@material-ui/core/LinearProgress';
import Loader from '../../../utils/loader/loaders';
import { instituteDetailsMappings } from "./instituteDetailsMappings";
import { Tooltip, CircularProgress, useTheme } from "@material-ui/core";
import { Alert, Modal } from 'rsuite';
import PubNubReact from 'pubnub-react';
import PaginationUI from "../../../utils/pagination/pagination";
import Snackbar from '@material-ui/core/Snackbar';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
let myref = null;
interface TabPanelProps {
    children?: React.ReactNode;
    index: any;
    value: any;
}
function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;
    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}
class UploadReview extends Component {
    constructor(props) {
        super(props)
        this.state = {
            studentData: [],
            uploadedData: [],
            env: JSON.parse(localStorage.getItem('env')),
            authToken: localStorage.getItem('auth_token'),
            channel: localStorage.getItem('channel'),
            tableResponse: "Fetching Data...",
            rowsPerPage: 10,
            page: 1,
            limit: 10,
            totalPages: 1,
            loader: false,
            activeTab: 0,
            activeTabValue: '',
            fileName: "",
            uploadBtn: false,
            credentials: {},
            OpenUploadModel: false,
            pubnubPercentage: 0,
            uploadingItem: '',
            toasterOpen: false,
            toasterOpenError: false,
            showFinalText: false,
            message: '',
            errorMessage: '',
            toasterMessage: '',
            fileList: [],
            currentItem: '',
            nextItem: '',
        }
        this.pubnub = new PubNubReact({
            subscribeKey: "sub-c-982dbaba-1d98-11ea-8c76-2e065dbe5941",
            publishKey: "pub-c-87ae3cc8-8d0a-40e0-8e0f-dbb286306b21",
            secretKey: "sec-c-ODRhYWJjZmYtZGQ0MS00ZjY2LTkzMGMtY2VhNGZhYjYzOWRi",
            ssl: false,
        });
        this.pubnub.init(this);
    }
    componentDidMount() {
        this.setState({ toasterOpen: true, message: 'Please review and upload' });
        this.pubnub.subscribe({
            channels: [localStorage.getItem('instituteId')],
            withPresence: true
        });
        let fileList = this.state.fileList;
        this.pubnub.getMessage(localStorage.getItem('instituteId'), (message) => {
            console.log(message);
            var name = message.message.description.message
            var current = message.message.description.current
            var next = message.message.description.next
            fileList.map(item => {
                if (item.name == current) {
                    item['status'] = true
                }
                else if (item.name == next) {
                    item['status'] = false
                }
            })
            console.log("Message", fileList)
            var loadedPercentage = Number(message.message.status)
            if (loadedPercentage == 100) {
                this.setState({ showFinalText: true });
            }
            this.setState({ upload: true, pubnubPercentage: loadedPercentage, uploadingItem: name, currentItem: current, nextItem: next })
        });
    }
    componentWillMount() {
        let uploadedData = this.props.uploadedData;
        let credentials = this.props.credentials
        this.setState({ credentials: credentials });
        let parsedData = JSON.parse(uploadedData)
        let studentData = parsedData.uidata;
        let fileName = parsedData.fileName;
        let fileList = []
        let listItems = ["Institute Details", "Fee Types", "Fee Structures", "Program Plans", "Payment Schedules", "Reminder Plans", "Late Fee Plans", "Categories", "Installment Plans", "Concession Plans", "Student Details", "Fee Manager"]
        console.log("list", listItems)
        listItems.map(item => {
            fileList.push({ name: item })
        })
        fileList.map(item => {
            if (item.name == 'Institute Details') {
                item['status'] = false
            }
        })
        console.log("File List Item", fileList);
        let apidata = parsedData.apidata;
        let stringifyData = {
            apidata: apidata,
            uidata: studentData,
        }
        let uploadData = JSON.stringify(stringifyData)
        console.log("upload data", uploadData)
        let initTabName = Object.keys(studentData)
        this.setState({ uploadedData: uploadData, studentData: studentData, activeTabValue: initTabName[0], uploadBtn: true, fileName: fileName, fileList: fileList });
    }
    dateFilter = (ev) => {
        var ts = new Date(ev);
        let getDate = `${String(ts.getDate()).length == 1 ? `0${ts.getDate()}` : ts.getDate()}`;
        let getMonth = `${String(ts.getMonth() + 1).length == 1 ? `0${ts.getMonth() + 1}` : ts.getMonth() + 1}`;
        let getYear = `${ts.getFullYear()}`
        let today = `${getDate}/${getMonth}/${getYear}`
        return today
    }

    formatCurrency = (amount) => {
        return (new Intl.NumberFormat('en-IN', {
            style: 'currency',
            currency: 'INR',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        }).format(amount))
    }
    handleTabChange = (event, value) => {
        this.setState({ activeTab: value, activeTabValue: event.target.textContent, page: 1 });
    }
    // handleChangePage = (event, newPage) => {
    //     this.setState({ page: newPage });
    // };
    // handleChangeRowsPerPage = (event) => {
    //     this.setState({ rowsPerPage: (parseInt(event.target.value, 10)), page: 0 });
    // };
    onPaginationChange = (page, limit) => {
        this.setState({ page: page, limit: limit })
    };
    confirmUpload = () => {
        this.setState({ OpenUploadModel: true });
        var payload = this.state.uploadedData;
        let authToken = this.state.credentials.authToken
        let fd = new FormData();
        const jsonData = new Blob([payload], { type: "application/json" })
        fd.append('file', jsonData)
        var headers = {
            'Authorization': localStorage.getItem('auth_token')
        }
        Axios.post(`${this.state.env['studentUpload']}/edu/uploadMaster?filename=${this.state.fileName}`, fd, { headers: headers })
            .then(res => {
                console.log(res.data)
                if (res.data.status == 'success') {
                    this.setState({ loader: false });
                    this.setState({ toasterOpen: true, message: res.data.message });
                    this.uploadConfirm()
                    this.setState({ OpenUploadModel: false });
                }
                if (res.data.success == true) {
                    Alert.info(res.data.message)
                    this.setState({ loader: false, OpenUploadModel: false });
                    this.goBack();
                    this.setState({ OpenUploadModel: false });
                }
            }).catch(err => {
                console.log(err)
                this.goBack();
                this.setState({ toasterOpenError: true, message: 'Failed to upload data' });
                this.setState({ loader: false, OpenUploadModel: false });
            })
    }
    uploadConfirm = () => {
        this.props.uploadConfirm();
    }
    goBack = () => {
        this.props.goBack();
    }
    toasterClick = (message, toasterMessage) => {
        this.setState({ message, toasterMessage }, () => {
            this.setState({ toasterOpen: true })
        })
    };
    toasterClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        this.setState({ toasterOpen: false, showFinalText: false })
    };
    render() {
        return (
            <React.Fragment>
                <Snackbar
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                    open={this.state.toasterOpen}
                    autoHideDuration={2000}
                    onClose={this.toasterClose}
                    message={`${this.state.message}`}
                    className={`toaster-green ${this.state.toasterMessage}`}
                />
                <Snackbar
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                    open={this.state.toasterOpenError}
                    autoHideDuration={2000}
                    onClose={this.toasterClose}
                    message={`${this.state.errorMessage}`}
                    className={`toaster-red ${this.state.toasterMessage}`}
                />
                {this.state.loader ?
                    <div style={{ position: 'absolute', zIndex: 110, top: 0, left: 0, width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                        <CircularProgress size={24} />
                        {/* <p className="loader-text" style={{ position: "fixed", top: "55vh" }}>Please wait. This might take a while...</p> */}
                    </div> : null}
                <div className="pubnub-progress">
                    <Modal className="ChangeModelBox student-data-model" size="xs" show={this.state.OpenUploadModel}>
                        <Modal.Header className="ModelHeader">
                            <Modal.Title className="ModelTitle">
                                Uploading Student Details:
                            </Modal.Title> </Modal.Header>
                        <Modal.Body className="ModelBody" style={{ width: '100%' }}>
                            <div className="ModelBody-MainDiv">
                                <Box display="flex" alignItems="center">
                                    <Box width="90%" mr={1}>
                                        <div style={{ display: "flex", position: "relative", left: "5px" }}>
                                            <LinearProgress variant="determinate" value={Math.round(this.state.pubnubPercentage)} style={{ width: "98%", top: "12px" }} />
                                            <p style={{ position: "fixed", right: "20px", lineHeight: "26px" }}>{`${Math.round(this.state.pubnubPercentage)}%`}</p>
                                        </div>
                                        {this.state.loader ?
                                            <div style={{ position: 'relative', zIndex: 110, top: "62px", left: "18px", width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', background: 'rgba(255,255,255,0.8)' }}>
                                                <CircularProgress size={24} />
                                            </div> : null}
                                    </Box>
                                </Box>
                                {/* <p className="current-upload-model">{this.state.pubnubPercentage == 100 ? "Upload Completed: " : "Uploading Progress: "} <span>&nbsp; {`${Math.round(this.state.pubnubPercentage)}%`}</span></p> */}
                                <div style={{ marginTop: "30px" }}>
                                    {this.state.fileList.map((item, i) => {
                                        return <div className="progress-report">
                                            <p className="current-upload-item" style={{ fontSize: "12px", fontFamily: "OpenSans-Regular", fontWeight: "600", marginTop: "10px" }}>{item.name}</p>
                                            <div className="circular-loader" style={{ display: item.status ? "block" : "none" }}>
                                                <CheckCircleIcon className="student-confirm-icon" />
                                            </div>
                                            <div className="circular-loader" style={{ display: item.status === false ? "block" : "none" }}>
                                                <CircularProgress size={12} />
                                            </div>
                                        </div>
                                    })}
                                    {this.state.showFinalText ? <p style={{ position: "fixed", bottom: "-15px", fontSize: "12px", fontFmily: "OpenSans-Regular", fontWeight: "600" }}>Completing the setup</p> : null}
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer className="ModelFooter">
                        </Modal.Footer>
                    </Modal>
                </div>
                <div className='migration-main-div migration-body-content-section' style={{ backgroundColor: "transparent", marginTop: "40px" }}>
                    {this.state.uploadBtn ?
                        <div className='student-data-center-headers'>
                            <Button variant="primary" onClick={this.confirmUpload} className="confirm-upload-btn" style={{ width: "140px auto" }}> Confirm Upload </Button>
                        </div>
                        : null}
                    <div className='preview-table'>
                        <Tabs
                            value={this.state.activeTab}
                            indicatorColor="primary"
                            textColor="primary"
                            onChange={this.handleTabChange}
                            className='preview-tabs'
                        >
                            {Object.keys(this.state.studentData).map((tab, tabIndex) => {
                                return <Tab label={tab} />
                            })}
                        </Tabs>
                        {Object.keys(this.state.studentData).map((tab, tabIndex) => {
                            const { activeTab, page, activeTabValue, limit, studentData } = this.state;
                            let activeTabData = [];
                            if (activeTab == 0) {
                                studentData[activeTabValue].forEach((item) => {
                                    if (!item[""]) item[""] = ""
                                    activeTabData.push(item);
                                })
                            } else {
                                activeTabData = studentData[activeTabValue];
                            }
                            const start = activeTab == 0 ? 0 : ((page - 1) * limit);
                            const end = activeTab == 0 ? activeTabData.length : ((page * limit));
                            const tableArrayData = activeTabData.slice(start, end);
                            return <TabPanel value={this.state.activeTab} index={tabIndex}>
                                <CloseIcon className="close-preview" onClick={this.goBack} />
                                {tab.length == 0 ? <p className="fetching-data">No data has been uploaded so far...</p> :
                                    <React.Fragment>
                                        <div className="preview-table-main">
                                            <TableContainer className="preview-table-container">
                                                <Table
                                                    className='table-confirm-upload'
                                                    aria-labelledby="tableTitle"
                                                    size={this.state.dense ? 'small' : 'medium'}
                                                    aria-label="enhanced table">
                                                    <TableHead className="thead-confirm-upload">
                                                        {this.state.studentData[this.state.activeTabValue][0] != undefined ?
                                                            <TableRow className='preview-table-headRow'>
                                                                {Object.keys(this.state.studentData[this.state.activeTabValue][0]).map((key) => {
                                                                    return (<TableCell className='preview-table-headCell'>{key}</TableCell>)
                                                                })}
                                                            </TableRow> : <p style={{ marginTop: "20px" }}>No Data</p>}
                                                    </TableHead>
                                                    <TableBody className='tbody-preview'>
                                                        {tableArrayData.map(data => {
                                                            return <TableRow className='preview-table-bodyRow'>
                                                                {Object.keys(data).map(data1 => {
                                                                    if (data1.toLowerCase().includes('fees') || data1.toLowerCase().includes('amount') || data1.toLowerCase().includes('paid') || data1.toLowerCase().includes('total fees ') || data1.toLowerCase().includes('fees collected ')) {
                                                                        return (
                                                                            <React.Fragment>
                                                                                <TableCell className='preview-table-headCellAmt'>{this.formatCurrency(data[data1] == "-" ? 0.00 : data[data1])}</TableCell>
                                                                            </React.Fragment>
                                                                        )
                                                                    }
                                                                    if (data1.toLowerCase().includes('date of registration') || data1.toLowerCase().includes('dob')) {
                                                                        return (
                                                                            <React.Fragment>
                                                                                <TableCell className='preview-table-headCellDate'>{data[data1] === "" ? "-" : this.dateFilter(data[data1])}</TableCell>
                                                                            </React.Fragment>
                                                                        )
                                                                    }
                                                                    if (data1.toLowerCase().includes('status')) {
                                                                        return (
                                                                            <React.Fragment>
                                                                                <TableCell className='preview-table-active'><p className="active-tag">{data[data1] === "-" ? "-" : data[data1]}</p></TableCell>
                                                                            </React.Fragment>
                                                                        )
                                                                    }
                                                                    else if (data[data1].toString().replace(/ /g, "") == "") {
                                                                        return (
                                                                            <React.Fragment>
                                                                                <TableCell className={String(data[data1]).includes('addingHead') ? 'preview-table-mainCell' : 'preview-table-bodyCell'}>{(!data[data1] || data[data1].trim() == "" ? "-" : data[data1]).toString().replace('addingHead', '')}</TableCell>
                                                                            </React.Fragment>
                                                                        )
                                                                    } {
                                                                        // console.log("data[data1].trim()", data[data1].trim())
                                                                        return (
                                                                            <React.Fragment>
                                                                                <TableCell className={String(data[data1]).includes('addingHead') ? 'preview-table-mainCell' : 'preview-table-bodyCell'}>{(!data[data1] || data[data1].trim() == "" ? "-" : data[data1]).toString().replace('addingHead', '')}</TableCell>
                                                                            </React.Fragment>
                                                                        )
                                                                    }
                                                                })}
                                                            </TableRow>
                                                        })}
                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                            {activeTab != 0 && this.state.studentData[this.state.activeTabValue].length > 0 ? <PaginationUI
                                                total={this.state.studentData[this.state.activeTabValue].length}
                                                onPaginationApi={this.onPaginationChange}
                                                totalPages={Math.ceil(this.state.studentData[this.state.activeTabValue].length / this.state.limit)}
                                                limit={this.state.limit}
                                                currentPage={this.state.page}
                                            /> : ''}
                                        </div>
                                    </React.Fragment>
                                }
                            </TabPanel>
                        })}
                    </div>
                </div>
            </React.Fragment >
        )
    }
}
export default withRouter(UploadReview)