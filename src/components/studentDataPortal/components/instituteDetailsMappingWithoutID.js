export const instituteDetailsMappings = {
    "a2": {
        "parent": null,
        "key": "legalName",
        "value": "b2"
    },
    "a3": {
        "parent": "legalName",
        "key": "legalName",
        "value": "b3"
    },
    "a4": {
        "parent": "legalName",
        "key": "dateOfRegistration",
        "value": "b4"
    },
    "a5": {
        "parent": null,
        "key": "legalAddress",
        "value": "b5"
    },
    "a6": {
        "parent": "legalAddress",
        "key": "address1",
        "value": "b6"
    },
    "a7": {
        "parent": "legalAddress",
        "key": "address2",
        "value": "b7"
    },
    "a8": {
        "parent": "legalAddress",
        "key": "address3",
        "value": "b8"
    },
    "a9": {
        "parent": "legalAddress",
        "key": "city",
        "value": "b9"
    },
    "a10": {
        "parent": "legalAddress",
        "key": "state",
        "value": "b10"
    },
    "a11": {
        "parent": "legalAddress",
        "key": "country",
        "value": "b11"
    },
    "a12": {
        "parent": "legalAddress",
        "key": "pincode",
        "value": "b12"
    },
    "a13": {
        "parent": null,
        "key": "gstPan",
        "value": "13"
    },
    "a14": {
        "parent": "gstPan",
        "key": "GSTIN",
        "value": "b14"
    },
    "a15": {
        "parent": "gstPan",
        "key": "PAN",
        "value": "b15"
    },
    "a16": {
        "parent": null,
        "key": "bankDetails1",
        "value": "16"
    },
    "a17": {
        "parent": "bankDetails1",
        "key": "bankName",
        "value": "b17"
    },
    "a18": {
        "parent": "bankDetails1",
        "key": "bankAccountName",
        "value": "b18"
    },
    "a19": {
        "parent": "bankDetails1",
        "key": "bankAccountNumber",
        "value": "b19"
    },
    "a20": {
        "parent": "bankDetails1",
        "key": "bankIFSC",
        "value": "b20"
    },
    "a21": {
        "parent": null,
        "key": "bankDetails2",
        "value": "b21"
    },
    "a22": {
        "parent": "bankDetails2",
        "key": "bankName",
        "value": "b22"
    },
    "a23": {
        "parent": "bankDetails2",
        "key": "bankAccountName",
        "value": "b23"
    },
    "a24": {
        "parent": "bankDetails2",
        "key": "bankAccountNumber",
        "value": "b24"
    },
    "a25": {
        "parent": "bankDetails2",
        "key": "bankIFSC",
        "value": "b25"
    },
    "a26": {
        "parent": null,
        "key": "bankDetails3",
        "value": "b26"
    },
    "a27": {
        "parent": "bankDetails3",
        "key": "bankName",
        "value": "b27"
    },
    "a28": {
        "parent": "bankDetails3",
        "key": "bankAccountName",
        "value": "b28"
    },
    "a29": {
        "parent": "bankDetails3",
        "key": "bankAccountNumber",
        "value": "b29"
    },
    "a30": {
        "parent": "bankDetails3",
        "key": "bankIFSC",
        "value": "b30"
    },
    "a31": {
        "parent": null,
        "key": "bankDetails4",
        "value": "b31"
    },
    "a32": {
        "parent": "bankDetails4",
        "key": "bankName",
        "value": "b32"
    },
    "a33": {
        "parent": "bankDetails4",
        "key": "bankAccountName",
        "value": "b33"
    },
    "a34": {
        "parent": "bankDetails4",
        "key": "bankAccountNumber",
        "value": "b34"
    },
    "a35": {
        "parent": "bankDetails4",
        "key": "bankIFSC",
        "value": "b35"
    },
    "a36": {
        "parent": null,
        "key": "bankDetails5",
        "value": "b36"
    },
    "a37": {
        "parent": "bankDetails5",
        "key": "bankName",
        "value": "b37"
    },
    "a38": {
        "parent": "bankDetails5",
        "key": "bankAccountName",
        "value": "b38"
    },
    "a39": {
        "parent": "bankDetails5",
        "key": "bankAccountNumber",
        "value": "b39"
    },
    "a40": {
        "parent": "bankDetails5",
        "key": "bankIFSC",
        "value": "b40"
    },
    "a41": {
        "parent": null,
        "key": "instituteContactDetail1",
        "value": "b41"
    },
    "a42": {
        "parent": "instituteContactDetail1",
        "key": "contactname",
        "value": "b42"
    },
    "a43": {
        "parent": "instituteContactDetail1",
        "key": "designation",
        "value": "b43"
    },
    "a44": {
        "parent": "instituteContactDetail1",
        "key": "department",
        "value": "b44"
    },
    "a45": {
        "parent": "instituteContactDetail1",
        "key": "emailAddress",
        "value": "b45"
    },
    "a46": {
        "parent": "instituteContactDetail1",
        "key": "phoneNumber",
        "value": "b46"
    },
    "a47": {
        "parent": "instituteContactDetail1",
        "key": "mobileNumber",
        "value": "b47"
    },
    "a48": {
        "parent": null,
        "key": "instituteContactDetail2",
        "value": "b48"
    },
    "a49": {
        "parent": "instituteContactDetail2",
        "key": "contactname",
        "value": "b49"
    },
    "a50": {
        "parent": "instituteContactDetail2",
        "key": "designation",
        "value": "b50"
    },
    "a51": {
        "parent": "instituteContactDetail2",
        "key": "department",
        "value": "b51"
    },
    "a52": {
        "parent": "instituteContactDetail2",
        "key": "emailAddress",
        "value": "b52"
    },
    "a53": {
        "parent": "instituteContactDetail2",
        "key": "phoneNumber",
        "value": "b53"
    },
    "a54": {
        "parent": "instituteContactDetail2",
        "key": "mobileNumber",
        "value": "b54"
    }
}